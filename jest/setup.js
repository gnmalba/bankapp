/* eslint-disable no-undef */
jest.mock('react-native-gesture-handler', () => {
  const View = require('react-native/Libraries/Components/View/View');
  return {
    Swipeable: View,
    DrawerLayout: View,
    State: {},
    ScrollView: View,
    Slider: View,
    Switch: View,
    TextInput: View,
    ToolbarAndroid: View,
    ViewPagerAndroid: View,
    DrawerLayoutAndroid: View,
    WebView: View,
    NativeViewGestureHandler: View,
    TapGestureHandler: View,
    FlingGestureHandler: View,
    ForceTouchGestureHandler: View,
    LongPressGestureHandler: View,
    PanGestureHandler: View,
    PinchGestureHandler: View,
    RotationGestureHandler: View,
    /* Buttons */
    RawButton: View,
    BaseButton: View,
    RectButton: View,
    BorderlessButton: View,
    /* Other */
    FlatList: View,
    gestureHandlerRootHOC: jest.fn(),
    Directions: {},
    TouchableOpacity: View,
  };
});

jest.mock('react-native-tab-view', () => {});
jest.mock('reanimated-bottom-sheet', () => 'Animated.View');
import mockAsyncStorage from '@react-native-async-storage/async-storage/jest/async-storage-mock';

jest.mock('@react-native-async-storage/async-storage', () => mockAsyncStorage);
jest.mock('react-native-reanimated', () => {
  const Reanimated = require('react-native-reanimated/mock');
  Reanimated.default.call = () => {};
  return Reanimated;
});
jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');
jest.mock('react-native', () => jest.requireActual('react-native'));
jest.mock('react-native-elements', () => {
  return {
    Text: 'Text',
    Divider: 'Divider',
    Tooltip: 'Tooltip',
    CheckBox: 'CheckBox',
    Overlay: 'Overlay',
    Header: 'Header',
    ListItem: () => 'ListItem',
  };
});
jest.mock('react-native-vector-icons', () => ({
  Icon: 'Icon',
  createIconSetFromIcoMoon: () => 'Icon',
}));
jest.mock('react-native-vector-icons/Ionicons', () => 'Icon');
jest.mock('react-native-vector-icons/MaterialIcons', () => 'Icon');
jest.mock('react-native-vector-icons/Entypo', () => 'Icon');
jest.mock('react-native-vector-icons/Fontisto', () => 'Icon');
jest.mock('react-native-vector-icons/Feather', () => 'Icon');
jest.mock('@utils/componentCustomProps', () => ({
  testProps: (reference: string): Object => ({
    testID: reference,
    accessibilityLabel: reference,
    theme: {
      circle: {
        dashboardBgColor: '#153961',
        overviewBgColor: '#153961',
      },
      mainTopMenuItems: {
        color: '#ffffff',
      },
      tab: {
        activeTintColor: '#09BAD0',
        backgroundColor: '#010E1D',
        boxShadow: '0px -2px 32px #04172D',
      },
      sidebar: {
        headerBg: '#010E1D',
      },
      logoSrc: require('@assets/images/mb-logo-dark.png'),
      color: {
        PRIMARY_DISABLED: '#8DA0B2',
        PRIMARY_DEFAULT: '#00539F',
        PRIMARY_PRESSED: '#004585',
        SECONDARY_DISABLED: '#B4CDD0',
        SECONDARY_DEFAULT: '#09BAD0',
        SECONDARY_PRESSED: '#08A2B5',
        BACKGROUND_300: '#02152B',
        BACKGROUND_200: '#07203C',
        BACKGROUND_100: '#102C4B',
        TEXT_300_LOW: '#445870',
        TEXT_300_HIGH: '#FFFFFF',
        TEXT_200_MEDIUIM: '#51667C',
        TEXT_100_HIGH: '#A2BDDB',
        GREEN_STATUS: '#84D268',
        RED_STATUS: '#F65A4C',
        DISABLED_STATUS: '#45586D',
        ORANGE_STATUS: '#FF8A00',
        YELLOW_STATUS: '#F9DE7D',
        HIGH_EMPHASIS: '#A2BDDB',
      },
      card: {
        textColor: '#ffffff',
        backgroundColor: '#102c4b',
        dividerBgColor: '#445870',
        availableBalance: '#A2BDDB',
      },
      dashboard: {
        tabIcon: require('@assets/icons/tabs/dashboard-dark.png'),
      },
      text: {
        textColor: '#ffffff',
      },
      textInput: {
        backgroundColor: {
          default: '#02152b',
          filled: '#02152B',
          disabled: '#051931',
          active: '#02152B',
          correct: '#02152B',
          error: '#02152B',
        },
        textColor: '#ffffff',
        placeholderTextColor: '#486485',
        border: {
          default: 'transparent',
          filled: 'transparent',
          disabled: 'transparent',
          active: '1px solid #09BAD0',
          correct: '1px solid #84D268',
          error: '1px solid #F65A4C',
        },
        helperTextColor: {
          default: '#A2BDDB',
          filled: '#A2BDDB',
          disabled: '#A2BDDB',
          active: '#A2BDDB',
          correct: '#A2BDDB',
          error: '#F65A4C',
        },
        iconInputStatusImagesTint: 'none',
        calendarActiveTint: 'none',
        searchImageTint: 'none',
        dividerColor: '#445870',
      },
      topAppBar: {
        backgroundColor: '#02152b',
        textColor: '#ffffff',
        barStyle: 'light-content',
        loading: '#030e1d',
        iconColor: '#ffffff',
      },
      layout: {
        src: require('@assets/images/main-bg-dark.png'),
        backgroundColor: '#051931',
      },
      passwordInput: {
        src: require('@assets/icons/visibility-dark.png'),
        mode: 'dark',
      },
      button: {
        textColor: {
          disabled: '#748291',
        },
        backgroundColor: {
          disabled: '#45586D',
        },
      },
      skeletonPlaceholder: {
        highlightColor: '#f5f5f5',
        backgroundColor: '#486788',
      },
      modal: {
        backgroundColor: '#102C4B',
      },
      tabNavigator: {
        borderBottomColor: '#445870',
      },
      FilterIcon: require('@assets/icons/filter.png'),
      transaction: {
        background: '#051931',
      },
      circularGraphBackground: '#02152B',
      defaultColor: '#09BAD0',
      calendar: {
        defaultDateColor: '#09BAD0',
        defaultDateColorWithOpacity: 'rgba(9, 186, 208, 0.5)',
        selectedDayTextColor: '#FFFFFF',
      },
      carousel: {
        indicatorColor: '#445870',
      },
      guidedTourTooltip: {
        background: '#18416E',
      },
      customizeDashboard: {
        customizeText: '#FFFFFF',
      },
      displayOptionsTooltip: {
        tooltip: '#18416E',
        divider: '#486485',
        widgetHeaderIcon: ' #2a69af',
      },
      filterTransaction: {
        divider: '#445870',
        calendarInputBorder: '#02152B',
        placeholderText: '#445870',
        numberOfFiltersCircle: '#163E69',
      },
      reminder: {
        yellow: {
          backgroundColor: '#F2EBC3',
          borderColor: '#BDA726',
          textColor: '#7E6F1B',
        },
      },
    },
  }),
}));
jest.mock('jsencrypt', () => ({
  JSEncrypt: function () {
    this.publicKey = '';
    this.setPublicKey = function (key) {
      this.publicKey = key;
    };
    this.encrypt = function (code) {
      return code;
    };
  },
}));
jest.mock('react-native-device-info', () => {
  return {
    hasNotch: jest.fn(),
    getDeviceId: jest.fn(),
    getUniqueId: jest.fn(),
    getSystemVersion: jest.fn(),
    getModel: jest.fn(),
  };
});
jest.mock('react-native-background-timer', () => {
  return {
    runBackgroundTimer: jest.fn(),
    stopBackgroundTimer: jest.fn(),
  };
});
jest.mock('@react-navigation/core', () => {
  return {
    ...jest.requireActual('@react-navigation/core'),
    useNavigation: () => ({
      navigate: () => {},
    }),
    useIsFocused: () => true,
  };
});
jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: () => {},
    }),
    useIsFocused: () => true,
  };
});
jest.mock('react-native-sensitive-info', () => ({
  setItem: (key, value, options) => {},
  deleteItem: (key, options) => {},
  getItem: (key, options) => Promise.resolve('mockKeyValue'),
}));
jest.mock('react-native-rsa-native', () => {
  return {
    RSA: {
      decrypt: (tkn, key) => tkn,
      signWithAlgorithm: (token, key, alg) => token,
      generateKeys: (bits) => ({
        public: 'Random Public',
        private: 'Random Private',
      }),
    },
  };
});
jest.mock('react-native-biometrics', () => ({
  isSensorAvailable: () => ({
    available: true,
    biometryType: 'TouchID',
    error: '',
  }),
  TouchID: 'TouchID',
  FaceID: 'FaceID',
  Biometrics: 'Biometrics',
}));
