module.exports = {
  presets: ['module:metro-react-native-babel-preset', '@babel/preset-flow'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '@components': './src/components',
          '@composition': './src/composition',
          '@screens': './src/screens',
          '@navigations': './src/navigations',
          '@assets': './src/assets',
          '@ducks': './src/ducks',
          '@utils': './src/utils',
          '@api': './src/api',
          '@types': './src/types',
          '@enums': './src/enums',
        },
      },
    ],
    [
      'module:react-native-dotenv',
      {
        moduleName: 'react-native-dotenv',
      },
    ],
  ],
};
