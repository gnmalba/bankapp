# BankApp

A replica of a bank app

## Running the tests

```
This command will execute test script
$ yarn test
```

```
This command will execute test script. It aso ndicates that test coverage information should be collected and reported in the output.
$ yarn test:coverage
```

## Deployment

For iOS, the xarchive is required
-Use the Organizer in Xcode to deploy to Testflight or generate .ipa

For Android, use the APK to install directly to the device and/or upload to Google Play

For Huawei, use the APK to upload to Huawei App Gallery

## Built With

- [React Native](https://reactnative.dev/) - The framework used
- [Android Studio](https://developer.android.com/) - Android Dependency
- [XCODE](https://developer.apple.com/xcode/) - IOS Dependency
