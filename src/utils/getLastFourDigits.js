// @flow
export const getLastFourDigits = (cardNum: string): string => {
  return cardNum.slice(cardNum.length - 4);
};
