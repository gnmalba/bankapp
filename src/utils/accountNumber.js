export const accountsFormat = (props) => {
  const accountNo = props || '0000000000';
  return `*** *** *** ${accountNo.substring(accountNo.length - 4)}`;
};

export const accountsFormat16 = (props) => {
  const accountNo = props || '0000000000';

  let stars: string;
  switch (accountNo.length) {
    case 13:
      stars = '*** *** *** ';
      break;
    case 14:
      stars = '**********';
      break;
    case 15:
      stars = '***********';
      break;
    default:
      stars = '**** **** **** ';
  }

  return `${stars}${accountNo.substring(accountNo.length - 4)}`;
};

export const maskingFormat = (
  value = '',
  mask = false,
  isAccountDetail = false,
) => {
  if (mask) {
    const last4digits = value?.slice(-4);
    let maskNumber = '';

    for (var i = value?.length - 4; i > 0; i--) {
      maskNumber += '*';
    }

    let number, chuncks;
    if (value.length === 13) {
      // deposit, mb prepaid
      number = `${maskNumber}`;
      chuncks = number.match(/.{1,3}/g) || [];
      return `${chuncks.join(' ')}  ${last4digits}`;
    } else {
      // credit, yazz
      number = `${maskNumber}${last4digits}`;
      chuncks = number.match(/.{1,4}/g) || [];
      return chuncks.join(' ');
    }
  } else {
    if (isAccountDetail) {
      const item = value.match(/.{1,4}/g) || [];
      return item.join(' ');
    }

    const last4digits = value?.slice(-4);
    const chuncks = value?.slice(0, -4).match(/.{1,3}/g) || [];
    return `${chuncks.join(' ')} ${last4digits}`;
  }
};

export const numberFormat = (number = 0, fractionDigits = 2) => {
  let newval = parseFloat(Math.round(number * 100) / 100).toFixed(
    fractionDigits,
  );

  if (countDecimals(newval) === 1) {
    newval = newval + '0';
  }

  return newval?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const countDecimals = (value) => {
  if (!value || !value.includes('.')) {
    return 0;
  }

  const hasDecimal = value.split('.').length > 1;
  if (hasDecimal) {
    return value.split('.')[1].length;
  }
};

// eslint-disable-next-line sonarjs/cognitive-complexity
export const accountNumberInputMask = (value = '', isPlaceholder = false) => {
  let chunks = [];
  const creditChunksRegex = /.{1,4}/g;
  const depositChunksRegex = /.{1,3}/g;
  const onlyDigitsRegex = /[^0-9]/g;

  if (value) {
    const cleanedValue = !isPlaceholder
      ? value.replace(onlyDigitsRegex, '')
      : value;
    if (cleanedValue.length === 16) {
      chunks = cleanedValue.match(creditChunksRegex) || [];
      chunks = chunks.join(' ');
    } else if (cleanedValue.length > 13) {
      chunks = cleanedValue;
    } else {
      if (cleanedValue.length > 9) {
        const chunk1 = cleanedValue.substring(0, 9).match(depositChunksRegex);
        let chunk2 = cleanedValue.substring(9, cleanedValue.length);

        if (chunk2.length >= 4) {
          chunk2 = chunk2.substring(0, 9).match(creditChunksRegex).join(' ');
        }

        chunks = `${chunk1.join(' ')} ${chunks} ${chunk2}`;
      } else {
        chunks = cleanedValue.match(depositChunksRegex) || [];
        chunks = chunks.join(' ');
      }
    }
  } else {
    return '';
  }

  if (chunks.length > 19) {
    return chunks.slice(0, -4);
  }
  return chunks;
};
