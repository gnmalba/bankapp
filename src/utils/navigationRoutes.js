// Enrollment Navigation
export const NAVIGATION_ENROLLMENT_FORM = 'EnrollmentForm';
export const NAVIGATION_ENROLLMENT_OTP = 'EnrollmentOTP';
export const NAVIGATION_ENROLLMENT_SUCCESS = 'EnrollmentSuccess';
export const NAVIGATION_ENROLLMENT_ERROR = 'EnrollmentError';
