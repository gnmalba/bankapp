// @flow

import ACCOUNT_TYPES from '@enums/account-types.enum';
import type {InstallmentTypesString} from '@ducks/installments/types';
import type {IShortcut} from '@types/shortcut.type';

export const sortAliasAlphabetically = (arr: any): any =>
  arr.sort((a, b) => {
    if (a.depositAccount.alias < b.depositAccount.alias) {
      return -1;
    }
    if (a.depositAccount.alias > b.depositAccount.alias) {
      return 1;
    }
    return 0;
  });

export const sortCreditCardAlphabetically = (
  arr: any[],
  installmentType: InstallmentTypesString,
): any =>
  arr
    .sort((a, b) => {
      if (a.cardEmbossedName.toLowerCase() < b.cardEmbossedName.toLowerCase()) {
        return -1;
      }
      if (a.cardEmbossedName.toLowerCase() > b.cardEmbossedName.toLowerCase()) {
        return 1;
      }
      return 0;
    })
    .sort((a, b) => {
      const property =
        installmentType === 'BC' ? 'outstandingBalance' : 'availableCredit';

      return b[property] - a[property];
    });

export const getCardNumber = (type: any, item: any): any => {
  switch (type) {
    case ACCOUNT_TYPES.DEPOSIT:
    case ACCOUNT_TYPES.MB_PREPAID:
      return item.accountNo;
    case ACCOUNT_TYPES.CC:
    case ACCOUNT_TYPES.YAZZ:
      return item.cardNo;
    default:
      return '';
  }
};

export const getEnabledShortcutsByCode = (
  shortcuts: IShortcut[],
  code: string[],
): IShortcut[] => {
  return shortcuts.filter(
    (shortcut) => code.includes(shortcut.code) && shortcut.enabled,
  );
};
