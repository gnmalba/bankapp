// @flow

export type ImageResizeMode = 'contain' | 'cover' | 'center' | 'repeat';
export type BasicTextInputState =
  | 'default'
  | 'filled'
  | 'disabled'
  | 'active'
  | 'correct'
  | 'error';

type iconEnumType = 'date' | 'username' | 'password' | 'search';

export type themeColorType =
  | 'PRIMARY_DISABLED'
  | 'PRIMARY_DEFAULT'
  | 'PRIMARY_PRESSED'
  | 'SECONDARY_DISABLED'
  | 'SECONDARY_DEFAULT'
  | 'SECONDARY_PRESSED'
  | 'BACKGROUND_300'
  | 'BACKGROUND_200'
  | 'BACKGROUND_100'
  | 'TEXT_300_LOW'
  | 'TEXT_300_HIGH'
  | 'TEXT_200_MEDIUIM'
  | 'TEXT_100_HIGH'
  | 'GREEN_STATUS'
  | 'RED_STATUS'
  | 'DISABLED_STATUS'
  | 'ORANGE_STATUS'
  | 'YELLOW_STATUS'
  | 'CANCEL_BUTTON';

export type accessibilityType = {
  testID: string,
  accessibilityLabel: string,
};

export type BasicTextInputType = {
  testID?: string,
  accessibilityLabel: string,
  placeholder?: string,
  value?: string,
  onChangeText?: () => any,
  nativeProps?: Object,
  state?: BasicTextInputState,
  theme?: Object,
};

export type LabelledTextInputType = {
  ...BasicTextInputType,
  label?: string,
  labelSuffix?: Node,
  labelProps?: Object,
  inputProps?: Object,
  theme?: Object,
};

export type IconInputStatusType = {
  ...BasicTextInputType,
  icon?: iconEnumType,
  customIcon?: React$Node,
  customStatus?: React$Node,
  iconLabel?: string,
  showStatus?: boolean,
  handleClear?: () => any,
  secureTextEntry?: boolean,
  iconPressHandler?: (options: any) => any,
  theme?: Object,
  testID?: string,
};

export type TextInputProps = {
  ...IconInputStatusType,
  label?: string,
  helperText?: string,
  theme?: Object,
  dividerHeight?: number,
  isFromNewPassword?: boolean,
  showTextValidation?: (options: any) => any,
  setError?: () => void,
  onBlur?: () => void,
};

export type ThemeInterface = {|
  deviceIcon: string,
  circle: {
    dashboardBgColor: string,
    overviewBgColor: string,
  },
  mainTopMenuItems: {
    color: string,
  },
  tab: {
    activeTintColor: string,
    backgroundColor: string,
    boxShadow: string,
  },
  sidebar: {
    headerBg: string,
  },
  logoSrc: string,
  color: {
    PRIMARY_DISABLED: string,
    PRIMARY_DEFAULT: string,
    PRIMARY_PRESSED: string,
    SECONDARY_DISABLED: string,
    SECONDARY_DEFAULT: string,
    SECONDARY_PRESSED: string,
    BACKGROUND_300: string,
    BACKGROUND_200: string,
    BACKGROUND_100: string,
    TEXT_300_LOW: string,
    TEXT_300_HIGH: string,
    TEXT_200_MEDIUIM: string,
    TEXT_100_HIGH: string,
    GREEN_STATUS: string,
    RED_STATUS: string,
    DISABLED_STATUS: string,
    ORANGE_STATUS: string,
    YELLOW_STATUS: string,
    HIGH_EMPHASIS: string,
  },
  card: {
    textColor: string,
    backgroundColor: string,
    dividerBgColor: string,
    availableBalance: string,
  },
  dashboard: {
    tabIcon: string,
  },
  text: {
    textColor: string,
    textEmphasis: string,
  },
  textInput: {
    backgroundColor: {
      default: string,
      filled: string,
      disabled: string,
      active: string,
      correct: string,
      error: string,
    },
    textColor: string,
    placeholderTextColor: string,
    border: {
      default: string,
      filled: string,
      disabled: string,
      active: string,
      correct: string,
      error: string,
    },
    helperTextColor: {
      default: string,
      filled: string,
      disabled: string,
      active: string,
      correct: string,
      error: string,
    },
    iconInputStatusImagesTint: string,
    calendarActiveTint: string,
    searchImageTint: string,
    dividerColor: string,
  },
  topAppBar: {
    backgroundColor: string,
    textColor: string,
    barStyle: string,
    loading: string,
    iconColor: string,
  },
  layout: {
    src: string,
    backgroundColor: string,
  },
  passwordInput: {
    src: string,
    mode: string,
  },
  button: {
    textColor: {
      disabled: string,
    },
    backgroundColor: {
      disabled: string,
    },
  },
  skeletonPlaceholder: {
    highlightColor: string,
    backgroundColor: string,
  },
  modal: {
    backgroundColor: string,
  },
  tabNavigator: {
    borderBottomColor: string,
  },
  FilterIcon: string,
  transaction: {
    background: string,
  },
  circularGraphBackground: string,
  defaultColor: string,
  calendar: {
    defaultDateColor: string,
    defaultDateColorWithOpacity: string,
    selectedDayTextColor: string,
  },
  carousel: {
    indicatorColor: string,
  },
  guidedTourTooltip: {
    background: string,
  },
  customizeDashboard: {
    customizeText: string,
  },
  displayOptionsTooltip: {
    tooltip: string,
    divider: string,
    widgetHeaderIcon: string,
  },
  filterTransaction: {
    divider: string,
    calendarInputBorder: string,
    placeholderText: string,
    numberOfFiltersCircle: string,
    cancelButton: string,
  },
  reminder: {
    yellow: {
      backgroundColor: string,
      borderColor: string,
      textColor: string,
    },
  },
  trasactionList: {
    background: string,
  },
  circleBackground: string,
  sectionHeader: string,
  otherActions: {
    containerBackground: string,
    contentContainerBackground: string,
  },
  activityLog: {
    date: string,
  },
|};
