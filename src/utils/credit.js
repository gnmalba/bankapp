export const getCreditPercentage = (
  creditLimit: number,
  availableCredit: number,
  flag: ?string,
): number => {
  if (flag === null || flag === 'NOT_ALLOWED') {
    return 0;
  }

  const calculatedPercentage =
    ((creditLimit - availableCredit) / creditLimit) * 100;

  return Math.round((calculatedPercentage + Number.EPSILON) * 100) / 100 || 0;
};
