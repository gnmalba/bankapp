// @flow

export const testProps = (reference: string): Object => ({
  testID: reference,
  accessibilityLabel: reference,
});

export const generateId = (reference: string): Object => {
  if (!reference) {
    return {};
  }
  return testProps(transformTxt(reference));
};
function transformTxt(string: string): string {
  if (!string) {
    return '';
  }
  var id = string
    .toLowerCase()
    .replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
  return `id_${id}`;
}
