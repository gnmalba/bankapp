/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import {useState, useEffect} from 'react';

export const useCountdown = (length: number): Array<any> => {
  const [expire, setExpire] = useState(length + 1);
  const [basis, setBasis] = useState();
  const [timer, setTimer] = useState(0);
  const [timerDisp, setTimerDisp] = useState(length);
  const [timerInt, setTimerInt] = useState();

  useEffect(() => {
    setBasis(new Date(Date.now() + 1000 * expire));
  }, [expire]);

  useEffect(() => {
    let intervalId;
    if (basis) {
      intervalId = setInterval(() => {
        setTimer(new Date().valueOf());
      }, 100);
      setTimerInt(intervalId);
    }
    return () => {
      clearInterval(intervalId);
    };
  }, [basis]);

  useEffect(() => {
    if (basis && timer) {
      const toDisp = Math.floor((basis - timer) / 1000);
      if (timerDisp !== toDisp) {
        setTimerDisp(toDisp);
      }
    }
  }, [timer]);

  useEffect(() => {
    if (timerDisp <= 0) {
      clearInterval(timerInt);
      setExpire(0);
    }
  }, [timerDisp]);

  return [timerDisp, setExpire];
};
