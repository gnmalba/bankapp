/**
 * @flow
 */

/*############### SAMPLE ########################*/
/*
const accountLoading = useAccountsLoading();
true === accounts and widgets are not loaded yet.
false === loadings of widgets and accounts are done.
*/
import {getActiveWidgets} from '@ducks/config/selectors';
import {useSelector} from 'react-redux';

const useAccountsLoading = (): boolean => {
  const activeWidgets = useSelector(getActiveWidgets);

  /**
   * select loading state of all accounts
   */
  const {creditLoading, depositsLoading, yazzLoading} = useSelector(
    (state) => ({
      creditLoading: state.credit.creditList.loading,
      depositsLoading: state.deposit.fetching,
      yazzLoading: state.yazz.loading,
    }),
  );

  //this makes sure all accounts are loaded
  const cardLoadingStates = [
    creditLoading,
    depositsLoading,
    yazzLoading,
    ...activeWidgets.map((row) => row.loading),
  ];

  const isFalse = (currentValue) => currentValue === false;

  const areCardsNotLoading = cardLoadingStates.every(isFalse);

  //loading state if cards/accounts are still fetching
  return !areCardsNotLoading;
};

export default useAccountsLoading;
