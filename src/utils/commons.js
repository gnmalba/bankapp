import {
  getUniqueId,
  getSystemVersion,
  getDeviceId,
} from 'react-native-device-info';
import {Platform, Dimensions} from 'react-native';
import MD5 from 'crypto-js/md5';
// sort for string values only
export const sortByObjectKey = (key: string) => (a, b) =>
  a[key].toLowerCase() > b[key].toLowerCase()
    ? 1
    : b[key].toLowerCase() > a[key].toLowerCase()
    ? -1
    : 0;

export const removeSpace = (value: string) => {
  return value.replace(/\s+/g, '');
};

/**
 *iterate entries of an object Object values
 * @param {*} object
 * @param {*} fn
 * Usage:
 * iterateEntries(yourObject, (entry) => {
  return entry; // do something with entry here
});
 */
export const iterateEntries = (object: Object, fn: any) => {
  const newObject = {};
  Object.entries(object).forEach(([key, val]) => (newObject[key] = fn(val)));
  return newObject;
};

export const getDeviceInfo = () => {
  return {
    deviceId: getUniqueId(),
    brand: getDeviceId(),
    model: `${Platform.OS} ${getSystemVersion()}`,
    isDeveloperMode: true,
    isRooted: true,
  };
};

export type hashProps = {
  accountId?: string,
  username: string,
  amount: string,
  currencyCode: string,
  billerId: string,
};
//generate Hash for PBC
export const generateHash = ({
  accountId,
  username,
  amount,
  currencyCode,
  billerId,
}: hashProps): string =>
  MD5(
    accountId.toString() +
      '' +
      username.toLowerCase() +
      '' +
      amount +
      '' +
      currencyCode +
      '' +
      billerId,
  ).toString();

export const generateHashCpc = ({
  username,
  amount,
  currencyCode,
  billerId,
}: hashProps): string =>
  MD5(
    username.toLowerCase() + '' + amount + '' + currencyCode + '' + billerId,
  ).toString();

export const isDeviceTablet = () => {
  const deviceScreenWidth = Dimensions.get('screen').width;
  const BREAKPOINT = 728; // Breakpoint as per figma

  return deviceScreenWidth > BREAKPOINT;
};
