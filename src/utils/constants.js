// @flow

import {Dimensions, Platform} from 'react-native';
import {
  getUniqueId,
  getSystemVersion,
  getDeviceId,
} from 'react-native-device-info';

// Get this variable from .env
import {IS_STATIC_DEVICE} from 'react-native-dotenv';

import type {ImageResizeMode, ThemeInterface} from './commonTypes';
import {numberToCurrencyFormatNoDecimals} from '@ducks/deposit/utils';

const {width, height}: Object = Dimensions.get('window');

export const SCREEN_WIDTH: number = Dimensions.get('screen').width;
export const BREAKPOINT: number = 728;
export const IS_DEVICE_TABLET: boolean = SCREEN_WIDTH > BREAKPOINT;
export const CENTER: string = 'center';
export const CONTAIN: ImageResizeMode = 'contain';
export const GROUP: string = 'group';
export const WIDTH: number = width;
export const HEIGHT: number = height;
export const GUIDELINE_BASE_WIDTH: number = 375;
export const GUIDELINE_BASE_HEIGHT: number = 667;

export const DARK_THEME: ThemeInterface = {
  circle: {
    dashboardBgColor: '#153961',
    overviewBgColor: '#153961',
  },
  mainTopMenuItems: {
    notification: require('@assets/icons/notification-bell.png'),
    color: '#ffffff',
  },
  tab: {
    activeTintColor: '#09BAD0',
    backgroundColor: '#010E1D',
    boxShadow: '0px -2px 32px #04172D',
    bottomActiveTintColor: '#ffffff',
    bottomInactiveTintColor: 'rgba(255, 255, 255, 0.3)',
  },
  sidebar: {
    headerBg: '#010E1D',
  },
  logoSrc: require('@assets/images/mb-logo-dark.png'),
  color: {
    PRIMARY_DISABLED: '#8DA0B2',
    PRIMARY_DEFAULT: '#00539F',
    PRIMARY_PRESSED: '#004585',
    SECONDARY_DISABLED: '#B4CDD0',
    SECONDARY_DEFAULT: '#09BAD0',
    SECONDARY_PRESSED: '#08A2B5',
    BACKGROUND_300: '#02152B',
    BACKGROUND_200: '#07203C',
    BACKGROUND_100: '#102C4B',
    TEXT_300_LOW: '#445870',
    TEXT_300_HIGH: '#FFFFFF',
    TEXT_200_MEDIUIM: '#51667C',
    TEXT_100_HIGH: '#A2BDDB',
    GREEN_STATUS: '#84D268',
    RED_STATUS: '#F65A4C',
    DISABLED_STATUS: '#45586D',
    ORANGE_STATUS: '#FF8A00',
    YELLOW_STATUS: '#F9DE7D',
    HIGH_EMPHASIS: '#A2BDDB',
    CANCEL_BUTTON: '#E9E9E9',
  },
  deviceIcon: '#FFFFFF',
  card: {
    textColor: '#ffffff',
    backgroundColor: '#102c4b',
    dividerBgColor: '#445870',
    availableBalance: '#A2BDDB',
  },
  dashboard: {
    tabIcon: require('@assets/icons/tabs/dashboard-dark.png'),
  },
  text: {
    textColor: '#ffffff',
    textEmphasis: '#445870',
  },
  textInput: {
    username: require('@assets/icons/username.png'),
    password: require('@assets/icons/password.png'),
    backgroundColor: {
      default: '#02152b',
      filled: '#02152B',
      disabled: '#051931',
      active: '#02152B',
      correct: '#02152B',
      error: '#02152B',
    },
    textColor: '#ffffff',
    placeholderTextColor: '#486485',
    border: {
      default: 'transparent',
      filled: 'transparent',
      disabled: 'transparent',
      active: '#09BAD0',
      correct: '#84D268',
      error: '#F65A4C',
    },
    helperTextColor: {
      default: '#A2BDDB',
      filled: '#A2BDDB',
      disabled: '#A2BDDB',
      active: '#A2BDDB',
      correct: '#A2BDDB',
      error: '#F65A4C',
    },
    iconColor: {
      search: '#FFFFFF',
      clear: '#FFFFFF',
    },
    iconInputStatusImagesTint: 'none',
    calendarActiveTint: 'none',
    searchImageTint: 'none',
    dividerColor: '#445870',
  },
  topAppBar: {
    backgroundColor: '#02152b',
    textColor: '#ffffff',
    barStyle: 'light-content',
    loading: '#030e1d',
    iconColor: '#ffffff',
  },
  layout: {
    src: require('@assets/images/main-bg-dark.png'),
    backgroundColor: '#051931',
  },
  passwordInput: {
    src: require('@assets/icons/visibility-dark.png'),
    mode: 'dark',
  },
  button: {
    textColor: {
      disabled: '#748291',
    },
    backgroundColor: {
      disabled: '#45586D',
    },
  },
  skeletonPlaceholder: {
    highlightColor: '#f5f5f5',
    backgroundColor: '#486788',
  },
  modal: {
    backgroundColor: '#07203C',
  },
  tabNavigator: {
    borderBottomColor: '#445870',
  },
  FilterIcon: require('@assets/icons/filter.png'),
  transaction: {
    background: '#051931',
    dividerColor: '#445870',
  },
  circularGraphBackground: '#02152B',
  defaultColor: '#09BAD0',
  calendar: {
    defaultDateColor: '#09BAD0',
    defaultDateColorWithOpacity: 'rgba(9, 186, 208, 0.5)',
    selectedDayTextColor: '#FFFFFF',
    todayTextColor: '#ffffff',
    iconActiveBgColor: '#09BAD0',
    iconInActiveBgColor: '#445870',
  },
  carousel: {
    indicatorColor: '#445870',
  },
  guidedTourTooltip: {
    background: '#18416E',
  },
  customizeDashboard: {
    customizeText: '#FFFFFF',
  },
  displayOptionsTooltip: {
    tooltip: '#18416E',
    divider: '#486485',
    widgetHeaderIcon: ' #2a69af',
  },
  filterTransaction: {
    divider: '#445870',
    calendarInputBorder: '#02152B',
    placeholderText: '#445870',
    numberOfFiltersCircle: '#163E69',
    cancelButton: '#FFFFFF',
  },
  reminder: {
    yellow: {
      backgroundColor: '#F2EBC3',
      borderColor: '#BDA726',
      textColor: '#7E6F1B',
    },
  },
  trasactionList: {
    background: '#163E69',
  },
  circleBackground: '#153961',
  sectionHeader: '#445870',
  otherActions: {
    containerBackground: '#102C4B',
    contentContainerBackground: '#214369',
    location: require('@assets/icons/location.png'),
    qr: require('@assets/icons/qr.png'),
    bank: require('@assets/icons/bank.png'),
    chevronUp: '#A2BDDB',
  },
  activityLog: {
    date: '#A2BDDB',
  },
};

export const LIGHT_THEME: ThemeInterface = {
  circle: {
    dashboardBgColor: '#F9F9F9',
    overviewBgColor: '#ffffff',
  },
  mainTopMenuItems: {
    notification: require('@assets/icons/notification-bell-light.png'),
    color: '#00539F',
  },
  tab: {
    activeTintColor: '#00539F',
    backgroundColor: '#ffffff',
    boxShadow: '0px -2px 32px #E0E0E0',
    bottomActiveTintColor: '#00539F',
    bottomInactiveTintColor: 'rgba(0, 83, 159, 0.3)',
  },
  sidebar: {
    headerBg: '#ffffff',
  },
  logoSrc: require('@assets/images/mb-logo-light.png'),
  color: {
    PRIMARY_DISABLED: '#8DA0B2',
    PRIMARY_DEFAULT: '#00539F',
    PRIMARY_PRESSED: '#004585',
    SECONDARY_DISABLED: '#B4CDD0',
    SECONDARY_DEFAULT: '#00539F',
    SECONDARY_PRESSED: '#2196F3',
    BACKGROUND_300: '#FFFFFF',
    BACKGROUND_200: '#F9F9F9',
    BACKGROUND_100: '#FFFFFF',
    TEXT_300_LOW: '#BABABA',
    TEXT_300_HIGH: '#212121',
    TEXT_200_MEDIUIM: '#808080',
    TEXT_100_HIGH: '#808080',
    GREEN_STATUS: '#27AE60',
    RED_STATUS: '#FF0000',
    DISABLED_STATUS: '#E0E8F0',
    ORANGE_STATUS: '#FF8A00',
    YELLOW_STATUS: '#FFD21F',
    HIGH_EMPHASIS: '#A2BDDB',
    CANCEL_BUTTON: '#808080',
  },
  card: {
    textColor: '#212121',
    backgroundColor: '#ffffff',
    dividerBgColor: '#E9E9E9',
    availableBalance: '#BABABA',
  },
  dashboard: {
    tabIcon: require('@assets/icons/tabs/dashboard-light.png'),
  },
  text: {
    textColor: '#212121',
    textEmphasis: '#BABABA',
  },
  deviceIcon: '#BABABA',
  textInput: {
    username: require('@assets/icons/username-light.png'),
    password: require('@assets/icons/password-light.png'),
    backgroundColor: {
      default: '#ffffff',
      filled: '#ffffff',
      disabled: '#E4E4E7',
      active: '#ffffff',
      correct: '#ffffff',
      error: '#ffffff',
    },
    textColor: '#212121',
    placeholderTextColor: '#9A9A9A',
    border: {
      default: '#E9E9E9',
      filled: '#E9E9E9',
      disabled: 'transparent',
      active: '#00539F',
      correct: '#27AE60',
      error: '#FF0000',
    },
    helperTextColor: {
      default: '#BABABA',
      filled: '#BABABA',
      disabled: '#BABABA',
      active: '#BABABA',
      correct: '#BABABA',
      error: '#FF0000',
    },
    iconColor: {
      search: '#000000',
      clear: '#808080',
    },
    iconInputStatusImagesTint: '#BABABA',
    calendarActiveTint: '#00539F',
    searchImageTint: '#000000',
    dividerColor: '#BABABA',
  },
  topAppBar: {
    textColor: '#212121',
    backgroundColor: '#ffffff',
    barStyle: 'dark-content',
    loading: '#999999',
    iconColor: '#02152B',
  },
  layout: {
    src: require('@assets/images/main-bg-light.png'),
    backgroundColor: '#F9F9F9',
  },
  passwordInput: {
    src: require('@assets/icons/visibility-light2.png'),
    mode: 'light',
  },
  button: {
    textColor: {
      disabled: '#B9CDE0',
    },
    backgroundColor: {
      disabled: '#E0E8F0',
    },
  },
  skeletonPlaceholder: {
    highlightColor: '#f5f5f5',
    backgroundColor: '#486788',
  },
  modal: {
    backgroundColor: '#FFFFFF',
  },
  tabNavigator: {
    borderBottomColor: '#E9E9E9',
  },
  FilterIcon: require('@assets/icons/filter-light.png'),
  transaction: {
    background: '#E4E4E7',
    dividerColor: '#E9E9E9',
  },
  circularGraphBackground: '#BABABA',

  defaultColor: '#00539F',
  calendar: {
    defaultDateColor: '#00539F',
    defaultDateColorWithOpacity: 'rgba(0, 83, 159, 0.5)',
    selectedDayTextColor: '#FFFFFF',
    todayTextColor: '#212121',
    iconActiveBgColor: '#00539F',
    iconInActiveBgColor: '#BABABA',
  },
  carousel: {
    indicatorColor: '#9A9A9A',
  },
  guidedTourTooltip: {
    background: '#F8F8F8',
  },
  customizeDashboard: {
    customizeText: '#00539F',
  },
  displayOptionsTooltip: {
    tooltip: '#F8F8F8',
    divider: '#E9E9E9',
    widgetHeaderIcon: '#F8F8F8',
  },
  filterTransaction: {
    divider: '#E9E9E9',
    calendarInputBorder: '#E9E9E9',
    placeholderText: '#9A9A9A',
    numberOfFiltersCircle: '#E4E4E7',
    cancelButton: '#808080',
  },
  reminder: {
    yellow: {
      backgroundColor: '#F2EBC3',
      borderColor: '#BDA726',
      textColor: '#7E6F1B',
    },
  },
  trasactionList: {
    background: '#E4E4E7',
  },
  circleBackground: '#F9F9F9',
  sectionHeader: '#00539F',
  otherActions: {
    containerBackground: '#f6f8f9',
    contentContainerBackground: '#ffffff',
    location: require('@assets/icons/location-light.png'),
    qr: require('@assets/icons/qr-light.png'),
    bank: require('@assets/icons/bank-light.png'),
    chevronUp: '#BABABA',
  },
  activityLog: {
    date: '#BABABA',
  },
};

export const NAVIGATION_DARK_THEME = {
  dark: true,
  colors: {
    primary: DARK_THEME.color.PRIMARY_DEFAULT,
    background: DARK_THEME.color.BACKGROUND_300,
    card: DARK_THEME.card.backgroundColor,
    text: DARK_THEME.color.TEXT_300_HIGH,
    border: 'transparent',
    notification: DARK_THEME.color.RED_STATUS,
  },
};

export const NAVIGATION_LIGHT_THEME = {
  dark: false,
  colors: {
    primary: LIGHT_THEME.color.PRIMARY_DEFAULT,
    background: LIGHT_THEME.color.BACKGROUND_300,
    card: LIGHT_THEME.card.backgroundColor,
    text: LIGHT_THEME.color.TEXT_300_HIGH,
    border: 'transparent',
    notification: LIGHT_THEME.color.RED_STATUS,
  },
};

export const X_DEVICE = 'Chrome 80.0.3987.116';
export const X_DEVICE_ID = '9bd650269509628a38772dba5d5dabd9';
export const X_OS = 'Mac OS 10.15.4';

export const headers: Object = {
  'X-DEVICE': IS_STATIC_DEVICE ? X_DEVICE : getDeviceId(),
  'X-DEVICE-ID': IS_STATIC_DEVICE ? X_DEVICE_ID : getUniqueId(),
  'X-OS': IS_STATIC_DEVICE ? X_OS : `${Platform.OS} ${getSystemVersion()}`,
};

export const touchIdDevices = [
  'iPhone 6',
  'iPhone 6 Plus',
  'iPhone SE (2016)',
  'iPhone 6s',
  'iPhone 6s Plus',
  'iPhone 7',
  'iPhone 7 Plus',
  'iPhone 8',
  'iPhone 8 Plus',
  'iPhone SE (2020)',
  'iPad Mini',
  'iPad Mini 2',
  'iPad Mini 3',
  'iPad Mini 4',
  'iPad (2017)',
  'iPad (2018)',
  'iPad (2019)',
  'iPad (2020)',
  'iPad Air 2',
  'iPad Pro 12.9in (2015)',
  'iPad Pro 9.7in (2016)',
  'iPad Mini (2019)',
  'iPad Air 2',
  'iPad Air (2019)',
  'iPad Air (2020) ',
  'iPad Pro 12.9in (2017)',
  'iPad Pro 10.5in (2017)',
];

export const INSTALLMENT_SHORTCUT_CODES = [
  'CASH2GO INSTL',
  'BALANCE CONVERSION',
  'BALANCE TRANSFER',
];

export const CARD_INSTALLMENT_MAPPER = [
  {
    id: 1,
    icon: require('@assets/icons/cash2go-icon.png'),
    title: 'Cash2Go',
    description: `Convert your unused${'\n'}credit limit to cash.`,
    navigation: '',
    code: 'CASH2GO INSTL',
  },
  {
    id: 2,
    icon: require('@assets/icons/balance-conversion-icon.png'),
    title: 'Convert Balance',
    description: `Convert your purchases${'\n'}into monthly installments.`,
    navigation: '',
    code: 'BALANCE CONVERSION',
  },
  {
    id: 3,
    icon: require('@assets/icons/transfer-icon.png'),
    title: 'Transfer Balance',
    description: `Transfer balances from${'\n'}your other cards.`,
    navigation: '',
    code: 'BALANCE TRANSFER',
  },
];
export const EXCLUDED_SCREENS = [
  'Login Settings',
  'Installments',
  'Change Passcode',
];

export const SHORTCUT_ICONS = {
  PAY_BILLS: require('@assets/icons/shortcuts/pay-bills.png'),
  SEND_MONEY: require('@assets/icons/shortcuts/send-money.png'),
  ONLINE_TD: require('@assets/icons/shortcuts/open-time-deposit.png'),
  ACTIVATE_CARDS: require('@assets/icons/shortcuts/activate-cards.png'),
  VIEW_REWARDS: require('@assets/icons/view-rewards.png'),
  REPORT_LOST_CARD: require('@assets/icons/shortcuts/report-lost-card.png'),
  PROMISE_TO_PAY: require('@assets/icons/shortcuts/promise-to-pay.png'),
  CASH2GO: require('@assets/icons/shortcuts/cash2go.png'),
  BALANCE_CONVERSION: require('@assets/icons/shortcuts/balance-conversion-alt.png'),
  BALANCE_TRANSFER: require('@assets/icons/shortcuts/balance-transfer.png'),
  VIEW_STATEMENT: require('@assets/icons/shortcuts/view-soa.png'),
};

export const SHORTCUT_ICONS_CODE = {
  'ADD ACCT': null,
  'DOWNLOAD STMT': null,
  SIGNUP: null,
  ACTIVATE_CARDS: SHORTCUT_ICONS.ACTIVATE_CARDS,
  'CASH2GO INSTL': SHORTCUT_ICONS.CASH2GO,
  'BALANCE CONVERSION': SHORTCUT_ICONS.BALANCE_CONVERSION,
  'BALANCE TRANSFER': SHORTCUT_ICONS.BALANCE_TRANSFER,
  'PAY BILLS': SHORTCUT_ICONS.PAY_BILLS,
  'SEND MONEY\r\n(recipient)': SHORTCUT_ICONS.SEND_MONEY,
  'SEND MONEY (source)': SHORTCUT_ICONS.SEND_MONEY,
  'VIEW STMT': SHORTCUT_ICONS.VIEW_STATEMENT,
  REPORT_LOST_CARD: SHORTCUT_ICONS.REPORT_LOST_CARD,
  LINK_AN_ACCOUNT: null,
  INTRATE: null,
  BALANCE_INQUIRY: null,
  STMT_INQUIRY: null,
  SEND_MONEY: SHORTCUT_ICONS.SEND_MONEY,
  MOVE_MONEY: null,
  PAY_BILLS: SHORTCUT_ICONS.PAY_BILLS,
  INSTAPAY: null,
  PESONET: null,
  CARDLESS_WITHDRAWAL: null,
  EADA: null,
  PAY_TAX: null,
  ONLINE_TD: SHORTCUT_ICONS.ONLINE_TD,
  READ_QR_CODE: null,
  GENERATE_QR_CODE: null,
  LOAD_PREPAID: null,
  ORDER_CHECKBOOK: null,
  VIEW_REWARDS: SHORTCUT_ICONS.VIEW_REWARDS,
  PROMISE_TO_PAY: SHORTCUT_ICONS.PROMISE_TO_PAY,
};

const SendMoney = 'Send Money';
export const SHORTCUT_ICONS_NAME = {
  'ADD ACCT': 'Add Account',
  'DOWNLOAD STMT': 'Download Statement',
  SIGNUP: 'Signup',
  ACTIVATE_CARDS: 'Activate Cards',
  'CASH2GO INSTL': 'Convert to Cash',
  'BALANCE CONVERSION': 'Convert Balance',
  'BALANCE TRANSFER': 'Transfer Balance',
  'PAY BILLS': 'Pay Bills',
  'SEND MONEY\r\n(recipient)': SendMoney,
  'SEND MONEY (source)': SendMoney,
  'VIEW STMT': 'View Statement',
  REPORT_LOST_CARD: 'Report Lost Card',
  LINK_AN_ACCOUNT: 'Link an Account',
  INTRATE: 'Interest Rate',
  BALANCE_INQUIRY: 'Balance Inquiry',
  STMT_INQUIRY: 'Statement Inquiry',
  SEND_MONEY: SendMoney,
  MOVE_MONEY: 'Move Money',
  PAY_BILLS: 'Pay Bills',
  INSTAPAY: 'Instapay',
  PESONET: 'Pesonet',
  CARDLESS_WITHDRAWAL: 'Cardless Withdrawal',
  EADA: 'Real Time Debit',
  PAY_TAX: 'Pay Tax',
  ONLINE_TD: 'Open Time Deposit',
  READ_QR_CODE: 'Send via QR',
  GENERATE_QR_CODE: 'Generate QR',
  LOAD_PREPAID: 'Load Prepaid',
  ORDER_CHECKBOOK: 'Order Checkbook',
  VIEW_REWARDS: 'View Rewards',
  PROMISE_TO_PAY: 'Promise to Pay',
};

export const ACCOUNT_TYPES_ENUM = {
  DEPOSIT: 0,
  MB_PREPAID: 1,
  CREDIT: 2,
  YAZZ: 3,
};

export const DATE_FORMAT = 'MM/DD/YYYY';
export const CARD_STATUS = {
  ACTIVE: 'ACTV',
};

export const CREDIT_SHORTCUTS_DEFAULT = {
  data: {
    shortcuts: [
      {
        name: 'Pay Bills',
        code: 'PAY_BILLS',
        credit: true,
        debit: false,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 1009,
      },
      {
        name: 'Activate Cards',
        code: 'ACTIVATE_CARDS',
        credit: true,
        debit: false,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 1008,
      },
      {
        name: 'View Rewards',
        code: 'VIEW_REWARDS',
        credit: true,
        debit: false,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 1007,
      },
      {
        name: 'Report lost card',
        code: 'REPORT_LOST_CARD',
        credit: true,
        debit: false,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 1006,
      },
      {
        name: 'Promise to Pay',
        code: 'PROMISE_TO_PAY',
        credit: true,
        debit: false,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 1005,
      },
    ],
  },
};
export const DEPOSIT_PREPAID_SHORTCUTS_DEFAULT = {
  depositAccount: {
    shortcuts: [
      {
        name: 'Pay Bills',
        code: 'PAY_BILLS',
        credit: true,
        debit: true,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 1009,
      },
      {
        name: 'Send Money',
        code: 'SEND MONEY\r\n(recipient)',
        credit: true,
        debit: true,
        enabled: true,
        forShortcut: true,
        icon: 'icon.png',
        id: 607,
      },
    ],
  },
};
export const INVALID_USER_CREDENTIALS = 'Invalid user credentials';
export const INVALID_USER_CREDENTIALS_MSG =
  'Invalid credentials. Please try again.';
export const ACCOUNT_LOCKED = 'Your account is locked.';

export const YAZZ_BINLIST = ['409461'];

export const ASYNC_STORAGE_KEYS = {
  SHOW_INFO_MODAL: {
    BT: 'BT_DO_NOT_SHOW_INFO_MODAL',
    BC: 'BC_DO_NOT_SHOW_INFO_MODAL',
    C2GO: 'C2GO_DO_NOT_SHOW_INFO_MODAL',
    '': '',
  },
};

export const INSTALLLMENT_INFO_FALLBACK = {
  BC: {
    Header: 'Learn more about \nBalance Conversion',
    Details:
      'This facility lets you convert your straight credit card purchases into light monthly installments for up to 60 months.',
    Link: 'https://www.metrobankcard.com/cardsservices/balance-conversion', // NOSONAR
  },
  BT: {
    Header: 'Learn more about \nBalance Transfer',
    Details:
      "This facility lets you transfer your other credit card's balances to your Metrobank credit card at a low rate for up to 60 months.\n \nIf you have any further questions about Balance Transfer you can ask us via email.", // NOSONAR
    Link: 'https://www.metrobankcard.com/cardsservices/balance-conversion', // NOSONAR
  },
  C2GO: {
    Header: 'Learn more about Cash2Go',
    Details:
      "This facility lets you convert your credit card's unused credit limit to cash for up to 60 months.",
    Link: 'https://www.metrobankcard.com/cardsservices/cash-2-go', // NOSONAR
  },
};

export const CASH2GO_NON_METRO_MAX_CAP_AMOUNT = 100000;
export const CASH2GO_EXCEEDED_CAP_WARNING = `Cash2Go applications of more than PHP ${numberToCurrencyFormatNoDecimals(
  CASH2GO_NON_METRO_MAX_CAP_AMOUNT,
)} are only allowed for enrolled accounts.`;

export const CREDIT_FULL_PAGE_ERROR = {
  header: 'We are unable to process your request.',
  body:
    'Refer to Collections\n(Call (02) 88-700-970 or email collections@metrobankcard.com',
};

// Used for recover access feature
export const NAVIGATION_RECOVERACCESS = 'RecoverAccess';
export const NAVIGATION_ENTERPASSCODE = 'EnterPasscode';
export const NAVIGATION_ENTEREMAIL = 'EnterEmail';
export const NAVIGATION_ENTEROTP = 'EnterOTP';
export const NAVIGATION_ENTERPASSWORD = 'EnterPassword';
export const NAVIGATION_RECOVERED = 'Recovered';
export const NAVIGATION_PASSWORDCHANGED = 'PasswordChanged';
export const NAVIGATION_ENTERUSERNAMEEMAIL = 'EnterUsernameEmail';
export const NAVIGATION_MAXIMUMINVALID = 'MaximumInvalid';
export const NAVIGATION_PASSCODECHANGED = 'PasscodeChanged';
export const LOGIN = 'Login';
export const VERIFICATION = 'Verification';
export const CREATE_NEW_PASSWORD = 'Create new password';
export const CHANGE_PASSCODE = 'Change Passcode';
export const NAVIGATION_CHANGEPASSCODE = 'ChangePasscode';

export const MB_PREPAID_PRODUCT_TYPE = '769';
