/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import {useState, useEffect} from 'react';

export const useCntDown = (): Array<any> => {
  const [expire, setExpire] = useState(0);
  const [timer, setTimer] = useState(0);
  const [basis, setBasis] = useState(0);
  const [intervalId, setIntervalId] = useState();

  useEffect(() => {
    if (expire > 0) {
      const dateNow = Date.now();
      setBasis(new Date(dateNow + 1000 * expire).valueOf());
      setTimer(dateNow.valueOf());
      setIntervalId(
        setInterval(() => {
          setTimer(new Date().valueOf());
        }, 200),
      );
    } else {
      clearInterval(intervalId);
    }
    return () => {
      clearInterval(intervalId);
    };
  }, [expire]);

  useEffect(() => {
    if (Math.floor((basis - timer) / 1000) <= 0) {
      setExpire(0);
      clearInterval(intervalId);
    }
    if (basis - timer <= 0) {
      setExpire(0);
      clearInterval(intervalId);
    }
  }, [timer]);

  return [Math.floor((basis - timer) / 1000), setExpire, intervalId];
};
