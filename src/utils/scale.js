// @flow

import {
  WIDTH,
  HEIGHT,
  GUIDELINE_BASE_WIDTH,
  GUIDELINE_BASE_HEIGHT,
} from '@utils/constants';

const scale = (size: number): number => (WIDTH / GUIDELINE_BASE_WIDTH) * size;
const verticalScale = (size: number): number =>
  (HEIGHT / GUIDELINE_BASE_HEIGHT) * size;
const moderateScale = (size: number, factor: number = 0.5): number =>
  size + (scale(size) - size) * factor;

export {scale, verticalScale, moderateScale};
