// @flow

import React from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {DARK_THEME} from '@utils/constants';
import {ThemeProvider} from 'styled-components/native';

type MockProviderType = {
  children?: React$Node,
  store?: Object,
};

const MockProvider = (props: MockProviderType): React$Node => {
  const {store, children} = props;
  const mockStore = configureStore([]);
  const mocked = mockStore(store);
  return <Provider store={mocked}>{children}</Provider>;
};

export const renderComponentWithTheme = (
  children?: React$Node,
  theme: Object = DARK_THEME,
): React$Node => {
  return <ThemeProvider theme={DARK_THEME}>{children}</ThemeProvider>;
};

export default MockProvider;
