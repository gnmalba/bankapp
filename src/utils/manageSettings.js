import moment from 'moment';
import type {THEME} from '@ducks/config/types';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const UI = {
  MARGINS: {
    separator_8: 8,
    separator_40: 40,
  },
  PADDINGS: {
    layout: 32,
  },
};

export const APPEARANCE_SETTINGS = {
  DARK_VALUE: 'dark',
  LIGHT_VALUE: 'light',
};

export const DEVICE_SETTINGS_CONSTANTS = {
  MODE: 'raw',
  SAVE_APPEARANCE_SUCCESS_MESSAGE:
    'Appearance setting has been successfully saved',
  ASYNCSTORAGE_THEME_KEY: 'localTheme',
};

export const DEVICE_ICONS = {
  mobile: require('@assets/icons/phone.png'),
  pc: require('@assets/icons/desktop.png'),
};

export const DATE_FORMAT: string = 'MMMM DD, YYYY hh:mm A';

export const formatDate = (date: string): string =>
  moment(date).format(DATE_FORMAT);

export const nonMobileDevices = ['WINDOWS', 'MAC', 'CHROME'];

export const isMobile = (brand: string): boolean => {
  // Edge case
  if (brand.startsWith('OS')) {
    return false;
  }

  return !nonMobileDevices.some((dev) => {
    return brand.toUpperCase().includes(dev);
  });
};

export const buildUnlinkSuccessMessage = (device: string) => {
  return 'You have successfully unlinked ' + device;
};

export const ERRORS = {
  CODES: {
    MAX_PASSCODE_ATTEMPTS: 'invalid_mpin_exceeded',
  },
  MESSAGES: {
    MAX_PASSCODE_ATTEMPTS:
      'You have reached the maximum number of attempts. Please try again after(x) minutes.',
    EMPTY_PASSWORD: 'Please enter your current password.',
    MINIMUM_CHARACTERS: 'Minimum of 8 characters.',
    HAS_NOT_MET_ONE_CRITERION:
      'The password you entered does not meet the required conditions.',
  },
};

export const setThemeOnAsyncStorage = async (theme: THEME) => {
  try {
    await AsyncStorage.setItem(
      DEVICE_SETTINGS_CONSTANTS.ASYNCSTORAGE_THEME_KEY,
      theme,
    );
  } catch (error) {
    console.log(error);
  }
};

export const getThemeOnAsyncStorage = async (): Promise => {
  try {
    const value = await AsyncStorage.getItem(
      DEVICE_SETTINGS_CONSTANTS.ASYNCSTORAGE_THEME_KEY,
    );
    if (value) {
      return value;
    }
    return APPEARANCE_SETTINGS.DARK_VALUE;
  } catch (error) {
    console.log(error);
  }
};
