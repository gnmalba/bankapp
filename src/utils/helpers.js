/**
 * @format
 * @flow
 */
import Text from '@components/Text';
import {
  CardListItemName,
  CardListItemNumber,
} from '@screens/private/SelectCreditCardWithSearchScreen/Styled';
import React from 'react';
import {Platform} from 'react-native';
import {View} from 'react-native';
import {testProps} from './componentCustomProps';

// should refactor this "function" into a reusable component
export const toText = (
  value: string | number,
  testID: string,
  mode?:
    | 'h1'
    | 'h2'
    | 'h3'
    | 'body1'
    | 'body2'
    | 'subtitle'
    | 'buttontext'
    | 'buttonlink'
    | 'link'
    | 'caption'
    | 'overline1'
    | 'overline2'
    | 'inputerror'
    | 'custom' = 'body1',
): any => (
  <Text mode={mode} {...testProps(testID)}>
    {value}
  </Text>
);

// should refactor this "function" into a reusable component
export const toCard = (
  name: string,
  number: string,
  nameMode: string = 'body2',
  numberMode: string = 'overline2',
  testID: string = '',
): any => {
  if (Platform.OS === 'android') {
    return (
      <>
        <CardListItemName mode={nameMode} {...testProps(testID + 'Name')}>
          {name}
        </CardListItemName>

        <View>
          <CardListItemNumber
            mode={numberMode}
            {...testProps(testID + 'Number')}>
            {number}
          </CardListItemNumber>
        </View>
      </>
    );
  } else {
    return (
      <View>
        <CardListItemName mode={nameMode} {...testProps(testID + 'Name')}>
          {name}
        </CardListItemName>

        <CardListItemNumber mode={numberMode} {...testProps(testID + 'Number')}>
          {number}
        </CardListItemNumber>
      </View>
    );
  }
};

export const getApiResponseError = (apiResponseError: any): Object => {
  const errorArr = apiResponseError.response?.data?.data?.errors;

  if (errorArr && errorArr.length > 0) {
    return errorArr[0];
  }
  return {code: 'ERROR_CODE', message: 'There was an error.'};
};
