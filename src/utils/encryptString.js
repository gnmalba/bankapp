// @flow
import {JSEncrypt} from 'jsencrypt';

const encryptString = (encryptionPublicKey: string): Object => {
  const encrypt = new JSEncrypt();
  encrypt.setPublicKey(encryptionPublicKey);
  return encrypt;
};

export default encryptString;
