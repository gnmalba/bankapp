/**
 * @flow
 */

import Text from '@components/Text';
import type {ComponentType} from 'react';
import {ScrollView} from 'react-native';
// import styled from 'styled-components';
import styled from 'styled-components/native';
import {verticalScale} from './scale';

export const EndButtonWrapper: ComponentType<any> = styled.View`
  margin-top: auto;
`;

export const HeaderContainer: ComponentType<any> = styled.View`
  margin-bottom: ${(props) => verticalScale(props.marginBottom || 0)}px;
  margin-top: ${(props) => verticalScale(props.marginTop || 0)}px;
  margin-left: ${(props) => verticalScale(props.marginLeft || 0)}px;
  margin-right: ${(props) => verticalScale(props.marginRight || 0)}px;
`;

export const Subtitle: ComponentType<any> = styled(Text)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  text-align: ${(props) => (props.textAlign ? props.textAlign : 'left')};
`;

export const SemiboldText: ComponentType<any> = styled(Text)`
  font-family: 'MyriadPro-Semibold';
  font-weight: 600;
`;

export const EndButtonWrapperAbsolute: ComponentType<any> = styled.View`
  flex: 1;
  justify-content: flex-end;
`;

export const FlexContainer: ComponentType<any> = styled.View`
  flex: 1;
`;
export const Flex2Container: ComponentType<any> = styled.View`
  flex: 2;
`;

export const FlexGrowScrollView: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    contentContainerStyle: {
      flexGrow: 1,
    },
  }),
)``;

export const FlexGrowScrollViewChildWrapper: ComponentType<any> = styled.View`
  flex: 1;
  justify-content: space-evenly;
`;
