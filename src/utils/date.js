/**
 * @flow
 */

import moment from 'moment';

const getDateUTC = (date: string): string => moment.utc(date).toDate();

export const formatDateTime = (date: string): string =>
  moment.utc(getDateUTC(date)).local().format('MMM-DD-YYYY HH:mm');

export const formatDate = (date: string, customFormat?: string): string =>
  moment(getDateUTC(date))
    .local()
    .format(customFormat ? customFormat : 'MMM-DD-YYYY');
