const reverse = (str) => {
  return str.split('').reverse().join('');
};

const maxDigitsLength = 12;
export const numberFormatNoLimit = (number = 0) => {
  const isExceeded = number?.toString()?.length > maxDigitsLength;
  const wholeNumber = number?.toString()?.split('.')[0];
  const decimals = number?.toString()?.split('.')[1];

  let returnValue = '';

  if (isExceeded) {
    const reverseSplitNum = reverse(wholeNumber)
      .toString()
      .split(/(.{3})/)
      .filter((x) => x.length > 0);
    const splitNum = reverseSplitNum
      .map((item) => {
        return reverse(item);
      })
      .reverse()
      .join(',');

    returnValue = splitNum.toString();
    if (decimals) {
      returnValue = splitNum.toString() + '.' + decimals;
    } else {
      returnValue = splitNum.toString();
    }
  } else {
    returnValue = numberFormatClean(number);
  }
  return returnValue;
};

export const addDecimal = (number) => {
  return number.replace(/[^0-9.]/g, '');
};

export const numberFormatClean = (numberStr) => {
  let str = numberStr + '';
  str = str.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return addDecimals(str);
};

export const removeCommas = (number) => {
  return number.split(',').join('');
};

const addDecimals = (str) => {
  if (str.includes('.')) {
    const strSplit = str.split('.');
    const decimals = strSplit[1];
    if (decimals.length === 1) {
      return str + '0';
    }
    return str;
  } else {
    return str + '.00';
  }
};
