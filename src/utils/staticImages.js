const staticImages = {
  icons: {
    dashboard: {
      sidebar: require('@assets/icons/sidebar/dashboard-icon.png'),
      tabs: {
        light: require('@assets/icons/dashboard-icon.png'),
        dark: require('@assets/icons/dashboard-icon.png'),
      },
    },
    accounts: {
      sidebar: require('@assets/icons/sidebar/my-accounts-icon.png'),
    },
    loans: {
      sidebar: require('@assets/icons/sidebar/loans-icon.png'),
    },
    uitf: {
      sidebar: require('@assets/icons/sidebar/uitf-icon.png'),
    },
    wealthManager: {
      sidebar: require('@assets/icons/sidebar/wealth-manager-icon.png'),
    },
    enrolledContacts: {
      sidebar: require('@assets/icons/sidebar/enrolled-contacts-icon.png'),
    },
    billers: {
      sidebar: require('@assets/icons/sidebar/billers-icon.png'),
    },
    schedules: {
      sidebar: require('@assets/icons/sidebar/schedules-icon.png'),
    },
    allRequest: {
      sidebar: require('@assets/icons/sidebar/all-requests-icon.png'),
    },
    settings: {
      sidebar: require('@assets/icons/sidebar/customize-icon.png'),
    },
    _logout: {
      sidebar: require('@assets/icons/sidebar/logout-icon.png'),
    },
    contacts: {
      sidebar: require('@assets/icons/enrolled-contacts-icon.png'),
    },
    logout: require('@assets/icons/logout-icon.png'),
  },
  images: {
    logo: require('@assets/images/mb-logo-dark.png'),
  },
};

export default staticImages;
