import React from 'react';
import {useCntDown} from '../useCntDown';
import {View, Text, Button} from 'react-native';
import {render, fireEvent} from '@testing-library/react-native';

jest.useFakeTimers();

jest.mock('react-native', () => {
  return jest.requireActual('react-native');
});

const TestComponent = ({display}) => {
  const [time, setExpire] = useCntDown(display);
  return (
    <View>
      <Text>{time}</Text>
      <Button
        testID="btnTestId"
        onPress={() => {
          var t = new Date();
          t.setSeconds(t.getSeconds() + 10);
          setExpire(t.valueOf());
        }}
        title={'Set'}
      />
    </View>
  );
};

describe('useCntDown', () => {
  test.each([0, 1])('should work properly', (num) => {
    const all = render(<TestComponent display={num} />);
    const btnEl = all.getByTestId('btnTestId');
    fireEvent(btnEl, 'onPress');
  });
});
