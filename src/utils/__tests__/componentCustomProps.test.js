import {generateId, testProps} from '../componentCustomProps';

jest.unmock('@utils/componentCustomProps');

describe('componentCustomProps', () => {
  test('componentCustomProps', () => {
    expect(testProps('testId')).toEqual({
      testID: 'testId',
      accessibilityLabel: 'testId',
    });
    expect(generateId('')).toEqual({});
    expect(generateId('test')).toEqual({
      testID: 'id_test',
      accessibilityLabel: 'id_test',
    });
  });
});
