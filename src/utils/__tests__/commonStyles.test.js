import {
  EndButtonWrapper,
  HeaderContainer,
  Subtitle,
  FlexGrowScrollView,
} from '../commonStyles';
import React from 'react';
import {render} from '@testing-library/react-native';
import {testProps} from '../componentCustomProps';

describe('commonStyles', () => {
  test('EndButtonWrapper', () => {
    const all = render(<EndButtonWrapper />);
    expect(all.toJSON()).toMatchSnapshot();
  });

  test('HeaderContainer', () => {
    const all = render(<HeaderContainer />);
    expect(all.toJSON()).toMatchSnapshot();
  });

  test('Subtitle', () => {
    const all = render(<Subtitle {...testProps('testId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });

  test('FlexGrowScrollView', () => {
    const all = render(<FlexGrowScrollView />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
