import {
  accountsFormat,
  accountsFormat16,
  maskingFormat,
  numberFormat,
  countDecimals,
  accountNumberInputMask,
} from '../accountNumber';

const getLast4Characters = (characters: string): string =>
  characters.substring(characters.length - 4);

const _9_digits = '123456789',
  _13_digits = '1234567898954',
  _14_digits = '12345678989542',
  _15_digits = '123456789895456',
  _16_digits = '1234567898954863',
  _20_digits = '12345678989548639762';

describe('Unit test for account number util', () => {
  it('Should return formatted account number if parameter is undefined', () => {
    expect(accountsFormat(undefined)).toBe('*** *** *** 0000');
  });

  it('Should return formatted account number with unmask account number', () => {
    expect(accountsFormat('1234567898954')).toBe('*** *** *** 8954');
  });

  it('Should return formatted account number with depeding on character length', () => {
    expect(accountsFormat16(undefined)).toBe('**** **** **** 0000');

    expect(accountsFormat16(_13_digits)).toBe(
      `*** *** *** ${getLast4Characters(_13_digits)}`,
    );

    expect(accountsFormat16(_14_digits)).toBe(
      `**********${getLast4Characters(_14_digits)}`,
    );

    expect(accountsFormat16(_15_digits)).toBe(
      `***********${getLast4Characters(_15_digits)}`,
    );

    expect(accountsFormat16(_16_digits)).toBe(
      `**** **** **** ${getLast4Characters(_16_digits)}`,
    );
  });

  it('Should add space for mb prepaid, yazz, deposit, and credit accounts', () => {
    expect(maskingFormat(undefined)).toBe(' ');
    expect(maskingFormat(_13_digits)).toBe('123 456 789 8954');
    expect(maskingFormat(_14_digits)).toBe('123 456 789 8 9542');
    expect(maskingFormat(_15_digits)).toBe('123 456 789 89 5456');
    expect(maskingFormat(_16_digits)).toBe('123 456 789 895 4863');
  });

  it('Should add space and mask for mb prepaid, yazz, deposit, and credit accounts', () => {
    expect(maskingFormat(_13_digits, true)).toBe('*** *** ***  8954');
    expect(maskingFormat(_14_digits, true)).toBe('**** **** **95 42');
  });

  it('Should add spaces check if its account details', () => {
    expect(maskingFormat(_13_digits, false, true)).toBe('1234 5678 9895 4');
    expect(maskingFormat(_15_digits, false, true)).toBe('1234 5678 9895 456');
    expect(maskingFormat(_16_digits, false, true)).toBe('1234 5678 9895 4863');
  });

  it('Should return a string if first and second parameters are undefined', () => {
    expect(numberFormat(undefined, undefined)).toBe('0.00');
  });

  it('Should return a string with 2 decimal', () => {
    expect(numberFormat(20000)).toBe('20,000.00');
    expect(numberFormat(20000.21)).toBe('20,000.21');
  });

  it('Should add 0 if 1 decimal place detected', () => {
    expect(numberFormat(20000.1)).toBe('20,000.10');
    expect(numberFormat(20000.1, 1)).toBe('20,000.10');
  });

  it('Should count decimal with the given string', () => {
    expect(countDecimals(undefined)).toBe(0);
    expect(countDecimals('200')).toBe(0);
    expect(countDecimals('200.1')).toBe(1);
    expect(countDecimals('200,1')).toBe(0);
  });

  it('Should return empty string if param 1 is undefined', () => {
    expect(accountNumberInputMask(undefined)).toBe('');
  });

  it('Should return formatted account number', () => {
    expect(accountNumberInputMask(_13_digits)).toBe('123 456 789  8954');
    expect(accountNumberInputMask(_14_digits)).toBe('12345678989542');
    expect(accountNumberInputMask(_15_digits)).toBe('123456789895456');
    expect(accountNumberInputMask(_16_digits)).toBe('1234 5678 9895 4863');
    expect(accountNumberInputMask(_20_digits)).toBe('1234567898954863');
    expect(accountNumberInputMask(_9_digits)).toBe('123 456 789');
  });
});
