import {toText, toCard, getApiResponseError} from '../helpers';

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  delete RN.Platform;
  RN.Platform = {
    OS: 'ios',
  };
  return RN;
});

describe('helpers', () => {
  test('getApiResponseError', () => {
    expect(
      getApiResponseError({
        response: {data: {data: {errors: ['mpin required']}}},
      }),
    ).toEqual('mpin required');
  });

  test('getApiResponseError 2', () => {
    expect(
      getApiResponseError({
        response: {data: {data: {errors: []}}},
      }),
    ).toEqual({code: 'ERROR_CODE', message: 'There was an error.'});
  });

  test('toCard', () => {
    expect(toCard('test', 'test')).toBeDefined();
  });

  test('toText', () => {
    expect(toText('test', 'testID', 'h1')).toBeDefined();
    expect(toText('test', 'testID')).toBeDefined();
  });
});
