import {toText, toCard} from '../helpers';

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  delete RN.Platform;
  RN.Platform = {
    OS: 'android',
  };
  return RN;
});

describe('helpers', () => {
  test('toCard', () => {
    expect(toCard('test', 'test')).toBeDefined();
  });

  test('toText', () => {
    expect(toText('test', 'testID', 'h1')).toBeDefined();
  });
});
