import {
  NAVIGATION_ENROLLMENT_FORM,
  NAVIGATION_ENROLLMENT_OTP,
  NAVIGATION_ENROLLMENT_SUCCESS,
  NAVIGATION_ENROLLMENT_ERROR,
} from '../navigationRoutes';

describe('navigationRoutes', () => {
  test.each([
    NAVIGATION_ENROLLMENT_FORM,
    NAVIGATION_ENROLLMENT_OTP,
    NAVIGATION_ENROLLMENT_SUCCESS,
    NAVIGATION_ENROLLMENT_ERROR,
  ])('navigationRoutes', (routes) => {
    expect(routes).toBeDefined();
  });
});
