import {
  getThemeOnAsyncStorage,
  setThemeOnAsyncStorage,
} from '../manageSettings';

jest.mock('@react-native-async-storage/async-storage', () => 'View');

describe('manageSettings', () => {
  test('getThemeOnAsyncStorage', async () => {
    await getThemeOnAsyncStorage();
  });

  test('setThemeOnAsyncStorage', async () => {
    await setThemeOnAsyncStorage();
  });
});
