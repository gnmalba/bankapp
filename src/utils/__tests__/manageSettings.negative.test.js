import {getThemeOnAsyncStorage, isMobile} from '../manageSettings';

jest.mock('@react-native-async-storage/async-storage', () => ({
  getItem: (key) => null,
}));

describe('manageSettings', () => {
  test('getThemeOnAsyncStorage', async () => {
    await getThemeOnAsyncStorage();
  });

  test('isMobile ', () => {
    const mobile = isMobile('OSAndroid');
    expect(mobile).toEqual(false);
  });
});
