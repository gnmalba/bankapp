import {scale, verticalScale, moderateScale} from '../scale';

describe('scale', () => {
  test('scale', () => {
    const scaled = scale(10);
    expect(scaled).toBeGreaterThan(10);
  });

  test('verticalScale', () => {
    const scaled = verticalScale(10);
    expect(scaled).toBeGreaterThan(10);
  });

  test('moderateScale', () => {
    const scaled = moderateScale(10);
    expect(scaled).toBeGreaterThan(10);
  });
});
