import {getLastFourDigits} from '../getLastFourDigits';

describe('getLastFourDigits', () => {
  test('getLastFourDigits', () => {
    expect(getLastFourDigits('12345678')).toEqual('5678');
  });
});
