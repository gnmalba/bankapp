import {getCreditPercentage} from '../credit';

describe('credit', () => {
  test('credit', () => {
    expect(getCreditPercentage(100, 50)).toEqual(50);
    expect(getCreditPercentage(100, 50, null)).toEqual(0);
  });
});
