import {
  formatDate,
  isMobile,
  buildUnlinkSuccessMessage,
  setThemeOnAsyncStorage,
  getThemeOnAsyncStorage,
} from '../manageSettings';

jest.mock('@react-native-async-storage/async-storage', () => ({
  getItem: (key) => key,
  setItem: (key) => {},
}));

describe('manageSettings', () => {
  test('getThemeOnAsyncStorage', async () => {
    await getThemeOnAsyncStorage();
  });

  test('setThemeOnAsyncStorage', async () => {
    await setThemeOnAsyncStorage();
  });

  test('buildUnlinkSuccessMessage', () => {
    const txt = buildUnlinkSuccessMessage('Android');
    expect(txt).toEqual('You have successfully unlinked Android');
  });

  test('isMobile ', () => {
    const mobile = isMobile('Android');
    expect(mobile).toEqual(true);
  });

  test('formatDate', () => {
    const formatted = formatDate('1/1/21');
    expect(formatted).toEqual('January 01, 2021 12:00 AM');
  });
});
