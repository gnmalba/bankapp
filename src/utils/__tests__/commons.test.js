import {
  sortByObjectKey,
  removeSpace,
  iterateEntries,
  getDeviceInfo,
  generateHash,
  generateHashCpc,
  isDeviceTablet,
} from '../commons';

describe('commons', () => {
  test('sortByObjectKey', () => {
    expect(sortByObjectKey({a: 'leeks', b: 'meeks'})).toBeDefined();
  });

  test('removeSpace', () => {
    expect(removeSpace(' ')).toEqual('');
  });

  test('iterateEntries', () => {
    expect(iterateEntries({}, () => {})).toEqual({});
  });

  test('getDeviceInfo', () => {
    expect(getDeviceInfo()).toEqual({
      brand: undefined,
      deviceId: undefined,
      isDeveloperMode: true,
      isRooted: true,
      model: 'ios undefined',
    });
  });

  test('generateHash', () => {
    expect(
      generateHash({
        accountId: '1111',
        username: 'test',
        amount: 1,
        currencyCode: 'PHP',
        billerId: '',
      }),
    ).toEqual('63bdd89af63bae2e844d6d13e623381d');
  });

  test('generateHashCpc', () => {
    expect(
      generateHashCpc({
        username: 'test',
        amount: 1,
        currencyCode: 'PHP',
        billerId: '',
      }),
    ).toEqual('54856cdaddc10f66cea9944e49f3d3ad');
  });

  test('isDeviceTablet', () => {
    expect(isDeviceTablet()).toEqual(true);
  });
});
