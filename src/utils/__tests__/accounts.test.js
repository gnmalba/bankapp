import {
  sortAliasAlphabetically,
  sortCreditCardAlphabetically,
  getCardNumber,
  getEnabledShortcutsByCode,
} from '../accounts';

describe('accounts', () => {
  test('sortAliasAlphabetically ', () => {
    expect(
      sortAliasAlphabetically([
        {depositAccount: {alias: 'bbb'}},
        {depositAccount: {alias: 'aaa'}},
      ]),
    ).toEqual([
      {depositAccount: {alias: 'aaa'}},
      {depositAccount: {alias: 'bbb'}},
    ]);
    expect(
      sortAliasAlphabetically([
        {depositAccount: {alias: 'aaa'}},
        {depositAccount: {alias: 'bbb'}},
      ]),
    ).toEqual([
      {depositAccount: {alias: 'aaa'}},
      {depositAccount: {alias: 'bbb'}},
    ]);
    expect(
      sortAliasAlphabetically([
        {depositAccount: {alias: 'aaa'}},
        {depositAccount: {alias: 'aaa'}},
      ]),
    ).toEqual([
      {depositAccount: {alias: 'aaa'}},
      {depositAccount: {alias: 'aaa'}},
    ]);
  });

  test('sortCreditCardAlphabetically ', () => {
    expect(
      sortCreditCardAlphabetically([
        {
          cardEmbossedName: 'aaa',
        },
        {
          cardEmbossedName: 'bbb',
        },
      ]),
    ).toEqual([{cardEmbossedName: 'aaa'}, {cardEmbossedName: 'bbb'}]);
    expect(
      sortCreditCardAlphabetically([
        {
          cardEmbossedName: 'bbb',
        },
        {
          cardEmbossedName: 'aaa',
        },
      ]),
    ).toEqual([{cardEmbossedName: 'aaa'}, {cardEmbossedName: 'bbb'}]);
    expect(
      sortCreditCardAlphabetically([
        {
          cardEmbossedName: 'aaa',
        },
        {
          cardEmbossedName: 'aaa',
        },
      ]),
    ).toEqual([{cardEmbossedName: 'aaa'}, {cardEmbossedName: 'aaa'}]);
  });

  test('getCardNumber ', () => {
    expect(getCardNumber(0, {accountNo: 1111})).toEqual(1111);
    expect(getCardNumber(2, {cardNo: 1111})).toEqual(1111);
    expect(getCardNumber(21, {cardNo: 1111})).toEqual('');
  });

  test('getEnabledShortcutsByCode ', () => {
    expect(getEnabledShortcutsByCode([])).toBeDefined();
  });
});
