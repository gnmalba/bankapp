import React from 'react';
import {View, Text} from 'react-native';
import useAccountsLoading from '../useAccountsLoading';
import {render} from '@testing-library/react-native';
import MockProvider from '@utils/testFramework';

jest.useFakeTimers();

jest.mock('react-native', () => {
  return jest.requireActual('react-native');
});

const store = {
  config: {
    widgets: [],
    checked: [],
  },
  credit: {
    creditList: {
      loading: true,
    },
  },
  deposit: {
    fetching: true,
  },
  yazz: {
    loading: true,
  },
};

const TestComponent = ({display}) => {
  const accountLoading = useAccountsLoading();
  return (
    <View>
      <Text>{accountLoading ? 'loading' : 'not loading'}</Text>
    </View>
  );
};

describe('useCntDown', () => {
  test('should work properly', () => {
    render(
      <MockProvider store={store}>
        <TestComponent />
      </MockProvider>,
    );
  });
});
