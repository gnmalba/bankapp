import {formatDateTime, formatDate} from '../date';

describe('date', () => {
  test('date', () => {
    expect(formatDateTime('1/1/21')).toBeDefined();
    expect(formatDate('1/1/21')).toEqual('Jan-01-2021');
  });
});
