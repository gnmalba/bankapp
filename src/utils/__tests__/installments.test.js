import {
  toCardList,
  toCardListDeposits,
  preparePaymentTermsRequest,
  prepareSavePayload,
  getCardSerNo,
  getCardSerNoCreditState,
  mapSortStr,
  getCardToDispatch,
  getFilterCount,
  saveInformationModal,
  getErrorMessage,
  hasMaximumAttempts,
  getErrorCode,
  getHighestAvailableBalanceCard,
  sortByStatus,
  handleGoBack,
  sortBanks,
  getCleanCreditList,
  combineParams,
  strReplaceAll,
  getReviewScreenMessage,
  getDepositToC2G,
} from '../installments';

describe('installments', () => {
  test('getReviewScreenMessage ', () => {
    expect(getReviewScreenMessage('BC')).toEqual(
      'You will be notified on the status of your Balance Conversion application.',
    );
    expect(getReviewScreenMessage('BT')).toBeDefined();
    expect(getReviewScreenMessage('BC')).toEqual(
      'You will be notified on the status of your Balance Conversion application.',
    );
    expect(getReviewScreenMessage('C2GO')).toEqual(
      'You will be notified on the status of your Cash2Go application.',
    );
    expect(getReviewScreenMessage('NP')).toEqual('');
  });

  test('strReplaceAll ', () => {
    expect(strReplaceAll('meeks', 'm', 's')).toEqual('meekm');
  });

  test('combineParams', () => {
    expect(combineParams({filters: []}, 'BC', {})).toEqual({});
    expect(combineParams({filters: []}, 'BT', {})).toEqual({});
    expect(combineParams({filters: []}, 'C2GO', {})).toEqual({});
    expect(combineParams({filters: []}, 'NP', {})).toEqual({});
  });

  test('getCleanCreditList', () => {
    expect(getCleanCreditList([{cardSerNo: 1111}, {cardSerNo: 2222}])).toEqual([
      {cardSerNo: 1111},
      {cardSerNo: 2222},
    ]);
  });

  test('sortBanks', () => {
    expect(sortBanks([{Name: 'BDO'}, {Name: 'BPI'}])).toEqual([
      {Name: 'BDO'},
      {Name: 'BPI'},
    ]);
  });

  test('sortByStatus ', () => {
    expect(sortByStatus([{STATUS: 'IN PROGRESS'}])).toEqual([
      {STATUS: 'IN PROGRESS'},
    ]);
  });

  test('getHighestAvailableBalanceCard', () => {
    expect(
      getHighestAvailableBalanceCard(
        [
          {name: 'a', cardNumber: 1111, cardNo: 1111},
          {name: 'b', cardNumber: 1111, cardNo: 1111},
        ],
        'name',
      ),
    ).toEqual({name: 'a', cardNumber: 1111, cardNo: 1111});
    expect(getHighestAvailableBalanceCard([], '')).toEqual(null);
  });

  test('getErrorCode', () => {
    expect(getErrorCode({errors: [{code: 'invalid_mpin_exceeded'}]})).toEqual(
      'invalid_mpin_exceeded',
    );
    expect(getErrorCode(null)).toEqual('');
  });

  test('hasMaximumAttempts', () => {
    expect(
      hasMaximumAttempts({errors: [{code: 'invalid_mpin_exceeded'}]}),
    ).toEqual(true);
    expect(hasMaximumAttempts({errors: [{code: ''}]})).toEqual(false);
    expect(hasMaximumAttempts(null)).toEqual(false);
  });

  test('getErrorMessage', () => {
    expect(getErrorMessage({errors: [{code: 'mpin'}]})).toEqual(
      'Passcode is invalid. Please try again.',
    );
    expect(
      getErrorMessage({errors: [{code: 'invalid_mpin_exceeded'}]}),
    ).toEqual('');
    expect(getErrorMessage({errors: [{code: ''}]})).toEqual(undefined);
    expect(getErrorMessage({})).toEqual('');
    expect(getErrorMessage(null)).toEqual(
      'Sorry, there was a problem processing your request.',
    );
  });

  test('getFilterCount', () => {
    expect(
      getFilterCount({
        status: 'test',
        minAmount: '0',
        maxAmount: '999999999',
      }),
    ).toEqual(1);
  });

  test('getFilterCount 2', () => {
    expect(getFilterCount({})).toEqual(1);
  });

  test('mapSortStr status', () => {
    expect(mapSortStr('status')).toEqual(0);
    expect(mapSortStr('Date Applied by Latest')).toEqual(1);
    expect(mapSortStr('Date Applied by Oldest')).toEqual(2);
    expect(mapSortStr('Amount: High to Low')).toEqual(3);
    expect(mapSortStr('Amount: Low to High')).toEqual(4);
    expect(mapSortStr('np')).toEqual(-1);
  });

  test('getCardSerNoCreditState', () => {
    const res = getCardSerNoCreditState([]);
    expect(res).toEqual('');
  });

  test('getCardSerNoCreditState positive', () => {
    const res = getCardSerNoCreditState([
      {
        cardSerNo: 1111,
      },
    ]);
    expect(res).toEqual(1111);
  });

  test('getCardToDispatch positive', () => {
    getCardToDispatch([], {
      cardSerNo: 1111,
    });
  });

  test('toCardList', () => {
    toCardList([], 'BC');
  });

  test('toCardListDeposits', () => {
    toCardListDeposits([]);
  });

  test.each([
    {
      currentInstallmentType: 'BT',
      balanceTransfer: {
        transferTo: {
          cardSerNo: 1111,
        },
        amount: 1,
      },
    },
    {
      currentInstallmentType: 'BC',
      balanceConversion: {
        from: {
          cardSerNo: 1111,
        },
        amount: 1,
      },
    },
    {
      currentInstallmentType: 'C2GO',
      cash2Go: {
        from: {
          cardSerNo: 1111,
        },
        amount: 1,
      },
    },
    {
      currentInstallmentType: 'NP',
    },
  ])('preparePaymentTermsRequest ', (state) => {
    const res = preparePaymentTermsRequest(state);
    if (state.currentInstallmentType !== 'NP') {
      expect(res).toEqual({cardSerNo: 1111, amount: 1});
    }
  });

  test.each([
    {
      currentInstallmentType: 'BT',
      balanceTransfer: {
        transferTo: {
          cardSerNo: 1111,
        },
        amount: 1,
        paymentTerms: {
          monthlyAmortization: 1,
          interestRate: 10,
          totalInstallmentAmount: 1,
          term: 5,
        },
        transferFrom: {
          bankName: 'test',
          cardNumber: 123412341234,
        },
      },
    },
    {
      currentInstallmentType: 'BC',
      balanceConversion: {
        from: {
          cardSerNo: 1111,
        },
        amount: 1,
        paymentTerms: {
          monthlyAmortization: 1,
          interestRate: 10,
          totalInstallmentAmount: 1,
          term: 5,
        },
      },
    },
    {
      currentInstallmentType: 'C2GO',
      cash2Go: {
        from: {
          cardSerNo: 1111,
        },
        amount: 1,
        paymentTerms: {
          monthlyAmortization: 1,
          interestRate: 10,
          totalInstallmentAmount: 1,
          term: 5,
        },
        to: {
          disbursementOption: false,
          alias: 'test',
          accountNo: 1111,
        },
      },
    },
  ])('prepareSavePayload', (type) => {
    const res = prepareSavePayload(type.currentInstallmentType, type, '123456');
    expect(res).toBeDefined();
  });

  test.each([
    {transferTo: {cardSerNo: 1111}, from: {cardSerNo: 2222}},
    {from: {cardSerNo: 1111}},
  ])('getCardSerNo', (state) => {
    const res = getCardSerNo(state);
    expect(res).toEqual(1111);
  });

  test('saveInformationModal', () => {
    saveInformationModal('BC');
  });

  test('handleGoBack', () => {
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: false,
          selectedCredit: {
            cardSerNo: 1111,
            alias: 'test',
          },
          rootScreen: 'soa',
        },
        navigation: {pop: () => {}, goBack: () => {}},
        route: {name: 'Cash2GoScreen'},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: true,
          isFormDirty: false,
          selectedCredit: {
            cardSerNo: 1111,
            alias: 'test',
          },
          rootScreen: 'soa',
        },
        navigation: {pop: () => {}, goBack: () => {}},
        route: {name: 'Cash2GoScreen'},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          selectedCredit: {
            cardSerNo: 1111,
            alias: 'test',
          },
          rootScreen: 'soa',
        },
        navigation: {pop: () => {}, goBack: () => {}},
        route: {name: 'Cash2GoScreen'},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          selectedCredit: {
            cardSerNo: 1111,
            alias: 'test',
          },
          rootScreen: 'soa',
        },
        navigation: {pop: () => {}, goBack: () => {}, navigate: () => {}},
        route: {name: 'ReviewScreen'},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          selectedCredit: {
            cardSerNo: 1111,
            alias: 'test',
          },
          rootScreen: 'soa',
        },
        navigation: {pop: () => {}, goBack: () => {}, navigate: () => {}},
        route: {name: 'ViewInstallmentsScreen'},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          selectedCredit: {
            cardSerNo: 1111,
            alias: 'test',
          },
          rootScreen: '',
        },
        navigation: {
          pop: () => {},
          goBack: () => {},
          navigate: () => {},
          reset: () => {},
        },
        route: {name: 'ViewInstallmentsScreen'},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          rootScreen: '',
        },
        navigation: {
          pop: () => {},
          goBack: () => {},
          navigate: () => {},
          reset: () => {},
        },
        route: {name: 'ViewInstallmentsScreen', params: {fromReview: true}},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          rootScreen: '',
        },
        navigation: {
          goBack: () => {},
        },
        route: {name: 'ViewInstallmentsScreen', params: {fromReview: false}},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          rootScreen: '',
        },
        navigation: {
          navigate: () => {},
        },
        route: {name: 'MaximumPasscodeScreen', params: {fromReview: false}},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
    expect(
      handleGoBack({
        installmentsState: {
          leavePromptVisible: false,
          isFormDirty: true,
          rootScreen: '',
        },
        navigation: {
          pop: () => {},
        },
        route: {name: 'else', params: {fromReview: false}},
        dispatch: () => {},
      }),
    ).toEqual(undefined);
  });

  test('getDepositToC2G ', () => {
    getDepositToC2G({to: {disbursementOption: 'E'}});
    getDepositToC2G({to: {disbursementOption: 'U'}});
    getDepositToC2G({to: {disbursementOption: 'N'}});
  });
});
