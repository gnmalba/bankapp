import {numberFormatNoLimit, addDecimal, removeCommas} from '../numberInputs';

describe('numberFormatNoLimit', () => {
  test.each([null, 123456, 12345612345612345, 123412341234123.1231])(
    'numberFormatNoLimit',
    (nums) => {
      numberFormatNoLimit(nums);
    },
  );

  test('addDecimal', () => {
    const res = addDecimal('10.09sdf');
    expect(res).toEqual('10.09');
  });

  test('removeCommas', () => {
    const res = removeCommas('10,110,100');
    expect(res).toEqual('10110100');
  });
});
