// @flow

import InstallmentDetail from '@composition/InstallmentDetail';
import {clearInstallments, setLeavePrompt} from '@ducks/installments/actions';
import type {
  BalanceTransferSaveRequest,
  BalanceConversionSaveRequest,
  InstallmentTypesString,
  InstallmentStateType,
  InstallmentBankType,
} from '@ducks/installments/types';

import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';

import React from 'react';
import {sortCreditCardAlphabetically} from './accounts';
import {testProps} from './componentCustomProps';
import {ASYNC_STORAGE_KEYS} from './constants';
import {toText} from './helpers';

// TODO: add types
export const bankDataToSelectInput = (data: any): any[] => {
  return data.map((item) => ({...item, id: item.Code, value: item.Name}));
};

const shortcutsAllowedFilter = (
  item: Object,
  installmentType: InstallmentTypesString,
): boolean => {
  const {shortcuts} = item;
  const codes = {
    BT: 'BALANCE TRANSFER',
    BC: 'BALANCE CONVERSION',
    C2GO: 'CASH2GO INSTL',
    '': 'BALANCE TRANSFER',
  };

  const correspondingShortcut = shortcuts.find(
    (s) => s.code === codes[installmentType],
  );

  if (correspondingShortcut) {
    return correspondingShortcut.enabled;
  }

  return true;
};

export const toCardList = (
  data: any,
  installmentType: InstallmentTypesString,
): any[] => {
  if (data) {
    return sortCreditCardAlphabetically(
      data.filter(
        (account) => account.flag !== null && account.flag !== 'NOT_ALLOWED',
      ),
      installmentType,
    )
      .filter((acc) => shortcutsAllowedFilter(acc, installmentType))
      .map((account) => {
        return {
          ...account,
          name: account.cardEmbossedName,
          number: account.cardNo,
          id: account.cardNo,
        };
      });
  }
  return [];
};

export const toCardListDeposits = (data: any): any[] => {
  if (data) {
    return data.map((item) => {
      return {
        ...item,
        name: item.alias,
        id: item.id,
        number: item.accountNo,
      };
    });
  }
  return [];
};

// TODO: add types
export const preparePaymentTermsRequest = (state: any): Object => {
  const {currentInstallmentType} = state;
  switch (currentInstallmentType) {
    case 'BT':
      return {
        cardSerNo: state.balanceTransfer.transferTo.cardSerNo,
        amount: state.balanceTransfer.amount,
      };
    case 'BC':
      return {
        cardSerNo: state.balanceConversion.from.cardSerNo,
        amount: state.balanceConversion.amount,
      };
    case 'C2GO':
      return {
        cardSerNo: state.cash2Go.from.cardSerNo,
        amount: state.cash2Go.amount,
      };
    default:
      return {};
  }
};

const prepareC2GoDepositTo = (currentState: Object): Object => {
  if (!currentState.disbursementOption) {
    return {
      disbursementOption: DISBURSEMENT_OPTIONS.ENROLLED,
      destinationAccountName: currentState.alias,
      destinationAccountNumber: currentState.accountNo,
    };
  }
  return currentState;
};

// todo param flow typing
export const prepareSavePayload = (
  installmentType: InstallmentTypesString,
  state: any, // TODO: add type
  passcode: string,
): any => {
  let payload: BalanceTransferSaveRequest | BalanceConversionSaveRequest;

  if (installmentType === 'BT') {
    state = state.balanceTransfer;
    const {paymentTerms} = state;

    payload = {
      customerId: state.transferTo.customerId,
      cardSerNo: state.transferTo.cardSerNo,
      amount: state.amount,
      currency: 'PHP', // hardcoded for now?
      amortization: Number(paymentTerms.monthlyAmortization),
      rate: parseFloat(paymentTerms.interestRate),
      totalAmount: Number(paymentTerms.totalInstallmentAmount),
      duration: Number(paymentTerms.term),
      sourceBankName: state.transferFrom.bankName,
      sourceBankCardNumber: state.transferFrom.cardNumber,
      passcode,
      shortCode: paymentTerms.shortCode,
    };
  } else if (installmentType === 'BC') {
    state = state.balanceConversion;
    const {paymentTerms} = state;
    payload = {
      customerId: state.from.customerId,
      cardSerNo: state.from.cardSerNo,
      amount: state.amount,
      currency: 'PHP', // hardcoded for now?
      amortization: Number(paymentTerms.monthlyAmortization),
      rate: parseFloat(paymentTerms.interestRate),
      totalAmount: Number(paymentTerms.totalInstallmentAmount),
      duration: Number(paymentTerms.term),
      passcode,
      shortCode: paymentTerms.shortCode,
    };
  } else if (installmentType === 'C2GO') {
    state = state.cash2Go;
    const {paymentTerms} = state;
    payload = {
      customerId: state.from.customerId,
      cardSerNo: state.from.cardSerNo,
      amount: state.amount,
      currency: 'PHP', // hardcoded for now?
      amortization: Number(paymentTerms.monthlyAmortization),
      rate: parseFloat(paymentTerms.interestRate),
      totalAmount: Number(paymentTerms.totalInstallmentAmount),
      duration: Number(paymentTerms.term),
      passcode,
      shortCode: paymentTerms.shortCode,
      ...prepareC2GoDepositTo(state.to),
    };
  }

  return payload;
};

export const renderSuccessDetails = (
  state: any,
  testID: string = 'installment',
): React$Node => {
  return (
    <>
      <InstallmentDetail
        small
        label="reference number"
        value={toText(
          strReplaceAll(state?.responseData?.Code, '', '-'),
          testID + 'ReferenceNumber',
          'inputerror',
        )}
        {...testProps(testID + 'ReferenceNumber')}
      />
      <InstallmentDetail
        small
        label="date and time"
        value={toText(
          moment(state.dateTime).format('MMM-DD-yyyy [\n]h:mm A [(GMT +8)]'),
          testID + 'DateAndTime',
          'inputerror',
        )}
        {...testProps(testID + 'DateAndTime')}
      />
    </>
  );
};

export const getCardSerNo = (state: Object): string => {
  if (state.transferTo) {
    return state.transferTo.cardSerNo;
  }
  return state.from.cardSerNo;
};

export const getCardSerNoCreditState = (data: any[]): string => {
  const card = data.find((cr) => cr.cardSerNo);

  if (card) {
    return card.cardSerNo;
  }

  return '';
};

export const mapSortStr = (str: string): number => {
  switch (str) {
    case SORT_OPTIONS.status:
      return 0;
    case SORT_OPTIONS.dateLatest:
      return 1;
    case SORT_OPTIONS.dateOldest:
      return 2;
    case SORT_OPTIONS.amountHigh:
      return 3;
    case SORT_OPTIONS.amountLow:
      return 4;
    default:
      return -1;
  }
};

export const getCardToDispatch = (
  list: any[],
  data: {cardSerNo: string},
): Object | null => {
  const {cardSerNo} = data;

  const card = list.find((item) => item.cardSerNo === cardSerNo);
  if (card) {
    card.cardNumber = card.cardNo;
  }
  return card;
};

export const FILTER_CONSTANTS = {
  MINIMUM_AMOUNT: '0',
  MAXIMUM_AMOUNT: '999999999',
};

export const getFilterCount = (state: Object): number => {
  let filterCount = 0;
  if (state) {
    const {status, minAmount, maxAmount} = state;
    if (
      minAmount === FILTER_CONSTANTS.MINIMUM_AMOUNT &&
      maxAmount === FILTER_CONSTANTS.MAXIMUM_AMOUNT
    ) {
      if (status !== INSTALLMENTS_STATUS_FILTERS.SHOW_ALL && status) {
        filterCount += 1;
      }
    } else {
      if (status !== INSTALLMENTS_STATUS_FILTERS.SHOW_ALL && status) {
        filterCount += 2;
      } else {
        filterCount += 1;
      }
    }
  }

  return filterCount;
};

export const saveInformationModal = async (
  installmentType: InstallmentTypesString,
) => {
  if (installmentType) {
    await AsyncStorage.setItem(
      ASYNC_STORAGE_KEYS.SHOW_INFO_MODAL[installmentType],
      JSON.stringify(true),
    );
  }
};

export const getErrorMessage = (data: Object): string => {
  try {
    const errors = data.errors;
    if (errors) {
      if (errors[0].code === 'mpin') {
        return 'Passcode is invalid. Please try again.';
      } else if (errors[0].code === 'invalid_mpin_exceeded') {
        return '';
      } else {
        return errors[0].message;
      }
    }

    return '';
  } catch {
    return 'Sorry, there was a problem processing your request.';
  }
};

export const hasMaximumAttempts = (data: Object): boolean => {
  try {
    const errors = data.errors;
    if (errors && errors[0].code === 'invalid_mpin_exceeded') {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
};

export const getErrorCode = (data: Object): string => {
  try {
    const errors = data.errors;

    return errors[0].code;
  } catch (e) {
    return '';
  }
};

export const ERROR_SCREEN_STR = 'Error Screen';

export const MINIMUM_AMOUNT = 6000;
export const MAIN_SCREENS = [
  'BalanceTransferScreen',
  'BalanceConversionScreen',
  'Cash2GoScreen',
];

export const PREVENTED_SCREENS_BACK = ['ReviewScreen'];

export const getHighestAvailableBalanceCard = (
  creditList: any[],
  field: string,
): Object | null => {
  const sortedList = creditList.sort((a, b) => b[field] - a[field]);
  if (sortedList.length) {
    sortedList[0].cardNumber = sortedList[0].cardNo;
    return sortedList[0];
  }

  return null;
};

export const INSTALLMENT_CODES_TITLE = {
  BT: 'Balance Transfer',
  BC: 'Balance Conversion',
  C2GO: 'Cash2Go',
  '': '',
};

const sortByDate = (data: any[]): any[] => {
  const inProgress = data
    .filter((item) => item.STATUS === INSTALLMENTS_STATUS_FILTERS.IN_PROGRESS)
    .sort((a, b) =>
      moment(b.TRANSTATUS_DATECREATED).diff(moment(a.TRANSTATUS_DATECREATED)),
    );
  const approved = data
    .filter((item) => item.STATUS === INSTALLMENTS_STATUS_FILTERS.APPROVED)
    .sort((a, b) =>
      moment(b.TRANSTATUS_DATECREATED).diff(moment(a.TRANSTATUS_DATECREATED)),
    );
  const declined = data
    .filter((item) => item.STATUS === INSTALLMENTS_STATUS_FILTERS.DECLINED)
    .sort((a, b) =>
      moment(b.TRANSTATUS_DATECREATED).diff(moment(a.TRANSTATUS_DATECREATED)),
    );

  return inProgress.concat(approved).concat(declined);
};

export const sortByStatus = (data: any[]): any[] => {
  const indexOfFirstInProgress = data.findIndex(
    (item) => item.STATUS === INSTALLMENTS_STATUS_FILTERS.IN_PROGRESS,
  );
  if (indexOfFirstInProgress > -1) {
    const inProgressArr = data.slice(indexOfFirstInProgress, data.length);
    const restArr = data.slice(0, indexOfFirstInProgress);
    data = inProgressArr.concat(restArr);
  }

  return sortByDate([...data]);
};

export const handleGoBack = ({
  navigation,
  route,
  installmentsState,
  dispatch,
}: {
  navigation: any,
  route: any,
  installmentsState: InstallmentStateType,
  dispatch: any,
}): void => {
  const {
    leavePromptVisible,
    isFormDirty,
    selectedCredit,
    rootScreen,
  } = installmentsState;

  if (MAIN_SCREENS.includes(route.name)) {
    if (leavePromptVisible) {
      return;
    } else if (isFormDirty) {
      dispatch(setLeavePrompt(true));
      return;
    }

    dispatch(clearInstallments());
    navigation.goBack();
  } else if (PREVENTED_SCREENS_BACK.includes(route.name)) {
    return;
  } else if (route.name === 'ViewInstallmentsScreen') {
    if (rootScreen === 'soa') {
      navigation.navigate('Accounts', {
        screen: 'StatementOfAccountScreen',
      });
    } else if (rootScreen === 'overview') {
      navigation.navigate('CreditScreenOverview', {
        id: selectedCredit?.cardSerNo,
        alias: selectedCredit?.alias,
      });
    } else if (route.params?.fromReview) {
      navigation.navigate('Dashboard');
      navigation.reset({
        index: 0,
        routes: [{name: 'Dashboard'}],
      });
    } else {
      navigation.goBack();
    }

    dispatch(clearInstallments());
  } else if (route.name === 'MaximumPasscodeScreen') {
    navigation.navigate('ConfirmScreen');
  } else {
    navigation.pop();
  }
};

export const sortBanks = (
  data: InstallmentBankType[],
): InstallmentBankType[] => {
  return data.sort((a, b) => a.Name.localeCompare(b.Name));
};

export const getCleanCreditList = (data: any[]): any[] => {
  return data.filter((card) => card.cardSerNo);
};

export const combineParams = (
  state: Object,
  installmentType: InstallmentTypesString,
  params: Object,
): Object => {
  const {filters} = state;

  if (filters) {
    switch (installmentType) {
      case 'BT':
        return {
          ...params,
          ...filters.bt,
        };
      case 'BC':
        return {
          ...params,
          ...filters.bc,
        };
      case 'C2GO':
        return {
          ...params,
          ...filters.c2g,
        };
      default:
        return params;
    }
  }

  return params;
};

export const MAX_CC_LENGTH = 16;

export const DISBURSEMENT_OPTIONS = {
  ENROLLED: 'E',
  UNENROLLED: 'U',
  NONMB: 'N',
};

export const DISBURSEMENT_OPTIONS_RADIO = {
  ENROLLED: 0,
  UNENROLLED: 1,
  NONMB: 2,
};

export const INSTALLMENTS_STATUS_FILTERS = {
  SHOW_ALL: 'SHOW ALL',
  APPROVED: 'APPROVED',
  DECLINED: 'DECLINED',
  IN_PROGRESS: 'IN PROGRESS',
};

export const DEFAULT_CURRENCY = 'PHP';

export const strReplaceAll = (
  subject: string,
  strToReplace: string,
  strToRemove: string,
): string => {
  return subject.split(strToRemove).join(strToReplace);
};

export const formatEAIR = (eair: string): string => {
  const cleanEAIR = Number(strReplaceAll(eair, '', '%'));
  const rounded = Math.round(cleanEAIR * 100) / 100;
  return `${rounded}%`;
};

export const AMOUNT_INPUT_ERRORS_INSTALLMENTS = {
  EMPTY: 'Please enter amount',
};

export const getReviewScreenMessage = (
  installmentType: InstallmentTypesString,
): string => {
  switch (installmentType) {
    case 'BT':
      return `You will be notified via SMS on the status of your application for a
    balance transfer.`;

    case 'BC':
      return 'You will be notified on the status of your Balance Conversion application.';

    case 'C2GO':
      return 'You will be notified on the status of your Cash2Go application.';
    default:
      return '';
  }
};

export const getDepositToC2G = (state: Object): Object => {
  const {to} = state;
  let defaultValues = {
    selectedValue: -1,
    bankName: '',
    accountName: '',
    accountNumber: '',
  };
  if (to) {
    if (to.disbursementOption) {
      if (to.disbursementOption === DISBURSEMENT_OPTIONS.UNENROLLED) {
        defaultValues = {
          selectedValue: 1,
          accountName: to.destinationAccountName,
          accountNumber: to.destinationAccountNumber,
        };
      } else if (to.disbursementOption === DISBURSEMENT_OPTIONS.NONMB) {
        defaultValues = {
          selectedValue: 2,
          accountName: to.destinationAccountName,
          accountNumber: to.destinationAccountNumber,
          bankName: to.destinationBankName,
        };
      }
    } else {
      defaultValues = {...defaultValues, selectedValue: 0};
    }
  }

  return defaultValues;
};

export const getBankByName = (banks: any[], bankName: string): Object => {
  return banks.find((b) => b.Name === bankName);
};
export const SORT_OPTIONS = {
  status: 'status',
  dateLatest: 'Date Applied by Latest',
  dateOldest: 'Date Applied by Oldest',
  amountHigh: 'Amount: High to Low',
  amountLow: 'Amount: Low to High',
};

export const filterOnlyPHP = (arr: any[]): any[] =>
  arr.filter((dep) => dep.currencyCode === 'PHP');
