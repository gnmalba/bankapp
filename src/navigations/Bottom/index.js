/**
 * @flow
 */

// React modules
import React from 'react';
import {View, Text, Platform} from 'react-native';

// Third party libraries
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {connect, useDispatch} from 'react-redux';
import styled from 'styled-components/native';

// Navigation Components
import PayBillsCreditStack from '@navigations/Stack/PayBillsCredit';
import CustomizeDashboardScreen from '@composition/CustomizeDashboard';
import SendMoneyScreen from '@screens/private/SendMoney/MainScreen';

// Components
import Icon from '@components/Icon';
import QuickLinks from '@composition/GuidedTourTooltip';
import DashboardScreen from '@screens/private/Dashboard';
import {Container, IconContainer, QuickLinksContainer} from './Styled';

// Utils
import {moderateScale, verticalScale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';

const Tab = createBottomTabNavigator();

const commonStyle = {flex: 1, justifyContent: 'center', alignItems: 'center'};

const SendLoadScreens = () => {
  return (
    <View style={commonStyle}>
      <Text>Send Load!</Text>
    </View>
  );
};

const MoreScreen = () => {
  return (
    <View style={commonStyle}>
      <Text>More!</Text>
    </View>
  );
};

const DashboardStack = createStackNavigator();

const TabNavigator = styled(Tab.Navigator).attrs((props) => ({
  tabBarOptions: {
    activeTintColor: props.theme.tab.bottomActiveTintColor,
    inactiveTintColor: props.theme.tab.bottomInactiveTintColor,
    labelPosition: 'below-icon',
    labelStyle: {
      fontSize: moderateScale(10),
      fontWeight: '600',
      fontFamily: 'MyriadPro-Semibold',
    },
    style: {
      borderTopWidth: 0,
      borderTopLeftRadius: 17,
      borderTopRightRadius: 17,
      backgroundColor: props.theme.tab.backgroundColor,
      width: '100%',
      boxShadow: props.theme.tab.boxShadow,
      ...Platform.select({
        ios: {
          ...(IS_DEVICE_TABLET && {
            paddingBottom: verticalScale(5),
          }),
          height: verticalScale(IS_DEVICE_TABLET ? 50 : 60),
        },
        android: {
          paddingBottom: verticalScale(5),
          height: verticalScale(50),
        },
      }),
    },
  },
}))``;

const BottomNavigation = ({dashboard}): React$Node => {
  const insets = useSafeAreaInsets();
  const dispatch = useDispatch();
  const {isQuickLinks} = dashboard;

  const handleContinue = () => {
    dispatch({
      type: 'UPDATE_GUIDE_QUICK_LINKS',
    });
  };

  return (
    <Container>
      <TabNavigator paddingBottom={insets.bottom}>
        <Tab.Screen
          name="dashboard"
          component={DashboardScreen}
          options={{
            tabBarLabel: 'Dashboard',
            tabBarIcon: ({color, focused, size}) => {
              if (isQuickLinks) {
                return (
                  <>
                    <QuickLinksContainer
                      bottomMargin={Platform.select({android: 42, ios: 20})}>
                      <QuickLinks
                        isVisible={isQuickLinks}
                        onClose={handleContinue}
                      />
                      <Icon
                        name="dashboard"
                        color={color}
                        size={moderateScale(20)}
                      />
                    </QuickLinksContainer>
                  </>
                );
              } else {
                return (
                  <IconContainer isSelected={focused}>
                    <Icon
                      name="dashboard"
                      color={color}
                      size={moderateScale(20)}
                    />
                  </IconContainer>
                );
              }
            },
          }}
        />
        <Tab.Screen
          name="SendMoneyTab"
          component={SendMoneyScreen}
          options={{
            tabBarLabel: 'Send Money',
            tabBarIcon: ({color, size, focused}) => (
              <IconContainer isSelected={focused}>
                <Icon
                  name="send-money"
                  color={color}
                  size={moderateScale(20)}
                />
              </IconContainer>
            ),
          }}
        />
        <Tab.Screen
          name="PayBillsCredit"
          component={PayBillsCreditStack}
          options={{
            tabBarLabel: 'Pay Bills',
            tabBarIcon: ({color, focused}) => (
              <IconContainer isSelected={focused}>
                <Icon name="pay-bills" color={color} size={moderateScale(20)} />
              </IconContainer>
            ),
          }}
        />
        <Tab.Screen
          name="Send Load"
          component={SendLoadScreens}
          options={{
            tabBarLabel: 'Send Load',
            tabBarIcon: ({color, focused}) => (
              <IconContainer isSelected={focused}>
                <Icon name="send-load" color={color} size={moderateScale(20)} />
              </IconContainer>
            ),
          }}
        />
        <Tab.Screen
          name="More"
          component={MoreScreen}
          options={{
            tabBarLabel: 'More',
            tabBarIcon: ({color, focused}) => (
              <IconContainer isSelected={focused}>
                <Icon name="more" color={color} size={moderateScale(20)} />
              </IconContainer>
            ),
          }}
        />
      </TabNavigator>
    </Container>
  );
};

const mapStateToProps = ({dashboard}) => ({dashboard});
const connectedBottomTab = connect(mapStateToProps)(BottomNavigation);

const DashboardRoutes = (): React$Node => (
  <DashboardStack.Navigator mode="modal" headerMode="none">
    <DashboardStack.Screen
      name="DashboardScreen"
      component={connectedBottomTab}
    />
    <DashboardStack.Screen
      name="CustomizeWidgetScreen"
      component={CustomizeDashboardScreen}
    />
  </DashboardStack.Navigator>
);
export default DashboardRoutes;
