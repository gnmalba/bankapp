/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {moderateScale, verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  display: flex;
  flex: 1;
  background-color: ${(props) => props.theme.layout.backgroundColor};
`;

export const QuickLinksContainer: ComponentType<any> = styled.View`
  margin-bottom: ${(props) => verticalScale(props.bottomMargin)}px;
`;

export const IconContainer: ComponentType<any> = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  border-top-width: 2px;
  border-top-color: ${(props) => (props.isSelected ? '#09BAD0' : 'transparent')}
  width: ${moderateScale(40)}px;
`;
