## ActivateCard

Activate Card component.

# Usage

```js
import ActivateCard from '@navigations/ActivateCard';

# Props
```

| Prop               | Type   | Required | Description                 |
| ------------------ | ------ | -------- | --------------------------- |
| testID             | string | true     | The testID prop             |
| accessibilityLabel | string | true     | The accessibilityLabel prop |
