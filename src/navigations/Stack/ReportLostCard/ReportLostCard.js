/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

// Components
import Header from '@components/Header';
import ReportLostCardList from '@screens/private/ReportLostCardList';
import ReportLostCardConfirmation from '@screens/private/ReportLostCardConfirmation';
import ReportLostCardPasscode from '@screens/private/ReportLostCardPasscode';
import ReportLostCardSuccess from '@screens/private/ReportLostCardSuccess';

// services
import * as NavigationService from '@navigations/service';

import type {PropsType} from './types';
import ReportLostCardTerms from '@screens/private/ReportLostCardTerms';

const Stack = createStackNavigator();

const ReportLostCardStack = (props: PropsType): React$Element<any> => {
  const header = (title: string = 'Report lost card', back: boolean = true) => (
    <Header
      title={title}
      isBackButton={back}
      onBackClick={() => NavigationService.pop()}
    />
  );

  return (
    <Stack.Navigator
      initialRouteName="ReportLostCardList"
      screenOptions={{
        headerShown: true,
        title: 'Report Lost Card',
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen
        name="ReportLostCardList"
        options={{
          header: () => header(),
        }}>
        {(screenProps) => <ReportLostCardList {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="ReportLostCardConfirmation"
        options={{
          header: () => header(),
        }}>
        {(screenProps) => <ReportLostCardConfirmation {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="ReportLostCardPasscode"
        options={{
          header: () => header('Verification'),
        }}>
        {(screenProps) => <ReportLostCardPasscode {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="ReportLostCardSuccess"
        options={{
          header: () => (
            <Header title="Report lost card" leftComponent={() => null} />
          ),
        }}>
        {(screenProps) => <ReportLostCardSuccess {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="ReportLostCardTerms"
        options={{
          header: () => header('Terms and Conditions'),
        }}>
        {(screenProps) => <ReportLostCardTerms {...screenProps} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};

export default ReportLostCardStack;
