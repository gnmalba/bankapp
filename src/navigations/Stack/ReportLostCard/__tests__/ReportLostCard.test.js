import React from 'react';
import {render} from '@testing-library/react-native';
import ReportLostCard from '../ReportLostCard';
import {ThemeProvider} from 'styled-components/native';
import {NavigationContainer} from '@react-navigation/native';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

jest.mock('@navigations/service');
jest.mock('@react-navigation/native', () => {
  const nav = jest.requireActual('@react-navigation/native');
  nav.useNavigation = () => jest.fn();
  return nav;
});

describe('ReportLostCard', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const navigation = {navigate: jest.fn()};

    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{
            reportLostCard: {
              loading: false,
              error: [],
              data: [],
            },
          }}>
          <ThemeProvider theme={DARK_THEME}>
            <ReportLostCard
              {...testProps('lostCardStackId')}
              navigation={navigation}
            />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
