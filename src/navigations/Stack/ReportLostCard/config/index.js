/**
 * @flow
 */

import React from 'react';
import {CENTER, IS_DEVICE_TABLET} from '@utils/constants';
import {moderateScale, verticalScale} from '@utils/scale';
import IconButton from '@components/IconButton';

import type {navigationOptionType} from '../types';
const source = require('@assets/icons/back-icon.png');

export const navigationOption: navigationOptionType = {
  headerTitleAllowFontScaling: false,
  headerBackTitleVisible: false,
  headerLeft: (props) => <IconButton source={source} {...props} />,
  headerStyle: {
    borderBottomWidth: 0,
    shadowOpacity: 0,
  },
  headerTitleAlign: CENTER,
  headerTitleStyle: {
    fontSize: moderateScale(IS_DEVICE_TABLET ? 16 : 20),
    lineHeight: verticalScale(24),
  },
  headerRightContainerStyle: {
    marginRight: moderateScale(IS_DEVICE_TABLET ? -10 : 0),
  },
  cardStyle: {opacity: 1},
};
