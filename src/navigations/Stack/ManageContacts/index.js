// @flow

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {withTheme} from 'styled-components';
import Header from '@components/Header';

import {screens} from './config';

import type {ManageContactsNavProps} from './types';

const Stack = createStackNavigator();

const renderCustomHeader = (props) => {
  const {
    navigation,
    pop,
    headerTitle,
    prevScreen,
    goBackDrawer,
    isBackButton = false,
  } = props;
  return (
    <Header
      title={headerTitle}
      isBackButton={isBackButton}
      onBackClick={() => {
        if (goBackDrawer) {
          return navigation.openDrawer();
        }
        if (prevScreen) {
          return navigation.navigate(prevScreen);
        }
        return pop();
      }}
    />
  );
};

const getHeaderOptions = (screenProps) => {
  if (screenProps.headerTitle) {
    return {
      header: () => renderCustomHeader(screenProps),
    };
  }
  return {headerShown: false};
};

const ManageContacts = (props: ManageContactsNavProps): React$Element<any> => {
  const navigationPop = () => props.navigation.pop();

  return (
    <Stack.Navigator initialRouteName="Contacts">
      {screens.map((screen) => {
        const {name, component} = screen;

        const headerOptions = getHeaderOptions({
          ...screen,
          pop: navigationPop,
          navigation: props.navigation,
        });

        return (
          <Stack.Screen
            key={name}
            name={name}
            component={component}
            options={{
              ...headerOptions,
            }}
          />
        );
      })}
    </Stack.Navigator>
  );
};

export default (withTheme(ManageContacts): any);
