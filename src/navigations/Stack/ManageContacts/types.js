/**
 * @flow
 */

export type ManageContactsNavProps = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: Object,
};
