import Contacts from '@screens/private/ManageContacts/Contacts';
import AddContact from '@screens/private/ManageContacts/AddContact';
import ConfirmDetails from '@screens/private/ManageContacts/ConfirmDetails';
import PasscodeScreen from '@screens/private/ManageContacts/PasscodeScreen';
import SuccessScreen from '@screens/private/ManageContacts/SuccessScreen';
import ViewContact from '@screens/private/ManageContacts/ViewContact';
import EditContact from '@screens/private/ManageContacts/EditContact';

export const screens = [
  {
    name: 'Contacts',
    component: Contacts,
    headerTitle: 'Enrolled Contacts',
    goBackDrawer: true,
  },
  {
    name: 'Add Contact',
    component: AddContact,
    headerTitle: 'Enroll Contact',
    prevScreen: 'Contacts',
    isBackButton: true,
  },
  {
    name: 'Edit Contact',
    component: EditContact,
    headerTitle: 'Edit Details',
    isBackButton: true,
  },
  {
    name: 'View Contact',
    component: ViewContact,
    headerTitle: 'Contact Details',
    isBackButton: true,
  },
  {
    name: 'ConfirmDetails',
    component: ConfirmDetails,
    headerTitle: 'Confirm Details',
    prevScreen: 'Add Contact',
    isBackButton: true,
  },
  {
    name: 'PasscodeScreen',
    component: PasscodeScreen,
    headerTitle: 'Verification',
    prevScreen: 'ConfirmDetails',
    isBackButton: true,
  },
  {
    name: 'SuccessScreen',
    component: SuccessScreen,
    headerTitle: '',
    isHeaderTransparent: true,
  },
];
