/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

// Component
import Header from '@components/Header';

// Screens
import MainScreen from '@screens/private/SendMoney/MainScreen';
import SourceAccountScreen from '@screens/private/SendMoney/SourceAccountScreen';
import RecipientScreen from '@screens/private/SendMoney/RecipientScreen';
import ConfirmDetailsScreen from '@screens/private/SendMoney/ConfirmDetailsScreen';
import VerificationScreen from '@screens/private/SendMoneyVerification/SendMoneyVerification';
import SendMoneyTransactionResultScreen from '@screens/private/SendMoneyTransactionResult/SendMoneyTransactionResult';

const Stack = createStackNavigator();

const SendMoneyStack = (props: any): React$Element<any> => {
  const {headerShown} = props;
  const header = () => (
    <Header
      title={'Send Money'}
      onBackClick={() => props.navigation.goBack()}
      isBackButton
    />
  );

  return (
    <Stack.Navigator
      initialRouteName="SendMoneyStack"
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen
        name="MainScreen"
        component={MainScreen}
        options={{
          headerShown,
          header,
        }}
      />
      <Stack.Screen
        name="SourceAccountScreen"
        component={SourceAccountScreen}
        options={{
          headerShown,
          header,
        }}
      />
      <Stack.Screen
        name="RecipientScreen"
        component={RecipientScreen}
        options={{
          headerShown,
          header,
        }}
      />
      <Stack.Screen
        name="ConfirmDetailsScreen"
        component={ConfirmDetailsScreen}
        options={{
          headerShown,
          header: () => (
            <Header
              title={'Confirm Details'}
              onBackClick={() => props.navigation.goBack()}
              isBackButton
            />
          ),
        }}
      />
      <Stack.Screen
        name="VerificationScreen"
        component={VerificationScreen}
        options={{
          headerShown,
          header: () => (
            <Header
              title={'Verification'}
              onBackClick={() => props.navigation.goBack()}
              isBackButton
            />
          ),
        }}
      />
      <Stack.Screen
        name="SendMoneyTransactionResult"
        component={SendMoneyTransactionResultScreen}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default SendMoneyStack;
