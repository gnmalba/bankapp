/**
 * @flow
 * @format
 *
 * Main navigation
 *
 */

import React from 'react';
import type {ComponentType} from 'react';

import {useSelector} from 'react-redux';

import {TouchableOpacity, Platform} from 'react-native';

import {withTheme} from 'styled-components/native';
import {createStackNavigator} from '@react-navigation/stack';
import Drawer from '@navigations/Drawer';

import {GearIcon, BackIcon} from '@components/Header/Styled';
import Header from '@components/Header';
import Text from '@components/Text';
import {scale} from '@utils/scale';

import DepositScreenOverview from '@screens/private/DepositScreenOverview';
import CreditScreenOverview from '@screens/private/CreditScreenOverview';
import PrepaidScreenOverview from '@screens/private/PrepaidScreenOverview';
import YazzSceenOverview from '@screens/private/YazzSceenOverview';
import {
  ChangePasscode,
  PasscodeChanged,
} from '@screens/public/RecoverAccess/index';

import {NAVIGATION_PASSCODECHANGED} from '@utils/constants';

import ManageAccountSettings from '@screens/private/ManageAccountSettings';

import EnrollAccount from '@navigations/Stack/EnrollAccount';

import ProcessingScreen from '@screens/common/ProcessingScreen';

import ReportLostCard from '@navigations/Stack/ReportLostCard';

import {InstallmentStack} from '@navigations/Stack/Installment';

import ActivateCardStack from './ActivateCard';

import SendMoneyStack from './SendMoney';

import type {ThemeInterface} from '@utils/commonTypes';

import ErrorScreen from '@screens/common/ErrorScreen';

import {aliasSelector} from '@ducks/manageAccountSettings/selectors';

import PayBillsCreditStack from '@navigations/Stack/PayBillsCredit';
import styled from 'styled-components';

import BillersStack from '@navigations/Stack/Billers';
import ManageContacts from './ManageContacts';

const Stack = createStackNavigator();

const StyledHeaderText = styled(Text).attrs((props) => ({
  mode: 'h3',
}))`
  font-family: MyriadPro-Semibold;
`;

const buildOverviewHeaderOptions = ({navigation, route, theme}, alias) => ({
  headerBackImage: () => <BackIcon />,
  headerBackTitle: ' ',
  headerTitle: () => (
    <StyledHeaderText numberOfLines={1}>
      {alias || route.params?.alias}
    </StyledHeaderText>
  ),
  headerRight: () => (
    <TouchableOpacity
      onPress={() => navigation.navigate('ManageAccountSettings')}
      activeOpacity={0.8}>
      <GearIcon />
    </TouchableOpacity>
  ),
  ...Platform.select({
    ios: {
      headerLeftContainerStyle: {
        marginLeft: scale(10),
      },
      headerRightContainerStyle: {
        marginRight: scale(10),
      },
    },
    android: {
      headerRightContainerStyle: {
        marginRight: scale(10),
      },
    },
  }),
  headerTintColor: theme.topAppBar.backgroundColor,
  headerTitleAlign: 'center',
  headerStyle: {
    backgroundColor: theme.topAppBar.backgroundColor,
  },
  gestureEnabled: false,
});

type MainNavigationPropTypes = {|
  theme: ThemeInterface,
|};

const MainNavigation = ({theme}: MainNavigationPropTypes) => {
  const alias = useSelector(aliasSelector);
  const data = useSelector((state) => state.authentication.data);

  const passcodeChangeOptions = {
    headerShown: true,
    header: ({navigation}) => {
      return (
        <Header
          title="Change Passcode"
          onBackClick={() => {
            navigation.navigate('Drawer');
          }}
        />
      );
    },
  };

  return (
    <Stack.Navigator
      screenOptions={{animationEnabled: false}}
      initialRouteName={
        !data.mpinRegistered && data.mpinStatus === 'RESET'
          ? 'Change Passcode'
          : 'Drawer'
      }>
      <Stack.Screen
        name="Drawer"
        options={{headerShown: false}}
        component={Drawer}
      />

      {/* All overviews that accessible inside dashboard */}

      <Stack.Screen
        name="CreditScreenOverview"
        options={({navigation, route}) =>
          buildOverviewHeaderOptions({navigation, route, theme}, alias)
        }
        component={CreditScreenOverview}
      />

      <Stack.Screen
        name="DepositScreenOverview"
        component={DepositScreenOverview}
        options={({navigation, route}) =>
          buildOverviewHeaderOptions({navigation, route, theme}, alias)
        }
      />

      <Stack.Screen
        name="PrepaidScreenOverview"
        options={({navigation, route}) =>
          buildOverviewHeaderOptions({navigation, route, theme}, alias)
        }
        component={PrepaidScreenOverview}
      />

      <Stack.Screen
        name="YazzScreenOverview"
        options={({navigation, route}) =>
          buildOverviewHeaderOptions({navigation, route, theme}, alias)
        }
        component={YazzSceenOverview}
      />

      <Stack.Screen
        name="ManageAccountSettings"
        component={ManageAccountSettings}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Enroll Account"
        component={EnrollAccount}
        options={{headerShown: false}}
      />

      <Stack.Screen
        name="Report Lost Card"
        component={ReportLostCard}
        options={{headerShown: false}}
      />

      {/* Common Screens */}
      <Stack.Screen
        name="Processing Screen"
        component={ProcessingScreen}
        options={{animationEnabled: false, headerShown: false}}
      />

      <Stack.Screen
        name="PayBillsCredit"
        component={PayBillsCreditStack}
        options={{headerShown: false, gestureEnabled: false}}
      />

      <Stack.Screen
        name="Installments"
        component={InstallmentStack}
        options={{headerShown: false, gestureEnabled: false}}
      />

      <Stack.Screen
        name="SendMoneyStack"
        component={SendMoneyStack}
        options={{headerShown: false, gestureEnabled: false}}
      />

      <Stack.Screen
        name="ManageContactsStack"
        component={ManageContacts}
        options={{headerShown: false, gestureEnabled: false}}
      />

      <Stack.Screen
        name="Error Screen"
        options={{
          headerBackImage: () => <BackIcon />,
          headerTitle: () => (
            <StyledHeaderText numberOfLines={1}>Error Screen</StyledHeaderText>
          ),
          headerBackTitle: ' ',
          headerTintColor: theme.topAppBar.backgroundColor,
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: theme.topAppBar.backgroundColor,
          },
        }}>
        {({navigation, route}) => (
          <ErrorScreen
            isSystemError={route.params?.isSystemError}
            heading={route.params?.heading}
            details={route.params?.details}
            headingAlignCenter={route.params?.headingAlignCenter}
            primaryButtonTitle="Go Back"
            handlePrimaryButton={() => navigation.goBack()}
          />
        )}
      </Stack.Screen>

      <Stack.Screen
        name="ActivateCardMainStack"
        options={{headerShown: false, header: () => null}}
        component={ActivateCardStack}
      />

      <Stack.Screen
        name="Change Passcode"
        component={ChangePasscode}
        options={passcodeChangeOptions}
      />

      <Stack.Screen
        name={NAVIGATION_PASSCODECHANGED}
        component={PasscodeChanged}
        options={passcodeChangeOptions}
      />

      <Stack.Screen name="Billers" component={BillersStack} />
    </Stack.Navigator>
  );
};

export default (withTheme(MainNavigation): ComponentType<any>);
