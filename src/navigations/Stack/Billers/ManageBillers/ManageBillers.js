/**
 * @format
 * @flow
 */
// React Modules
import React from 'react';

// Screens
import BillerDetailsScreen from '@screens/private/Billers/ManageBillers/BillerDetails/BillerDetailsScreen';
import EditBillerDetailsScreen from '@screens/private/Billers/ManageBillers/EditBillerDetailsScreen/EditBillerDetailsScreen';

// Third party libraries
import {createStackNavigator} from '@react-navigation/stack';

const ManageBillerStack = createStackNavigator();

const ManageBillers = (): React$Node => {
  const screenOptions = {
    headerShown: false,
    gestureEnabled: false,
  };

  return (
    <ManageBillerStack.Navigator screenOptions={screenOptions}>
      <ManageBillerStack.Screen
        name="BillerDetails"
        component={BillerDetailsScreen}
      />

      <ManageBillerStack.Screen
        name="EditBillerDetails"
        component={EditBillerDetailsScreen}
      />
    </ManageBillerStack.Navigator>
  );
};

export default ManageBillers;
