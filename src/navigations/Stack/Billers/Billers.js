/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

// Stacks
import ManageBillersStack from './ManageBillers/ManageBillers';
import BillersTab from './BillerTabs/BillerTabs';

// Types
import type {PropsType} from './types';

const Stack = createStackNavigator();

const BillersStack = (props: PropsType): React$Element<any> => {
  return (
    <Stack.Navigator
      initialRouteName="Billers"
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        gestureEnabled: false,
        headerShown: false,
      }}>
      <Stack.Screen name="Billers" component={BillersTab} />

      <Stack.Screen name="ManageBillersStack" component={ManageBillersStack} />

      {/* <Stack.Screen name="EnrollBillersStack" component={TabsComponent} /> TODO: Enroll biller stack */}
    </Stack.Navigator>
  );
};

export default BillersStack;
