/**
 * @format
 * @flow
 */

export type TabsProps = {
  navigation: Object,
};
