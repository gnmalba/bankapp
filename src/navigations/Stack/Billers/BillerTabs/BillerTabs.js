/**
 * @format
 * @flow
 */
/* eslint-disable react/jsx-no-duplicate-props */
import React from 'react';

// Third party libraries
import {useFocusEffect} from '@react-navigation/native';
import {useDispatch} from 'react-redux';

// Screens
import EnrolledBillersTab from '@screens/private/Billers/BillersTab/EnrolledBillersTab';
import AllBillersTab from '@screens/private/Billers/BillersTab/AllBillersTab';

// Components
import Header from '@components/Header';
import {StyledTabNavigator, TabScreen} from '@components/TabNavigator/Styled';

// Types
import type {TabsProps} from './types';

const BillerTabs = ({navigation}: TabsProps): React$Node => {
  const dispatch = useDispatch();

  useFocusEffect(
    React.useCallback(() => {
      // When the screen is focused
      // if (!enrolledBillers.length) {
      dispatch({type: 'GET_BILLER_REQUEST'});
      // }

      return () => {
        // When the screen is unfocused
        navigation.jumpTo('Enrolled');
      };
    }, [dispatch, navigation]),
  );

  const handleBackClick = () => navigation.navigate('Dashboard');

  return (
    <>
      <Header isBackButton title="Billers" onBackClick={handleBackClick} />

      <StyledTabNavigator>
        <TabScreen
          name="Enrolled"
          component={EnrolledBillersTab}
          component={() => <EnrolledBillersTab navigation={navigation} />}
        />

        <TabScreen name="All Billers" component={AllBillersTab} />
      </StyledTabNavigator>
    </>
  );
};

export default BillerTabs;
