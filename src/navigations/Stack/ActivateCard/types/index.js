/**
 * @flow
 */

export type PropsType = {};

export type navigationOptionType = {
  headerTitleAllowFontScaling: boolean,
  headerBackTitleVisible: boolean,
  headerLeft: (props: Object) => React$Element<any>,
  headerStyle: Object,
  headerTitleAlign: string,
  headerTitleStyle: Object,
  headerRightContainerStyle: Object,
  cardStyle: Object,
};

export type LoginNavProps = {
  theme: Object,
};
