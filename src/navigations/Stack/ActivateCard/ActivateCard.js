/**
 * @format
 * @flow
 */

import React from 'react';

import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

import ActivateCardList from '@screens/private/ActivateCardList';
import ActivateCardConfirmation from '@screens/private/ActivateCardConfirmation';
import ActivateCardPasscode from '@screens/private/ActivateCardPasscode';
import ActivateCardSuccess from '@screens/private/ActivateCardSuccess';
import ErrorScreen from '@screens/common/ErrorScreen';

import Button from '@components/Button';

import Header from '@components/Header';

import type {PropsType} from './types';

const Stack = createStackNavigator();

const PasscodeExceedErrorScreen = ({navigation}) => {
  return (
    <ErrorScreen
      headingAlignCenter
      heading="You have reached the maximum no. of attempts. Please try again after(x) minutes."
      buttonContents={
        <Button
          title="Go Back"
          type="primary"
          state="default"
          onPress={() => navigation.navigate('Dashboard')}
        />
      }
    />
  );
};

const ActivationFailed = ({navigation}) => {
  return (
    <ErrorScreen
      isSystemError
      primaryButtonTitle="Try Again"
      handlePrimaryButton={() => navigation.navigate('ActivateCardList')}
      handleSecondaryButton={() => navigation.navigate('Dashboard')}
    />
  );
};

const DEFAULT_TITLE = 'Activate Card';

const CustomHeader = ({title = DEFAULT_TITLE, navigation = false}) => (
  <Header
    title={title}
    {...(navigation && {
      onBackClick: () => navigation.goBack(),
    })}
  />
);

const ActivateCardStack = (props: PropsType): React$Element<any> => {
  return (
    <Stack.Navigator
      initialRouteName="ActivateCardList"
      screenOptions={{
        header: ({navigation}) => <CustomHeader navigation={navigation} />,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen name="ActivateCardList" component={ActivateCardList} />
      <Stack.Screen
        name="ActivateCardConfirmation"
        component={ActivateCardConfirmation}
      />
      <Stack.Screen
        name="ActivateCardPasscode"
        options={{
          header: ({navigation}) => (
            <CustomHeader title="Verification" navigation={navigation} />
          ),
        }}
        component={ActivateCardPasscode}
      />
      <Stack.Screen
        name="ActivateCardPasscodeExceeds"
        options={{
          headerLeft: () => null,
          header: () => <CustomHeader title="Verification" />,
        }}
        component={PasscodeExceedErrorScreen}
      />
      <Stack.Screen
        name="ActivateCardSuccess"
        component={ActivateCardSuccess}
        options={{
          header: ({navigation}) => <CustomHeader />,
        }}
      />
      <Stack.Screen
        name="ActivationFailed"
        options={{
          header: () => <CustomHeader title="Error" />,
        }}
        component={ActivationFailed}
      />
    </Stack.Navigator>
  );
};

export default ActivateCardStack;
