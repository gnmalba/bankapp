import React from 'react';
import {render} from '@testing-library/react-native';
import {InstallmentStack} from '../index';
import {ThemeProvider} from 'styled-components/native';
import {NavigationContainer} from '@react-navigation/native';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

jest.mock('@react-navigation/native', () => {
  const nav = jest.requireActual('@react-navigation/native');
  nav.useNavigation = () => jest.fn();
  return nav;
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

describe('Installments', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const navigation = {navigate: jest.fn()};

    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{
            installments: {
              currentInstallmentType: '',
              formLoading: false,
              showReviewScreen: false,
              information: {
                data: [],
                loading: false,
                visible: false,
                isInitial: false,
              },
              balanceTransfer: {
                transferFrom: {},
                transferTo: {},
                paymentTerms: {},
                amount: {},
              },
            },

            credit: {
              data: [
                {
                  cardSerNo: '',
                },
              ],
              creditList: {data: []},
              // creditList: [
              //   {
              //     cardSerNo: '',
              //   },
              // ],
            },
          }}>
          <ThemeProvider theme={DARK_THEME}>
            <InstallmentStack
              // {...testProps('activateCardStackId')}
              navigation={navigation}
            />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
