/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  CardStyleInterpolators,
  createStackNavigator,
  HeaderStyleInterpolators,
} from '@react-navigation/stack';

import Header from '@components/Header';
import {InfoIcon} from '@components/Header/Styled';

import BTScreen from '@screens/private/BalanceTransfer';
import BCScreen from '@screens/private/BalanceConversion';
import C2GOScreen from '@screens/private/Cash2Go';
import ConfirmDetailsScreen from '@screens/private/ConfirmDetailsScreen';
import PaymentTermsScreen from '@screens/private/PaymentTermsScreen';
import SelectCreditCardScreen from '@screens/private/SelectCreditCardScreen';
import SelectCreditCardWithSearchScreen from '@screens/private/SelectCreditCardWithSearchScreen';
import ReviewDetailsScreen from '@screens/private/ReviewDetailsScreen';
import InstallmentsPasscodeScreen from '@screens/private/InstallmentsPasscodeScreen';
import SelectBankAccountScreen from '@screens/private/SelectBankAccountScreen';
import ViewInstallmentsScreen from '@screens/private/ViewInstallmentsScreen';
import BTDetails from '@screens/private/BalanceTransfer/components/Details';
import BCDetails from '@screens/private/BalanceConversion/components/Details';
import C2GODetails from '@screens/private/Cash2Go/components/Details';
import TermsCondition from '@screens/common/TermsCondition';

import InformationModal from '@composition/InformationModal';
import {testProps} from '@utils/componentCustomProps';

import type {InstallmentStateType} from '@ducks/installments/types';
import MaximumPasscodeScreen from '@screens/private/InstallmentsPasscodeScreen/components/MaximumPasscodeScreen';
import {
  handleGoBack,
  INSTALLMENT_CODES_TITLE,
  MAIN_SCREENS,
} from '@utils/installments';
import InstallmentLeavePromptModal from '@composition/InstallmentLeavePromptModal';
import {setInformationModalVisible} from '@ducks/installments/actions';

const Stack = createStackNavigator();

const HeaderInfoIcon = ({
  onPress,
  hidden,
}: {
  onPress: Function,
  hidden: boolean,
}): React$Node => {
  if (hidden) {
    return <></>;
  }

  return (
    <TouchableOpacity {...testProps('informationTestId')} onPress={onPress}>
      <InfoIcon />
    </TouchableOpacity>
  );
};

export const InstallmentStack: any = ({navigation}: any) => {
  const PasscodeScreen = () => {
    return <InstallmentsPasscodeScreen navigation={navigation} />;
  };

  const installmentsState: InstallmentStateType = useSelector(
    (state) => state.installments,
  );
  const {
    currentInstallmentType,
    formLoading,
    showReviewScreen,
  } = installmentsState;
  const dispatch = useDispatch();
  const installmentTitle = INSTALLMENT_CODES_TITLE[currentInstallmentType];

  const handleOnPressInformation = () => {
    dispatch(setInformationModalVisible(true));
  };

  return (
    <>
      <Stack.Navigator
        {...testProps('installmentMainNav')}
        screenOptions={{
          headerShown: true,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
          headerStyleInterpolator: HeaderStyleInterpolators.forNoAnimation,
          gestureEnabled: false,
          header: (props) => {
            const headerTitle = props.scene.descriptor.options?.headerTitle;
            const titleDisplay = headerTitle === '' ? '' : headerTitle;
            const isMainScreen = MAIN_SCREENS.includes(props.scene.route.name);

            if (showReviewScreen || formLoading) {
              return null;
            }

            return (
              <Header
                showLogoOnCenter={false}
                showLogout={false}
                onBackClick={() =>
                  handleGoBack({
                    navigation: props.navigation,
                    route: props.scene.route,
                    installmentsState,
                    dispatch,
                  })
                }
                title={titleDisplay}
                rightComponent={
                  <HeaderInfoIcon
                    onPress={handleOnPressInformation}
                    hidden={!isMainScreen}
                  />
                }
              />
            );
          },
        }}>
        <Stack.Screen
          name="BalanceTransferScreen"
          component={BTScreen}
          options={{headerTitle: 'Balance Transfer'}}
          initialParams={{navigation}}
        />
        <Stack.Screen
          name="BalanceConversionScreen"
          component={BCScreen}
          options={{headerTitle: 'Balance Conversion'}}
          initialParams={{navigation}}
        />
        <Stack.Screen
          name="Cash2GoScreen"
          component={C2GOScreen}
          options={{
            headerTitle: 'Cash2Go',
          }}
          initialParams={{navigation}}
        />
        <Stack.Screen
          name="PaymentTermsScreen"
          component={PaymentTermsScreen}
          options={{headerTitle: installmentTitle}}
        />
        <Stack.Screen
          name="ConfirmScreen"
          component={ConfirmDetailsScreen}
          options={{headerTitle: 'Confirm Details'}}
        />
        <Stack.Screen
          name="PasscodeScreen"
          component={PasscodeScreen}
          options={{headerTitle: 'Verification', headerShown: !formLoading}}
        />
        <Stack.Screen
          name="MaximumPasscodeScreen"
          component={MaximumPasscodeScreen}
          options={{headerTitle: 'Verification'}}
        />

        <Stack.Screen
          name="ReviewScreen"
          component={ReviewDetailsScreen}
          options={{
            headerTitle: installmentTitle,
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ViewInstallmentsScreen"
          component={ViewInstallmentsScreen}
          options={{headerTitle: 'Card Installment Status'}}
          initialParams={{navigation}}
        />

        <Stack.Screen
          name="SelectCardScreen"
          options={{headerTitle: installmentTitle}}
          component={SelectCreditCardWithSearchScreen}
        />

        {/* balance transfer */}
        <Stack.Screen
          name="SelectSourceCardScreen"
          component={SelectCreditCardScreen}
          options={{headerTitle: installmentTitle}}
        />
        <Stack.Screen
          name="BTDetailsScreen"
          component={BTDetails}
          options={{headerTitle: 'Balance Transfer'}}
        />

        {/* Cash 2 Go */}
        <Stack.Screen
          name="SelectBankAccountScreen"
          component={SelectBankAccountScreen}
          options={{headerTitle: installmentTitle}}
        />
        <Stack.Screen
          name="C2GODetailsScreen"
          component={C2GODetails}
          options={{headerTitle: 'Cash2Go'}}
        />

        {/* Balance Conversion */}
        <Stack.Screen
          name="BCDetailsScreen"
          component={BCDetails}
          options={{headerTitle: 'Balance Conversion'}}
        />
        <Stack.Screen
          name="TermsConditionScreen"
          component={TermsCondition}
          options={{headerTitle: 'Terms and Conditions'}}
        />
      </Stack.Navigator>

      {/* Re-usable modals for installments */}
      <InformationModal />

      <InstallmentLeavePromptModal />
    </>
  );
};
