import React from 'react';
import {render} from '@testing-library/react-native';
import AccountSettings from '../AccountSettings';
import {ThemeProvider} from 'styled-components/native';
import {NavigationContainer} from '@react-navigation/native';

import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

describe('AccountSettings', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{manageSettings: {isAppearanceSettingSuccess: false}}}>
          <ThemeProvider theme={DARK_THEME}>
            <AccountSettings />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
