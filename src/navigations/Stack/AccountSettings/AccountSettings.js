/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

// Components
import Header from '@components/Header';

import type {PropsType} from './types';
import AccountSettings from '@screens/private/AccountSettings';
import AccountSettingsLoginSettings from '@screens/private/AccountSettingsLoginSettings';
import ManageDevices from '@screens/private/ManageDevices';
import AppearanceSettings from '@screens/private/AppearanceSettings';
import AccountSettingsChangePassword from '@screens/private/AccountSettingsChangePassword';
import AccountSettingsPasscodeScreen from '@screens/private/AccountSettingsVerification';
import AccountSettingsNewPassword from '@screens/private/AccountSettingsChangePassword/AccountSettingsNewPassword';
import AccountSettingsChangePasscode from '@screens/private/AccountSettingsChangePasscode';
import AccountSettingsNewPasscode from '@screens/private/AccountSettingsChangePasscode/NewPasscodeScreen';
import AccountSettingsSuccessScreen from '@screens/private/AccountSettingsSuccessScreen';

const Stack = createStackNavigator();

const AccountSettingsStack = (props: PropsType): React$Element<any> => {
  const header = (title: string = 'Account Settings') => (
    <Header
      title={title}
      isBackButton
      onBackClick={() => props.navigation.pop()}
    />
  );

  const appearanceSettingsHeader = (title: string) => (
    <Header
      title={title}
      isBackButton
      onBackClick={() => {
        props.navigation.navigate('AccountSettings', {
          hasSavedAppearance: false,
        });
      }}
    />
  );

  return (
    <Stack.Navigator
      initialRouteName="AccountSettings"
      screenOptions={{
        headerShown: true,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        gestureEnabled: false,
      }}>
      <Stack.Screen
        name="AccountSettings"
        options={{
          header: () => (
            <Header
              title={'Account Settings'}
              isBackButton={true}
              onBackClick={() => props.navigation.goBack()}
            />
          ),
        }}>
        {(screenProps) => <AccountSettings {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="AccountSettingsChangePassword"
        options={{
          header: () => header('Change Password'),
        }}>
        {(screenProps) => <AccountSettingsChangePassword {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="AccountSettingsNewPassword"
        options={{
          header: () => header('Change Password'),
        }}>
        {(screenProps) => <AccountSettingsNewPassword {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="ChangePasscodeScreen"
        options={{
          header: () => header('Change Passcode'),
        }}>
        {(screenProps) => <AccountSettingsChangePasscode {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="NewPasscodeScreen"
        options={{
          header: () => header('Change Passcode'),
        }}>
        {(screenProps) => <AccountSettingsNewPasscode {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="AccountSettingsPasscode"
        options={{
          header: () => header('Verification'),
        }}>
        {(screenProps) => <AccountSettingsPasscodeScreen {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="LoginSettingsScreen"
        options={{
          header: () => header('Login Settings'),
        }}>
        {(screenProps) => <AccountSettingsLoginSettings {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="AccountSettingsSuccessScreenPassword"
        options={{
          header: () => (
            <Header title="Change Password" leftComponent={() => null} />
          ),
        }}>
        {(screenProps) => <AccountSettingsSuccessScreen {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="AccountSettingsSuccessScreenPasscode"
        options={{
          header: () => (
            <Header title="Change Passcode" leftComponent={() => null} />
          ),
        }}>
        {(screenProps) => (
          <AccountSettingsSuccessScreen {...screenProps} isPasscode />
        )}
      </Stack.Screen>
      <Stack.Screen
        name="ManageDevicesScreen"
        options={{header: () => header('Manage Devices')}}>
        {() => <ManageDevices />}
      </Stack.Screen>
      <Stack.Screen
        name="AppearanceSettingsScreen"
        options={{
          header: () => appearanceSettingsHeader('Appearance Settings'),
        }}>
        {(screenProps) => <AppearanceSettings {...screenProps} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};

export default AccountSettingsStack;
