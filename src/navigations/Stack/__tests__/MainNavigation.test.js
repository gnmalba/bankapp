import React from 'react';
import {render} from '@testing-library/react-native';
import {NavigationContainer} from '@react-navigation/native';
import {ThemeProvider} from 'styled-components/native';
import MainNavigation from '../MainNavigation';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';
jest.mock('@react-navigation/native', () => {
  const nav = jest.requireActual('@react-navigation/native');
  nav.useNavigation = () => jest.fn();
  return nav;
});

describe('MainNavigation', () => {
  const mockAppState = {
    manageAccountSettings: {
      selectedAccount: {
        alias: 'TestAlias',
      },
    },
    biometrics: {
      registerBiometrics: false,
    },
    dashboard: {
      isQuickLinks: false,
    },
    config: {
      theme: 'dark',
      widgets: [
        {
          ref: 1,
          label: 'Test Label A',
          loading: false,
        },
        {
          ref: 2,
          label: 'Test Label B',
          loading: false,
        },
      ],
      removedWidgets: [1, 2],
      checked: [],
      limitShown: false,
      customizedWidgets: [1, 2],
    },
    authentication: {
      data: {},
      showLogoutModal: false,
      anotherSessionClosed: false,
      loading: false,
    },
    credit: {
      loading: false,
      data: [],
      error: false,
      errors: [],
    },
    soa: {
      soaLastestDetails: '',
    },
    recoverAccess: {
      showUsernameSentSnackbar: false,
    },
  };

  it('Should work as expected', () => {
    const store = {
      ...mockAppState,
    };

    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <NavigationContainer>
          <MockProvider store={store}>
            <MainNavigation />
          </MockProvider>
        </NavigationContainer>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it.each([
    {
      data: {
        mpinRegistered: false,
        mpinStatus: 'RESET',
      },
    },
    {data: {}},
  ])('should render correctly when %o', (additionalState) => {
    const store = {
      ...mockAppState,
      authentication: {
        ...mockAppState.authentication,
        ...additionalState,
      },
    };

    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <NavigationContainer>
          <MockProvider store={store}>
            <MainNavigation />
          </MockProvider>
        </NavigationContainer>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
