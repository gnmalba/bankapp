import React from 'react';
import {render} from '@testing-library/react-native';
import {NavigationContainer} from '@react-navigation/native';
import {ThemeProvider} from 'styled-components/native';
import EnrollAccount from '../EnrollAccount';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

import {ERROR_CODES} from '@screens/private/EnrollAccount/constants';
describe('EnrollAccount', () => {
  const mockAppState = {
    enrollment: {
      yazzCardBins: [],
      apiResponse: {},
      loading: false,
      showProcessingScreen: false,
      invalidOTPRetries: 0,
    },
  };

  const navigation = {
    navigate: jest.fn(() => {}),
  };

  it('Should work as expected', () => {
    const store = {
      ...mockAppState,
    };

    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <NavigationContainer>
          <MockProvider store={store}>
            <EnrollAccount navigation={navigation} />
          </MockProvider>
        </NavigationContainer>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
  it('correct api response should work as expected', () => {
    const store = {
      ...mockAppState,
      enrollment: {
        ...mockAppState.enrollment,
        apiResponse: {
          errorCode: ERROR_CODES.CPC.GENERAL_ERROR,
          message: ERROR_CODES.CPC.MAXIMUM_OTP_ATTEMPTS_MESSAGE,
        },
      },
    };

    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <NavigationContainer>
          <MockProvider store={store}>
            <EnrollAccount navigation={navigation} />
          </MockProvider>
        </NavigationContainer>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
