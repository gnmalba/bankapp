/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

// Screens
import EnrollAccount from '@screens/private/EnrollAccount';
import EnrollAccountOtp from '@screens/private/EnrollAccountOtp';
import EnrollAccountSuccess from '@screens/private/EnrollAccountSuccess';
import EnrollAccountError from '@screens/private/EnrollAccountError';

// Components
import Header from '@components/Header';

// Types
import type {PropsType} from './types';

// Utils
import {
  NAVIGATION_ENROLLMENT_FORM,
  NAVIGATION_ENROLLMENT_OTP,
  NAVIGATION_ENROLLMENT_SUCCESS,
  NAVIGATION_ENROLLMENT_ERROR,
} from '@utils/navigationRoutes';
import {
  ERROR_CODES,
  INVALID_OTP_ATTEMPTS,
} from '@screens/private/EnrollAccount/constants';
import {testProps} from '@utils/componentCustomProps';
const Stack = createStackNavigator();

const EnrollAccountStack = (props: PropsType): React$Element<any> => {
  const enrollment = useSelector((state) => state.enrollment);
  const {apiResponse, invalidOTPRetries} = enrollment;
  const cpcMaxAttemptOTP = !!(
    apiResponse?.errorCode === ERROR_CODES.CPC.GENERAL_ERROR &&
    apiResponse?.message.includes(ERROR_CODES.CPC.MAXIMUM_OTP_ATTEMPTS_MESSAGE)
  );

  return (
    <Stack.Navigator
      initialRouteName={NAVIGATION_ENROLLMENT_FORM}
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen
        name={NAVIGATION_ENROLLMENT_FORM}
        component={EnrollAccount}
        options={{
          header: () => (
            <Header
              title="Enroll Account"
              onBackClick={() => props.navigation.navigate('deposit')}
              isBackButton={true}
              {...testProps('enrollAccountEnrollForm')}
            />
          ),
        }}
      />
      <Stack.Screen
        name={NAVIGATION_ENROLLMENT_OTP}
        component={EnrollAccountOtp}
        options={{
          header: () => (
            <Header
              title="Verification"
              onBackClick={() =>
                props.navigation.navigate(NAVIGATION_ENROLLMENT_FORM)
              }
              isBackButton={true}
            />
          ),
        }}
      />
      <Stack.Screen
        name={NAVIGATION_ENROLLMENT_SUCCESS}
        component={EnrollAccountSuccess}
        options={{
          header: () => (
            <Header title="Enroll Account" leftComponent={() => null} />
          ),
        }}
      />
      <Stack.Screen
        name={NAVIGATION_ENROLLMENT_ERROR}
        component={EnrollAccountError}
        options={{
          header: () => (
            <Header
              title="Enroll Account"
              onBackClick={
                invalidOTPRetries !== INVALID_OTP_ATTEMPTS && !cpcMaxAttemptOTP
                  ? () => props.navigation.navigate(NAVIGATION_ENROLLMENT_FORM)
                  : null
              }
              isBackButton={true}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default EnrollAccountStack;
