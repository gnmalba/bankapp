/**
 * @flow
 */

import {StyleSheet} from 'react-native';

type StylesType = {
  headerStyle: Object,
};

const Styles: StylesType = StyleSheet.create({
  headerStyle: {
    elevation: 0.1,
    shadowOpacity: 0.1,
  },
});

export default Styles;
