// @flow

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

// Components
import Header from '@components/Header';
import Accounts from '@screens/private/Accounts';

import SupplementaryScreen from '@screens/private/SupplementaryScreen';
import StatementOfAccountScreen from '@screens/private/StatementOfAccountScreen';
import ManageAccountSettings from '@screens/private/ManageAccountSettings';
const Stack = createStackNavigator();

const MyAccounts = (props: any): React$Element<any> => {
  const {alias} = useSelector(
    (state) => state.manageAccountSettings.selectedAccount,
  );

  const overviewNavigate = () => {
    props.navigation.navigate('CreditScreenOverview', {
      id: props.route.params.params.primaryId,
      alias: alias,
      fromSuppleOverview: true,
    });
  };

  const handleGoBack = (route: any) => {
    switch (route.name) {
      case 'ManageAccountSettings':
        props.navigation.pop();
        break;

      case 'My Accounts':
        props.navigation.reset({
          index: 0,
          key: null,
          routes: [{name: 'Dashboard'}],
        });
        break;

      default:
        props.navigation.goBack();
        overviewNavigate();
        break;
    }
  };

  return (
    <Stack.Navigator
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen
        name="My Accounts"
        component={Accounts}
        options={{
          header: () => (
            <Header
              title="My Accounts"
              onBackClick={() => props.navigation.openDrawer()}
              isBackButton={false}
            />
          ),
        }}
        initialParams={{handleGoBack}}
      />

      <Stack.Screen
        name="SupplementaryScreen"
        component={SupplementaryScreen}
        options={{
          header: ({scene}) => (
            <Header
              title={props.route?.params?.supplementary?.alias}
              isBackButton={true}
              onBackClick={() => {
                handleGoBack(scene.route);
              }}
            />
          ),
        }}
        initialParams={{handleGoBack}}
      />

      <Stack.Screen
        name="StatementOfAccountScreen"
        component={StatementOfAccountScreen}
        options={{
          header: ({scene}) => (
            <Header
              title="Statement of Account"
              isBackButton={true}
              onBackClick={() => {
                handleGoBack(scene.route);
              }}
            />
          ),
        }}
        initialParams={{handleGoBack}}
      />

      <Stack.Screen
        name="ManageAccountSettings"
        component={ManageAccountSettings}
        options={{
          header: ({scene}) => (
            <Header
              isBackButton
              alias={alias}
              title="My Accounts"
              onBackClick={() => handleGoBack(scene.route)}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default MyAccounts;
