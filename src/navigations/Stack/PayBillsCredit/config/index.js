/**
 * @flow
 */

/**
 * Config file for the Stack Navigator of PayBillsCredit
 * contains the screens/routes of the stack
 * and other default options
 */

// 3rd Party Modules
import {CardStyleInterpolators} from '@react-navigation/stack';

// Screens

import MainScreen, {
  SourceAccountScreen,
  BillersScreen,
  ConfirmScreen,
  PassCodeScreen,
  EndOfTransactionScreen,
  LoadingScreen,
  MaximumPasscodeScreen,
} from '@screens/private/PayBillsCredit';

export const defaultScreenOptions = {
  headerShown: true,
  cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
};

export const screens = [
  {
    name: 'MainScreen',
    options: null,
    ScreenComponent: MainScreen,
  },
  {
    name: 'SourceAccountScreen',
    options: {
      gestureEnabled: false,
    },
    ScreenComponent: SourceAccountScreen,
  },
  {
    name: 'BillersScreen',
    options: null,
    ScreenComponent: BillersScreen,
  },
  {
    name: 'ConfirmScreen',
    options: null,
    ScreenComponent: ConfirmScreen,
  },
  {
    name: 'EndOfTransactionScreen',
    options: null,
    ScreenComponent: EndOfTransactionScreen,
  },
  {
    name: 'PassCodeScreen',
    options: null,
    ScreenComponent: PassCodeScreen,
  },
  {
    name: 'LoadingScreen',
    options: {
      headerShown: false,
    },
    ScreenComponent: LoadingScreen,
  },
  {
    name: 'MaximumPasscodeScreen',
    options: {
      headerShown: false,
      // headerTitle: 'Verification'
    },
    ScreenComponent: MaximumPasscodeScreen,
  },
];
