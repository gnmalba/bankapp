/**
 * @flow
 */

// React, React Native modules
import React, {useEffect, useMemo} from 'react';

// 3rd Party Modules
import {
  useNavigationState,
  useNavigation,
  useRoute,
  getFocusedRouteNameFromRoute,
} from '@react-navigation/core';
import {createStackNavigator} from '@react-navigation/stack';

// Hooks
import {
  useBackButton,
  useLeavePrompt,
  useFormControl,
  usePayBillsCredit,
} from '@screens/private/PayBillsCredit/hooks';

// Config
import {defaultScreenOptions, screens} from './config';

// Components
import Header from '@components/Header';
import {LeavePromptModal} from '@screens/private/PayBillsCredit/components';

// Component Style
// import {} from './Styled';

const Stack = createStackNavigator();

const header = (title, backPress, isBackButton) => (
  <Header title={title} onBackClick={backPress} isBackButton={isBackButton} />
);

function PayBillsCreditStack(): React$Node {
  const {
    selectedBiller,
    selectedSourceAccount,
    isPayCredit,
  } = usePayBillsCredit();

  const {
    isLeavePromptVisible,
    openLeavePrompt,
    closeLeavePrompt,
  } = useLeavePrompt();

  const {isFormDirty, formControl, updateFormControl} = useFormControl({
    selectedSourceAccount,
    selectedBiller,
  });

  const [handleBackPress] = useBackButton({
    isFormDirty,
    formControl,
    openLeavePrompt,
    closeLeavePrompt,
    isLeavePromptVisible,
  });

  const navigatorType = useNavigationState((state) => state.type);
  const navigation = useNavigation();
  const route = useRoute();

  const headerShown = useMemo(() => {
    return navigatorType !== 'tab';
  }, [navigatorType]);

  const title = isPayCredit ? 'Pay Credit Card' : 'Pay Bills';
  useEffect(() => {
    if (navigatorType === 'tab') {
      const currentRoute = getFocusedRouteNameFromRoute(route) ?? 'MainScreen';

      if (currentRoute === 'MainScreen') {
        navigation.setOptions({
          tabBarVisible: true,
        });
      } else {
        navigation.setOptions({
          tabBarVisible: false,
        });
      }
    }
  }, [navigatorType, navigation, route]);

  return (
    <>
      <Stack.Navigator
        initialRouteName="MainScreen"
        screenOptions={defaultScreenOptions}>
        {screens.map((screen, index) => (
          <Stack.Screen // map screens config into navigation routes
            key={index}
            name={screen.name}
            options={{
              ...screen.options,
              headerShown,
              header: () => header(title, handleBackPress, true),
            }}>
            {(props) => (
              <screen.ScreenComponent
                {...props}
                formControl={formControl}
                updateFormControl={updateFormControl}
              />
            )}
          </Stack.Screen>
        ))}
      </Stack.Navigator>
      <LeavePromptModal
        isVisible={isLeavePromptVisible}
        handlePressGoBack={handleBackPress}
        handlePressCancel={closeLeavePrompt}
      />
    </>
  );
}

export default PayBillsCreditStack;
