/**
 * @flow
 */

export type StackProps = {
  navigation: Object,
};
