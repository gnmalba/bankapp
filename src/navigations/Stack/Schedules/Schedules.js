/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {
  createStackNavigator,
  CardStyleInterpolators,
} from '@react-navigation/stack';

// Component
import Header from '@components/Header';

// Screens
import ViewSchedule from '@screens/private/ManageSchedules/ViewSchedule';
import Schedules from '@screens/private/ManageSchedules/Schedules';
import EditSchedule from '@screens/private/ManageSchedules/EditSchedule';

// Types
import type {ManageSchedulesNavProps} from './types';

const Stack = createStackNavigator();

const EnrollAccountStack = (
  props: ManageSchedulesNavProps,
): React$Element<any> => {
  return (
    <Stack.Navigator
      initialRouteName="Schedules"
      screenOptions={{
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      <Stack.Screen
        name="Schedules"
        component={Schedules}
        options={{
          header: () => (
            <Header
              title={'Schedules'}
              onBackClick={() => props.navigation.openDrawer()}
              isBackButton={false}
            />
          ),
        }}
      />
      <Stack.Screen
        name="ViewSchedule"
        component={ViewSchedule}
        options={{
          header: () => (
            <Header title={'Schedule Details'} isBackButton={true} />
          ),
        }}
      />
      <Stack.Screen
        name="EditSchedule"
        component={EditSchedule}
        options={{
          header: () => <Header title={'Edit Schedule'} isBackButton={true} />,
        }}
      />
    </Stack.Navigator>
  );
};

export default EnrollAccountStack;
