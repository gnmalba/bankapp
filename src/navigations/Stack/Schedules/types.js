/**
 * @flow
 */

export type ManageSchedulesNavProps = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: Object,
};
