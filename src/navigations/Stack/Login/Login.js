// @flow

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {withTheme} from 'styled-components';
import {useSelector, useDispatch} from 'react-redux';

import AdvisoryIcon from '@components/AdvisoryIcon';

import LoginScreen from '@screens/public/LoginScreen';
import DeviceSecurityAlert from '@screens/public/DeviceSecurityAlert';
import SlowInternetAlert from '@screens/public/SlowInternetAlert';
import LoginOtpVerification from '@screens/public/LoginOtpVerification';
import FullPageError from '@screens/public/FullPageError';
import BiometricsRegistration from '@screens/public/BiometricsRegistration';
import OnboardingScreen from '@screens/public/OnboardingScreen';
import Advisory from '@screens/public/Advisory';
import AdvisoryDetail from '@screens/public/AdvisoryDetail';

import {RECOVER_ACCESS_FAILED} from '@ducks/recover-access/actionTypes';
import {
  RecoverAccessScreen,
  EnterUsernameEmail,
  EnterPasscode,
  EnterEmail,
  EnterOTP,
  EnterPassword,
  PasswordChanged,
  MaximumInvalid,
} from '@screens/public/RecoverAccess';
import {
  NAVIGATION_ENTERUSERNAMEEMAIL,
  NAVIGATION_ENTERPASSCODE,
  VERIFICATION,
  NAVIGATION_ENTEREMAIL,
  NAVIGATION_ENTEROTP,
  NAVIGATION_ENTERPASSWORD,
  CREATE_NEW_PASSWORD,
} from '@utils/constants';

import IconButton from '@components/IconButton';
import Text from '@components/Text';
import {navigationOption} from './config';
import Styles from './styles';
import type {LoginNavProps} from './types';
import {testProps} from '@utils/componentCustomProps';

const Stack = createStackNavigator();

const Login = (props: LoginNavProps): React$Element<any> => {
  const authentication = useSelector((state) => state.authentication);
  const dispatch = useDispatch();
  const source = require('@assets/icons/notification-bell.png');
  const {theme} = props;
  const headerStyle = {
    ...navigationOption.headerStyle,
    ...Styles.headerStyle,
    backgroundColor: theme.topAppBar.backgroundColor,
  };
  const headerTitleStyle = {
    ...navigationOption.headerTitleStyle,
    color: theme.topAppBar.textColor,
  };

  const commonHeaderRight = (navigation) => (
    <AdvisoryIcon
      onPress={() => {
        navigation.navigate('Advisory');
      }}
    />
  );

  return (
    <Stack.Navigator
      initialRouteName={authentication.onboarded ? 'Login' : 'Onboarding'}>
      <Stack.Screen
        name="Login"
        options={({navigation}) => ({
          ...navigationOption,
          headerTitle: () => (
            <Text
              mode="h3"
              numberOfLines={1}
              {...testProps('loginHeaderTitle')}>
              {'Login'}
            </Text>
          ),
          headerRight: () => commonHeaderRight(navigation),
          headerLeft: () => {},
          headerStyle,
          headerTitleStyle,
          swipeEnabled: false,
        })}>
        {(screenProps) => <LoginScreen theme={theme} {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="DeviceSecurityAlert"
        options={({navigation}) => ({
          ...navigationOption,
          headerRight: () => commonHeaderRight(navigation),
          headerLeft: () => {},
          headerStyle,
          headerTitleStyle,
          title: '',
        })}>
        {(screenProps) => (
          <DeviceSecurityAlert theme={theme} {...screenProps} />
        )}
      </Stack.Screen>
      <Stack.Screen
        name="SlowInternetAlert"
        options={{
          ...navigationOption,
          headerRight: (navProps) => (
            <IconButton source={source} {...navProps} />
          ),
          headerLeft: () => {},
          headerStyle,
          headerTitleStyle,
          title: '',
        }}>
        {(screenProps) => <SlowInternetAlert theme={theme} {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="Verification"
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
        }}>
        {(screenProps) => <LoginOtpVerification {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="FullPageError"
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Error',
        }}>
        {(screenProps) => <FullPageError {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="BiometricsRegistration"
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Login',
        }}>
        {(screenProps) => <BiometricsRegistration {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen name="Onboarding" options={{headerShown: false}}>
        {(screenProps) => <OnboardingScreen theme={theme} {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="Advisory"
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Notifications',
        }}>
        {(screenProps) => <Advisory />}
      </Stack.Screen>
      <Stack.Screen
        name="AdvisoryDetail"
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Announcement',
        }}>
        {(screenProps) => <AdvisoryDetail {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen
        name="Recover Access"
        options={{...navigationOption, headerStyle, headerTitleStyle}}
        component={RecoverAccessScreen}
      />
      <Stack.Screen
        name={NAVIGATION_ENTERUSERNAMEEMAIL}
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Forgot password',
        }}
        component={EnterUsernameEmail}
      />
      <Stack.Screen
        name={NAVIGATION_ENTERPASSCODE}
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: VERIFICATION,
        }}
        component={EnterPasscode}
      />
      <Stack.Screen
        name={NAVIGATION_ENTEREMAIL}
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Forgot username',
        }}
        component={EnterEmail}
      />
      <Stack.Screen
        name={NAVIGATION_ENTEROTP}
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: VERIFICATION,
          headerLeft: (headerProps) => (
            <IconButton
              source={require('@assets/icons/back-icon.png')}
              onPress={() => {
                dispatch({
                  type: RECOVER_ACCESS_FAILED,
                  proceedToOtp: true,
                  invalidOtp: true,
                  error: '',
                  clearMpin: false,
                });
                headerProps.onPress();
              }}
            />
          ),
        }}
        component={EnterOTP}
      />
      <Stack.Screen
        name={NAVIGATION_ENTERPASSWORD}
        options={{
          ...navigationOption,
          headerLeft: () => {},
          headerStyle,
          headerTitleStyle,
          title: CREATE_NEW_PASSWORD,
        }}
        component={EnterPassword}
      />
      <Stack.Screen
        name="PasswordChanged"
        options={{
          ...navigationOption,
          headerLeft: () => {},
          headerStyle,
          headerTitleStyle,
          title: CREATE_NEW_PASSWORD,
        }}
        component={PasswordChanged}
      />
      <Stack.Screen
        name="MaximumInvalid"
        options={{
          ...navigationOption,
          headerStyle,
          headerTitleStyle,
          title: 'Verification',
        }}
        component={MaximumInvalid}
      />
    </Stack.Navigator>
  );
};

export default (withTheme(Login): any);
