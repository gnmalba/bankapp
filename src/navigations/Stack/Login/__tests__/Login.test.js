import React from 'react';
import {render} from '@testing-library/react-native';
import Login from '../Login';
import {NavigationContainer} from '@react-navigation/native';
import {DARK_THEME} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

import MockProvider from '@utils/testFramework';
const mockStore = (err) => ({
  authentication: {
    username: err,
    password: err,
    usernameError: err,
    passwordError: err,
  },
  biometrics: {
    deviceSensor: 'Fingerprint',
  },
});
jest.mock('@react-native-community/netinfo', () => ({
  useNetInfo: () => ({
    details: {
      strength: 100,
    },
    isInternetReachable: true,
  }),
}));

describe('Login', () => {
  it('Should work as expected', () => {
    const all = render(
      <NavigationContainer>
        <MockProvider store={mockStore}>
          <Login theme={DARK_THEME} {...testProps('loginNavigationTestId')} />
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
