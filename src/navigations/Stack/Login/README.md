## Login

Login component.

# Usage

```js
import Login from '@navigations/Login';

# Props
```

| Prop       | Type    | Required | Description           |
| ---------- | ------- | -------- | --------------------- |
| SampleProp | boolean | false    | This is only a sample |
