// @flow

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {withTheme} from 'styled-components';
import DashboardHeader from '@components/DashboardHeader';

import LoginSettings from '@screens/private/LoginSettings';
import AccountSettings from '@screens/private/AccountSettings';

import type {LoginNavProps} from '@navigations/Stack/Login/types';

const Stack = createStackNavigator();

const Login = (props: LoginNavProps): React$Element<any> => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        header: ({
          scene: {
            descriptor: {navigation},
          },
        }) => {
          return (
            <DashboardHeader onDrawerClick={() => navigation.toggleDrawer()} />
          );
        },
      }}>
      <Stack.Screen name="Account Settings" component={AccountSettings} />
      <Stack.Screen name="Login Settings" component={LoginSettings} />
    </Stack.Navigator>
  );
};

export default (withTheme(Login): any);
