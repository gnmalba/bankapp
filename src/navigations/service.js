import React from 'react';
import {StackActions} from '@react-navigation/native';

export const navigationRef = React.createRef();
export function navigate(name, params) {
  return navigationRef.current?.navigate(name, params);
}

export const pop = (...args) => {
  return navigationRef.current?.dispatch(StackActions.pop(...args));
};
