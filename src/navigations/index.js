import React from 'react';
import {useSelector} from 'react-redux';

import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ThemeProvider} from 'styled-components/native';
import type {State, AuthenticationType} from '@ducks/types';
import UserInactivitys from '@composition/UserInactivitys';

import {
  DARK_THEME,
  LIGHT_THEME,
  NAVIGATION_DARK_THEME,
  NAVIGATION_LIGHT_THEME,
} from '@utils/constants';

import MainNavigation from './Stack/MainNavigation';
import Login from './Stack/Login';
import {navigationRef} from './service';

import {APPEARANCE_SETTINGS} from '@utils/manageSettings';

import {getThemeForMainApp} from '@ducks/config/selectors';

function RootNavigation() {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );

  const theme = useSelector(getThemeForMainApp);

  return (
    <ThemeProvider
      theme={
        theme === APPEARANCE_SETTINGS.DARK_VALUE ? DARK_THEME : LIGHT_THEME
      }>
      <SafeAreaProvider>
        <NavigationContainer
          ref={navigationRef}
          theme={
            theme === APPEARANCE_SETTINGS.DARK_VALUE
              ? NAVIGATION_DARK_THEME
              : NAVIGATION_LIGHT_THEME
          }>
          {authentication.authenticated ? (
            <UserInactivitys>
              <MainNavigation />
            </UserInactivitys>
          ) : (
            <Login />
          )}
        </NavigationContainer>
      </SafeAreaProvider>
    </ThemeProvider>
  );
}

export default RootNavigation;
