/**
 * @format
 * @flow
 */

import React from 'react';
import {useSelector} from 'react-redux';
import styled from 'styled-components/native';

import DashboardHeader from '@components/DashboardHeader';

import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from './DrawerContent';

import Bottom from '@navigations/Bottom';
import Accounts from '@navigations/Stack/Accounts';
import AccountSettings from '@navigations/Stack/AccountSettings';
import AccountSettingsLoginSettings from '@screens/private/AccountSettingsLoginSettings';

import SchedulesStack from '@navigations/Stack/Schedules';
import ManageContacts from '@navigations/Stack/ManageContacts';
import BillersStack from '@navigations/Stack/Billers';
/**
 * TODO: Temporary screen for those menus that not ready
 */
const StyledView = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  flex: 1;
`;

const StyledText = styled.Text`
  color: ${(props) => props.theme.card.textColor};
  align-self: center;
  margin: 10px;
`;

function NotReadyScreen(props) {
  return (
    <StyledView>
      <DashboardHeader onDrawerClick={() => props.navigation.toggleDrawer()} />
      <StyledText>Screen not ready for {props.route.name}</StyledText>
    </StyledView>
  );
}

/**
 * END of temporary screen for those menus that not ready
 */

const Drawer = createDrawerNavigator();

const StyledNavigator = styled(Drawer.Navigator).attrs((props) => ({
  drawerStyle: {
    width: 250,
  },
  drawerContentOptions: {
    itemStyle: {
      marginHorizontal: 0,
      borderRadius: 0,
    },
  },
}))``;

function MainNavigation(): React$Element<{}> {
  const biometrics = useSelector((state) => state.biometrics);

  return (
    <StyledNavigator
      initialRouteName={
        biometrics.registerBiometrics ? 'Login Settings' : 'Dashboard'
      }
      screenOptions={{
        headerShown: true,
        header: ({
          scene: {
            descriptor: {navigation},
          },
        }) => {
          return (
            <DashboardHeader onDrawerClick={() => navigation.toggleDrawer()} />
          );
        },
      }}
      drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="Dashboard" component={Bottom} />
      <Drawer.Screen
        name="Accounts"
        component={Accounts}
        options={{
          header: () => {},
          swipeEnabled: false,
        }}
      />
      <Drawer.Screen
        name="Loans"
        component={NotReadyScreen}
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen
        name="UITF"
        component={NotReadyScreen}
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen
        name="Wealth Manager"
        component={NotReadyScreen}
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen
        name="Enrolled Contacts"
        component={ManageContacts} // tech debt[major change]: update this implementation
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen
        name="Billers"
        component={BillersStack}
        options={{
          header: () => {},
          swipeEnabled: false,
        }}
      />
      <Drawer.Screen
        name="Schedules"
        component={SchedulesStack}
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen
        name="All Request"
        component={NotReadyScreen}
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen
        name="Settings"
        component={AccountSettings}
        options={{
          header: () => {},
        }}
      />
      <Drawer.Screen name="Login Settings">
        {(screenProps) => (
          <AccountSettingsLoginSettings
            {...screenProps}
            checkLinkedDevices={true}
          />
        )}
      </Drawer.Screen>
    </StyledNavigator>
  );
}

export default MainNavigation;
