// @flow

import {StyleSheet} from 'react-native';

type drawerContentStyles = {
  headerContainer: Object,
  headerLogo: Object,
  imageIcon: Object,
  sideBarMenuItemListStyle: Object,
  sideBarMenuItemListContainer: Object,
};

const styles: drawerContentStyles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    padding: 25,
    alignItems: 'center',
  },
  headerLogo: {
    width: 148,
    height: 32,
  },
  imageIcon: {
    height: 18,
    width: 18,
    marginLeft: 20,
  },
  sideBarMenuItemListContainer: {
    marginLeft: 20,
  },
  sideBarMenuItemListStyle: {
    fontSize: 12,
    marginTop: 20,
    fontWeight: 'bold',
  },
});

export default styles;
