/**
 * @format
 * @flow
 */

import React, {type Node} from 'react';
import {View, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import ImageIcon from '@components/ImageIcon';
import staticImage from '@utils/staticImages';

import styled from 'styled-components/native';
import {scale} from '@utils/scale';
import {EXCLUDED_SCREENS} from '@utils/constants';

import type {DrawerContentProps} from './types';
import {testProps} from '@utils/componentCustomProps';
import {AUTHENTICATION_UPDATE_STATE} from '@ducks/authentication/actionTypes';
import Text from '@components/Text';
import LogoutModal from '@screens/private/Dashboard/components/LogoutModal';

const Logo = styled(Image).attrs((props) => ({
  source: props.theme.logoSrc,
  resizeMode: 'stretch',
}))`
  height: 32px;
  width: 148px;
`;

const Banner = styled.View`
  background-color: ${(props) => props.theme.sidebar.headerBg};
  align-items: center;
  padding: 25px;
`;

// only static color since we don't have dark or white theme here
const StyledDrawerContentScrollView = styled(DrawerContentScrollView)`
  background-color: #02152b;
  margin-top: ${scale(-5)}px;
`;

const StyledDrawerItem = styled(DrawerItem).attrs((props) => ({
  label: props.label,
  onPress: props.onPress,
  icon: props.icon,
  labelStyle: {
    color: 'white',
    marginLeft: -15,
  },
}))`
  margin-left: 0;
`;

const drawerIcons = {
  Dashboard: staticImage.icons.dashboard.sidebar,
  Accounts: staticImage.icons.accounts.sidebar,
  Loans: staticImage.icons.loans.sidebar,
  UITF: staticImage.icons.uitf.sidebar,
  WealthManager: staticImage.icons.wealthManager.sidebar,
  EnrolledContacts: staticImage.icons.enrolledContacts.sidebar,
  Billers: staticImage.icons.billers.sidebar,
  Schedules: staticImage.icons.schedules.sidebar,
  AllRequest: staticImage.icons.allRequest.sidebar,
  Settings: staticImage.icons.settings.sidebar,
};

const StyledText = styled(Text).attrs((props) => ({
  mode: 'overline2',
}))`
  margin-left: ${scale(10)}px;
  color: #a2bddb;
  margin-top: ${scale(20)}px;
  margin-bottom: ${scale(10)}px;
`;

const DrawerContent = (props: DrawerContentProps): Node => {
  const dispatch = useDispatch();
  const {showLogoutModal} = useSelector((state) => state.authentication);

  const state = props.state;
  const routeNames = state.routeNames;
  const selectedMenuIndex = state.index;
  const itemStyle = props.itemStyle;

  const renderSideBarItem = (
    routeName: string,
    index: number,
    drawerItemKey: string,
  ) => (
    <StyledDrawerItem
      key={`${routeName}-${index}-${drawerItemKey}`}
      label={routeName === 'Accounts' ? 'My Accounts' : routeName}
      icon={() => (
        <ImageIcon
          source={drawerIcons[drawerItemKey]}
          testID={`drawer${drawerItemKey}IconId`}
        />
      )}
      onPress={() => {
        if (routeName === 'Accounts') {
          props.navigation.reset({
            index: 0,
            routes: [{name: routeName}],
          });
        } else {
          props.navigation.navigate(routeName);
        }
      }}
      focused={index === selectedMenuIndex}
      activeTintColor="white"
      inactiveTintColor="white"
      activeBackgroundColor="#163E69"
      inactiveBackgroundColor="#02152B"
      style={itemStyle}
    />
  );

  return (
    <StyledDrawerContentScrollView {...testProps('drawerTestId')}>
      <Banner>
        <Logo />
      </Banner>
      {routeNames
        .filter((routeName) => !EXCLUDED_SCREENS.includes(routeName))
        .map((routeName, index) => {
          const drawerItemKey = routeName.replace(/\s/g, '');
          const item = renderSideBarItem(routeName, index, drawerItemKey);

          if (drawerItemKey === 'Loans') {
            return (
              <View key={`${routeName}-${index}`}>
                {item}
                <StyledText>MY INVESTMENTS</StyledText>
              </View>
            );
          }

          if (drawerItemKey === 'WealthManager') {
            return (
              <View key={`${routeName}-${index}`}>
                {item}
                <StyledText>RECIPIENTS</StyledText>
              </View>
            );
          }

          if (drawerItemKey === 'Billers') {
            return (
              <View key={`${routeName}-${index}`}>
                {item}
                <StyledText>TRANSACTIONS</StyledText>
              </View>
            );
          }

          if (drawerItemKey === 'Settings') {
            return (
              <View key={`${routeName}-${index}`}>
                <StyledText>TOOLS</StyledText>
                {item}
              </View>
            );
          }

          return item;
        })}
      <StyledText>ACCOUNT</StyledText>
      <LogoutModal visible={showLogoutModal} />
      <StyledDrawerItem
        label="Logout"
        onPress={() => {
          dispatch({
            type: AUTHENTICATION_UPDATE_STATE,
            newState: {showLogoutModal: true},
          });
          props.navigation.closeDrawer();
        }}
        icon={() => (
          <ImageIcon
            source={staticImage.icons._logout.sidebar}
            testID="drawerLogoutIconId"
          />
        )}
      />
    </StyledDrawerContentScrollView>
  );
};

export default DrawerContent;
