// @flow

import React from 'react';
import {Image} from 'react-native';

import styles from '@navigations/Drawer/styles';
import type {ImageIconProps} from '@navigations/Drawer/types';

const ImageIcon = ({source}: ImageIconProps): React$Element<any> => (
  <Image source={source} style={styles.imageIcon} />
);

export default ImageIcon;
