// @flow

import React from 'react';
import {ListItem} from 'react-native-elements';

import styles from '@navigations/Drawer/styles';
import type {SideBarMenuItemProps} from '@navigations/Drawer/types';

const SideBarMenuItem = ({
  onPress,
  label,
  icon,
  containerStyle,
  listItemTitleStyle,
  groupLabel,
}: SideBarMenuItemProps): React$Element<any> => {
  if (!groupLabel) {
    return (
      <ListItem onPress={onPress} containerStyle={containerStyle}>
        {icon}
        <ListItem.Content>
          <ListItem.Title style={listItemTitleStyle}>{label}</ListItem.Title>
        </ListItem.Content>
      </ListItem>
    );
  }

  return (
    <ListItem
      containerStyle={{
        ...containerStyle,
        ...styles.sideBarMenuItemListContainer,
      }}>
      <ListItem.Content>
        <ListItem.Title
          style={{
            ...listItemTitleStyle,
            ...styles.sideBarMenuItemListStyle,
          }}>
          {label}
        </ListItem.Title>
      </ListItem.Content>
    </ListItem>
  );
};

SideBarMenuItem.defaultProps = {
  groupLabel: false,
};

export default SideBarMenuItem;
