import React from 'react';
import {render} from '@testing-library/react-native';
import {NavigationContainer} from '@react-navigation/native';
import Drawer from '@navigations/Drawer';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

jest.mock('@react-navigation/native', () => {
  const nav = jest.requireActual('@react-navigation/native');
  nav.useNavigation = () => jest.fn();
  return nav;
});

describe('Drawer Navigation', () => {
  const mockAppState = {
    dashboard: {
      isQuickLinks: false,
      guidedTour: {},
    },
    config: {
      theme: 'dark',
      widgets: [
        {
          ref: 1,
          label: 'Test Label A',
          loading: false,
        },
        {
          ref: 2,
          label: 'Test Label B',
          loading: false,
        },
      ],
      removedWidgets: [1, 2],
      checked: [],
      limitShown: false,
      customizedWidgets: [1, 2],
    },
    authentication: {
      showLogoutModal: false,
      anotherSessionClosed: false,
      loading: false,
    },
    credit: {
      loading: false,
      data: [],
      error: false,
      errors: [],
    },
    recoverAccess: {
      showUsernameSentSnackbar: false,
    },
    biometrics: {
      registerBiometrics: false,
    },
  };

  it('Should work as expected', () => {
    const store = {
      ...mockAppState,
    };

    const all = render(
      <NavigationContainer>
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <Drawer />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it.each([{isQuickLinks: false}, {isQuickLinks: true}])(
    'should render correctly when %o',
    (additionalProps) => {
      const store = {
        ...mockAppState,
        dashboard: {
          ...mockAppState.dashboard,
          ...additionalProps,
        },
      };

      const all = render(
        <NavigationContainer>
          <MockProvider store={store}>
            <ThemeProvider theme={DARK_THEME}>
              <Drawer />
            </ThemeProvider>
          </MockProvider>
        </NavigationContainer>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
