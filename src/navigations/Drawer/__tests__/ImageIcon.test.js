import React from 'react';
import {render} from '@testing-library/react-native';
import {ImageIcon} from '../components';

describe('ImageIcon', () => {
  it('Should render properly', () => {
    const all = render(<ImageIcon />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
