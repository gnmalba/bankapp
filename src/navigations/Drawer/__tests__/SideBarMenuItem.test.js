import React from 'react';
import {render} from '@testing-library/react-native';
import {SideBarMenuItem} from '../components';

describe('ImageIcon', () => {
  it.each([true, false])('Should render properly %s', (bools) => {
    const all = render(<SideBarMenuItem groupLabel={bools} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
