// @flow

export type State = {
  count: number,
};

export type Item = {
  ref: string,
  label: string,
};

export type ImageIconProps = {
  source: Node,
};

export type DrawerContentProps = {
  navigation: {
    navigate: (string) => void,
    closeDrawer: () => void,
    reset: (Object) => void,
  },
  activeBackgroundColor: string,
  inactiveBackgroundColor: string,
  inactiveTintColor: string,
  state: {
    index: number,
    routeNames: Array<string>,
  },
  itemStyle: Object,
};

export type containerStyleProps = {
  backgroundColor: string,
};

export type listItemTitleStyleProps = {
  color: string,
};

export type SideBarMenuItemProps = {
  onPress?: () => void,
  icon?: React$Element<any>,
  listItemTitleStyle?: listItemTitleStyleProps,
  containerStyle?: containerStyleProps,
  label?: string,
  groupLabel?: boolean,
};
