// @flow

import type {Item} from '@navigations/Drawer/types';
import {GROUP} from '@utils/constants';

export const SIDEBAR_ITEMS: Array<Item> = [
  {
    ref: 'dashboard',
    label: 'Dashboard',
  },
  {
    ref: 'accounts',
    label: 'My Accounts',
  },
  {
    ref: 'group',
    label: 'MY INVESTMENTS',
  },
];

export const SIDEBAR_ITEMS_LINKS: Array<string> = SIDEBAR_ITEMS.filter(
  (item) => item.ref !== GROUP,
).reduce((prev, cur) => [...prev, cur.ref], []);
