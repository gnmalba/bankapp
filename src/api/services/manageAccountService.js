/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {MANAGE_ACCOUNT_SETTINGS_SERVICE, CASAS} from '@api/constants';

const MangageAccountConfig = {
  // CASA
  editAliasCasaAccount: (params: {
    alias: string,
    accountId: string,
  }): typeof AxiosResponse =>
    api.patch(`${CASAS}/${params.accountId}`, {
      ...params,
    }),
  deleteCasaAccount: (params: {
    authValue: string,
    customerId: string,
    accountId: string,
  }): typeof AxiosResponse => {
    return api.delete(`accounts/${CASAS}/${params.accountId}`, {
      data: {
        ...params,
        authType: 'mpin',
      },
    });
  },

  // CPC
  editAliasCPCAccount: (params: {
    customerId: string,
    cardSerialNo: string,
    cardAlias: string,
  }): typeof AxiosResponse =>
    api.post(
      `${MANAGE_ACCOUNT_SETTINGS_SERVICE.CPC_CARD_MANAGEMENT}/change-alias`,
      {
        ...params,
      },
    ),
  deleteCPCAccount: (params: {
    customerId: string,
    cardSerialNo: string,
    username: string,
    mpin: string,
  }): typeof AxiosResponse =>
    api.post(
      `${MANAGE_ACCOUNT_SETTINGS_SERVICE.CPC_CARD_MANAGEMENT}/delete-account`,
      {
        ...params,
      },
    ),
};

export default MangageAccountConfig;
