/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';

import {SEND_MONEY_API} from '@api/constants';
import type {SendMoneyTransactionType} from '@ducks/sendMoney/types';

const sendMoneyService = {
  getSourceAccounts: (
    customerId: string,
    cardType: 'prepaidcard' | 'creditcard' | 'all' = 'all',
  ): typeof AxiosResponse => {
    return api
      .get(SEND_MONEY_API.GET_SOURCE_ACCOUNTS, {
        params: {customerId, cardType},
      })
      .then((response) => response.data.data);
  },
  getEnrolledContacts: (customerId: string): typeof AxiosResponse => {
    return api
      .get(`${SEND_MONEY_API.GET_ENROLLED_CONTACTS}/${customerId}/accounts`)
      .then((response) => response.data.data);
  },
  getEnrolledContactDetails: (
    customerId: string,
    contactId: string,
  ): typeof AxiosResponse => {
    return api
      .get(
        `${SEND_MONEY_API.GET_ENROLLED_CONTACTS}/accounts/${contactId}?customerId=${customerId}`,
      )
      .then((response) => response.data.data);
  },
  getBanks: (): typeof AxiosResponse => {
    return api
      .get(SEND_MONEY_API.GET_BANKS)
      .then((response) => response.data.data);
  },
  getCountryCodes: (): typeof AxiosResponse => {
    return api
      .get(SEND_MONEY_API.GET_COUNTRY_CODES)
      .then((response) => response.data.data);
  },
  getTransferServices: (): typeof AxiosResponse => {
    return api
      .get(SEND_MONEY_API.GET_TRANSFER_SERVICES)
      .then((response) => response.data.data);
  },

  submit: (
    payload: any,
    transactionType: SendMoneyTransactionType,
    useCpc: boolean = false,
  ): typeof AxiosResponse => {
    const service = useCpc ? SEND_MONEY_API.CPC : SEND_MONEY_API.CASA;

    return api
      .post(service[transactionType], payload)
      .then((response) => response.data.data);
  },
};

export default sendMoneyService;
