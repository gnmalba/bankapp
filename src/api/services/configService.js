/**
 * @flow
 */

import {AxiosResponse} from 'axios';

import api from '@api/';

import {SETTINGS_SERVICE} from '@api/constants';

import {getDefaultDeviceSettingsConfig} from '@api/utils';

import type {ConfigLocalStorage} from '@ducks/config/types';

const deviceDefaultConfig = getDefaultDeviceSettingsConfig();

type Response = typeof AxiosResponse;

const ConfigService = {
  get: (username: string, customerId: string): Response =>
    api.get(
      `${SETTINGS_SERVICE.DEVICE}?deviceId=${deviceDefaultConfig['X-DEVICE-ID']}&username=${username}&customerId=${customerId}`,
    ),
  update: (
    params: {username: string, customerId: string},
    postData: ConfigLocalStorage,
  ): Response =>
    api.post(
      `${SETTINGS_SERVICE.DEVICE}?deviceId=${deviceDefaultConfig['X-DEVICE-ID']}&username=${params.username}&customerId=${params.customerId}`,
      postData,
    ),
};

export default ConfigService;
