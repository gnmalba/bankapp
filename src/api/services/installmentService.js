/**
 * @flow
 */

import {AxiosResponse} from 'axios';
import api from '@api/';
import {INSTALLMENT_SERVICE} from '@api/constants';
import type {
  PaymentTermsRequest,
  BalanceTransferSaveRequest,
  BalanceConversionSaveRequest,
  Cash2GoSaveRequest,
  ViewInstallmentsRequestType,
  MoreInformationRequest,
  InstallmentTypesString,
} from '@ducks/installments/types';

const InstallmentsConfig = {
  getBanks: (installmentType: InstallmentTypesString): typeof AxiosResponse => {
    return api.get(INSTALLMENT_SERVICE.BANK_LIST, {
      params: {
        installmentType,
      },
    });
  },
  getMoreInformation: (
    params: MoreInformationRequest,
  ): typeof AxiosResponse => {
    return api.get(INSTALLMENT_SERVICE.MORE_INFORMATION, {
      params,
    });
  },
  getPaymentTerms: (
    paymentTermsParams: PaymentTermsRequest,
  ): typeof AxiosResponse => {
    return api.get(INSTALLMENT_SERVICE.PAYMENT_TERMS, {
      params: paymentTermsParams,
    });
  },
  balanceTransferSave: (
    params: BalanceTransferSaveRequest,
  ): typeof AxiosResponse => {
    return api.post(INSTALLMENT_SERVICE.BALANCE_TRANSFER_SAVE, params);
  },
  balanceConversionSave: (
    params: BalanceConversionSaveRequest,
  ): typeof AxiosResponse => {
    return api.post(INSTALLMENT_SERVICE.BALANCE_CONVERSION_SAVE, params);
  },
  cash2GoSave: (params: Cash2GoSaveRequest): typeof AxiosResponse => {
    const {disbursementOption} = params;
    if (disbursementOption === 'E') {
      return api.post(INSTALLMENT_SERVICE.CASH2GO_SAVE.enrolled, params);
    } else if (disbursementOption === 'U') {
      return api.post(INSTALLMENT_SERVICE.CASH2GO_SAVE.unenrolled, params);
    } else if (disbursementOption === 'N') {
      return api.post(INSTALLMENT_SERVICE.CASH2GO_SAVE.nonMb, params);
    }
  },
  viewInstallments: (
    params: ViewInstallmentsRequestType,
  ): typeof AxiosResponse => {
    return api.get(INSTALLMENT_SERVICE.VIEW_INSTALLMENTS, {params});
  },
  sortInstallments: (
    params: ViewInstallmentsRequestType,
  ): typeof AxiosResponse => {
    return api.get(INSTALLMENT_SERVICE.SORT_INSTALLMENTS, {params});
  },
  viewTermsCondition: (
    params: ViewInstallmentsRequestType,
  ): typeof AxiosResponse => {
    return api.get(INSTALLMENT_SERVICE.VIEW_TERMS_CONDITION, {params});
  },
};

export default InstallmentsConfig;
