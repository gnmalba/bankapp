/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {DASHBOARD_SERVICE, TRANSACTION_HISTORY_SERVICE} from '@api/constants';

const DepositConfig = {
  getPesoBalance: (customerId: number): typeof AxiosResponse =>
    api.get(`${DASHBOARD_SERVICE.PESOS_ACCOUNT_BALANCES}/${customerId}`),

  getPesoTotalBalance: (customerId: number): typeof AxiosResponse =>
    api.get(`${DASHBOARD_SERVICE.PESOS_TOTAL_BALANCE}/${customerId}`),

  getYazz: (customerId: number): typeof AxiosResponse =>
    api.get(
      `${DASHBOARD_SERVICE.CREDIT_CARD_LIST_ACCOUNTS}?customerId=${customerId}&cardTypes=prepaidcard`,
    ),

  getForeignBalance: (customerId: number): typeof AxiosResponse =>
    api.get(`${DASHBOARD_SERVICE.FOREIGN_ACCOUNT_BALANCES}/${customerId}`),

  getAccountDetails: (payload: number): typeof AxiosResponse =>
    api.get(`casas/${payload}/balance`),

  getTransactionHistory: (payload: number): typeof AxiosResponse =>
    api.get(`casas/${payload}/transaction-history`),
  getTransactionCodes: (): typeof AxiosResponse =>
    api.get(`${TRANSACTION_HISTORY_SERVICE.TRANSACTION_CODE}`),
};

export default DepositConfig;
