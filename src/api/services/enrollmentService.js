/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {ENROLLMENT_SERVICE} from '@api/constants';
import type {
  EnrollDepositServiceType,
  EnrollCreditServiceType,
} from '@ducks/enrollment/types';

const EnrollmentConfig = {
  getYazzBins: (): typeof AxiosResponse =>
    api.get(ENROLLMENT_SERVICE.YAZZ_BINS),
  getEnrollmentPublicKey: (): typeof AxiosResponse =>
    api.get(ENROLLMENT_SERVICE.PUBLIC_KEY),
  getAccountDetails: (accountNo: string): typeof AxiosResponse =>
    api.get(`${ENROLLMENT_SERVICE.CASAS}/${accountNo}/details`),
  enrollDepositCard: ({
    values,
  }: EnrollDepositServiceType): typeof AxiosResponse =>
    api.post(ENROLLMENT_SERVICE.DEPOSIT_ACCOUNT_ENROLLMENT, {
      ...values,
    }),
  enrollCreditCard: ({values}: EnrollCreditServiceType): typeof AxiosResponse =>
    api.post(ENROLLMENT_SERVICE.CREDIT_ACCOUNT_ENROLLMENT, {
      ...values,
    }),
};

export default EnrollmentConfig;
