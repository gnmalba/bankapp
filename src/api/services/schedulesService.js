/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {SCHEDULES_API} from '@api/constants';
import type {ScheduleFetchParamsProps} from '@ducks/schedules/types';

const SchedulesService = {
  getScheduledPayBill: (
    payload: $Shape<ScheduleFetchParamsProps>,
  ): typeof AxiosResponse => {
    const {customerId, pageNumber, pageSize, status} = payload;
    return api.get(
      `${SCHEDULES_API.GET_SCHEDULED_PAY_BILL}${customerId}&page=${pageNumber}&size=${pageSize}&filter=${status}`,
    );
  },

  getScheduledSendMoney: (
    payload: $Shape<ScheduleFetchParamsProps>,
  ): typeof AxiosResponse => {
    const {customerId, pageNumber, pageSize, status} = payload;
    return api.get(
      `${SCHEDULES_API.GET_SCHEDULED_SEND_MONEY}${customerId}&page=${pageNumber}&size=${pageSize}&filter=${status}`,
    );
  },

  cancelSchedule: (referenceNumber: number): typeof AxiosResponse =>
    api.delete(`${SCHEDULES_API.CANCEL_SCHEDULE}?refNo=${referenceNumber}`),
};

export default SchedulesService;
