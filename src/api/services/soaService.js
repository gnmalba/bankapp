/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {SOA_SERVICE} from '@api/constants';

const CreditConfig = {
  getIncoming: (customerId: number, serialNo: string): typeof AxiosResponse =>
    api.get(
      `${SOA_SERVICE.SOA_CURRENT}?customerId=${customerId}&serNo=${serialNo}`,
    ),

  getLast: (customerId: number, serialNo: string): typeof AxiosResponse =>
    api.get(
      `${SOA_SERVICE.SOA_LAST}?statementCount=12&customerId=${customerId}&serNo=${serialNo}`,
    ),

  getTransactionIncoming: (params: Object): typeof AxiosResponse =>
    api.get(`${SOA_SERVICE.SOA_TRANSACTION_CURRENT}`, {
      params,
    }),

  getTransactionPast: (params: Object): typeof AxiosResponse =>
    api.get(`${SOA_SERVICE.SOA_TRANSACTION_PAST}`, {
      params,
    }),
};

export default CreditConfig;
