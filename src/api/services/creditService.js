/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {
  CREDIT_SERVICE,
  CARD_LIST_SERVICE,
  ACTIVATE_CARD_SERVICE,
} from '@api/constants';

type AxiosResponseType = typeof AxiosResponse;

const CreditConfig = {
  getCreditCards: (customerId: number, cardTypes: string): AxiosResponseType =>
    api.get(
      `${CREDIT_SERVICE.CREDIT_CARD_LIST_ACCOUNTS}?customerId=${customerId}&cardTypes=${cardTypes}`,
    ),

  getCreditCardDetails: (
    customerId: number,
    cardSerialNo: number,
  ): AxiosResponseType =>
    api.get(
      `${CREDIT_SERVICE.CREDIT_CARD_OVERVIEW}?cardSerialNo=${cardSerialNo}&customerId=${customerId}`,
    ),

  getCreditTransactionCurrent: (params: Object): AxiosResponseType =>
    api.get(`${CREDIT_SERVICE.CREDIT_CARD_TRANSACTIONS_CURRENT}`, {
      params,
    }),
  getCreditTransactionLast: (params: Object): typeof AxiosResponse =>
    api.get(`${CREDIT_SERVICE.CREDIT_CARD_TRANSACTIONS_LAST}`, {
      params,
    }),
  getCreditCardsForActivation: (customerId: string): AxiosResponseType =>
    api.get(`${CARD_LIST_SERVICE}?customerId=${customerId}&status=activation`),
  activateCreditCard: (
    customerId: string,
    postData: {
      username: string,
      cardSerialNo: string,
      passcode: string,
    },
  ): AxiosResponseType =>
    api.post(`${ACTIVATE_CARD_SERVICE}?customerId=${customerId}`, postData),
};

export default CreditConfig;
