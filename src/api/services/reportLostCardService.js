/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {REPORT_LOST_SERVICE} from '@api/constants';

const reportLostCardService = {
  get: (
    customerId: string,
    status: string = 'blocking',
  ): typeof AxiosResponse =>
    api.get(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD}?customerId=${customerId}&status=${status}`,
    ),
  getCardDetails: (params: {
    customerId: string,
    cardSerNo: string,
  }): typeof AxiosResponse =>
    api.get(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD_DETAILS}?customerId=${params.customerId}&cardSerialNo=${params.cardSerNo}`,
    ),
  reportCard: (params: {
    customerId: string,
    cardSerialNo: string,
    username: string,
    passcode: string,
  }): typeof AxiosResponse =>
    api.post(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD_BLOCKING}?customerId=${params.customerId}`,
      {
        ...params,
      },
    ),
  getTermsAndConditions: (params: {
    customerId: string,
    cardSerNo: string,
  }): typeof AxiosResponse =>
    api.get(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD_TERMS_CONDITIONS}?customerId=${params.customerId}&cardSerNo=${params.cardSerNo}`,
    ),
};

export default reportLostCardService;
