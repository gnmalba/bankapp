/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {PAYBILLS_API_CASA, PAYBILLS_API_CPC} from '@api/constants';

type Response = typeof AxiosResponse;

export type PbcInputProps = {
  whatApi: 'CPC' | 'CASA',
  data: Object,
  transactionType: $Keys<typeof PAYBILLS_API_CASA>,
};

export const apiConds = {
  CASA: (transactionType: string): string => PAYBILLS_API_CASA[transactionType],
  CPC: (transactionType: string): string => PAYBILLS_API_CPC[transactionType],
};

const payBillsCreditConfig = {
  payBillsCredit: async ({
    whatApi,
    data,
    transactionType,
  }: PbcInputProps): Response =>
    await api.post(apiConds[whatApi](transactionType), data),
};

export default payBillsCreditConfig;
