/**
 * @flow
 */
import {AxiosResponse} from 'axios';

import api from '@api/';

import {ACTIVITY_LOGS_SERVICE} from '@api/constants';
import type {getActivityLogApiParams} from '@ducks/activity-log/types';

const activityLogService = {
  get: ({
    username,
    customerId,
    page,
    size,
  }: getActivityLogApiParams): typeof AxiosResponse =>
    api.get(
      `${ACTIVITY_LOGS_SERVICE}?username=${username}&customerId=${customerId}&page=${page}&size=${size}`,
    ),
};

export default activityLogService;
