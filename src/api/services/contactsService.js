import api from '@api/';
import {AxiosResponse} from 'axios';
import {CONTACTS_API} from '@api/constants';

import type {
  AddContactFields,
  EditContactFields,
  ContactsFilterFields,
} from '@ducks/contacts/types';

const ContactsConfig = {
  getContacts: (customerId: number): typeof AxiosResponse =>
    api.get(`${CONTACTS_API.GET_CONTACTS}/${customerId}/accounts`),

  getContactDetails: (
    customerId: number,
    contactId: number,
  ): typeof AxiosResponse =>
    api.get(
      `${CONTACTS_API.GET_CONTACTS}/${contactId}?customerId=${customerId}`,
    ),

  getContactTransactions: (
    payload: ContactsFilterFields,
  ): typeof AxiosResponse =>
    api.get(`${CONTACTS_API.GET_CONTACT_TRANSACTIONS_BY_FILTER}`, {
      ...payload,
    }),

  getContactSendMoneyTransactions: (
    payload: ContactsFilterFields,
  ): typeof AxiosResponse =>
    api.get(`${CONTACTS_API.GET_CONTACT_SEND_MONEY_TRANSACTIONS}`, {
      ...payload,
    }),

  addContact: (payload: AddContactFields): typeof AxiosResponse =>
    api.post(`${CONTACTS_API.ADD_CONTACT}`, {
      ...payload,
    }),

  getBankList: (): typeof AxiosResponse =>
    api.get(`${CONTACTS_API.GET_BANK_LIST}`),

  editContact: (payload: EditContactFields): typeof AxiosResponse =>
    api.put(`${CONTACTS_API.EDIT_CONTACT}/${payload.contactId}`, {
      customerId: payload.customerId,
      alias: payload.alias,
      mpin: payload.mpin,
    }),

  deleteContact: (payload: DeleteContactFields): typeof AxiosResponse =>
    api.delete(`${CONTACTS_API.DELETE_CONTACT}`, {
      data: {
        customerId: payload.customerId,
        contactId: payload.contactId,
        mpin: payload.mpin,
      },
    }),
};

export default ContactsConfig;
