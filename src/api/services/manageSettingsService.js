import {AxiosResponse} from 'axios';
import api from '@api/';
import {MANAGE_SETTINGS_SERVICE} from '@api/constants';
import type {SaveDeviceSettingsQueryParams} from '@ducks/managesettings/types';

const ManageSettingsConfig = {
  getDevices: (customerId: string): typeof AxiosResponse => {
    return api.get(MANAGE_SETTINGS_SERVICE.DEVICES + `/${customerId}`);
  },
  unlinkDevice: (params: any): typeof AxiosResponse => {
    return api.post(MANAGE_SETTINGS_SERVICE.UNLINK_DEVICE, params);
  },
  saveDeviceSettings: (
    queryParams: SaveDeviceSettingsQueryParams,
    body: any,
  ) => {
    return api.post(MANAGE_SETTINGS_SERVICE.SAVE_DEVICE_SETTINGS, body, {
      params: queryParams,
    });
  },
  getDeviceSettings: (params: SaveDeviceSettingsQueryParams) =>
    api.get(MANAGE_SETTINGS_SERVICE.SAVE_DEVICE_SETTINGS, {params}),
  validateMPIN: (params: any): typeof AxiosResponse => {
    return api.post(MANAGE_SETTINGS_SERVICE.VALIDATE_MPIN, params);
  },
  updateMPIN: (params: any): typeof AxiosResponse => {
    return api.post(MANAGE_SETTINGS_SERVICE.UPDATE_MPIN, params);
  },
  getPasswordPolicy: (params: any): typeof AxiosResponse => {
    return api.get(MANAGE_SETTINGS_SERVICE.GET_PASSWORD_POLICY);
  },
  changePassword: (params: any): typeof AxiosResponse => {
    return api.post(MANAGE_SETTINGS_SERVICE.CHANGE_PASSWORD, params);
  },
};

export default ManageSettingsConfig;
