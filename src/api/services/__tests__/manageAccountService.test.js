import api from '@api/';
import {MANAGE_ACCOUNT_SETTINGS_SERVICE, CASAS} from '@api/constants';

import manageAccountService from '../manageAccountService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('manageAccountService', () => {
  it('editAliasCasaAccount() should call the correct URL', async () => {
    const params = {
      alias: 'account alias',
      accountId: '1',
    };
    manageAccountService.editAliasCasaAccount(params);

    expect(api.patch).toHaveBeenCalledWith(`${CASAS}/${params.accountId}`, {
      ...params,
    });
  });

  it('deleteCasaAccount() should call the correct URL', async () => {
    const params = {
      authValue: 'e87123df',
      customerId: '121',
      accountId: '6281',
    };
    manageAccountService.deleteCasaAccount(params);

    expect(api.delete).toHaveBeenCalledWith(
      `accounts/${CASAS}/${params.accountId}`,
      {
        data: {
          ...params,
          authType: 'mpin',
        },
      },
    );
  });

  it('editAliasCPCAccount() should call the correct URL', async () => {
    const params = {
      customerId: '121',
      cardSerialNo: '12345678',
      cardAlias: 'Alias',
    };
    manageAccountService.editAliasCPCAccount(params);

    expect(api.post).toHaveBeenCalledWith(
      `${MANAGE_ACCOUNT_SETTINGS_SERVICE.CPC_CARD_MANAGEMENT}/change-alias`,
      {
        ...params,
      },
    );
  });

  it('deleteCPCAccount() should call the correct URL', async () => {
    const params = {
      customerId: '121',
      cardSerialNo: '12345678',
      username: 'user',
      mpin: '123456',
    };
    manageAccountService.deleteCPCAccount(params);

    expect(api.post).toHaveBeenCalledWith(
      `${MANAGE_ACCOUNT_SETTINGS_SERVICE.CPC_CARD_MANAGEMENT}/delete-account`,
      {
        ...params,
      },
    );
  });
});
