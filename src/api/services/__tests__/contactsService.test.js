import api from '@api/';
import {CONTACTS_API} from '@api/constants';

import contactsService from '../contactsService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});
describe('contactsService', () => {
  it('getContacts() should call the correct URL', async () => {
    const customerId = 1;
    contactsService.getContacts(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${CONTACTS_API.GET_CONTACTS}/${customerId}/accounts`,
    );
  });

  it('getContactDetails() should call the correct URL', async () => {
    const customerId = 1;
    const contactId = 2;
    contactsService.getContactDetails(customerId, contactId);

    expect(api.get).toHaveBeenCalledWith(
      `${CONTACTS_API.GET_CONTACTS}/${contactId}?customerId=${customerId}`,
    );
  });

  it('getContactTransactions() should call the correct URL', async () => {
    const payload = {
      contactId: '1',
      page: 1,
    };
    contactsService.getContactTransactions(payload);

    expect(api.get).toHaveBeenCalledWith(
      `${CONTACTS_API.GET_CONTACT_TRANSACTIONS_BY_FILTER}`,
      {
        ...payload,
      },
    );
  });

  it('getContactSendMoneyTransactions() should call the correct URL', async () => {
    const payload = {
      contactId: '1',
      page: 1,
    };
    contactsService.getContactSendMoneyTransactions(payload);

    expect(api.get).toHaveBeenCalledWith(
      `${CONTACTS_API.GET_CONTACT_SEND_MONEY_TRANSACTIONS}`,
      {
        ...payload,
      },
    );
  });

  it('addContact() should call the correct URL', async () => {
    const payload = {
      contactType: 'type',
      alias: 'alias',
      mpin: '1234',
      customerId: 1,
    };
    contactsService.addContact(payload);

    expect(api.post).toHaveBeenCalledWith(`${CONTACTS_API.ADD_CONTACT}`, {
      ...payload,
    });
  });

  it('getBankList() should call the correct URL', async () => {
    contactsService.getBankList();

    expect(api.get).toHaveBeenCalledWith(`${CONTACTS_API.GET_BANK_LIST}`);
  });

  it('editContact() should call the correct URL', async () => {
    const payload = {
      contactId: '1',
      mpin: '1234',
      alias: 'alias',
    };
    contactsService.editContact(payload);

    expect(api.put).toHaveBeenCalledWith(
      `${CONTACTS_API.EDIT_CONTACT}/${payload.contactId}`,
      {
        customerId: payload.customerId,
        alias: payload.alias,
        mpin: payload.mpin,
      },
    );
  });

  it('deleteContact() should call the correct URL', async () => {
    const payload = {
      contactId: '1',
      mpin: '1234',
    };
    contactsService.deleteContact(payload);

    expect(api.delete).toHaveBeenCalledWith(`${CONTACTS_API.DELETE_CONTACT}`, {
      data: {
        customerId: payload.customerId,
        contactId: payload.contactId,
        mpin: payload.mpin,
      },
    });
  });
});
