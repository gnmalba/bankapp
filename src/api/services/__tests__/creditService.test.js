import api from '@api/';
import {
  CREDIT_SERVICE,
  CARD_LIST_SERVICE,
  ACTIVATE_CARD_SERVICE,
} from '@api/constants';

import creditService from '../creditService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('creditService', () => {
  it('getCreditCards() should call the correct URL', async () => {
    const customerId = 1;
    const cardTypes = 'all';
    creditService.getCreditCards(customerId, cardTypes);

    expect(api.get).toHaveBeenCalledWith(
      `${CREDIT_SERVICE.CREDIT_CARD_LIST_ACCOUNTS}?customerId=${customerId}&cardTypes=${cardTypes}`,
    );
  });

  it('getCreditCardDetails() should call the correct URL', async () => {
    const customerId = 1;
    const cardSerialNo = 1234;
    creditService.getCreditCardDetails(customerId, cardSerialNo);

    expect(api.get).toHaveBeenCalledWith(
      `${CREDIT_SERVICE.CREDIT_CARD_OVERVIEW}?cardSerialNo=${cardSerialNo}&customerId=${customerId}`,
    );
  });

  it('getCreditTransactionCurrent() should call the correct URL', async () => {
    const params = {data: 'test'};
    creditService.getCreditTransactionCurrent(params);

    expect(api.get).toHaveBeenCalledWith(
      `${CREDIT_SERVICE.CREDIT_CARD_TRANSACTIONS_CURRENT}`,
      {
        params,
      },
    );
  });

  it('getCreditTransactionLast() should call the correct URL', async () => {
    const params = {data: 'test'};
    creditService.getCreditTransactionLast(params);

    expect(api.get).toHaveBeenCalledWith(
      `${CREDIT_SERVICE.CREDIT_CARD_TRANSACTIONS_LAST}`,
      {
        params,
      },
    );
  });

  it('getCreditCardsForActivation() should call the correct URL', async () => {
    const customerId = 1;
    creditService.getCreditCardsForActivation(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${CARD_LIST_SERVICE}?customerId=${customerId}&status=activation`,
    );
  });

  it('activateCreditCard() should call the correct URL', async () => {
    const customerId = 1;
    const postData = {
      username: 'user',
      cardSerialNo: '1234',
      passcode: '5678',
    };
    creditService.activateCreditCard(customerId, postData);

    expect(api.post).toHaveBeenCalledWith(
      `${ACTIVATE_CARD_SERVICE}?customerId=${customerId}`,
      postData,
    );
  });
});
