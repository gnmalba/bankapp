import api from '@api/';
import {SEND_MONEY_API} from '@api/constants';

import sendMoneyService from '../sendMoneyService';

jest.mock('api', () => ({
  get: jest.fn().mockResolvedValue({data: {}}),
  post: jest.fn().mockResolvedValue({data: {}}),
}));

describe('sendMoneyService', () => {
  it('getSourceAccounts() should call the correct URL', async () => {
    const customerId = '1';
    const cardType = 'all';
    await sendMoneyService.getSourceAccounts(customerId, cardType);

    expect(api.get).toHaveBeenCalledWith(SEND_MONEY_API.GET_SOURCE_ACCOUNTS, {
      params: {customerId, cardType},
    });
  });

  it('getEnrolledContacts() should call the correct URL', async () => {
    const customerId = '1';
    await sendMoneyService.getEnrolledContacts(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${SEND_MONEY_API.GET_ENROLLED_CONTACTS}/${customerId}/accounts`,
    );
  });

  it('getEnrolledContactDetails() should call the correct URL', async () => {
    const customerId = '1';
    const contactId = '2';
    await sendMoneyService.getEnrolledContactDetails(customerId, contactId);

    expect(api.get).toHaveBeenCalledWith(
      `${SEND_MONEY_API.GET_ENROLLED_CONTACTS}/accounts/${contactId}?customerId=${customerId}`,
    );
  });

  it('getBanks() should call the correct URL', async () => {
    await sendMoneyService.getBanks();

    expect(api.get).toHaveBeenCalledWith(SEND_MONEY_API.GET_BANKS);
  });

  it('getCountryCodes() should call the correct URL', async () => {
    await sendMoneyService.getCountryCodes();

    expect(api.get).toHaveBeenCalledWith(SEND_MONEY_API.GET_COUNTRY_CODES);
  });

  it('getTransferServices() should call the correct URL', async () => {
    await sendMoneyService.getTransferServices();

    expect(api.get).toHaveBeenCalledWith(SEND_MONEY_API.GET_TRANSFER_SERVICES);
  });

  it.each([
    ['IMMEDIATE_OWN', false, SEND_MONEY_API.CASA.IMMEDIATE_OWN],
    ['IMMEDIATE_ENROLLED', false, SEND_MONEY_API.CASA.IMMEDIATE_ENROLLED],
    ['IMMEDIATE_UNENROLLED', false, SEND_MONEY_API.CASA.IMMEDIATE_UNENROLLED],
    ['SCHEDULED_OWN', false, SEND_MONEY_API.CASA.SCHEDULED_OWN],
    ['SCHEDULED_ENROLLED', false, SEND_MONEY_API.CASA.SCHEDULED_ENROLLED],
    ['SCHEDULED_UNENROLLED', false, SEND_MONEY_API.CASA.SCHEDULED_UNENROLLED],
    ['IMMEDIATE_OWN', true, SEND_MONEY_API.CPC.IMMEDIATE_OWN],
    ['IMMEDIATE_ENROLLED', true, SEND_MONEY_API.CPC.IMMEDIATE_ENROLLED],
    ['IMMEDIATE_UNENROLLED', true, SEND_MONEY_API.CPC.IMMEDIATE_UNENROLLED],
    ['SCHEDULED_OWN', true, SEND_MONEY_API.CPC.SCHEDULED_OWN],
    ['SCHEDULED_ENROLLED', true, SEND_MONEY_API.CPC.SCHEDULED_ENROLLED],
    ['SCHEDULED_UNENROLLED', true, SEND_MONEY_API.CPC.SCHEDULED_UNENROLLED],
  ])(
    'submit() should call the correct URL',
    async (transactionType, useCpc, expectedApi) => {
      const payload = {data: {}};
      await sendMoneyService.submit(payload, transactionType, useCpc);

      expect(api.post).toHaveBeenCalledWith(expectedApi, payload);
    },
  );
});
