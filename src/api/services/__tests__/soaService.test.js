import api from '@api/';
import {SOA_SERVICE} from '@api/constants';

import soaService from '../soaService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('soaService', () => {
  it('getIncoming() should call the correct URL', async () => {
    const customerId = 1;
    const serialNo = '1234';
    soaService.getIncoming(customerId, serialNo);

    expect(api.get).toHaveBeenCalledWith(
      `${SOA_SERVICE.SOA_CURRENT}?customerId=${customerId}&serNo=${serialNo}`,
    );
  });

  it('getLast() should call the correct URL', async () => {
    const customerId = 1;
    const serialNo = '1234';
    soaService.getLast(customerId, serialNo);

    expect(api.get).toHaveBeenCalledWith(
      `${SOA_SERVICE.SOA_LAST}?statementCount=12&customerId=${customerId}&serNo=${serialNo}`,
    );
  });

  it('getTransactionIncoming() should call the correct URL', async () => {
    const params = {data: 'test'};
    soaService.getTransactionIncoming(params);

    expect(api.get).toHaveBeenCalledWith(
      `${SOA_SERVICE.SOA_TRANSACTION_CURRENT}`,
      {
        params,
      },
    );
  });

  it('getTransactionPast() should call the correct URL', async () => {
    const params = {data: 'test'};
    soaService.getTransactionPast(params);

    expect(api.get).toHaveBeenCalledWith(
      `${SOA_SERVICE.SOA_TRANSACTION_PAST}`,
      {
        params,
      },
    );
  });
});
