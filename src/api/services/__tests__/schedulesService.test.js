import api from '@api/';
import {SCHEDULES_API} from '@api/constants';

import schedulesService from '../schedulesService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('schedulesService', () => {
  it('getScheduledPayBill() should call the correct URL', async () => {
    const mockParameters = {
      customerId: '1',
      pageNumber: 1,
      pageSize: 15,
      status: 'active',
    };
    const {customerId, pageNumber, pageSize, status} = mockParameters;
    schedulesService.getScheduledPayBill(mockParameters);

    expect(api.get).toHaveBeenCalledWith(
      `${SCHEDULES_API.GET_SCHEDULED_PAY_BILL}${customerId}&page=${pageNumber}&size=${pageSize}&filter=${status}`,
    );
  });

  it('getScheduledSendMoney() should call the correct URL', async () => {
    const mockParameters = {
      customerId: '1',
      pageNumber: 1,
      pageSize: 15,
      status: 'active',
    };
    const {customerId, pageNumber, pageSize, status} = mockParameters;
    schedulesService.getScheduledSendMoney(mockParameters);

    expect(api.get).toHaveBeenCalledWith(
      `${SCHEDULES_API.GET_SCHEDULED_SEND_MONEY}${customerId}&page=${pageNumber}&size=${pageSize}&filter=${status}`,
    );
  });

  it('cancelSchedule() should call the correct URL', async () => {
    const referenceNumber = 1234;
    schedulesService.cancelSchedule(referenceNumber);

    expect(api.delete).toHaveBeenCalledWith(
      `${SCHEDULES_API.CANCEL_SCHEDULE}?refNo=${referenceNumber}`,
    );
  });
});
