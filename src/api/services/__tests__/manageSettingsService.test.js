import api from '@api/';
import {MANAGE_SETTINGS_SERVICE} from '@api/constants';

import manageSettingsService from '../manageSettingsService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('manageSettingsService', () => {
  it('getDevices() should call the correct URL', async () => {
    const customerId = '1';

    manageSettingsService.getDevices(customerId);

    expect(api.get).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.DEVICES + `/${customerId}`,
    );
  });

  it('unlinkDevice() should call the correct URL', async () => {
    const params = 'test';

    manageSettingsService.unlinkDevice(params);

    expect(api.post).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.UNLINK_DEVICE,
      params,
    );
  });

  it('saveDeviceSettings() should call the correct URL', async () => {
    const queryParams = {
      customerId: '1',
      username: 'user',
    };
    const body = 'test';

    manageSettingsService.saveDeviceSettings(queryParams, body);

    expect(api.post).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.SAVE_DEVICE_SETTINGS,
      body,
      {
        params: queryParams,
      },
    );
  });

  it('getDeviceSettings() should call the correct URL', async () => {
    const params = {
      customerId: '1',
      username: 'user',
    };

    manageSettingsService.getDeviceSettings(params);

    expect(api.get).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.SAVE_DEVICE_SETTINGS,
      {
        params,
      },
    );
  });

  it('validateMPIN() should call the correct URL', async () => {
    const params = {data: 'test'};

    manageSettingsService.validateMPIN(params);

    expect(api.post).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.VALIDATE_MPIN,
      params,
    );
  });

  it('updateMPIN() should call the correct URL', async () => {
    const params = {data: 'test'};

    manageSettingsService.updateMPIN(params);

    expect(api.post).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.UPDATE_MPIN,
      params,
    );
  });

  it('getPasswordPolicy() should call the correct URL', async () => {
    const params = {data: 'test'};

    manageSettingsService.getPasswordPolicy(params);

    expect(api.get).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.GET_PASSWORD_POLICY,
    );
  });

  it('changePassword() should call the correct URL', async () => {
    const params = {data: 'test'};

    manageSettingsService.changePassword(params);

    expect(api.post).toHaveBeenCalledWith(
      MANAGE_SETTINGS_SERVICE.CHANGE_PASSWORD,
      params,
    );
  });
});
