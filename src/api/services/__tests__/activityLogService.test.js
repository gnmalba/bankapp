import api from '@api/';
import {ACTIVITY_LOGS_SERVICE} from '@api/constants';

import activityLogService from '../activityLogService';

jest.mock('api');

describe('activityLogService', () => {
  it('get() should call the correct URL', async () => {
    const mockParameters = {
      username: 'user',
      customerId: 1234,
      page: 1,
      size: 1,
    };
    activityLogService.get(mockParameters);

    expect(api.get).toHaveBeenCalledWith(
      `${ACTIVITY_LOGS_SERVICE}?username=${mockParameters.username}&customerId=${mockParameters.customerId}&page=${mockParameters.page}&size=${mockParameters.size}`,
    );
  });
});
