import api from '@api/';
import {PAYBILLS_API_CASA, PAYBILLS_API_CPC} from '@api/constants';

import payBillsCreditService, {apiConds} from '../payBillsCreditService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('payBillsCreditService', () => {
  it.each([
    ['CPC', 'IMMEDIATE_ENROLLED', PAYBILLS_API_CPC.IMMEDIATE_ENROLLED],
    ['CPC', 'IMMEDIATE_UNENROLLED', PAYBILLS_API_CPC.IMMEDIATE_UNENROLLED],
    ['CPC', 'SCHEDULED_ENROLLED', PAYBILLS_API_CPC.SCHEDULED_ENROLLED],
    ['CPC', 'SCHEDULED_UNENROLLED', PAYBILLS_API_CPC.SCHEDULED_UNENROLLED],
    ['CASA', 'IMMEDIATE_ENROLLED', PAYBILLS_API_CASA.IMMEDIATE_ENROLLED],
    ['CASA', 'IMMEDIATE_UNENROLLED', PAYBILLS_API_CASA.IMMEDIATE_UNENROLLED],
    ['CASA', 'SCHEDULED_ENROLLED', PAYBILLS_API_CASA.SCHEDULED_ENROLLED],
    ['CASA', 'SCHEDULED_UNENROLLED', PAYBILLS_API_CASA.SCHEDULED_UNENROLLED],
  ])(
    'payBillsCredit() %s %s should call the correct URL',
    async (whatApi, transactionType, expectedApi) => {
      const data = {
        username: 'user',
        customerId: 1234,
        page: 1,
        size: 1,
      };
      payBillsCreditService.payBillsCredit({
        whatApi,
        data,
        transactionType,
      });

      expect(api.post).toHaveBeenCalledWith(
        apiConds[whatApi](transactionType),
        data,
      );
    },
  );
});
