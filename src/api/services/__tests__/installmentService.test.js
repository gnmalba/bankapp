import api from '@api/';
import {INSTALLMENT_SERVICE} from '@api/constants';

import installmentService from '../installmentService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('installmentService', () => {
  it('getBanks() should call the correct URL', async () => {
    const installmentType = 'BT';
    installmentService.getBanks(installmentType);

    expect(api.get).toHaveBeenCalledWith(INSTALLMENT_SERVICE.BANK_LIST, {
      params: {
        installmentType,
      },
    });
  });

  it('getMoreInformation() should call the correct URL', async () => {
    const params = {
      installmentType: 'BT',
      customerId: '1',
    };
    installmentService.getMoreInformation(params);

    expect(api.get).toHaveBeenCalledWith(INSTALLMENT_SERVICE.MORE_INFORMATION, {
      params,
    });
  });

  it('getPaymentTerms() should call the correct URL', async () => {
    const params = {
      cardSerNo: '1234',
      amount: 10,
      installmentType: 'BT',
      customerId: '1',
    };
    installmentService.getPaymentTerms(params);

    expect(api.get).toHaveBeenCalledWith(INSTALLMENT_SERVICE.PAYMENT_TERMS, {
      params,
    });
  });

  it('balanceTransferSave() should call the correct URL', async () => {
    const params = {
      cardSerNo: '1234',
      amount: 10,
      currency: 'PHP',
      amortization: 5,
      rate: 1,
      totalAmount: 25,
      duration: 15,
      sourceBankName: 'Metrobank',
      sourceBankCardNumber: '08340834',
      sourceBankCardName: 'Card Name',
      passcode: '123456',
      shortCode: '123',
    };
    installmentService.balanceTransferSave(params);

    expect(api.post).toHaveBeenCalledWith(
      INSTALLMENT_SERVICE.BALANCE_TRANSFER_SAVE,
      params,
    );
  });

  it('balanceConversionSave() should call the correct URL', async () => {
    const params = {
      cardSerNo: '1234',
      amount: 10,
      currency: 'PHP',
      amortization: 5,
      rate: 1,
      totalAmount: 25,
      duration: 15,
      passcode: '123456',
      shortCode: '123',
    };
    installmentService.balanceConversionSave(params);

    expect(api.post).toHaveBeenCalledWith(
      INSTALLMENT_SERVICE.BALANCE_CONVERSION_SAVE,
      params,
    );
  });

  it.each([
    ['E', INSTALLMENT_SERVICE.CASH2GO_SAVE.enrolled],
    ['U', INSTALLMENT_SERVICE.CASH2GO_SAVE.unenrolled],
    ['N', INSTALLMENT_SERVICE.CASH2GO_SAVE.nonMb],
  ])(
    'cash2GoSave(), disbursementOption = %s should call the correct URL',
    async (disbursementOption, expectedEndpoint) => {
      const params = {
        disbursementOption,
        cardSerNo: '1234',
        amount: 10,
        currency: 'PHP',
        amortization: 2,
        rate: 5,
        totalAmount: 15,
        duration: 12,
        passcode: '123456',
        shortCode: '123',
        destinationAccountName: 'destination account',
        destinationAccountNumber: '12345678',
      };
      installmentService.cash2GoSave(params);

      expect(api.post).toHaveBeenCalledWith(expectedEndpoint, params);
    },
  );

  it('viewInstallments() should call the correct URL', async () => {
    const params = {
      installmentType: 'BT',
    };
    installmentService.viewInstallments(params);

    expect(api.get).toHaveBeenCalledWith(
      INSTALLMENT_SERVICE.VIEW_INSTALLMENTS,
      {params},
    );
  });

  it('sortInstallments() should call the correct URL', async () => {
    const params = {
      installmentType: 'BT',
    };
    installmentService.sortInstallments(params);

    expect(api.get).toHaveBeenCalledWith(
      INSTALLMENT_SERVICE.SORT_INSTALLMENTS,
      {params},
    );
  });

  it('viewTermsCondition() should call the correct URL', async () => {
    const params = {
      installmentType: 'BT',
    };
    installmentService.viewTermsCondition(params);

    expect(
      api.get,
    ).toHaveBeenCalledWith(INSTALLMENT_SERVICE.VIEW_TERMS_CONDITION, {params});
  });
});
