import api from '@api/';
import {BILLERS_SERVICE} from '@api/constants';

import billerService from '../billerService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('billerService', () => {
  it('getBillerCategories() should call the correct URL', () => {
    billerService.getBillerCategories();
    expect(api.get).toHaveBeenCalledWith(`${BILLERS_SERVICE.CATEGORIES}`);
  });

  it('getAllBillers() should call the correct URL', () => {
    billerService.getAllBillers();
    expect(api.get).toHaveBeenCalledWith(`${BILLERS_SERVICE.ALL_BILLERS}`);
  });

  it('getEnrolledBillers() should call the correct URL', () => {
    const customerId = 1;
    billerService.getEnrolledBillers(customerId);
    expect(api.get).toHaveBeenCalledWith(
      `${BILLERS_SERVICE.ENROLLED_BILLERS}/${customerId}/billers`,
    );
  });

  it('getMboaEnrolledBillers() should call the correct URL', () => {
    const customerId = 1;
    billerService.getMboaEnrolledBillers(customerId);
    expect(api.get).toHaveBeenCalledWith(
      `${BILLERS_SERVICE.MBOA}/${customerId}/${BILLERS_SERVICE.ENROLLED_BILLERS_MBOA}`,
    );
  });

  it('getBillerFieldsUnenrolled() should call the correct URL', () => {
    const billerId = 1;
    billerService.getBillerFieldsUnenrolled(billerId);
    expect(api.get).toHaveBeenCalledWith(
      `${BILLERS_SERVICE.ALL_BILLERS}/${billerId}/fields/payment-unenrolled`,
    );
  });

  it('getBillerFieldsEnrolled() should call the correct URL', () => {
    const billerId = 1;
    billerService.getBillerFieldsEnrolled(billerId);
    expect(api.get).toHaveBeenCalledWith(
      `${BILLERS_SERVICE.ALL_BILLERS}/${billerId}/fields/payment-enrolled`,
    );
  });
});
