import api from '@api/';
import {REPORT_LOST_SERVICE} from '@api/constants';

import reportLostCardService from '../reportLostCardService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('reportLostCardService', () => {
  it('get() should call the correct URL', async () => {
    const customerId = '1';
    const status = 'blocking';
    reportLostCardService.get(customerId, status);

    expect(api.get).toHaveBeenCalledWith(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD}?customerId=${customerId}&status=${status}`,
    );
  });

  it('getCardDetails() should call the correct URL', async () => {
    const params = {
      customerId: '1',
      cardSerNo: '123456',
    };
    reportLostCardService.getCardDetails(params);

    expect(api.get).toHaveBeenCalledWith(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD_DETAILS}?customerId=${params.customerId}&cardSerialNo=${params.cardSerNo}`,
    );
  });

  it('reportCard() should call the correct URL', async () => {
    const params = {
      customerId: '1',
      cardSerialNo: '123456',
      username: 'user',
      passcode: 'password',
    };
    reportLostCardService.reportCard(params);

    expect(api.post).toHaveBeenCalledWith(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD_BLOCKING}?customerId=${params.customerId}`,
      {
        ...params,
      },
    );
  });

  it('getTermsAndConditions() should call the correct URL', async () => {
    const params = {
      customerId: '1',
      cardSerNo: '123456',
    };
    reportLostCardService.getTermsAndConditions(params);

    expect(api.get).toHaveBeenCalledWith(
      `${REPORT_LOST_SERVICE.CC_LOSTCARD_TERMS_CONDITIONS}?customerId=${params.customerId}&cardSerNo=${params.cardSerNo}`,
    );
  });
});
