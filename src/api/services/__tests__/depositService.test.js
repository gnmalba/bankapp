import api from '@api/';

import {DASHBOARD_SERVICE, TRANSACTION_HISTORY_SERVICE} from '@api/constants';

import depositService from '../depositService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('depositService', () => {
  it('getPesoBalance() should call the correct URL', async () => {
    const customerId = 1;
    depositService.getPesoBalance(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${DASHBOARD_SERVICE.PESOS_ACCOUNT_BALANCES}/${customerId}`,
    );
  });

  it('getPesoTotalBalance() should call the correct URL', async () => {
    const customerId = 1;
    depositService.getPesoTotalBalance(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${DASHBOARD_SERVICE.PESOS_TOTAL_BALANCE}/${customerId}`,
    );
  });

  it('getYazz() should call the correct URL', async () => {
    const customerId = 1;
    depositService.getYazz(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${DASHBOARD_SERVICE.CREDIT_CARD_LIST_ACCOUNTS}?customerId=${customerId}&cardTypes=prepaidcard`,
    );
  });

  it('getForeignBalance() should call the correct URL', async () => {
    const customerId = 1;
    depositService.getForeignBalance(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${DASHBOARD_SERVICE.FOREIGN_ACCOUNT_BALANCES}/${customerId}`,
    );
  });

  it('getAccountDetails() should call the correct URL', async () => {
    const customerId = 1;
    depositService.getAccountDetails(customerId);

    expect(api.get).toHaveBeenCalledWith(`casas/${customerId}/balance`);
  });

  it('getTransactionHistory() should call the correct URL', async () => {
    const customerId = 1;
    depositService.getTransactionHistory(customerId);

    expect(api.get).toHaveBeenCalledWith(
      `casas/${customerId}/transaction-history`,
    );
  });

  it('getTransactionCodes() should call the correct URL', async () => {
    depositService.getTransactionCodes();

    expect(api.get).toHaveBeenCalledWith(
      `${TRANSACTION_HISTORY_SERVICE.TRANSACTION_CODE}`,
    );
  });
});
