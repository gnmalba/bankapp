import api from '@api/';
import {ENROLLMENT_SERVICE} from '@api/constants';

import enrollmentService from '../enrollmentService';

jest.mock('api');

afterEach(() => {
  jest.clearAllMocks();
});

describe('enrollmentService', () => {
  it('getYazzBins() should call the correct URL', async () => {
    enrollmentService.getYazzBins();

    expect(api.get).toHaveBeenCalledWith(ENROLLMENT_SERVICE.YAZZ_BINS);
  });

  it('getEnrollmentPublicKey() should call the correct URL', async () => {
    enrollmentService.getEnrollmentPublicKey();

    expect(api.get).toHaveBeenCalledWith(ENROLLMENT_SERVICE.PUBLIC_KEY);
  });

  it('getAccountDetails() should call the correct URL', async () => {
    const accountNo = 1;
    enrollmentService.getAccountDetails(accountNo);

    expect(api.get).toHaveBeenCalledWith(
      `${ENROLLMENT_SERVICE.CASAS}/${accountNo}/details`,
    );
  });

  it('enrollDepositCard() should call the correct URL', async () => {
    const values = {
      accountNo: '1234',
      alias: 'test',
      customerId: 1,
    };
    const payload = {values};

    enrollmentService.enrollDepositCard(payload);

    expect(api.post).toHaveBeenCalledWith(
      ENROLLMENT_SERVICE.DEPOSIT_ACCOUNT_ENROLLMENT,
      {
        ...values,
      },
    );
  });

  it('enrollCreditCard() should call the correct URL', async () => {
    const values = {
      accountNo: '1234',
      alias: 'test',
      nameOnCard: 'Card Owner',
      dateOfBirth: '31 January 1990',
      customerId: 1,
    };
    const payload = {values};

    enrollmentService.enrollCreditCard(payload);

    expect(api.post).toHaveBeenCalledWith(
      ENROLLMENT_SERVICE.CREDIT_ACCOUNT_ENROLLMENT,
      {
        ...values,
      },
    );
  });
});
