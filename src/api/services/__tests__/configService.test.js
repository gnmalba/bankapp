import {getDeviceId} from 'react-native-device-info';
import api from '@api/';
import {SETTINGS_SERVICE} from '@api/constants';

import configService from '../configService';

jest.mock('api');

describe('configService', () => {
  it('get() should call the correct URL', async () => {
    const username = 'user';
    const customerId = '1234';

    configService.get(username, customerId);

    expect(api.get).toHaveBeenCalledWith(
      `${
        SETTINGS_SERVICE.DEVICE
      }?deviceId=${getDeviceId()}&username=${username}&customerId=${customerId}`,
    );
  });
});
