/**
 * @flow
 */

import api from '@api/';
import {AxiosResponse} from 'axios';
import {BILLERS_SERVICE} from '@api/constants';

type Response = typeof AxiosResponse;

const billerConfig = {
  getBillerCategories: (): Response => api.get(`${BILLERS_SERVICE.CATEGORIES}`),
  getAllBillers: (): Response => api.get(`${BILLERS_SERVICE.ALL_BILLERS}`),
  getEnrolledBillers: (customerId: number): Response =>
    api.get(`${BILLERS_SERVICE.ENROLLED_BILLERS}/${customerId}/billers`),
  getMboaEnrolledBillers: (customerId: number): Response =>
    api.get(
      `${BILLERS_SERVICE.MBOA}/${customerId}/${BILLERS_SERVICE.ENROLLED_BILLERS_MBOA}`,
    ),
  getBillerFieldsUnenrolled: (billerId: number): Response =>
    api.get(
      `${BILLERS_SERVICE.ALL_BILLERS}/${billerId}/fields/payment-unenrolled`,
    ),
  getBillerFieldsEnrolled: (billerId: number): Response =>
    api.get(
      `${BILLERS_SERVICE.ALL_BILLERS}/${billerId}/fields/payment-enrolled`,
    ),
};

export default billerConfig;
