import api from '@api/';
import mockAdapter from 'axios-mock-adapter';
import ConfigService from '../services/configService';
import {SETTINGS_SERVICE} from '@api/constants';

const mockApi = new mockAdapter(api);

jest.mock('@api/utils', () => ({
  getDefaultDeviceSettingsConfig: () => ({
    'X-DEVICE-ID': '56789ghjk56789ovbnm5678',
  }),
}));

describe('ConfigService', () => {
  test('ConfigService', async () => {
    mockApi
      .onGet(
        `${SETTINGS_SERVICE.DEVICE}?deviceId=56789ghjk56789ovbnm5678&username=test&customerId=1111`,
      )
      .reply(200);
    const res = await ConfigService.get('test', 1111);
    expect(res.status).toEqual(200);
  });
});
