import api from '@api/';
import mockAdapter from 'axios-mock-adapter';
import billerConfig from '../services/billerService';
import {BILLERS_SERVICE} from '@api/constants';

const mockApi = new mockAdapter(api);

mockApi.onGet(`${BILLERS_SERVICE.CATEGORIES}`).reply(200);
mockApi.onGet(`${BILLERS_SERVICE.ALL_BILLERS}`).reply(200);
mockApi.onGet(`${BILLERS_SERVICE.ENROLLED_BILLERS}/1111/billers`).reply(200);
mockApi
  .onGet(
    `${BILLERS_SERVICE.MBOA}/1111/${BILLERS_SERVICE.ENROLLED_BILLERS_MBOA}`,
  )
  .reply(200);
mockApi
  .onGet(`${BILLERS_SERVICE.ALL_BILLERS}/1111/fields/payment-unenrolled`)
  .reply(200);
mockApi
  .onGet(`${BILLERS_SERVICE.ALL_BILLERS}/1111/fields/payment-enrolled`)
  .reply(200);

describe('billerConfig', () => {
  it('getBillerCategories', async () => {
    const res = await billerConfig.getBillerCategories();
    expect(res.status).toEqual(200);
  });
  it('getAllBillers', async () => {
    const res = await billerConfig.getAllBillers();
    expect(res.status).toEqual(200);
  });
  it('getEnrolledBillers', async () => {
    const res = await billerConfig.getEnrolledBillers(1111);
    expect(res.status).toEqual(200);
  });
  it('getMboaEnrolledBillers', async () => {
    const res = await billerConfig.getMboaEnrolledBillers(1111);
    expect(res.status).toEqual(200);
  });
  it('getBillerFieldsUnenrolled', async () => {
    const res = await billerConfig.getBillerFieldsUnenrolled(1111);
    expect(res.status).toEqual(200);
  });
  it('getBillerFieldsEnrolled', async () => {
    const res = await billerConfig.getBillerFieldsEnrolled(1111);
    expect(res.status).toEqual(200);
  });
});
