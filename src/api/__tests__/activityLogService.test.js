import api from '@api/';
import mockAdapter from 'axios-mock-adapter';
import activityLogService from '../services/activityLogService';
import {ACTIVITY_LOGS_SERVICE} from '@api/constants';

const mockApi = new mockAdapter(api);

describe('activityLogService', () => {
  test('activityLogService', async () => {
    mockApi
      .onGet(
        `${ACTIVITY_LOGS_SERVICE}?username=test&customerId=1111&page=1&size=1`,
      )
      .reply(200);
    const res = await activityLogService.get({
      username: 'test',
      customerId: 1111,
      page: 1,
      size: 1,
    });
    expect(res.status).toEqual(200);
  });
});
