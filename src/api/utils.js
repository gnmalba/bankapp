/**
 * @flow
 */

import {Platform} from 'react-native';

import {
  getUniqueId,
  getSystemVersion,
  getDeviceId,
} from 'react-native-device-info';

import {X_DEVICE, X_DEVICE_ID, X_OS} from '@utils/constants';
import {IS_STATIC_DEVICE} from 'react-native-dotenv';

type DeviceConfig = {
  'X-DEVICE': string,
  'X-DEVICE-ID': string,
  'X-OS': string,
};

export const getDefaultDeviceSettingsConfig = (): DeviceConfig => {
  const deviceConfig = {
    'X-DEVICE': X_DEVICE,
    'X-DEVICE-ID': X_DEVICE_ID,
    'X-OS': X_OS,
    'Cache-Control': 'no-cache',
  };

  if (IS_STATIC_DEVICE) {
    return deviceConfig;
  }

  return {
    ...deviceConfig,
    'X-DEVICE': getDeviceId(),
    'X-DEVICE-ID': getUniqueId(),
    'X-OS': `${Platform.OS} ${getSystemVersion()}`,
  };
};
