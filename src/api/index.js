/**
 * @flow
 */

import axios, {AxiosInstance} from 'axios';

import store from '@ducks/store';
import {
  AUTHENTICATION_START_LOGOUT,
  AUTHENTICATION_ANOTHER_DEVICE_LOGIN,
} from '@ducks/authentication/actionTypes';

import {BASE_URL} from 'react-native-dotenv';

import {getDefaultDeviceSettingsConfig} from './utils';

/**
 * ===================================
 * START OF AXIOS CONFIGURATION
 * ===================================
 */

const config = {
  baseURL: BASE_URL,
  headers: {
    ...getDefaultDeviceSettingsConfig(),
    'Cache-Control': 'no-cache',
  },
  timeout: 10000,
};

/**
 * ===================================
 * END OF AXIOS CONFIGURATION
 * ===================================
 */

const axiosInstance: typeof AxiosInstance = axios.create(config);

axiosInstance.interceptors.request.use(
  async (requestConfig) => {
    const {
      authentication: {accessToken},
    } = store.getState();

    if (accessToken) {
      requestConfig.headers.Authorization = `Bearer ${accessToken}`;
    } else {
      requestConfig.headers.Authorization = '';
    }
    return requestConfig;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  async (requestConfig) => {
    const {
      authentication: {accessToken},
    } = store.getState();
    requestConfig.headers = {
      Authorization: `Bearer ${accessToken}`,
      Accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    return requestConfig;
  },
  async function (error) {
    if (error.response?.status === 401) {
      store.dispatch({
        type: AUTHENTICATION_START_LOGOUT,
        payload: {
          onSuccess: AUTHENTICATION_ANOTHER_DEVICE_LOGIN,
        },
      });
    }
    return Promise.reject(error);
  },
);

export default axiosInstance;
