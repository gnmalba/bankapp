/**
 * MCC Frontend
 * https://sra-git.yondu.technology/cluster3/metrobank/mcc-mobile-app/mccfrontend
 *
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {Provider} from 'react-redux';

import store from '@ducks/store';
import RootNavigation from '@navigations/';
import SplashScreen from 'react-native-splash-screen';

const App = (): React$Node => {
  useEffect(() => {
    //   setTimeout(() => {
    //     SplashScreen.hide();
    //   }, 1000);
    SplashScreen.hide();
  }, []);
  return (
    <Provider store={store}>
      <RootNavigation />
    </Provider>
  );
};

export default App;
