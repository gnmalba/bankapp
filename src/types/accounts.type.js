// @flow

import type {IShortcut} from './shortcut.type';
import type {ILimits} from './common.type';

export type IAccount = {
  id: number,
  accountNo: string,
  customerId: string,
  productType: string,
  alias: string,
  currencyCode: string,
  currencyHostCode: string,
  initialAccount: boolean,
  flag: string | null,
  accountType: 'Savings', // add more if not handled
  accountTypeDescription: string,
  mobile: null,
  branch: string,
  relationshipCode: string,
  created: string,
  shortcuts: IShortcut[],
  allowedTransactions: IAllowedTransactions[],
};

export type IAllowedTransactions = {
  id: number,
  code: string,
  name: string,
  enabled: boolean,
  forShortcut: boolean,
  icon: string,
  limits?: ILimits[],
  debit: boolean,
  credit: boolean,
};
