// @flow

import type {ILimits} from './common.type';

export type IShortcut = {
  code: string,
  credit: boolean,
  debit: boolean,
  enabled: boolean,
  forShortcut: boolean,
  icon: string,
  id: number,
  name: string,
  limits?: ILimits[],
};
