// @flow

export type IAmount = {
  value: number,
  currencyCode: string,
};

export type IBalance = {
  value: number,
  currencyCode: string,
};

export type ILimits = {
  id: number,
  type: string,
  amount: IAmount,
};
