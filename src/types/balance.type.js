/**
 * @flow
 * @format
 */

import type {IBalance} from './common.type';
import type {IAccount} from './accounts.type';

export type IPesoBalance = {
  customerId: string,
  currencyCode: string,
  totalAvailableBalance: IBalance,
  totalCurrentBalance: IBalance,
};

export type ForeignAccountBalance = {
  depositAccount: IAccount,
  availableBalance: IBalance,
  currentBalance: IBalance,
  active: boolean,
};
