// @flow

import type {IShortcut} from './shortcut.type';

export type ICard = {
  cardNo: string,
  cardSerNo: string | null,
  cardEmbossedName: string | null,
  customerId: string | null,
  primary: number | null,
  flag: string | null,
  outstandingBalance: number | null,
  availableCredit: number | null,
  creditLimit: number | null,
  alias: string,
  currency: string | null,
  type: 'creditcard' | 'prepaidcard',
  shortcuts: IShortcut[] | null,
};
