/**
 * @flow
 */

import type {
  BillerAction,
  Billers,
  BillerCategory,
  BillerByCategory,
  AllBillers,
  Biller,
} from './types';

const getAllBillersRequested = (): BillerAction => ({
  type: 'GET_BILLER_REQUEST',
});

const getAllBillerSuccess = (
  billers: Billers,
  categories: Array<BillerCategory>,
  allBillersByCategory: BillerByCategory,
  payBillsAllBillers: AllBillers,
  payBillsEnrolledBiller: Array<Biller>,
): BillerAction => ({
  type: 'GET_BILLER_SUCCESS',
  payload: {
    billers,
    categories,
    allBillersByCategory,
    payBillsAllBillers,
    payBillsEnrolledBiller,
  },
});

const setPbcBiller = (
  id: string,
  billerType: string,
  setFieldValue: any,
): BillerAction => ({
  type: 'SELECT_PBC_BILLER',
  payload: {id, billerType, setFieldValue},
});

const completedPbcBillerSelect = (newSelectedBiller: Biller): BillerAction => ({
  type: 'COMPLETE_PBC_SELECT',
  payload: newSelectedBiller,
});

const selectPbcCategory = (categoryId: string): BillerAction => ({
  type: 'SELECT_PBC_CATEGORY',
  payload: categoryId,
});

const resetBillers = (): BillerAction => ({
  type: 'RESET_BILLERS',
});

export {
  getAllBillersRequested,
  getAllBillerSuccess,
  setPbcBiller,
  completedPbcBillerSelect,
  selectPbcCategory,
  resetBillers,
};
