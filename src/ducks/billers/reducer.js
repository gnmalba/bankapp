/**
 * @flow
 */

import type {BillerState, BillerAction, ActionMapperType} from './types';

const INITIAL_STATE: BillerState = {
  loading: false,
  billers: {
    enrolledBillers: [],
    allBillers: [],
  },
  selectedBiller: {},
  allBillersByCategory: [],
  categories: [],
  payBillsSelectedBiller: null,
  payBillsAllBillers: {
    alphabetical: [],
    section: [],
  },
  payBillsEnrolledBiller: [],
  selectedBillerId: '',
  selectedBillerType: '', //allBillers === unenrolled billers
  payBillsSelectedCategory: '',
};

const actionMap = {
  GET_BILLER_REQUEST: ({state, payload}: ActionMapperType): BillerState => {
    return {
      ...state,
      loading: true,
    };
  },
  GET_BILLER_SUCCESS: ({state, payload}: ActionMapperType): BillerState => {
    const {
      billers,
      categories,
      allBillersByCategory,
      payBillsAllBillers,
      payBillsEnrolledBiller,
    } = payload;
    return {
      ...state,
      loading: false,
      billers,
      categories,
      allBillersByCategory,
      payBillsAllBillers,
      payBillsEnrolledBiller,
    };
  },

  GET_ENROLLED_BILLER_SUCCESS: ({
    state,
    payload,
  }: ActionMapperType): BillerState => {
    return {
      ...state,
      loading: false,
      billers: {
        ...state.billers,
        enrolledBillers: payload,
      },
    };
  },

  GET_BILLER_FAILED: ({state, payload}: ActionMapperType): BillerState => {
    return state;
  },
  SELECT_PBC_BILLER: ({state, payload}: ActionMapperType): BillerState => {
    const {id, billerType} = payload;
    return {
      ...state,
      loading: true,
      selectedBillerId: id,
      selectedBillerType: billerType,
    };
  },
  COMPLETE_PBC_SELECT: ({state, payload}: ActionMapperType): BillerState => {
    return {...state, loading: false, payBillsSelectedBiller: payload};
  },
  SELECT_PBC_CATEGORY: ({state, payload}: ActionMapperType): BillerState => {
    return {...state, payBillsSelectedCategory: payload};
  },
  RESET_BILLERS: ({state, payload}: ActionMapperType): BillerState => {
    return INITIAL_STATE;
  },
};

const biller = (
  state: BillerState = INITIAL_STATE,
  action: BillerAction,
): BillerState => {
  if (actionMap[action.type]) {
    return actionMap[action.type]({state, payload: action.payload || {}});
  }
  return state;
};

export default biller;
