/**
 * @flow
 */

import {takeLatest, call, put, all, select, delay} from 'redux-saga/effects';
import type {
  AllBillerResponse,
  EnrolledBillerResponse,
  MboaEnrolledBillerResponse,
} from './types';

import api from '@api/services/billerService';
import {getCustomerID} from '@ducks/commonSelectors';
import {
  getAllBillersRequested,
  getAllBillerSuccess,
  completedPbcBillerSelect,
} from './actions';
import {
  allBillerResponseToArrayObject,
  categoriesResponseToArrayObject,
  enrolledBillerResponseToArrayObject,
  formatBillersByCategory,
  formatBillerforPbc,
  billersByAlphabet,
} from './utils';

import {
  selectedBillerIdSelector,
  billersSelector,
  selectedBillerTypeSelector,
} from './selector';

import {payBillsAccountsSelector} from '@ducks/pay-bills-credit/selector';

import {
  ifOnlyOneAccountGetAccount,
  handleNullValue,
} from '@ducks/pay-bills-credit/utils';

import {initializePbcComplete} from '@ducks/pay-bills-credit/actions';

import type {Account, Accounts} from '@ducks/pay-bills-credit/types';
import type {Biller, Billers} from './types';
import {iterateEntries} from '@utils/commons';
import {navigate} from '@navigations/service';

import {isPbcInitial} from '@ducks/pay-bills-credit/selector';

function* getAllBillers(): Generator<any, AllBillerResponse, any> {
  let returnedResultSets = [];
  try {
    const response = yield call(api.getAllBillers);
    return response.data.data;
  } catch (error) {
    return returnedResultSets;
  }
}

function* getEnrolledBillers(
  customerId,
): Generator<any, EnrolledBillerResponse, any> {
  let returnedResultsets = [];
  try {
    const response = yield call(api.getEnrolledBillers, customerId);
    return response.data.data;
  } catch (apiErrorResponse) {
    return returnedResultsets;
  }
}

function* getBillersEnrolled(): Generator<any, any, any> {
  try {
    // const customerId = yield select(getCustomerID);
    yield delay(5000);
    yield put({type: 'GET_ENROLLED_BILLER_SUCCESS', payload: []});
  } catch (apiErrorResponse) {
    console.log('error in getEnrolledBillers2: ', apiErrorResponse);
  }
}

function* getMboaEnrolledBillers(customerId): Generator<any, any, any> {
  let returnedResultsets = [];
  try {
    const response = yield call(api.getMboaEnrolledBillers, customerId);
    return response.data.data;
  } catch (apiErrorResponse) {
    return returnedResultsets;
  }
}

function* getBillerCategories(): Generator<
  any,
  MboaEnrolledBillerResponse,
  any,
> {
  let returnedResultsets = [];
  try {
    const response = yield call(api.getBillerCategories);
    return response.data.data;
  } catch (apiErrorResponse) {
    return returnedResultsets;
  }
}

function* getBillerFields(
  id: string,
  type: string,
): Generator<any, Array<Object>, any> {
  let returnedResultsets = [];
  const billerType = {
    allBillers: 'getBillerFieldsUnenrolled',
    enrolledBillers: 'getBillerFieldsEnrolled',
  };

  try {
    const response = yield call(api[billerType[type]], id);

    return response.data.data.fields;
  } catch (apiErrorResponse) {
    return returnedResultsets;
  }
}
export function* processAllAndEnrolledBiller(): Generator<any, any, any> {
  try {
    const customerId = yield select(getCustomerID);

    yield put(getAllBillersRequested());

    const [
      allBillerRespone,
      enrolledBillerResponse,
      categoriesResponse,
      mboaEnrolledBillers,
    ] = yield all([
      call(getAllBillers),
      call(getEnrolledBillers, customerId),
      call(getBillerCategories),
      call(getMboaEnrolledBillers, customerId),
    ]);

    const allBillers = allBillerResponseToArrayObject(allBillerRespone);
    const enrolledBillers = enrolledBillerResponseToArrayObject(
      enrolledBillerResponse,
      allBillerRespone,
      mboaEnrolledBillers,
    );
    const billers = {
      enrolledBillers: enrolledBillers,
      allBillers: allBillers,
    };
    const categories = categoriesResponseToArrayObject(categoriesResponse);

    const allBillersByCategory = formatBillersByCategory(
      billers.allBillers,
      categories,
    );

    const billersBySection = formatBillerforPbc(allBillersByCategory);

    const payBillsAllBillers = {
      alphabetical: billersByAlphabet(billers.allBillers),
      section: billersBySection,
    };

    yield put(
      getAllBillerSuccess(
        billers,
        categories,
        allBillersByCategory,
        payBillsAllBillers,
        billers.enrolledBillers,
      ),
    );
  } catch (error) {}
}

const conditions = {
  isBillerCreditCard: (biller: Biller) =>
    biller.category?.code === 'CREDIT_CARD',
  isAccountCreditCard: (account: Account) =>
    account.accountNo === null ? true : false,
};

export function* processAccountsAndBillerFields(
  action: Object,
): Generator<any, any, any> {
  const {setFieldValue} = action.payload;

  const selectedBillerId: ?Biller = yield select(selectedBillerIdSelector);
  const getBillers: Billers = yield select(billersSelector);
  const billerType: string = yield select(selectedBillerTypeSelector);

  const billerFieldsResponse = yield call(
    getBillerFields,
    selectedBillerId,
    billerType,
  );

  const selectedBiller: Biller = getBillers[billerType].find(
    (billerRow: Biller) => billerRow.billerId === selectedBillerId,
  );

  const PbcSourceAccounts: Accounts = yield select(payBillsAccountsSelector);

  const isInitial: boolean = yield select(isPbcInitial);

  const currencyCode = selectedBiller.currencyCode;

  const filterSourceAccounts = (accounts: Accounts): Accounts =>
    iterateEntries(accounts, (accountRow: Array<Account>) =>
      accountRow.filter((row: Account) => row.currencyCode === currencyCode),
    );

  const isCreditCard = (row: Account) => !conditions.isAccountCreditCard(row);

  const payBillsAccount = iterateEntries(
    filterSourceAccounts(PbcSourceAccounts),
    (accountRow: Array<Account>) =>
      accountRow.filter((row: Account) =>
        conditions.isBillerCreditCard(selectedBiller)
          ? isCreditCard(row)
          : true,
      ),
  );

  if (!isInitial) {
    yield put(
      initializePbcComplete(
        PbcSourceAccounts,
        handleNullValue(ifOnlyOneAccountGetAccount(payBillsAccount.all)),
        payBillsAccount,
      ),
    );
  }

  setFieldValue('biller', {
    ...selectedBiller,
    fields: {
      inputFields: billerFieldsResponse,
      labelFields: selectedBiller.fields.labelFields,
    },
  });

  yield put(
    completedPbcBillerSelect({
      ...selectedBiller,
      fields: {
        inputFields: billerFieldsResponse,
        labelFields: selectedBiller.fields.labelFields,
      },
    }),
  );

  navigate('PayBillsCredit', {screen: 'MainScreen'});
}

function* billerWatcher(): Generator<any, any, any> {
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', processAllAndEnrolledBiller);
  yield takeLatest('SELECT_PBC_BILLER', processAccountsAndBillerFields);
  yield takeLatest('GET_ENROLLED_BILLER_REQUEST', getBillersEnrolled);
}
export default billerWatcher;
