/**
 * @flow
 */
import type {
  AllBillerResponse,
  EnrolledBillerResponse,
  Biller,
  EnrolledBillerApiObject,
  AllBillerApiObject,
  BillerCategory,
  BillerCategoryResponse,
  BillerCategoryObject,
  BillerByCategory,
  MboaEnrolledBillerResponse,
  MboaEnrolledBilleApiObject,
  CategoryWithBillers,
  PbcBillers,
} from './types';

import {sortByObjectKey} from '@utils/commons';

import {alphabet} from '@composition/AlphabetList/utils';

const formatBillerObject = (object: Object) => {
  return {
    billerId: object.billerId,
    name: object.billerName,
    category: object.category,
    currencyCode: object.currencyCode,
    allowedChannel: object.allowedChannel,
    alias: object.alias,
    contactId: object.contactId,
    icon: object.icon,
    createdDate: object.createdDate,
    updateDate: object.updateDate,
    fields: {
      inputFields: [],
      labelFields: object.fields,
    },
    bins: [],
  };
};

const getCorrespondingBiller = (
  allBillersResponse: AllBillerResponse,
  enrolledBillerRow: EnrolledBillerApiObject,
): ?AllBillerApiObject =>
  allBillersResponse.find(
    (allBillerRow: AllBillerApiObject) =>
      allBillerRow.id.toString() === enrolledBillerRow.billerId,
  );

const getCorrespondingMboaBiller = (
  mboaBillersResponse: MboaEnrolledBillerResponse,
  enrolledBillerRow: EnrolledBillerApiObject,
): ?Object =>
  mboaBillersResponse?.find(
    (mboaBiller: MboaEnrolledBilleApiObject) =>
      mboaBiller.billerId.toString() === enrolledBillerRow.billerId,
  );

const isAllowedChannel = (element: Biller): boolean =>
  element.allowedChannel !== 'WEB';

export const allBillerResponseToArrayObject = (
  response: AllBillerResponse,
): Array<Biller> =>
  response
    .map((billerRow: AllBillerApiObject) => {
      return {
        billerId: billerRow.id.toString(),
        name: billerRow.name,
        category: billerRow.category,
        currencyCode: billerRow.currencyCode,
        allowedChannel: billerRow.allowedChannel,
        fields: {
          inputFields: [],
          labelFields: [],
        },
        alias: '',
        contactId: '',
        bins: billerRow.bins,
      };
    })
    .filter(isAllowedChannel)
    .sort(sortByObjectKey('name'));

/**
 * Map the corresponding biller for the enrolled biller and remove biller under 'WEB'
 * @param {*} enrolledBillerResponse
 * @param {*} allBillersResponse
 */
export const enrolledBillerResponseToArrayObject = (
  enrolledBillerResponse: EnrolledBillerResponse,
  allBillersResponse: AllBillerResponse,
  mboaBillerResponse: MboaEnrolledBillerResponse,
): Array<Biller> =>
  enrolledBillerResponse
    .map((enrolledBillerRow: EnrolledBillerApiObject) => {
      const correspondingBiller = getCorrespondingBiller(
        allBillersResponse,
        enrolledBillerRow,
      );

      const correspondingMboaBiller = getCorrespondingMboaBiller(
        mboaBillerResponse,
        enrolledBillerRow,
      );

      if (correspondingBiller && correspondingMboaBiller) {
        return formatBillerObject({
          ...enrolledBillerRow,
          category: correspondingBiller.category,
          allowedChannel: correspondingBiller.allowedChannel,
          icon: correspondingMboaBiller.billerIcon,
          createdDate: correspondingMboaBiller.createdDate,
          updateDate: correspondingMboaBiller.updateDate,
        });
      }
      return formatBillerObject(enrolledBillerRow);
    })
    .filter(isAllowedChannel)
    .sort(sortByObjectKey('alias'));

export const categoriesResponseToArrayObject = (
  response: BillerCategoryResponse,
): Array<BillerCategory> =>
  response.map((row: BillerCategoryObject) => {
    const {name, id, code} = row;
    return {
      name,
      id,
      code,
    };
  });
export const formatBillersByCategory = (
  allBillers: Array<Biller>,
  categories: Array<BillerCategory>,
): BillerByCategory =>
  categories
    .map((categoryRow: BillerCategory) => {
      return {
        ...categoryRow,
        billers: allBillers.filter(
          (allBillerRow: Biller) =>
            allBillerRow.category &&
            allBillerRow.category.id === categoryRow.id,
        ),
      };
    })
    .filter(checkIfBillersAreEmpty);

const checkIfBillersAreEmpty = (row: CategoryWithBillers): boolean =>
  row.billers.length > 0;
/**
 *
 * @param {*} biller
 * @returns [{title: "Category Name", billres: [{id: 1 , name: "Biller Name"}]}]
 */
export const formatBillerforPbc = (biller: BillerByCategory): PbcBillers =>
  biller.map((row: CategoryWithBillers) => {
    return {
      title: row.name,
      data: row.billers.map((billerRow, index) => {
        return {
          id: billerRow.billerId,
          name: billerRow.name,
          isLastItem: index === row.billers.length - 1,
        };
      }),
    };
  });

export const billersByAlphabet = (billers: Array<Biller>): PbcBillers =>
  alphabet.map((letter) => {
    const billarsStartWithLetter = billers
      .filter((biller: Biller) => biller.name.toUpperCase().startsWith(letter))
      .map((row) => {
        return {
          id: row.billerId,
          name: row.name,
        };
      });
    return {
      title: letter,
      data: billarsStartWithLetter.map((row, index) => {
        return {
          ...row,
          isLastItem: billarsStartWithLetter.length - 1 === index,
        };
      }),
    };
  });
