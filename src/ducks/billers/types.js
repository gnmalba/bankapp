/**
 * @flow
 */

export type BillerState = {
  loading: boolean,
  billers: Billers,
  categories: Array<BillerCategory>,
  payBillsAllBillers: AllBillers,
  allBillersByCategory: BillerByCategory,
  errorMessage?: BillerError,
  payBillsSelectedBiller?: ?Biller,
  payBillsEnrolledBiller: Array<Biller>,
  selectedBillerId: string,
  selectedBillerType: string,
  payBillsSelectedCategory: string,
};

export type AllBillers = {
  alphabetical: PbcBillers,
  section: PbcBillers,
};

export type PbcBillers = Array<{
  title: string,
  data: Array<{id: string, name: string, isLastItem: boolean}>,
}>;

export type BillerError = {
  code: string,
  type?: string,
  message?: string,
};

export type BillerFields = {
  forEnrollment: boolean,
  forPayment: boolean,
  id: number,
  label: string,
  name: string,
  placeholder: string,
  position: number,
  regularExpression: string,
  required: boolean,
  validation: ValidationRule[],
  value?: string,
};

export type ValidationRule = {
  ruleCategory: RuleCategory,
  referenceValue: null,
};

export type RuleCategory = {
  name: 'MIN_LENGTH' | 'MAX_LENGTH' | 'NUMERIC' | 'ALPHANUMBERIC',
  description: string,
  referenceValue?: string,
};

export type Billers = {
  enrolledBillers: Array<Biller>,
  allBillers: Array<Biller>,
};

export type Biller = {
  billerId: string,
  name: string,
  currencyCode: string,
  allowedChannel: string,
  category: ?BillerCategory,
  alias?: ?string,
  contactId?: string,
  icon?: ?string,
  createdDate?: string,
  reminder?: any, //dont know what object yet will type any for now
  updateDate?: ?string,
  fields: {
    inputFields: Array<BillerFields>,
    labelFields: Array<Field>,
  },
  bins: Array<Bin>,
};

export type BillerCategory = {
  id: ?number,
  name: string,
  code: string,
};
export type CategoryWithBillers = BillerCategory & {billers: Array<Biller>};

export type BillerByCategory = Array<CategoryWithBillers>;

export type Bin = {
  bin: string,
  id: number,
};

export type AllBillerApiObject = {
  allowedChannel: string,
  bins: Array<Bin>,
  category: BillerCategory,
  description: string,
  enrollmentEnabled: boolean,
  id: number,
  mid: string,
  name: string,
  currencyCode: string,
  paymentEnabled: boolean,
  restrictedForCC: boolean,
  shortName: string,
  tid: number,
  type: {id: string, code: string, name: string},
  utilityCode: string,
};

export type Field = {
  label: string,
  id: string,
  value: string,
  name: string,
};

export type EnrolledBillerApiObject = {
  alias: string,
  billerId: string,
  billerName: string,
  contactId: string,
  currencyCode: string,
  enabled: boolean,
  feilds: Array<Field>,
};

export type MboaEnrolledBilleApiObject = {
  billerAlias: string,
  billerId: string,
  createdDate: string,
  currencyCode: string,
  fields: Array<Object>,
  id: string,
  lastTransactiondate?: string,
  referenceNo: string,
  reminder?: string,
  subscriberName: string,
  updateDate?: string,
  billerIcon?: string,
};

export type BillerCategoryObject = BillerCategory & {
  billers: Array<any>,
};
export type BillerCategoryResponse = Array<BillerCategoryObject>;
export type AllBillerResponse = Array<AllBillerApiObject>;
export type EnrolledBillerResponse = Array<EnrolledBillerApiObject>;
export type MboaEnrolledBillerResponse = Array<MboaEnrolledBilleApiObject>;

export type BillerAction =
  | {
      type: 'GET_BILLER_REQUEST',
    }
  | {
      type: 'GET_BILLER_SUCCESS',
      payload: {
        billers: Billers,
        categories: Array<BillerCategory>,
        allBillersByCategory: BillerByCategory,
        payBillsAllBillers: AllBillers,
        payBillsEnrolledBiller: Array<Biller>,
      },
    }
  | {
      type: 'GET_BILLER_FAILED',
    }
  | {
      type: 'SELECT_PBC_BILLER',
      payload: {id: string, billerType: string},
    }
  | {
      type: 'COMPLETE_PBC_SELECT',
      payload: Biller,
    }
  | {
      type: 'SELECT_PBC_CATEGORY',
      payload: string,
    }
  | {
      type: 'RESET_BILLERS',
    };

export type ActionMapperType = {
  state: BillerState,
  payload: Object,
};
