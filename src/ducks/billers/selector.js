/**
 * @flow
 */
import type {State} from '@ducks/types';
import type {Billers, BillerCategory, Biller} from './types';

import {createSelector} from 'reselect';

export const billersSelector = (state: State): Billers => state.billers.billers;

export const billerCategorySelector = (state: State): Array<BillerCategory> =>
  state.billers.categories;

export const selectedBillerSelector = (state: State): ?Biller =>
  state.billers.payBillsSelectedBiller;

export const selectedBillerIdSelector = (state: State): string =>
  state.billers.selectedBillerId;

export const selectedBillerTypeSelector = (state: State): string =>
  state.billers.selectedBillerType;

export const selectedCategorySelector = (state: State): string =>
  state.billers.payBillsSelectedCategory;

export const selectedCategorNameySelector: ?string = createSelector(
  [selectedCategorySelector, billerCategorySelector],
  (categoryId, categories) =>
    categories.find((row) => row.id === categoryId)?.name,
);

export const enrolledBillerSelector = (state: State): Array<Biller> =>
  state.billers.billers.enrolledBillers;
