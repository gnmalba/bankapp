/**
 * @flow
 */
import type {
  ManageAccountSettingsState,
  ManageAccountSettingsAction,
} from './types';

const INITIAL_STATE: ManageAccountSettingsState = {
  isLoading: false,
  error: null,
  selectedAccount: {
    alias: '',
    cardType: '',
    // for CASA
    accountId: '',
    // for CPC
    cardSerNo: '',
    isAccountPrimary: false,
  },
  snackbar: {
    state: false,
    message: '',
  },
  action: '',
};

const CHANGE_ALIAS_SUCCESS_MSG =
  'You have successfully changed your account alias';
const DELETE_ACCOUNT_SUCCESS_MSG = 'You have successfully deleted ';
const DUPLICATE_ALIAS_MSG =
  'This alias is already in use. Please create another.';

const manageAccountSettings = (
  state: ManageAccountSettingsState = INITIAL_STATE,
  action: ManageAccountSettingsAction,
): ManageAccountSettingsState => {
  switch (action.type) {
    case 'UPDATE_SELECTED_ACCOUNT':
      return {
        ...state,
        selectedAccount: {...state.selectedAccount, ...action.payload},
      };

    case 'EDIT_ALIAS_CPC_REQUESTED':
    case 'EDIT_ALIAS_CASA_REQUESTED':
      return {
        ...state,
        isLoading: true,
        action: 'EDIT',
      };

    case 'DELETE_ACCOUNT_CPC_REQUESTED':
    case 'DELETE_ACCOUNT_CASA_REQUESTED':
      return {
        ...state,
        isLoading: true,
        action: 'DELETE',
      };
    case 'EDIT_ALIAS_CPC_SUCCESS':
    case 'EDIT_ALIAS_CASA_SUCCESS':
      return {
        ...state,
        isLoading: false,
        selectedAccount: {
          ...state.selectedAccount,
          alias: action.payload,
        },
        snackbar: {
          state: true,
          message: CHANGE_ALIAS_SUCCESS_MSG,
        },
        // action: '',
      };

    case 'DELETE_ACCOUNT_CPC_SUCCESS':
    case 'DELETE_ACCOUNT_CASA_SUCCESS':
      return {
        ...state,
        isLoading: false,
        snackbar: {
          state: true,
          message: DELETE_ACCOUNT_SUCCESS_MSG + action.payload,
        },
        // action: '',
      };

    case 'EDIT_ALIAS_CPC_FAILED':
    case 'DELETE_ACCOUNT_CPC_FAILED':
    case 'EDIT_ALIAS_CASA_FAILED':
    case 'DELETE_ACCOUNT_CASA_FAILED':
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };

    case 'TOGGLE_SNACKBAR':
      return {
        ...state,
        snackbar: action.payload,
      };

    case 'HAS_DUPLICATE_ALIAS':
      return {
        ...state,
        error: DUPLICATE_ALIAS_MSG,
      };

    case 'CLEAR_ERROR':
      return {
        ...state,
        error: null,
      };

    default:
      return state;
  }
};

export default manageAccountSettings;
