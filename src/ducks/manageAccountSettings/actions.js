/**
 * @flow
 */

import type {
  ManageAccountSettingsAction,
  EditAliasParams,
  DeleteAccountParams,
  AccountDetails,
  SnackbarState,
} from './types';

/* START - CPC */
export const changeAliasRequestedCPC = (
  params: EditAliasParams,
): ManageAccountSettingsAction => ({
  type: 'EDIT_ALIAS_CPC_REQUESTED',
  payload: params,
});

export const changeAliasSuccessCPC = (
  alias: string,
): ManageAccountSettingsAction => ({
  type: 'EDIT_ALIAS_CPC_SUCCESS',
  payload: alias,
});

export const changeAliasFailedCPC = (
  error: string,
): ManageAccountSettingsAction => ({
  type: 'EDIT_ALIAS_CPC_FAILED',
  payload: error,
});

export const deleteAccountRequestedCPC = (
  params: DeleteAccountParams,
): ManageAccountSettingsAction => ({
  type: 'DELETE_ACCOUNT_CPC_REQUESTED',
  payload: params,
});

export const deleteAccountSuccessCPC = (
  alias: string,
): ManageAccountSettingsAction => ({
  type: 'DELETE_ACCOUNT_CPC_SUCCESS',
  payload: alias,
});

export const deleteAccountFailedCPC = (
  error: string,
): ManageAccountSettingsAction => ({
  type: 'DELETE_ACCOUNT_CPC_FAILED',
  payload: error,
});
/* END - CPC */

/* START - CASA */
export const changeAliasRequestedCASA = (
  params: EditAliasParams,
): ManageAccountSettingsAction => ({
  type: 'EDIT_ALIAS_CASA_REQUESTED',
  payload: params,
});

export const changeAliasSuccessCASA = (
  alias: string,
): ManageAccountSettingsAction => ({
  type: 'EDIT_ALIAS_CASA_SUCCESS',
  payload: alias,
});

export const changeAliasFailedCASA = (
  error: string,
): ManageAccountSettingsAction => ({
  type: 'EDIT_ALIAS_CASA_FAILED',
  payload: error,
});

export const deleteAccountRequestedCASA = (
  params: DeleteAccountParams,
): ManageAccountSettingsAction => ({
  type: 'DELETE_ACCOUNT_CASA_REQUESTED',
  payload: params,
});

export const deleteAccountSuccessCASA = (
  alias: string,
): ManageAccountSettingsAction => ({
  type: 'DELETE_ACCOUNT_CASA_SUCCESS',
  payload: alias,
});

export const deleteAccountFailedCASA = (
  error: string,
): ManageAccountSettingsAction => ({
  type: 'DELETE_ACCOUNT_CASA_FAILED',
  payload: error,
});
/* END - CASA */

// common
export const toggleSnackbar = (
  params: SnackbarState,
): ManageAccountSettingsAction => ({
  type: 'TOGGLE_SNACKBAR',
  payload: params,
});

export const updateSelectedAccount = (
  accountType: AccountDetails,
): ManageAccountSettingsAction => ({
  type: 'UPDATE_SELECTED_ACCOUNT',
  payload: accountType,
});

export const clearError = (): ManageAccountSettingsAction => ({
  type: 'CLEAR_ERROR',
});
