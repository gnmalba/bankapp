/**
 * @flow
 */
// API
import api from '@api/services/manageAccountService';

// Third party libraries
import _ from 'lodash';
import {takeLatest, call, put, select, delay} from 'redux-saga/effects';

// Selectors
import {
  manageAccountSettingsSelector,
  selectedAccountSelector,
} from './selectors';
import {
  getCustomerID,
  getEncryptionPublicKey,
  getUsername,
} from '@ducks/commonSelectors';
import {
  depositsSelector,
  getPrepaidFromDeposit,
} from '@ducks/deposit/selectors';
import {yazzSelector} from '@ducks/yazz/selectors';

// Actions
import {
  //CASA
  changeAliasSuccessCASA,
  changeAliasFailedCASA,
  deleteAccountSuccessCASA,
  deleteAccountFailedCASA,
  // CPC
  changeAliasSuccessCPC,
  changeAliasFailedCPC,
  deleteAccountSuccessCPC,
  deleteAccountFailedCPC,
  // common
  updateSelectedAccount,
} from './actions';
import {
  updateSavingsAndDeposits,
  updateTimeDeposits,
  updatePrepaids,
} from '@ducks/deposit/actions';
import {changeAliasDepositPrepaid} from '@ducks/depositprepaidoverview/actions';
import {updateCreditCardList} from '@ducks/credit/actions';
import {updateYazzList, changeAliasYazz} from '@ducks/yazz/actions';
import MangageAccountConfig from '@api/services/manageAccountService';

// Utils
import {checkDuplicateAlias} from '@ducks/commonGenerators';
import encrypt from '@utils/encryptString';
import {
  getTotalLength,
  mutateAliasForCasaAccts,
  mutateAliasForCPCAccts,
} from './utils';

// Types
import type {
  EditAliasParams,
  DeleteAccountParams,
  AccountDetails,
} from './types';

const GENERAL_ERROR = // TODO: constants
  'Something went wrong on your request. Please try again later.';
const REQUEST_TIMEOUT = 'Request timeout';
// CASA
function* editAccountAliasCASA({
  payload,
}: {
  payload: EditAliasParams,
}): Generator<any, any, any> {
  const {alias} = payload;
  try {
    const {selectedAccount} = yield select(manageAccountSettingsSelector);

    const response = yield call(MangageAccountConfig.editAliasCasaAccount, {
      accountId: selectedAccount.accountId,
      alias,
    });

    if (response.status === 200) {
      yield changeAliasManager(alias);
      yield put(changeAliasSuccessCASA(alias));
    } else {
      yield put(changeAliasFailedCASA(GENERAL_ERROR));
    }
  } catch (apiErrorResponse) {
    if (apiErrorResponse.response === undefined) {
      yield put(changeAliasFailedCASA(REQUEST_TIMEOUT));
    } else {
      yield put(changeAliasFailedCASA(GENERAL_ERROR));
    }
    console.log('editAccountAliasCASA error: ', apiErrorResponse);
  }
}

function* deleteAccountCASA({payload}): Generator<any, any, any> {
  const {mpin} = payload;
  try {
    const {selectedAccount} = yield select(manageAccountSettingsSelector);

    yield delay(300); // NOTE: Need this for iOS modal issue
    yield checkIfPrimary(selectedAccount.isAccountPrimary);
    yield checkIfLastAccount();

    const encryptionPublicKey = yield select(getEncryptionPublicKey);
    const customerId = yield select(getCustomerID);

    const response = yield call(api.deleteCasaAccount, {
      accountId: selectedAccount.accountId,
      authValue: encrypt(encryptionPublicKey).encrypt(mpin),
      customerId,
    });

    if (response.status === 200) {
      yield deleteAccountManager();
      yield put(deleteAccountSuccessCASA(selectedAccount.alias));
    } else {
      yield put(deleteAccountFailedCASA(GENERAL_ERROR));
    }
  } catch (error) {
    yield deleteAccountErrorHandler(error.response, deleteAccountFailedCASA);
    console.log('deleteAccountCASA error: ', error);
  }
}

// CPC
function* editAccountAliasCPC({
  payload,
}: {
  payload: EditAliasParams,
}): Generator<any, any, any> {
  const {alias} = payload;
  try {
    const {selectedAccount} = yield select(manageAccountSettingsSelector);

    const customerId = yield select(getCustomerID);
    const response = yield call(MangageAccountConfig.editAliasCPCAccount, {
      cardAlias: alias,
      cardSerialNo: selectedAccount.cardSerNo,
      customerId,
    });

    if (response.status === 200) {
      yield changeAliasManager(alias);
      yield put(changeAliasSuccessCPC(alias));
    } else {
      yield put(changeAliasFailedCPC(GENERAL_ERROR));
    }
  } catch (apiErrorResponse) {
    if (apiErrorResponse.response === undefined) {
      yield put(changeAliasFailedCPC(REQUEST_TIMEOUT));
    } else if (apiErrorResponse.response === 400) {
      const {message} = apiErrorResponse.response?.data?.errors[0];
      yield put(changeAliasFailedCPC(message));
    } else {
      yield put(changeAliasFailedCPC(GENERAL_ERROR));
    }
    console.log('editAccountAliasCPC error: ', apiErrorResponse);
  }
}

function* deleteAccountCPC({
  payload,
}: {
  payload: DeleteAccountParams,
}): Generator<any, any, any> {
  const {mpin} = payload;
  try {
    const {selectedAccount} = yield select(manageAccountSettingsSelector);

    yield delay(300); // NOTE: Need this for iOS modal issue
    yield checkIfLastAccount();

    // As per BA requirement is deferred, but commenting out for now
    // if (yield checkIfPrimary(selectedAccount.isAccountPrimary, 'CPC')) return;

    const encryptionPublicKey = yield select(getEncryptionPublicKey);
    const customerId = yield select(getCustomerID);
    const username = yield select(getUsername);
    const response = yield call(MangageAccountConfig.deleteCPCAccount, {
      customerId,
      cardSerialNo: selectedAccount.cardSerNo,
      username,
      mpin: encrypt(encryptionPublicKey).encrypt(mpin),
    });

    if (response.status === 200) {
      yield deleteAccountManager();
      yield put(deleteAccountSuccessCPC(selectedAccount.alias));
    } else {
      yield put(deleteAccountFailedCPC(GENERAL_ERROR));
    }
  } catch (error) {
    yield deleteAccountErrorHandler(error.response, deleteAccountFailedCPC);
    console.log('deleteAccountCPC error: ', error);
  }
}

// common
function* changeAliasManager(alias: string): Generator<any, any, any> {
  const {cardType, cardSerNo, accountId} = yield select(
    selectedAccountSelector,
  );

  switch (cardType) {
    case 'DEPOSIT': {
      const {savingsAndDeposits} = yield select(depositsSelector);
      const {data} = yield select(
        (state) => state.depositprepaidoverview.depositPrepaidDetails,
      );

      yield put(
        updateSavingsAndDeposits(
          mutateAliasForCasaAccts(savingsAndDeposits, accountId, alias),
        ),
      );
      yield put(
        changeAliasDepositPrepaid({
          ...data,
          depositAccount: {
            ...data.depositAccount,
            alias,
          },
        }),
      );
      return;
    }
    case 'PREPAID': {
      const prepaids = yield select(getPrepaidFromDeposit);
      const {data} = yield select(
        (state) => state.depositprepaidoverview.depositPrepaidDetails,
      );

      yield put(
        updatePrepaids(mutateAliasForCasaAccts(prepaids, accountId, alias)),
      );
      yield put(
        changeAliasDepositPrepaid({
          ...data,
          depositAccount: {
            ...data.depositAccount,
            alias,
          },
        }),
      );
      return;
    }

    case 'TIME_DEPOSIT': {
      const {timeDeposits} = yield select(depositsSelector);
      const {data} = yield select(
        (state) => state.depositprepaidoverview.depositPrepaidDetails,
      );

      yield put(
        updateTimeDeposits(
          mutateAliasForCasaAccts(timeDeposits, accountId, alias),
        ),
      );
      yield put(
        changeAliasDepositPrepaid({
          ...data,
          depositAccount: {
            ...data.depositAccount,
            alias,
          },
        }),
      );
      return;
    }

    case 'CREDIT': {
      const creditAccounts = yield select(
        (state) => state.credit.creditList.data,
      );

      yield put(
        updateCreditCardList(
          mutateAliasForCPCAccts(creditAccounts, cardSerNo, alias),
        ),
      );

      return;
    }

    case 'YAZZ': {
      const yazzAccounts = yield select(yazzSelector);
      const yazzCardDetail = yield select((state) => state.yazz.cardDetails);
      yazzCardDetail[0].alias = alias;

      yield put(
        updateYazzList(mutateAliasForCPCAccts(yazzAccounts, cardSerNo, alias)),
      );
      yield changeAliasYazz(yazzCardDetail[0]);
      return;
    }

    default:
      return;
  }
}

function* deleteAccountManager(): Generator<any, any, any> {
  const {cardType, cardSerNo, accountId}: AccountDetails = yield select(
    selectedAccountSelector,
  );

  switch (cardType) {
    case 'DEPOSIT': {
      const {savingsAndDeposits} = yield select(depositsSelector);
      const newSavingsAndDeposits = _.remove(
        savingsAndDeposits,
        (item) => item.id !== accountId,
      );

      yield put(updateSavingsAndDeposits(newSavingsAndDeposits));
      return;
    }

    case 'TIME_DEPOSIT': {
      const {timeDeposits} = yield select(depositsSelector);
      const newTimeDeposits = _.remove(
        timeDeposits,
        (item) => item.id !== accountId,
      );

      yield put(updateTimeDeposits(newTimeDeposits));
      return;
    }

    case 'PREPAID': {
      const prepaids = yield select(getPrepaidFromDeposit);
      const newPrepaids = _.remove(prepaids, (item) => item.id !== accountId);

      yield put(updatePrepaids(newPrepaids));
      return;
    }

    case 'CREDIT': {
      const creditAccounts = yield select(
        (state) => state.credit.creditList.data,
      );
      const newCreditAccounts = _.remove(
        creditAccounts,
        (item) => item.cardSerNo !== cardSerNo,
      );

      yield put(updateCreditCardList(newCreditAccounts));

      return;
    }

    case 'YAZZ': {
      const yazzAccounts = yield select(yazzSelector);
      const newYazzAccounts = _.remove(
        yazzAccounts,
        (item) => item.cardSerNo !== cardSerNo,
      );

      yield put(updateYazzList(newYazzAccounts));
      return;
    }
    default:
      return;
  }
}
function* checkDuplicateAliasRequested({
  payload,
}: {
  payload: string,
}): Generator<any, any, any> {
  const hasDuplicate = yield call(checkDuplicateAlias, {payload});
  if (hasDuplicate) {
    yield put({type: 'HAS_DUPLICATE_ALIAS'});
  } else {
    yield put({type: 'CLEAR_ERROR'});
  }
}

function* checkIfPrimary(status: boolean): Generator<any, any, any> {
  const error =
    'You cannot remove this account because this is your primary account';
  if (status) {
    throw Object({response: {error}});
  }
}

function* deleteAccountErrorHandler(
  response: Object,
  action: (errorText: string) => {},
): Generator<any, any, any> {
  if (response === undefined) {
    yield put(action(REQUEST_TIMEOUT));
  } else {
    const {data, status, error} = response;

    if (status === 400) {
      const {code, message} = data.data.errors[0];
      if (code === 'invalid_mpin_exceeded') {
        const INVALID_MPIN_MAX =
          'You have reached the maximum number of attempts. Please try again after (x) minutes.';
        yield put(action(INVALID_MPIN_MAX));
      } else if (code === 'mpin') {
        yield put(action('Passcode is invalid.'));
      } else {
        yield put(action(message));
      }
    } else {
      // Error other than status code of 400
      yield put(action(error ? error : GENERAL_ERROR));
    }
  }
}

function* navigationHandler({payload}): Generator<any, any, any> {
  const {isLoading, error, action} = yield select(
    manageAccountSettingsSelector,
  );

  if (!isLoading) {
    if (error) {
      payload.navigate('ManageAccountSettings');
    } else {
      if (action === 'DELETE') {
        payload.navigate('Accounts');
      } else if (action === 'EDIT') {
        payload.navigate('ManageAccountSettings');
      }
    }
  } else {
    yield delay(150); // NOTE: Need this for iOS modal issue
    payload.navigate('Processing Screen', {
      label: action === 'EDIT' ? 'Processing Request' : 'Deleting Account',
    });
  }
}

function* getAccountDetailsCASA({payload}): Generator<any, any, any> {
  const {depositAccount} = payload;
  const {productType, alias, id, initialAccount, accountType} = depositAccount;

  let cardType = productType === '769' ? 'PREPAID' : 'DEPOSIT';
  if (accountType === 'Time Deposit') {
    cardType = 'TIME_DEPOSIT';
  }

  yield put(
    updateSelectedAccount({
      alias,
      accountId: id,
      cardType,
      isAccountPrimary: initialAccount,
    }),
  );
}

function* getAccountDetailsCPC({payload}): Generator<any, any, any> {
  const {cardSerNo} = payload;

  const creditList = yield select((state) => state.credit.creditList.data);
  const index = creditList.findIndex((item) => cardSerNo === item.cardSerNo);

  yield put(
    updateSelectedAccount({
      alias: creditList[index].alias,
      cardSerNo,
      cardType: 'CREDIT',
    }),
  );
}

function* getAccountDetailsYAZZ({payload}): Generator<any, any, any> {
  const {cardSerNo} = payload.data[0];

  const yazzAccounts = yield select(yazzSelector);
  const index = yazzAccounts.findIndex((item) => item.cardSerNo === cardSerNo);

  yield put(
    updateSelectedAccount({
      alias: yazzAccounts[index].alias,
      cardSerNo,
      cardType: 'YAZZ',
    }),
  );
}

function* checkIfLastAccount(): Generator<any, any, any> {
  const error =
    'You cannot remove this account. At least one active account is needed to continue using Metrobank Online.';

  const {savingsAndDeposits, timeDeposits} = yield select(depositsSelector);
  const prepaids = yield select(getPrepaidFromDeposit);
  const creditAccounts = yield select((state) => state.credit.creditList.data);
  const yazzAccounts = yield select((state) => state.yazz.data);

  const totalNumberOfAccounts = getTotalLength([
    savingsAndDeposits,
    timeDeposits,
    prepaids,
    creditAccounts,
    yazzAccounts,
  ]);

  if (totalNumberOfAccounts === 1) {
    throw Object({response: {error}});
  }
}

function* manageAcctSettingsWatcher(): Generator<any, any, any> {
  yield takeLatest('EDIT_ALIAS_CASA_REQUESTED', editAccountAliasCASA);
  yield takeLatest('DELETE_ACCOUNT_CASA_REQUESTED', deleteAccountCASA);
  yield takeLatest('EDIT_ALIAS_CPC_REQUESTED', editAccountAliasCPC);
  yield takeLatest('DELETE_ACCOUNT_CPC_REQUESTED', deleteAccountCPC);
  yield takeLatest('CHECK_DUPLICATE_ALIAS', checkDuplicateAliasRequested);
  yield takeLatest('PASS_NAVIGATION', navigationHandler);
  // Listeners used to update the selected account
  yield takeLatest('GET_DEPOSIT_PREPAID_API_SUCCESS', getAccountDetailsCASA);
  yield takeLatest('GET_CREDIT_CARDS_DETAILS_SUCCESS', getAccountDetailsCPC);
  yield takeLatest('GET_YAZZ_CARDS_DETAILS_SUCCESS', getAccountDetailsYAZZ);
}

export default manageAcctSettingsWatcher;
