/**
 * @flow
 */

export const isAccountCPC = (acctNo: string): boolean => acctNo.includes('*');

export const getTotalLength = (arr: Array<Object>): number => {
  let count = 0;
  arr.map((item) => {
    count += item.length;
  });

  return count;
};

export const mutateAliasForCasaAccts = (
  data: Array<Object>,
  uid: string,
  alias: string,
): Array<Object> => {
  const copyOfData = data;

  const index = copyOfData.findIndex((item) => item.id === uid);

  copyOfData[index].alias = alias;

  return copyOfData;
};

export const mutateAliasForCPCAccts = (
  data: Array<Object>,
  uid: string,
  alias: string,
): Array<Object> => {
  const dataToBeMutated = data;

  const i = dataToBeMutated.findIndex((item) => item.cardSerNo === uid);

  dataToBeMutated[i].alias = alias;

  return dataToBeMutated;
};
