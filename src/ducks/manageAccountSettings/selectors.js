/**
 * @flow
 */

import type {State} from '@ducks/types';
import type {
  ManageAccountSettingsState,
  AccountDetails,
  SnackbarState,
} from './types';

export const manageAccountSettingsSelector = (
  state: State,
): ManageAccountSettingsState => state.manageAccountSettings;

export const selectedAccountSelector = (state: State): AccountDetails =>
  state.manageAccountSettings.selectedAccount;

export const aliasSelector = (state: State): string =>
  state.manageAccountSettings.selectedAccount.alias;

export const snackbarSelector = (state: State): SnackbarState =>
  state.manageAccountSettings.snackbar;

export const errorSelector = (state: State): string | null =>
  state.manageAccountSettings.error;
