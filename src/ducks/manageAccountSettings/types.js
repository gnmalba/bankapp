/**
 * @flow
 */

export type EditAliasParams = {
  alias: string,
};

export type DeleteAccountParams = {
  acctNo?: string,
  mpin: string,
};

export type AccountDetails = {
  alias: string,
  accountId?: string,
  cardSerNo?: string,
  cardType: Card,
  isAccountPrimary?: boolean,
};

export type Card =
  | 'DEPOSIT'
  | 'TIME_DEPOSIT'
  | 'CREDIT'
  | 'PREPAID'
  | 'YAZZ'
  | '';
export type SnackbarState = {
  state: boolean,
  message: string,
};

export type ManageAccountSettingsAction =
  | {
      type: 'TOGGLE_SNACKBAR',
      payload: SnackbarState,
    }
  | {
      type: 'UPDATE_SELECTED_ACCOUNT',
      payload: AccountDetails,
    }
  | {
      type: 'EDIT_ALIAS_CPC_REQUESTED',
      payload: EditAliasParams,
    }
  | {
      type: 'EDIT_ALIAS_CPC_SUCCESS',
      payload: string,
    }
  | {
      type: 'EDIT_ALIAS_CPC_FAILED',
      payload: string,
    }
  | {
      type: 'EDIT_ALIAS_CASA_REQUESTED',
      payload: EditAliasParams,
    }
  | {
      type: 'EDIT_ALIAS_CASA_SUCCESS',
      payload: string,
    }
  | {
      type: 'EDIT_ALIAS_CASA_FAILED',
      payload: string,
    }
  | {
      type: 'DELETE_ACCOUNT_CPC_REQUESTED',
    }
  | {
      type: 'DELETE_ACCOUNT_CPC_SUCCESS',
      payload: string,
    }
  | {
      type: 'DELETE_ACCOUNT_CPC_FAILED',
      payload: string,
    }
  | {
      type: 'DELETE_ACCOUNT_CASA_REQUESTED',
    }
  | {
      type: 'DELETE_ACCOUNT_CASA_SUCCESS',
      payload: string,
    }
  | {
      type: 'DELETE_ACCOUNT_CASA_FAILED',
      payload: string,
    }
  | {
      type: 'HAS_DUPLICATE_ALIAS',
      payload: string,
    }
  | {
      type: 'CLEAR_ERROR',
      payload: string,
    }
  | {
      type: 'CLEAR_ERROR',
    };

export type ManageAccountSettingsState = {
  isLoading: boolean,
  error: string | null,
  selectedAccount: AccountDetails,
  snackbar: SnackbarState,
};
