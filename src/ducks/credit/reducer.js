/**
 * @flow
 */

import type {CreditState} from './types';

export const INITIAL_STATE: CreditState = {
  creditList: {
    loading: false,
    data: [],
    error: false,
    errors: [],
  },
  selectedCredit: null,
  creditDetails: {
    loading: false,
    data: {},
    error: false,
    loader: [],
    carouselCards: [],
  },
  crediTransactionCurrent: {
    loading: false,
    data: [],
    error: false,
    errors: [],
  },
  supplementaryDetails: {
    loading: false,
    data: {},
    loader: [],
  },
  supplementaryTransactionCurrent: {
    loading: false,
    data: [],
    error: false,
    errors: [],
  },
};

const accounts = (
  state: any = INITIAL_STATE,
  action: Object = {},
): CreditState => {
  switch (action.type) {
    case 'REQUEST_CREDIT_CARD_LIST':
      return {
        ...state,
        creditList: {
          loading: true,
          data: [],
          error: false,
          errors: [],
        },
      };

    case 'GET_CREDIT_CARDS_SUCCESS':
      return {
        ...state,
        creditList: {
          loading: false,
          data: action.payload,
          error: false,
          errors: [],
        },
      };

    case 'GET_CREDIT_CARDS_FAILED':
      return {
        ...state,
        creditList: {
          loading: false,
          data: [],
          error: true,
          errors: action.payload,
        },
      };

    case 'REQUEST_CREDIT_CARD_DETAILS':
      return {
        ...state,
        creditDetails: {
          loading: true,
          data: [],
          error: false,
          loader: [],
          carouselCards: [],
        },
      };

    case 'GET_CREDIT_CARDS_DETAILS_SUCCESS':
      return {
        ...state,
        creditDetails: {
          loading: false,
          data: action.payload,
          error: false,
          loader: ['credit'],
          carouselCards: [action.payload, ...action.payload.supplementaryCards],
        },
      };

    case 'GET_CREDIT_CARDS_DETAILS_FAILED':
      return {
        ...state,
        creditDetails: {
          loading: false,
          data: action.payload,
          error: true,
          loader: [],
          carouselCards: [],
        },
      };

    case 'REQUEST_CREDIT_TRANSACTION_CURRENT_LIST':
      return {
        ...state,
        crediTransactionCurrent: {
          loading: true,
          data: [],
          error: false,
          errors: [],
        },
      };

    case 'GET_CREDIT_TRANSACTION_CURRENT_SUCCESS':
      return {
        ...state,
        crediTransactionCurrent: {
          loading: false,
          data: action.payload,
          error: false,
          errors: [],
        },
      };

    case 'GET_CREDIT_TRANSACTION_CURRENT_FAILED':
      return {
        ...state,
        crediTransactionCurrent: {
          loading: false,
          data: [],
          error: true,
          errors: action.payload,
        },
      };

    case 'REQUEST_SUPPLEMENTARY_DETAILS':
      return {
        ...state,
        supplementaryDetails: {
          loading: true,
          data: {},
          loader: [],
        },
      };

    case 'GET_SUPPLEMENTARY_CARDS_DETAILS_SUCCESS':
      return {
        ...state,
        supplementaryDetails: {
          loading: false,
          data: action.payload,
          loader: ['supplementary'],
        },
      };

    case 'REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST':
      return {
        ...state,
        supplementaryTransactionCurrent: {
          loading: true,
          data: [],
          error: false,
          errors: [],
        },
      };

    case 'GET_SUPPLEMENTARY_TRANSACTION_CURRENT_SUCCESS':
      return {
        ...state,
        supplementaryTransactionCurrent: {
          loading: false,
          data: action.payload,
          error: false,
          errors: [],
        },
      };

    case 'GET_SUPPLEMENTARY_TRANSACTION_CURRENT_FAILED':
      return {
        ...state,
        supplementaryTransactionCurrent: {
          loading: false,
          data: [],
          error: true,
          errors: action.payload,
        },
      };

    case '@credit/UPDATE_CREDIT_CARD_LIST':
      return {
        ...state,
        creditList: {
          ...state.creditList,
          data: action.payload,
        },
      };

    case '@credit/SET_SELECTED_CREDIT':
      return {
        ...state,
        selectedCredit: action.payload,
      };

    case 'CREDIT_LOGOUT_RESET_STATE':
      return INITIAL_STATE;

    default:
      return state;
  }
};

export default accounts;
