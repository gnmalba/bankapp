/**
 * @flow
 */

import {takeLatest, put, call, select, spawn, cancel} from 'redux-saga/effects';
import type {T} from './types';
import CreditConfig from '@api/services/creditService';

import {getCustomerID} from '@ducks/commonSelectors';
import {reconstructTransactionData} from './utils';
import {updateWidgetLoadingState} from '@ducks/config/actions';

import {
  addPesoAccountByType,
  addForeignAccountByType,
  validateSelectedCurrencyByAccountType,
} from '@ducks/accounts-displayoptions/actions';

import {setSelectedCredit} from '@ducks/credit/actions';
import {initializeShortcutsForInstallmentsWidget} from '@ducks/shortcuts/actions';

export function* addPesoAndForeignAccount(credit: any): any {
  if (credit.length === 0) {
    // if we don't have credit accounts, terminate this generator
    yield cancel();
  }

  const pesoAccount: string = 'PHP';

  for (let i = 0; i < credit.length; i++) {
    let account = credit[i];

    if (account.flag === null || account.flag === 'NOT_ALLOWED') {
      // we don't need to add invalid accounts
      continue;
    }

    if (account.currency === pesoAccount) {
      yield put(addPesoAccountByType(account.cardNo, 'credit'));
    } else {
      yield put(addForeignAccountByType(account.cardNo, 'credit'));
    }
  }

  yield put(validateSelectedCurrencyByAccountType('credit'));
}

export function* getCreditCards(): T {
  const customerId = yield select(getCustomerID);
  const cardTypes = 'creditcard';

  yield put(updateWidgetLoadingState(2, true));
  yield put(updateWidgetLoadingState(4, true));
  yield put(updateWidgetLoadingState(5, true));

  yield put({type: 'REQUEST_CREDIT_CARD_LIST'});
  try {
    const response = yield call(
      CreditConfig.getCreditCards,
      customerId,
      cardTypes,
    );
    const sortItem =
      response &&
      response.data.data.sort((a, b) =>
        a.alias.localeCompare(b.alias, undefined, {sensitivity: 'base'}),
      );

    yield spawn(addPesoAndForeignAccount, sortItem);

    yield put({
      type: 'GET_CREDIT_CARDS_SUCCESS',
      payload: sortItem,
    });
  } catch (error) {
    yield put({type: 'GET_CREDIT_CARDS_FAILED', payload: error});
  } finally {
    yield put({type: 'GET_CREDIT_CARDS_RESOLVED'});
    yield put(updateWidgetLoadingState(2, false));
    yield put(updateWidgetLoadingState(4, false));
    yield put(updateWidgetLoadingState(5, false));
    yield put(initializeShortcutsForInstallmentsWidget());
  }
}

export function* getCreditCardDetails({payload}: any): T {
  const customerId = yield select(getCustomerID);
  try {
    const response = yield call(
      CreditConfig.getCreditCardDetails,
      customerId,
      payload,
    );
    const creditDetails = response.data.data;
    yield put({
      type: 'GET_CREDIT_CARDS_DETAILS_SUCCESS',
      payload: creditDetails,
    });

    yield put(setSelectedCredit(creditDetails));
  } catch (error) {
    yield put({
      type: 'GET_CREDIT_CARDS_DETAILS_FAILED',
      payload: error.response,
    });
  }
}

export function* getCreditTransactionCurrent({payload}: any): T {
  const customerId = yield select(getCustomerID);
  const params = {
    ...payload,
    customerId,
  };
  const url = new URLSearchParams();

  /*  description = string
   *  cardOwners = ["2323232"]
   *  trxnTypes = CR/DB
   *  startDate =  0000-00-00
   *  endDate = 0000-00-00
   */

  for (const key in params) {
    if (key === 'cardOwners' || key === 'trxnTypes') {
      Array.isArray(params[key]) &&
        params[key].forEach((item) => {
          url.append(key, item);
        });
    } else {
      url.append(key, params[key]);
    }
  }

  try {
    const response = yield call(CreditConfig.getCreditTransactionCurrent, url);
    const reconstructItem = reconstructTransactionData(response, true);

    yield put({
      type: 'GET_CREDIT_TRANSACTION_CURRENT_SUCCESS',
      payload: {transactionHistory: reconstructItem},
    });
  } catch (error) {
    yield put({
      type: 'GET_CREDIT_TRANSACTION_CURRENT_FAILED',
      payload: error.response,
    });
  }
}

export function* getSupplementaryTransactionCurrent({payload}: any): T {
  const customerId = yield select(getCustomerID);
  const params = {
    ...payload,
    customerId,
  };
  const url = new URLSearchParams();

  /*  description = string
   *  cardOwners = "2323232"
   *  trxnTypes = CR/DB
   *  startDate =  0000-00-00
   *  endDate = 0000-00-00
   */

  for (const key in params) {
    if (key === 'trxnTypes') {
      Array.isArray(params[key]) &&
        params[key].forEach((item) => {
          url.append(key, item);
        });
    } else {
      url.append(key, params[key]);
    }
  }

  try {
    const response = yield call(CreditConfig.getCreditTransactionCurrent, url);
    const reconstructItem = reconstructTransactionData(response, true);

    yield put({
      type: 'GET_SUPPLEMENTARY_TRANSACTION_CURRENT_SUCCESS',
      payload: {
        transactionHistory: reconstructItem,
      },
    });
  } catch (error) {
    yield put({
      type: 'GET_SUPPLEMENTARY_TRANSACTION_CURRENT_FAILED',
      payload: error.response,
    });
  }
}

export function* getSupplmentaryDetails(action: any): T {
  const data = yield select((state) => state.credit.creditDetails.data);

  const findItem = data.supplementaryCards.filter(
    (x) => x.cardSerNo === action.payload,
  );

  const item = {
    data: {...findItem[0]},
    item: {
      ...findItem[0],
    },
  };

  yield put({
    type: 'GET_SUPPLEMENTARY_CARDS_DETAILS_SUCCESS',
    payload: item,
  });
}

export default function* watcher(): any {
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', getCreditCards);
  yield takeLatest('ENROLLMENT_CREDIT_SUCCESS', getCreditCards);
  yield takeLatest('REQUEST_CREDIT_CARD_DETAILS', getCreditCardDetails);
  yield takeLatest('REQUEST_SUPPLEMENTARY_DETAILS', getSupplmentaryDetails);
  yield takeLatest(
    'REQUEST_CREDIT_TRANSACTION_CURRENT_LIST',
    getCreditTransactionCurrent,
  );
  yield takeLatest(
    'REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST',
    getSupplementaryTransactionCurrent,
  );
}
