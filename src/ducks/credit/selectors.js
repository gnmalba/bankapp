/**
 * @flow
 */

import type {State} from '@ducks/types';
import type {CreditList} from './types';

export const creditSelector = (state: State): CreditList =>
  state.credit.creditList;

export const creditListLoadingSelector = (state: State): CreditList =>
  creditSelector.loading;

export const creditStateSelector = (state: State): any => state.credit;

export const selectedCreditSelector = (state: State): Object =>
  state.credit.selectedCredit;
