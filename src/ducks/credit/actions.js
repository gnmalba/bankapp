/**
 * @flow
 */

import type {CreditAction} from './types';
import * as types from './actionTypes';

export const updateCreditCardList = (
  creditAccounts: Array<Object>,
): CreditAction => ({
  type: types.UPDATE_CREDIT_CARD_LIST,
  payload: creditAccounts,
});

export const setSelectedCredit = (credit: Object): CreditAction => ({
  type: types.SET_SELECTED_CREDIT,
  payload: credit,
});

export const getCreditCardDetails = (accountId: ?number): CreditAction => ({
  type: types.REQUEST_CREDIT_CARD_DETAILS,
  payload: accountId,
});

export const getCreditTransactionCurrent = (
  accountId: ?number,
): CreditAction => ({
  type: types.REQUEST_CREDIT_TRANSACTION_CURRENT_LIST,
  payload: {
    cardSerialNo: accountId,
  },
});

export const getCreditSOALastDetails = (accountId: ?number): CreditAction => ({
  type: types.REQUEST_SOA_LAST_DETAILS_API,
  payload: accountId,
});
