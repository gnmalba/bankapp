import CreditConfig from '@api/services/creditService';
import {expectSaga} from 'redux-saga-test-plan';
import {call} from 'redux-saga-test-plan/matchers';
import {
  addPesoAndForeignAccount,
  getCreditCards,
  getCreditCardDetails,
  getCreditTransactionCurrent,
  getSupplementaryTransactionCurrent,
  getSupplmentaryDetails,
} from '../sagas';

const CREDIT_CARDS = [
  {
    cardNo: '405598******1592',
    cardSerNo: '2686960',
    cardEmbossedName: 'JEMMALYN A JAVIEsR',
    customerId: '5688',
    primary: 1,
    flag: '0',
    outstandingBalance: 1000850,
    availableCredit: -850,
    creditLimit: 1000000,
    alias: "D5 MCC Employed Officer's Related Interests DOSRI",
    currency: 'PHP',
    type: 'creditcard',
  },
  {
    cardNo: '546498******7894',
    cardSerNo: '2686498',
    cardEmbossedName: 'JEMMALYN A JAVIER',
    customerId: '5688',
    primary: 0,
    flag: '0',
    outstandingBalance: 0,
    availableCredit: 500000,
    creditLimit: 500000,
    alias: 'HRP CARD C2G - Status XPDR',
    currency: 'SGD',
    type: 'creditcard',
  },
  {
    flag: 'NOT_ALLOWED',
    cardNo: '546498******7894',
    cardSerNo: '2686498',
    cardEmbossedName: 'JEMMALYN A JAVIER',
    customerId: '5688',
    primary: 0,
    outstandingBalance: 0,
    availableCredit: 500000,
    creditLimit: 500000,
    alias: 'HRP CARD C2G - Status XPDR',
    currency: 'PHP',
    type: 'creditcard',
  },
];

describe('Unit test for credit saga', () => {
  it('addPesoAndForeignAccount should work if credit is not empty', () => {
    return expectSaga(addPesoAndForeignAccount, CREDIT_CARDS).run();
  });
  it('addPesoAndForeignAccount should work if credit is empty', () => {
    return expectSaga(addPesoAndForeignAccount, []).run();
  });

  it('getCreditCards should work', () => {
    return expectSaga(getCreditCards)
      .withState({
        authentication: {
          data: {customerId: 'asd'},
          encryptionPublicKey: 'asd',
        },
      })
      .run();
  });
  it('getCreditCardDetails should work', () => {
    const response = {
      data: {
        data: [],
      },
    };

    return expectSaga(getCreditCardDetails, {payload: {}})
      .withState({
        authentication: {
          data: {customerId: 5688},
          encryptionPublicKey: 'asd',
        },
      })
      .provide([
        [call(CreditConfig.getCreditCards, 5688, 'creditcard'), response],
      ])
      .run();
  });
  it('getCreditTransactionCurrent should work', () => {
    return expectSaga(getCreditTransactionCurrent, {
      payload: {cardOwners: ['']},
    })
      .withState({
        authentication: {
          data: {customerId: 'asd'},
          encryptionPublicKey: 'asd',
        },
      })
      .run();
  });
  it('getSupplementaryTransactionCurrent should work', () => {
    return expectSaga(getSupplementaryTransactionCurrent, {
      payload: {trxnTypes: ['']},
    })
      .withState({
        authentication: {
          data: {customerId: 'asd'},
          encryptionPublicKey: 'asd',
        },
      })
      .run();
  });
  it('getSupplmentaryDetails should work', () => {
    return expectSaga(getSupplmentaryDetails, {
      payload: '2686960',
    })
      .withState({
        authentication: {
          data: {customerId: 'asd'},
          encryptionPublicKey: 'asd',
        },
        credit: {
          creditDetails: {
            data: {supplementaryCards: CREDIT_CARDS},
          },
        },
      })
      .run();
  });
});
