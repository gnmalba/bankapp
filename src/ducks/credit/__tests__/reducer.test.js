import reducer, {INITIAL_STATE} from '../reducer';
import {
  REQUEST_CREDIT_CARD_DETAILS,
  REQUEST_CREDIT_TRANSACTION_CURRENT_LIST,
} from '../actionTypes';

import {CREDIT_CARDS} from '@composition/Widget/__mocks__/accounts.mock';

describe('credit reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('should handle empty state', () => {
    expect(reducer(undefined, undefined)).toEqual(INITIAL_STATE);
  });

  it('should handle REQUEST_CREDIT_CARD_LIST', () => {
    const actionStarter = {
      type: 'REQUEST_CREDIT_CARD_LIST',
      payload: '1234',
    };

    const expectedState = {
      creditList: {
        loading: true,
        data: [],
        error: false,
        errors: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle REQUEST_CREDIT_CARD_DETAILS', () => {
    const actionStarter = {
      type: REQUEST_CREDIT_CARD_DETAILS,
      payload: '1234',
    };

    const expectedState = {
      creditDetails: {
        carouselCards: [],
        data: [],
        error: false,
        loader: [],
        loading: true,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_CREDIT_CARDS_SUCCESS', () => {
    const actionStarter = {
      type: 'GET_CREDIT_CARDS_SUCCESS',
      payload: CREDIT_CARDS,
    };

    const expectedState = {
      creditList: {
        loading: false,
        data: CREDIT_CARDS,
        error: false,
        errors: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_CREDIT_CARDS_FAILED', () => {
    const actionStarter = {
      type: 'GET_CREDIT_CARDS_FAILED',
      payload: ['error1', 'error2'],
    };

    const expectedState = {
      creditList: {
        loading: false,
        data: [],
        error: true,
        errors: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_CREDIT_CARDS_DETAILS_SUCCESS', () => {
    const actionStarter = {
      type: 'GET_CREDIT_CARDS_DETAILS_SUCCESS',
      payload: {someOtherData: {}, supplementaryCards: []},
    };

    const expectedState = {
      creditDetails: {
        loading: false,
        data: actionStarter.payload,
        error: false,
        loader: ['credit'],
        carouselCards: [
          actionStarter.payload,
          ...actionStarter.payload.supplementaryCards,
        ],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_CREDIT_CARDS_DETAILS_FAILED', () => {
    const actionStarter = {
      type: 'GET_CREDIT_CARDS_DETAILS_FAILED',
      payload: {someOtherData: {}, supplementaryCards: []},
    };

    const expectedState = {
      creditDetails: {
        loading: false,
        data: actionStarter.payload,
        error: true,
        loader: [],
        carouselCards: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle REQUEST_CREDIT_TRANSACTION_CURRENT_LIST', () => {
    const actionStarter = {
      type: REQUEST_CREDIT_TRANSACTION_CURRENT_LIST,
      payload: {cardSerialNo: '12312321'},
    };

    const expectedState = {
      crediTransactionCurrent: {
        loading: true,
        data: [],
        error: false,
        errors: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_CREDIT_TRANSACTION_CURRENT_SUCCESS', () => {
    const actionStarter = {
      type: 'GET_CREDIT_TRANSACTION_CURRENT_SUCCESS',
      payload: {transactionHistory: {someObject: {}}},
    };

    const expectedState = {
      crediTransactionCurrent: {
        loading: false,
        data: actionStarter.payload,
        error: false,
        errors: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_CREDIT_TRANSACTION_CURRENT_FAILED', () => {
    const actionStarter = {
      type: 'GET_CREDIT_TRANSACTION_CURRENT_FAILED',
      payload: [{}, {}],
    };

    const expectedState = {
      crediTransactionCurrent: {
        loading: false,
        data: [],
        error: true,
        errors: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle @credit/UPDATE_CREDIT_CARD_LIST', () => {
    const actionStarter = {
      type: '@credit/UPDATE_CREDIT_CARD_LIST',
      payload: [],
    };

    const expectedState = {
      creditList: {
        data: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle @credit/SET_SELECTED_CREDIT', () => {
    const actionStarter = {
      type: '@credit/SET_SELECTED_CREDIT',
      payload: {someCreditData: {}, shortcuts: []},
    };

    const expectedState = {
      selectedCredit: actionStarter.payload,
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle CREDIT_LOGOUT_RESET_STATE', () => {
    const actionStarter = {
      type: 'CREDIT_LOGOUT_RESET_STATE',
    };

    expect(reducer(undefined, actionStarter)).toEqual(INITIAL_STATE);
  });
});

describe('credit supplementary reducer', () => {
  it('should handle REQUEST_SUPPLEMENTARY_DETAILS', () => {
    const actionStarter = {
      type: 'REQUEST_SUPPLEMENTARY_DETAILS',
    };

    const expectedState = {
      supplementaryDetails: {
        loading: true,
        data: {},
        loader: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_SUPPLEMENTARY_CARDS_DETAILS_SUCCESS', () => {
    const actionStarter = {
      type: 'GET_SUPPLEMENTARY_CARDS_DETAILS_SUCCESS',
      payload: {sampleResponse: {}},
    };

    const expectedState = {
      supplementaryDetails: {
        loading: false,
        data: actionStarter.payload,
        loader: ['supplementary'],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST', () => {
    const actionStarter = {
      type: 'REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST',
    };

    const expectedState = {
      supplementaryTransactionCurrent: {
        loading: true,
        data: [],
        error: false,
        errors: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_SUPPLEMENTARY_TRANSACTION_CURRENT_SUCCESS', () => {
    const actionStarter = {
      type: 'GET_SUPPLEMENTARY_TRANSACTION_CURRENT_SUCCESS',
      payload: {transactionHistory: {}},
    };

    const expectedState = {
      supplementaryTransactionCurrent: {
        loading: false,
        data: actionStarter.payload,
        error: false,
        errors: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_SUPPLEMENTARY_TRANSACTION_CURRENT_FAILED', () => {
    const actionStarter = {
      type: 'GET_SUPPLEMENTARY_TRANSACTION_CURRENT_FAILED',
      payload: [],
    };

    const expectedState = {
      supplementaryTransactionCurrent: {
        loading: false,
        data: [],
        error: true,
        errors: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });
});
