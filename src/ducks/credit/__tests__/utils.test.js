import {reconstructTransactionData} from '../utils';
import {
  CREDIT_CURRENT_TRANSACTION_MOCKS,
  PREPAID_CURRENT_TRANSACTION_MOCKS,
} from '../__mocks__/credit.mocks';

describe('credit selectors', () => {
  it('should run reconstructTransactionData with credit properly', () => {
    const mockCreditResponse = {
      data: CREDIT_CURRENT_TRANSACTION_MOCKS,
    };

    const reconstructedData = reconstructTransactionData(
      mockCreditResponse,
      true,
    );

    expect(reconstructedData).toBe(reconstructedData);
  });

  it('should run reconstructTransactionData with non-credit properly', () => {
    const mockPrepaidResponse = {
      data: PREPAID_CURRENT_TRANSACTION_MOCKS,
    };

    const reconstructedData = reconstructTransactionData(
      mockPrepaidResponse,
      false,
    );

    expect(reconstructedData).toBe(reconstructedData);
  });
});
