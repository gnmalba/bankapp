import {
  getCreditCardDetails,
  getCreditSOALastDetails,
  getCreditTransactionCurrent,
  setSelectedCredit,
  updateCreditCardList,
} from '../actions';

import {
  REQUEST_CREDIT_CARD_DETAILS,
  REQUEST_CREDIT_TRANSACTION_CURRENT_LIST,
  REQUEST_SOA_LAST_DETAILS_API,
  SET_SELECTED_CREDIT,
  UPDATE_CREDIT_CARD_LIST,
} from '../actionTypes';

describe('Unit test for Credit Actions', () => {
  it('Should create getCreditCardDetails properly', () => {
    const id = 2320;

    const expectedAction = {
      type: REQUEST_CREDIT_CARD_DETAILS,
      payload: id,
    };

    expect(getCreditCardDetails(id)).toEqual(expectedAction);
  });

  it('Should create getCreditSOALastDetails properly', () => {
    const id = 2320;

    const expectedAction = {
      type: REQUEST_SOA_LAST_DETAILS_API,
      payload: id,
    };

    expect(getCreditSOALastDetails(id)).toEqual(expectedAction);
  });

  it('Should create getCreditTransactionCurrent properly', () => {
    const id = 2320;

    const expectedAction = {
      type: REQUEST_CREDIT_TRANSACTION_CURRENT_LIST,
      payload: {cardSerialNo: id},
    };

    expect(getCreditTransactionCurrent(id)).toEqual(expectedAction);
  });

  it('Should create setSelectedCredit properly', () => {
    const mockCreditObject = {accountId: '', shortcuts: []};

    const expectedAction = {
      type: SET_SELECTED_CREDIT,
      payload: mockCreditObject,
    };

    expect(setSelectedCredit(mockCreditObject)).toEqual(expectedAction);
  });

  it('Should create updateCreditCardList  properly', () => {
    const mockCreditObject = [
      {accountId: 123, shortcuts: []},
      {accountId: 444, shortcuts: []},
    ];

    const expectedAction = {
      type: UPDATE_CREDIT_CARD_LIST,
      payload: mockCreditObject,
    };

    expect(updateCreditCardList(mockCreditObject)).toEqual(expectedAction);
  });
});
