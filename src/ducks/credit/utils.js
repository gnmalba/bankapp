/**
 * @flow
 */

const currencyCode = (item: string) => {
  return item === null ? 'PHP' : item;
};

const ccAmountCondition = (x) => (x.transactionType === 'DB' ? '+' : '-');
const yazzAmountCondition = (x) => (x.transactionType === 'DB' ? '-' : '+');
const ccSignCondition = (x) => (x.transactionType === 'DB' ? true : false);
const yazzSignCondition = (x) => (x.transactionType === 'DB' ? false : true);

export const reconstructTransactionData = (
  response: any,
  isCredit: boolean,
): any => {
  const reconstructItem = [];

  if (response.data.data.length > 0) {
    response.data.data.map((item) => [
      isCredit &&
        reconstructItem.push({
          title: true,
          cardEmbossingName: item.cardEmbossingName,
          cardNo: item.cardNo,
        }),

      item.details.map((x) => {
        const itemTransaction = {
          amount: {
            currencyCode: currencyCode(x.billCurrency),
            value: x.billAmount,
          },
          description: {
            line1: x.description,
          },
          transactionDate: x.transactionDate,
          posted: x.posted === 'Y' ? true : false,
          transactionType: x.transactionType,
          amountSign: isCredit ? ccAmountCondition(x) : yazzAmountCondition(x),
          isPositve: isCredit ? ccSignCondition(x) : yazzSignCondition(x),
          transactionAmount: x.transactionAmount,
          transactionCurrency: x.transactionCurrency,
        };
        reconstructItem.push(itemTransaction);
      }),
    ]);
  }

  return reconstructItem;
};
