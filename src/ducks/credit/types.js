/**
 * @flow
 * @format
 */

export type T = Generator<any, any, any>;

export type CreditState = {
  creditList: {
    loading?: boolean,
    data?: Array<any>,
    error?: boolean,
    errors?: Array<any>,
  },
  selectedCredit: ?Object,
  creditDetails: {
    loading: boolean,
    data: Object,
    error: boolean,
    loader: Array<string>,
    carouselCards: Array<Object>,
  },
  crediTransactionCurrent: {
    loading: boolean,
    data: Array<any>,
    error: boolean,
    errors: Array<any>,
  },
  supplementaryDetails: {
    loading: boolean,
    data: Object,
    loader: Array<any>,
  },
};

export type CreditAction =
  | {
      type: '@credit/UPDATE_CREDIT_CARD_LIST',
      payload: Array<Object>,
    }
  | {type: '@credit/SET_SELECTED_CREDIT', payload: Object}
  | {type: 'REQUEST_CREDIT_CARD_DETAILS', payload: Object}
  | {type: 'REQUEST_CREDIT_TRANSACTION_CURRENT_LIST', payload: Object}
  | {type: 'REQUEST_SOA_LAST_DETAILS_API', payload: Object};

export type CreditList = {
  loading?: boolean,
  data?: Array<any>,
  error?: boolean,
  errors?: Array<any>,
};
