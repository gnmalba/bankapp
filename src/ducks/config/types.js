/**
 * @flow
 * @format
 */

export type THEME = 'dark' | 'light';
export type REF = number;
export type REFS = Array<REF>;

export type Widget = {|
  +ref: REF,
  +label: string,
  +loading: boolean,
|};

export type ConfigState = {|
  theme: THEME,
  widgets: Array<Widget>,
  checked: REFS,
  customizedWidgets: REFS,
  removedWidgets: REFS,
  limitShown: boolean,
|};

export type ConfigLocalStorage = {
  theme: THEME,
  widgets: REFS,
};

export type Direction = 'up' | 'down';

export type ConfigApiResponse = ?ConfigLocalStorage;

export type ConfigAction =
  | {type: '@config/INIT'}
  | {type: 'GET_ASYNC_LOCAL_CONFIG'}
  | {
      type: 'GET_ASYNC_LOCAL_CONFIG_SUCCESS',
      +payload: {
        data: ConfigLocalStorage,
      },
    }
  | {type: 'GET_CONFIG_API_REQUESTED'}
  | {type: 'GET_CONFIG_API_SUCCESS'}
  | {type: 'GET_CONFIG_API_FAILURE'}
  | {
      type: 'UPDATE_CHECKED_WIDGETS',
      payload: {
        checked: REFS,
      },
    }
  | {
      type: 'UPDATE_WIDGET_LOADING',
      +payload: {
        ref: REF,
        state: boolean,
      },
    }
  | {
      type: 'UPDATE_WIDGETS',
      +payload: {
        widgets: Widget[],
      },
    }
  | {
      type: 'ADD_WIDGET',
      payload: {
        ref: REF,
      },
    }
  | {
      type: 'REMOVE_WIDGET',
      payload: {
        ref: REF,
        removeImmediately: boolean,
      },
    }
  | {
      type: 'MOVE_WIDGET',
      payload: {
        ref: REF,
        direction: Direction,
      },
    }
  | {
      type: 'REMOVE_DISABLED_WIDGET',
    }
  | {
      type: 'SET_APP_THEME',
      payload: {
        theme: THEME,
      },
    }
  | {
      type: 'ADD_WIGET_TO_BE_RESOLVE',
      payload: {
        ref: REF,
      },
    }
  | {
      type: 'RESOLVE_WIGET',
      payload: {
        ref: REF,
      },
    }
  | {
      type: 'UPDATE_CHECKED_WIDGETS_FOR_CUSTOMIZATION',
      payload: {
        customizedWidgets: REFS,
      },
    }
  | {
      type: 'RESET_CUSOMIZATION_WIDGETS',
    }
  | {
      type: 'ADD_TO_REMOVED_WIDGETS',
      payload: {
        ref: REF,
      },
    }
  | {
      type: 'TOGGLE_REMOVE_LIMIT',
      payload: {
        toggle: boolean,
      },
    }
  | {
      type: 'RESET_WIDGETS_CONFIG',
    }
  | {
      type: 'API_UPDATE_THEME',
      payload: {
        theme: THEME,
      },
    }
  | {
      type: 'UPDATE_CONFIG',
      payload: ConfigLocalStorage,
    }
  | {
      type: 'API_UPDATE_WIDGETS_PLACEMENTS',
      payload: {
        widgets: REFS,
      },
    };
