/**
 * @flow
 */

import type {
  ConfigAction,
  ConfigLocalStorage,
  REFS,
  Widget,
  REF,
  Direction,
  THEME,
} from './types';

export const getConfigFromLS = (): ConfigAction => ({
  type: 'GET_ASYNC_LOCAL_CONFIG',
});

export const getConfigFromLSSuccess = (
  data: ConfigLocalStorage,
): ConfigAction => ({
  type: 'GET_ASYNC_LOCAL_CONFIG_SUCCESS',
  payload: {data},
});

export const updateCheckedWidgets = (checked: REFS): ConfigAction => ({
  type: 'UPDATE_CHECKED_WIDGETS',
  payload: {
    checked,
  },
});

export const updateWidgets = (widgets: Widget[]): ConfigAction => ({
  type: 'UPDATE_WIDGETS',
  payload: {widgets},
});

export const getConfigFromAPIRequested = (): ConfigAction => ({
  type: 'GET_CONFIG_API_REQUESTED',
});

export const getConfigFromAPISuccess = (): ConfigAction => ({
  type: 'GET_CONFIG_API_SUCCESS',
});

export const getConfigFromAPIFailure = (): ConfigAction => ({
  type: 'GET_CONFIG_API_FAILURE',
});

export const updateWidgetLoadingState = (
  ref: number,
  state: boolean,
): ConfigAction => ({
  type: 'UPDATE_WIDGET_LOADING',
  payload: {ref, state},
});

export const addWidget = (ref: REF): ConfigAction => ({
  type: 'ADD_WIDGET',
  payload: {ref},
});

export const removeWidget = (
  ref: REF,
  removeImmediately: boolean = false,
): ConfigAction => ({
  type: 'REMOVE_WIDGET',
  payload: {ref, removeImmediately},
});

export const moveWidget = (ref: REF, direction: Direction): ConfigAction => ({
  type: 'MOVE_WIDGET',
  payload: {ref, direction},
});

export const removeDisabledWidget = (): ConfigAction => ({
  type: 'REMOVE_DISABLED_WIDGET',
});

export const setTheme = (theme: THEME): ConfigAction => ({
  type: 'SET_APP_THEME',
  payload: {
    theme,
  },
});

export const apiUpdateTheme = (theme: THEME): ConfigAction => ({
  type: 'API_UPDATE_THEME',
  payload: {
    theme,
  },
});

export const addWidgetToBeResolve = (ref: REF): ConfigAction => ({
  type: 'ADD_WIGET_TO_BE_RESOLVE',
  payload: {
    ref,
  },
});

export const resolveWidget = (ref: REF): ConfigAction => ({
  type: 'RESOLVE_WIGET',
  payload: {
    ref,
  },
});

export const updateCheckedWidgetsForCustomization = (
  customizedWidgets: REFS,
): ConfigAction => ({
  type: 'UPDATE_CHECKED_WIDGETS_FOR_CUSTOMIZATION',
  payload: {customizedWidgets},
});

export const removeWidgetFromSource = (ref: REF): ConfigAction => ({
  type: 'ADD_TO_REMOVED_WIDGETS',
  payload: {ref},
});

export const toggleRemoveLimit = (toggle: boolean): ConfigAction => ({
  type: 'TOGGLE_REMOVE_LIMIT',
  payload: {
    toggle,
  },
});

export const updateConfig = ({
  theme,
  widgets,
}: ConfigLocalStorage): ConfigAction => ({
  type: 'UPDATE_CONFIG',
  payload: {
    theme,
    widgets,
  },
});

export const updateWidgetsPlacement = (widgets: REFS): ConfigAction => ({
  type: 'API_UPDATE_WIDGETS_PLACEMENTS',
  payload: {
    widgets,
  },
});
