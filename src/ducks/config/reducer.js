/**
 * @format
 * @flow
 */

import type {ConfigState, ConfigAction, Widget, REFS} from './types';
import {APPEARANCE_SETTINGS} from '@utils/manageSettings';

export const createWigetObject = (
  ref: number,
  label: string,
  loading: boolean = false,
): Widget => ({
  ref,
  label,
  loading,
});

/**
 * TODO:
 * Checked widgets should came from reducer default if local storage config is empty
 */
export const CHECKED_WIDGETS: REFS = [1, 2, 3, 5, 4];

export const ALL_WIDGETS: Array<Widget> = [
  createWigetObject(1, 'Deposits'),
  createWigetObject(2, 'Credit Card'),
  createWigetObject(3, 'Prepaid Card'),
  createWigetObject(4, 'Shortcuts'),
  createWigetObject(5, 'Card Installment'),
  createWigetObject(6, 'Inbox'),
  createWigetObject(7, 'Activity Log'),
  // createWigetObject(8, 'Recent Transactions'), // deferred
];

const INITIAL_STATE: ConfigState = {
  theme: APPEARANCE_SETTINGS.DARK_VALUE,
  widgets: ALL_WIDGETS,
  removedWidgets: [],
  checked: CHECKED_WIDGETS,
  limitShown: false,
  /**
   * We need to store widget state in temporary variable and update later when done button was clicked
   */
  customizedWidgets: CHECKED_WIDGETS,
};

const config = (
  state: ConfigState = INITIAL_STATE,
  action: ConfigAction,
): ConfigState => {
  switch (action.type) {
    case 'TOGGLE_REMOVE_LIMIT':
      return {
        ...state,
        limitShown: action.payload.toggle,
      };
    case 'ADD_TO_REMOVED_WIDGETS': {
      const actionRef = action.payload.ref;
      return {
        ...state,
        removedWidgets: [...state.removedWidgets, actionRef],
        checked: state.checked.filter((ref) => ref !== actionRef),
        customizedWidgets: state.customizedWidgets.filter(
          (ref) => ref !== actionRef,
        ),
      };
    }
    case 'UPDATE_CHECKED_WIDGETS': {
      const checked = action.payload.checked;
      return {
        ...state,
        checked,
        customizedWidgets: checked,
      };
    }
    case 'UPDATE_CHECKED_WIDGETS_FOR_CUSTOMIZATION':
      return {
        ...state,
        customizedWidgets: action.payload.customizedWidgets,
      };
    case 'RESET_CUSOMIZATION_WIDGETS':
      return {
        ...state,
        customizedWidgets: state.checked,
      };
    case 'UPDATE_WIDGET_LOADING':
      return {
        ...state,
        widgets: state.widgets.map((row) => {
          if (row.ref !== action.payload.ref) {
            return row;
          }

          return {
            ...row,
            loading: action.payload.state,
          };
        }),
      };
    case 'SET_APP_THEME': {
      return {
        ...state,
        theme: action.payload.theme,
      };
    }
    case 'UPDATE_CONFIG': {
      const {theme, widgets} = action.payload;
      return {
        ...state,
        theme,
        checked: widgets,
        customizedWidgets: widgets,
      };
    }
    case 'RESET_WIDGETS_CONFIG':
      return {
        ...state,
        widgets: INITIAL_STATE.widgets,
        removedWidgets: INITIAL_STATE.removedWidgets,
        checked: INITIAL_STATE.checked,
      };
    default:
      return state;
  }
};

export default config;
