import type {ConfigState} from '../types';
import reducer, {CHECKED_WIDGETS, ALL_WIDGETS} from '../reducer';
import {APPEARANCE_SETTINGS} from '@utils/manageSettings';

const INITIAL_STATE: ConfigState = {
  theme: APPEARANCE_SETTINGS.DARK_VALUE,
  widgets: ALL_WIDGETS,
  removedWidgets: [],
  checked: CHECKED_WIDGETS,
  limitShown: false,
  temporaryTheme: APPEARANCE_SETTINGS.DARK_VALUE,
  /**
   * We need to store widget state in temporary variable and update later when done button was clicked
   */
  customizedWidgets: CHECKED_WIDGETS,
};
describe('config reducer', () => {
  const payload = {
    data: {},
  };

  it('should handle TOGGLE_REMOVE_LIMIT', () => {
    const action = {
      type: 'TOGGLE_REMOVE_LIMIT',
      payload: {toogle: false},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      limitShown: action.payload.toggle,
    });
  });

  it('should handle ADD_TO_REMOVED_WIDGETS', () => {
    const action = {
      type: 'ADD_TO_REMOVED_WIDGETS',
      payload: {
        ref: 1,
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      removedWidgets: [...INITIAL_STATE.removedWidgets, action.payload.ref],
      checked: INITIAL_STATE.checked.filter(
        (ref) => ref !== action.payload.ref,
      ),
      customizedWidgets: INITIAL_STATE.customizedWidgets.filter(
        (ref) => ref !== action.payload.ref,
      ),
    });
  });

  it('should handle UPDATE_CHECKED_WIDGETS_FOR_CUSTOMIZATION', () => {
    const action = {
      type: 'UPDATE_CHECKED_WIDGETS_FOR_CUSTOMIZATION',
      payload: INITIAL_STATE.customizedWidgets,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      customizedWidgets: action.payload.customizedWidgets,
    });
  });

  it('should handle RESET_CUSOMIZATION_WIDGETS', () => {
    const action = {
      type: 'RESET_CUSOMIZATION_WIDGETS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      customizedWidgets: INITIAL_STATE.checked,
    });
  });

  it('should handle UPDATE_WIDGET_LOADING', () => {
    const action = {
      type: 'UPDATE_WIDGET_LOADING',
      payload: {ref: 1, state: false},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      widgets: INITIAL_STATE.widgets.map((row) => {
        if (row.ref !== action.payload.ref) {
          return row;
        }

        return {
          ...row,
          loading: action.payload.state,
        };
      }),
    });
  });

  it('should handle SET_APP_THEME', () => {
    const action = {
      type: 'SET_APP_THEME',
      payload: {theme: INITIAL_STATE.theme},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      theme: action.payload.theme,
    });
  });

  it('should handle SET_TEMPORARY_THEME', () => {
    const action = {
      type: 'SET_TEMPORARY_THEME',
      payload: {theme: INITIAL_STATE.theme},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      temporaryTheme: action.payload.theme,
    });
  });

  it('should handle RESET_WIDGETS_CONFIG', () => {
    const action = {
      type: 'RESET_WIDGETS_CONFIG',
      payload: {theme: INITIAL_STATE.theme},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      widgets: INITIAL_STATE.widgets,
      removedWidgets: INITIAL_STATE.removedWidgets,
      checked: INITIAL_STATE.checked,
    });
  });

  it('should handle UPDATE_CHECKED_WIDGETS', () => {
    const action = {
      type: 'UPDATE_CHECKED_WIDGETS',
      payload: {checked: INITIAL_STATE.checked},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      checked: action.payload.checked,
      customizedWidgets: action.payload.checked,
    });
  });
  it('should handle default state', () => {
    const action = {
      type: 'UPDATE_CHECKED_WIDGETSS',
      payload: {checked: INITIAL_STATE.checked},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
    });
  });
});
