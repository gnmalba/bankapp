import * as actions from '../actions';

describe('deposit actions', () => {
  const payload = {
    checked: {},
    widgets: [],
    ref: 1,
    state: true,
    direction: '',
    removeImmediately: false,
    customizedWidgets: 1,
    theme: '',
    toggle: true,
    data: {
      theme: '',
      widgets: '',
    },
  };

  it('handle async local config', () => {
    const action = actions.getConfigFromLS();
    const expectedAction = {
      type: 'GET_ASYNC_LOCAL_CONFIG',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get config from LS Success', () => {
    const action = actions.getConfigFromLSSuccess(payload.data);
    let data = {theme: '', widgets: ''};
    const expectedAction = {
      type: 'GET_ASYNC_LOCAL_CONFIG_SUCCESS',
      payload: {
        data,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle Update Check Widgets', () => {
    const action = actions.updateCheckedWidgets(payload.checked);
    const expectedAction = {
      type: 'UPDATE_CHECKED_WIDGETS',
      payload: {
        checked: {},
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update widgets', () => {
    const action = actions.updateWidgets(payload.widgets);
    const expectedAction = {
      type: 'UPDATE_WIDGETS',
      payload: {widgets: []},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get Config from API Requested', () => {
    const action = actions.getConfigFromAPIRequested(payload);
    const expectedAction = {
      type: 'GET_CONFIG_API_REQUESTED',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  get config from api success', () => {
    const action = actions.getConfigFromAPISuccess(payload);
    const expectedAction = {
      type: 'GET_CONFIG_API_SUCCESS',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  get config from api failed', () => {
    const action = actions.getConfigFromAPIFailure(payload);
    const expectedAction = {
      type: 'GET_CONFIG_API_FAILURE',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  update widget loading state', () => {
    const action = actions.updateWidgetLoadingState(payload.ref, payload.state);
    const expectedAction = {
      type: 'UPDATE_WIDGET_LOADING',
      payload: {ref: 1, state: true},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  add widget', () => {
    const action = actions.addWidget(payload.ref);
    const expectedAction = {
      type: 'ADD_WIDGET',
      payload: {ref: 1},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  remove widget', () => {
    const action = actions.removeWidget(payload.ref, payload.removeImmediately);
    const expectedAction = {
      type: 'REMOVE_WIDGET',
      payload: {ref: 1, removeImmediately: false},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  move widget', () => {
    const action = actions.moveWidget(payload.ref, payload.direction);
    const expectedAction = {
      type: 'MOVE_WIDGET',
      payload: {ref: 1, direction: ''},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle  remove disable widget', () => {
    const action = actions.removeDisabledWidget();
    const expectedAction = {
      type: 'REMOVE_DISABLED_WIDGET',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle set Theme', () => {
    const action = actions.setTheme(payload.theme);
    const expectedAction = {
      type: 'SET_APP_THEME',
      payload: {theme: ''},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle add widget to be resolve', () => {
    const action = actions.addWidgetToBeResolve(payload.ref);
    const expectedAction = {
      type: 'ADD_WIGET_TO_BE_RESOLVE',
      payload: {ref: 1},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle resolve widget', () => {
    const action = actions.resolveWidget(payload.ref);
    const expectedAction = {
      type: 'RESOLVE_WIGET',
      payload: {ref: 1},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update checked widges for customization', () => {
    const action = actions.updateCheckedWidgetsForCustomization(
      payload.customizedWidgets,
    );
    const expectedAction = {
      type: 'UPDATE_CHECKED_WIDGETS_FOR_CUSTOMIZATION',
      payload: {customizedWidgets: 1},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle remove widgets from source', () => {
    const action = actions.removeWidgetFromSource(payload.ref);
    const expectedAction = {
      type: 'ADD_TO_REMOVED_WIDGETS',
      payload: {ref: 1},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle toggle Remove Limit', () => {
    const action = actions.toggleRemoveLimit(payload.toggle);
    const expectedAction = {
      type: 'TOGGLE_REMOVE_LIMIT',
      payload: {toggle: true},
    };
    expect(action).toEqual(expectedAction);
  });
});
