/**
 * @flow
 */

import type {REF, REFS, Direction, ConfigLocalStorage} from './types';

/**
 * Check if widget is movable up or down and vice versa
 * @param {*} currentRefPosition
 * @param {*} currentCheckedWidgets
 * @param {*} direction
 * @returns boolean
 */
export const widgetIsMovable = (
  currentRefPosition: number,
  currentCheckedWidgets: REFS,
  direction: Direction,
): boolean =>
  (currentRefPosition === 0 && direction === 'up') ||
  (currentRefPosition === currentCheckedWidgets.length - 1 &&
    direction === 'down');

/**
 * Build new widget position
 * @param {*} direction
 * @param {*} currentRefPosition
 * @param {*} currentCheckedWidgets
 * @param {*} ref
 * @returns Array<REF> | REFS
 */
export const buildNewWidgetPosition = (
  direction: Direction,
  currentRefPosition: number,
  currentCheckedWidgets: REFS,
  ref: REF,
): REFS => {
  const newPosition =
    direction === 'up' ? currentRefPosition - 1 : currentRefPosition + 1;

  // get prev value from new position to use later
  const prevValueForNewPosition = currentCheckedWidgets[newPosition];

  return currentCheckedWidgets.map((_ref, _index) => {
    if (_index === currentRefPosition) {
      return prevValueForNewPosition;
    } else if (_index === newPosition) {
      return ref;
    } else {
      return _ref;
    }
  });
};

/**
 * Check if config object is valid
 * @param {*} obj
 * @returns boolean
 */
export const isValidConfigObject = (obj: ConfigLocalStorage): boolean =>
  obj !== null &&
  typeof obj === 'object' &&
  obj.hasOwnProperty('theme') &&
  obj.hasOwnProperty('widgets');
