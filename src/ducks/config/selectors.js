/**
 * @flow
 */
import {createSelector} from 'reselect';

import type {State} from '@ducks/types';
import type {THEME, Widget, REFS} from './types';
import {manageSettingsSelector} from '@ducks/managesettings/selectors';

export const getTheme = (state: State): THEME => state.config.theme;
export const getThemeForMainApp: THEME = createSelector(
  getTheme,
  manageSettingsSelector,
  (theme, {temporaryTheme}) => {
    if (!temporaryTheme || (temporaryTheme && temporaryTheme === theme)) {
      return theme;
    }

    return temporaryTheme;
  },
);
export const getWidgets = (state: State): Array<Widget> =>
  state?.config?.widgets;
export const getCheckedWidgets = (state: State): REFS => state.config.checked;
export const getRemovedWidgets = (state: State): REFS =>
  state.config.removedWidgets;

export const getConfig = (state: State): Object => state.config;
export const getCheckedWidgetsForCustomization = (state: State): REFS =>
  state.config.customizedWidgets;

const mapAndFilterActiveWidgets = (checked, widgets) =>
  checked.map((ref) => widgets.find((_ref) => _ref.ref === ref));

export const getActiveWidgets: Array<Widget> = createSelector(
  getWidgets,
  getCheckedWidgets,
  (widgets, checked) => mapAndFilterActiveWidgets(checked, widgets),
);

export const getActiveWidgetsForCustomization: Array<Widget> = createSelector(
  getCheckedWidgetsForCustomization,
  getWidgets,
  (checked, widgets) => mapAndFilterActiveWidgets(checked, widgets),
);

export const getInActiveWidgetsForCustomization: Array<Widget> = createSelector(
  getWidgets,
  getCheckedWidgetsForCustomization,
  getRemovedWidgets,
  (widgets, checked, removedWidgets) =>
    widgets.filter(
      (row) =>
        checked.indexOf(row.ref) === -1 && !removedWidgets.includes(row.ref),
    ),
);

export const getToggleState = (state: State): boolean =>
  state.config.limitShown;
