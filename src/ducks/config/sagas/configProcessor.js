/**
 * @flow
 */

import {
  call,
  spawn,
  select,
  cancel,
  take,
  put,
  all,
  fork,
} from 'redux-saga/effects';

import type {ConfigLocalStorage, ConfigApiResponse, THEME} from '../types';

import {APPEARANCE_SETTINGS} from '@utils/manageSettings';
import {CHECKED_WIDGETS} from '../reducer';

import api from '@api/services/configService';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {getUsername, getCustomerID} from '@ducks/commonSelectors';

import {setTheme, updateConfig} from '../actions';
import {getConfig} from '../selectors';

import widgetsModificatorWatchers from './widgetsModificator';
import widgetsResolverWatchers from './widgetsResolver';

import {isValidConfigObject} from '../utils';

const DEFAULT_CONFIG: ConfigLocalStorage = {
  theme: APPEARANCE_SETTINGS.DARK_VALUE,
  widgets: CHECKED_WIDGETS,
};

export function* setUserConfigurationFromApi(
  username: string,
  customerId: string,
  config: ConfigLocalStorage,
): Generator<any, boolean, any> {
  try {
    yield call(api.update, {username, customerId}, config);
    return true;
  } catch (apiSaveConfigError) {
    console.error('apiSaveConfigError', apiSaveConfigError);

    return false;
  }
}

function* getUserConfigurationFromApi(
  username: string,
  customerId: string,
): Generator<any, ConfigApiResponse, any> {
  try {
    const response = yield call(api.get, username, customerId);

    return response.data?.data;
  } catch (apiGetConfigError) {
    console.error('apiGetConfigError', apiGetConfigError);

    return null;
  }
}

export const getConfigLocalStorage = async (): Promise<?string> => {
  try {
    return await AsyncStorage.getItem('@APP:CONFIG');
  } catch (asyncStorageFetchingError) {
    /**
     * if config async storage was not set show console error and return null
     */

    console.error('asyncStorageFetchingError', asyncStorageFetchingError);

    return null;
  }
};

export const setConfigLocalStorage = async (
  localConfig: ConfigLocalStorage,
): Promise<boolean> => {
  try {
    await AsyncStorage.setItem('@APP:CONFIG', JSON.stringify(localConfig));
    return true;
  } catch (asyncStorageSavingError) {
    /**
     * if config async storage was not set show console error and return null
     */

    console.error('asyncStorageSavingError', asyncStorageSavingError);

    return false;
  }
};

/**
 * Get local configuration and returns default if LS is empty
 * @returns ConfigLocalStorage | null
 */
function* getConfigAndValidate(): Generator<any, ?ConfigLocalStorage, any> {
  const localConfig = yield call(getConfigLocalStorage);

  if (!localConfig) {
    return {
      theme: APPEARANCE_SETTINGS.DARK_VALUE,
      widgets: CHECKED_WIDGETS,
    };
  }

  const parsedLocalConfig = JSON.parse(localConfig);

  return isValidConfigObject(parsedLocalConfig) ? parsedLocalConfig : null;
}

function* setDefaultConfig(): Generator<any, void, any> {
  const wasSuccessfullySet = yield call(setConfigLocalStorage, DEFAULT_CONFIG);

  if (!wasSuccessfullySet) {
    // terminate process since it was already defined as default in config reducer
    yield cancel();
  }
}

export function* updateConfigTheme({
  payload: {theme},
}: {
  payload: {theme: THEME},
}): Generator<any, void, any> {
  const localConfig = yield getConfigAndValidate();
  const username = yield select(getUsername);
  const customerId = yield select(getCustomerID);

  const newConfig = {
    ...localConfig,
    theme,
  };

  const apiSuccess = yield call(
    setUserConfigurationFromApi,
    username,
    customerId,
    {...newConfig},
  );

  const setLocalConfigSuccess = yield call(setConfigLocalStorage, newConfig);

  if (apiSuccess && setLocalConfigSuccess) {
    yield put(setTheme(theme));
  } else {
    yield spawn(setDefaultConfig);
  }
}

export function* watchGetCustomerInfoSuccess(): Generator<any, void, any> {
  while (yield take('GET_CUSTOMER_INFO_SUCCESS')) {
    const username = yield select(getUsername);
    const customerId = yield select(getCustomerID);
    const config = yield select(getConfig);

    const configApiResponse = yield call(
      getUserConfigurationFromApi,
      username,
      customerId,
    );

    if (configApiResponse) {
      const latestConfig = configApiResponse.setting.properties;

      if (!isValidConfigObject(latestConfig)) {
        /*
         * if config from api is not valid, terminate saga
         * default config should be selected
         * */
        yield cancel();
      }

      if (
        JSON.stringify(latestConfig) !==
        JSON.stringify({theme: config.theme, checked: config.checked})
      ) {
        // should sync from latest config
        yield call(setConfigLocalStorage, latestConfig);
        yield all([
          fork(widgetsModificatorWatchers),
          fork(widgetsResolverWatchers),
        ]);
      }
    }
  }
}

export default function* processAppConfigInit(): Generator<any, void, any> {
  const localStorage = yield call(getConfigLocalStorage);

  // if local config is not yet proccess,
  if (!localStorage) {
    yield spawn(setDefaultConfig);
  } else {
    const parsedLocalStorage = JSON.parse(localStorage);

    // check localstorage validity, if not valid set to default config
    if (!isValidConfigObject(parsedLocalStorage)) {
      yield spawn(setDefaultConfig);
    } else if (localStorage !== JSON.stringify(DEFAULT_CONFIG)) {
      yield put(updateConfig(parsedLocalStorage));
    }
  }
}
