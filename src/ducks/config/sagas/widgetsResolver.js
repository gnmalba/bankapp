/**
 * @flow
 */

import {
  select,
  put,
  cancel,
  all,
  take,
  takeLatest,
  fork,
} from 'redux-saga/effects';

import {removeWidgetFromSource, updateWidgetLoadingState} from '../actions';

import {getPrepaidFromDeposit} from '@ducks/deposit/selectors';
import {yazzSelector} from '@ducks/yazz/selectors';
import {
  initializeShortcutsForInstallmentsWidget,
  initializeShortcutsForWidget,
} from '@ducks/shortcuts/actions';

function* validateWidgetsForDepositAndPrepaid(): Generator<any, void, any> {
  while (true) {
    const {
      payload: {
        data: {deposits},
      },
    } = yield take('GET_DEPOSIT_API_SUCCESS');

    if (
      deposits.savingsAndDeposits.length === 0 &&
      deposits.timeDeposits.length === 0
    ) {
      yield put(removeWidgetFromSource(1));
    }

    yield put(updateWidgetLoadingState(1, false));
  }
}

function* validateWidgetsForYazzAndPrepaids(): Generator<any, void, any> {
  yield put(updateWidgetLoadingState(3, true));

  yield all([take('GET_DEPOSIT_API_SUCCESS'), take('YAZZ_API_RESOLVED')]);

  const mbPrepaids = yield select(getPrepaidFromDeposit);
  const yazzAccounts = yield select(yazzSelector);

  const prepaids = [...mbPrepaids, ...yazzAccounts];

  if (prepaids.length === 0) {
    yield put(removeWidgetFromSource(3));
  }

  yield put(updateWidgetLoadingState(3, false));
}

function* validateWidgetsForCredit(): Generator<any, void, any> {
  while (true) {
    yield take('GET_CREDIT_CARDS_FAILED');
    yield put(removeWidgetFromSource(2));
  }
}

function* validateWidgetForShortcuts(): Generator<any, void, any> {
  yield put(updateWidgetLoadingState(4, true));
  yield all([
    take('GET_DEPOSIT_API_SUCCESS'),
    take('YAZZ_API_RESOLVED'),
    take('GET_CREDIT_CARDS_RESOLVED'),
  ]);
  yield put(updateWidgetLoadingState(4, false));
  yield put(initializeShortcutsForWidget());
}

function* validateWidgetForCardInstallment({
  payload,
  type,
}: {
  payload: Array<[]>,
  type: 'GET_CREDIT_CARDS_SUCCESS' | 'GET_CREDIT_CARDS_FAILED',
}): Generator<any, void, any> {
  if (
    (type === 'GET_CREDIT_CARDS_SUCCESS' && !payload.length) ||
    type === 'GET_CREDIT_CARDS_FAILED'
  ) {
    yield put(removeWidgetFromSource(5));
    return cancel();
  }

  yield put(initializeShortcutsForInstallmentsWidget());
}

export default function* watchers(): Generator<any, void, any> {
  yield takeLatest(
    ['GET_CREDIT_CARDS_SUCCESS', 'GET_CREDIT_CARDS_FAILED'],
    validateWidgetForCardInstallment,
  );
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', validateWidgetForShortcuts);
  yield all([
    fork(validateWidgetsForDepositAndPrepaid),
    fork(validateWidgetsForCredit),
    fork(validateWidgetsForYazzAndPrepaids),
    fork(validateWidgetForShortcuts),
  ]);
}
