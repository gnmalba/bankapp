/**
 * @flow
 */

import {fork} from 'redux-saga/effects';

import {watchGetCustomerInfoSuccess} from './configProcessor';

export default function* watchers(): Generator<any, any, any> {
  yield fork(watchGetCustomerInfoSuccess);
}
