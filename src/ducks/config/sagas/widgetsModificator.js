/**
 * @flow
 */

import {select, put, cancel, takeLatest, call} from 'redux-saga/effects';

import {
  updateCheckedWidgetsForCustomization,
  toggleRemoveLimit,
  updateCheckedWidgets,
} from '../actions';

import {
  widgetIsMovable,
  buildNewWidgetPosition,
  isValidConfigObject,
} from '../utils';
import type {REF, REFS, Direction} from '../types';

import {getCheckedWidgetsForCustomization} from '../selectors';

import {
  updateConfigTheme,
  getConfigLocalStorage,
  setUserConfigurationFromApi,
  setConfigLocalStorage,
} from './configProcessor';

import {getUsername, getCustomerID} from '@ducks/commonSelectors';

function* addWidget(action: {payload: {ref: REF}}): Generator<any, void, any> {
  // temporary fix, this should handle async storage
  const currentCheckedWidgets = yield select(getCheckedWidgetsForCustomization);

  yield put(
    updateCheckedWidgetsForCustomization([
      ...currentCheckedWidgets,
      action.payload.ref,
    ]),
  );
}

function* removeWidget(action: {
  payload: {ref: REF, removeImmediately: boolean},
}): Generator<any, void, any> {
  // temporary fix, this should handle async storage
  const currentCheckedWidgetsForCustomization = yield select(
    getCheckedWidgetsForCustomization,
  );

  if (currentCheckedWidgetsForCustomization.length === 1) {
    // if 1 widget left in customize widget
    yield put(toggleRemoveLimit(true));
    yield cancel();
  }

  const filteredCurrentCheckedWidgetsForCustomization = currentCheckedWidgetsForCustomization.filter(
    (ref) => ref !== action.payload.ref,
  );

  if (action.payload.removeImmediately) {
    yield put(
      updateCheckedWidgets(filteredCurrentCheckedWidgetsForCustomization),
    );
  } else {
    yield put(
      updateCheckedWidgetsForCustomization(
        filteredCurrentCheckedWidgetsForCustomization,
      ),
    );
  }
}

function* moveWidget(action: {
  payload: {ref: REF, direction: Direction},
}): Generator<any, void, any> {
  // temporary fix, this should handle async storage
  const currentCheckedWidgets: REFS = yield select(
    getCheckedWidgetsForCustomization,
  );

  const {ref, direction} = action.payload;

  const currentRefPosition = currentCheckedWidgets.indexOf(ref);

  if (widgetIsMovable(currentRefPosition, currentCheckedWidgets, direction)) {
    // widget is already on top or bottom do nothing for now
    yield cancel();
  }

  const newPositions = buildNewWidgetPosition(
    direction,
    currentRefPosition,
    currentCheckedWidgets,
    ref,
  );

  yield put(updateCheckedWidgetsForCustomization(newPositions));
}

function* updateWidgetPlacements({
  payload: {widgets},
}: {
  payload: {widgets: REFS},
}): Generator<any, void, any> {
  const localStorage = yield call(getConfigLocalStorage);

  if (
    !localStorage ||
    (localStorage && !isValidConfigObject(JSON.parse(localStorage)))
  ) {
    // if local storage is empty or not a valid config then terminate saga

    /**
     * TODO: dispatch action for UI
     */
    yield cancel();
  }

  if (
    JSON.stringify(JSON.parse(localStorage).widgets) === JSON.stringify(widgets)
  ) {
    // do nothing, there's no changes in positions
    yield cancel();
  }

  const username = yield select(getUsername);
  const customerId = yield select(getCustomerID);

  const newConfig = {
    ...JSON.parse(localStorage),
    widgets,
  };

  const configLocalStorageSuccess = yield call(
    setConfigLocalStorage,
    newConfig,
  );

  if (!configLocalStorageSuccess) {
    // we should cancel this process if failed from local storage
    yield cancel();
  }

  const apiSavingSuccess = yield call(
    setUserConfigurationFromApi,
    username,
    customerId,
    {
      ...newConfig,
    },
  );

  if (!apiSavingSuccess) {
    /**
     * TODO: dispatch action for UI
     */

    // do nothing since we already have the config in our reducer, we don't need to reset them
    yield cancel();
  }

  // If everything goes well, update Redux state
  yield put(updateCheckedWidgets(newConfig.widgets));
}

export default function* watchers(): Generator<any, void, any> {
  yield takeLatest('ADD_WIDGET', addWidget);
  yield takeLatest('REMOVE_WIDGET', removeWidget);
  yield takeLatest('MOVE_WIDGET', moveWidget);
  yield takeLatest('API_UPDATE_THEME', updateConfigTheme);
  yield takeLatest('API_UPDATE_WIDGETS_PLACEMENTS', updateWidgetPlacements);
}
