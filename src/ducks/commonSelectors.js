/**
 * @flow
 */
import {createSelector} from 'reselect';

import type {State} from './types';
import {
  depositFetchingStatusSelector,
  depositsSelector,
  getPrepaidFromDeposit,
} from '@ducks/deposit/selectors';
import {creditSelector} from '@ducks/credit/selectors';
import {yazzLoadingSelector, yazzSelector} from '@ducks/yazz/selectors';

type customerId = number;
type encryptionPublicKey = string;

export const getCustomerID = (state: State): customerId =>
  state.authentication.data.customerId;

export const getEncryptionPublicKey = (state: State): encryptionPublicKey =>
  state.authentication.encryptionPublicKey;

const aliasMapper = (row) => row.alias.toLowerCase();

export const getAliasList: Array<string> = createSelector(
  [depositsSelector, getPrepaidFromDeposit, creditSelector, yazzSelector],
  (deposits, prepaid, credit, yazz) => {
    const savingsDepositAlias = deposits.savingsAndDeposits.map(aliasMapper);

    const timeDepositAlias = deposits.timeDeposits.map(aliasMapper);

    const prepaidAlias = prepaid.map(aliasMapper);

    const creditAlias = credit.data.map(aliasMapper);

    const yazzAlias = yazz.map(aliasMapper);

    return [
      ...savingsDepositAlias,
      ...timeDepositAlias,
      ...prepaidAlias,
      ...creditAlias,
      ...yazzAlias,
    ];
  },
);

export const getAllAccounts: Array<any> = createSelector(
  [depositsSelector, getPrepaidFromDeposit, creditSelector, yazzSelector],
  (deposits, prepaid, credit, yazz) => {
    return {
      depositsAndPrepaid: [
        ...deposits.savingsAndDeposits,
        ...deposits.timeDeposits,
        ...prepaid,
      ],
      creditAndYazz: [...credit.data, ...yazz],
      all: [
        ...deposits.savingsAndDeposits,
        ...deposits.timeDeposits,
        ...prepaid,
        ...credit.data,
        ...yazz,
      ],
    };
  },
);

export const getUsername = (state: State): encryptionPublicKey =>
  state.authentication.username;

export const getAllLoadingAccounts: boolean = createSelector(
  [depositFetchingStatusSelector, creditSelector, yazzLoadingSelector],
  (depositLoading, creditList, yazzLoadding) => {
    return depositLoading && creditList.loading && yazzLoadding;
  },
);
