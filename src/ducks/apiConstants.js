// @flow

export const LOGIN_API = {
  TOKEN: 'login/token',
  AUTHENTICATION_PUBLIC_KEY: 'security/authentication-public-key/',
  ADVISORY: 'advisories/current/mbx',
  LOGOUT: 'login/logout',
  BIOMETRIC_REGISTRATION: 'customers/register-biometric',
  BIOMETRIC_UNREGISTRATION: 'customers/unregister-biometric',
  RECOVER_ACCESS: 'recover-access/mobile',
  REGISTER_MPIN: 'customers/register-mpin',
  CUSTOMER_INFO: 'login/customerinfo',
};
