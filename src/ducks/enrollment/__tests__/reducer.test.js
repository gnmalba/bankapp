/* eslint-disable sonarjs/no-identical-functions */
import reducer, {
  INITIAL_STATE,
  getIsInitialCall,
  getEnrollmentPublicKey,
} from '../reducer';
import configureStore from 'redux-mock-store';
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';

const mockStore = configureStore([]);
const mockData = {
  yazzCardBins: ['409461'],
  publicKey: 'samplePublicKey',
  mobileNumber: '*******1234',
  cardType: ACCOUNT_TYPES_ENUM.CREDIT,
  snackBar: {
    showSnackbar: true,
    snackbarMessage: 'Enrollment Successful!',
  },
};
const store = mockStore({
  enrollment: INITIAL_STATE,
});

describe('Unit test for Enrollment Reducer', () => {
  it('should return isInitialCall data from reducer when called', () => {
    expect(getIsInitialCall(store.getState())).toEqual(
      INITIAL_STATE.isInitialCall,
    );
  });

  it('should return publicKey data from reducer when called', () => {
    expect(getEnrollmentPublicKey(store.getState())).toEqual(
      INITIAL_STATE.publicKey,
    );
  });

  it('Should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it.each([
    'FETCH_YAZZ_CARD_BINS',
    'FETCH_ENROLLMENT_PUBLIC_KEY',
    'ENROLLMENT_DEPOSIT_REQUESTED',
    'ENROLLMENT_CREDIT_REQUESTED',
    'ENROLLMENT_YAZZ_REQUESTED',
  ])('Should update loading to true when called', (actionType) => {
    expect(reducer(INITIAL_STATE, {type: actionType})).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  it.each([
    [
      {type: 'FETCH_YAZZ_CARD_BINS_SUCCESS', payload: mockData.yazzCardBins},
      {
        ...INITIAL_STATE,
        yazzCardBins: mockData.yazzCardBins,
        loading: false,
      },
    ],
    [
      {type: 'FETCH_YAZZ_CARD_BINS_FAILED'},
      {
        ...INITIAL_STATE,
        yazzCardBins: mockData.yazzCardBins,
      },
    ],
  ])('Should update yazzCardBins when called', (action, expectedResult) => {
    expect(reducer(INITIAL_STATE, action)).toEqual(expectedResult);
  });

  it('Should update publicKey when called', () => {
    const action = {
      type: 'FETCH_ENROLLMENT_PUBLIC_KEY_SUCCESS',
      payload: mockData.publicKey,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      publicKey: mockData.publicKey,
      loading: false,
    });
  });

  it('Should update mobileNumber when called', () => {
    const action = {
      type: 'FETCH_MOBILE_NUMBER_SUCCESS',
      payload: mockData.mobileNumber,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      mobileNumber: mockData.mobileNumber,
    });
  });

  it('Should update specific reducer data when called', () => {
    const action = {
      type: 'UPDATE_ENROLLMENT_STATE',
      payload: {state: 'yazzCardBins', value: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      [action.payload.state]: action.payload.value,
    });
  });

  it.each([
    'FETCH_ENROLLMENT_PUBLIC_KEY_FAILED',
    'ENROLLMENT_DEPOSIT_FAILED',
    'ENROLLMENT_CREDIT_FAILED',
    'ENROLLMENT_YAZZ_FAILED',
  ])(
    'Should update loading to false and showProcessingScreen to false when called',
    (actionType) => {
      expect(reducer(INITIAL_STATE, {type: actionType})).toEqual({
        ...INITIAL_STATE,
        loading: false,
        showProcessingScreen: false,
      });
    },
  );

  it.each([
    'ENROLLMENT_DEPOSIT_SUCCESS',
    'ENROLLMENT_CREDIT_SUCCESS',
    'ENROLLMENT_YAZZ_SUCCESS',
  ])(
    'Should update loading to false, showProcessingScreen to true, and enrolledAccountFetching to true when called',
    (actionType) => {
      expect(reducer(INITIAL_STATE, {type: actionType})).toEqual({
        ...INITIAL_STATE,
        loading: false,
        showProcessingScreen: true,
        enrolledAccountFetching: true,
      });
    },
  );

  it.each([
    'ENROLLMENT_DEPOSIT_SUCCESS',
    'ENROLLMENT_CREDIT_SUCCESS',
    'ENROLLMENT_YAZZ_SUCCESS',
  ])(
    'Should update loading to false, showProcessingScreen to true, and enrolledAccountFetching to true when called',
    (actionType) => {
      expect(reducer(INITIAL_STATE, {type: actionType})).toEqual({
        ...INITIAL_STATE,
        loading: false,
        showProcessingScreen: true,
        enrolledAccountFetching: true,
      });
    },
  );

  it.each([
    'GET_DEPOSIT_API_SUCCESS',
    'GET_DEPOSIT_API_FAILURE',
    'GET_CREDIT_CARDS_SUCCESS',
    'GET_CREDIT_CARDS_FAILED',
    'GET_YAZZ_CREDIT_CARDS_API_SUCCESS',
    'GET_YAZZ_CREDIT_CARDS_API_FAILURE',
  ])(
    'Should update showProcessingScreen to false, and enrolledAccountFetching to false when called',
    (actionType) => {
      expect(reducer(INITIAL_STATE, {type: actionType})).toEqual({
        ...INITIAL_STATE,
        showProcessingScreen: false,
        enrolledAccountFetching: false,
      });
    },
  );

  it('Should update cardType when called', () => {
    const action = {
      type: 'SET_ENROLLMENT_CARD_TYPE',
      payload: mockData.cardType,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      cardType: mockData.cardType,
    });
  });

  it('Should update snackbar data when called', () => {
    const action = {
      type: 'SHOW_ENROLLMENT_SNACKBAR',
      payload: mockData.snackBar,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      showSnackbar: mockData.snackBar.showSnackbar,
      snackbarMessage: mockData.snackBar.snackbarMessage,
    });
  });

  it('Should update invalidOTPRetries count when called', () => {
    const action = {
      type: 'UPDATE_INVALID_OTP_RETRIES',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      invalidOTPRetries: INITIAL_STATE.invalidOTPRetries + 1,
    });
  });

  it('Should reset the following data when called', () => {
    const action = {
      type: 'PARTIAL_RESET_ENROLLMENT',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      loading: false,
      enrolledAccountFetching: false,
      showProcessingScreen: false,
      isInitialCall: true,
      formValues: {},
      cardType: null,
      mobileNumber: null,
      apiResponse: null,
      generatorId: null,
      invalidOTPRetries: 0,
    });
  });

  it('Should reset to default when called', () => {
    const action = {
      type: 'ENROLLMENT_LOGOUT_RESET_STATE',
    };
    expect(reducer(undefined, action)).toEqual(INITIAL_STATE);
  });
});
