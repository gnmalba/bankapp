import {
  fnEncryptData,
  formatBirthDate,
  extractGeneratorId,
  extractInvalidField,
} from '../helpers.js';

const samplePublicKey =
  'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe4eCZ0FPqri0cb2JZfXJ/DgYS' +
  'F6vUpwmJG8wVQZKjeGcjDOL5UlsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ51s1SPrCBkedbNf0Tp0GbMJDyR4e9T04ZZwIDAQAB';

describe('Unit test for Enroll Account Helpers', () => {
  it('Should return encrypted data', () => {
    expect(fnEncryptData('Test', samplePublicKey)).not.toBeNull();
  });
  it('Should return birth date to YYYY-MM-DD format', () => {
    expect(formatBirthDate('01/15/1995')).toEqual('1995-01-15');
  });

  it('Should return generatorId from the message', () => {
    expect(extractGeneratorId('Generator ID: 123456')).toEqual('123456');
  });

  it('Should return null if generator id response from the api is not returned', () => {
    expect(extractGeneratorId('Error encountered')).toEqual('');
  });

  it('Should return field name from the message', () => {
    expect(extractInvalidField('Mismatch data. nameOnCard')).toEqual(
      'nameOnCard',
    );
  });

  it('Should return null if mismatch data from the api is not returned', () => {
    expect(extractInvalidField('Error encountered')).toEqual('');
  });
});
