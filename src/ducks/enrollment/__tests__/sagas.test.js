import {select, call} from 'redux-saga/effects';
import {expectSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';

import {
  fnFetchYazzBins,
  fnFetchPublicKey,
  fnFetchMobileNumber,
  fnStartEnrollDeposit,
  fnStartEnrollCredit,
  fnStartEnrollYazz,
} from '../sagas.js';

import {
  fetchYazzBinsSuccess,
  fetchYazzBinsFailed,
  fetchEnrollmentPublicKeySuccess,
  fetchEnrollmentPublicKeyFailed,
  fetchMobileNumberSuccess,
  fetchMobileNumberFailed,
  enrollmentDepositFailed,
  enrollmentDepositSuccess,
  enrollmentCreditSuccess,
  enrollmentCreditFailed,
  enrollmentYazzSuccess,
  enrollmentYazzFailed,
} from '../actions.js';

import {getIsInitialCall, getEnrollmentPublicKey} from '../reducer';
import {getCustomerId} from '@ducks/authentication/reducer';

import EnrollmentConfig from '@api/services/enrollmentService';

import {checkDuplicateAlias} from '@ducks/commonGenerators.js';

const mockData = {
  yazzBins: ['409461'],
  publicKey:
    'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCqGKukO1De7zhZj6+H0qtjTkVxwTCpvKe4eCZ0FPqri0cb2JZfXJ/DgYSF6vUpwmJG8w' +
    'VQZKjeGcjDOL5UlsuusFncCzWBQ7RKNUSesmQRMSGkVb1/3j+skZ6UtW+5u09lHNsj6tQ51s1SPrCBkedbNf0Tp0GbMJDyR4e9T04ZZwIDAQAB',
  mobile: '09170001234',
  maskedMobile: '*******1234',
  customerId: '1234',
  uniqueAlias: 'Unique',
  notUniqueAlias: 'Not Unique',
  accountNo: '0133013756105',
  enrollDeposit: {
    accountNo: '0133013756105',
    alias: 'Deposit Account',
    otp: '123456',
    generatorId: 'sampleGeneratorId',
  },
  enrollCredit: {
    accountNo: 'encryptedAccountNo',
    alias: 'encryptedAlias',
    nameOnCard: 'encryptedNameOnCard',
    dateOfBirth: 'encryptedDateOfBirth',
    otp: '123456',
    generatorId: 'sampleGeneratorId',
  },
  enrollYazz: {
    accountNo: 'encryptedAccountNo',
    alias: 'encryptedAlias',
    otp: '123456',
    generatorId: 'sampleGeneratorId',
  },
};

describe('Unit test for Enroll Account Sagas', () => {
  it('Should call the fnFetchYazzBins successfully and run the expected effects', () => {
    const response = {
      data: {
        data: mockData.yazzBins,
      },
    };

    return expectSaga(fnFetchYazzBins, EnrollmentConfig.getYazzBins)
      .provide([[call(EnrollmentConfig.getYazzBins), response]])
      .put(fetchYazzBinsSuccess(mockData.yazzBins))
      .run();
  });

  it('Should call the fnFetchYazzBins and handles the error encountered', () => {
    return expectSaga(fnFetchYazzBins, EnrollmentConfig.getYazzBins)
      .provide({
        call() {
          throw new Error();
        },
      })
      .put(fetchYazzBinsFailed())
      .run();
  });

  it('Should call the fnFetchPublicKey successfully and run the expected effects', () => {
    const response = {
      data: {
        data: mockData.publicKey,
      },
    };

    return expectSaga(fnFetchPublicKey, EnrollmentConfig.getEnrollmentPublicKey)
      .provide([[call(EnrollmentConfig.getEnrollmentPublicKey), response]])
      .put(fetchEnrollmentPublicKeySuccess(mockData.publicKey))
      .run();
  });

  it('Should call the fnFetchPublicKey and handles the error encountered', () => {
    return expectSaga(fnFetchPublicKey, EnrollmentConfig.getEnrollmentPublicKey)
      .provide({
        call() {
          throw new Error();
        },
      })
      .put(fetchEnrollmentPublicKeyFailed())
      .run();
  });

  it('Should call the fnFetchMobileNumber successfully and run the expected effects', () => {
    const response = {
      data: {
        data: {mobile: mockData.mobile},
      },
    };

    return expectSaga(fnFetchMobileNumber, {payload: mockData.accountNo})
      .provide([
        [
          call(EnrollmentConfig.getAccountDetails, mockData.accountNo),
          response,
        ],
      ])
      .put(fetchMobileNumberSuccess(mockData.maskedMobile))
      .run();
  });

  it('Should call the fnFetchMobileNumber and handles the error encountered', () => {
    return expectSaga(
      fnFetchMobileNumber,
      {payload: mockData.accountNo},
      EnrollmentConfig.getAccountDetails,
    )
      .provide({
        call() {
          throw new Error();
        },
      })
      .put(fetchMobileNumberFailed())
      .run();
  });

  it('Should call the fnStartEnrollDeposit successfully and run the expected effects', () => {
    const response = {
      data: {
        data: {},
      },
    };

    return expectSaga(
      fnStartEnrollDeposit,
      {payload: mockData.enrollDeposit},
      EnrollmentConfig.enrollDepositCard,
    )
      .provide([
        [select(getCustomerId), mockData.customerId],
        [select(getIsInitialCall), true],
        [
          matchers.call.fn(checkDuplicateAlias, {
            payload: mockData.uniqueAlias,
          }),
          false,
        ],
        [
          matchers.call.fn(fnFetchMobileNumber, {payload: mockData.mobile}),
          mockData.maskedMobile,
        ],
        [matchers.call.fn(EnrollmentConfig.enrollDepositCard), response],
      ])
      .put(enrollmentDepositSuccess())
      .run();
  });

  it('Should call the fnStartEnrollDeposit and handles duplicate alias', () => {
    return expectSaga(
      fnStartEnrollDeposit,
      {payload: mockData.enrollDeposit},
      EnrollmentConfig.enrollDepositCard,
    )
      .provide([
        [select(getCustomerId), mockData.customerId],
        [select(getIsInitialCall), true],
        [
          matchers.call.fn(checkDuplicateAlias, {
            payload: mockData.notUniqueAlias,
          }),
          true,
        ],
      ])
      .put(enrollmentDepositFailed())
      .run();
  });

  it('Should call the fnStartEnrollDeposit and handles the error encountered', () => {
    return expectSaga(
      fnStartEnrollDeposit,
      {payload: mockData.enrollDeposit},
      EnrollmentConfig.enrollDepositCard,
    )
      .provide({
        call() {
          throw new Error();
        },
      })
      .put(enrollmentDepositFailed())
      .run();
  });

  it('Should call the fnStartEnrollCredit successfully and run the expected effects', () => {
    const response = {
      data: {
        data: {},
      },
    };

    return expectSaga(
      fnStartEnrollCredit,
      {payload: mockData.enrollCredit},
      EnrollmentConfig.enrollCreditCard,
    )
      .provide([
        [select(getCustomerId), mockData.customerId],
        [
          matchers.call.fn(checkDuplicateAlias, {
            payload: mockData.uniqueAlias,
          }),
          false,
        ],
        [select(getEnrollmentPublicKey), mockData.publicKey],
        [matchers.call.fn(EnrollmentConfig.enrollCreditCard), response],
      ])
      .put(enrollmentCreditSuccess())
      .run();
  });

  it('Should call the fnStartEnrollCredit and handles duplicate alias', () => {
    return expectSaga(
      fnStartEnrollCredit,
      {payload: mockData.enrollCredit},
      EnrollmentConfig.enrollCreditCard,
    )
      .provide([
        [select(getCustomerId), mockData.customerId],
        [
          matchers.call.fn(checkDuplicateAlias, {
            payload: mockData.notUniqueAlias,
          }),
          true,
        ],
      ])
      .put(enrollmentCreditFailed())
      .run();
  });

  it('Should call the fnStartEnrollCredit and handles the error encountered', () => {
    return expectSaga(
      fnStartEnrollCredit,
      {payload: mockData.enrollCredit},
      EnrollmentConfig.enrollCreditCard,
    )
      .provide({
        call() {
          throw new Error();
        },
      })
      .put(enrollmentCreditFailed())
      .run();
  });

  it('Should call the fnStartEnrollYazz successfully and run the expected effects', () => {
    const response = {
      data: {
        data: {},
      },
    };

    return expectSaga(
      fnStartEnrollYazz,
      {payload: mockData.enrollYazz},
      EnrollmentConfig.enrollCreditCard,
    )
      .provide([
        [select(getCustomerId), mockData.customerId],
        [
          matchers.call.fn(checkDuplicateAlias, {
            payload: mockData.uniqueAlias,
          }),
          false,
        ],
        [select(getEnrollmentPublicKey), mockData.publicKey],
        [matchers.call.fn(EnrollmentConfig.enrollCreditCard), response],
      ])
      .put(enrollmentYazzSuccess())
      .run();
  });

  it('Should call the fnStartEnrollYazz and handles duplicate alias', () => {
    return expectSaga(
      fnStartEnrollYazz,
      {payload: mockData.enrollYazz},
      EnrollmentConfig.enrollCreditCard,
    )
      .provide([
        [select(getCustomerId), mockData.customerId],
        [
          matchers.call.fn(checkDuplicateAlias, {
            payload: mockData.notUniqueAlias,
          }),
          true,
        ],
      ])
      .put(enrollmentYazzFailed())
      .run();
  });

  it('Should call the fnStartEnrollYazz and handles the error encountered', () => {
    return expectSaga(
      fnStartEnrollYazz,
      {payload: mockData.enrollYazz},
      EnrollmentConfig.enrollCreditCard,
    )
      .provide({
        call() {
          throw new Error();
        },
      })
      .put(enrollmentYazzFailed())
      .run();
  });
});
