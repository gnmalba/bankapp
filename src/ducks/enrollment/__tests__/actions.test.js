import {
  fetchYazzBins,
  fetchYazzBinsSuccess,
  fetchYazzBinsFailed,
  fetchEnrollmentPublicKey,
  fetchEnrollmentPublicKeySuccess,
  fetchEnrollmentPublicKeyFailed,
  fetchMobileNumber,
  fetchMobileNumberSuccess,
  fetchMobileNumberFailed,
  enrollmentDeposit,
  enrollmentDepositSuccess,
  enrollmentDepositFailed,
  enrollmentCredit,
  enrollmentCreditSuccess,
  enrollmentCreditFailed,
  enrollmentYazz,
  enrollmentYazzSuccess,
  enrollmentYazzFailed,
  updateEnrollmentState,
  partialResetEnrollment,
  showEnrollmentSnackbar,
  updateInvalidOtpRetries,
  setEnrollmentCardType,
} from '../actions';

import {ACCOUNT_TYPES_ENUM} from '@utils/constants';

describe('Unit test for Enroll Account Actions', () => {
  it('Should create an action to fetch the yazz bins', () => {
    const expectedAction = {
      type: 'FETCH_YAZZ_CARD_BINS',
    };

    expect(fetchYazzBins()).toEqual(expectedAction);
  });

  it('Should create an action to store the successfully fetched yazz bins', () => {
    const yazzBins = ['409461'];
    const expectedAction = {
      type: 'FETCH_YAZZ_CARD_BINS_SUCCESS',
      payload: yazzBins,
    };

    expect(fetchYazzBinsSuccess(yazzBins)).toEqual(expectedAction);
  });

  it('Should create an action to call if yazz bins is not successfully fetched', () => {
    const expectedAction = {
      type: 'FETCH_YAZZ_CARD_BINS_FAILED',
    };

    expect(fetchYazzBinsFailed()).toEqual(expectedAction);
  });

  it('Should create an action to fetch the enrollment public key for encryption', () => {
    const expectedAction = {
      type: 'FETCH_ENROLLMENT_PUBLIC_KEY',
    };

    expect(fetchEnrollmentPublicKey()).toEqual(expectedAction);
  });

  it('Should create an action to store the successfully fetched public key', () => {
    const publicKey = 'samplePublicKey';
    const expectedAction = {
      type: 'FETCH_ENROLLMENT_PUBLIC_KEY_SUCCESS',
      payload: publicKey,
    };

    expect(fetchEnrollmentPublicKeySuccess(publicKey)).toEqual(expectedAction);
  });

  it('Should create an action to call if public key is not successfully fetched', () => {
    const expectedAction = {
      type: 'FETCH_ENROLLMENT_PUBLIC_KEY_FAILED',
    };

    expect(fetchEnrollmentPublicKeyFailed()).toEqual(expectedAction);
  });

  it('Should create an action to fetch the user mobile number for casa (deposit/mb prepaid) enrollment', () => {
    const expectedAction = {
      type: 'FETCH_MOBILE_NUMBER',
    };

    expect(fetchMobileNumber()).toEqual(expectedAction);
  });

  it('Should create an action to store the successfully fetched and masked mobile number', () => {
    const mobileNumber = '*******1234';
    const expectedAction = {
      type: 'FETCH_MOBILE_NUMBER_SUCCESS',
      payload: mobileNumber,
    };

    expect(fetchMobileNumberSuccess(mobileNumber)).toEqual(expectedAction);
  });

  it('Should create an action to call if mobile number is not successfully fetched', () => {
    const expectedAction = {
      type: 'FETCH_MOBILE_NUMBER_FAILED',
    };

    expect(fetchMobileNumberFailed()).toEqual(expectedAction);
  });

  it('Should create an action to trigger enrollment of deposit', () => {
    const depositData = {
      accountNo: '0133013756105',
      alias: 'Deposit Account',
      otp: '123456',
      generatorId: 'sampleGeneratorId',
    };

    const expectedAction = {
      type: 'ENROLLMENT_DEPOSIT_REQUESTED',
      payload: depositData,
    };

    expect(enrollmentDeposit(depositData)).toEqual(expectedAction);
  });

  it('Should create an action to call after successful enrollment of deposit', () => {
    const expectedAction = {
      type: 'ENROLLMENT_DEPOSIT_SUCCESS',
    };

    expect(enrollmentDepositSuccess()).toEqual(expectedAction);
  });

  it('Should create an action to call after unsuccessful enrollment of deposit', () => {
    const expectedAction = {
      type: 'ENROLLMENT_DEPOSIT_FAILED',
    };

    expect(enrollmentDepositFailed()).toEqual(expectedAction);
  });

  it('Should create an action to trigger enrollment of credit', () => {
    const creditData = {
      accountNo: '5464981660114009',
      alias: 'Credit Account',
      otp: '123456',
      generatorId: 'sampleGeneratorId',
    };

    const expectedAction = {
      type: 'ENROLLMENT_CREDIT_REQUESTED',
      payload: creditData,
    };

    expect(enrollmentCredit(creditData)).toEqual(expectedAction);
  });

  it('Should create an action to call after successful enrollment of credit', () => {
    const expectedAction = {
      type: 'ENROLLMENT_CREDIT_SUCCESS',
    };

    expect(enrollmentCreditSuccess()).toEqual(expectedAction);
  });

  it('Should create an action to call after unsuccessful enrollment of credit', () => {
    const expectedAction = {
      type: 'ENROLLMENT_CREDIT_FAILED',
    };

    expect(enrollmentCreditFailed()).toEqual(expectedAction);
  });

  it('Should create an action to trigger enrollment of yazz', () => {
    const yazzData = {
      accountNo: '4094610083542022',
      alias: 'Yazz Account',
      otp: '123456',
      generatorId: 'sampleGeneratorId',
    };

    const expectedAction = {
      type: 'ENROLLMENT_YAZZ_REQUESTED',
      payload: yazzData,
    };

    expect(enrollmentYazz(yazzData)).toEqual(expectedAction);
  });

  it('Should create an action to call after successful enrollment of yazz', () => {
    const expectedAction = {
      type: 'ENROLLMENT_YAZZ_SUCCESS',
    };

    expect(enrollmentYazzSuccess()).toEqual(expectedAction);
  });

  it('Should create an action to call after unsuccessful enrollment of yazz', () => {
    const expectedAction = {
      type: 'ENROLLMENT_YAZZ_FAILED',
    };

    expect(enrollmentYazzFailed()).toEqual(expectedAction);
  });

  it('Should create an action to call to update data in the reducer', () => {
    const updatedData = {
      state: 'reducerData',
      value: 'testValue',
    };

    const expectedAction = {
      type: 'UPDATE_ENROLLMENT_STATE',
      payload: updatedData,
    };

    expect(updateEnrollmentState(updatedData)).toEqual(expectedAction);
  });

  it('Should create an action to call to partially reset enrollment reducer', () => {
    const expectedAction = {
      type: 'PARTIAL_RESET_ENROLLMENT',
    };

    expect(partialResetEnrollment()).toEqual(expectedAction);
  });

  it('Should create an action to call to show the snackbar after successful enrollment', () => {
    const snackbarData = {
      showSnackbar: true,
      snackbarMessage: 'Show this message',
    };

    const expectedAction = {
      type: 'SHOW_ENROLLMENT_SNACKBAR',
      payload: snackbarData,
    };

    expect(showEnrollmentSnackbar(snackbarData)).toEqual(expectedAction);
  });

  it('Should create an action to call to update the invalid otp retries count', () => {
    const expectedAction = {
      type: 'UPDATE_INVALID_OTP_RETRIES',
    };

    expect(updateInvalidOtpRetries()).toEqual(expectedAction);
  });

  it('Should create an action to call to update the card type of the account the user is trying to enroll', () => {
    const cardType = ACCOUNT_TYPES_ENUM.DEPOSIT;

    const expectedAction = {
      type: 'SET_ENROLLMENT_CARD_TYPE',
      payload: cardType,
    };

    expect(setEnrollmentCardType(cardType)).toEqual(expectedAction);
  });
});
