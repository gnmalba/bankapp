// /**
//  * @flow
//  */
import {takeLatest, call, put, select} from 'redux-saga/effects';
import {
  fetchYazzBinsSuccess,
  fetchYazzBinsFailed,
  fetchEnrollmentPublicKeySuccess,
  fetchEnrollmentPublicKeyFailed,
  fetchMobileNumberSuccess,
  fetchMobileNumberFailed,
  updateEnrollmentState,
  enrollmentDepositFailed,
  enrollmentDepositSuccess,
  enrollmentCreditSuccess,
  enrollmentCreditFailed,
  enrollmentYazzSuccess,
  enrollmentYazzFailed,
  updateInvalidOtpRetries,
} from '@ducks/enrollment/actions';
import type {
  EnrollDepositSagaType,
  EnrollCreditSagaType,
  EnrollYazzTypeSagaType,
} from '@ducks/enrollment/types';
import {
  getIsInitialCall,
  getEnrollmentPublicKey,
} from '@ducks/enrollment/reducer';
import {getCustomerId} from '@ducks/authentication/reducer';
import {
  fnEncryptData,
  formatBirthDate,
  extractGeneratorId,
  extractInvalidField,
} from '@ducks/enrollment/helpers';
import EnrollmentConfig from '@api/services/enrollmentService';
import {checkDuplicateAlias} from '@ducks/commonGenerators';
import {ERROR_CODES} from '@screens/private/EnrollAccount/constants';

export function* fnFetchYazzBins(): any {
  try {
    const {data} = yield call(EnrollmentConfig.getYazzBins);
    yield put(fetchYazzBinsSuccess(data.data));
  } catch (error) {
    yield put(fetchYazzBinsFailed());
    console.log('Fetch Yazz Bins Error', error);
  }
}

export function* fnFetchPublicKey(): any {
  try {
    const {data} = yield call(EnrollmentConfig.getEnrollmentPublicKey);
    yield put(fetchEnrollmentPublicKeySuccess(data.data));
  } catch (error) {
    yield put(fetchEnrollmentPublicKeyFailed());
    console.log('Fetch Enrollment Public Key Error', error);
  }
}

export function* fnFetchMobileNumber({
  payload: accountNo,
}: {
  payload: string,
}): any {
  try {
    const {data} = yield call(EnrollmentConfig.getAccountDetails, accountNo);
    let mobileNo = '';
    if (data.data.mobile) {
      const maskedMobile = (mobile) => {
        const lastFourDigit = mobile.substr(mobile.length - 4);
        return `*******${lastFourDigit}`;
      };

      mobileNo = maskedMobile(data.data.mobile);
    }
    yield put(fetchMobileNumberSuccess(mobileNo));
  } catch (error) {
    yield put(fetchMobileNumberFailed());
    console.log('Fetch Account Mobile Number Error', error);
  }
}

export function* fnStartEnrollDeposit({payload}: EnrollDepositSagaType): any {
  try {
    const {accountNo, alias, otp = '', generatorId = ''} = payload;
    const customerId = yield select(getCustomerId);
    const isInitialCall = yield select(getIsInitialCall);

    // check duplicate alias
    const isDuplicate = yield call(checkDuplicateAlias, {payload: alias});
    if (isDuplicate) {
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            errorCode: 'duplicate_alias',
          },
        }),
      );
      yield put(enrollmentDepositFailed());
    } else {
      if (isInitialCall) {
        // fetch mobile number
        yield call(fnFetchMobileNumber, {payload: accountNo});
        yield put(
          updateEnrollmentState({state: 'isInitialCall', value: false}),
        );
      }

      // call enroll deposit service
      const {data} = yield call(EnrollmentConfig.enrollDepositCard, {
        values: {
          accountNo,
          alias,
          customerId,
          otp,
          generatorId,
        },
      });
      yield put(enrollmentDepositSuccess());
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            status: data.code,
            data: data.data,
          },
        }),
      );
    }
  } catch (error) {
    console.log('Deposit Enrollment Error', error);
    const errorCode = error?.response?.data?.data?.errors[0].code;
    if (errorCode === ERROR_CODES.CASA.INVALID_OTP) {
      yield put(updateInvalidOtpRetries());
    }
    yield put(
      updateEnrollmentState({
        state: 'apiResponse',
        value: {
          status: error?.response?.data?.code,
          errorCode: errorCode || 'general_error',
        },
      }),
    );
    if (errorCode === ERROR_CODES.CASA.OTP_REQUIRED) {
      yield put(
        updateEnrollmentState({
          state: 'generatorId',
          value: error?.response?.data?.data?.errors[0].data.generatorId,
        }),
      );
    }
    yield put(enrollmentDepositFailed());
  }
}

export function* fnStartEnrollCredit({payload}: EnrollCreditSagaType): any {
  try {
    const {
      accountNo,
      alias,
      nameOnCard,
      dateOfBirth,
      otp = null,
      generatorId = null,
    } = payload;
    const customerId = yield select(getCustomerId);

    // check duplicate alias
    const isDuplicate = yield call(checkDuplicateAlias, {payload: alias});
    if (isDuplicate) {
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            errorCode: 'duplicate_alias',
          },
        }),
      );
      yield put(enrollmentCreditFailed());
    } else {
      // call enroll credit service with encrypted data
      const publicKey = yield select(getEnrollmentPublicKey);
      const values = {
        customerId,
        cardNo: fnEncryptData(accountNo, publicKey),
        alias: fnEncryptData(alias, publicKey),
        cardEmbossingName: fnEncryptData(nameOnCard, publicKey),
        birthDate: fnEncryptData(formatBirthDate(dateOfBirth), publicKey),
        otpCode: otp,
        generatorId,
      };

      const {data} = yield call(EnrollmentConfig.enrollCreditCard, {
        values: values,
      });
      yield put(enrollmentCreditSuccess());
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            status: data.code,
            data: data.data,
          },
        }),
      );
    }
  } catch (error) {
    console.log('Credit Enrollment Error', error);
    const errorCode = error?.response?.data?.data?.errors[0].code;
    const message = error?.response?.data?.data?.errors[0].message;
    if (
      errorCode === ERROR_CODES.CPC.GENERAL_ERROR &&
      message.includes(ERROR_CODES.CPC.INVALID_OTP_MESSAGE)
    ) {
      yield put(updateInvalidOtpRetries());
    }
    yield put(
      updateEnrollmentState({
        state: 'apiResponse',
        value: {
          errorCode: errorCode || 'general_error',
          message: message,
        },
      }),
    );
    if (errorCode === ERROR_CODES.CPC.OTP_REQUIRED) {
      yield put(
        updateEnrollmentState({
          state: 'generatorId',
          value: extractGeneratorId(message),
        }),
      );
    }
    if (errorCode === ERROR_CODES.CPC.MISMATCH_DATA) {
      const extracted = extractInvalidField(message);
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            errorCode: errorCode || 'general_error',
            message: message,
            mismatchedData:
              extracted === 'cardEmbossingName' ? 'nameOnCard' : extracted,
          },
        }),
      );
    }
    yield put(enrollmentCreditFailed());
  }
}

export function* fnStartEnrollYazz({payload}: EnrollYazzTypeSagaType): any {
  try {
    const {accountNo, alias, otp = null, generatorId = null} = payload;
    const customerId = yield select(getCustomerId);

    // check duplicate alias
    const isDuplicate = yield call(checkDuplicateAlias, {payload: alias});
    if (isDuplicate) {
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            errorCode: 'duplicate_alias',
          },
        }),
      );
      yield put(enrollmentYazzFailed());
    } else {
      // call enroll credit service  with encrypted data
      // set dummy data for nameOnCard and dateOfBirth because it still needed in the api
      const publicKey = yield select(getEnrollmentPublicKey);
      const dummyData = {
        nameOnCard: 'User 1',
        dateOfBirth: '01/01/1991',
      };
      const formattedDateOfBirth = yield formatBirthDate(dummyData.dateOfBirth);
      const values = {
        customerId,
        cardNo: fnEncryptData(accountNo, publicKey),
        alias: fnEncryptData(alias, publicKey),
        cardEmbossingName: fnEncryptData(dummyData.nameOnCard, publicKey),
        birthDate: fnEncryptData(formattedDateOfBirth, publicKey),
        otpCode: otp,
        generatorId,
      };

      const {data} = yield call(EnrollmentConfig.enrollCreditCard, {
        values: values,
      });
      yield put(enrollmentYazzSuccess());
      yield put(
        updateEnrollmentState({
          state: 'apiResponse',
          value: {
            status: data.code,
            data: data.data,
          },
        }),
      );
    }
  } catch (error) {
    console.log('Yazz Enrollment Error', error);
    const errorCode = error?.response?.data?.data?.errors[0].code;
    const message = error?.response?.data?.data?.errors[0].message;
    if (
      errorCode === ERROR_CODES.CPC.GENERAL_ERROR &&
      message.includes(ERROR_CODES.CPC.INVALID_OTP_MESSAGE)
    ) {
      yield put(updateInvalidOtpRetries());
    }
    yield put(
      updateEnrollmentState({
        state: 'apiResponse',
        value: {
          errorCode: errorCode || 'general_error',
          message: message,
        },
      }),
    );
    if (errorCode === ERROR_CODES.CPC.OTP_REQUIRED) {
      yield put(
        updateEnrollmentState({
          state: 'generatorId',
          value: extractGeneratorId(message),
        }),
      );
    }
    yield put(enrollmentYazzFailed());
  }
}

export default function* watcher(): any {
  yield takeLatest('FETCH_YAZZ_CARD_BINS', fnFetchYazzBins);
  yield takeLatest('FETCH_ENROLLMENT_PUBLIC_KEY', fnFetchPublicKey);
  yield takeLatest('FETCH_MOBILE_NUMBER', fnFetchMobileNumber);
  yield takeLatest('ENROLLMENT_DEPOSIT_REQUESTED', fnStartEnrollDeposit);
  yield takeLatest('ENROLLMENT_CREDIT_REQUESTED', fnStartEnrollCredit);
  yield takeLatest('ENROLLMENT_YAZZ_REQUESTED', fnStartEnrollYazz);
}
