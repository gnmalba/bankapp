// @flow

export type EnrollmentReducerType = {
  yazzCardBins?: ?(string[]),
  loading: boolean,
  enrolledAccountFetching: boolean,
  isInitialCall: boolean,
  formValues: Object,
  cardType: ?number,
  mobileNumber: ?number,
  apiResponse: any,
  publicKey?: ?string,
  generatorId: ?string,
  invalidOTPRetries: number,
};

export type EnrollDepositType = {
  +accountNo: string,
  +alias: string,
  otp?: ?number | ?string,
  generatorId?: ?string,
};

export type EnrollDepositSagaType = {
  payload: EnrollDepositType,
};

export type EnrollDepositServiceType = {
  values: {...EnrollDepositType, customerId: number},
};

export type EnrollCreditType = {
  +accountNo: string,
  +alias: string,
  +nameOnCard: string,
  +dateOfBirth: string,
  otp?: ?number | ?string,
  generatorId?: ?string,
};

export type EnrollCreditSagaType = {
  payload: EnrollCreditType,
};

export type EnrollCreditServiceType = {
  values: {...EnrollCreditType, customerId: number},
};

export type EnrollYazzType = {
  +accountNo: string,
  +alias: string,
  nameOnCard?: ?string,
  dateOfBirth?: ?string,
  otp?: ?number | ?string,
  generatorId?: ?string,
};

export type EnrollYazzTypeSagaType = {
  payload: EnrollYazzType,
};

export type EnrollYazzTypeServiceType = {
  values: {...EnrollYazzType, customerId: number},
};
