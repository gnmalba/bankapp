// @flow

import type {EnrollmentReducerType} from './types';
import {YAZZ_BINLIST} from '@utils/constants';

export const INITIAL_STATE: EnrollmentReducerType = {
  yazzCardBins: [],
  enrolledAccountFetching: false,
  loading: false,
  showProcessingScreen: false,
  isInitialCall: true,
  formValues: {},
  cardType: null,
  mobileNumber: null,
  apiResponse: null,
  publicKey: null,
  generatorId: null,
  showSnackbar: false,
  snackbarMessage: null,
  invalidOTPRetries: 0,
};

export const getIsInitialCall = (state: {
  enrollment: EnrollmentReducerType,
}): boolean => state.enrollment.isInitialCall;

export const getEnrollmentPublicKey = (state: {
  enrollment: EnrollmentReducerType,
}): ?string => state.enrollment.publicKey;

const enrollment = (
  state: EnrollmentReducerType = INITIAL_STATE,
  action: Object = {},
): EnrollmentReducerType => {
  switch (action.type) {
    case 'FETCH_YAZZ_CARD_BINS':
    case 'FETCH_ENROLLMENT_PUBLIC_KEY':
    case 'ENROLLMENT_DEPOSIT_REQUESTED':
    case 'ENROLLMENT_CREDIT_REQUESTED':
    case 'ENROLLMENT_YAZZ_REQUESTED':
      return {
        ...state,
        loading: true,
      };

    case 'FETCH_YAZZ_CARD_BINS_SUCCESS':
      return {
        ...state,
        yazzCardBins: action.payload,
        loading: false,
      };
    case 'FETCH_ENROLLMENT_PUBLIC_KEY_SUCCESS':
      return {
        ...state,
        publicKey: action.payload,
        loading: false,
      };
    case 'FETCH_MOBILE_NUMBER_SUCCESS':
      return {
        ...state,
        mobileNumber: action.payload,
      };
    case 'UPDATE_ENROLLMENT_STATE':
      return {
        ...state,
        [action.payload.state]: action.payload.value,
      };
    case 'FETCH_YAZZ_CARD_BINS_FAILED':
      return {
        ...state,
        yazzCardBins: YAZZ_BINLIST,
      };
    case 'FETCH_ENROLLMENT_PUBLIC_KEY_FAILED':
    case 'ENROLLMENT_DEPOSIT_FAILED':
    case 'ENROLLMENT_CREDIT_FAILED':
    case 'ENROLLMENT_YAZZ_FAILED':
      return {
        ...state,
        loading: false,
        showProcessingScreen: false,
      };
    case 'ENROLLMENT_DEPOSIT_SUCCESS':
    case 'ENROLLMENT_CREDIT_SUCCESS':
    case 'ENROLLMENT_YAZZ_SUCCESS':
      return {
        ...state,
        loading: false,
        showProcessingScreen: true,
        enrolledAccountFetching: true,
      };
    case 'GET_DEPOSIT_API_SUCCESS':
    case 'GET_DEPOSIT_API_FAILURE':
    case 'GET_CREDIT_CARDS_SUCCESS':
    case 'GET_CREDIT_CARDS_FAILED':
    case 'GET_YAZZ_CREDIT_CARDS_API_SUCCESS':
    case 'GET_YAZZ_CREDIT_CARDS_API_FAILURE':
      return {
        ...state,
        showProcessingScreen: false,
        enrolledAccountFetching: false,
      };
    case 'SET_ENROLLMENT_CARD_TYPE':
      return {
        ...state,
        cardType: action.payload,
      };
    case 'SHOW_ENROLLMENT_SNACKBAR':
      return {
        ...state,
        showSnackbar: action.payload.showSnackbar,
        snackbarMessage: action.payload.snackbarMessage,
      };
    case 'UPDATE_INVALID_OTP_RETRIES': {
      const invalidRetries = state.invalidOTPRetries + 1;
      return {
        ...state,
        invalidOTPRetries: invalidRetries,
      };
    }
    case 'PARTIAL_RESET_ENROLLMENT':
      return {
        ...state,
        loading: false,
        enrolledAccountFetching: false,
        showProcessingScreen: false,
        isInitialCall: true,
        formValues: {},
        cardType: null,
        mobileNumber: null,
        apiResponse: null,
        generatorId: null,
        invalidOTPRetries: 0,
      };
    case 'ENROLLMENT_LOGOUT_RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default enrollment;
