// @flow

import forge from 'node-forge';
import moment from 'moment';

export const fnEncryptData = (value: string, key: string): string => {
  const pk = forge.pki.publicKeyFromPem(
    `-----BEGIN PUBLIC KEY-----
      ${key}
      -----END PUBLIC KEY-----`,
  );
  const encrypted = pk.encrypt(value, 'RSA-OAEP');
  return forge.util.encode64(encrypted);
};

export const formatBirthDate = (dateOfBirth: string): string => {
  const date = moment(dateOfBirth, 'MM/DD/YYYY');
  return date.format('YYYY-MM-DD');
};

export const extractGeneratorId = (value: string): string => {
  const regex = new RegExp(/ID:(.*)/, 'i');
  const generatorId = regex.exec(value);
  return generatorId ? generatorId[1].trim() : '';
};

export const extractInvalidField = (value: string): string => {
  const regex = new RegExp(/Mismatch data.(.*)/, 'i');
  const message = regex.exec(value);
  const invalidFieldMessage = message && message[1].trim();
  const invalidField = invalidFieldMessage?.split(':');
  return invalidField ? invalidField[0] : '';
};
