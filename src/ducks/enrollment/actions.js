/**
 * @flow
 */

import type {
  EnrollDepositType,
  EnrollCreditType,
  EnrollYazzType,
} from '@ducks/enrollment/types';

export const fetchYazzBins = (): any => ({
  type: 'FETCH_YAZZ_CARD_BINS',
});
export const fetchYazzBinsSuccess = (data: number[]): any => ({
  type: 'FETCH_YAZZ_CARD_BINS_SUCCESS',
  payload: data,
});
export const fetchYazzBinsFailed = (): any => ({
  type: 'FETCH_YAZZ_CARD_BINS_FAILED',
});

export const fetchEnrollmentPublicKey = (): any => ({
  type: 'FETCH_ENROLLMENT_PUBLIC_KEY',
});
export const fetchEnrollmentPublicKeySuccess = (data: string): any => ({
  type: 'FETCH_ENROLLMENT_PUBLIC_KEY_SUCCESS',
  payload: data,
});
export const fetchEnrollmentPublicKeyFailed = (): any => ({
  type: 'FETCH_ENROLLMENT_PUBLIC_KEY_FAILED',
});

export const fetchMobileNumber = (data: string): any => ({
  type: 'FETCH_MOBILE_NUMBER',
  payload: data,
});
export const fetchMobileNumberSuccess = (data: string): any => ({
  type: 'FETCH_MOBILE_NUMBER_SUCCESS',
  payload: data,
});
export const fetchMobileNumberFailed = (): any => ({
  type: 'FETCH_MOBILE_NUMBER_FAILED',
});

export const enrollmentDeposit = (data: EnrollDepositType): any => ({
  type: 'ENROLLMENT_DEPOSIT_REQUESTED',
  payload: data,
});
export const enrollmentDepositSuccess = (): any => ({
  type: 'ENROLLMENT_DEPOSIT_SUCCESS',
});
export const enrollmentDepositFailed = (): any => ({
  type: 'ENROLLMENT_DEPOSIT_FAILED',
});

export const enrollmentCredit = (data: EnrollCreditType): any => ({
  type: 'ENROLLMENT_CREDIT_REQUESTED',
  payload: data,
});
export const enrollmentCreditSuccess = (): any => ({
  type: 'ENROLLMENT_CREDIT_SUCCESS',
});
export const enrollmentCreditFailed = (): any => ({
  type: 'ENROLLMENT_CREDIT_FAILED',
});

export const enrollmentYazz = (data: EnrollYazzType): any => ({
  type: 'ENROLLMENT_YAZZ_REQUESTED',
  payload: data,
});
export const enrollmentYazzSuccess = (): any => ({
  type: 'ENROLLMENT_YAZZ_SUCCESS',
});
export const enrollmentYazzFailed = (): any => ({
  type: 'ENROLLMENT_YAZZ_FAILED',
});

export const updateEnrollmentState = (data: any): any => ({
  type: 'UPDATE_ENROLLMENT_STATE',
  payload: data,
});

export const partialResetEnrollment = (): any => ({
  type: 'PARTIAL_RESET_ENROLLMENT',
});

export const showEnrollmentSnackbar = (data: {
  showSnackbar: boolean,
  snackbarMessage: ?string,
}): any => ({
  type: 'SHOW_ENROLLMENT_SNACKBAR',
  payload: {
    showSnackbar: data.showSnackbar,
    snackbarMessage: data.snackbarMessage,
  },
});

export const updateInvalidOtpRetries = (): any => ({
  type: 'UPDATE_INVALID_OTP_RETRIES',
});

export const setEnrollmentCardType = (data: number): any => ({
  type: 'SET_ENROLLMENT_CARD_TYPE',
  payload: data,
});
