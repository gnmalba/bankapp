/**
 * @flow
 */

import type {SchedulesState} from './types';

const initialState = {
  scheduledSendMoneyList: [],
  scheduledPayBillList: [],
  isLoading: false,
  isSubmitting: false,
  isError: false,
  errorMessage: null,
  apiResponse: {status: null, error: null, loading: false, data: null},
};

const schedules = (
  state: any = initialState,
  action: Object = {},
): SchedulesState => {
  switch (action.type) {
    case 'GET_SCHEDULED_PAY_BILL_REQUEST': {
      const {isLoadingPayBill} = action.payload;
      return {
        ...state,
        isLoading: isLoadingPayBill,
        isError: false,
        errorMessage: null,
      };
    }
    case 'GET_SCHEDULED_PAY_BILL_SUCCESS': {
      const {scheduledPayBillList} = action.payload;
      return {
        ...state,
        scheduledPayBillList: scheduledPayBillList,
        isLoading: false,
        isError: false,
        errorMessage: null,
      };
    }
    case 'GET_SCHEDULED_PAY_BILL_FAILED': {
      const {error, errorMessage} = action.payload;
      return {
        ...state,
        isLoading: false,
        isError: error,
        errorMessage: errorMessage,
      };
    }
    case 'GET_SCHEDULED_SEND_MONEY_REQUEST': {
      const {isLoadingSendMoney} = action.payload;

      return {
        ...state,
        isLoading: isLoadingSendMoney,
        isError: false,
        errorMessage: null,
      };
    }
    case 'GET_SCHEDULED_SEND_MONEY_SUCCESS': {
      const {scheduledSendMoneyList} = action.payload;
      return {
        ...state,
        scheduledSendMoneyList: scheduledSendMoneyList,
        isLoading: false,
        isError: false,
        errorMessage: null,
      };
    }
    case 'GET_SCHEDULED_SEND_MONEY_FAILED': {
      const {isLoadingSendMoney, error, errorMessage} = action.payload;
      return {
        ...state,
        isLoading: isLoadingSendMoney,
        isError: error,
        errorMessage: errorMessage,
      };
    }
    case 'UPDATE_SCHEDULE_REQUEST_STATE':
      return {
        ...state,
        [action.payload.state]: action.payload.value,
      };
    case 'CLEAR_API_RESPONSE':
      return {
        ...state,
        apiResponse: {status: null, error: null, loading: false, data: null},
      };
    case 'CANCEL_SCHEDULE_REQUESTED':
    default:
      return state;
  }
};

export default schedules;
