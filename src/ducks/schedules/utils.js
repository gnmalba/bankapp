/**
 * @flow
 */

/* REFERENCE DATA FROM API
 amount: "
 currency: ""
 customerId: "5678"
 endDate: "2021-03-25T12:00"
 frequency: ""
 jobStatus: "PENDING"
 jobTransactionDate: ""
 jobTransactions: [] (0)
 occurrences: "1"
 recipient: null
 referenceNo: "1616551452776"
 serviceFee: ""
 source: {firstName: "Elly Mae", lastName: "Flores", middleName: null, cardNo: ""}
 startDate: "2021-03-25T12:00"
*/

import {formatDate} from '@utils/date';

import type {
  SchedulePayBillFromApi,
  ScheduledPayBillList,
  ScheduleSendMoneyFromApi,
  ScheduledSendMoneyList,
} from './types';

export const sortByDateDescending = (
  data: Array<Object>,
  propertyName: string,
): Array<Object> => {
  return data.sort((a, b) => b[propertyName] - a[propertyName]);
};

const generatePayBillOverview = (scheduleData) => {
  const {
    currency,
    billerName,
    billerAccountNo,
    startDate,
    endDate,
    occurrences,
    frequency,
    notes,
    scheduleReferenceNo,
    source,
    serviceFee,
  } = scheduleData;
  return [
    {
      label: 'From',
      value: `${source?.firstName || source?.lastName || ''}`,
      extraValue: `${source?.cardNo || ''}`,
      showDivider: true,
    },
    {
      label: 'Fee',
      value: currency && serviceFee ? `${currency} ${serviceFee}` : '',
      showDivider: true,
    },
    {label: 'Biller', value: `${billerName}`},
    {label: 'Account No', value: `${billerAccountNo}`},
    {
      label: 'Subscriber name',
      value: `${source?.firstName || source?.lastName || ''}`,
    },
    {label: 'Start date', value: startDate},
    {label: 'repeats every', value: frequency},
    {
      label: 'ends after',
      value: occurrences ? `${occurrences} occurrences` : '',
      extraValue: endDate ? `${formatDate(endDate, 'MM/DD/YYYY')}` : '',
      showDivider: true,
    },
    {label: 'notes', value: notes, showDivider: true},
    {label: 'reference no.', value: scheduleReferenceNo},
  ];
};

const getRecipientInfo = (recipient) => {
  if (recipient) {
    return [
      {
        label: 'Receiver',
        value: `${recipient.firstName} ${recipient.lastName}`,
      },
      {label: 'Account No', value: `${recipient.accountNo}`},
    ];
  }
  return [];
};

const generateSendMoneyOverview = (scheduleData) => {
  const {
    amount,
    currency,
    serviceFee,
    occurrences,
    frequency,
    startDate,
    endDate,
    source,
    recipient,
    notes,
    jobTransactionDate,
    referenceNo,
  } = scheduleData;
  return [
    {
      label: 'From',
      value: `${source?.firstName || source?.lastName || ''}`,
      extraValue: `${source?.cardNo || ''}`,
      showDivider: true,
    },
    {label: 'Amount', value: currency && amount ? `${currency} ${amount}` : ''},
    {
      label: 'Fee',
      value: currency && serviceFee ? `${currency} ${serviceFee}` : '',
      showDivider: true,
    },
    ...getRecipientInfo(recipient),
    {
      label: 'Subscriber name',
      value: `${source?.firstName || source?.lastName || ''}`,
    },
    {label: 'Start date', value: startDate},
    {label: 'repeats every', value: frequency},
    {
      label: 'ends after',
      value: occurrences ? `${occurrences} occurrences` : '',
      extraValue: endDate ? `${formatDate(endDate, 'MM/DD/YYYY')}` : '',
      showDivider: true,
    },
    {label: 'notes', value: notes || '', showDivider: true},
    {label: 'reference no.', value: referenceNo},
    {label: 'date and time', value: jobTransactionDate},
  ];
};

const getDisplayTransactionAmount = (schedule) => {
  const {amount, currency} = schedule;
  if (amount && currency) {
    return `${currency} ${amount}`;
  }
  return '';
};

const getDisplayBillerName = (schedule) => {
  if (schedule.billerName) {
    return `${schedule.billerName}`;
  }
  return '';
};

const getDisplayRecipientName = (schedule) => {
  if (schedule.recipient) {
    const {firstName, lastName} = schedule.recipient;
    return `${firstName} ${lastName}`;
  }
  return '';
};

const getDisplayLabelForDate = (status) => {
  if (status) {
    if (status === 'pending' || status === 'in progress') {
      return 'NEXT RUN DATE';
    }
    if (status === 'completed' || status === 'failed') {
      return 'PAYMENT DATE';
    }
    if (status === 'canceled') {
      return 'DATE CANCELLED';
    }
  }
  return '';
};

const getDisplayDate = (transactionDate) => {
  if (transactionDate) {
    return formatDate(transactionDate);
  }
  return '';
};

export const formatScheduledSendMoneyList = (
  schedules: Array<ScheduleSendMoneyFromApi>,
): ScheduledSendMoneyList => {
  return schedules.map((scheduleFromApi) => {
    const jobStatus = scheduleFromApi.jobStatus.toLowerCase();
    const jobTransactions = scheduleFromApi.jobTransactions || [];
    const schedule = {...scheduleFromApi, jobStatus, jobTransactions};
    return {
      title: getDisplayRecipientName(schedule),
      subtitle: getDisplayTransactionAmount(schedule),
      label: getDisplayLabelForDate(schedule.jobStatus),
      value: getDisplayDate(schedule.jobTransactionDate),
      status: jobStatus,
      overview: generateSendMoneyOverview(schedule),
      data: schedule,
    };
  });
};

export const formatScheduledPayBillList = (
  schedules: Array<SchedulePayBillFromApi>,
): ScheduledPayBillList => {
  return schedules.map((scheduleFromApi) => {
    const jobStatus = scheduleFromApi?.jobStatus?.toLowerCase();
    const transactions = scheduleFromApi.transactions || [];
    const schedule = {...scheduleFromApi, jobStatus, transactions};
    return {
      title: getDisplayBillerName(schedule),
      subtitle: getDisplayTransactionAmount(schedule),
      label: getDisplayLabelForDate(schedule.jobStatus),
      value: getDisplayDate(schedule.jobTransactionDate),
      status: jobStatus,
      overview: generatePayBillOverview(schedule),
      data: schedule,
    };
  });
};
