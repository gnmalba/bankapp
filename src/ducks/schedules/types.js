/**
 * @flow
 */

// SCHEDULES

export type ScheduledPayBillFromApi = Object; //update this @ian

export type ScheduleOverviewItem = {
  label: string,
  value: ?string | ?number,
  extraValue?: ?string | ?number,
  showDivider?: boolean,
};

export type ScheduledPayBillItem = {
  title: string,
  subtitle: string,
  label: string,
  value: string,
  data: ScheduledPayBillFromApi,
  overview: Array<ScheduleOverviewItem>,
};

export type ScheduledPayBillList = Array<ScheduledPayBillItem>;

export type ScheduleSendMoneyFromApi = {
  referenceNo: string,
  customerId: string,
  amount: ?number,
  currency: ?string,
  serviceFee: ?number,
  occurrences: string | number,
  frequency: number | string,
  startDate: string,
  endDate: string,
  jobStatus: string,
  jobTransactionDate: ?string,
  source: ?{
    firstName: string,
    lastName: string,
    middleName: ?string,
    cardNo: string,
  },
  recipient: Object,
  notes: ?string,
  jobTransactions: Array<any>,
};

export type SchedulePayBillFromApi = {
  id: string,
  scheduleReferenceNo: string,
  customerId: string,
  billerName: string,
  billerAccountNo: string,
  amount: ?number,
  currency: ?string,
  serviceFee: number,
  occurrences: number,
  frequency: number,
  startDate: string,
  endDate: string,
  jobStatus?: string,
  jobTransactionDate: ?string,
  firstName: string,
  lastName: string,
  middleName: string,
  notes: string,
  source?: ?{
    firstName: string,
    lastName: string,
    middleName: ?string,
    cardNo: string,
  },
  recipient: null,
  transactions: Array<any>,
};

export type ScheduledSendMoneyItem = {
  title: string,
  subtitle: string,
  label: string,
  value: string,
  data: ScheduleSendMoneyFromApi,
  overview: Array<ScheduleOverviewItem>,
};

export type ScheduledSendMoneyList = Array<ScheduledSendMoneyItem>;

export type ScheduleFilterParamsProps = {
  pageNumber: ?number,
  pageSize: ?number,
  status: ?string,
};

export type CancelScheduleFields = {
  referenceNo: string,
  mpin: string,
};
export type CancelScheduleParams = {
  payload: CancelScheduleFields,
};

// MAIN (ACTIONS & STATE)

export type SchedulesAction =
  | {
      type: 'GET_SCHEDULED_PAY_BILL_REQUEST',
      +payload: {
        params: ScheduleFilterParamsProps,
        isLoadingPayBill: boolean,
      },
    }
  | {
      type: 'GET_SCHEDULED_PAY_BILL_SUCCESS',
      +payload: {
        scheduledPayBillList: ScheduledPayBillList,
      },
    }
  | {
      type: 'GET_SCHEDULED_PAY_BILL_FAILED',
      +payload: {
        isError: boolean,
      },
    }
  | {
      type: 'GET_SCHEDULED_SEND_MONEY_REQUEST',
      +payload: {
        params: ScheduleFilterParamsProps,
        isLoadingSendMoney: boolean,
      },
    }
  | {
      type: 'GET_SCHEDULED_SEND_MONEY_SUCCESS',
      +payload: {
        scheduledSendMoneyList: ScheduledSendMoneyList,
      },
    }
  | {
      type: 'GET_SCHEDULED_SEND_MONEY_FAILED',
      +payload: {
        isError: boolean,
      },
    }
  | {
      type: 'RESET_SCHEDULES_DATA',
    };

export type ScheduleFetchParamsProps = {
  customerId: string,
  pageNumber: string | number,
  pageSize: string | number,
  status: string,
};

export type SchedulesState = {
  scheduledPayBillList: ScheduledPayBillList,
  scheduledSendMoneyList: ScheduledSendMoneyList,
  isLoading: boolean,
  isSubmitting: boolean,
  isError: boolean,
};
