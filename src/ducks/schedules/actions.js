/**
 * @flow
 */

import type {
  SchedulesAction,
  ScheduledPayBillList,
  ScheduledSendMoneyList,
  CancelScheduleFields,
  ScheduleFilterParamsProps,
} from './types';

export const getScheduledPayBillRequest = (
  params: ScheduleFilterParamsProps,
): SchedulesAction => ({
  type: 'GET_SCHEDULED_PAY_BILL_REQUEST',
  payload: {isLoadingPayBill: true, params},
});
export const getScheduledPayBillSuccess = (
  data: ScheduledPayBillList,
): SchedulesAction => ({
  type: 'GET_SCHEDULED_PAY_BILL_SUCCESS',
  payload: {scheduledPayBillList: data},
});
export const getScheduledPayBillFailed = (error: Object): SchedulesAction => ({
  type: 'GET_SCHEDULED_PAY_BILL_FAILED',
  payload: {isError: error},
});

export const getScheduledSendMoneyRequest = (
  params: ScheduleFilterParamsProps,
): SchedulesAction => ({
  type: 'GET_SCHEDULED_SEND_MONEY_REQUEST',
  payload: {
    isLoadingSendMoney: true,
    params,
  },
});
export const getScheduledSendMoneySuccess = (
  data: ScheduledSendMoneyList,
): SchedulesAction => ({
  type: 'GET_SCHEDULED_SEND_MONEY_SUCCESS',
  payload: {scheduledSendMoneyList: data},
});
export const getScheduledSendMoneyFailed = (
  error: Object,
): SchedulesAction => ({
  type: 'GET_SCHEDULED_SEND_MONEY_FAILED',
  payload: {isError: error},
});

export const updatePostRequestState = (data: any): any => ({
  type: 'UPDATE_SCHEDULE_REQUEST_STATE',
  payload: data,
});

export const clearApiResponse = (): any => ({
  type: 'CLEAR_API_RESPONSE',
});

export const deleteScheduleRequest = (data: CancelScheduleFields): any => ({
  type: 'CANCEL_SCHEDULE_REQUESTED',
  payload: data,
});
