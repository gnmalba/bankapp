/**
 * @flow
 */

import {call, put, takeLatest, select} from 'redux-saga/effects';

import {getCustomerID} from '@ducks/commonSelectors';
import api from '@api/services/schedulesService';

import {
  getScheduledPayBillFailed,
  getScheduledPayBillSuccess,
  getScheduledSendMoneyFailed,
  getScheduledSendMoneySuccess,
  updatePostRequestState,
} from './actions';

import {
  formatScheduledSendMoneyList,
  formatScheduledPayBillList,
} from './utils';

import type {CancelScheduleParams} from './types';

import {getCustomerId} from '@ducks/authentication/reducer';

import {getEncryptionPublicKey} from '@ducks/commonSelectors';
import encrypt from '@utils/encryptString';

import config from '@api/services/schedulesService';

function* fetchScheduledPayBill({payload}): Generator<any, any, any> {
  const customerId = yield select(getCustomerID);
  const prevList = yield select(
    (state) => state.schedules.scheduledSendMoneyList,
  );

  try {
    const response = yield call(api.getScheduledPayBill, customerId);

    const formattedResponse = formatScheduledPayBillList(response.data.data);

    if (payload.pageNumber === 0) {
      yield put(getScheduledPayBillSuccess(formattedResponse));
    }

    yield put(getScheduledPayBillSuccess([...prevList, ...formattedResponse]));
  } catch (error) {
    yield put(getScheduledPayBillFailed(error));
  }
}

function* fetchScheduledSendMoney({payload}): Generator<any, any, any> {
  const customerId = yield select(getCustomerID);
  const prevList = yield select(
    (state) => state.schedules.scheduledSendMoneyList,
  );

  try {
    const response = yield call(api.getScheduledSendMoney, {
      customerId,
      ...payload,
    });

    const formattedResponse = formatScheduledSendMoneyList(
      response.data.data.content,
    );

    if (payload.pageNumber === 0) {
      yield put(getScheduledSendMoneySuccess(formattedResponse));
    }

    yield put(
      getScheduledSendMoneySuccess([...prevList, ...formattedResponse]),
    );
  } catch (error) {
    yield put(getScheduledSendMoneyFailed(error));
  }
}

function* fnCancelSchedule({payload}: CancelScheduleParams): any {
  try {
    const {mpin} = payload;

    const customerId = yield select(getCustomerId);
    const encryptionPublicKey = yield select(getEncryptionPublicKey);

    const response = yield call(config.cancelSchedule, {
      customerId,
      mpin: encrypt(encryptionPublicKey).encrypt(mpin),
    });

    yield put(
      updatePostRequestState({
        state: 'apiResponse',
        value: {
          status: response.code,
          data: response.data,
        },
      }),
    );
  } catch (error) {
    const errorMessage = error?.response?.data?.data?.errors?.[0]?.message;
    yield put(
      updatePostRequestState({
        state: 'apiResponse',
        value: {
          status: error.response.data.code,
          data: null,
          loading: false,
          error: errorMessage,
        },
      }),
    );
  }
}

export default function* watcher(): Generator<any, any, any> {
  yield takeLatest('GET_SCHEDULED_PAY_BILL', fetchScheduledPayBill);
  yield takeLatest('GET_SCHEDULED_SEND_MONEY_REQUEST', fetchScheduledSendMoney);
  yield takeLatest('CANCEL_SCHEDULE_REQUESTED', fnCancelSchedule);
}
