/**
 * @flow
 */

import type {
  AccountsDisplayOptionsState,
  SourceAccountType,
  ID,
  IDS,
  DisplayOptionsCurrency,
} from './types';

const validateAccountType = (accountType: string): string => {
  if (accountType === 'credit' || accountType === 'deposit') {
    return accountType;
  } else {
    return 'prepaid';
  }
};

export const validateCurrency = (currency: string): string =>
  currency === 'peso' ? 'peso' : 'foreign';

export const createAccountModification = (
  state: AccountsDisplayOptionsState,
  payload: {
    type: SourceAccountType,
    id: ID,
  },
  currency?: string = 'peso',
): AccountsDisplayOptionsState => {
  const selectedAccountType = validateAccountType(payload.type);
  const selectedCurrencty = validateCurrency(currency);

  return {
    ...state,
    [selectedAccountType]: {
      ...state[selectedAccountType],
      [selectedCurrencty]: [
        ...state[selectedAccountType][selectedCurrencty],
        payload.id,
      ],
    },
  };
};

export const getSelected = (
  peso: IDS,
  foreign: IDS,
): DisplayOptionsCurrency => {
  if (peso.length !== 0 && foreign.length !== 0) {
    return 'all';
  }

  if (peso.length !== 0) {
    return 'peso';
  } else {
    return 'foreign';
  }
};

export const createSelectedCurrencyModification = (
  state: AccountsDisplayOptionsState,
  type: SourceAccountType,
  selected: DisplayOptionsCurrency,
): AccountsDisplayOptionsState => {
  const selectedAccountType = validateAccountType(type);
  return {
    ...state,
    [selectedAccountType]: {
      ...state[selectedAccountType],
      selected,
    },
  };
};
