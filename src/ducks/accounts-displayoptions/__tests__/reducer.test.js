import reducer from '../reducer';
import {
  addPesoAccountByType,
  addForeignAccountByType,
  validateSelectedCurrencyByAccountType,
  setSelectedOptionByAccount,
} from '../actions';

const INITIAL_STATE = {
  deposit: {
    peso: [],
    foreign: [],
    selected: 'all',
  },
  credit: {
    peso: [],
    foreign: [],
    selected: 'all',
  },
  prepaid: {
    peso: [],
    foreign: [],
    selected: 'all',
  },
};

const testID = 78926;

describe('Unit test for Accounts Display Options Reducer', () => {
  it('Should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it.each([
    [
      addPesoAccountByType(testID, 'deposit'),
      {
        ...INITIAL_STATE,
        deposit: {
          ...INITIAL_STATE.deposit,
          peso: [testID],
        },
      },
    ],
    [
      addPesoAccountByType(testID, 'credit'),
      {
        ...INITIAL_STATE,
        credit: {
          ...INITIAL_STATE.credit,
          peso: [testID],
        },
      },
    ],
    [
      addPesoAccountByType(testID, 'prepaid'),
      {
        ...INITIAL_STATE,
        prepaid: {
          ...INITIAL_STATE.prepaid,
          peso: [testID],
        },
      },
    ],
    [
      addForeignAccountByType(testID, 'deposit'),
      {
        ...INITIAL_STATE,
        deposit: {
          ...INITIAL_STATE.deposit,
          foreign: [testID],
        },
      },
    ],
    [
      addForeignAccountByType(testID, 'credit'),
      {
        ...INITIAL_STATE,
        credit: {
          ...INITIAL_STATE.credit,
          foreign: [testID],
        },
      },
    ],
    [
      addForeignAccountByType(testID, 'prepaid'),
      {
        ...INITIAL_STATE,
        prepaid: {
          ...INITIAL_STATE.prepaid,
          foreign: [testID],
        },
      },
    ],
  ])(
    `Using action %o should work as expected or match with filled testID ${testID}`,
    (action, expected) => {
      expect(reducer(undefined, action)).toEqual(expected);
    },
  );

  it('Should add both foreign and peso in deposit account', () => {
    const initialState = {...INITIAL_STATE};

    const currentState = reducer(
      initialState,
      addPesoAccountByType(testID, 'deposit'),
    );

    const testId2 = 23212;
    const latestStateAfterAddPesoAccount = reducer(
      currentState,
      addPesoAccountByType(testId2, 'deposit'),
    );

    expect(
      reducer(
        latestStateAfterAddPesoAccount,
        addForeignAccountByType(testID, 'deposit'),
      ),
    ).toEqual({
      ...latestStateAfterAddPesoAccount,
      deposit: {
        ...latestStateAfterAddPesoAccount.deposit,
        foreign: [testID],
        peso: [testID, testId2],
      },
    });
  });

  it('Should validate selected currency by account and select peso as default', () => {
    const selectedSourceAccount = 'deposit';
    const defaultPesoAccounts = [1, 2, 3];
    expect(
      reducer(
        {
          ...INITIAL_STATE,
          deposit: {
            ...INITIAL_STATE.deposit,
            peso: defaultPesoAccounts,
          },
        },
        validateSelectedCurrencyByAccountType(selectedSourceAccount),
      ),
    ).toEqual({
      ...INITIAL_STATE,
      deposit: {
        ...INITIAL_STATE.deposit,
        peso: defaultPesoAccounts,
        selected: 'peso',
      },
    });
  });

  it('Should select deposit account and select peso account', () => {
    const selectedSourceAccount = 'deposit';
    const selectedCurrency = 'peso';

    expect(
      reducer(
        undefined,
        setSelectedOptionByAccount(selectedCurrency, selectedSourceAccount),
      ),
    ).toEqual({
      ...INITIAL_STATE,
      deposit: {
        ...INITIAL_STATE.deposit,
        selected: selectedCurrency,
      },
    });
  });

  it('Should select all under deposit account if theres  peso and foreign accounts', () => {
    const selectedSourceAccount = 'deposit';

    const DEFAULT = {
      ...INITIAL_STATE,
      deposit: {
        ...INITIAL_STATE.deposit,
        peso: [1, 2, 3],
        foreign: [1, 2, 3],
      },
    };

    expect(
      reducer(
        DEFAULT,
        validateSelectedCurrencyByAccountType(selectedSourceAccount),
      ),
    ).toEqual({
      ...DEFAULT,
      deposit: {
        ...DEFAULT.deposit,
        selected: 'all',
      },
    });
  });

  it('Should select foreign under deposit account', () => {
    const selectedSourceAccount = 'deposit';

    const DEFAULT = {
      ...INITIAL_STATE,
      deposit: {
        ...INITIAL_STATE.deposit,
        peso: [],
        foreign: [1, 2, 3],
      },
    };

    expect(
      reducer(
        DEFAULT,
        validateSelectedCurrencyByAccountType(selectedSourceAccount),
      ),
    ).toEqual({
      ...DEFAULT,
      deposit: {
        ...DEFAULT.deposit,
        selected: 'foreign',
      },
    });
  });
});
