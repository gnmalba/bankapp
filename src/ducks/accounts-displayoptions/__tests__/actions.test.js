import {
  addForeignAccountByType,
  addPesoAccountByType,
  validateSelectedCurrencyByAccountType,
  setSelectedOptionByAccount,
} from '../actions';

describe('Unit test for Accounts Display Options Actions', () => {
  it('Should create an action to add a foriegn account by deposit type', () => {
    const id = 2320;
    const type = 'deposit';

    const expectedAction = {
      type: 'ADD_FOREIGN_ACCOUNT',
      payload: {
        id,
        type,
      },
    };

    expect(addForeignAccountByType(id, type)).toEqual(expectedAction);
  });

  it('Should create an action to add a peso account by deposit type', () => {
    const id = 2320;
    const type = 'deposit';

    const expectedAction = {
      type: 'ADD_PESO_ACCOUNT',
      payload: {
        id,
        type,
      },
    };

    expect(addPesoAccountByType(id, type)).toEqual(expectedAction);
  });

  it('Should create an action to validate currency by account type', () => {
    const type = 'deposit';

    const expectedAction = {
      type: 'VALIDATE_SELECTED_CURRENCY_BY_ACCOUNT',
      payload: {
        type,
      },
    };

    expect(validateSelectedCurrencyByAccountType(type)).toEqual(expectedAction);
  });

  it('Should create an action to select option by account type', () => {
    const selectedCurrency = 'peso';
    const type = 'deposit';

    const expectedAction = {
      type: 'SET_SELECTED_OPTION_BY_ACCOUNT',
      payload: {
        selectedCurrency,
        type,
      },
    };

    expect(setSelectedOptionByAccount(selectedCurrency, type)).toEqual(
      expectedAction,
    );
  });
});
