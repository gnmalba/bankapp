import configureStore from 'redux-mock-store';

import {
  getDepositPesoAndForeignAccounts,
  getCreditPesoAndForeignAccounts,
  getPrepaidPesoAndForeignAccounts,
} from '../selectors';

const mockStore = configureStore([]);

const INITIAL_STATE = {
  deposit: {
    peso: [1, 2, 3],
    foreign: [4, 5, 6],
    selected: 'all',
  },
  credit: {
    peso: [7, 9, 9],
    foreign: [10, 11],
    selected: 'all',
  },
  prepaid: {
    peso: [1, 5],
    foreign: [7, 9],
    selected: 'all',
  },
};

const store = mockStore({
  accountDisplayOptions: INITIAL_STATE,
});

describe('Unit test for Accounts Display Options Selectors', () => {
  it('Should get peso and foreign currency from deposit account', () => {
    expect(getDepositPesoAndForeignAccounts(store.getState())).toEqual(
      INITIAL_STATE.deposit,
    );
  });

  it('Should get peso and foreign currency from credit account', () => {
    expect(getCreditPesoAndForeignAccounts(store.getState())).toEqual(
      INITIAL_STATE.credit,
    );
  });

  it('Should get peso and foreign currency from prepaid account', () => {
    expect(getPrepaidPesoAndForeignAccounts(store.getState())).toEqual(
      INITIAL_STATE.prepaid,
    );
  });
});
