/**
 * @flow
 */

export type ID = number | string;
export type IDS = Array<ID>;

export type DisplayOptionsCurrency = 'peso' | 'foreign' | 'all';
export type AccountShape = {|
  peso: IDS,
  foreign: IDS,
  selected: DisplayOptionsCurrency,
|};

export type SourceAccountType = 'credit' | 'deposit' | 'prepaid';

export type AccountsDisplayOptionsState = {|
  credit: AccountShape,
  deposit: AccountShape,
  prepaid: AccountShape,
|};

export type AccountsDisplayOptionsAction =
  | {
      type: 'ADD_PESO_ACCOUNT',
      payload: {id: ID, type: SourceAccountType},
    }
  | {
      type: 'ADD_FOREIGN_ACCOUNT',
      payload: {id: ID, type: SourceAccountType},
    }
  | {
      type: 'VALIDATE_SELECTED_CURRENCY_BY_ACCOUNT',
      payload: {type: SourceAccountType},
    }
  | {
      type: 'SET_SELECTED_OPTION_BY_ACCOUNT',
      payload: {
        selectedCurrency: DisplayOptionsCurrency,
        type: SourceAccountType,
      },
    };
