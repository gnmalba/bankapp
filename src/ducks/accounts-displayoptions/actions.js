/**
 * @flow
 */

import type {
  AccountsDisplayOptionsAction,
  ID,
  SourceAccountType,
  DisplayOptionsCurrency,
} from './types';

export const addPesoAccountByType = (
  id: ID,
  type: SourceAccountType,
): AccountsDisplayOptionsAction => ({
  type: 'ADD_PESO_ACCOUNT',
  payload: {
    id,
    type,
  },
});

export const addForeignAccountByType = (
  id: ID,
  type: SourceAccountType,
): AccountsDisplayOptionsAction => ({
  type: 'ADD_FOREIGN_ACCOUNT',
  payload: {
    id,
    type,
  },
});

export const validateSelectedCurrencyByAccountType = (
  type: SourceAccountType,
): AccountsDisplayOptionsAction => ({
  type: 'VALIDATE_SELECTED_CURRENCY_BY_ACCOUNT',
  payload: {
    type,
  },
});

export const setSelectedOptionByAccount = (
  selectedCurrency: DisplayOptionsCurrency,
  type: SourceAccountType,
): AccountsDisplayOptionsAction => ({
  type: 'SET_SELECTED_OPTION_BY_ACCOUNT',
  payload: {
    selectedCurrency,
    type,
  },
});
