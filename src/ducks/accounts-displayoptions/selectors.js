/**
 * @flow
 */
import type {State} from '@ducks/types';
import type {AccountShape} from './types';

export const getDepositPesoAndForeignAccounts = (state: State): AccountShape =>
  state.accountDisplayOptions.deposit;

export const getCreditPesoAndForeignAccounts = (state: State): AccountShape =>
  state.accountDisplayOptions.credit;

export const getPrepaidPesoAndForeignAccounts = (state: State): AccountShape =>
  state.accountDisplayOptions.prepaid;
