/**
 * @flow
 */

import type {
  AccountsDisplayOptionsState,
  AccountsDisplayOptionsAction,
} from './types';

import {
  createAccountModification,
  getSelected,
  createSelectedCurrencyModification,
} from './utils';

const INITIAL_STATE: AccountsDisplayOptionsState = {
  deposit: {
    peso: [],
    foreign: [],
    selected: 'all',
  },
  credit: {
    peso: [],
    foreign: [],
    selected: 'all',
  },
  prepaid: {
    peso: [],
    foreign: [],
    selected: 'all',
  },
};

export default (
  state: AccountsDisplayOptionsState = INITIAL_STATE,
  action: AccountsDisplayOptionsAction,
): AccountsDisplayOptionsState => {
  switch (action.type) {
    case 'ADD_PESO_ACCOUNT':
      return createAccountModification(state, action.payload);
    case 'ADD_FOREIGN_ACCOUNT':
      return createAccountModification(state, action.payload, 'foreign');
    case 'VALIDATE_SELECTED_CURRENCY_BY_ACCOUNT':
      const accountType = action.payload.type;
      return createSelectedCurrencyModification(
        state,
        accountType,
        getSelected(state[accountType].peso, state[accountType].foreign),
      );
    case 'SET_SELECTED_OPTION_BY_ACCOUNT':
      return createSelectedCurrencyModification(
        state,
        action.payload.type,
        action.payload.selectedCurrency,
      );
    default:
      return state;
  }
};
