/**
 * @flow
 */

import {PAY_BILLS_ACTIONS} from './constants';
import type {
  PayBillsCreditAction,
  Accounts,
  Account,
  PbcImmediateProps,
  ResponsePayLoad,
} from './types';

const initializePbc = (): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.INITIALIZE_PBC,
});
const initializePbcStart = (): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.INITIALIZE_PBC_START,
});
const initializePbcComplete = (
  accounts: Accounts,
  selectedSourceAccount: Account,
  payBillsAccount: Accounts,
): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.INITIALIZE_PBC_COMPLETE,
  payload: {
    accounts,
    selectedSourceAccount,
    payBillsAccount,
  },
});

const setPbcSourceAccount = (account: Account): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.SELECT_PBC_SOURCE_ACCOUNT,
  payload: account,
});

const payBillsCreditImmediate = (
  payload: PbcImmediateProps,
): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.PAY_BILLS_CREDIT_IMMEDIATE,
  payload,
});

const payBillsCreditScheduled = (
  payload: PbcImmediateProps,
): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.PAY_BILLS_CREDIT_SCHEDULED,
  payload,
});

const payBillsCreditImmediateComplete = (
  payload: ResponsePayLoad,
): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.PAY_BILLS_CREDIT_COMPLETE,
  payload,
});

const startPayBillsCredit = (): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.START_PAY_BILLS_CREDIT,
});

const setIsPayCredit = (flag: boolean): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.SET_PAY_BILLS_TYPE,
  payload: flag,
});

const resetPayBillsCredit = (): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.RESET_PAY_BILLS_CREDIT,
});

const resetPayBillsCreditResponse = (): PayBillsCreditAction => ({
  type: PAY_BILLS_ACTIONS.RESET_PAY_BILLS_CREDIT_RESPONSE,
});

export {
  initializePbc,
  initializePbcComplete,
  initializePbcStart,
  setPbcSourceAccount,
  payBillsCreditImmediate,
  payBillsCreditImmediateComplete,
  startPayBillsCredit,
  payBillsCreditScheduled,
  setIsPayCredit,
  resetPayBillsCredit,
  resetPayBillsCreditResponse,
};
