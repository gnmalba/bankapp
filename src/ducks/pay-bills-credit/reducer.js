/**
 * @flow
 */
import type {
  PayBillsCreditState,
  ActionTypeMapper,
  PayBillsCreditAction,
} from './types';

const INITIAL_STATE: PayBillsCreditState = {
  accounts: {
    all: [],
    creditAndYazz: [],
    depositAndPrepaid: [],
  },
  selectedSourceAccount: null,
  loading: false,
  error: '',
  isInitial: true,
  payBillsAccounts: {
    all: [],
    creditAndYazz: [],
    depositAndPrepaid: [],
  },
  postRequestResponse: {
    referenceNumber: '',
    dateTime: '',
    error: {
      code: '',
      message: '',
    },
    errorType: '',
    status: null,
  },
  isPayBillsCredit: false,
};

const actionMap = {
  INITIALIZE_PBC: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
    };
  },
  INITIALIZE_PBC_START: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      loading: true,
      isInitial: true,
    };
  },
  INITIALIZE_PBC_COMPLETE: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      loading: false,
      isInitial: false,
      accounts: payload.accounts,
      payBillsAccounts: payload.payBillsAccount,
      selectedSourceAccount:
        payload.selectedSourceAccount || state.selectedSourceAccount,
    };
  },
  SELECT_PBC_SOURCE_ACCOUNT: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      selectedSourceAccount: payload,
    };
  },
  START_PAY_BILLS_CREDIT: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      loading: true,
    };
  },
  PAY_BILLS_CREDIT_COMPLETE: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      postRequestResponse: payload,
      loading: false,
    };
  },
  /* eslint-disable sonarjs/no-identical-functions */
  PAY_BILLS_CREDIT_IMMEDIATE: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
    };
  },
  PAY_BILLS_CREDIT_SCHEDULED: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
    };
  },
  SET_PAY_BILLS_TYPE: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      isPayBillsCredit: payload,
    };
  },
  RESET_PAY_BILLS_CREDIT: ({state, payload}: ActionTypeMapper) => {
    return INITIAL_STATE;
  },
  RESET_PAY_BILLS_CREDIT_RESPONSE: ({state, payload}: ActionTypeMapper) => {
    return {
      ...state,
      postRequestResponse: INITIAL_STATE.postRequestResponse,
    };
  },
};

const paybillscredit = (
  state: PayBillsCreditState = INITIAL_STATE,
  action: PayBillsCreditAction,
): PayBillsCreditState => {
  if (actionMap[action.type]) {
    return actionMap[action.type]({state, payload: action.payload || {}});
  }
  return state;
};

export default paybillscredit;
