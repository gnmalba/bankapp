/**
 * @flow
 */
import type {State} from '@ducks/types';
import {createSelector} from 'reselect';
import type {PayBillsCreditState, Account, Accounts} from './types';

export const payBillsCreditSelector = (state: State): PayBillsCreditState =>
  state.paybillscredit;

export const isPayBillCreditLoading: boolean = createSelector(
  payBillsCreditSelector,
  (paybillscredit) => paybillscredit.loading,
);

export const getSelectedSouourceAccount = (state: State): ?Account =>
  state.paybillscredit.selectedSourceAccount;

export const isPbcInitial = (state: State): boolean =>
  state.paybillscredit.isInitial;

export const payBillsAccountsSelector = (state: State): Accounts =>
  state.paybillscredit.accounts;

export const payBillsCreditResponseSelector = (state: State): Object =>
  state.paybillscredit.postRequestResponse;
