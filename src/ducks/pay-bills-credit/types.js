/**
 * @flow
 */

import type {Biller} from '@ducks/billers/types';
import ACCOUNT_TYPES from '../../enums/account-types.enum';

export type PayBillsCreditState = {
  accounts: Accounts,
  loading: boolean,
  error: string,
  isInitial: boolean,
  selectedSourceAccount: ?Account,
  payBillsAccounts: Accounts,
  postRequestResponse: ResponsePayLoad,
  isPayBillsCredit: boolean,
};

export type PbcImmediate = {
  accountId: number, //source account
  acctType: 0 | 1 | 2 | 3, // source account
  alias: string, // source account
  amount: string, // form values
  authType?: string, //passcode
  authValue?: string, //passcode
  billerId: string, // biller
  cardSerNo: string, //account
  channel: 'MOBILE' | 'WEB' | 'ALL', //default to Mobile, always mobile
  contactId: string, // biller
  currencyCode: string, // account
  customerDevice: Object, //device
  customerId: string, //from reducer
  hash: string, //accountId + amount + currencyCode + contactId
  notes: string, // forms
  paymentFields: Object, //forms
  additionalPaymentFields: Object, //fomrs
  schedule?: ?Schedule,
};

export type Schedule = {
  endDate: ?string,
  startDate: string,
  frequency: string,
  numberOfOccurrences: ?number,
};
/**
 * * DEPOSIT = 0
 * * MB_PREPAID = 1
 * * CC = 2
 * * YAZZ = 3
 */
export type AccountType = $Values<typeof ACCOUNT_TYPES>;

export type Account = {
  alias: string,
  availableBalance: string,
  accountType: AccountType,
  currencyCode: string,
  isAllowedforCcBillers?: ?boolean,
  availableCredit?: ?number,
  cardSerNo?: string,
  accountNo?: string,
  id?: ?number,
  cardNo?: ?string,
};

export type Accounts = {
  all: Array<Account>,
  depositAndPrepaid: Array<Account>,
  creditAndYazz: Array<Account>,
};

export type PayBillsCreditAction =
  | {
      type: 'INITIALIZE_PBC',
    }
  | {
      type: 'INITIALIZE_PBC_COMPLETE',
      payload: {
        accounts: Accounts,
        selectedSourceAccount: ?Account,
        payBillsAccount: Accounts,
      },
    }
  | {
      type: 'INITIALIZE_PBC_START',
    }
  | {
      type: 'SELECT_PBC_SOURCE_ACCOUNT',
      payload: Account,
    }
  | {
      type: 'PAY_BILLS_CREDIT_IMMEDIATE',
      payload: PbcImmediateProps,
    }
  | {
      type: 'PAY_BILLS_CREDIT_COMPLETE',
      payload: ResponsePayLoad,
    }
  | {
      type: 'START_PAY_BILLS_CREDIT',
    }
  | {
      type: 'PAY_BILLS_CREDIT_SCHEDULED',
      payload: PbcImmediateProps,
    }
  | {
      type: 'SET_PAY_BILLS_TYPE',
      payload: boolean,
    }
  | {
      type: 'RESET_PAY_BILLS_CREDIT',
    }
  | {
      type: 'RESET_PAY_BILLS_CREDIT_RESPONSE',
    };
export type ResponsePayLoad = {
  referenceNumber: string,
  dateTime: string,
  error: {
    code: string,
    message: string,
  },
  errorType: string,
  status: ?number,
};

export type ActionTypeMapper = {
  state: PayBillsCreditState,
  payload: Object,
};

export type PbcImmediateProps = {
  account: Account,
  biller: Biller,
  formValues: Object,
  pinCode: string,
};
