/**
 * @flow
 */

import type {
  Account,
  AccountType,
  PbcImmediateProps,
  PbcImmediate,
} from './types';
import type {
  Biller,
  BillerCategory,
  Billers,
  BillerByCategory,
  CategoryWithBillers,
} from '@ducks/billers/types';
import {generateHashCpc, sortByObjectKey} from '@utils/commons';
import {formatBillersByCategory} from '@ducks/billers/utils';
import encrypt from '@utils/encryptString';
import {generateHash, getDeviceInfo, iterateEntries} from '@utils/commons';
import Moment from 'moment';
import {extendMoment} from 'moment-range';
import {OCCURRENCE_TYPE} from '@screens/private/PayBillsCredit/constants/enums';

const moment = extendMoment(Moment);

const isPbc = (val: Object): boolean =>
  val.code === 'PAY_BILLS' || val.code === 'PAY BILLS';

const isPayBillsAllowed = (account: Object): boolean => {
  const getPbcShortcut = account.shortcuts && account.shortcuts.find(isPbc);

  return getPbcShortcut ? getPbcShortcut.enabled : false;
};
/**
 * *Removes currency string to the amount
 * @param {*} value
 * @returns PHP 123456 to --> 123456
 */
const currencyToNumber = (value: string): number =>
  Number(value.replace(/[^0-9\.-]+/g, '')); // eslint-disable-line no-useless-escape

export const accountsObjectToPbcAccount = (accounts: Object): Array<Account> =>
  accounts
    .filter(isPayBillsAllowed) //get only accounts that are allowed to pay bills
    .map(formatAccountObject)
    .sort(sortByObjectKey('alias'));

export const ACCOUNT_TYPES_ENUM = {
  DEPOSIT: 0,
  MB_PREPAID: 1,
  CC: 2,
  YAZZ: 3,
};

const getAccountType = (isCc: boolean, type: string): AccountType =>
  isCc
    ? type === 'creditcard'
      ? ACCOUNT_TYPES_ENUM.CC
      : ACCOUNT_TYPES_ENUM.YAZZ
    : type === '769'
    ? ACCOUNT_TYPES_ENUM.MB_PREPAID
    : ACCOUNT_TYPES_ENUM.DEPOSIT;

export const isCreditCard = (accountNumber: ?string): boolean =>
  accountNumber ? false : true;

export const formatAccountObject = (object: Object): Account => {
  return {
    accountNo: handleNullValue(object.accountNo),
    alias: object.alias,
    availableBalance: object.availableBalanceValueWithCurrency
      ? currencyToNumber(object.availableBalanceValueWithCurrency).toString()
      : handleNullValue(object.availableBalance),
    availableCredit: handleNullValue(object.availableCredit),
    cardNo: handleNullValue(object.cardNo || object.cardno),
    cardSerNo: handleNullValue(object.cardSerNo),
    id: handleNullValue(object.id),
    accountType: getAccountType(
      isCreditCard(object.accountNo),
      object.productType || object.type,
    ),
    currencyCode: object.currencyCode || object.currency,
    isAllowedForCcBillers: !isCreditCard(object.accountNo),
  };
};

/**
 * Check if user has only one account for pay bills
 * This Auto-populates the said account to the selected source account field
 * @param {*} all all allowed to pbc accounts
 * @returns has only one account = returns the account, else returns null
 */
export const ifOnlyOneAccountGetAccount = (all: Array<Account>): ?Account =>
  all.length === 1 ? all[0] : null;

/**
 * Check if user has only one DEPOSIT account for PAY CREDIT
 * This Auto-populates the said account to the selected source account field
 * @param {*} all all allowed to pbc accounts
 * @returns has only one account = returns the account, else returns null
 */
export const ifOnlyOneDepositAccountGetAccount = (
  depositsAndPrepaid: Array<Account>,
): ?Account => (depositsAndPrepaid.length === 1 ? depositsAndPrepaid[0] : null);

export const handleNullValue = (val: any, type?: 'string' | 'null'): any => {
  if (val) {
    return val;
  }
  return type === 'string' ? '' : null;
};

export const filterBillersByAccountCurrency = (
  currentBillers: Billers,
  categories: Array<BillerCategory>,
  currencyCode: string,
  isCc: boolean,
): {
  billers: Billers,
  allBillersByCategory: BillerByCategory,
  payBillsAllBillers: BillerByCategory,
} => {
  const matchAccountCurrToBiller = (billerRow: Biller) =>
    billerRow.currencyCode === currencyCode;
  const _billers: Billers = {
    enrolledBillers: currentBillers.enrolledBillers.filter(
      matchAccountCurrToBiller,
    ),
    allBillers: currentBillers.allBillers.filter(matchAccountCurrToBiller),
  };

  const allBillersByCategory = formatBillersByCategory(
    currentBillers.allBillers,
    categories,
  );

  const billersWithCreditCard = formatBillersByCategory(
    _billers.allBillers,
    categories,
  );

  const billersWithOutCreditCards = removeCreditCardBillers(
    billersWithCreditCard,
  );

  //get billers w/out Credit cards if source account is credit card/yazz
  //credit to credit paybills is not allowed
  const payBillsAllBillers: BillerByCategory = isCc
    ? billersWithOutCreditCards
    : billersWithCreditCard;

  const billers: Billers = isCc
    ? iterateEntries(_billers, (biller: Array<Biller>) =>
        removeCreditCardBillersEnrolledAccounts(biller),
      )
    : _billers;

  return {
    billers,
    allBillersByCategory,
    payBillsAllBillers,
  };
};

const removeCreditCardBillers = (
  billersPerCategory: BillerByCategory,
): BillerByCategory =>
  billersPerCategory.filter(
    (row: CategoryWithBillers) => row.code !== 'CREDIT_CARD',
  );

const removeCreditCardBillersEnrolledAccounts = (
  enrolledBillers: Array<Biller>,
): Array<Biller> =>
  enrolledBillers.filter((row: Biller) => row.category?.code !== 'CREDIT_CARD');

export const shouldBillerBeFiltered = (
  selectedBiller: ?Biller,
  currencyCode: string,
): boolean => {
  if (selectedBiller === null && currencyCode) {
    return true;
  }
  return false;
};

export type BuildFormDataProps = {
  payload: Object,
  encryptionPublicKey: any,
  customerId: string,
  username: string,
  billerType: string,
  cardType: string,
};

export const dateTimeToISO = (dateTime: string): string =>
  moment(dateTime).toISOString(true);

const contactIdConds = {
  allBillers: (biller: Biller) => biller.billerId,
  enrolledBillers: (biller: Biller) => biller.contactId,
};

export const billerIdAsKey = (paymentFields: Object): Object => {
  const newObject = {};
  paymentFields &&
    Object.entries(paymentFields).forEach(
      ([key, val]) => (newObject[getIdFromKey(key).toString()] = val),
    );
  return newObject;
};
const getIdFromKey = (key: string = '') => key.split('_').pop();

//TO DO: Add Object from specific conditions
const additionalPaymentFieldConds = {
  allBillers: (paymentFields: Object) => {},
  enrolledBillers: (paymentFields: Object) => billerIdAsKey(paymentFields),
};

export type occurrenceProps = {
  type: string,
  startDate: string,
  endDate: string,
  occurrence: ?number,
};

const occurrenceConds = {
  On: (props: occurrenceProps) =>
    getMaxOccurrences(props.type, props.startDate, props.endDate),
  After: (props: occurrenceProps) => props.occurrence,
};

const roundOffToTwoDecimal = (numberString: string) =>
  (Math.round(parseFloat(numberString) * 100) / 100).toFixed(2).toString();

/*################### BUILD FORM DATA START ################################################### */
/*################### DEBT: This should be refactor into one function onleh ################### */

export const buildFormData = ({
  payload,
  encryptionPublicKey,
  customerId,
  username,
  billerType,
}: BuildFormDataProps): PbcImmediate => {
  const {account, biller, formValues, pinCode}: PbcImmediateProps = payload;

  const amount = roundOffToTwoDecimal(formValues.amount);

  return {
    accountId: handleNullValue(account.id, 'string'),
    acctType: account.accountType,
    alias: account.alias,
    amount,
    authType: 'mpin',
    authValue: encrypt(encryptionPublicKey).encrypt(pinCode),
    billerId: biller.billerId,
    cardSerNo: handleNullValue(account.cardSerNo, 'string'),
    channel: 'MOBILE',
    contactId: handleNullValue(biller.contactId, 'string'),
    currencyCode: account.currencyCode,
    customerDevice: getDeviceInfo(),
    customerId: customerId,
    hash: generateHash({
      username: username,
      accountId: handleNullValue(account.id, 'string'),
      amount,
      currencyCode: account.currencyCode,
      billerId: contactIdConds[billerType](biller),
    }),
    notes: formValues.notes,
    paymentFields: billerIdAsKey(formValues.paymentFields),
    additionalPaymentFields: additionalPaymentFieldConds[billerType](
      formValues.additionalPaymentFields,
    ),
  };
};

export const buildFormDataCpc = ({
  payload,
  encryptionPublicKey,
  customerId,
  username,
  billerType,
}: BuildFormDataProps): PbcImmediate => {
  const {account, biller, formValues, pinCode}: PbcImmediateProps = payload;

  const amount = roundOffToTwoDecimal(formValues.amount);

  return {
    accountId: handleNullValue(account.id, 'string'),
    acctType: account.accountType,
    alias: account.alias,
    amount,
    passcode: encrypt(encryptionPublicKey).encrypt(pinCode),
    billerId: biller.billerId,
    cardSerNo: handleNullValue(account.cardSerNo, 'string'),
    channel: 'MOBILE',
    contactId: handleNullValue(biller.contactId, 'string'),
    currencyCode: account.currencyCode,
    customerDevice: getDeviceInfo(),
    customerId: customerId,
    hash: generateHashCpc({
      username: username,
      amount,
      currencyCode: account.currencyCode,
      billerId: contactIdConds[billerType](biller),
    }),
    schedule: {
      endDate: '',
      frequency: '',
      numberOfOccurrences: 0,
      startDate: '',
    },
    notes: formValues.notes,
    paymentFields: billerIdAsKey(formValues.paymentFields),
    additionalPaymentFields: additionalPaymentFieldConds[billerType](
      formValues.additionalPaymentFields,
    ),
  };
};

export const buildFormDataScheduled = ({
  payload,
  encryptionPublicKey,
  customerId,
  username,
  billerType,
  cardType,
}: BuildFormDataProps): PbcImmediate => {
  const {account, biller, formValues, pinCode}: PbcImmediateProps = payload;

  const amount = roundOffToTwoDecimal(formValues.amount);

  return {
    accountId: handleNullValue(account.id, 'string'),
    acctType: account.accountType,
    alias: account.alias,
    amount,
    authType: 'mpin',
    authValue: encrypt(encryptionPublicKey).encrypt(pinCode),
    billerId: biller.billerId,
    cardSerNo: handleNullValue(account.cardSerNo, 'string'),
    channel: 'MOBILE',
    contactId: handleNullValue(biller.contactId, 'string'),
    currencyCode: account.currencyCode,
    customerDevice: getDeviceInfo(),
    customerId: customerId,
    hash: generateHash({
      username: username,
      accountId: handleNullValue(account.id, 'string'),
      amount,
      currencyCode: account.currencyCode,
      billerId: contactIdConds[billerType](biller),
    }),
    notes: formValues.notes,
    paymentFields: billerIdAsKey(formValues.paymentFields),
    additionalPaymentFields: additionalPaymentFieldConds[billerType](
      formValues.additionalPaymentFields,
    ),
    schedule: buildScheduleObject(formValues, cardType),
  };
};

const buildScheduleObject = (formValues: Object, cardType: string) => {
  const isOneTime = formValues.occurrenceType === OCCURRENCE_TYPE.ONE_TIME;

  return {
    startDate: dateConds[cardType](formValues.dateTime || formValues.startDate),
    endDate: formValues.endDate
      ? dateConds[cardType](formValues.endDate)
      : null,
    frequency: handleNullValue(formValues.frequencyType?.value?.toUpperCase()),
    numberOfOccurrences: isOneTime
      ? null
      : occurrenceConds[formValues.endType]({
          type: formValues.frequencyType?.value,
          startDate: formValues.startDate,
          endDate: formValues.endDate,
          occurrence: formValues.occurrences,
        }),
  };
};

export const buildFormDataCpcScheduled = ({
  payload,
  encryptionPublicKey,
  customerId,
  username,
  billerType,
  cardType,
}: BuildFormDataProps): PbcImmediate => {
  const {account, biller, formValues, pinCode}: PbcImmediateProps = payload;

  const amount = roundOffToTwoDecimal(formValues.amount);

  return {
    accountId: handleNullValue(account.id, 'string'),
    acctType: account.accountType,
    alias: account.alias,
    amount,
    passcode: encrypt(encryptionPublicKey).encrypt(pinCode),
    billerId: biller.billerId,
    cardSerNo: handleNullValue(account.cardSerNo, 'string'),
    channel: 'MOBILE',
    contactId: handleNullValue(biller.contactId, 'string'),
    currencyCode: account.currencyCode,
    customerDevice: getDeviceInfo(),
    customerId: customerId,
    hash: generateHashCpc({
      username: username,
      amount,
      currencyCode: account.currencyCode,
      billerId: contactIdConds[billerType](biller),
    }),
    schedule: buildScheduleObject(formValues, cardType),
    notes: formValues.notes,
    paymentFields: billerIdAsKey(formValues.paymentFields),
    additionalPaymentFields: additionalPaymentFieldConds[billerType](
      formValues.additionalPaymentFields,
    ),
  };
};

/*################### BUILD FORM DATA END ################### */

export type CardType = 'CREDIT_CARD' | 'DEBIT_PREPAID';

export const FREQUENCY_MOMENT_TYPE = {
  Daily: 'days',
  Weekly: 'week',
  Monthly: 'month',
  Quarterly: 'quarter',
  'Semi-annually': 'year',
  Annually: 'year',
};

export const formatDate = (dateTime: string): string => {
  return moment(dateTime).format('MMMM D, YYYY hh:mm A');
};

const dateConds = {
  CREDIT_CARD: (date) => dateTimeToISO(date),
  DEBIT_PREPAID: (date) => formatDate(date),
};

export const getMaxOccurrences = (
  frequencyType: string,
  startDateTime: string,
  endDate: string,
): number => {
  let startDate = moment().format('MM/DD/yyyy, hh:mm A');
  let startDatePlusThreeYears = moment().add(3, 'y');
  const rangeBy = FREQUENCY_MOMENT_TYPE[frequencyType];
  const range = moment().range(
    moment(startDateTime).isValid() ? startDateTime : startDate,
    moment(endDate).isValid()
      ? moment(`${endDate}, ${moment(startDateTime).format('hh:mm A')}`)
      : startDatePlusThreeYears,
  );
  const occurrences = Array.from(range.by(rangeBy));

  if (frequencyType === 'Semi-annually') {
    return (occurrences?.length - 1) * 2;
  }
  return occurrences?.length - 1;
};
