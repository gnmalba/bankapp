/**
 * @flow
 */
import {put, select, takeEvery, takeLatest, call} from 'redux-saga/effects';
import {
  initializePbcComplete,
  initializePbcStart,
  payBillsCreditImmediateComplete,
  startPayBillsCredit,
  resetPayBillsCredit,
  resetPayBillsCreditResponse,
  setPbcSourceAccount,
} from './actions';
import {
  accountsObjectToPbcAccount,
  ifOnlyOneAccountGetAccount,
  handleNullValue,
  filterBillersByAccountCurrency,
  isCreditCard,
  shouldBillerBeFiltered,
  buildFormData,
  buildFormDataCpc,
  buildFormDataScheduled,
  buildFormDataCpcScheduled,
  formatAccountObject,
} from './utils';

import {AUTHENTICATION_LOGOUT} from '@ducks/authentication/actionTypes';

import {
  selectedBillerSelector,
  billersSelector,
  billerCategorySelector,
  selectedBillerTypeSelector,
} from '@ducks/billers/selector';

import {getSelectedSouourceAccount, isPbcInitial} from './selector';

import type {
  Biller,
  BillerCategory,
  Billers,
  BillerByCategory,
  Field,
  Bin,
} from '@ducks/billers/types';
import type {Account} from './types';
import {getAllBillerSuccess, setPbcBiller} from '@ducks/billers/actions';
import {billersByAlphabet, formatBillerforPbc} from '@ducks/billers/utils';

import {navigate} from '@navigations/service';
import {
  getCustomerID,
  getUsername,
  getEncryptionPublicKey,
  getAllAccounts,
} from '@ducks/commonSelectors';

import api from '@api/services/payBillsCreditService';
import {
  PAY_BILLS_ACTIONS,
  CARD_TYPES,
  ERROR_TYPES,
  ERROR_STATUS,
} from './constants';

import {selectedCreditSelector} from '@ducks/credit/selectors';
import {BILLER_TYPE} from '@screens/private/PayBillsCredit/BillersScreen/constants';
export function* initializePayBillsCredit(): Generator<any, any, any> {
  yield put(initializePbcStart());

  const {all: allAccounts, creditAndYazz, depositsAndPrepaid} = yield select(
    getAllAccounts,
  );

  const accounts = {
    all: accountsObjectToPbcAccount(allAccounts),
    depositAndPrepaid: accountsObjectToPbcAccount(depositsAndPrepaid),
    creditAndYazz: accountsObjectToPbcAccount(creditAndYazz),
  };
  if (accounts.depositAndPrepaid.length === 1) {
    yield put(setPbcSourceAccount(accounts.depositAndPrepaid[0]));
  }
  yield put(
    initializePbcComplete(
      accounts,
      handleNullValue(ifOnlyOneAccountGetAccount(accounts.all)),
      accounts, //will add filter for this one
    ),
  );
}
const getNewCategories = (billersByCategory: BillerByCategory) =>
  billersByCategory.map(({billers, ...row}) => row);
export function* processBillersAfterSourceAccountSelect(): Generator<
  any,
  any,
  any,
> {
  /*START SELECT BILLERS */
  const currentBillers: Billers = yield select(billersSelector);
  const currentBillerCategory: Array<BillerCategory> = yield select(
    billerCategorySelector,
  );
  const currentSelectedBiller: ?Biller = yield select(selectedBillerSelector);
  /*END SELECT BILLERS */

  const selectedSourceAccount: ?Account = yield select(
    getSelectedSouourceAccount,
  );

  const isInitial: boolean = yield select(isPbcInitial);

  const currencyCode = handleNullValue(selectedSourceAccount?.currencyCode);
  const isCc = isCreditCard(selectedSourceAccount?.accountNo);

  /*
   *This filters biller base on selected account currency if there is no selected biller yet
   */
  if (shouldBillerBeFiltered(currentSelectedBiller, currencyCode)) {
    //go here if:
    //has no biller and selected account is not null
    const {
      billers,
      allBillersByCategory,
      payBillsAllBillers,
    } = filterBillersByAccountCurrency(
      currentBillers,
      currentBillerCategory,
      currencyCode,
      isCc,
    );

    const AllBillers = {
      alphabetical: billersByAlphabet(billers.allBillers),
      section: formatBillerforPbc(payBillsAllBillers),
    };
    yield put(
      getAllBillerSuccess(
        currentBillers,
        getNewCategories(payBillsAllBillers),
        allBillersByCategory,
        AllBillers,
        billers.enrolledBillers,
      ),
    );
  }

  //goback to main screen after account select
  if (!isInitial || selectedSourceAccount) {
    navigate('PayBillsCredit', {screen: 'MainScreen'});
  }
}

const convertApiResponseToObject = (response: Object) => {
  return {
    referenceNumber: response.data.data,
    dateTime: handleNullValue(response.data.datetime),
    error: null,
    status: response.status,
  };
};
const convertApiErrorResponseToObject = (response: Object) => {
  const error = response?.data?.data?.errors[0];

  const errorTypes = {
    mpin: ERROR_TYPES.IN_LINE,
    'MB-1729': ERROR_TYPES.IN_LINE,
    invalid_mpin_exceeded: ERROR_TYPES.FULL_PAGE,
    'MB-1749': ERROR_TYPES.FULL_PAGE,
  };

  return {
    referenceNumber: '',
    dateTime: handleNullValue(response.data.datetime),
    error,
    status: response.status,
    errorType:
      response.status === ERROR_STATUS.ERROR_400
        ? errorTypes[error?.code] ?? ERROR_TYPES.FULL_PAGE
        : ERROR_TYPES.SYSTEM_ERROR,
  };
};

const immediateConds = {
  allBillers: 'IMMEDIATE_UNENROLLED',
  enrolledBillers: 'IMMEDIATE_ENROLLED',
};

const scheduleConds = {
  allBillers: 'SCHEDULED_UNENROLLED',
  enrolledBillers: 'SCHEDULED_ENROLLED',
};

const apiConds = {
  CREDIT_CARD: 'CPC',
  DEBIT_PREPAID: 'CASA',
};

function* payImmediate({
  data,
  billerType,
  cardType,
}: {
  data: Object,
  billerType: string,
  cardType: CardType,
}): Generator<any, Object, any> {
  try {
    const response = yield call(api.payBillsCredit, {
      whatApi: apiConds[cardType],
      data,
      transactionType: immediateConds[billerType],
    });
    return convertApiResponseToObject(response);
  } catch (apiErrorResponse) {
    return convertApiErrorResponseToObject(apiErrorResponse.response);
  }
}

function* payScheduled({
  data,
  billerType,
  cardType,
}: {
  data: Object,
  billerType: string,
  cardType: CardType,
}): Generator<any, Object, any> {
  try {
    const response = yield call(api.payBillsCredit, {
      whatApi: apiConds[cardType],
      data,
      transactionType: scheduleConds[billerType],
    });
    return convertApiResponseToObject(response);
  } catch (apiErrorResponse) {
    return convertApiErrorResponseToObject(apiErrorResponse.response);
  }
}

export type CardType = 'CREDIT_CARD' | 'DEBIT_PREPAID';

export function* payBillsImmediate(action: Object): Generator<any, any, any> {
  yield put(startPayBillsCredit());
  yield put(resetPayBillsCreditResponse());
  const customerId = yield select(getCustomerID);
  const username = yield select(getUsername);
  const encryptionPublicKey = yield select(getEncryptionPublicKey);
  const billerType: string = yield select(selectedBillerTypeSelector);

  const {account} = action.payload;
  const isCc = isCreditCard(account?.accountNo);

  const cardType: CardType = isCc
    ? CARD_TYPES.CREDIT_CARD
    : CARD_TYPES.DEBIT_PREPAID;

  const buildFormConds = {
    CREDIT_CARD: () =>
      buildFormDataCpc({
        payload: action.payload,
        encryptionPublicKey,
        customerId,
        username,
        billerType,
        cardType,
      }),
    DEBIT_PREPAID: () =>
      buildFormData({
        payload: action.payload,
        encryptionPublicKey,
        customerId,
        username,
        billerType,
        cardType,
      }),
  };

  const payBillsImmediateResponse = yield call(payImmediate, {
    data: buildFormConds[cardType](),
    billerType,
    cardType,
  });

  yield put(payBillsCreditImmediateComplete(payBillsImmediateResponse));

  if (payBillsImmediateResponse?.error === null) {
    navigate('PayBillsCredit', {screen: 'EndOfTransactionScreen'});
  } else if (payBillsImmediateResponse?.errorType !== ERROR_TYPES.IN_LINE) {
    yield put(resetPayBillsCreditResponse());
    navigate('PayBillsCredit', {
      screen: 'MaximumPasscodeScreen',
      params: payBillsImmediateResponse,
    });
  }
}

export function* payBillsScheduled(action: Object): Generator<any, any, any> {
  yield put(startPayBillsCredit());
  yield put(resetPayBillsCreditResponse());
  const customerId = yield select(getCustomerID);
  const username = yield select(getUsername);
  const encryptionPublicKey = yield select(getEncryptionPublicKey);
  const billerType: string = yield select(selectedBillerTypeSelector);

  const {account} = action.payload;
  const isCc = isCreditCard(account?.accountNo);

  const cardType: CardType = isCc
    ? CARD_TYPES.CREDIT_CARD
    : CARD_TYPES.DEBIT_PREPAID;

  const buildFormConds = {
    CREDIT_CARD: () =>
      buildFormDataCpcScheduled({
        payload: action.payload,
        encryptionPublicKey,
        customerId,
        username,
        billerType,
        cardType,
      }),
    DEBIT_PREPAID: () =>
      buildFormDataScheduled({
        payload: action.payload,
        encryptionPublicKey,
        customerId,
        username,
        billerType,
        cardType,
      }),
  };

  const payBillsScheduledResponse = yield call(payScheduled, {
    data: buildFormConds[cardType](),
    billerType,
    cardType,
  });

  yield put(payBillsCreditImmediateComplete(payBillsScheduledResponse));

  if (payBillsScheduledResponse?.error === null) {
    navigate('PayBillsCredit', {screen: 'EndOfTransactionScreen'});
  } else if (payBillsScheduledResponse?.errorType !== ERROR_TYPES.IN_LINE) {
    yield put(resetPayBillsCreditResponse());
    navigate('PayBillsCredit', {
      screen: 'MaximumPasscodeScreen',
      params: payBillsScheduledResponse,
    });
  }
}

export function* setPayCreditBiller(action: Object): Generator<any, any, any> {
  if (action.payload === true) {
    const selectedCreditCard = yield select(selectedCreditSelector);
    const {enrolledBillers, allBillers}: Billers = yield select(
      billersSelector,
    );
    const toPbcAcountObject = formatAccountObject(selectedCreditCard);
    const firstSixDigitOfCardNo = handleNullValue(
      toPbcAcountObject.cardNo,
      'string',
    ).substring(0, 6);

    const getBillerFromSelectedCc = (): {
      biller: string,
      billerType: string,
    } => {
      let matchedEnrolledBiller = null;
      enrolledBillers.map((billerRow: Biller) => {
        const {labelFields} = billerRow.fields;

        const getCorrespondingEnrolledBiller = labelFields.find(
          (labelFieldsRow: Field) =>
            labelFieldsRow.value ===
            handleNullValue(toPbcAcountObject.cardNo, 'string'),
        );

        matchedEnrolledBiller = getCorrespondingEnrolledBiller
          ? billerRow.billerId
          : null;
      });

      const billerType = matchedEnrolledBiller
        ? BILLER_TYPE.enrolledBillers
        : BILLER_TYPE.allBillers;

      const matchUnenrolledBiller = (): string => {
        let unenrolledBiller = '';
        allBillers.map((billerRow: Biller) => {
          const correspondingBiller = billerRow?.bins?.find(
            (bin: Bin) => bin?.bin === firstSixDigitOfCardNo,
          );

          if (correspondingBiller) {
            unenrolledBiller = billerRow.billerId;
          }
        });
        return unenrolledBiller;
      };

      return {
        biller: matchedEnrolledBiller ?? matchUnenrolledBiller(),
        billerType,
      };
    };

    const {biller, billerType: _billerType} = getBillerFromSelectedCc();

    yield put(
      setPbcBiller(biller, _billerType, () => {
        console.log(biller);
      }),
    );
  }
}

export function* clearPBC(): Generator<any, any, any> {
  yield put(resetPayBillsCredit());
}

function* payBillsCreditWatcher(): Generator<any, any, any> {
  yield takeEvery(PAY_BILLS_ACTIONS.INITIALIZE_PBC, initializePayBillsCredit);
  yield takeLatest(
    PAY_BILLS_ACTIONS.SELECT_PBC_SOURCE_ACCOUNT,
    processBillersAfterSourceAccountSelect,
  );
  yield takeLatest(
    PAY_BILLS_ACTIONS.INITIALIZE_PBC_COMPLETE,
    processBillersAfterSourceAccountSelect,
  );

  yield takeLatest(
    PAY_BILLS_ACTIONS.PAY_BILLS_CREDIT_IMMEDIATE,
    payBillsImmediate,
  );

  yield takeLatest(
    PAY_BILLS_ACTIONS.PAY_BILLS_CREDIT_SCHEDULED,
    payBillsScheduled,
  );

  yield takeLatest(PAY_BILLS_ACTIONS.SET_PAY_BILLS_TYPE, setPayCreditBiller);

  yield takeLatest(AUTHENTICATION_LOGOUT, clearPBC);
}

export default payBillsCreditWatcher;
