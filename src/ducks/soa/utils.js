/**
 * @flow
 */

import moment from 'moment';
import {DATE_FORMAT} from '@utils/constants';

export const dateChecker = (date: string): any => {
  if (date === 'IMMEDIATE') {
    return date;
  } else {
    if (moment(date).format(DATE_FORMAT) === 'Invalid date') {
      return '-';
    } else {
      return moment(date).format(DATE_FORMAT);
    }
  }
};
