/**
 * @flow
 */

import type {SOAstate} from './types';

export const INITIAL_STATE: SOAstate = {
  soaList: {
    loading: false,
    data: [],
    loader: [],
    incomingError: false,
    incomingErrors: [],
    lastError: false,
    lastErrors: [],
    lastIndex: '',
  },
  soaTransaction: {
    data: [],
    loading: false,
    loader: [],
    error: true,
  },
  soaLastestDetails: {
    data: {},
    loading: false,
    error: true,
  },
};

const accounts = (
  state: any = INITIAL_STATE,
  action: Object = {},
): SOAstate => {
  switch (action.type) {
    case 'REQUEST_SOA_INCOMING_LAST_API':
      return {
        ...state,
        soaList: {
          ...state.soaList,
          loading: true,
          loader: [],
        },
      };

    case 'GET_SOA_INCOMING_LAST_LIST_SUCCESS':
      return {
        ...state,
        soaList: {
          ...state.soaList,
          loading: false,
          data: action.payload,
          loader: ['soa'],
          lastIndex: action.lastIndex,
        },
      };

    case 'GET_SOA_INCOMING_LIST_FAILED':
      return {
        ...state,
        soaList: {
          ...state.soaList,
          loading: true,
          incomingError: true,
          incomingErrors: action.payload,
          loader: [],
        },
      };

    case 'GET_SOA_LAST_LIST_FAILED':
      return {
        ...state,
        soaList: {
          ...state.soaList,
          loading: true,
          lastError: true,
          lastErrors: action.payload,
          loader: [],
        },
      };

    case 'REQUEST_SOA_TRANSACTION_API':
      return {
        ...state,
        soaTransaction: {
          data: [],
          loading: true,
          loader: [],
          error: false,
        },
      };

    case 'GET_SOA_TRANSACTION_SUCCESS':
      return {
        ...state,
        soaTransaction: {
          data: action.payload,
          loading: false,
          loader: ['soa'],
          error: false,
        },
      };

    case 'GET_SOA_TRANSACTION_FAILED':
      return {
        ...state,
        soaTransaction: {
          data: action.payload,
          loading: false,
          loader: ['soa'],
          error: true,
        },
      };

    case 'REQUEST_SOA_LAST_DETAILS_API':
      return {
        ...state,
        soaLastestDetails: {
          data: {},
          loading: true,
          error: false,
        },
      };

    case 'GET_SOA_LAST_DETAILS_SUCCESS':
      return {
        ...state,
        soaLastestDetails: {
          data: action.payload,
          loading: false,
          error: false,
        },
      };

    case 'GET_SOA_LAST_DETAILS_FAILED':
      return {
        ...state,
        soaLastestDetails: {
          data: action.payload,
          loading: false,
          error: true,
        },
      };

    default:
      return state;
  }
};

export default accounts;
