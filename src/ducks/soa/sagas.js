/**
 * @flow
 */

import {takeLatest, put, call, select, all} from 'redux-saga/effects';
import CreditConfig from '@api/services/soaService';
import moment from 'moment';

import type {T} from './types';
import {getCustomerID} from '@ducks/commonSelectors';
import {DATE_FORMAT} from '@utils/constants';
import {numberFormat} from '@utils/accountNumber';
import {reconstructTransactionData} from '../credit/utils';
import {dateChecker} from './utils';

function* getIncoming(payload): T {
  const customerId = yield select(getCustomerID);
  try {
    const response = yield call(CreditConfig.getIncoming, customerId, payload);
    return response.data.data;
  } catch (error) {
    yield put({
      type: 'GET_SOA_LAST_LIST_FAILED',
      payload: error.response,
    });
    return [];
  }
}

function* getLast(payload): T {
  const customerId = yield select(getCustomerID);
  try {
    const response = yield call(CreditConfig.getLast, customerId, payload);
    return response.data.data;
  } catch (error) {
    yield put({
      type: 'GET_SOA_INCOMING_LIST_FAILED',
      payload: error.response,
    });
    return [];
  }
}

export function* getSoa({payload}: {payload: string}): T {
  const [incoming, last] = yield all([
    call(getIncoming, payload),
    call(getLast, payload),
  ]);

  const arrIncoming = [];
  let arrLast = [];

  if (last.statementList.length > 0) {
    last.statementList.forEach((item) => {
      const val = {
        generatedDate: moment(item.generatedDate).format(DATE_FORMAT),
        totalAmountDue: `${numberFormat(item.totalAmountDue)}`,
        minimumAmountDue: `${numberFormat(item.minimumAmountDue)}`,
        dueDate: item.dueDate ? dateChecker(item.dueDate) : '-',
        isIncoming: false,
        isDue: false,
        billingDate: moment(item.billingDate).format(DATE_FORMAT),
        lastPaymentDate: !item.lastPaymentDate
          ? '-'
          : moment(item.lastPaymentDate).format(DATE_FORMAT),
        lastPayment: item.lastPayment
          ? `${numberFormat(item.lastPayment)}`
          : '0.00',
      };

      arrLast.push(val);
    });
  }

  if (incoming.statementList.length > 0) {
    incoming.statementList.forEach((item) => {
      const val = {
        billingDate: 'Incoming',
        totalAmountDue: `${numberFormat(item.totalAmountDue)}`,
        minimumAmountDue: `${numberFormat(item.minimumAmountDue)}`,
        dueDate: item.dueDate ? dateChecker(item.dueDate) : '-',
        isIncoming: true,
        isDue: false,
        lastPaymentDate: moment(item.lastPaymentDate).format(DATE_FORMAT),
        lastPayment: item.lastPayment,
      };
      arrIncoming.push(val);
    });
  }

  arrLast = arrLast.reverse();
  const combinedIncomingLast = [...arrLast, ...arrIncoming];
  let lastIndex = '';

  if (combinedIncomingLast.length > 0) {
    lastIndex =
      combinedIncomingLast[combinedIncomingLast.length - 2].billingDate;
  }

  yield put({
    type: 'GET_SOA_INCOMING_LAST_LIST_SUCCESS',
    payload: combinedIncomingLast,
    lastIndex: lastIndex,
  });
}

export function* getGetTransaction(action: any): T {
  const customerId = yield select(getCustomerID);

  // remove keys that has no data
  if (action.payload?.cardOwners || action.payload?.trxnTypes) {
    if (action.payload?.trxnTypes.length === 0) {
      delete action.payload.trxnTypes;
    }
    if (action.payload?.cardOwners.length === 0) {
      delete action.payload.cardOwners;
    }
  }
  const params = {
    ...action.payload,
    customerId,
  };
  const url = new URLSearchParams();

  for (const key in params) {
    if (key === 'cardOwners' || key === 'trxnTypes') {
      Array.isArray(params[key]) &&
        params[key].forEach((item) => {
          url.append(key, item);
        });
    } else {
      url.append(key, params[key]);
    }
  }

  try {
    let response = [];

    if (action.tab === 'incoming') {
      response = yield call(CreditConfig.getTransactionIncoming, url);
    } else {
      response = yield call(CreditConfig.getTransactionPast, url);
    }

    const reconstructItem = reconstructTransactionData(response, true);

    yield put({
      type: 'GET_SOA_TRANSACTION_SUCCESS',
      payload: {transactionHistory: reconstructItem},
    });
  } catch (error) {
    yield put({
      type: 'GET_SOA_TRANSACTION_FAILED',
      payload: error.response,
    });
  }
}

export function* getGetTransactionLast({payload}: {payload: string}): T {
  const customerId = yield select(getCustomerID);
  try {
    const response = yield call(CreditConfig.getLast, customerId, payload);
    let item = {};

    if (response.data.data.statementList.length > 0) {
      const latest = response.data.data.statementList[0];
      const data = {
        minimumAmountDue: `PHP ${numberFormat(latest.minimumAmountDue)}`,
        totalAmountDue: `PHP ${numberFormat(latest.totalAmountDue)}`,
        dueDate: latest.dueDate ? dateChecker(latest.dueDate) : '-',
      };

      item = data;
    }

    yield put({
      type: 'GET_SOA_LAST_DETAILS_SUCCESS',
      payload: item,
    });
  } catch (error) {
    yield put({
      type: 'GET_SOA_LAST_DETAILS_FAILED',
      payload: error.response,
    });
  }
}

export default function* watcher(): any {
  yield takeLatest('REQUEST_SOA_INCOMING_LAST_API', getSoa);
  yield takeLatest('REQUEST_SOA_TRANSACTION_API', getGetTransaction);
  yield takeLatest('REQUEST_SOA_LAST_DETAILS_API', getGetTransactionLast);
}
