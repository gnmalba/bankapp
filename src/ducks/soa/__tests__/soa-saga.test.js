// import {takeLatest, call, all, select} from 'redux-saga/effects';
import {
  getSoa,
  getGetTransaction,
  //   getIncoming,
  //   getLast,
  //   getGetTransactionLast,
} from '../sagas';
import {expectSaga} from 'redux-saga-test-plan';
// import CreditConfig from '@api/services/soaService';

// import {getCustomerId} from '@ducks/authentication/reducer';

describe('Deposit and Credit Sagas', () => {
  const payload = {
    customerId: '5811',
  };

  it('soa', () => {
    expectSaga(getSoa, {payload}).run();
  });

  it('soa transaction', () => {
    expectSaga(getGetTransaction, {payload}).run();
  });

  //   it('soa transaction success', () => {
  //     return expectSaga(getGetTransactionLast, {payload})
  //       .provide([
  //         [select(getCustomerId), payload.customerId],
  //         [matchers.call.fn(CreditConfig.getLast), payload],
  //       ])
  //       .put({type: 'GET_SOA_LAST_DETAILS_SUCCESS', payload: {}})
  //       .run();
  //   });

  //   it('soa transaction fails', () => {
  //     return expectSaga(getGetTransactionLast, {payload})
  //       .provide({
  //         call() {
  //           throw new Error('Not Found');
  //         },
  //       })
  //       .put.like({action: {type: 'GET_SOA_LAST_DETAILS_FAILED'}})
  //       .run();
  //   });

  //   it('Should call soa incoming', () => {
  // expect(gen.next().value).toEqual(
  //   takeLatest('REQUEST_SOA_INCOMING_LAST_API', getSoa),
  // );
  // const generator = sagaHelper(getSoa(payload));
  // console.log("-------------'", generator);
  // expectSaga(generator.next().value).toEqual(
  //   all([
  //     call(getIncoming, payload.customerId),
  //     call(getLast, payload.customerId),
  //   ]),
  // );
  // test('Should call GET_CUSTOMER_INFO_SUCCESS and trigger fetch activation card ', async () => {
  //   const payload = {};
  //   expectSaga(fetchCardsForActivation, {payload}).run();
  // });
  // test('Should run activate Card Requested ', async () => {
  //   const payload = {
  //     cardSerialNo: '123',
  //     passcode: '123',
  //   };
  //   expectSaga(activateCardRequested, {payload}).run();
  // });
  //   });
});
