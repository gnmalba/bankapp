import reducer, {INITIAL_STATE} from '../reducer';

describe('SOA reducer', () => {
  const payload = {};

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('should handle REQUEST_SOA_INCOMING_LAST_API', () => {
    const action = {
      type: 'REQUEST_SOA_INCOMING_LAST_API',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaList: {
        ...INITIAL_STATE.soaList,
        loading: true,
        loader: [],
      },
    });
  });

  it('should handle GET_SOA_INCOMING_LAST_LIST_SUCCESS', () => {
    const action = {
      type: 'GET_SOA_INCOMING_LAST_LIST_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaList: {
        ...INITIAL_STATE.soaList,
        loading: false,
        data: payload,
        loader: ['soa'],
        lastIndex: undefined,
      },
    });
  });

  it('should handle GET_SOA_INCOMING_LIST_FAILED', () => {
    const action = {
      type: 'GET_SOA_INCOMING_LIST_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaList: {
        ...INITIAL_STATE.soaList,
        loading: true,
        incomingError: true,
        incomingErrors: payload,
        loader: [],
      },
    });
  });

  it('should handle GET_SOA_LAST_LIST_FAILED', () => {
    const action = {
      type: 'GET_SOA_LAST_LIST_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaList: {
        ...INITIAL_STATE.soaList,
        loading: true,
        lastError: true,
        lastErrors: payload,
        loader: [],
      },
    });
  });

  it('should handle REQUEST_SOA_TRANSACTION_API', () => {
    const action = {
      type: 'REQUEST_SOA_TRANSACTION_API',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaTransaction: {
        data: [],
        loading: true,
        loader: [],
        error: false,
      },
    });
  });

  it('should handle GET_SOA_TRANSACTION_SUCCESS', () => {
    const action = {
      type: 'GET_SOA_TRANSACTION_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaTransaction: {
        data: payload,
        loading: false,
        loader: ['soa'],
        error: false,
      },
    });
  });

  it('should handle GET_SOA_TRANSACTION_FAILED', () => {
    const action = {
      type: 'GET_SOA_TRANSACTION_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaTransaction: {
        data: payload,
        loading: false,
        loader: ['soa'],
        error: true,
      },
    });
  });

  it('should handle REQUEST_SOA_LAST_DETAILS_API', () => {
    const action = {
      type: 'REQUEST_SOA_LAST_DETAILS_API',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaLastestDetails: {
        data: {},
        loading: true,
        error: false,
      },
    });
  });

  it('should handle GET_SOA_LAST_DETAILS_SUCCESS', () => {
    const action = {
      type: 'GET_SOA_LAST_DETAILS_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaLastestDetails: {
        data: payload,
        loading: false,
        error: false,
      },
    });
  });

  it('should handle GET_SOA_LAST_DETAILS_FAILED', () => {
    const action = {
      type: 'GET_SOA_LAST_DETAILS_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      soaLastestDetails: {
        data: payload,
        loading: false,
        error: true,
      },
    });
  });
});
