/**
 * @format
 * @flow
 */
import {all} from 'redux-saga/effects';

import initSagas from './init/sagas';
import authSagas from './authentication/sagas';
import configSagas from './config/sagas';
import bioSagas from './biometrics/sagas';
import depositcredit from './deposit/sagas';
import enrollmentSagas from './enrollment/sagas';
import installmentsSaga from './installments/sagas';
import manageAccountSettings from './manageAccountSettings/sagas';
import depositprepaidoverview from './depositprepaidoverview/sagas';
import reportLostCardSagas from './lostcard/saga';
import credit from './credit/sagas';
import yazzSagas from './yazz/sagas';
import soa from './soa/sagas';
import billersSagas from './billers/sagas';
import paybillscreditSagas from './pay-bills-credit/sagas';
import contacts from './contacts/sagas';
import schedules from './schedules/sagas';
import manageSettingsSagas from './managesettings/sagas';

import termsConditionSagas from './termscondition/sagas';
import cardActivationSagas from './card-activation/sagas';
import recoverAccessSagas from './recover-access/sagas';
import sendMoneySagas from './sendMoney/sagas';

import shortcuts from './shortcuts/sagas';

export default function* rootSaga(): Generator<any, void, any> {
  yield all([
    authSagas(),
    initSagas(),
    recoverAccessSagas(),
    configSagas(),
    bioSagas(),
    depositcredit(),
    enrollmentSagas(),
    installmentsSaga(),
    manageAccountSettings(),
    depositprepaidoverview(),
    credit(),
    reportLostCardSagas(),
    yazzSagas(),
    soa(),
    billersSagas(),
    paybillscreditSagas(),
    contacts(),
    schedules(),
    termsConditionSagas(),
    sendMoneySagas(),
    cardActivationSagas(),
    manageSettingsSagas(),
    shortcuts(),
  ]);
}
