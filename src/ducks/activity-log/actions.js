/**
 * @flow
 */
import type {ActivityLogAction} from './types';

export const getActivityLogRequested = (
  page: number,
  size: number,
): ActivityLogAction => ({
  type: 'GET_ACTIVITY_LOG_REQUESTED',
  payload: {
    page,
    size,
  },
});
