/**
 * @flow
 */

export type getActivityLogApiParams = {
  username: string,
  customerId: number,
  page: number,
  size: number,
};

export type ActivityLogState = {|
  data: Array<number>,
  loading: boolean,
|};

export type ActivityLogAction = {
  type: 'GET_ACTIVITY_LOG_REQUESTED',
  +payload: {
    page: $PropertyType<getActivityLogApiParams, 'page'>,
    size: $PropertyType<getActivityLogApiParams, 'size'>,
  },
};
