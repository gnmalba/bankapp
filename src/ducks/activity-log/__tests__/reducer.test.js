import {activityLog, INITIAL_STATE} from '../reducer';

describe('reducer', () => {
  test('reducer', () => {
    expect(
      activityLog(INITIAL_STATE, {type: 'GET_ACTIVITY_LOG_REQUESTED'}),
    ).toEqual({...INITIAL_STATE, loading: true});
    expect(activityLog(INITIAL_STATE, {type: 'DEFAULT'})).toEqual(
      INITIAL_STATE,
    );
    expect(activityLog(null, {type: 'DEFAULT'})).toEqual(null);
  });
});
