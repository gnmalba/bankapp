import {getActivityLogRequested} from '../actions';

describe('actions', () => {
  test('actions', () => {
    expect(getActivityLogRequested(1, 5)).toEqual({
      type: 'GET_ACTIVITY_LOG_REQUESTED',
      payload: {
        page: 1,
        size: 5,
      },
    });
  });
});
