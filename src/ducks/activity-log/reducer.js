/**
 * @flow
 */

import type {ActivityLogState, ActivityLogAction} from './types';

export const INITIAL_STATE: ActivityLogState = {
  data: [],
  loading: false,
};

export const activityLog = (
  state: ActivityLogState = INITIAL_STATE,
  action: ActivityLogAction,
): ActivityLogState => {
  if (action.type === 'GET_ACTIVITY_LOG_REQUESTED') {
    return {
      ...state,
      loading: true,
    };
  }
  return state;
};
