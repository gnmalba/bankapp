/**
 * @flow
 */

import {takeLatest} from 'redux-saga/effects';

export default function* watcher(): Generator<any, void, any> {
  yield takeLatest('GET_ACTIVITY_LOG_REQUESTED');
}
