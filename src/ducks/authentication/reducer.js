// @flow

import type {AuthenticationType} from '../types';
import * as A from './actionTypes';

export const INITIAL_STATE: AuthenticationType = {
  loading: false,
  username: '',
  usernameError: '',
  password: '',
  passwordError: '',
  passcode: '',
  passcodeError: '',
  encryptionPublicKey: '',
  authenticated: false,
  accessToken: '',
  refreshToken: '',
  data: {},
  mfaToken: '',
  proceedToOtp: false,
  error: '',
  showModal: false,
  proceedToFullPageError: false,
  modalPrimaryBtnAction: {
    type: A.AUTHENTICATION_UPDATE_STATE,
    newState: {
      showModal: false,
      username: '',
      password: '',
    },
  },
  showPrimaryModalBtn: true,
  modalSecondaryBtnAction: {
    type: A.AUTHENTICATION_UPDATE_STATE,
    newState: {
      showModal: false,
    },
  },
  showSecondaryModalBtn: false,
  proceedToBioRegistration: false,
  showSnackbar: false,
  snackbarMessage: '',
  onboarded: true,
  modalPrimaryBtnText: '',
  modalSecondaryBtnText: '',
  showLogoutModal: false,
  anotherDeviceLogin: false,
  anotherSessionClosed: false,
  isFirstLogin: false,
  rememberThisDevice: false,
  numberOfOtpRequests: 0,
  inactivityCounter: 180,
  tokenExpiry: 9999999999999,
  loginMethod: 'password',
  signedToken: '',
};

export const getCustomerId = (state: {
  authentication: AuthenticationType,
}): number => state.authentication.data?.customerId;

const authentication = (
  state: AuthenticationType = INITIAL_STATE,
  action: Object = {},
): AuthenticationType => {
  switch (action.type) {
    case A.AUTHENTICATION_SET_LOADING:
      return {
        ...state,
        loading: action.loading,
      };
    case A.AUTHENTICATION_COUNT_DOWN:
      return {
        ...state,
        inactivityCounter: state.inactivityCounter - 1,
      };
    case A.AUTHENTICATION_SET_USERNAME:
      return {
        ...state,
        username: action.username,
        error: '',
      };
    case A.AUTHENTICATION_TOGGLE_REMEMBER_THIS_DEVICE:
      return {
        ...state,
        rememberThisDevice: !state.rememberThisDevice,
      };
    case A.AUTHENTICATION_INCREMENT_NUMBER_OF_OTP_REQUESTS:
      return {
        ...state,
        numberOfOtpRequests: state.numberOfOtpRequests + 1,
      };
    case A.AUTHENTICATION_SET_PASSWORD:
      return {
        ...state,
        password: action.password,
        error: '',
      };
    case A.AUTHENTICATION_UPDATE_STATE:
      return {
        ...state,
        ...action.newState,
      };
    case A.AUTHENTICATION_LOGIN_FAILED:
      return {
        ...state,
        authenticated: false,
        loading: false,
        ...action.additionalState,
        loginMethod: 'password',
        signedToken: '',
      };
    case A.AUTHENTICATION_LOGOUT:
      return {
        ...INITIAL_STATE,
        onboarded: state.onboarded,
        showSnackbar: true,
        snackbarMessage: 'You have successfully logged out.',
      };
    case A.AUTHENTICATION_ANOTHER_DEVICE_LOGIN:
      return {
        ...INITIAL_STATE,
        onboarded: state.onboarded,
        showModal: true,
        error:
          'You’ve been logout because another session started on a different device.',
        showPrimaryModalBtn: false,
        showSecondaryModalBtn: true,
        modalSecondaryBtnText: 'Okay',
      };
    case A.AUTHENTICATION_INACTIVITY_LOGOUT:
      return {
        ...INITIAL_STATE,
        onboarded: state.onboarded,
        showPrimaryModalBtn: false,
        showSecondaryModalBtn: true,
        showModal: true,
        error: 'For your security, you have been logged out due to inactivity.',
        modalSecondaryBtnText: 'Okay',
      };
    default:
      return state;
  }
};

export default authentication;
