/**
 * @flow
 */

import * as T from './actionTypes';

export const getPublicKey = (): any => ({
  type: T.AUTHENTICATION_GET_PUBLIC_KEY,
});

export const getOnBoarded = (): any => ({
  type: T.AUTHENTICATION_GET_ONBOARDED,
});
