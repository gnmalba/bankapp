/**
 * @flow
 */

import {put, call} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnGetPublicKey as getPublicKey} from '@ducks/helpers/authentication';

export function* fnGetPublicKey(): Generator<any, any, any> {
  try {
    const {data} = yield call(getPublicKey);
    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        encryptionPublicKey: data.data,
      },
    });
  } catch (error) {
    yield put({
      type: types.AUTHENTICATION_LOGIN_FAILED,
      additionalState: {},
    });
  }
}

export default fnGetPublicKey;
