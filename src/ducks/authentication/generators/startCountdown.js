/**
 * @flow
 */

import {put, select} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';

export function* fnStartCountdown({payload}: Object): Generator<any, any, any> {
  try {
    const inactivityCounter = yield select(
      (state) => state.authentication.inactivityCounter,
    );
    if (inactivityCounter === 0) {
      yield put({
        type: types.AUTHENTICATION_START_LOGOUT,
        payload: {
          onSuccess: types.AUTHENTICATION_INACTIVITY_LOGOUT,
        },
      });
    } else {
      yield put({
        type: types.AUTHENTICATION_COUNT_DOWN,
      });
    }
  } catch (error) {}
}

export default fnStartCountdown;
