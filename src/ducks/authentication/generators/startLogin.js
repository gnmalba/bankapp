/**
 * @flow
 */

import {put, call} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnGetLogin, fnGetPublicKey} from '@ducks/helpers/authentication';
import {
  INVALID_USER_CREDENTIALS,
  ACCOUNT_LOCKED,
  INVALID_USER_CREDENTIALS_MSG,
} from '@utils/constants';

export function* fnStartLogin({payload}: Object): Generator<any, any, any> {
  yield put({
    type: types.AUTHENTICATION_SET_LOADING,
    loading: true,
  });
  try {
    const res = yield call(fnGetPublicKey);
    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        encryptionPublicKey: res.data.data,
      },
    });
    const {data} = yield call(fnGetLogin, {
      ...payload,
      encryptionPublicKey: res.data.data,
    });
    yield put({
      type: types.AUTHENTICATION_LOGIN_SUCCESS,
      payload: {...payload, data, encryptionPublicKey: res.data.data},
    });
  } catch (error) {
    yield put({
      type: types.AUTHENTICATION_SET_LOADING,
      loading: false,
    });
    const err = error.response?.data?.error;
    const mfa = error.response?.data?.mfa_token;
    const desc = error.response?.data?.error_description;
    const common = {
      showModal: true,
      showPrimaryModalBtn: false,
      showSecondaryModalBtn: true,
      modalSecondaryBtnText: 'Okay',
    };
    switch (err) {
      case 'invalid_grant_otp_required':
        yield put({
          type: types.AUTHENTICATION_UPDATE_STATE,
          newState: {
            mfaToken: mfa,
            proceedToOtp: true,
          },
        });
        break;
      case 'invalid_grant_otp_invalid':
        yield put({
          type: types.AUTHENTICATION_UPDATE_STATE,
          newState: {
            mfaToken: mfa,
            proceedToOtp: false,
            passcodeError: desc,
          },
        });
        break;
      case 'invalid_grant':
        switch (desc) {
          case 'Your account is temporarily locked for your security. Try again later.':
            yield put({
              type: types.AUTHENTICATION_LOGIN_FAILED,
              additionalState: {
                error: ACCOUNT_LOCKED,
                ...common,
              },
            });
            break;
          case INVALID_USER_CREDENTIALS:
            yield put({
              type: types.AUTHENTICATION_LOGIN_FAILED,
              additionalState: {
                error: INVALID_USER_CREDENTIALS_MSG,
                ...common,
                modalSecondaryBtnText: 'Try again',
              },
            });
            break;
          default:
            yield put({
              type: types.AUTHENTICATION_LOGIN_FAILED,
              additionalState: {
                error: desc,
                ...common,
              },
            });
            break;
        }
        break;
      default:
        yield put({
          type: types.AUTHENTICATION_LOGIN_FAILED,
          additionalState: {
            error: desc || error.message,
            showModal: false,
            proceedToFullPageError: true,
          },
        });
        break;
    }
  }
}

export default fnStartLogin;
