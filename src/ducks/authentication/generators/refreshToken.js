/**
 * @flow
 */

import {put, call, select} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnGetLogin} from '@ducks/helpers/authentication';

export function* fnRefreshToken({payload}: Object): Generator<any, any, any> {
  try {
    const authentication = yield select((state) => state.authentication);
    const {
      username,
      password,
      encryptionPublicKey,
      refreshToken,
      tokenExpiry,
    } = authentication;
    const expired = tokenExpiry - Date.now() <= 65000;
    const {noExpiryChecking} = payload || {};
    if (refreshToken && (expired || noExpiryChecking)) {
      const res = yield call(fnGetLogin, {
        username,
        password,
        encryptionPublicKey,
        refreshToken,
        grantType: 'refresh_token',
      });
      const {data} = res;
      yield put({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {
          accessToken: data.access_token,
          refreshToken: data.refresh_token,
          tokenExpiry: new Date(Date.now() + 1000 * data.expires_in).valueOf(),
        },
      });
    }
  } catch (error) {
    const desc = error.response?.data?.error_description;
    switch (desc) {
      case 'Session not active':
        yield put({
          type: types.AUTHENTICATION_START_LOGOUT,
          payload: {
            onSuccess: types.AUTHENTICATION_ANOTHER_DEVICE_LOGIN,
          },
        });
        break;
      case 'Invalid refresh token':
      default:
        yield put({
          type: types.AUTHENTICATION_LOGIN_FAILED,
          additionalState: {
            error: desc || error.message,
            showModal: true,
            proceedToFullPageError: true,
            showPrimaryModalBtn: false,
            showSecondaryModalBtn: true,
            modalSecondaryBtnText: 'Okay',
          },
        });
    }
  }
}

export default fnRefreshToken;
