/**
 * @flow
 */

import {put, call} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnGetLogin, fnGetPublicKey} from '@ducks/helpers/authentication';

export function* fnSubmitOtp({payload}: Object): Generator<any, any, any> {
  yield put({
    type: types.AUTHENTICATION_SET_LOADING,
    loading: true,
  });
  try {
    const res = yield call(fnGetPublicKey);
    const {data} = yield call(fnGetLogin, {
      ...payload,
      encryptionPublicKey: res.data.data,
    });
    if (payload.rememberMe === 'true') {
      yield put({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {
          showSnackbar: true,
          snackbarMessage: 'Your device has been successfully linked.',
        },
      });
    }
    yield put({
      type: types.AUTHENTICATION_LOGIN_SUCCESS,
      payload: {...payload, data},
    });
  } catch (error) {
    const err = error.response?.data?.error;
    const desc = error.response?.data?.error_description;
    const genericMsg = error.message;
    switch (err) {
      case 'invalid_grant_otp_invalid_exceeded':
        yield put({
          type: types.AUTHENTICATION_LOGIN_FAILED,
          additionalState: {
            error: desc,
            showModal: false,
            proceedToFullPageError: true,
          },
        });
        break;
      case 'invalid_grant_otp_invalid':
        yield put({
          type: types.AUTHENTICATION_LOGIN_FAILED,
          additionalState: {
            passcodeError: desc,
          },
        });
        break;
      default:
        yield put({
          type: types.AUTHENTICATION_LOGIN_FAILED,
          additionalState: {
            error: desc || genericMsg,
            showModal: false,
            proceedToFullPageError: true,
          },
        });
        break;
    }
  }
}

export default fnSubmitOtp;
