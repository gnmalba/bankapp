/**
 * @flow
 */

import {put, call} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnGetCustomerInfo} from '@ducks/helpers/authentication';
import jwt_decode from 'jwt-decode';

function* fnLoginSuccess({payload}: Object): Generator<any, any, any> {
  try {
    const {data} = payload;
    const cxInfoResponse = yield call(fnGetCustomerInfo, {
      username: payload.username,
      accessToken: data.access_token,
    });
    const decodedInfo = yield jwt_decode(data.access_token);
    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        data: cxInfoResponse.data.data,
        loading: false,
        authenticated: true,
        accessToken: data.access_token,
        refreshToken: data.refresh_token,
        tokenExpiry: new Date(Date.now() + 1000 * data.expires_in).valueOf(),
        password: '',
        usernameError: '',
        passwordError: '',
        anotherSessionClosed: decodedInfo.ks === 1,
        encryptionPublicKey: payload.encryptionPublicKey,
        loginMethod: 'password',
        signedToken: '',
      },
    });
    yield put({type: 'GET_CUSTOMER_INFO_SUCCESS'});
  } catch (error) {
    yield put({
      type: types.AUTHENTICATION_LOGIN_FAILED,
      additionalState: {
        proceedToFullPageError: true,
        error: error.message,
      },
    });
  }
}

export default fnLoginSuccess;
