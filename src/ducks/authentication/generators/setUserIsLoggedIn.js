/**
 * @flow
 */

import AsyncStorage from '@react-native-async-storage/async-storage';

import {put} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';

export function* setUserIsLoggedIn(): Generator<any, any, any> {
  try {
    yield AsyncStorage.setItem('firstLogin', 'true');

    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {isFirstLogin: false},
    });
  } catch (error) {
    console.log('error in setUserIsLoggedIn: ', error);
  }
}

export default setUserIsLoggedIn;
