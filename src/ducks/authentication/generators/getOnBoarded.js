/**
 * @flow
 */

import {put, call} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnGetOnBoarded as getOnBoarded} from '@ducks/helpers/authentication';

export function* fnGetOnBoarded(): Generator<any, any, any> {
  try {
    const onBoarded = yield call(getOnBoarded);
    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        onboarded: onBoarded === 'true',
      },
    });
  } catch (error) {
    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        onboarded: false,
      },
    });
  }
}

export default fnGetOnBoarded;
