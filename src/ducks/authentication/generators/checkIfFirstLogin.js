/**
 * @flow
 */

import AsyncStorage from '@react-native-async-storage/async-storage';

import {put} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';

export function* checkIfFirstLogin(): Generator<any, any, any> {
  try {
    const value = yield AsyncStorage.getItem('firstLogin');

    if (!value) {
      yield put({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {isFirstLogin: true},
      });
    }
  } catch (error) {
    console.log('error in checkIfFirstLogin: ', error);
  }
}

export default checkIfFirstLogin;
