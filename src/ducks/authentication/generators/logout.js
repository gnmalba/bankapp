/**
 * @flow
 */

import {put, call, select} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {fnLogout} from '@ducks/helpers/authentication';

export function* fnStartLogout({payload}: Object): Generator<any, any, any> {
  const authentication = yield select((state) => state.authentication);
  const {onSuccess, onFailure} = payload || {};
  const {refreshToken} = authentication;
  try {
    yield put({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        loading: true,
      },
    });
    yield call(fnLogout, {
      refreshToken,
    });
    yield put({
      type: onSuccess || types.AUTHENTICATION_LOGOUT,
    });
  } catch (error) {
    yield put({
      type: onFailure || types.AUTHENTICATION_LOGOUT,
    });
  } finally {
    yield put({
      type: 'CREDIT_LOGOUT_RESET_STATE',
    });
    yield put({
      type: 'DEPOSIT_LOGOUT_RESET_STATE',
    });
    yield put({
      type: 'DEPOSITPREPAIDOVERVIEW_LOGOUT_RESET_STATE',
    });
    yield put({
      type: 'ENROLLMENT_LOGOUT_RESET_STATE',
    });
    yield put({
      type: 'LOSTCARD_LOGOUT_RESET_STATE',
    });
    yield put({
      type: 'YAZZ_LOGOUT_RESET_STATE',
    });
    yield put({
      type: 'INSTALLMENTS__RESET_STATE',
    });
    yield put({
      type: 'SHORTCUTS_LOGOUT_RESET_STATE',
    });

    yield put({
      type: 'RESET_WIDGETS_CONFIG',
    });
  }
}

export default fnStartLogout;
