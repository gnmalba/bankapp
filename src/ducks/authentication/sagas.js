/**
 * @flow
 */

import {takeLatest} from 'redux-saga/effects';
import * as types from '@ducks/authentication/actionTypes';
import {
  fnStartLogin,
  fnSubmitOtp,
  fnLoginSuccess,
  fnGetPublicKey,
  fnGetOnBoarded,
  fnRefreshToken,
  fnStartLogout,
  fnStartCountdown,
  setUserIsLoggedIn,
  checkIfFirstLogin,
} from './generators';

export default function* watcher(): any {
  yield takeLatest(types.AUTHENTICATION_START_LOGIN, fnStartLogin);
  yield takeLatest(types.AUTHENTICATION_SUBMIT_OTP, fnSubmitOtp);
  yield takeLatest(types.AUTHENTICATION_LOGIN_SUCCESS, fnLoginSuccess);
  yield takeLatest(types.AUTHENTICATION_GET_PUBLIC_KEY, fnGetPublicKey);
  yield takeLatest(types.AUTHENTICATION_GET_ONBOARDED, fnGetOnBoarded);
  yield takeLatest(types.AUTHENTICATION_REFRESH_TOKEN, fnRefreshToken);
  yield takeLatest(types.AUTHENTICATION_START_LOGOUT, fnStartLogout);
  yield takeLatest(types.AUTHENTICATION_START_COUNTDOWN, fnStartCountdown);
  yield takeLatest(types.AUTHENTICATION_START_LOGIN, checkIfFirstLogin);
  yield takeLatest('UPDATE_GUIDE_CUSTOMIZE_WIDGETS', setUserIsLoggedIn);
}
