import {expectSaga} from 'redux-saga-test-plan';
import fnSetUserIsLoggedIn from '../generators/setUserIsLoggedIn';

jest.mock('@react-native-async-storage/async-storage', () => 'View');

describe('checkIfFirstLogin generator', () => {
  test('fnSetUserIsLoggedIn should work', () => {
    return expectSaga(fnSetUserIsLoggedIn).run();
  });
});
