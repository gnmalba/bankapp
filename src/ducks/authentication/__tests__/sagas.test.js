import watcher from '../sagas';
import {expectSaga} from 'redux-saga-test-plan';

describe('saga', () => {
  test('saga', () => {
    return expectSaga(watcher).run();
  });
});
