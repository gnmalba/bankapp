import {expectSaga} from 'redux-saga-test-plan';
import fnStartCountdown from '../generators/startCountdown';

const payload = {};

const initialState = {
  authentication: {
    inactivityCounter: 0,
  },
};

function reducer(state = initialState, action = {}) {
  return state;
}

describe('fnStartCountdown generator', () => {
  test('fnStartCountdown should work', () => {
    return expectSaga(fnStartCountdown, {payload}).withReducer(reducer).run();
  });
});
