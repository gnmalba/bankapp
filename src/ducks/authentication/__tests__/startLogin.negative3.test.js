import {expectSaga} from 'redux-saga-test-plan';
import fnStartLogin from '../generators/startLogin';

const payload = {};

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetPublicKey: () => ({
    data: {
      data: 'test',
    },
  }),
  fnGetLogin: async () =>
    Promise.reject({
      response: {
        data: {
          error: 'invalid_grant',
          mfa_token: 'test',
          error_description:
            'Your account is temporarily locked for your security. Try again later.',
        },
      },
    }),
}));

describe('fnStartLogin generator', () => {
  test('fnStartLogin should work', () => {
    return expectSaga(fnStartLogin, {payload}).run();
  });
});
