import {expectSaga} from 'redux-saga-test-plan';
import fnRefreshToken from '../generators/refreshToken';

const payload = {
  noExpiryChecking: true,
};

const initialState = {
  authentication: {
    refreshToken: 'test',
  },
};

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetLogin: () => ({
    data: {
      access_token: 'test',
      refresh_token: 'test',
      expires_in: 300,
    },
  }),
}));

function reducer(state = initialState, action = {}) {
  return state;
}

describe('fnRefreshToken generator', () => {
  test('fnRefreshToken should work', () => {
    return expectSaga(fnRefreshToken, {payload}).withReducer(reducer).run();
  });
});
