import {expectSaga} from 'redux-saga-test-plan';
import fnSetUserIsLoggedIn from '../generators/setUserIsLoggedIn';
import * as types from '@ducks/authentication/actionTypes';

describe('fnSetUserIsLoggedIn generator', () => {
  test('fnSetUserIsLoggedIn should work', () => {
    return expectSaga(fnSetUserIsLoggedIn)
      .put({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {isFirstLogin: false},
      })
      .run();
  });
});
