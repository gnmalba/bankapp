import {expectSaga} from 'redux-saga-test-plan';
import fnGetPublicKey from '../generators/getPublicKey';
import * as types from '@ducks/authentication/actionTypes';

jest.mock('@ducks/helpers/authentication', () => Promise.reject());

describe('fnGetPublicKey generator', () => {
  test('fnGetPublicKey should work', () => {
    return expectSaga(fnGetPublicKey)
      .put({
        type: types.AUTHENTICATION_LOGIN_FAILED,
        additionalState: {},
      })
      .run();
  });
});
