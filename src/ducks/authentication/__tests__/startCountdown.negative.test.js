import {expectSaga} from 'redux-saga-test-plan';
import fnStartCountdown from '../generators/startCountdown';
import * as types from '@ducks/authentication/actionTypes';

const payload = {};

const initialState = {
  authentication: {
    inactivityCounter: 20,
  },
};

function reducer(state = initialState, action = {}) {
  return state;
}

describe('fnStartCountdown generator', () => {
  test('fnStartCountdown should work', () => {
    return expectSaga(fnStartCountdown, {payload})
      .withReducer(reducer)
      .put({
        type: types.AUTHENTICATION_COUNT_DOWN,
      })
      .run();
  });
});
