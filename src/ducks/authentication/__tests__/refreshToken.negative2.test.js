import {expectSaga} from 'redux-saga-test-plan';
import fnRefreshToken from '../generators/refreshToken';

const payload = {
  noExpiryChecking: true,
};

const initialState = {
  authentication: {
    refreshToken: 'test',
  },
};

function reducer(state = initialState, action = {}) {
  return state;
}

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetLogin: async () =>
    Promise.reject({
      response: {data: {error_description: 'Invalid refresh token'}},
    }),
}));

describe('fnRefreshToken generator', () => {
  test('fnRefreshToken should work', () => {
    return expectSaga(fnRefreshToken, {payload}).withReducer(reducer).run();
  });
});
