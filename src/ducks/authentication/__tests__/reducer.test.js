import authentication, {getCustomerId, INITIAL_STATE} from '../reducer';
import * as A from '../actionTypes';

describe('reducer', () => {
  test('reducer', () => {
    expect(getCustomerId({authentication: {data: {customerId: 1111}}})).toEqual(
      1111,
    );
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_SET_LOADING,
        loading: true,
      }),
    ).toEqual({...INITIAL_STATE, loading: true});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_COUNT_DOWN,
      }),
    ).toEqual({...INITIAL_STATE, inactivityCounter: 179});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_SET_USERNAME,
        username: 'test',
      }),
    ).toEqual({...INITIAL_STATE, username: 'test', error: ''});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_TOGGLE_REMEMBER_THIS_DEVICE,
      }),
    ).toEqual({...INITIAL_STATE, rememberThisDevice: true});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_INCREMENT_NUMBER_OF_OTP_REQUESTS,
      }),
    ).toEqual({...INITIAL_STATE, numberOfOtpRequests: 1});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_SET_PASSWORD,
        password: 'test',
      }),
    ).toEqual({...INITIAL_STATE, password: 'test', error: ''});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_UPDATE_STATE,
        newState: {},
      }),
    ).toEqual({...INITIAL_STATE});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_LOGIN_FAILED,
      }),
    ).toEqual({...INITIAL_STATE, authenticated: false, loading: false});
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_LOGOUT,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      showSnackbar: true,
      snackbarMessage: 'You have successfully logged out.',
    });
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_ANOTHER_DEVICE_LOGIN,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      showModal: true,
      error:
        'You’ve been logout because another session started on a different device.',
      showPrimaryModalBtn: false,
      showSecondaryModalBtn: true,
      modalSecondaryBtnText: 'Okay',
    });
    expect(
      authentication(INITIAL_STATE, {
        type: A.AUTHENTICATION_INACTIVITY_LOGOUT,
      }),
    ).toEqual({
      ...INITIAL_STATE,
      showPrimaryModalBtn: false,
      showSecondaryModalBtn: true,
      showModal: true,
      error: 'For your security, you have been logged out due to inactivity.',
      modalSecondaryBtnText: 'Okay',
    });
    expect(
      authentication(INITIAL_STATE, {
        type: 'DEFAULT',
      }),
    ).toEqual(INITIAL_STATE);
    expect(authentication()).toEqual(INITIAL_STATE);
  });
});
