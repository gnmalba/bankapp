import {expectSaga} from 'redux-saga-test-plan';
import fnLoginSuccess from '../generators/loginSuccess';

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetCustomerInfo: () => ({
    data: {
      data: 'test',
    },
  }),
}));

jest.mock('jwt-decode', () => {
  return (value) => {
    return {
      ks: 1,
    };
  };
});

const payload = {
  data: {
    access_token: 'test',
    expires_in: 300,
  },
  encryptionPublicKey: 'test',
};

describe('fnLoginSuccess generator', () => {
  test('fnLoginSuccess should work', () => {
    return expectSaga(fnLoginSuccess, {payload}).run();
  });
});
