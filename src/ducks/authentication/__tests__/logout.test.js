import {expectSaga} from 'redux-saga-test-plan';
import fnLogout from '../generators/logout';

const payload = {
  onSuccess: 'AUTHENTICATION_LOGOUT',
  onFailure: 'AUTHENTICATION_LOGOUT',
};

const initialState = {
  authentication: {
    refreshToken: '',
  },
};

jest.mock('@ducks/helpers/authentication', () => ({
  fnLogout: () => ({}),
}));

function reducer(state = initialState, action = {}) {
  return state;
}

describe('fnLogout generator', () => {
  test('fnLogout should work', () => {
    return expectSaga(fnLogout, {payload})
      .withReducer(reducer)
      .put({
        type: 'AUTHENTICATION_LOGOUT',
      })
      .run();
  });
});
