import {getPublicKey, getOnBoarded} from '../actions';
import * as T from '../actionTypes';

describe('actions', () => {
  test('actions', () => {
    expect(getPublicKey()).toEqual({
      type: T.AUTHENTICATION_GET_PUBLIC_KEY,
    });
    expect(getOnBoarded()).toEqual({
      type: T.AUTHENTICATION_GET_ONBOARDED,
    });
  });
});
