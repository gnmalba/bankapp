import {expectSaga} from 'redux-saga-test-plan';
import fnSubmitOtp from '../generators/submitOtp';

const payload = {
  rememberMe: 'true',
};

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetPublicKey: () => ({
    data: {
      data: 'test',
    },
  }),
  fnGetLogin: () =>
    Promise.reject({
      response: {
        data: {
          error: 'invalid_grant_otp_invalid',
          desc: 'test',
        },
      },
    }),
}));

describe('fnSubmitOtp generator', () => {
  test('fnSubmitOtp should work', () => {
    return expectSaga(fnSubmitOtp, {payload}).run();
  });
});
