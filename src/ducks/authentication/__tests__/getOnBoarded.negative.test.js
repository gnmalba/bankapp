import {expectSaga} from 'redux-saga-test-plan';
import fnGetOnBoarded from '../generators/getOnBoarded';
import * as types from '@ducks/authentication/actionTypes';

jest.mock('@ducks/helpers/authentication', () => 'View');

describe('fnGetOnBoarded generator', () => {
  test('fnGetOnBoarded should work', () => {
    return expectSaga(fnGetOnBoarded)
      .put({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {
          onboarded: false,
        },
      })
      .run();
  });
});
