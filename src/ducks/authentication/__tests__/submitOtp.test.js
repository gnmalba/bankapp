import {expectSaga} from 'redux-saga-test-plan';
import fnSubmitOtp from '../generators/submitOtp';

const payload = {
  rememberMe: 'true',
};

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetPublicKey: () => ({
    data: {
      data: 'test',
    },
  }),
  fnGetLogin: () => ({
    data: {
      access_token: 'test',
      refresh_token: 'test',
      expires_in: 300,
    },
  }),
}));

describe('fnSubmitOtp generator', () => {
  test('fnSubmitOtp should work', () => {
    return expectSaga(fnSubmitOtp, {payload}).run();
  });
});
