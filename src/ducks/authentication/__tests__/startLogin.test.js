import {expectSaga} from 'redux-saga-test-plan';
import fnStartLogin from '../generators/startLogin';

const payload = {};

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetPublicKey: () => ({
    data: {
      data: 'test',
    },
  }),
  fnGetLogin: () => ({
    data: {
      access_token: 'test',
      refresh_token: 'test',
      expires_in: 300,
    },
  }),
}));

describe('fnStartLogin generator', () => {
  test('fnStartLogin should work', () => {
    return expectSaga(fnStartLogin, {payload}).run();
  });
});
