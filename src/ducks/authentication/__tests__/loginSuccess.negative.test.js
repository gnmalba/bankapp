import {expectSaga} from 'redux-saga-test-plan';
import fnLoginSuccess from '../generators/loginSuccess';

describe('fnLoginSuccess generator', () => {
  test('fnLoginSuccess should work', () => {
    return expectSaga(fnLoginSuccess, {}).run();
  });
});
