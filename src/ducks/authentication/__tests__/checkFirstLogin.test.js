import {expectSaga} from 'redux-saga-test-plan';
import checkIfFirstLogin from '../generators/checkIfFirstLogin';
import * as types from '@ducks/authentication/actionTypes';

describe('checkIfFirstLogin generator', () => {
  test('checkIfFirstLogin should work', () => {
    return expectSaga(checkIfFirstLogin)
      .put({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {isFirstLogin: true},
      })
      .run();
  });
});
