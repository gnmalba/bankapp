import {expectSaga} from 'redux-saga-test-plan';
import checkIfFirstLogin from '../generators/checkIfFirstLogin';

jest.mock('@react-native-async-storage/async-storage', () => ({
  getItem: (key) => key,
}));

describe('checkIfFirstLogin generator', () => {
  test('checkIfFirstLogin should work', () => {
    return expectSaga(checkIfFirstLogin).run();
  });
});
