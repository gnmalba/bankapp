// @flow

export type AuthAction = {
  type: string,
  loading?: boolean,
  username?: string,
  usernameError?: string,
  password?: string,
  passwordError?: string,
  encryptionPublicKey?: string,
  authenticated?: boolean,
};
