/**
 * @flow
 */

import {
  takeLatest,
  call,
  put,
  all,
  select,
  spawn,
  cancel,
} from 'redux-saga/effects';

import DepositConfig from '@api/services/depositService';

import {getCustomerID} from '@ducks/commonSelectors';
import {getDepositRequested, getDepositSuccess} from './actions';
import {updateWidgetLoadingState} from '@ducks/config/actions';
import {
  addPesoAccountByType,
  addForeignAccountByType,
  validateSelectedCurrencyByAccountType,
} from '@ducks/accounts-displayoptions/actions';

import type {
  PesoAccountResponse,
  ForeignAccountResponse,
  Deposits,
  MyAccounts,
} from './types';

import {
  responseArrayToObjectArray,
  getDepositOrPrepaid,
  getDepositPrimaryAccount,
  buildDespositForMyAccounts,
} from './utils';

function* getPesoAccounts(
  customerId,
): Generator<any, PesoAccountResponse, any> {
  const returnedResultsets = [];
  try {
    const response = yield call(DepositConfig.getPesoBalance, customerId);

    return response.data.data;
  } catch (apiErrorResponse) {
    return returnedResultsets;
  }
}

function* getForeignAccounts(
  customerId,
): Generator<any, ForeignAccountResponse, any> {
  const returnedResultsets = [];
  try {
    const response = yield call(DepositConfig.getForeignBalance, customerId);

    return response.data.data;
  } catch (apiErrorResponse) {
    return returnedResultsets;
  }
}

function* addPesoAndForeignAccount(
  accounts: MyAccounts,
  accountType: 'deposit' | 'prepaid',
) {
  if (accounts.length === 0) {
    // if we don't have deposit/prepaids accounts, terminate this generator
    yield cancel();
  }

  const pesoAccount: string = 'PHP';

  for (let i = 0; i < accounts.length; i++) {
    const account = accounts[i];

    if (account.currencyCode === pesoAccount) {
      yield put(addPesoAccountByType(account.id, accountType));
    } else {
      yield put(addForeignAccountByType(account.id, accountType));
    }
  }

  yield put(validateSelectedCurrencyByAccountType(accountType));
}

export function* processDepositAndMbPrepaid(): Generator<any, any, any> {
  const customerId = yield select(getCustomerID);

  yield put(getDepositRequested());
  yield put(updateWidgetLoadingState(1, true));
  yield put(updateWidgetLoadingState(4, true));

  const [pesoAccountsResponse, foreignAccountsResponse] = yield all([
    call(getPesoAccounts, customerId),
    call(getForeignAccounts, customerId),
  ]);

  const pesoAccounts = responseArrayToObjectArray(pesoAccountsResponse);
  const foreignAccounts = responseArrayToObjectArray(foreignAccountsResponse);

  const depositAccounts: MyAccounts = getDepositOrPrepaid(
    pesoAccounts,
    foreignAccounts,
  );

  const deposits: Deposits = buildDespositForMyAccounts(depositAccounts);

  yield spawn(
    addPesoAndForeignAccount,
    [...deposits.savingsAndDeposits, ...deposits.timeDeposits],
    'deposit',
  );

  const prepaids: MyAccounts = getDepositOrPrepaid(
    pesoAccounts,
    foreignAccounts,
    true,
  );

  const prepaidPrimaryAccount = getDepositPrimaryAccount(prepaids);

  const prepaidsWithPrimary = prepaidPrimaryAccount
    ? [
        prepaidPrimaryAccount,
        ...prepaids.filter(
          (prepaid) => prepaid.id !== prepaidPrimaryAccount.id,
        ),
      ]
    : prepaids;

  yield spawn(addPesoAndForeignAccount, prepaidsWithPrimary, 'prepaid');

  yield put(getDepositSuccess(deposits, prepaidsWithPrimary));
}

function* depositCreditWatcher(): Generator<any, any, any> {
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', processDepositAndMbPrepaid);
  yield takeLatest('ENROLLMENT_DEPOSIT_SUCCESS', processDepositAndMbPrepaid);
}
export default depositCreditWatcher;
