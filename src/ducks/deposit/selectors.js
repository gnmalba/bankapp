/**
 * @flow
 */

import {createSelector} from 'reselect';
import type {State} from '@ducks/types';
import type {Deposits, DepositWidgets, Prepaids} from './types';

export const depositsSelector = (state: State): Deposits =>
  state.deposit.deposits;

export const getPrepaidFromDeposit = (state: State): Prepaids =>
  state.deposit.prepaids;

export const depositFetchingStatusSelector = (state: State): boolean =>
  state.deposit.fetching;

export const getDepositsForWidgets: DepositWidgets = createSelector(
  depositsSelector,
  (deposits) => [...deposits.savingsAndDeposits, ...deposits.timeDeposits],
);
