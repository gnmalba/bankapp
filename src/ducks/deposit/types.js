/**
 * @flow
 */

/**
 * Types for api response
 */

type Amount = {
  value: number,
  currencyCode: string,
};

type Limits = Array<{
  id: number,
  type: String,
  amount: Amount,
}>;

type Shortcut = {
  code: string,
  credit: boolean,
  debit: boolean,
  enabled: boolean,
  forShortcut: boolean,
  icon: string,
  id: number,
  name: string,
  limits?: Limits,
};

type AllowedTransaction = Array<{
  id: number,
  code: string,
  name: string,
  enabled: boolean,
  forShortcut: boolean,
  icon: string,
  limits?: Limits,
  debit: boolean,
  credit: boolean,
}>;

export type DepositAccount = {
  id: number,
  accountNo: string,
  customerId: string,
  productType: string,
  alias: string,
  currencyCode: string,
  currencyHostCode: string,
  initialAccount: boolean,
  accountType: 'Savings',
  accountTypeDescription: string,
  mobile: ?string,
  branch: string,
  relationshipCode: string,
  created: string,
  shortcuts: Array<Shortcut>,
  allowedTransactions: Array<AllowedTransaction>,
};

export type Account = {
  depositAccount: DepositAccount,
  availableBalance: Amount,
  currentBalance: Amount,
  active: boolean,
};

export type PesoAccountResponse = Array<Account>;

export type ForeignAccountResponse = Array<Account>;

/**
 * End of types for api response
 */

/**
 * Types for redux
 */

export type MyAccount = DepositAccount & {
  availableBalanceValueWithCurrency: string,
  active: boolean,
};

export type MyAccounts = Array<MyAccount>;

export type Deposits = {
  savingsAndDeposits: MyAccounts,
  timeDeposits: MyAccounts,
};

export type Prepaids = MyAccounts;

export type DepositWidget = MyAccount & {
  currentBalanceValueWithCurrency: string,
};

export type DepositWidgets = Array<DepositWidget>;

export type DepositState = {
  fetching: boolean,
  deposits: Deposits,
  prepaids: Prepaids,
};

export type DepositAction =
  | {
      type: 'GET_DEPOSIT_API_REQUESTED',
    }
  | {
      type: 'GET_DEPOSIT_API_SUCCESS',
      +payload: {
        data: {
          deposits: Deposits,
          prepaids: Prepaids,
        },
      },
    }
  | {
      type: 'GET_DEPOSIT_API_FAILURE',
    }
  | {
      type: 'DEPOSIT_LOGOUT_RESET_STATE',
    }
  | {
      type: 'UPDATE_SAVINGS_AND_DEPOSITS',
      payload: MyAccounts,
    }
  | {
      type: 'UPDATE_TIME_DEPOSITS',
      payload: MyAccounts,
    }
  | {
      type: 'UPDATE_PREPAIDS',
      payload: Prepaids,
    };

/**
 * End Types for redux
 */

export type PrimaryAccount = ?MyAccount;
