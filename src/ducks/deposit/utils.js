/**
 * @flow
 */

import type {
  MyAccount,
  MyAccounts,
  PrimaryAccount,
  Deposits,
  Account,
  DepositWidget,
} from './types';

export const numberToCurrencyFormat = (_number: number = 0): string => {
  if (!_number) {
    return '0.00';
  }

  const convertedNumberToString = _number.toString();

  const splittedNumberByComma = convertedNumberToString.split('.');

  // regex pattern to split string by 3
  const regexPattern = /\B(?=(\d{3})+(?!\d))/g;

  // If decimal is not equals to 00 then replace converted string to currency otherwise append 00
  if (typeof splittedNumberByComma[1] !== 'undefined') {
    return convertedNumberToString.replace(regexPattern, ',');
  }

  return `${splittedNumberByComma[0].replace(regexPattern, ',')}.00`;
};

export const numberToCurrencyFormatNoDecimals = (
  _number: number = 0,
): string => {
  if (!_number) {
    return '0.00';
  }

  const convertedNumberToString = _number.toString();

  const splittedNumberByComma = convertedNumberToString.split('.');

  // regex pattern to split string by 3
  const regexPattern = /\B(?=(\d{3})+(?!\d))/g;

  // If decimal is not equals to 00 then replace converted string to currency otherwise append 00
  if (typeof splittedNumberByComma[1] !== 'undefined') {
    return convertedNumberToString.replace(regexPattern, ',');
  }

  return `${splittedNumberByComma[0].replace(regexPattern, ',')}`;
};

const sortByObjectKey = (key: string) => (a, b) =>
  a[key] > b[key] ? 1 : b[key] > a[key] ? -1 : 0;

export const responseArrayToObjectArray = (
  response: Array<Account>,
): Array<MyAccount | DepositWidget> =>
  response.map((row) => {
    const despositAccount = row.depositAccount;
    const active = row.active;

    return {
      ...despositAccount,
      availableBalanceValueWithCurrency: active
        ? `${row.availableBalance.currencyCode} ${numberToCurrencyFormat(
            row.availableBalance.value,
          )}`
        : '',
      currentBalanceValueWithCurrency: active
        ? `${row.currentBalance.currencyCode} ${numberToCurrencyFormat(
            row.currentBalance.value,
          )}`
        : '',
      active: active,
    };
  });

export const isDepositAccount = (account: MyAccount): boolean =>
  account.productType !== '769';

export const isTimeDesposit = (account: MyAccount): boolean =>
  account.accountType === 'Time Deposit';

/**
 * Only get first primary account for deposit
 * @param {*} accounts
 * @returns Array<Account> | undefined
 */
export const getDepositPrimaryAccount = (
  accounts: MyAccounts,
): PrimaryAccount => accounts.find((account) => account.initialAccount);

/**
 * Build deposit account for savings or timedeposit under My Accounts component
 * @param {*} accounts
 * @returns { savingsAndDeposits = MyAccounts, timeDeposit = MyAccounts}
 */
export const buildDespositForMyAccounts = (accounts: MyAccounts): Deposits => {
  // get savings in sort by alias
  let savingsAndDeposits = accounts
    .filter((account) => !isTimeDesposit(account))
    .sort(sortByObjectKey('alias'));

  // get time desposit and sort by alias
  const timeDeposits = accounts
    .filter(isTimeDesposit)
    .sort(sortByObjectKey('alias'));

  return {
    savingsAndDeposits,
    timeDeposits,
  };
};

/**
 * Get all primary account for deposit or prepaid accounts using isPrepaidAccount flag
 * Only applicable for peso and foreign accounts
 * @param {*} pesoAccounts
 * @param {*} foreignAccounts
 * @param {*} isPrepaidAccount
 * @returns Array<Account>
 */
export const getDepositOrPrepaid = (
  pesoAccounts: MyAccounts,
  foreignAccounts: MyAccounts,
  isPrepaidAccount: boolean = false,
): MyAccounts => {
  const filterCondition = (account: MyAccount) =>
    isPrepaidAccount ? !isDepositAccount(account) : isDepositAccount(account);

  const filteredPesoAccounts: MyAccounts = pesoAccounts
    .filter(filterCondition)
    .sort(sortByObjectKey('alias'));
  const filteredForeignAccounts: MyAccounts = foreignAccounts
    .filter(filterCondition)
    .sort(sortByObjectKey('alias'));

  return [...filteredPesoAccounts, ...filteredForeignAccounts];
};

/**
 * Build widget for deposit and prepaid only
 * @param {*} primaryAccount
 * @param {*} accounts
 * @returns Array<Account>
 */
export const buildWidgetForDepositOrPrepaid = (
  primaryAccount: PrimaryAccount,
  accounts: MyAccounts,
): MyAccounts => {
  if (primaryAccount) {
    return [primaryAccount, ...accounts];
  }

  return accounts;
};
