/**
 * @flow
 */
import type {DepositState, DepositAction} from './types';

export const INITIAL_STATE: DepositState = {
  fetching: false,
  deposits: {
    savingsAndDeposits: [],
    timeDeposits: [],
  },
  prepaids: [],
};

const deposit = (
  state: DepositState = INITIAL_STATE,
  action: DepositAction,
): DepositState => {
  switch (action.type) {
    case 'GET_DEPOSIT_API_REQUESTED':
      return {
        ...state,
        fetching: true,
      };
    case 'GET_DEPOSIT_API_SUCCESS': {
      const {deposits, prepaids} = action.payload.data;
      return {
        ...state,
        deposits,
        prepaids,
        fetching: false,
      };
    }
    case 'GET_DEPOSIT_API_FAILURE':
      return {
        ...state,
        fetching: false,
      };

    case 'UPDATE_SAVINGS_AND_DEPOSITS':
      return {
        ...state,
        deposits: {
          ...state.deposits,
          savingsAndDeposits: action.payload,
        },
      };

    case 'UPDATE_PREPAIDS':
      return {
        ...state,
        prepaids: action.payload,
      };

    case 'UPDATE_TIME_DEPOSITS':
      return {
        ...state,
        deposits: {
          ...state.deposits,
          timeDeposits: action.payload,
        },
      };

    case 'DEPOSIT_LOGOUT_RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default deposit;
