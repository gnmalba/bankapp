import {processDepositAndMbPrepaid} from '../sagas';
import {expectSaga} from 'redux-saga-test-plan';
import reducer, {INITIAL_STATE} from '../reducer';
describe('Deposit and Credit Sagas', () => {
  let authenticate = {authentication: {data: {customerId: '2345'}}};
  test('Deposit and Credit Sagas ', async () => {
    return expectSaga(processDepositAndMbPrepaid)
      .withReducer(reducer)
      .withState(authenticate)
      .hasFinalState({
        ...INITIAL_STATE,
        fetching: false,
        deposits: {
          savingsAndDeposits: [],
          timeDeposits: [],
        },
        authentication: {data: {customerId: '2345'}},
        prepaids: [],
      })
      .run();
  });
});
