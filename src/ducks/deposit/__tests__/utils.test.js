import {
  numberToCurrencyFormat,
  numberToCurrencyFormatNoDecimals,
  isDepositAccount,
  isTimeDesposit,
} from '../utils';

describe('deposit actions', () => {
  it('handle number Currency format zero', () => {
    let expected = '0.00';
    let data = numberToCurrencyFormat(0);
    expect(data).toEqual(expected);
  });
  it('handle number Currency format hundreds', () => {
    let expected = '100.00';
    let data = numberToCurrencyFormat(100);
    expect(data).toEqual(expected);
  });
  it('handle number Currency format hundreds with special characters', () => {
    let expected = '1,000.00';
    let data = numberToCurrencyFormat('1000.00');
    expect(data).toEqual(expected);
  });
  it('handle number Currency format with no decimal places', () => {
    let expected = '0.00';
    let data = numberToCurrencyFormatNoDecimals(0);
    expect(data).toEqual(expected);
  });
  // it('handle number Currency format decimal with auto comma', () => {
  //   let expected = '1000';
  //   let data = numberToCurrencyFormatNoDecimals('1,000');
  //   expect(data).toEqual(expected);
  // });
  it('handle is Deposit account true', () => {
    let expected = true;
    let data = isDepositAccount({productType: '767'});
    expect(data).toEqual(expected);
  });
  it('handle is Deposit account false', () => {
    let expected = false;
    let data = isDepositAccount({productType: '769'});
    expect(data).toEqual(expected);
  });
  it('handle buildDespositForMyAccounts true', () => {
    let expected = true;
    let data = isTimeDesposit({accountType: 'Time Deposit'});
    expect(data).toEqual(expected);
  });
});
