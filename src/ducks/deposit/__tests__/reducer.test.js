import reducer, {INITIAL_STATE} from '../reducer';

describe('deposit reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  const payload = {
    data: {},
  };

  it('should handle GET_DEPOSIT_API_REQUESTED', () => {
    const action = {
      type: 'GET_DEPOSIT_API_REQUESTED',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      fetching: true,
    });
  });

  it('should handle GET_DEPOSIT_API_SUCCESS', () => {
    const action = {
      type: 'GET_DEPOSIT_API_SUCCESS',
      payload,
    };
    const {deposits, prepaids} = payload;
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      deposits,
      prepaids,
      fetching: false,
    });
  });

  it('should handle GET_DEPOSIT_API_FAILURE', () => {
    const action = {
      type: 'GET_DEPOSIT_API_FAILURE',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      fetching: false,
    });
  });

  it('should handle UPDATE_SAVINGS_AND_DEPOSITS', () => {
    const action = {
      type: 'UPDATE_SAVINGS_AND_DEPOSITS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      deposits: {
        ...INITIAL_STATE.deposits,
        savingsAndDeposits: payload,
      },
    });
  });

  it('should handle UPDATE_PREPAIDS', () => {
    const action = {
      type: 'UPDATE_PREPAIDS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      prepaids: payload,
    });
  });

  it('should handle UPDATE_TIME_DEPOSITS', () => {
    const action = {
      type: 'UPDATE_TIME_DEPOSITS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      deposits: {
        ...INITIAL_STATE.deposits,
        timeDeposits: payload,
      },
    });
  });

  it('should handle DEPOSIT_LOGOUT_RESET_STATE', () => {
    const action = {
      type: 'DEPOSIT_LOGOUT_RESET_STATE',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
    });
  });
});
