import * as actions from '../actions';

describe('deposit actions', () => {
  const payload = {
    data: {
      deposits: {},
      prepaids: {},
    },
  };

  it('handle deposit request', () => {
    const action = actions.getDepositRequested();
    const expectedAction = {
      type: 'GET_DEPOSIT_API_REQUESTED',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get deposit api success', () => {
    const action = actions.getDepositSuccess(
      payload.data.deposits,
      payload.data.prepaids,
    );
    const expectedAction = {
      type: 'GET_DEPOSIT_API_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle deposit api failure', () => {
    const action = actions.getDepositError();
    const expectedAction = {
      type: 'GET_DEPOSIT_API_FAILURE',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update savings and deposits', () => {
    const action = actions.updateSavingsAndDeposits(payload);
    const expectedAction = {
      type: 'UPDATE_SAVINGS_AND_DEPOSITS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update time deposits', () => {
    const action = actions.updateTimeDeposits(payload);
    const expectedAction = {
      type: 'UPDATE_TIME_DEPOSITS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update prepaid', () => {
    const action = actions.updatePrepaids(payload);
    const expectedAction = {
      type: 'UPDATE_PREPAIDS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});
