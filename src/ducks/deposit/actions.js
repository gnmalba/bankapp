/**
 * @flow
 */

import type {DepositAction, Deposits, Prepaids, MyAccounts} from './types';

export const getDepositRequested = (): DepositAction => ({
  type: 'GET_DEPOSIT_API_REQUESTED',
});

export const getDepositSuccess = (
  deposits: Deposits,
  prepaids: Prepaids,
): DepositAction => ({
  type: 'GET_DEPOSIT_API_SUCCESS',
  payload: {
    data: {deposits, prepaids},
  },
});

export const getDepositError = (): DepositAction => ({
  type: 'GET_DEPOSIT_API_FAILURE',
});

export const updateSavingsAndDeposits = (data: MyAccounts): DepositAction => ({
  type: 'UPDATE_SAVINGS_AND_DEPOSITS',
  payload: data,
});

export const updateTimeDeposits = (data: MyAccounts): DepositAction => ({
  type: 'UPDATE_TIME_DEPOSITS',
  payload: data,
});

export const updatePrepaids = (data: Prepaids): DepositAction => ({
  type: 'UPDATE_PREPAIDS',
  payload: data,
});
