/**
 * @flow
 */

import type {ContactsState} from './types';

const initialState = {
  contactsList: [],
  contactsTransactions: [],
  isLoading: false,
  isLoadingTXN: false,
  isSubmitting: false,
  isError: false,
  isErrorTXN: false,
  errorMessage: null,
  apiResponse: {status: null, error: null, loading: false, data: null},
  bankList: {data: [], loading: true, status: null},
};

const contacts = (
  state: any = initialState,
  action: Object = {},
): ContactsState => {
  switch (action.type) {
    case 'GET_CONTACTS_REQUEST':
      return {
        ...state,
        isLoading: true,
        isError: false,
        errorMessage: null,
      };
    case 'GET_CONTACTS_SUCCESS': {
      const {contactsList} = action.payload;
      return {
        ...state,
        contactsList: contactsList,
        isLoading: false,
        isError: false,
        errorMessage: null,
      };
    }
    case 'GET_CONTACTS_FAILED': {
      const {error, errorMessage} = action.payload;
      return {
        ...state,
        isLoading: false,
        isError: error,
        errorMessage: errorMessage,
      };
    }
    case 'GET_CONTACT_TRANSACTIONS_REQUEST':
      return {
        ...state,
        isLoadingTXN: true,
        isErrorTXN: false,
        errorMessage: null,
      };
    case 'GET_CONTACT_TRANSACTIONS_SUCCESS': {
      const {contactsTransactions} = action.payload;
      return {
        ...state,
        contactsTransactions: contactsTransactions,
        isLoadingTXN: false,
        isErrorTXN: false,
        errorMessage: null,
      };
    }
    case 'GET_CONTACT_TRANSACTIONS_FAILED': {
      const {error, errorMessage} = action.payload;
      return {
        ...state,
        isLoadingTXN: false,
        isError: error,
        errorMessage: errorMessage,
      };
    }
    case 'UPDATE_CONTACT_REQUEST_STATE':
      return {
        ...state,
        [action.payload.state]: action.payload.value,
      };
    case 'ADD_CONTACT_REQUESTED':
    case 'ADD_CONTACT_SUCCESS':
    case 'ADD_CONTACT_FAILED':
    case 'CLEAR_API_RESPONSE':
      return {
        ...state,
        apiResponse: {status: null, error: null, loading: false, data: null},
      };
    case 'GET_BANK_LIST':
    case 'EDIT_CONTACT_REQUESTED':
    case 'DELETE_CONTACT_REQUESTED':
    default:
      return state;
  }
};

export default contacts;
