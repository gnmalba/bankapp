import reducer from '../reducer';

describe('contacts reducer', () => {
  const INITIAL_STATE = {
    contactsList: [],
    contactsTransactions: [],
    isLoading: false,
    isLoadingTXN: false,
    isSubmitting: false,
    isError: false,
    isErrorTXN: false,
    errorMessage: null,
    apiResponse: {status: null, error: null, loading: false, data: null},
    bankList: {data: [], loading: true, status: null},
  };
  const payload = {
    data: {},
  };

  it('should handle GET_CONTACTS_REQUEST', () => {
    const action = {
      type: 'GET_CONTACTS_REQUEST',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoading: true,
      isError: false,
      errorMessage: null,
    });
  });

  it('should handle GET_CONTACTS_SUCCESS', () => {
    const action = {
      type: 'GET_CONTACTS_SUCCESS',
      payload: {
        contactsList: [],
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      contactsList: INITIAL_STATE.contactsList,
      isLoading: false,
      isError: false,
      errorMessage: null,
    });
  });

  it('should handle GET_CONTACTS_FAILED', () => {
    const action = {
      type: 'GET_CONTACTS_FAILED',
      payload: {error: true, errorMessage: true},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoading: false,
      isError: action.payload.error,
      errorMessage: action.payload.errorMessage,
    });
  });

  it('should handle GET_CONTACT_TRANSACTIONS_REQUEST', () => {
    const action = {
      type: 'GET_CONTACT_TRANSACTIONS_REQUEST',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingTXN: true,
      isErrorTXN: false,
      errorMessage: null,
    });
  });

  it('should handle GET_CONTACT_TRANSACTIONS_SUCCESS', () => {
    const action = {
      type: 'GET_CONTACT_TRANSACTIONS_SUCCESS',
      payload: {
        contactsTransactions: [],
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      contactsTransactions: action.payload.contactsTransactions,
      isLoadingTXN: false,
      isErrorTXN: false,
      errorMessage: null,
    });
  });

  it('should handle GET_CONTACT_TRANSACTIONS_FAILED', () => {
    const action = {
      type: 'GET_CONTACT_TRANSACTIONS_FAILED',
      payload: {
        error: false,
        errorMessage: false,
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingTXN: false,
      isError: action.payload.error,
      errorMessage: action.payload.errorMessage,
    });
  });

  it('should handle UPDATE_CONTACT_REQUEST_STATE', () => {
    const action = {
      type: 'UPDATE_CONTACT_REQUEST_STATE',
      payload: {state: '', value: ''},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      [action.payload.state]: action.payload.value,
    });
  });
});
