import * as actions from '../actions';

describe('contacts actions', () => {
  const payload = {
    data: [],
    error: {},
  };

  it('handle contact request', () => {
    const action = actions.getContactsRequest();
    const expectedAction = {
      type: 'GET_CONTACTS_REQUEST',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle contact request success', () => {
    const action = actions.getContactsSuccess(payload.data);
    const expectedAction = {
      type: 'GET_CONTACTS_SUCCESS',
      payload: {contactsList: payload.data},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle contacts failed', () => {
    const action = actions.getContactsFailed(payload.error);
    const expectedAction = {
      type: 'GET_CONTACTS_FAILED',
      payload: {isError: true},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle contacts transaction request', () => {
    const action = actions.getContactTransactionsRequest(payload.data);
    const expectedAction = {
      type: 'GET_CONTACT_TRANSACTIONS_REQUEST',
      payload: payload.data,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle contact transaction success', () => {
    const action = actions.getContactTransactionsSuccess(payload.data);
    const expectedAction = {
      type: 'GET_CONTACT_TRANSACTIONS_SUCCESS',
      payload: {contactsTransactions: payload.data},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle contact transactions failed', () => {
    const action = actions.getContactTransactionsFailed(payload.error);
    const expectedAction = {
      type: 'GET_CONTACT_TRANSACTIONS_FAILED',
      payload: {isError: true},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update add state', () => {
    const action = actions.updateAddState(payload.data);
    const expectedAction = {
      type: 'UPDATE_CONTACT_REQUEST_STATE',
      payload: payload.data,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle add contact request', () => {
    const action = actions.addContactRequest(payload.data);
    const expectedAction = {
      type: 'ADD_CONTACT_REQUESTED',
      payload: payload.data,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle fetch bank list', () => {
    const action = actions.getBankList(payload.data);
    const expectedAction = {
      type: 'GET_BANK_LIST',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle add contact list', () => {
    const action = actions.addContactSuccess(payload.data);
    const expectedAction = {
      type: 'ADD_CONTACT_SUCCESS',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle add contact failed', () => {
    const action = actions.addContactFailed(payload.data);
    const expectedAction = {
      type: 'ADD_CONTACT_FAILED',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle clear api response', () => {
    const action = actions.clearApiResponse();
    const expectedAction = {
      type: 'CLEAR_API_RESPONSE',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle edit contact request', () => {
    const action = actions.editContactRequest();
    const expectedAction = {
      type: 'EDIT_CONTACT_REQUESTED',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle delete contact request', () => {
    const action = actions.deleteContactRequest();
    const expectedAction = {
      type: 'DELETE_CONTACT_REQUESTED',
    };
    expect(action).toEqual(expectedAction);
  });
});
