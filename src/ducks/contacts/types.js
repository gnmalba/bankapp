/**
 * @flow
 */

// CONTACTS

export type ContactDataFromApi = {
  alias?: string,
  bankName: ?string,
  acctNumber: ?string,
  mobileNumber: ?string,
  lastTransaction: ?string,
  contactId: string,
  isEnabled: boolean,
  emailAddress: ?string,
  accountType: ?string,
  currencyCode: string,
  bankId: ?string,
  firstName: ?string,
  lastName: ?string,
  businessName: ?string,
  bankCode: ?string,
};

export type ContactItem = {
  title: string,
  subtitle: string,
  label: string,
  value: string,
  data: ContactDataFromApi,
  overview: Array<{label: string, value: ?string}>,
};

export type ContactsList = Array<ContactItem>;

// CONTACT TRANSACTIONS

export type ContactTransactionsFilterFields = {
  contactId: string,
  page: number,
  searchKeywords?: string,
  dateFrom?: string,
  dateTo?: string,
  amountFrom?: number,
  amountTo?: number,
};

export type ContactTransactionFromApi = {
  referenceNo: ?string,
  contactId: string,
  sourceAccount: string,
  transactionType: string,
  frequency: string,
  amount: string,
  date: string,
  alias: ?string,
  currency: string,
};

export type ContactTransactionItem = {
  name: string,
  date: string,
  value: string,
  label: string,
  transactionType: string,
};

export type ContactTransactionsList = Array<ContactTransactionItem>;

export type ContactAndTransactions = {
  contactId: string,
  transactions: ContactTransactionsList,
};

export type ContactsTransactions = Array<ContactAndTransactions>;

// ADD, EDIT, AND DELETE

export type AddContactFields = {
  contactType: string,
  accountType?: string,
  alias: string,
  emailAddress?: string,
  mobileNumber?: string,
  bankId?: string,
  accountNumber?: string,
  firstName?: string,
  lastName?: string,
  businessName?: string,
  mpin: string,
  customerId: number,
};

export type AddContactParams = {
  payload: {
    contactType: string,
    accountType?: string,
    values: {
      alias: string,
      emailAddress?: string,
      countryCode: {phoneCode: string},
      mobileNumber: string,
      bank: {bancnetCode?: string, Name?: String},
      accountNo?: string,
      firstName?: string,
      lastName?: string,
      businessName?: string,
      mpin: string,
      customerId: number,
    },
  },
};

export type bankListParams = {
  serviceType: string,
};

export type EditContactParams = {
  payload: EditContactFields,
};
export type EditContactFields = {
  contactId: string,
  customerId?: string,
  mpin: string,
  alias: string,
};

export type DeleteContactParams = {
  payload: DeleteContactFields,
};

export type DeleteContactFields = {
  contactId: string,
  customerId?: string,
  mpin: string,
};

// MAIN (ACTIONS & STATE)

export type ContactsAction =
  | {
      type: 'GET_CONTACTS_REQUEST',
    }
  | {
      type: 'GET_CONTACTS_SUCCESS',
      +payload: {
        contactsList: ContactsList,
      },
    }
  | {
      type: 'GET_CONTACTS_FAILED',
      +payload: {
        isError: boolean,
      },
    }
  | {
      type: 'GET_CONTACT_TRANSACTIONS_REQUEST',
    }
  | {
      type: 'GET_CONTACT_TRANSACTIONS_SUCCESS',
      +payload: {
        contactsTransactions: ContactsTransactions,
      },
    }
  | {
      type: 'GET_CONTACT_TRANSACTIONS_FAILED',
      +payload: {
        isError: boolean,
      },
    }
  | {
      type: 'RESET_CONTACTS_DATA',
    };

export type ContactsState = {
  contactsList: ContactsList,
  contactsTransactions: ContactTransactionsList,
  isLoading: boolean,
  isLoadingTXN: boolean,
  isSubmitting: boolean,
  isError: boolean,
  isErrorTXN: boolean,
};
