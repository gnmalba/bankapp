/**
 * @flow
 */

import {accountsFormat} from '@utils/accountNumber';
import {formatDate} from '@utils/date';

import type {
  ContactDataFromApi,
  ContactTransactionFromApi,
  ContactsTransactions,
  ContactsList,
} from './types';

const getDisplayName = (contact: ContactDataFromApi): string => {
  const {accountType, alias = '', businessName = ''} = contact;

  if (accountType === 'INDIVIDUAL') {
    return alias;
  }

  return businessName || '';
};

const getDisplayNumber = (contact: ContactDataFromApi): string => {
  const {bankName, acctNumber, mobileNumber} = contact;

  if (acctNumber && bankName) {
    return `${bankName} - ${accountsFormat(acctNumber)}`;
  }

  return mobileNumber || '';
};

const reformatTransactionDate = (date: ?string): string => {
  if (date) {
    return date.replace(/-/g, '/');
  }
  return '';
};

const generateOverview = (contact: ContactDataFromApi) => [
  {label: 'bank name', value: contact.bankName},
  {label: 'account no.', value: contact.acctNumber},
  {label: 'first name', value: contact.firstName},
  {label: 'last name', value: contact.lastName},
  {label: 'mobile no.', value: contact.mobileNumber},
  {label: 'email', value: contact.emailAddress},
];

export const formatContactsForList = (
  contacts: Array<ContactDataFromApi>,
): ContactsList => {
  return contacts.map((contact) => {
    return {
      title: getDisplayName(contact),
      subtitle: getDisplayNumber(contact),
      label: contact.lastTransaction ? 'last transaction' : '',
      value: reformatTransactionDate(contact.lastTransaction),
      overview: generateOverview(contact),
      data: contact,
    };
  });
};

export const getTransactionName = (
  transaction: ContactTransactionFromApi,
): string => {
  const transactionType = transaction.transactionType.toLowerCase();
  const alias = transaction.alias?.toLowerCase() || '';

  if (transactionType.match(/send/g) || alias.match(/send/g)) {
    return 'Sent Money';
  }

  return 'Received Money';
};

export const getTransactionAmount = (
  transaction: ContactTransactionFromApi,
): string => {
  const {amount, currency} = transaction;
  const type = getTransactionName(transaction);

  if (amount && currency) {
    return `${type === 'Sent Money' ? '-' : '+'} ${currency} ${amount}`;
  }
  return '';
};

function removeOldTransaction(array, contactId) {
  return array.filter((item) => item.contactId !== contactId);
}

export const formatContactTransactionsForList = (
  transactions: Array<ContactTransactionFromApi>,
  prevTransactions: ContactsTransactions,
  contactId: string,
): ContactsTransactions => {
  const updatedTransactions = transactions.map((transaction) => {
    return {
      name: getTransactionName(transaction),
      date: formatDate(transaction.date, 'MM/DD/YYYY'),
      label: transaction.alias || transaction.sourceAccount,
      value: getTransactionAmount(transaction),
      transactionType: getTransactionName(transaction),
    };
  });

  const filteredPrevTransactions = removeOldTransaction(
    prevTransactions,
    contactId,
  );

  return [
    ...filteredPrevTransactions,
    {contactId, transactions: updatedTransactions},
  ];
};
