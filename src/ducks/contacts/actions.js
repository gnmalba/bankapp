/**
 * @flow
 */

import type {
  ContactsAction,
  ContactsList,
  ContactTransactionsFilterFields,
  ContactsTransactions,
  AddContactFields,
  EditContactFields,
  DeleteContactFields,
} from './types';

export const getContactsRequest = (): ContactsAction => ({
  type: 'GET_CONTACTS_REQUEST',
  // payload: {customerId},
});
export const getContactsSuccess = (data: ContactsList): ContactsAction => ({
  type: 'GET_CONTACTS_SUCCESS',
  payload: {contactsList: data},
});
export const getContactsFailed = (error: Object): ContactsAction => ({
  type: 'GET_CONTACTS_FAILED',
  payload: {isError: true},
});

export const getContactTransactionsRequest = (
  data: ContactTransactionsFilterFields,
): ContactsAction => ({
  type: 'GET_CONTACT_TRANSACTIONS_REQUEST',
  payload: data,
});
export const getContactTransactionsSuccess = (
  data: ContactsTransactions,
): ContactsAction => ({
  type: 'GET_CONTACT_TRANSACTIONS_SUCCESS',
  payload: {contactsTransactions: data},
});
export const getContactTransactionsFailed = (
  error: Object,
): ContactsAction => ({
  type: 'GET_CONTACT_TRANSACTIONS_FAILED',
  payload: {isError: true},
});

export const updateAddState = (data: any): any => ({
  type: 'UPDATE_CONTACT_REQUEST_STATE',
  payload: data,
});

export const addContactRequest = (data: AddContactFields): any => ({
  type: 'ADD_CONTACT_REQUESTED',
  payload: data,
});

export const getBankList = (): any => ({
  type: 'GET_BANK_LIST',
});

export const addContactSuccess = (): any => ({
  type: 'ADD_CONTACT_SUCCESS',
});
export const addContactFailed = (): any => ({
  type: 'ADD_CONTACT_FAILED',
});

export const clearApiResponse = (): any => ({
  type: 'CLEAR_API_RESPONSE',
});

export const editContactRequest = (data: EditContactFields): any => ({
  type: 'EDIT_CONTACT_REQUESTED',
  payload: data,
});

export const deleteContactRequest = (data: DeleteContactFields): any => ({
  type: 'DELETE_CONTACT_REQUESTED',
  payload: data,
});
