/**
 * @flow
 */

import {call, put, takeLatest, select} from 'redux-saga/effects';

import {getCustomerID} from '@ducks/commonSelectors';
import api from '@api/services/contactsService';

import {
  getContactsSuccess,
  getContactsFailed,
  getContactTransactionsSuccess,
  getContactTransactionsFailed,
  updateAddState,
} from './actions';

import {formatContactsForList, formatContactTransactionsForList} from './utils';

import type {
  ContactTransactionsList,
  AddContactFields,
  AddContactParams,
  EditContactParams,
  DeleteContactParams,
} from './types';

import type {State} from '@ducks/types';

import {getCustomerId} from '@ducks/authentication/reducer';

import {getEncryptionPublicKey} from '@ducks/commonSelectors';
import encrypt from '@utils/encryptString';

// Constants
import {BANK_TYPE} from '@composition/ManageContacts/ContactType/constants';
import {
  ORGANIZATION_TYPE,
  INDIVIDUAL_TYPE,
} from '@composition/ManageContacts/AccountType/constants';

import config from '@api/services/contactsService';

const contactTransactionsState = (state: State): ContactTransactionsList =>
  state.contacts.contactsTransactions;

function* fetchContacts(): Generator<any, any, any> {
  const customerId = yield select(getCustomerID);
  try {
    const response = yield call(api.getContacts, customerId);

    const formattedResponse = formatContactsForList(response.data.data);

    yield put(getContactsSuccess(formattedResponse));
  } catch (error) {
    yield put(getContactsFailed(error));
  }
}

function* fetchContactTransactions({payload}): Generator<any, any, any> {
  const customerId = yield select(getCustomerID);
  const prevContactTransactions = yield select(contactTransactionsState);

  const filters = {
    ...payload,
    customerId,
  };

  try {
    const response = yield call(api.getContactSendMoneyTransactions, {
      params: {...filters},
    });

    const formattedResponse = formatContactTransactionsForList(
      response.data.data,
      prevContactTransactions,
      payload.contactId,
    );

    yield put(getContactTransactionsSuccess(formattedResponse));
  } catch (error) {
    yield put(getContactTransactionsFailed(error));
  }
}

function* fnAddContact({payload}: AddContactParams): any {
  yield put(
    updateAddState({
      state: 'apiResponse',
      value: {
        loading: true,
      },
    }),
  );

  try {
    const {
      contactType,
      accountType,
      values: {
        emailAddress,
        alias,
        countryCode,
        mobileNumber,
        mpin,
        bank,
        accountNo,
        businessName,
        firstName,
        lastName,
      },
    } = payload;

    const customerId = yield select(getCustomerId);
    const encryptionPublicKey = yield select(getEncryptionPublicKey);

    const params: AddContactFields = {
      emailAddress: emailAddress,
      alias: alias,
      contactType: contactType,
      mpin: encrypt(encryptionPublicKey).encrypt(mpin),
      customerId,
    };

    if (mobileNumber) {
      params.mobileNumber = `${countryCode.phoneCode}${mobileNumber}`;
    }

    if (contactType === BANK_TYPE.value) {
      params.bankId = bank.bancnetCode;
      params.accountNumber = accountNo;
      params.accountType = accountType;

      if (accountType === ORGANIZATION_TYPE.value) {
        params.businessName = businessName;
      } else {
        params.firstName = firstName;
        params.lastName = lastName;
      }
    } else {
      params.bankId = '0026';
      params.firstName = firstName;
      params.lastName = lastName;
      params.accountType = INDIVIDUAL_TYPE.value;
    }

    // call enroll deposit service
    const response = yield call(config.addContact, {
      ...params,
    });

    yield put(
      updateAddState({
        state: 'apiResponse',
        value: {
          status: response.code,
          data: response.data,
        },
      }),
    );
    // yield put(enrollmentDepositSuccess());
  } catch (error) {
    const errorMessage = error?.response?.data?.data?.errors?.[0]?.message;

    yield put(
      updateAddState({
        state: 'apiResponse',
        value: {
          status: error.response.data.code,
          data: null,
          loading: false,
          error: errorMessage,
        },
      }),
    );
  }
}

function* fnGetBankList(): any {
  try {
    const {data} = yield call(config.getBankList);
    yield put(
      updateAddState({
        state: 'bankList',
        value: {
          status: data.code,
          data: data.data,
          loading: false,
        },
      }),
    );
  } catch (error) {}
}

function* fnEditContact({payload}: EditContactParams): any {
  // yield put(
  //   updateAddState({
  //     state: 'apiResponse',
  //     value: {
  //       loading: true,
  //     },
  //   }),
  // );

  try {
    const {mpin, contactId, alias} = payload;

    const customerId = yield select(getCustomerId);
    const encryptionPublicKey = yield select(getEncryptionPublicKey);

    const response = yield call(config.editContact, {
      customerId,
      contactId,
      alias,
      mpin: encrypt(encryptionPublicKey).encrypt(mpin),
    });

    yield put(
      updateAddState({
        state: 'apiResponse',
        value: {
          status: response.code,
          data: response.data,
        },
      }),
    );
  } catch (error) {
    const errorMessage = error?.response?.data?.data?.errors?.[0]?.message;
    yield put(
      updateAddState({
        state: 'apiResponse',
        value: {
          status: error.response.data.code,
          data: null,
          loading: false,
          error: errorMessage,
        },
      }),
    );
  }
}

function* fnDeleteContact({payload}: DeleteContactParams): any {
  // yield put(
  //   updateAddState({
  //     state: 'apiResponse',
  //     value: {
  //       loading: true,
  //     },
  //   }),
  // );

  try {
    const {mpin, contactId} = payload;

    const customerId = yield select(getCustomerId);
    const encryptionPublicKey = yield select(getEncryptionPublicKey);

    const response = yield call(config.deleteContact, {
      customerId,
      contactId,
      mpin: encrypt(encryptionPublicKey).encrypt(mpin),
    });

    yield put(
      updateAddState({
        state: 'apiResponse',
        value: {
          status: response.code,
          data: response.data,
        },
      }),
    );
  } catch (error) {
    const errorMessage = error?.response?.data?.data?.errors?.[0]?.message;
    yield put(
      updateAddState({
        state: 'apiResponse',
        value: {
          status: error.response.data.code,
          data: null,
          loading: false,
          error: errorMessage,
        },
      }),
    );
  }
}

export default function* watcher(): Generator<any, any, any> {
  yield takeLatest('GET_CONTACTS_REQUEST', fetchContacts);
  yield takeLatest(
    'GET_CONTACT_TRANSACTIONS_REQUEST',
    fetchContactTransactions,
  );
  yield takeLatest('ADD_CONTACT_REQUESTED', fnAddContact);
  yield takeLatest('GET_BANK_LIST', fnGetBankList);
  yield takeLatest('EDIT_CONTACT_REQUESTED', fnEditContact);
  yield takeLatest('DELETE_CONTACT_REQUESTED', fnDeleteContact);
}
