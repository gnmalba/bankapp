/**
 * @flow
 */

import type {Store as ReduxStore, Dispatch as ReduxDispatch} from 'redux-types';

import type {ConfigState, ConfigAction} from '@ducks/config/types';
import type {AuthAction} from '@ducks/authentication/types';
import type {BioAction} from '@ducks/biometrics/types';
import type {EnrollmentReducerType} from '@ducks/enrollment/types';
import type {DepositState, DepositAction} from '@ducks/deposit/types';
import type {
  InstallmentStateType,
  InstallmentsAction,
} from '@ducks/installments/types';
import type {LostCardState, LostCardActions} from '@ducks/lostcard/types';
import type {YazzAccountState, YazzAccountAction} from '@ducks/yazz/types';
import type {CreditState} from '@ducks/credit/types';
import type {BillerState, BillerAction} from '@ducks/billers/types';
import type {
  PayBillsCreditState,
  PayBillsCreditAction,
} from '@ducks/pay-bills-credit/types';
import type {
  ManageAccountSettingsAction,
  ManageAccountSettingsState,
} from '@ducks/manageAccountSettings/types';
import type {
  PropTypes as DepositPrepaidOverviewState,
  DepositPrepaidOverviewAction,
} from '@ducks/depositprepaidoverview/types';
import type {
  AccountsDisplayOptionsState,
  AccountsDisplayOptionsAction,
} from '@ducks/accounts-displayoptions/types';
import type {
  CardActivationState,
  CardActivationAction,
} from '@ducks/card-activation/types';

import type {
  ManageSettingsAction,
  ManageSettingsStateType,
} from '@ducks/managesettings/types';
import type {ContactsState, ContactsAction} from '@ducks/contacts/types';

import type {ShortcutsAction, ShortcutsState} from '@ducks/shortcuts/types';

export type AuthenticationType = {
  anotherDeviceLogin: boolean,
  loading: boolean,
  username: string,
  usernameError: string,
  password: string,
  passwordError: string,
  encryptionPublicKey: string,
  authenticated: boolean,
  accessToken: string,
  refreshToken: string,
  data: Object,
  passcode: string,
  passcodeError: string,
  mfaToken: string,
  proceedToOtp: boolean,
  error: string,
  showModal: boolean,
  proceedToFullPageError: boolean,
  modalPrimaryBtnAction: Object,
  showPrimaryModalBtn: boolean,
  modalSecondaryBtnAction: Object,
  showSecondaryModalBtn: boolean,
  proceedToBioRegistration: boolean,
  showSnackbar: boolean,
  snackbarMessage: string,
  onboarded: boolean,
  modalPrimaryBtnText: string,
  modalSecondaryBtnText: string,
  showLogoutModal: boolean,
  anotherSessionClosed: boolean,
  rememberThisDevice: boolean,
  numberOfOtpRequests: number,
  inactivityCounter: number,
  tokenExpiry: number,
  loginMethod: string,
  signedToken: string,
};

export type BiometricsType = {
  passcode: string,
  isBiometricsEnrolled: boolean,
  deviceSensor: string,
  biometricsError: string,
  linkDisabled: boolean,
  locked: boolean,
  unlockTime: any,
  lockedTimes: number,
  registrationFailed: boolean,
  passcodeAttempts: number,
  registerBiometrics: boolean,
  loading: boolean,
  sendAndroidIntent: boolean,
};

/**
 * Common Types
 */

type Amount = {
  value: number,
  currencyCode: string,
};

type Limits = Array<{
  id: number,
  type: String,
  amount: Amount,
}>;

type Shortcut = {
  code: string,
  credit: boolean,
  debit: boolean,
  enabled: boolean,
  forShortcut: boolean,
  icon: string,
  id: number,
  name: string,
  limits?: Limits,
};

export type ShortCuts = Array<Shortcut>;

export type State = {|
  config: ConfigState,
  authentication: AuthenticationType,
  biometrics: BiometricsType,
  contacts: ContactsState,
  enrollment: EnrollmentReducerType,
  lostCardReducer: LostCardState,
  deposit: DepositState,
  installments: InstallmentStateType,
  yazz: YazzAccountState,
  credit: CreditState,
  billers: BillerState,
  paybillscredit: PayBillsCreditState,
  manageAccountSettings: ManageAccountSettingsState,
  depositprepaidoverview: DepositPrepaidOverviewState,
  accountDisplayOptions: AccountsDisplayOptionsState,
  cardActivation: CardActivationState,
  manageSettings: ManageSettingsStateType,
  shortcuts: ShortcutsState,
|};

export type Action =
  | ConfigAction
  | AuthAction
  | BioAction
  | ContactsAction
  | DepositAction
  | InstallmentsAction
  | LostCardActions
  | YazzAccountAction
  | BillerAction
  | PayBillsCreditAction
  | ManageAccountSettingsAction
  | DepositPrepaidOverviewAction
  | AccountsDisplayOptionsAction
  | CardActivationAction
  | ManageSettingsAction
  | ShortcutsAction;

export type Store = ReduxStore<State, Action>;

export type Dispatch = ReduxDispatch<Action>;

/**
 * END of Common Types
 */
