/**
 * @fow
 */
import type {DashboardAction} from './types';

export const toggleCustomizeWidget = (): DashboardAction => ({
  type: 'TOGGLE_CUSTOMIZE_WIDGET',
});
