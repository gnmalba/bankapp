/**
 * @flow
 * @format
 */

export type DashboardState = {
  isQuickLinks: boolean,
  isCustomizeDashboard: boolean,
  isCustomizeWidgets: boolean,
  customizeWidgetShown: boolean,
  isShowEnrollBillerPrompt: boolean,
};

export type DashboardAction =
  | {
      type: 'SHOW_GUIDED_TOUR',
    }
  | {
      type: 'UPDATE_GUIDE_QUICK_LINKS',
    }
  | {
      type: 'UPDATE_GUIDE_CUSTOMIZE_DASHBOARD',
    }
  | {
      type: 'UPDATE_GUIDE_CUSTOMIZE_WIDGETS',
    }
  | {
      type: 'TOGGLE_CUSTOMIZE_WIDGET',
    }
  | {
      type: 'SHOW_ENROLL_BILLER_PROMPT',
      payload: Object,
    }
  | {
      type: 'HIDE_ENROLL_BILLER_PROMPT',
    };
