/**
 * @flow
 * @format
 */

import type {DashboardState, DashboardAction} from './types';

const INITIAL_STATE: DashboardState = {
  isQuickLinks: false,
  isCustomizeDashboard: false,
  isCustomizeWidgets: false,
  customizeWidgetShown: false,
  isShowEnrollBillerPrompt: false,
};

const dashboard = (
  state: DashboardState = INITIAL_STATE,
  action: DashboardAction,
): DashboardState => {
  switch (action.type) {
    case 'TOGGLE_CUSTOMIZE_WIDGET':
      return {
        ...state,
        customizeWidgetShown: !state.customizeWidgetShown,
      };
    case 'SHOW_GUIDED_TOUR':
      return {
        ...state,
        isQuickLinks: true,
        isCustomizeDashboard: false,
        isCustomizeWidgets: false,
      };
    case 'UPDATE_GUIDE_QUICK_LINKS':
      return {
        ...state,
        isQuickLinks: false,
        isCustomizeDashboard: true,
        isCustomizeWidgets: false,
      };
    case 'UPDATE_GUIDE_CUSTOMIZE_DASHBOARD':
      return {
        ...state,
        isQuickLinks: false,
        isCustomizeDashboard: false,
        isCustomizeWidgets: true,
      };
    case 'SHOW_ENROLL_BILLER_PROMPT':
      return {
        ...state,
        billerAccount: action.payload,
        isShowEnrollBillerPrompt: true,
      };
    case 'HIDE_ENROLL_BILLER_PROMPT':
      return {
        ...state,
        billerAccount: null,
        isShowEnrollBillerPrompt: false,
      };
    default:
      return state;
  }
};

export default dashboard;
