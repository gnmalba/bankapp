/**
 * @flow
 */

/**
 * Types for api response
 */
import type {Account} from '../deposit/types';

export type T = Generator<any, any, any>;
export type PropTypes = {
  depositPrepaidDetails: {
    loading: boolean,
    data: Account,
    error: boolean,
    loader: Array<string>,
  },
  transactionDepositPrepaid: {
    loading: boolean,
    data: any,
    error: boolean,
  },
  transactionCresittSoaSupple: {
    loading: boolean,
    data: any,
    error: boolean,
  },
  transactionCodes: {
    list: Array<TransactionCodeItem>,
    loading: boolean,
  },
};

export type TransactionCodeItem = {
  'Host Response': string,
  'Recommended Translation': string,
};

export type DepositPrepaidOverviewAction =
  | {
      type: '@depositprepaidoverview/CHANGE_ALIAS',
      payload: Object,
    }
  | {type: 'TRANSACTION_HISTORY_CODE_REQUESTED'}
  | {
      type: 'FETCH_TRANSACTION_HISTORY_CODE_SUCCESS',
      payload: Array<TransactionCodeItem>,
    }
  | {type: 'FETCH_TRANSACTION_HISTORY_CODE_FAILED'};
