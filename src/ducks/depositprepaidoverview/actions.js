/**
 * @flow
 */

import type {DepositPrepaidOverviewAction, TransactionCodeItem} from './types';

export const changeAliasDepositPrepaid = (
  data: Object,
): DepositPrepaidOverviewAction => ({
  type: '@depositprepaidoverview/CHANGE_ALIAS',
  payload: data,
});

export const requestTransactionCodes = (): DepositPrepaidOverviewAction => ({
  type: 'TRANSACTION_HISTORY_CODE_REQUESTED',
});

export const updateTransactionCodesList = (
  transactionCodes: Array<TransactionCodeItem>,
): DepositPrepaidOverviewAction => ({
  type: 'FETCH_TRANSACTION_HISTORY_CODE_SUCCESS',
  payload: transactionCodes,
});

export const requestTransactionCodesFailed = (): DepositPrepaidOverviewAction => ({
  type: 'FETCH_TRANSACTION_HISTORY_CODE_FAILED',
});
