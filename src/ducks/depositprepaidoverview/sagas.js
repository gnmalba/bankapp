/**
 * @flow
 */

import {takeLatest, call, put, select} from 'redux-saga/effects';

import DepositConfig from '@api/services/depositService';
import type {T} from './types';
import moment from 'moment';
import {fnSearch, handleFilterSearch} from './utils';
import {transactionHistoryCodesSelector} from './selectors';
import {
  requestTransactionCodes,
  updateTransactionCodesList,
  requestTransactionCodesFailed,
} from './actions';

export function* getAccountDetails({payload}: {payload: number}): T {
  try {
    const response = yield call(DepositConfig.getAccountDetails, payload);
    yield put({
      type: 'GET_DEPOSIT_PREPAID_API_SUCCESS',
      payload: response.data.data,
    });
  } catch (error) {
    yield put({
      type: 'GET_DEPOSIT_PREPAID_API_FAILURE',
      payload: error.response,
    });
  }
}

export function* getTransactionCodes(): T {
  yield put(requestTransactionCodes());
  try {
    const response = yield call(DepositConfig.getTransactionCodes);

    yield put(updateTransactionCodesList(response.data.data));
  } catch (error) {
    yield put(requestTransactionCodesFailed());
  }
}

export function* getTrasactionDepositPrepaid({payload}: {payload: number}): T {
  try {
    const response = yield call(DepositConfig.getTransactionHistory, payload);
    const transactionCodes = yield select(transactionHistoryCodesSelector);
    const data = {...response.data.data};
    data.transactionHistory.map((item) => {
      const descriptiveName = handleFilterSearch(
        transactionCodes,
        item.description.line1.trim(),
      );
      item.description.line1 = descriptiveName;
    });
    yield put({
      type: 'GET_DEPOSIT_PREPAID_HISTORY_SUCCESS',
      payload: data,
    });
  } catch (error) {
    yield put({
      type: 'GET_DEPOSIT_PREPAID_HISTORY_FAILURE',
      payload: error.response,
    });
  }
}

export function* getSortFilterList({payload}: Object): T {
  /*
   * description: '',
   * dateFrom: '',
   * dateTo: '',
   * amountFrom: '',
   * amountTo: '',
   */

  const data = yield select(
    (state) => state.depositprepaidoverview.transactionDepositPrepaid,
  );

  let filtered = [];

  /* search here */
  filtered = fnSearch(data, payload);

  for (const key in payload) {
    /* filter amount here */
    if (
      (key === 'amountFrom' || key === 'amountTo') &&
      payload[key] !== null &&
      payload[key].length > 0
    ) {
      filtered = filtered.filter((item) => {
        const parsedFrom = parseFloat(payload.amountFrom.replace(/,/g, ''));
        const parsedTo = parseFloat(payload.amountTo.replace(/,/g, ''));

        return (
          Math.abs(item.amount.value) >= parsedFrom &&
          Math.abs(item.amount.value) <= parsedTo
        );
      });
    }

    /* filter dates here */
    if (
      (key === 'dateFrom' || key === 'dateTo') &&
      payload[key] !== null &&
      payload[key].length > 0
    ) {
      filtered = filtered.filter((item) =>
        moment(item.transactionDate).isBetween(
          payload.dateFrom,
          payload.dateTo,
          undefined,
          '[]',
        ),
      );
    }
  }

  yield put({
    type: 'FILTER_SEARCH_DEPOSIT_PREPAID_SUCCESS',
    payload: {transactionHistory: filtered},
  });
}

export default function* watcher(): any {
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', getTransactionCodes);
  yield takeLatest('GET_DEPOSIT_PREPAID_API_REQUESTED', getAccountDetails);
  yield takeLatest('FILTER_DEPOSIT_PREPAID_REQUEST', getSortFilterList);
  yield takeLatest(
    'GET_DEPOSIT_PREPAID_HISTORY_REQUESTED',
    getTrasactionDepositPrepaid,
  );
}
