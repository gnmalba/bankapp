/**
 * @flow
 */
import type {PropTypes} from './types';

const INITIAL_STATE: any = {
  depositPrepaidDetails: {
    loading: false,
    data: {},
    error: false,
    loader: [],
  },
  transactionDepositPrepaid: {
    loading: false,
    data: [],
    error: false,
    unfilteredData: [],
  },
  transactionCodes: {
    list: [],
    loading: false,
  },
};

const depositCredit = (
  state: any = INITIAL_STATE,
  action: Object = {},
): PropTypes => {
  switch (action.type) {
    case 'GET_DEPOSIT_PREPAID_API_REQUESTED':
      return {
        ...state,
        depositPrepaidDetails: {
          loading: true,
          error: false,
          data: [],
          loader: [],
        },
      };
    case 'GET_DEPOSIT_PREPAID_API_SUCCESS': {
      return {
        ...state,
        depositPrepaidDetails: {
          loading: false,
          error: false,
          data: action.payload,
          loader: ['deposit'],
        },
      };
    }
    case 'GET_DEPOSIT_PREPAID_API_FAILURE': {
      return {
        ...state,
        depositPrepaidDetails: {
          loading: false,
          error: true,
          data: action.payload,
          loader: ['deposit'],
        },
      };
    }

    case 'GET_DEPOSIT_PREPAID_HISTORY_REQUESTED':
      return {
        ...state,
        transactionDepositPrepaid: {
          loading: true,
          error: false,
          data: [],
          unfilteredData: [],
        },
      };
    case 'GET_DEPOSIT_PREPAID_HISTORY_SUCCESS': {
      return {
        ...state,
        transactionDepositPrepaid: {
          loading: false,
          error: false,
          data: action.payload,
          unfilteredData: action.payload,
        },
      };
    }

    case 'GET_DEPOSIT_PREPAID_HISTORY_FAILURE': {
      return {
        ...state,
        transactionDepositPrepaid: {
          loading: false,
          error: true,
          data: action.payload,
          unfilteredData: [],
        },
      };
    }

    case 'FILTER_DEPOSIT_PREPAID_REQUEST': {
      return {
        ...state,
        transactionDepositPrepaid: {
          ...state.transactionDepositPrepaid,
          loading: true,
          error: false,
          data: [],
        },
      };
    }

    case 'FILTER_SEARCH_DEPOSIT_PREPAID_SUCCESS': {
      return {
        ...state,
        transactionDepositPrepaid: {
          ...state.transactionDepositPrepaid,
          loading: false,
          error: false,
          data: action.payload,
        },
      };
    }

    case '@depositprepaidoverview/CHANGE_ALIAS':
      return {
        ...state,
        depositPrepaidDetails: {
          ...state.depositPrepaidDetails,
          data: action.payload,
        },
      };

    case 'TRANSACTION_HISTORY_CODE_REQUESTED':
      return {
        ...state,
        transactionCodes: {
          ...state.transactionCodes,
          loading: true,
        },
      };

    case 'FETCH_TRANSACTION_HISTORY_CODE_SUCCESS':
      return {
        ...state,
        transactionCodes: {
          ...state.transactionCodes,
          list: action.payload,
          loading: false,
        },
      };
    case 'FETCH_TRANSACTION_HISTORY_CODE_FAILED':
      return {
        ...state,
        transactionCodes: {
          ...state.transactionCodes,
          loading: false,
        },
      };

    case 'DEPOSITPREPAIDOVERVIEW_LOGOUT_RESET_STATE':
      return INITIAL_STATE;

    default:
      return state;
  }
};

export default depositCredit;
