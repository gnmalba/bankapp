import {transactionHistoryCodesSelector} from '../selectors';

import configureStore from 'redux-mock-store';

const INITIAL_STATE: any = {
  depositPrepaidDetails: {
    loading: false,
    data: {},
    error: false,
    loader: [],
  },
  transactionDepositPrepaid: {
    loading: false,
    data: [],
    error: false,
    unfilteredData: [],
  },
  transactionCodes: {
    list: [],
    loading: false,
  },
};
const mockStore = configureStore();

const store = mockStore({
  depositprepaidoverview: INITIAL_STATE,
});

describe('Unit test for selectors', () => {
  it('should working', () => {
    expect(transactionHistoryCodesSelector(store.getState())).toEqual([]);
  });
});
