import {select} from 'redux-saga/effects';
import {
  getAccountDetails,
  getTransactionCodes,
  getTrasactionDepositPrepaid,
  //   getSortFilterList,
} from '../sagas';
import DepositConfig from '@api/services/depositService';
import {transactionHistoryCodesSelector} from '../selectors';

import {expectSaga, matchers} from 'redux-saga-test-plan';

// const filters = {
//   description: '',
//   dateFrom: '',
//   dateTo: '',
//   amountFrom: '',
//   amountTo: '',
// };

// function getData(state) {
//   return state.depositprepaidoverview.transactionDepositPrepaid;
// }

describe('deposit and prepaid Sagas', () => {
  const payload = 5811;

  it('success and failed getAccountDetails', () => {
    return expectSaga(getAccountDetails, {payload})
      .provide([[matchers.call.fn(DepositConfig.getAccountDetails), {payload}]])
      .put({type: 'GET_DEPOSIT_PREPAID_API_FAILURE', payload: undefined})
      .run();
  });

  it('success and failed getTransactionCodes', () => {
    return expectSaga(getTransactionCodes)
      .put({type: 'TRANSACTION_HISTORY_CODE_REQUESTED'})
      .provide([[matchers.call.fn(DepositConfig.getTransactionCodes)]])
      .put({type: 'FETCH_TRANSACTION_HISTORY_CODE_FAILED'})
      .run();
  });

  it('success and failed getTrasactionDepositPrepaid', () => {
    return expectSaga(getTrasactionDepositPrepaid, payload)
      .provide([
        [
          matchers.call.fn(DepositConfig.getTransactionHistory, {
            payload: undefined,
          }),
        ],
        [select(transactionHistoryCodesSelector)],
      ])
      .put({type: 'GET_DEPOSIT_PREPAID_HISTORY_FAILURE', payload: undefined})
      .run();
  });

  //   it('success and failed getSortFilterList', () => {
  //     return expectSaga(getSortFilterList, payload)
  //       .provide([[select(getData)]])
  //       .put({type: 'GET_DEPOSIT_PREPAID_HISTORY_FAILURE', payload: undefined})
  //       .run();
  //   });

  //   it('handles errors', () => {
  //     const error = new Error('error');

  //     return expectSaga(getAccountDetails, payload)
  //       .provide([
  //         [
  //           matchers.call.fn(DepositConfig.getAccountDetails, payload),
  //           throwError(error),
  //         ],
  //       ])
  //       .put({type: 'FAIL_USER', error})
  //       .run();
  //   });
});
