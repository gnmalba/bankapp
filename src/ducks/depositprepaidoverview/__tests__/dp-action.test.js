import {
  changeAliasDepositPrepaid,
  requestTransactionCodes,
  updateTransactionCodesList,
  requestTransactionCodesFailed,
} from '../actions';

describe('Unit test for Deposti Prepaid Overview Actions', () => {
  it('Should create requestTransactionCodes properly', () => {
    const expectedAction = {
      type: 'TRANSACTION_HISTORY_CODE_REQUESTED',
    };

    expect(requestTransactionCodes()).toEqual(expectedAction);
  });

  it('Should create updateTransactionCodesList properly', () => {
    const expectedAction = {
      type: 'FETCH_TRANSACTION_HISTORY_CODE_SUCCESS',
      payload: [],
    };

    expect(updateTransactionCodesList([])).toEqual(expectedAction);
  });

  it('Should create requestTransactionCodesFailed properly', () => {
    const expectedAction = {
      type: 'FETCH_TRANSACTION_HISTORY_CODE_FAILED',
    };

    expect(requestTransactionCodesFailed()).toEqual(expectedAction);
  });

  it('Should create changeAliasDepositPrepaid properly', () => {
    const expectedAction = {
      type: '@depositprepaidoverview/CHANGE_ALIAS',
      payload: {},
    };

    expect(changeAliasDepositPrepaid({})).toEqual(expectedAction);
  });
});
