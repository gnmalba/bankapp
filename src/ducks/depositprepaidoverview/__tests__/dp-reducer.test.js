import reducer, {INITIAL_STATE} from '../reducer';

describe('deposit prepaid reducer', () => {
  const mockData = {
    depositPrepaidDetails: {data: {}, error: false, loader: [], loading: false},
    transactionCodes: {list: [], loading: false},
    transactionDepositPrepaid: {
      data: [],
      error: false,
      loading: false,
      unfilteredData: [],
    },
  };

  it('should return the initial state', () => {
    expect(reducer(INITIAL_STATE, {})).toEqual(mockData);
  });

  it('should handle GET_DEPOSIT_PREPAID_API_REQUESTED state', () => {
    const requestEmpty = {
      depositPrepaidDetails: {
        data: [],
        error: false,
        loader: [],
        loading: true,
      },
    };

    expect(
      reducer([], {
        type: 'GET_DEPOSIT_PREPAID_API_REQUESTED',
      }),
    ).toEqual(requestEmpty);
  });

  it('should handle GET_DEPOSIT_PREPAID_API_SUCCESS state', () => {
    const actionStarter = {
      type: 'GET_DEPOSIT_PREPAID_API_SUCCESS',
      payload: {data: {}, loader: ['deposit']},
    };

    const expectedState = {
      depositPrepaidDetails: {
        loading: false,
        error: false,
        data: actionStarter.payload,
        loader: ['deposit'],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_DEPOSIT_PREPAID_API_FAILURE state', () => {
    const actionStarter = {
      type: 'GET_DEPOSIT_PREPAID_API_FAILURE',
      payload: {data: {}, loader: ['deposit']},
    };

    const expectedState = {
      depositPrepaidDetails: {
        loading: false,
        error: true,
        data: actionStarter.payload,
        loader: ['deposit'],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_DEPOSIT_PREPAID_HISTORY_REQUESTED state', () => {
    const actionStarter = {
      type: 'GET_DEPOSIT_PREPAID_HISTORY_REQUESTED',
    };

    const expectedState = {
      transactionDepositPrepaid: {
        loading: true,
        error: false,
        data: [],
        unfilteredData: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_DEPOSIT_PREPAID_HISTORY_SUCCESS state', () => {
    const actionStarter = {
      type: 'GET_DEPOSIT_PREPAID_HISTORY_SUCCESS',
      payload: {data: {}, loader: ['deposit']},
    };

    const expectedState = {
      transactionDepositPrepaid: {
        loading: false,
        error: false,
        data: actionStarter.payload,
        unfilteredData: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle GET_DEPOSIT_PREPAID_HISTORY_FAILURE state', () => {
    const actionStarter = {
      type: 'GET_DEPOSIT_PREPAID_HISTORY_FAILURE',
      payload: {data: {}, loader: ['deposit']},
    };

    const expectedState = {
      transactionDepositPrepaid: {
        loading: false,
        error: true,
        data: actionStarter.payload,
        unfilteredData: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle FILTER_DEPOSIT_PREPAID_REQUEST state', () => {
    const actionStarter = {
      type: 'FILTER_DEPOSIT_PREPAID_REQUEST',
    };

    const expectedState = {
      transactionDepositPrepaid: {
        loading: true,
        error: false,
        data: [],
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle FILTER_SEARCH_DEPOSIT_PREPAID_SUCCESS state', () => {
    const actionStarter = {
      type: 'FILTER_SEARCH_DEPOSIT_PREPAID_SUCCESS',
      payload: {data: {}, loader: ['deposit']},
    };

    const expectedState = {
      transactionDepositPrepaid: {
        loading: false,
        error: false,
        data: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle @depositprepaidoverview/CHANGE_ALIAS state', () => {
    const actionStarter = {
      type: '@depositprepaidoverview/CHANGE_ALIAS',
      payload: {data: {}},
    };

    const expectedState = {
      depositPrepaidDetails: {
        data: actionStarter.payload,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle TRANSACTION_HISTORY_CODE_REQUESTED state', () => {
    const actionStarter = {
      type: 'TRANSACTION_HISTORY_CODE_REQUESTED',
    };

    const expectedState = {
      transactionCodes: {
        loading: true,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle FETCH_TRANSACTION_HISTORY_CODE_SUCCESS state', () => {
    const actionStarter = {
      type: 'FETCH_TRANSACTION_HISTORY_CODE_SUCCESS',
    };

    const expectedState = {
      transactionCodes: {
        list: actionStarter.payload,
        loading: false,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });

  it('should handle FETCH_TRANSACTION_HISTORY_CODE_FAILED state', () => {
    const actionStarter = {
      type: 'FETCH_TRANSACTION_HISTORY_CODE_FAILED',
    };

    const expectedState = {
      transactionCodes: {
        list: actionStarter.payload,
        loading: false,
      },
    };

    expect(reducer({}, actionStarter)).toEqual(expectedState);
  });
});
