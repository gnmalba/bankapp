import {fnSearch, handleFilterSearch} from '../utils';

const data = {
  unfilteredData: {
    transactionHistory: [
      {
        description: {
          line1: 'Apple',
        },
      },
    ],
  },
};

const payload = {
  description: 'string',
  description1: '',
};

const data1 = [{description: {line1: 'Apple'}}];
const array = [];
const string = '';

describe('Unit test for utils', () => {
  it('should working fnSearch', () => {
    expect(fnSearch(data, payload)).toEqual([]);
  });

  it('should working fnSearch else', () => {
    expect(fnSearch(data, payload.description1)).toEqual(data1);
  });

  it('should working handle', () => {
    expect(handleFilterSearch(array, string)).toEqual(string);
  });

  it('should working handle else if', () => {
    expect(handleFilterSearch(['data'], string)).toEqual(string);
  });

  it('should working handle else', () => {
    expect(handleFilterSearch(['data', 'data1'], string)).toEqual(string);
  });
});
