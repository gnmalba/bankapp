/**
 * @flow
 */

import type {TransactionCodeItem} from './types';

export const fnSearch = (data: Object, payload: Object): any => {
  let filtered;

  if (payload.description && payload.description.length > 0) {
    filtered = data.unfilteredData.transactionHistory.filter((item) =>
      item.description.line1
        .toLowerCase()
        .includes(payload.description.toLowerCase()),
    );
  } else {
    filtered = data.unfilteredData.transactionHistory;
  }

  return filtered;
};

export const handleFilterSearch = (
  array: Array<TransactionCodeItem>,
  text: string,
): string => {
  const dataArray = array?.filter(function (data) {
    return data['Host Response'] === text.replace(/\s+/g, '');
  });
  if (dataArray.length > 0) {
    return dataArray[0]['Recommended Translation'];
  }
  return text;
};
