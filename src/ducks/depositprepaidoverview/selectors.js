/**
 * @flow
 */

import type {State} from '@ducks/types';
import type {TransactionCodeItem} from './types';

export const transactionHistoryCodesSelector = (
  state: State,
): Array<TransactionCodeItem> =>
  state.depositprepaidoverview.transactionCodes.list;
