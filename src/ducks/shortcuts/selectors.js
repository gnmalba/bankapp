/**
 * @flow
 */

import type {State} from '@ducks/types';
import type {ShortcutsState, ShortcutType} from './types';

export const getCheckedShortcuts = (state: State): ShortcutType[] => {
  const {data, checkedShortcuts}: ShortcutsState = state.shortcuts;

  return data.filter((shortcut) => checkedShortcuts.includes(shortcut.id));
};

export const getCheckedInstallmentShortcuts = (
  state: State,
): ShortcutType[] => {
  const {
    installmentsShortcuts,
    checkedInstallmentsShortcuts,
  }: ShortcutsState = state.shortcuts;

  return installmentsShortcuts.filter((shortcut) =>
    checkedInstallmentsShortcuts.includes(shortcut.id),
  );
};
