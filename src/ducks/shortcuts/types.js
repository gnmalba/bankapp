// @flow

import type {IShortcut} from '@types/shortcut.type';

export type ShortcutsState = {
  data: ShortcutType[],
  checkedShortcuts: number[],
  checkedInstallmentsShortcuts: number[],
  installmentsShortcuts: ShortcutType[],
};

export type ShortcutType = IShortcut & {
  notification?: number,
};

export type ShortcutsAction =
  | {type: '@shortcuts/INITIALIZE_SHORTCUTS'}
  | {
      type: '@shortcuts/UPDATE_SHORTCUTS',
      payload: {shortcuts: ShortcutType[]},
    }
  | {
      type: '@shortcuts/UPDATE_CHECKED_SHORTCUTS',
      payload: {ids: number[]},
    }
  | {
      type: '@shortcuts/ADD_CHECKED_SHORTCUT',
      payload: {id: number},
    }
  | {
      type: '@shortcuts/REMOVE_CHECKED_SHORTCUT',
      payload: {id: number},
    }
  | {type: '@shortcuts/INITIALIZE_INSTALLMENTS_SHORTCUTS'}
  | {
      type: '@shortcuts/UPDATE_INSTALLMENTS_SHORTCUTS',
      payload: {shortcuts: ShortcutType[]},
    }
  | {
      type: '@shortcuts/UPDATE_INSTALLMENTS_CHECKED_SHORTCUTS',
      payload: {ids: number[]},
    }
  | {
      type: '@shortcuts/ADD_INSTALLMENTS_CHECKED_SHORTCUT',
      payload: {id: number},
    }
  | {
      type: '@shortcuts/REMOVE_INSTALLMENTS_CHECKED_SHORTCUT',
      payload: {id: number},
    }
  | {type: 'SHORTCUTS_LOGOUT_RESET_STATE'};
