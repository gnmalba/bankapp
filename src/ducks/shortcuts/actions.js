// @flow

import type {ShortcutsAction, ShortcutType} from './types';
import * as types from './actionTypes';

export const initializeShortcutsForWidget = (): ShortcutsAction => ({
  type: types.INITIALIZE_SHORTCUTS,
});

export const updateShortcuts = (
  shortcuts: ShortcutType[],
): ShortcutsAction => ({
  type: types.UPDATE_SHORTCUTS,
  payload: {shortcuts},
});

export const updateCheckedShortcuts = (ids: number[]): ShortcutsAction => ({
  type: types.UPDATE_CHECKED_SHORTCUTS,
  payload: {ids},
});

export const addCheckedShortcut = (id: number): ShortcutsAction => ({
  type: types.ADD_CHECKED_SHORTCUT,
  payload: {id},
});

export const removeCheckedShortcut = (id: number): ShortcutsAction => ({
  type: types.REMOVE_CHECKED_SHORTCUT,
  payload: {id},
});

export const initializeShortcutsForInstallmentsWidget = (): ShortcutsAction => ({
  type: types.INITIALIZE_INSTALLMENTS_SHORTCUTS,
});

export const updateInstallmentShortcuts = (
  shortcuts: ShortcutType[],
): ShortcutsAction => ({
  type: types.UPDATE_INSTALLMENTS_SHORTCUTS,
  payload: {shortcuts},
});

export const updateCheckedInstallmentShortcuts = (
  ids: number[],
): ShortcutsAction => ({
  type: types.UPDATE_INSTALLMENTS_CHECKED_SHORTCUTS,
  payload: {ids},
});

export const addCheckedInstallmentShortcut = (id: number): ShortcutsAction => ({
  type: types.ADD_INSTALLMENTS_CHECKED_SHORTCUT,
  payload: {id},
});

export const removeCheckedInstallmentShortcut = (
  id: number,
): ShortcutsAction => ({
  type: types.REMOVE_INSTALLMENTS_CHECKED_SHORTCUT,
  payload: {id},
});
