/**
 * @flow
 */

import type {ShortcutsState, ShortcutsAction} from './types';
import {
  DEFAULT_INSTALLMENT_CHECKED,
  DEFAULT_INSTALLMENT_SHORTCUTS,
} from './utils';

const INITIAL_STATE: ShortcutsState = {
  data: [],
  checkedShortcuts: [],
  installmentsShortcuts: DEFAULT_INSTALLMENT_SHORTCUTS,
  checkedInstallmentsShortcuts: DEFAULT_INSTALLMENT_CHECKED,
};

const shortcuts = (
  state: ShortcutsState = INITIAL_STATE,
  action: ShortcutsAction,
): ShortcutsState => {
  switch (action.type) {
    case '@shortcuts/UPDATE_SHORTCUTS':
      return {
        ...state,
        data: action.payload.shortcuts,
      };

    case '@shortcuts/UPDATE_CHECKED_SHORTCUTS':
      return {
        ...state,
        checkedShortcuts: action.payload.ids,
      };

    case '@shortcuts/ADD_CHECKED_SHORTCUT':
      return {
        ...state,
        checkedShortcuts: [...state.checkedShortcuts, action.payload.id],
      };

    case '@shortcuts/REMOVE_CHECKED_SHORTCUT':
      return {
        ...state,
        checkedShortcuts: state.checkedShortcuts.filter(
          (id) => id !== action.payload.id,
        ),
      };

    case '@shortcuts/UPDATE_INSTALLMENTS_SHORTCUTS':
      return {
        ...state,
        installmentsShortcuts: action.payload.shortcuts,
      };

    case '@shortcuts/UPDATE_INSTALLMENTS_CHECKED_SHORTCUTS':
      return {
        ...state,
        checkedInstallmentsShortcuts: action.payload.ids,
      };

    case '@shortcuts/ADD_INSTALLMENTS_CHECKED_SHORTCUT':
      return {
        ...state,
        checkedInstallmentsShortcuts: [
          ...state.checkedInstallmentsShortcuts,
          action.payload.id,
        ],
      };

    case '@shortcuts/REMOVE_INSTALLMENTS_CHECKED_SHORTCUT':
      return {
        ...state,
        checkedInstallmentsShortcuts: state.checkedInstallmentsShortcuts.filter(
          (id) => id !== action.payload.id,
        ),
      };

    case 'SHORTCUTS_LOGOUT_RESET_STATE':
      return INITIAL_STATE;

    default:
      return state;
  }
};

export default shortcuts;
