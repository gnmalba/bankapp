// @flow

import {INSTALLMENT_SHORTCUT_CODES} from '@utils/constants';

import type {ShortcutType} from './types';

export const mergeAndFilterShortcuts = (accounts: Object[]): ShortcutType[] => {
  if (!accounts) {
    return [];
  }

  const accountShortcuts = accounts.map(({shortcuts = []}) => shortcuts) || [];
  const uniqueShortcutCodes: string[] = [];
  const allowedShortcuts: ShortcutType[] = [];

  accountShortcuts.forEach(
    (shortcuts) =>
      shortcuts &&
      shortcuts
        .filter((shortcut) => SUPPORTED_SHORTCUTS.includes(shortcut.id))
        .forEach((shortcut) => {
          if (
            !uniqueShortcutCodes.includes(shortcut.code) &&
            !INSTALLMENT_SHORTCUT_CODES.includes(shortcut.code) &&
            shortcut.forShortcut
          ) {
            uniqueShortcutCodes.push(shortcut.code);
            allowedShortcuts.push(shortcut);
          }

          return shortcut;
        }),
  );

  return allowedShortcuts.sort((a, b) => a.id - b.id);
};

export const mergeAndFilterShortcutsForInstallments = (
  accounts: Object[],
): ShortcutType[] => {
  if (!accounts) {
    return [];
  }

  const accountShortcuts = accounts.map(({shortcuts = []}) => shortcuts) || [];
  const uniqueShortcutCodes: string[] = [];
  const allowedShortcuts: ShortcutType[] = DEFAULT_INSTALLMENT_SHORTCUTS.map(
    (shortcut) => {
      return {
        ...shortcut,
        enabled: false,
      };
    },
  );

  accountShortcuts.forEach(
    (shortcuts) =>
      shortcuts &&
      shortcuts
        .filter((shortcut) =>
          INSTALLMENT_SHORTCUT_CODES.includes(shortcut.code),
        )
        .forEach((shortcut, index) => {
          if (
            !uniqueShortcutCodes.includes(shortcut.code) &&
            shortcut.enabled
          ) {
            uniqueShortcutCodes.push(shortcut.code);
            allowedShortcuts[
              allowedShortcuts.findIndex((d) => d.code === shortcut.code)
            ].enabled = true;
          }

          return shortcut;
        }),
  );

  return allowedShortcuts;
};

export const DEFAULT_INSTALLMENT_SHORTCUTS: ShortcutType[] = [
  {
    code: 'CASH2GO INSTL',
    credit: true,
    debit: false,
    enabled: false,
    forShortcut: true,
    icon: 'icon.png',
    id: 342,
    name: 'CASH2GO INSTALLMENT',
  },
  {
    code: 'BALANCE CONVERSION',
    credit: true,
    debit: false,
    enabled: false,
    forShortcut: false,
    icon: 'icon.png',
    id: 405,
    name: 'BALANCE CONVERSION',
  },
  {
    code: 'BALANCE TRANSFER',
    credit: true,
    debit: false,
    enabled: false,
    forShortcut: false,
    icon: 'icon.png',
    id: 458,
    name: 'BALANCE TRANSFER',
  },
];

export const DEFAULT_INSTALLMENT_CHECKED: number[] = DEFAULT_INSTALLMENT_SHORTCUTS.map(
  (shortcut) => shortcut.id,
);

export const SUPPORTED_SHORTCUTS: number[] = [
  1, // SEND_MONEY
  3, // PAY_BILLS
];

export const DEFAULT_CHECKED_SHORTCUTS: number[] = SUPPORTED_SHORTCUTS;
