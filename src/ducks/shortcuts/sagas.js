/**
 * @flow
 */

import {takeLatest, put, select} from 'redux-saga/effects';

import * as actionTypes from './actionTypes';
import {
  DEFAULT_CHECKED_SHORTCUTS,
  mergeAndFilterShortcuts,
  mergeAndFilterShortcutsForInstallments,
} from './utils';

import {getAllAccounts} from '@ducks/commonSelectors';
import {
  updateCheckedShortcuts,
  updateInstallmentShortcuts,
  updateShortcuts,
} from './actions';
import {creditSelector} from '@ducks/credit/selectors';

export function* initializeShortcutWidget(): Generator<any, any, any> {
  const {all: allAccounts} = yield select(getAllAccounts);

  const newShortcuts = mergeAndFilterShortcuts(allAccounts);

  yield put(updateShortcuts(newShortcuts));

  const checkedShortcutsIds: number[] = DEFAULT_CHECKED_SHORTCUTS;

  yield put(updateCheckedShortcuts(checkedShortcutsIds));
}

export function* initializeCardInstallmentWidget(): Generator<any, any, any> {
  const {data: creditAccounts} = yield select(creditSelector);
  const newInstallmentsShortcuts = mergeAndFilterShortcutsForInstallments(
    creditAccounts,
  );

  yield put(updateInstallmentShortcuts(newInstallmentsShortcuts));
}

export default function* watcher(): any {
  yield takeLatest(actionTypes.INITIALIZE_SHORTCUTS, initializeShortcutWidget);
  yield takeLatest(
    actionTypes.INITIALIZE_INSTALLMENTS_SHORTCUTS,
    initializeCardInstallmentWidget,
  );
}
