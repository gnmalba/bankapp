// @flow

import type {TermsConditionActions} from './types';

export const BalTranstermsConditionRequest = (
  payload: any,
): TermsConditionActions => ({
  type: 'BAL_TRANS_TERMS_CONDITION_REQUEST',
});

export const BalTranstermsConditionSuccess = (
  payload: any,
): TermsConditionActions => ({
  type: 'BAL_TRANS_TERMS_CONDITION_SUCCESS',
  payload: {data: payload},
});

export const BalTranstermsConditionFailed = (): TermsConditionActions => ({
  type: 'BAL_TRANS_TERMS_CONDITION_FAILED',
  payload: {data: null},
});

export const BalConverttermsConditionRequest = (
  payload: any,
): TermsConditionActions => ({
  type: 'BAL_CONVERT_TERMS_CONDITION_REQUEST',
});

export const BalConverttermsConditionSuccess = (
  payload: any,
): TermsConditionActions => ({
  type: 'BAL_CONVERT_TERMS_CONDITION_SUCCESS',
  payload: {data: payload},
});

export const BalConverttermsConditionFailed = (): TermsConditionActions => ({
  type: 'BAL_CONVERT_TERMS_CONDITION_FAILED',
  payload: {data: null},
});

export const C2gotermsConditionRequest = (
  payload: any,
): TermsConditionActions => ({
  type: 'C2GO_TERMS_CONDITION_REQUEST',
});

export const C2goConverttermsConditionSuccess = (
  payload: any,
): TermsConditionActions => ({
  type: 'C2GO_TERMS_CONDITION_SUCCESS',
  payload: {data: payload},
});

export const C2goConverttermsConditionFailed = (): TermsConditionActions => ({
  type: 'C2GO_TERMS_CONDITION_FAILED',
  payload: {data: null},
});
