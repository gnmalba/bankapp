// @flow

type TermsConditonTypes = {
  status: boolean,
  termsCondition: Object,
};
export type TermsConditionStateType = {
  balanceTransfer: TermsConditonTypes,
  balanceConversion: TermsConditonTypes,
  cash2Go: TermsConditonTypes,
};
export type TermsConditionActions =
  | {
      type: 'BAL_TRANS_TERMS_CONDITION_REQUEST',
    }
  | {
      type: 'BAL_TRANS_TERMS_CONDITION_SUCCESS',
      +payload: {data: Object},
    }
  | {
      type: 'BAL_TRANS_TERMS_CONDITION_FAILED',
    }
  | {
      type: 'BAL_CONVERT_TERMS_CONDITION_REQUEST',
    }
  | {
      type: 'BAL_CONVERT_TERMS_CONDITION_SUCCESS',
      +payload: {data: Object},
    }
  | {
      type: 'BAL_CONVERT_TERMS_CONDITION_FAILED',
    }
  | {
      type: 'C2GO_TERMS_CONDITION_REQUEST',
    }
  | {
      type: 'C2GO_TERMS_CONDITION_SUCCESS',
      +payload: {data: Object},
    }
  | {
      type: 'C2GO_TERMS_CONDITION_FAILED',
    };
