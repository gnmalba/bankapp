// @flow

import type {TermsConditionStateType, TermsConditionActions} from './types';

const INITIAL_STATE: TermsConditionStateType = {
  balanceTransfer: {
    status: false,
    termsCondition: [],
  },
  balanceConversion: {
    status: false,
    termsCondition: [],
  },
  cash2Go: {
    status: false,
    termsCondition: [],
  },
};

const installment = (
  state: TermsConditionStateType = INITIAL_STATE,
  action: TermsConditionActions,
): TermsConditionStateType => {
  switch (action.type) {
    case 'BAL_TRANS_TERMS_CONDITION_REQUEST': {
      return {
        ...state,
        balanceTransfer: {
          ...state.balanceTransfer,
          status: true,
        },
      };
    }
    case 'BAL_TRANS_TERMS_CONDITION_SUCCESS': {
      return {
        ...state,
        balanceTransfer: {
          ...state.balanceTransfer,
          status: false,
          termsCondition: action.payload.data,
        },
      };
    }
    case 'BAL_TRANS_TERMS_CONDITION_FAILED': {
      return {
        ...state,
        balanceTransfer: {
          ...state.balanceTransfer,
          status: false,
          termsCondition: [],
        },
      };
    }
    case 'BAL_CONVERT_TERMS_CONDITION_REQUEST': {
      return {
        ...state,
        balanceConversion: {
          ...state.balanceConversion,
          status: true,
        },
      };
    }
    case 'BAL_CONVERT_TERMS_CONDITION_SUCCESS': {
      return {
        ...state,
        balanceConversion: {
          ...state.balanceConversion,
          status: false,
          termsCondition: action.payload.data,
        },
      };
    }
    case 'BAL_CONVERT_TERMS_CONDITION_FAILED': {
      return {
        ...state,
        balanceConversion: {
          ...state.balanceConversion,
          status: false,
          termsCondition: [],
        },
      };
    }
    case 'C2GO_TERMS_CONDITION_REQUEST': {
      return {
        ...state,
        cash2Go: {
          ...state.cash2Go,
          paymentTermsList: [],
          status: true,
          termsCondition: [],
        },
      };
    }
    case 'C2GO_TERMS_CONDITION_SUCCESS': {
      return {
        ...state,
        cash2Go: {
          ...state.cash2Go,
          paymentTermsList: [],
          status: false,
          termsCondition: action.payload.data,
        },
      };
    }
    case 'C2GO_TERMS_CONDITION_FAILED': {
      return {
        ...state,
        cash2Go: {
          ...state.cash2Go,
          paymentTermsList: [],
          status: false,
          termsCondition: [],
        },
      };
    }
    default:
      return state;
  }
};

export default installment;
