import type {TermsConditionStateType} from './types';
import type {State} from '@ducks/types';

export const installmentsSelector = (state: State): TermsConditionStateType =>
  state.termscondition;
