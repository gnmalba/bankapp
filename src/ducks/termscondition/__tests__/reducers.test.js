import reducer from '../reducer';
// import {CardActivationState, CardActivationAction} from '../types';
import type {TermsConditionStateType} from './types';
const INITIAL_STATE: TermsConditionStateType = {
  balanceTransfer: {
    status: false,
    termsCondition: [],
  },
  balanceConversion: {
    status: false,
    termsCondition: [],
  },
  cash2Go: {
    status: false,
    termsCondition: [],
  },
};
describe('Terms and Condition reducer', () => {
  test('BAL_TRANS_TERMS_CONDITION_REQUEST', () => {
    const action = {
      type: 'BAL_TRANS_TERMS_CONDITION_REQUEST',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      balanceTransfer: {
        ...INITIAL_STATE.balanceTransfer,
        status: true,
      },
    });
  });

  test('BAL_TRANS_TERMS_CONDITION_SUCCESS', () => {
    const action = {
      type: 'BAL_TRANS_TERMS_CONDITION_SUCCESS',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      balanceTransfer: {
        ...INITIAL_STATE.balanceTransfer,
        status: false,
        termsCondition: action.payload.data,
      },
    });
  });

  test('BAL_TRANS_TERMS_CONDITION_FAILED', () => {
    const action = {
      type: 'BAL_TRANS_TERMS_CONDITION_FAILED',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      balanceTransfer: {
        ...INITIAL_STATE.balanceTransfer,
        status: false,
        termsCondition: action.payload.data,
      },
    });
  });

  test('BAL_CONVERT_TERMS_CONDITION_REQUEST', () => {
    const action = {
      type: 'BAL_CONVERT_TERMS_CONDITION_REQUEST',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      balanceConversion: {
        ...INITIAL_STATE.balanceConversion,
        status: true,
      },
    });
  });

  test('BAL_CONVERT_TERMS_CONDITION_SUCCESS', () => {
    const action = {
      type: 'BAL_CONVERT_TERMS_CONDITION_SUCCESS',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      balanceConversion: {
        ...INITIAL_STATE.balanceConversion,
        status: false,
        termsCondition: action.payload.data,
      },
    });
  });

  test('BAL_CONVERT_TERMS_CONDITION_FAILED', () => {
    const action = {
      type: 'BAL_CONVERT_TERMS_CONDITION_FAILED',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      balanceConversion: {
        ...INITIAL_STATE.balanceConversion,
        status: false,
        termsCondition: action.payload.data,
      },
    });
  });

  test('C2GO_TERMS_CONDITION_REQUEST', () => {
    const action = {
      type: 'C2GO_TERMS_CONDITION_REQUEST',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      cash2Go: {
        ...INITIAL_STATE.cash2Go,
        status: true,
        paymentTermsList: [],
        termsCondition: action.payload.data,
      },
    });
  });

  test('C2GO_TERMS_CONDITION_SUCCESS', () => {
    const action = {
      type: 'C2GO_TERMS_CONDITION_SUCCESS',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      cash2Go: {
        ...INITIAL_STATE.cash2Go,
        status: false,
        paymentTermsList: [],
        termsCondition: action.payload.data,
      },
    });
  });

  test('C2GO_TERMS_CONDITION_FAILED', () => {
    const action = {
      type: 'C2GO_TERMS_CONDITION_FAILED',
      payload: {data: []},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      cash2Go: {
        ...INITIAL_STATE.cash2Go,
        status: false,
        paymentTermsList: [],
        termsCondition: action.payload.data,
      },
    });
  });
});
