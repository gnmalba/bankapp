import {
  BalTranstermsConditionRequest,
  BalTranstermsConditionSuccess,
  BalTranstermsConditionFailed,
  BalConverttermsConditionRequest,
  BalConverttermsConditionSuccess,
  BalConverttermsConditionFailed,
  C2gotermsConditionRequest,
  C2goConverttermsConditionSuccess,
  C2goConverttermsConditionFailed,
} from '../actions';

describe('BalTranstermsConditionRequest', () => {
  test('BalTranstermsConditionRequest', () => {
    const el = BalTranstermsConditionRequest();
    expect(el).toBeDefined();
  });
});

describe('BalTranstermsConditionSuccess', () => {
  test('BalTranstermsConditionSuccess', () => {
    const el = BalTranstermsConditionSuccess();
    expect(el).toBeDefined();
  });
});

describe('BalTranstermsConditionFailed', () => {
  test('BalTranstermsConditionFailed', () => {
    const el = BalTranstermsConditionFailed();
    expect(el).toBeDefined();
  });
});
describe('BalConverttermsConditionRequest', () => {
  test('BalConverttermsConditionRequest', () => {
    const el = BalConverttermsConditionRequest();
    expect(el).toBeDefined();
  });
});

describe('BalConverttermsConditionSuccess', () => {
  test('BalConverttermsConditionSuccess', () => {
    const el = BalConverttermsConditionSuccess();
    expect(el).toBeDefined();
  });
});

describe('BalConverttermsConditionFailed', () => {
  test('BalConverttermsConditionFailed', () => {
    const el = BalConverttermsConditionFailed();
    expect(el).toBeDefined();
  });
});

describe('C2gotermsConditionRequest', () => {
  test('C2gotermsConditionRequest', () => {
    const el = C2gotermsConditionRequest();
    expect(el).toBeDefined();
  });
});

describe('C2goConverttermsConditionSuccess', () => {
  test('C2goConverttermsConditionSuccess', () => {
    const el = C2goConverttermsConditionSuccess();
    expect(el).toBeDefined();
  });
});

describe('C2goConverttermsConditionFailed', () => {
  test('C2goConverttermsConditionFailed', () => {
    const el = C2goConverttermsConditionFailed();
    expect(el).toBeDefined();
  });
});
