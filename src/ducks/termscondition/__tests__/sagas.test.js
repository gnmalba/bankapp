import {expectSaga} from 'redux-saga-test-plan';
import {
  getBalConvertRequests,
  getBalTransRequest,
  getCash2GoRequests,
} from '../sagas';

describe('Activate Card Sagas', () => {
  test('Should run balance conversion', async () => {
    expectSaga(getBalConvertRequests, {}).run();
  });

  test('Should run balance transfer ', async () => {
    expectSaga(getBalTransRequest, {}).run();
  });

  test('Should run cash 2 go', async () => {
    expectSaga(getCash2GoRequests, {}).run();
  });
});
