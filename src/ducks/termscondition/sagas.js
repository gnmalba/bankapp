// @flow

import {takeLatest, call, put} from 'redux-saga/effects';
import InstallmentsConfig from '@api/services/installmentService';
import {
  BalTranstermsConditionSuccess,
  BalConverttermsConditionSuccess,
  C2goConverttermsConditionSuccess,
} from './actions';

export function* getBalTransRequest(action: any): any {
  try {
    const {params} = action.payload;
    const {data} = yield call(InstallmentsConfig.viewTermsCondition, params);
    yield put(BalTranstermsConditionSuccess(data));
  } catch (error) {
    console.log({error});
  }
}
export function* getBalConvertRequests(action: any): any {
  try {
    const {params} = action.payload;
    const {data} = yield call(InstallmentsConfig.viewTermsCondition, params);
    yield put(BalConverttermsConditionSuccess(data));
  } catch (error) {
    console.log({error});
  }
}
export function* getCash2GoRequests(action: any): any {
  try {
    console.log('test');
    const {params} = action.payload;
    const {data} = yield call(InstallmentsConfig.viewTermsCondition, params);
    yield put(C2goConverttermsConditionSuccess(data));
  } catch (error) {
    console.log({error});
  }
}

export default function* watcher(): Generator<any, any, any> {
  yield takeLatest('BAL_TRANS_TERMS_CONDITION_REQUEST', getBalTransRequest);
  yield takeLatest(
    'BAL_CONVERT_TERMS_CONDITION_REQUEST',
    getBalConvertRequests,
  );
  yield takeLatest('C2GO_TERMS_CONDITION_REQUEST', getCash2GoRequests);
}
