/**
 * @format
 * @flow
 */

import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {logger} from 'redux-logger';
import rootReducers from './rootReducer';
import rootSaga from './rootSaga';
import type {Store} from './types';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

if (__DEV__) {
  middlewares.push(logger);
}

const store: Store = createStore(rootReducers, applyMiddleware(...middlewares));

if (__DEV__ && module.hot) {
  module.hot.accept(() => {
    const nextReducer: any = require('./rootReducer').default;
    store.replaceReducer(nextReducer);
  });
}

sagaMiddleware.run(rootSaga);

export default store;
