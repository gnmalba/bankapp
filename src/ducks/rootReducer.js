/**
 * @format
 * @flow
 */

import {combineReducers} from 'redux';
import type {CombinedReducer} from 'redux-types';

import type {State, Action} from '@ducks/types';

import config from './config/reducer';
import authentication from './authentication/reducer';
import biometrics from './biometrics/reducer';
import deposit from './deposit/reducer';
import dashboard from './dashboard/reducer';
import credit from './credit/reducer';
import enrollment from './enrollment/reducer';
import installments from './installments/reducer';
import reportLostCard from './lostcard/reducer';
import manageAccountSettings from './manageAccountSettings/reducer';
import depositprepaidoverview from './depositprepaidoverview/reducer';
import yazz from './yazz/reducer';
import soa from './soa/reducer';
import billers from './billers/reducer';
import paybillscredit from './pay-bills-credit/reducer';
import contacts from './contacts/reducer';
import schedules from './schedules/reducer';
import termsCondition from './termscondition/reducer';
import accountDisplayOptions from './accounts-displayoptions/reducer';
import cardActivation from './card-activation/reducer';
import recoverAccess from './recover-access/reducers';
import sendMoney from './sendMoney/reducer';
import manageSettings from './managesettings/reducer';
import shortcuts from './shortcuts/reducer';

import {AUTHENTICATION_LOGOUT} from '@ducks/authentication/actionTypes';

const appReducer = combineReducers({
  config,
  recoverAccess,
  authentication,
  biometrics,
  deposit,
  dashboard,
  credit,
  enrollment,
  installments,
  reportLostCard,
  manageAccountSettings,
  depositprepaidoverview,
  yazz,
  soa,
  billers,
  paybillscredit,
  contacts,
  schedules,
  termsCondition,
  accountDisplayOptions,
  sendMoney,
  cardActivation,
  manageSettings,
  shortcuts,
});

const rootReducer = (
  state: State,
  action: Action,
): CombinedReducer<State, Action> => {
  if (action.type === AUTHENTICATION_LOGOUT) {
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};

export default rootReducer;
