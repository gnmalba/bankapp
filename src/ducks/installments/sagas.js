// @flow

import type {
  InstallmentBankType,
  PaymentTermsRequest,
  BalanceTransferSaveRequest,
  BalanceConversionSaveRequest,
  Cash2GoSaveRequest,
  ViewInstallmentsRequestType,
  MoreInformationRequest,
  InformationType,
  InstallmentStateType,
} from '@ducks/installments/types';

import {takeLatest, call, put, select} from 'redux-saga/effects';
import InstallmentsConfig from '@api/services/installmentService';
import {
  balanceConversionSuccess,
  balanceTransferSuccess,
  getBankListSuccess,
  getPaymentTermsSuccess,
  cash2GoSuccess,
  getInstallmentsSuccess,
  sortInstallmentsSuccess,
  filterInstallmentsSuccess,
  getInformationModalSuccess,
  getInformationModalFailed,
  setInformationModalVisible,
  setCurrentInstallmentType,
  saveInstallmentsError,
  viewInstallmentsError,
  paymentTermsError,
  getBankListError,
} from './actions';
import {getCustomerID, getEncryptionPublicKey} from '@ducks/commonSelectors';
import encryptString from '@utils/encryptString';
import {installmentsSelector} from './selectors';
import * as NavigationService from '@navigations/service';
import {
  combineParams,
  getCardSerNoCreditState,
  getErrorMessage,
  hasMaximumAttempts,
  sortBanks,
  INSTALLMENTS_STATUS_FILTERS,
  sortByStatus,
  getErrorCode,
  ERROR_SCREEN_STR,
  SORT_OPTIONS,
} from '@utils/installments';
import {creditSelector} from '@ducks/credit/selectors';
import {ASYNC_STORAGE_KEYS, INSTALLLMENT_INFO_FALLBACK} from '@utils/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';

export function* getBankList(action: any): any {
  try {
    const {installmentType} = action.payload;

    const {data} = yield call(InstallmentsConfig.getBanks, installmentType);
    const banks: InstallmentBankType[] = sortBanks(data.data);
    yield put(getBankListSuccess(banks, false));
  } catch (error) {
    yield put(getBankListError());
    console.log({error});
  }
}

function* getPaymentTerms(action: any): any {
  const params: PaymentTermsRequest = action.payload.params;
  const customerId = yield select(getCustomerID);
  const installmentsState = yield select(installmentsSelector);
  const installmentType = installmentsState.currentInstallmentType;
  params.customerId = customerId;

  try {
    const {data} = yield call(InstallmentsConfig.getPaymentTerms, params);
    yield put(getPaymentTermsSuccess(data.data, installmentType, false));
  } catch (error) {
    yield put(paymentTermsError());
    console.log({error});
  }
}

function* saveBalanceTransfer(action: any) {
  const params: BalanceTransferSaveRequest = action.payload;
  const auth = yield select((state) => state.authentication);
  const epk = yield select(getEncryptionPublicKey);

  const rawPasscode = params.passcode;

  params.username = auth.username;
  params.passcode = encryptString(epk).encrypt(params.passcode);

  try {
    const {data} = yield call(InstallmentsConfig.balanceTransferSave, params);
    console.log(
      '%c success balance transfer',
      'background-color: green; font-weight: bold',
    );
    yield put(
      balanceTransferSuccess({
        responseData: data.data,
        dateTime: data.datetime,
      }),
    );

    NavigationService.navigate('ReviewScreen');
  } catch (error) {
    if (hasMaximumAttempts(error.response.data.data)) {
      NavigationService.navigate('MaximumPasscodeScreen');
    } else if (getErrorCode(error.response.data.data) !== 'mpin') {
      NavigationService.navigate(ERROR_SCREEN_STR, {
        isSystemError: true,
      });
    }

    yield put(
      saveInstallmentsError(
        getErrorMessage(error.response.data.data),
        rawPasscode,
      ),
    );
  }
}

function* saveBalanceConversion(action: any) {
  const params: BalanceConversionSaveRequest = action.payload;
  const auth = yield select((state) => state.authentication);
  const epk = yield select(getEncryptionPublicKey);

  const rawPasscode = params.passcode;

  params.username = auth.username;
  params.passcode = encryptString(epk).encrypt(params.passcode);
  try {
    const {data} = yield call(InstallmentsConfig.balanceConversionSave, params);
    console.log(
      '%c success balance conversion',
      'background-color: blue; font-weight: bold',
    );
    yield put(
      balanceConversionSuccess({
        responseData: data.data,
        dateTime: data.datetime,
      }),
    );

    NavigationService.navigate('ReviewScreen');
  } catch (error) {
    if (hasMaximumAttempts(error.response.data.data)) {
      NavigationService.navigate('MaximumPasscodeScreen');
    } else if (getErrorCode(error.response.data.data) !== 'mpin') {
      NavigationService.navigate(ERROR_SCREEN_STR, {
        isSystemError: true,
      });
    }

    yield put(
      saveInstallmentsError(
        getErrorMessage(error.response.data.data),
        rawPasscode,
      ),
    );
  }
}

function* saveCash2Go(action: any) {
  const params: Cash2GoSaveRequest = action.payload;
  const auth = yield select((state) => state.authentication);
  const epk = yield select(getEncryptionPublicKey);

  const rawPasscode = params.passcode;
  params.username = auth.username;
  params.passcode = encryptString(epk).encrypt(params.passcode);

  if (!params.disbursementOption) {
    params.disbursementOption = 'E';
  }

  try {
    const {data} = yield call(InstallmentsConfig.cash2GoSave, params);
    console.log(
      '%c success cash 2 go',
      'background-color: gold; font-weight: bold',
    );
    yield put(
      cash2GoSuccess({
        responseData: data.data,
        dateTime: data.datetime,
      }),
    );

    NavigationService.navigate('ReviewScreen');
  } catch (error) {
    if (hasMaximumAttempts(error.response.data.data)) {
      NavigationService.navigate('MaximumPasscodeScreen');
    } else if (getErrorCode(error.response.data.data) !== 'mpin') {
      NavigationService.navigate(ERROR_SCREEN_STR, {
        isSystemError: true,
      });
    }

    yield put(
      saveInstallmentsError(
        getErrorMessage(error.response.data.data),
        rawPasscode,
      ),
    );
  }
}

function* sortInstallments(action: any) {
  const {params} = action.payload;
  const customerId = yield select(getCustomerID);

  params.customerId = customerId;
  const {installmentType, sortedBy} = params;
  const installmentsState = yield select(installmentsSelector);
  let combinedParams = combineParams(
    installmentsState,
    installmentType,
    params,
  );

  combinedParams = {...combinedParams, sortedBy: params.sortedBy};

  if (combinedParams.status === INSTALLMENTS_STATUS_FILTERS.SHOW_ALL) {
    delete combinedParams.status;
  }

  try {
    const {data} = yield call(
      InstallmentsConfig.sortInstallments,
      combinedParams,
    );

    console.log(
      '%c fetched installments successfully!',
      'background-color: #ae34eb; font-weight: bold',
    );

    let sortedInstallments: any[] = data.data;
    if (sortedBy === SORT_OPTIONS.status) {
      sortedInstallments = sortByStatus(sortedInstallments);
    }

    yield put(
      sortInstallmentsSuccess({
        response: sortedInstallments,
        installmentType,
        sortedBy,
        filterParams: combinedParams,
      }),
    );
  } catch (error) {
    yield put(viewInstallmentsError());
    console.log({error});
  }
}

function* getAllInstallments(action: any) {
  const params: ViewInstallmentsRequestType = action.payload.params;

  const customerId = yield select(getCustomerID);
  params.param = 'ALLTXN';
  params.customerId = customerId;

  try {
    const btParams: ViewInstallmentsRequestType = {
      ...params,
      installmentType: 'BT',
    };
    const bcParams: ViewInstallmentsRequestType = {
      ...params,
      installmentType: 'BC',
    };
    const c2gParams: ViewInstallmentsRequestType = {
      ...params,
      installmentType: 'C2GO',
    };

    const {data: bt} = yield call(
      InstallmentsConfig.viewInstallments,
      btParams,
    );
    const {data: bc} = yield call(
      InstallmentsConfig.viewInstallments,
      bcParams,
    );
    const {data: c2g} = yield call(
      InstallmentsConfig.viewInstallments,
      c2gParams,
    );

    const allInstallments = {
      bt: sortByStatus(bt.data),
      bc: sortByStatus(bc.data),
      c2g: sortByStatus(c2g.data),
    };

    console.log(
      '%c fetched installments successfully',
      'background-color: beige; font-weight: bold',
    );
    yield put(getInstallmentsSuccess(allInstallments));
  } catch (error) {
    yield put(viewInstallmentsError());
    console.log({error});
  }
}

function* filterInstallments(action: any) {
  let {params} = action.payload;
  const customerId = yield select(getCustomerID);
  const creditState = yield select(creditSelector);
  const installmentsState = yield select(installmentsSelector);

  params.customerId = customerId;
  params.cardSerNo = getCardSerNoCreditState(creditState.data);

  let tempParams = combineParams(
    installmentsState,
    params.installmentType,
    params,
  );
  params = {...tempParams, ...params};

  const copyParams = {...params};

  if (params.status === INSTALLMENTS_STATUS_FILTERS.SHOW_ALL) {
    params.status = null;
  }
  if (!params.sortedBy) {
    params.sortedBy = SORT_OPTIONS.status;
  }

  try {
    const {data} = yield call(InstallmentsConfig.sortInstallments, params);
    const response = data.data;
    const actionPayload = {
      response:
        params.sortedBy === SORT_OPTIONS.status || !params.sortedBy
          ? sortByStatus(response)
          : response,
      params: copyParams,
    };
    console.log(
      '%c fetched installments successfully',
      'background-color: lightblue; font-weight: bold',
    );
    yield put(filterInstallmentsSuccess(actionPayload));
  } catch (error) {
    console.log({error});
    yield put(viewInstallmentsError());
  }
}

export function* getMoreInformation(action: any): any {
  const {installmentType} = action.payload;

  try {
    const customerId = yield select(getCustomerID);
    const params: MoreInformationRequest = {customerId, installmentType};

    const {data} = yield call(InstallmentsConfig.getMoreInformation, params);
    const informations: InformationType[] = data.data;

    // Note: we only get the first item in the array
    if (informations.length) {
      yield put(getInformationModalSuccess(informations.pop(), false));
    } else {
      yield put(
        getInformationModalSuccess(
          INSTALLLMENT_INFO_FALLBACK[installmentType],
          false,
        ),
      );
    }
  } catch (error) {
    console.log({error});

    yield put(
      getInformationModalFailed(
        INSTALLLMENT_INFO_FALLBACK[installmentType],
        false,
      ),
    );
  }
}

export function* initializeInstallment(action: any): any {
  const {initialized}: InstallmentStateType = yield select(
    (state) => state.installments,
  );

  if (initialized === false) {
    try {
      const {installmentType} = action.payload;

      yield put(setCurrentInstallmentType(installmentType));

      // Determine if need to show modal automatically
      const value = yield AsyncStorage.getItem(
        ASYNC_STORAGE_KEYS.SHOW_INFO_MODAL[installmentType],
      );

      if (!value) {
        yield put(setInformationModalVisible(true, true));
      }
    } catch (error) {
      console.log({error});
    }
  }
}

export default function* watcher(): Generator<any, any, any> {
  yield takeLatest('GET_BANK_LIST_API_REQUESTED', getBankList);
  yield takeLatest('REQUEST_PAYMENT_TERMS_API', getPaymentTerms);
  yield takeLatest('BALANCE_TRANSFER_SAVE', saveBalanceTransfer);
  yield takeLatest('BALANCE_CONVERSION_SAVE', saveBalanceConversion);
  yield takeLatest('CASH2GO_SAVE', saveCash2Go);
  yield takeLatest('REQUEST_INSTALLMENTS', getAllInstallments);
  yield takeLatest('SORT_INSTALLMENTS_REQUEST', sortInstallments);
  yield takeLatest('FILTER_INSTALLMENTS_REQUEST', filterInstallments);
  yield takeLatest('GET_INFORMATION_FOR_MODAL_REQUESTED', getMoreInformation);
  yield takeLatest('INITIALIZE_INSTALLMENT', initializeInstallment);
}
