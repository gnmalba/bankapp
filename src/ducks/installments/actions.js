// @flow

import type {
  InstallmentsAction,
  BalanceTransferFromType,
  BalanceTransferToType,
  PaymentTermsType,
  InstallmentStateType,
  PaymentTermsRequest,
  BalanceTransferSaveRequest,
  BalanceConversionSaveRequest,
  Cash2GoDepositToType,
  Cash2GoSaveRequest,
  ViewInstallmentsRequestType,
  InstallmentTypesString,
  InformationType,
  InstallmentBankType,
  InstallmentRootScreenType,
} from './types';

export const setCurrentInstallmentType = (
  installmentType: string,
): InstallmentsAction => ({
  type: 'SET_CURRENT_INSTALLMENT_TYPE',
  payload: {data: installmentType},
});

export const setSelectedCreditforInstallments = (
  selectedCredit: ?Object,
): InstallmentsAction => ({
  type: 'SET_SELECTED_CREDIT_INSTALLMENTS',
  payload: {selectedCredit},
});

export const getBankListRequested = (
  installmentType: string,
): InstallmentsAction => ({
  type: 'GET_BANK_LIST_API_REQUESTED',
  payload: {installmentType},
});

export const getBankListSuccess = (
  data: Array<InstallmentBankType>,
  visible: boolean,
): InstallmentsAction => ({
  type: 'GET_BANK_LIST_API_SUCCESS',
  payload: {
    banks: data,
    visible,
  },
});

export const getBankListError = (): InstallmentsAction => ({
  type: 'GET_BANK_LIST_API_ERROR',
});

export const initializeInstallment = (
  installmentType: InstallmentTypesString,
  visible: boolean,
): InstallmentsAction => ({
  type: 'INITIALIZE_INSTALLMENT',
  payload: {installmentType, visible},
});

export const setInformationModalVisible = (
  visible: boolean,
  isInitial: boolean = false,
): InstallmentsAction => ({
  type: 'SET_INFORMATION_MODAL_VISIBLE',
  payload: {visible, isInitial},
});

export const getInformationModalRequested = (
  installmentType: InstallmentTypesString,
): InstallmentsAction => ({
  type: 'GET_INFORMATION_FOR_MODAL_REQUESTED',
  payload: {installmentType},
});

export const getInformationModalSuccess = (
  data: InformationType,
  visible: boolean,
): InstallmentsAction => ({
  type: 'GET_INFORMATION_FOR_MODAL_SUCCESS',
  payload: {data, visible},
});

export const getInformationModalFailed = (
  data: InformationType,
  visible: boolean,
): InstallmentsAction => ({
  type: 'GET_INFORMATION_FOR_MODAL_FAILED',
  payload: {data, visible},
});

export const updateBalanceTransferFrom = (
  data: BalanceTransferFromType | null,
): InstallmentsAction => ({
  type: 'UPDATE_BALANCE_TRANSFER_FROM',
  payload: {
    data,
  },
});

export const updateBalanceTransferTo = (
  data: BalanceTransferToType | null,
  isFormDirty: boolean = true,
): InstallmentsAction => ({
  type: 'UPDATE_BALANCE_TRANSFER_TO',
  payload: {
    data,
    isFormDirty,
  },
});

export const updateBalanceConversionFrom = (
  data: any,
  isFormDirty: boolean = true,
  visible: boolean,
): InstallmentsAction => ({
  type: 'UPDATE_BALANCE_CONVERSION_FROM',
  payload: {data, isFormDirty, visible},
});

export const updateCash2GoFrom = (
  data: any,
  isFormDirty: boolean = true,
  visible: boolean,
): InstallmentsAction => ({
  type: 'UPDATE_CASH2GO_FROM',
  payload: {data, isFormDirty, visible},
});

export const getPaymentTerms = (
  params: PaymentTermsRequest,
): InstallmentsAction => ({
  type: 'REQUEST_PAYMENT_TERMS_API',
  payload: {params},
});

export const getPaymentTermsSuccess = (
  data: Array<PaymentTermsType>,
  installmentType: InstallmentTypesString,
  visible: boolean,
): InstallmentsAction => ({
  type: 'REQUEST_PAYMENT_TERMS_API_SUCCESS',
  payload: {paymentTerms: data, installmentType, visible},
});

export const updatePaymentTerms = (
  installmentType: InstallmentTypesString,
  data: Object,
): InstallmentsAction => ({
  type: 'UPDATE_PAYMENT_TERMS',
  payload: {
    data,
    installmentType,
  },
});

export const updateAmount = (
  installmentType: InstallmentStateType,
  data: number,
): InstallmentsAction => ({
  type: 'UPDATE_AMOUNT',
  payload: {
    data,
    installmentType,
  },
});

export const updateCash2GoTo = (
  data: Cash2GoDepositToType,
): InstallmentsAction => ({
  type: 'UPDATE_CASH2GO_TO',
  payload: {data},
});

export const saveBalanceTransfer = (
  payload: BalanceTransferSaveRequest,
): InstallmentsAction => ({
  type: 'BALANCE_TRANSFER_SAVE',
  payload,
});

export const saveBalanceConversion = (
  payload: BalanceConversionSaveRequest,
): InstallmentsAction => ({
  type: 'BALANCE_CONVERSION_SAVE',
  payload,
});

export const saveCash2Go = (
  payload: Cash2GoSaveRequest,
): InstallmentsAction => ({
  type: 'CASH2GO_SAVE',
  payload,
});

export const balanceTransferSuccess = (payload: any): InstallmentsAction => ({
  type: 'BALANCE_TRANSFER_SUCCESS',
  payload,
});

export const balanceConversionSuccess = (payload: any): InstallmentsAction => ({
  type: 'BALANCE_CONVERSION_SUCCESS',
  payload,
});

export const cash2GoSuccess = (payload: any): InstallmentsAction => ({
  type: 'CASH2GO_SUCCESS',
  payload,
});

export const getInstallments = (
  params: ViewInstallmentsRequestType,
): InstallmentsAction => ({
  type: 'REQUEST_INSTALLMENTS',
  payload: {params},
});

export const getInstallmentsSuccess = (payload: any): InstallmentsAction => ({
  type: 'REQUEST_INSTALLMENTS_SUCCESS',
  payload,
});

export const sortInstallmentsRequest = (params: any): InstallmentsAction => ({
  type: 'SORT_INSTALLMENTS_REQUEST',
  payload: {params},
});

export const sortInstallmentsSuccess = (payload: any): InstallmentsAction => ({
  type: 'SORT_INSTALLMENTS_SUCCESS',
  payload: {data: payload},
});

export const filterInstallmentsRequest = (params: any): InstallmentsAction => ({
  type: 'FILTER_INSTALLMENTS_REQUEST',
  payload: {params},
});

export const filterInstallmentsSuccess = (
  payload: any,
): InstallmentsAction => ({
  type: 'FILTER_INSTALLMENTS_SUCCESS',
  payload: {data: payload},
});
export const clearInstallments = (): InstallmentsAction => ({
  type: 'CLEAR_INSTALLMENTS_STATE',
});

export const paymentTermsError = (): InstallmentsAction => ({
  type: 'REQUEST_PAYMENT_TERMS_ERROR',
});

export const saveInstallmentsError = (
  errorMsg: any,
  passcode: string,
): InstallmentsAction => ({
  type: 'SAVE_INSTALLMENTS_ERROR',
  payload: {data: errorMsg, passcode},
});

export const viewInstallmentsError = (): InstallmentsAction => ({
  type: 'VIEW_INSTALLMENTS_ERROR',
});

export const setLeavePrompt = (visible: boolean): InstallmentsAction => ({
  type: 'SET_LEAVE_PROMPT',
  payload: {visible},
});

export const setShowReviewHeader = (visible: boolean): InstallmentsAction => ({
  type: 'SET_SHOW_REVIEW_HEADER',
  payload: {visible},
});

export const setRootScreen = (
  screen: InstallmentRootScreenType,
): InstallmentsAction => ({
  type: 'SET_ROOT_SCREEN',
  payload: {screen},
});

export const clearInstallmentErrors = (): InstallmentsAction => ({
  type: 'CLEAR_INSTALLMENT_ERRORS',
});
