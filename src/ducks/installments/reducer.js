// @flow

import {
  getUpdatePaymentTermsState,
  handleUpdateAmount,
} from '@ducks/helpers/installments';
import {mapSortStr} from '@utils/installments';
import type {InstallmentStateType, InstallmentsAction} from './types';

export const INITIAL_STATE: InstallmentStateType = {
  balanceTransfer: {
    paymentTermsList: [],
  },
  balanceConversion: {
    paymentTermsList: [],
  },
  cash2Go: {
    paymentTermsList: [],
  },
  banks: [],
  formLoading: false,
  allInstallmentsLoading: false,
  allInstallments: {},
  sortedBy: {bt: 0, bc: 0, c2g: 0},
  filters: {},
  information: {
    visible: false,
    loading: false,
    data: null,
    isInitial: false,
  },
  currentInstallmentType: '',
  currentFilterInstallmentType: '',
  initialized: false,
  leavePromptVisible: false,
  isFormDirty: false,
  saveInstallmentsError: null,
  paymentTermsError: false,
  showReviewScreen: false,
  selectedCredit: null,
  rootScreen: '',
};

const installment = (
  state: InstallmentStateType = INITIAL_STATE,
  action: InstallmentsAction,
): InstallmentStateType => {
  // TODO: Refactor these and split into smaller components
  // eslint-disable-next-line sonarjs/max-switch-cases
  switch (action.type) {
    case 'GET_BANK_LIST_API_REQUESTED':
      return {...state, formLoading: true};

    case 'GET_BANK_LIST_API_SUCCESS':
      return {
        ...state,
        banks: action.payload.banks,
        formLoading: false,
      };

    case 'GET_BANK_LIST_API_ERROR': {
      return {
        ...state,
        banks: [],
        formLoading: false,
      };
    }

    case 'UPDATE_BALANCE_TRANSFER_FROM':
      return {
        ...state,
        balanceTransfer: {
          ...state.balanceTransfer,
          transferFrom: action.payload.data,
        },
        isFormDirty: true,
      };

    case 'UPDATE_BALANCE_CONVERSION_FROM':
      return {
        ...state,
        balanceConversion: {
          ...state.balanceConversion,
          from: action.payload.data,
          amount: null,
          paymentTerms: null,
        },
        isFormDirty: action.payload.isFormDirty,
      };

    case 'UPDATE_CASH2GO_FROM':
      return {
        ...state,
        cash2Go: {
          ...state.cash2Go,
          from: action.payload.data,
          amount: null,
          paymentTerms: null,
        },
        isFormDirty: action.payload.isFormDirty,
      };

    case 'UPDATE_BALANCE_TRANSFER_TO':
      return {
        ...state,
        balanceTransfer: {
          ...state.balanceTransfer,
          transferTo: action.payload.data,
          amount: null,
          paymentTerms: null,
        },
        isFormDirty: action.payload.isFormDirty,
      };

    case 'REQUEST_PAYMENT_TERMS_API_SUCCESS': {
      const {installmentType} = action.payload;

      return getUpdatePaymentTermsState(installmentType, state, action);
    }

    case 'REQUEST_PAYMENT_TERMS_API':
      return {
        ...state,
        formLoading: true,
        paymentTermsError: false,
      };
    case 'BALANCE_TRANSFER_SAVE':
    case 'BALANCE_CONVERSION_SAVE':
    case 'CASH2GO_SAVE':
      return {
        ...state,
        formLoading: true,
        saveInstallmentsError: null,
      };

    case 'UPDATE_PAYMENT_TERMS': {
      const {installmentType, data} = action.payload;
      if (installmentType === 'BT') {
        return {
          ...state,
          balanceTransfer: {...state.balanceTransfer, paymentTerms: data},
          isFormDirty: true,
        };
      } else if (installmentType === 'BC') {
        return {
          ...state,
          balanceConversion: {...state.balanceConversion, paymentTerms: data},
          isFormDirty: true,
        };
      } else if (installmentType === 'C2GO') {
        return {
          ...state,
          cash2Go: {...state.cash2Go, paymentTerms: data},
          isFormDirty: true,
        };
      }

      return state;
    }

    case 'UPDATE_CASH2GO_TO':
      return {
        ...state,
        cash2Go: {
          ...state.cash2Go,
          to: action.payload.data,
        },
        isFormDirty: true,
      };

    case 'UPDATE_AMOUNT': {
      return handleUpdateAmount(state, action);
    }

    case 'SET_CURRENT_INSTALLMENT_TYPE': {
      const installmentType = action.payload.data;
      return {
        ...state,
        currentInstallmentType: installmentType,
        initialized: true,
      };
    }
    case 'BALANCE_TRANSFER_SUCCESS':
      return {
        ...state,
        formLoading: false,
        balanceTransfer: {
          ...state.balanceTransfer,
          ...action.payload,
        },
        currentFilterInstallmentType: 'BT',
        showReviewScreen: true,
      };

    case 'BALANCE_CONVERSION_SUCCESS':
      return {
        ...state,
        formLoading: false,
        balanceConversion: {
          ...state.balanceConversion,
          ...action.payload,
        },
        currentFilterInstallmentType: 'BC',
        showReviewScreen: true,
      };

    case 'CASH2GO_SUCCESS':
      return {
        ...state,
        formLoading: false,
        cash2Go: {
          ...state.cash2Go,
          ...action.payload,
        },
        currentFilterInstallmentType: 'C2GO',
        showReviewScreen: true,
      };

    case 'REQUEST_INSTALLMENTS':
    case 'SORT_INSTALLMENTS_REQUEST':
    case 'FILTER_INSTALLMENTS_REQUEST':
      return {
        ...state,
        allInstallmentsLoading: true,
      };

    case 'REQUEST_INSTALLMENTS_SUCCESS':
      return {
        ...state,
        allInstallments: action.payload,
        allInstallmentsLoading: false,
      };

    case 'VIEW_INSTALLMENTS_ERROR':
      return {
        ...state,
        allInstallmentsLoading: false,
      };

    case 'SORT_INSTALLMENTS_SUCCESS': {
      const {
        installmentType,
        response,
        sortedBy,
        filterParams,
      } = action.payload.data;

      if (installmentType === 'BT') {
        return {
          ...state,
          allInstallmentsLoading: false,
          allInstallments: {
            ...state.allInstallments,
            bt: response,
          },
          sortedBy: {
            ...state.sortedBy,
            bt: mapSortStr(sortedBy),
          },
          filters: {
            ...state.filters,
            bt: {...state.filters.bt, ...filterParams},
          },
          currentFilterInstallmentType: installmentType,
        };
      } else if (installmentType === 'BC') {
        return {
          ...state,
          allInstallmentsLoading: false,
          allInstallments: {
            ...state.allInstallments,
            bc: response,
          },
          sortedBy: {
            ...state.sortedBy,
            bc: mapSortStr(sortedBy),
          },
          filters: {
            ...state.filters,
            bc: {...state.filters.bc, ...filterParams},
          },
          currentFilterInstallmentType: installmentType,
        };
      } else if (installmentType === 'C2GO') {
        return {
          ...state,
          allInstallmentsLoading: false,
          allInstallments: {
            ...state.allInstallments,
            c2g: response,
          },
          sortedBy: {
            ...state.sortedBy,
            c2g: mapSortStr(sortedBy),
          },
          filters: {
            ...state.filters,
            c2g: {...state.filters.c2g, ...filterParams},
          },
          currentFilterInstallmentType: installmentType,
        };
      }

      return {
        ...state,
        allInstallmentsLoading: false,
      };
    }

    case 'FILTER_INSTALLMENTS_SUCCESS': {
      const {params, response} = action.payload.data;
      const {installmentType} = params;

      if (installmentType === 'BT') {
        return {
          ...state,
          allInstallmentsLoading: false,
          allInstallments: {
            ...state.allInstallments,
            bt: response,
          },
          filters: {
            ...state.filters,
            bt: params,
          },
          currentFilterInstallmentType: installmentType,
        };
      } else if (installmentType === 'BC') {
        return {
          ...state,
          allInstallmentsLoading: false,
          allInstallments: {
            ...state.allInstallments,
            bc: response,
          },
          filters: {
            ...state.filters,
            bc: params,
          },
          currentFilterInstallmentType: installmentType,
        };
      } else if (installmentType === 'C2GO') {
        return {
          ...state,
          allInstallmentsLoading: false,
          allInstallments: {
            ...state.allInstallments,
            c2g: response,
          },
          filters: {
            ...state.filters,
            c2g: params,
          },
          currentFilterInstallmentType: installmentType,
        };
      } else {
        return {...state};
      }
    }
    case 'INSTALLMENTS__RESET_STATE':
    case 'CLEAR_INSTALLMENTS_STATE': {
      return {
        ...INITIAL_STATE,
        allInstallments: state.allInstallments,
      };
    }
    case 'GET_INFORMATION_FOR_MODAL_REQUESTED':
      return {
        ...state,
        information: {
          ...state.information,
          loading: true,
        },
      };

    case 'GET_INFORMATION_FOR_MODAL_SUCCESS':
    case 'GET_INFORMATION_FOR_MODAL_FAILED':
      return {
        ...state,
        information: {
          ...state.information,
          data: action.payload.data,
          loading: false,
        },
      };

    case 'SET_INFORMATION_MODAL_VISIBLE':
      return {
        ...state,
        information: {
          ...state.information,
          visible: action.payload.visible,
          isInitial: action.payload.isInitial,
        },
      };

    case 'REQUEST_PAYMENT_TERMS_ERROR':
      return {
        ...state,
        formLoading: false,
        paymentTermsError: true,
      };

    case 'SAVE_INSTALLMENTS_ERROR':
      return {
        ...state,
        formLoading: false,
        saveInstallmentsError: {
          isError: true,
          message: action.payload.data,
          passcode: action.payload.passcode,
        },
      };

    case 'BALANCE_TRANSFER_SUCCESS':
      return {
        ...state,
        formLoading: false,
        balanceTransfer: {...state.balanceTransfer, ...action.payload},
        currentFilterInstallmentType: 'BT',
      };
    case 'BALANCE_CONVERSION_SUCCESS':
      return {
        ...state,
        formLoading: false,
        balanceConversion: {...state.balanceConversion, ...action.payload},
        currentFilterInstallmentType: 'BC',
      };
    case 'CASH2GO_SUCCESS':
      return {
        ...state,
        formLoading: false,
        cash2Go: {...state.cash2Go, ...action.payload},
        currentFilterInstallmentType: 'C2GO',
      };

    case 'SET_LEAVE_PROMPT':
      return {
        ...state,
        leavePromptVisible: action.payload.visible,
      };

    case 'SET_SHOW_REVIEW_HEADER':
      return {
        ...state,
        showReviewScreen: action.payload.visible,
      };

    case 'SET_SELECTED_CREDIT_INSTALLMENTS': {
      const selectedCredit = action.payload.selectedCredit;
      return {
        ...state,
        selectedCredit,
      };
    }

    case 'SET_ROOT_SCREEN':
      return {
        ...state,
        rootScreen: action.payload.screen,
      };

    case 'CLEAR_INSTALLMENT_ERRORS':
      return {
        ...state,
        saveInstallmentsError: null,
      };

    default:
      return state;
  }
};

export default installment;
