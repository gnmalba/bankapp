import * as actions from '../actions';

describe('Installments actions', () => {
  const payload = {
    data: [],
    installmentType: '',
    selectedCredit: {},
    visible: false,
    isInitial: false,
    isFormDirty: false,
    params: '',
    paymentTerms: [],
    errorMsg: '',
    passcode: '',
    screen: {},
    payload: {
      BalanceTransfer: {
        customerId: '',
        username: '',
        cardSerNo: '',
        amount: 0,
        currency: '',
        amortization: '',
        rate: '',
        totalAmount: '',
        duration: '',
        passcode: '',
        shortCode: '',
      },
    },
  };

  it('handle set current installment type', () => {
    const action = actions.setCurrentInstallmentType(payload.installmentType);
    const expectedAction = {
      type: 'SET_CURRENT_INSTALLMENT_TYPE',
      payload: {data: payload.installmentType},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle set selected credit installments', () => {
    const action = actions.setSelectedCreditforInstallments(
      payload.selectedCredit,
    );
    const expectedAction = {
      type: 'SET_SELECTED_CREDIT_INSTALLMENTS',
      payload: {selectedCredit: payload.selectedCredit},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get bank list api requested', () => {
    const action = actions.getBankListRequested(payload.installmentType);
    const expectedAction = {
      type: 'GET_BANK_LIST_API_REQUESTED',
      payload: {installmentType: payload.installmentType},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle bank list success', () => {
    const action = actions.getBankListSuccess(payload.data, payload.visible);
    const expectedAction = {
      type: 'GET_BANK_LIST_API_SUCCESS',
      payload: {
        banks: payload.data,
        visible: payload.visible,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle banklist error', () => {
    const action = actions.getBankListError(payload);
    const expectedAction = {
      type: 'GET_BANK_LIST_API_ERROR',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle initialize installment', () => {
    const action = actions.initializeInstallment(
      payload.installmentType,
      payload.visible,
    );
    const expectedAction = {
      type: 'INITIALIZE_INSTALLMENT',
      payload: {
        installmentType: payload.installmentType,
        visible: payload.visible,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle set information modal invisible', () => {
    const action = actions.setInformationModalVisible(
      payload.visible,
      payload.isInitial,
    );
    const expectedAction = {
      type: 'SET_INFORMATION_MODAL_VISIBLE',
      payload: {visible: payload.visible, isInitial: payload.isInitial},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get information modal requested', () => {
    const action = actions.getInformationModalRequested(
      payload.installmentType,
    );
    const expectedAction = {
      type: 'GET_INFORMATION_FOR_MODAL_REQUESTED',
      payload: {installmentType: payload.installmentType},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get information modal success', () => {
    const action = actions.getInformationModalSuccess(
      payload.data,
      payload.visible,
    );
    const expectedAction = {
      type: 'GET_INFORMATION_FOR_MODAL_SUCCESS',
      payload: {data: payload.data, visible: payload.visible},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get information modal failed', () => {
    const action = actions.getInformationModalFailed(
      payload.data,
      payload.visible,
    );
    const expectedAction = {
      type: 'GET_INFORMATION_FOR_MODAL_FAILED',
      payload: {data: payload.data, visible: payload.visible},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update balance transfer form', () => {
    const action = actions.updateBalanceTransferFrom(payload.data);
    const expectedAction = {
      type: 'UPDATE_BALANCE_TRANSFER_FROM',
      payload: {data: payload.data},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update balance transfer to', () => {
    const action = actions.updateBalanceTransferTo(
      payload.data,
      payload.isFormDirty,
    );
    const expectedAction = {
      type: 'UPDATE_BALANCE_TRANSFER_TO',
      payload: {data: payload.data, isFormDirty: payload.isFormDirty},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update balance conversion form', () => {
    const action = actions.updateBalanceConversionFrom(
      payload.data,
      payload.isFormDirty,
      payload.visible,
    );
    const expectedAction = {
      type: 'UPDATE_BALANCE_CONVERSION_FROM',
      payload: {
        data: payload.data,
        isFormDirty: payload.isFormDirty,
        visible: payload.visible,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle update cash to go', () => {
    const action = actions.updateCash2GoFrom(
      payload.data,
      payload.isFormDirty,
      payload.visible,
    );
    const expectedAction = {
      type: 'UPDATE_CASH2GO_FROM',
      payload: {
        data: payload.data,
        isFormDirty: payload.isFormDirty,
        visible: payload.visible,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get payment terms', () => {
    const action = actions.getPaymentTerms(payload.params);
    const expectedAction = {
      type: 'REQUEST_PAYMENT_TERMS_API',
      payload: {
        params: payload.params,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get payment terms success', () => {
    const action = actions.getPaymentTermsSuccess(
      payload.data,
      payload.installmentType,
      payload.visible,
    );
    const expectedAction = {
      type: 'REQUEST_PAYMENT_TERMS_API_SUCCESS',
      payload: {
        paymentTerms: payload.data,
        installmentType: payload.installmentType,
        visible: payload.visible,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get update payment terms', () => {
    const action = actions.updatePaymentTerms(
      payload.installmentType,
      payload.data,
    );
    const expectedAction = {
      type: 'UPDATE_PAYMENT_TERMS',
      payload: {
        data: payload.data,
        installmentType: payload.installmentType,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get update amount', () => {
    const action = actions.updateAmount(payload.installmentType, payload.data);
    const expectedAction = {
      type: 'UPDATE_AMOUNT',
      payload: {
        data: payload.data,
        installmentType: payload.installmentType,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get update cash to go', () => {
    const action = actions.updateCash2GoTo(payload.data);
    const expectedAction = {
      type: 'UPDATE_CASH2GO_TO',
      payload: {
        data: payload.data,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get save balance transfer', () => {
    const action = actions.saveBalanceTransfer(payload.payload.BalanceTransfer);
    const expectedAction = {
      type: 'BALANCE_TRANSFER_SAVE',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get save balance convesion', () => {
    const action = actions.saveBalanceConversion(
      payload.payload.BalanceTransfer,
    );
    const expectedAction = {
      type: 'BALANCE_CONVERSION_SAVE',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get save cash to go', () => {
    const action = actions.saveCash2Go(payload.payload.BalanceTransfer);
    const expectedAction = {
      type: 'CASH2GO_SAVE',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get balance transfer Success', () => {
    const action = actions.balanceTransferSuccess(
      payload.payload.BalanceTransfer,
    );
    const expectedAction = {
      type: 'BALANCE_TRANSFER_SUCCESS',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get balance conversion Success', () => {
    const action = actions.balanceConversionSuccess(
      payload.payload.BalanceTransfer,
    );
    const expectedAction = {
      type: 'BALANCE_CONVERSION_SUCCESS',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get cash to go Success', () => {
    const action = actions.cash2GoSuccess(payload.payload.BalanceTransfer);
    const expectedAction = {
      type: 'CASH2GO_SUCCESS',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get installments', () => {
    const action = actions.getInstallments(payload.params);
    const expectedAction = {
      type: 'REQUEST_INSTALLMENTS',
      payload: {params: payload.params},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get installments Success', () => {
    const action = actions.getInstallmentsSuccess(
      payload.payload.BalanceTransfer,
    );
    const expectedAction = {
      type: 'REQUEST_INSTALLMENTS_SUCCESS',
      payload: payload.payload.BalanceTransfer,
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get sort installments', () => {
    const action = actions.sortInstallmentsRequest(payload.params);
    const expectedAction = {
      type: 'SORT_INSTALLMENTS_REQUEST',
      payload: {params: payload.params},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle sort installment success', () => {
    const action = actions.sortInstallmentsSuccess(
      payload.payload.BalanceTransfer,
    );
    const expectedAction = {
      type: 'SORT_INSTALLMENTS_SUCCESS',
      payload: {data: payload.payload.BalanceTransfer},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle get filter installment request', () => {
    const action = actions.filterInstallmentsRequest(payload.params);
    const expectedAction = {
      type: 'FILTER_INSTALLMENTS_REQUEST',
      payload: {params: payload.params},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle filter installment success', () => {
    const action = actions.filterInstallmentsSuccess(
      payload.payload.BalanceTransfer,
    );
    const expectedAction = {
      type: 'FILTER_INSTALLMENTS_SUCCESS',
      payload: {data: payload.payload.BalanceTransfer},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle filter clear installments', () => {
    const action = actions.clearInstallments();
    const expectedAction = {
      type: 'CLEAR_INSTALLMENTS_STATE',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle payment terms error', () => {
    const action = actions.paymentTermsError();
    const expectedAction = {
      type: 'REQUEST_PAYMENT_TERMS_ERROR',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle save installments error', () => {
    const action = actions.saveInstallmentsError(
      payload.errorMsg,
      payload.passcode,
    );
    const expectedAction = {
      type: 'SAVE_INSTALLMENTS_ERROR',
      payload: {data: payload.errorMsg, passcode: payload.passcode},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle installments error', () => {
    const action = actions.viewInstallmentsError();
    const expectedAction = {
      type: 'VIEW_INSTALLMENTS_ERROR',
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle save leave prompt', () => {
    const action = actions.setLeavePrompt(payload.visible);
    const expectedAction = {
      type: 'SET_LEAVE_PROMPT',
      payload: {visible: payload.visible},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle set review header', () => {
    const action = actions.setShowReviewHeader(payload.visible);
    const expectedAction = {
      type: 'SET_SHOW_REVIEW_HEADER',
      payload: {visible: payload.visible},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle set root screen', () => {
    const action = actions.setRootScreen(payload.screen);
    const expectedAction = {
      type: 'SET_ROOT_SCREEN',
      payload: {screen: payload.screen},
    };
    expect(action).toEqual(expectedAction);
  });

  it('handle clear installment errors', () => {
    const action = actions.clearInstallmentErrors(payload.screen);
    const expectedAction = {
      type: 'CLEAR_INSTALLMENT_ERRORS',
    };
    expect(action).toEqual(expectedAction);
  });
});
