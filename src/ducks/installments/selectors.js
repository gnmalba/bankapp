import type {InstallmentStateType} from './types';
import type {State} from '@ducks/types';

export const installmentsSelector = (state: State): InstallmentStateType =>
  state.installments;
