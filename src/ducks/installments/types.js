// @flow

export type BalanceTransferFromType = {
  bankName: string,
  cardNumber: string,
};

export type InstallmentTypesString = 'BT' | 'BC' | 'C2GO' | '';

export type BalanceTransferToType = {|
  // todo
  cardNumber: string,
  alias: string,
|};

export type Cash2GoDepositToType = {|
  disbursementOption: string,
  destinationAccountName: string,
  destinationAccountNumber: string,
  destinationBankName?: string | null,
|};

// possibly already existing
type CreditCard = {};

export type PaymentTermsType = {
  monthlyAmortization: string,
  term: string,
  interestRate: string,
  totalInstallmentAmount: string,
  shortCode: string,
  annualRate: string,
  transactionFee: string,
};

export type PaymentTermsRequest = {
  cardSerNo: string,
  amount: number,
  installmentType: string,
  customerId?: string,
};

export type MoreInformationRequest = {
  installmentType: InstallmentTypesString,
  customerId: string,
};

export type BalanceTransferSaveRequest = {
  customerId?: string,
  username?: string,
  cardSerNo: string,
  amount: number,
  currency: string,
  amortization: number,
  rate: number,
  totalAmount: number,
  duration: number,
  sourceBankName: string,
  sourceBankCardNumber: string,
  sourceBankCardName: string,
  passcode: string,
  shortCode: string,
};

export type BalanceConversionSaveRequest = {
  customerId?: string,
  username?: string,
  cardSerNo: string,
  amount: number,
  currency: string,
  amortization: number,
  rate: number,
  totalAmount: number,
  duration: number,
  passcode: string,
  shortCode: string,
};

export type Cash2GoSaveRequest = {
  customerId?: string,
  username?: string,
  cardSerNo: string,
  amount: number,
  currency: string,
  amortization: number,
  rate: number,
  totalAmount: number,
  duration: number,
  passcode: string,
  shortCode: string,
  ...Cash2GoDepositToType,
};

export type InformationType = {
  Code: string,
  Header: string,
  Details: string,
  Link: string,
};

export type InstallmentBankType = {
  Code: string,
  Name: string,
  C2GO: boolean,
  BT: boolean,
};

export type InformationStateType = {
  loading: boolean,
  visible: boolean,
  data: InformationType | null,
  isInitial: boolean,
};

export type IBalanceTransfer = {
  transferFrom?: BalanceTransferFromType,
  transferTo?: BalanceTransferToType,
  paymentTerms?: ??PaymentTermsType,
  paymentTermsList: Array<PaymentTermsType>,
  amount?: number | null,
};

type IBalanceConversion = {
  from?: CreditCard,
  paymentTerms?: ?PaymentTermsType,
  paymentTermsList?: Array<PaymentTermsType>,
  amount?: number | null,
};

type ICash2Go = {
  from?: CreditCard,
  to?: Cash2GoDepositToType,
  paymentTerms?: ?PaymentTermsType,
  paymentTermsList?: Array<PaymentTermsType>,
  amount?: number | null,
};

export type InstallmentStateType = {
  balanceTransfer: IBalanceTransfer,
  balanceConversion: IBalanceConversion,
  cash2Go: ICash2Go,
  currentInstallmentType: '',
  banks: any[],
  formLoading: boolean,
  allInstallmentsLoading: boolean,
  allInstallments: any,
  information: InformationStateType,
  sortedBy: any,
  filters: any,
  paymentTermsError: boolean,
  saveInstallmentsError: any,
  currentFilterInstallmentType: string,
  initialized: boolean,
  leavePromptVisible: boolean,
  isFormDirty: boolean,
  showReviewScreen: boolean,
  selectedCredit: ?Object,
  rootScreen: InstallmentRootScreenType,
};

export type InstallmentRootScreenType = 'dashboard' | 'overview' | 'soa' | '';

export type ViewInstallmentsRequestType = {
  cardSerNo?: string,
  installmentType: InstallmentTypesString,
  customerId?: string,
  param?: string,
};

export type BalanceConversionInstallmentType = {
  AMOUNT: string,
  CARDNUMBER: string,
  CARDSERNO: string,
  CURRENCY: string,
  DATECREATED: string,
  EAIR: string,
  EmbossingName: string,
  INSTALLMENTSHORTCODE: string,
  ISBUSINESSRULEPASS: string,
  MONTHLYAMORTIZATION: string,
  RATE: string,
  REASONCODE: string,
  REFERENCENUMBER: string,
  SERNO: string,
  SOURCEAPP: string,
  STATUS: string,
  TERM: string,
  TOTALINSTALLMENTAMOUNT: string,
  TRANSTATUSSERNO: string,
  TRANSTATUS_DATECREATED: string,
};

export type BalanceTransferInstallmentType = {
  ACQUIRERID: string,
  AMOUNT: string,
  BANKNAME: string,
  CARDNUMBER: string,
  CARDSERNO: string,
  CREDITLIMIT: string,
  DATECREATED: string,
  DATEOFBIRTH: string,
  EAIR: string,
  EXPIRYDATE: string,
  FIRSTNAME: string,
  INSTALLMENTSHORTCODE: string,
  ISBUSINESSRULEPASS: string,
  LASTNAME: string,
  MCC: string,
  MERCHANTCOUNTRY: string,
  MERCHANTNUMBER: string,
  MIDDLENAME: string,
  MONTHLYAMORTIZATION: string,
  OTHERBANKACCOUNTNAME: string,
  OTHERBANKCARDNUMBER: string,
  OUTSTANDINGBALANCE: string,
  RATE: string,
  REFERENCENUMBER: string,
  SERNO: string,
  SOURCEAPP: string,
  STATUS: string,
  TERM: string,
  TERMINALID: string,
  TOTALINSTALLMENTAMOUNT: string,
  TRANSACTIONFEE: string,
  TRANSTATUSSERNO: string,
  TRANSTATUS_DATECREATED: string,
  EmbossingName: string,
};

export type Cash2GoInstallmentType = {
  ACCOUNTNAME: string,
  ACCOUNTNUMBER: string,
  ACQUIRERID: string,
  AMOUNT: string,
  BANKNAME: string,
  BRANCHNAME: string,
  CARDHOLDERNAME: string,
  CARDNUMBER: string,
  CARDSERNO: string,
  CURRENCY: string,
  DATECREATED: string,
  DISBURSEMENTOPTION: string,
  DISBURSTMENTOPTION: string,
  EAIR: string,
  EXPIRYDATE: string,
  INSTALLMENTSHORTCODE: string,
  ISBUSINESSRULEPASS: string,
  MCC: string,
  MERCHANTCOUNTRY: string,
  MERCHANTNUMBER: string,
  MONTHLYAMORTIZATION: string,
  RATE: string,
  REFERENCENUMBER: string,
  SERNO: string,
  SOURCEAPP: string,
  STATUS: string,
  TERM: string,
  TERMINALID: string,
  TOTALINSTALLMENTAMOUNT: string,
  TRANSACTIONFEE: string,
  TRANSTATUSSERNO: string,
  TRANSTATUS_DATECREATED: string,
  USERID: string,
};

export type InstallmentsAction =
  | {type: 'GET_BANK_LIST_API_REQUESTED'}
  | {
      type: 'SET_CURRENT_INSTALLMENT_TYPE',
      payload: {data: Object},
    }
  | {
      type: 'GET_BANK_LIST_API_SUCCESS',
      payload: {
        banks: Array<InstallmentBankType>,
        visible: boolean,
      },
    }
  | {
      type: 'GET_BANK_LIST_API_ERROR',
    }
  | {
      type: 'UPDATE_BALANCE_TRANSFER_FROM',
      payload: {
        data: Object,
      },
    }
  | {
      type: 'UPDATE_BALANCE_TRANSFER_TO',
      payload: {
        data: Object,
        isFormDirty: boolean,
      },
    }
  | {
      type: 'UPDATE_PAYMENT_TERMS',
      payload: {
        installmentType: InstallmentTypesString,
        data: Object,
      },
    }
  | {
      type: 'UPDATE_AMOUNT',
    }
  | {
      type: 'REQUEST_PAYMENT_TERMS_API',
    }
  | {
      type: 'REQUEST_PAYMENT_TERMS_API_SUCCESS',
      payload: {
        paymentTerms: Array<PaymentTermsType>,
        installmentType: InstallmentTypesString,
        visible: boolean,
      },
    }
  | {type: 'BALANCE_TRANSFER_SAVE'}
  | {type: 'BALANCE_TRANSFER_SUCCESS', payload: Object}
  | {type: 'CASH2GO_SAVE'}
  | {type: 'CASH2GO_SUCCESS', payload: any}
  | {
      type: 'UPDATE_BALANCE_CONVERSION_FROM',
      +payload: {data: any, isFormDirty: boolean, visible: boolean},
    }
  | {type: 'BALANCE_CONVERSION_SAVE'}
  | {type: 'BALANCE_CONVERSION_SUCCESS', payload: Object}
  | {
      type: 'UPDATE_CASH2GO_FROM',
      +payload: {data: any, isFormDirty: boolean, visible: boolean},
    }
  | {
      type: 'UPDATE_CASH2GO_TO',
      payload: {data: Object},
    }
  | {type: 'REQUEST_INSTALLMENTS'}
  | {type: 'REQUEST_INSTALLMENTS_SUCCESS', payload: Object}
  | {
      type: 'SORT_INSTALLMENTS_REQUEST',
    }
  | {
      type: 'SORT_INSTALLMENTS_SUCCESS',
      payload: {data: Object},
    }
  | {
      type: 'FILTER_INSTALLMENTS_REQUEST',
    }
  | {type: 'FILTER_INSTALLMENTS_SUCCESS', payload: {data: Object}}
  | {type: 'GET_INFORMATION_FOR_MODAL_REQUESTED'}
  | {
      type: 'GET_INFORMATION_FOR_MODAL_SUCCESS',
      +payload: {data: InformationType, visible: boolean},
    }
  | {
      type: 'GET_INFORMATION_FOR_MODAL_FAILED',
      +payload: {data: InformationType, visible: boolean},
    }
  | {
      type: 'FILTER_INSTALLMENTS_SUCCESS',
      payload: {data: Object},
    }
  | {
      type: 'C2GO_TERMS_CONDITION_FAILED',
      type: 'CLEAR_INSTALLMENTS_STATE',
    }
  | {
      type: 'REQUEST_PAYMENT_TERMS_ERROR',
    }
  | {
      type: 'SAVE_INSTALLMENTS_ERROR',
      payload: {
        data: Object,
        passcode: string,
      },
    }
  | {
      type: 'SET_INFORMATION_MODAL_VISIBLE',
      +payload: {visible: boolean, isInitial: boolean},
    }
  | {
      type: 'INITIALIZE_INSTALLMENT',
      +payload: {installmentType: string, visible: boolean},
    }
  | {
      type: 'VIEW_INSTALLMENTS_ERROR',
    }
  | {
      type: 'SET_LEAVE_PROMPT',
      +payload: {visible: boolean},
    }
  | {
      type: 'UPDATE_BALANCE_TRANSFER_TO',
      +payload: {data: any, isFormDirty: boolean, visible: boolean},
    }
  | {
      type: 'INSTALLMENTS__RESET_STATE',
    }
  | {
      type: 'SET_SHOW_REVIEW_HEADER',
      payload: {visible: boolean},
    }
  | {
      type: 'SET_SELECTED_CREDIT_INSTALLMENTS',
      payload: {selectedCredit: ?Object},
    }
  | {
      type: 'SET_ROOT_SCREEN',
      payload: {screen: InstallmentRootScreenType},
    }
  | {
      type: 'CLEAR_INSTALLMENT_ERRORS',
    };
