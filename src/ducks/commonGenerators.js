import {select} from 'redux-saga/effects';
import {getAliasList} from '@ducks/commonSelectors';

export const checkDuplicateAlias = function* checkDuplicateAlias({
  payload: alias,
}): any {
  const aliasList = yield select(getAliasList);
  const isDuplicateAlias = aliasList.includes(alias.toLowerCase());
  if (isDuplicateAlias) {
    return true;
  }
  return false;
};
