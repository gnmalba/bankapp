/**
 * @flow
 */
import {SEND_MONEY_API} from '@api/constants';

export type SendMoneyState = {
  sourceAccounts: Array<Account>,
  recipientAccounts: Array<Account>,
  enrolledContacts: Array<EnrolledContact>,
  banks: Array<Bank>,
  countryCodes: Array<MobileNumberCode>,
  transferServices: Array<any>,
  enrolledContactDetails: EnrolledContact | null,
  isSubmitting: boolean,
  isLoadingSourceAccounts: boolean,
  isLoadingRecipientAccounts: boolean,
  isLoadingEnrolledContacts: boolean,
  isLoadingBanks: boolean,
  isLoadingCountryCodes: boolean,
  isLoadingTransferServices: boolean,
  isLoadingEnrolledContactDetails: boolean,
  getSourceAccountsError: Error | null,
  getRecipientAccountsError: Error | null,
  getEnrolledContactsError: Error | null,
  getBanksError: Error | null,
  getCountryCodesError: Error | null,
  getTransferServicesError: Error | null,
  getEnrolledContactDetailsError: Error | null,
  submitError: Error | null,
};

export type SendMoneyAction = {type: string, payload?: any};

export type SendMoneyTransactionType =
  | $Keys<typeof SEND_MONEY_API.CPC>
  | $Keys<typeof SEND_MONEY_API.CASA>;

export type TransactionType = {
  id: number,
  code: string,
  name: string,
  enabled: boolean,
  forShortcut: boolean,
  credit: boolean,
  debit: boolean,
  icon: string,
};

export type CpcAccount = {
  alias: string,
  availableBalance: string,
  cardType: string,
  cardSerNo: string,
  currency: string,
};

export type DepositAccount = {
  accountNo: string,
  accountType: string,
  accountTypeDescription: string,
  alias: string,
  allowedTransactions: Array<TransactionType>,
  availableBalanceValueWithCurrency: string,
  branch: string,
  currencyCode: string,
  currencyHostCode: string,
  currentBalanceValueWithCurrency: string,
  customerId: string,
  id: number,
  initialAccount: boolean,
  mobile: string | null,
  productType: string,
  relationshipCode: string,
  shortcuts: Array<TransactionType>,
};

export type Account = {
  identifier: string,
  isCpc?: boolean,
} & (DepositAccount | CpcAccount);

export type ApiErrorResponse = {
  error: string,
  message: string,
  path: string,
  status: number,
  timestamp: Date,
};

export type EnrolledContact = {
  accountType: string,
  acctNumber: string,
  alias: string,
  bankCode: string,
  bankId: string,
  bankName: string,
  businessName: string,
  contactId: string,
  currencyCode: string,
  emailAddress: string,
  firstName: string,
  isEnabled: boolean,
  lastName: string,
  lastTransaction: string,
  mobileNumber: string,
};

export type Bank = {
  bancnetCode: string,
  brstn: string,
  enrollmentEnabled: boolean,
  id: number,
  instapayEnabled: boolean,
  mboEnabled: boolean,
  name: string,
  pchcCode: string,
  pesonetEnabled: boolean,
  swiftCode: string,
  transferEnabled: boolean,
  type: string,
};

export type MobileNumberCode = {
  countryCode: string,
  id: number,
  imageUrl: string,
  phoneCode: string,
  phoneNumberSize: string,
};
