import reducer, {INITIAL_STATE} from '../reducer';

describe('sendMoney reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  const payload = 'mockPayload';

  it('should handle SEND_MONEY/GET_SOURCE_ACCOUNTS', () => {
    const action = {
      type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingSourceAccounts: true,
    });
  });

  it('should handle SEND_MONEY/GET_SOURCE_ACCOUNTS_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingSourceAccounts: false,
      sourceAccounts: payload,
    });
  });

  it('should handle SEND_MONEY/GET_SOURCE_ACCOUNTS_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingSourceAccounts: false,
      getSourceAccountsError: payload,
    });
  });

  it('should handle SEND_MONEY/GET_RECIPIENT_ACCOUNTS', () => {
    const action = {
      type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingRecipientAccounts: true,
    });
  });

  it('should handle SEND_MONEY/GET_RECIPIENT_ACCOUNTS_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingRecipientAccounts: false,
      recipientAccounts: payload,
    });
  });

  it('should handle SEND_MONEY/GET_RECIPIENT_ACCOUNTS_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingRecipientAccounts: false,
      getRecipientAccountsError: payload,
    });
  });

  it('should handle SEND_MONEY/GET_ENROLLED_CONTACTS', () => {
    const action = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACTS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingEnrolledContacts: true,
    });
  });

  it('should handle SEND_MONEY/GET_ENROLLED_CONTACTS_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACTS_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingEnrolledContacts: false,
      enrolledContacts: payload,
    });
  });

  it('should handle SEND_MONEY/GET_ENROLLED_CONTACTS_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACTS_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingEnrolledContacts: false,
      getEnrolledContactsError: payload,
    });
  });

  it('should handle SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS', () => {
    const action = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingEnrolledContactDetails: true,
    });
  });

  it('should handle SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingEnrolledContactDetails: false,
      enrolledContactDetails: payload,
    });
  });

  it('should handle SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingEnrolledContactDetails: false,
      getEnrolledContactDetailsError: payload,
    });
  });

  it('should handle SEND_MONEY/GET_BANKS', () => {
    const action = {
      type: 'SEND_MONEY/GET_BANKS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingBanks: true,
    });
  });

  it('should handle SEND_MONEY/GET_BANKS_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_BANKS_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingBanks: false,
      banks: payload,
    });
  });

  it('should handle SEND_MONEY/GET_BANKS_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_BANKS_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingBanks: false,
      getBanksError: payload,
    });
  });

  it('should handle SEND_MONEY/GET_COUNTRY_CODES', () => {
    const action = {
      type: 'SEND_MONEY/GET_COUNTRY_CODES',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingCountryCodes: true,
    });
  });

  it('should handle SEND_MONEY/GET_COUNTRY_CODES_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_COUNTRY_CODES_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingCountryCodes: false,
      countryCodes: payload,
    });
  });

  it('should handle SEND_MONEY/GET_COUNTRY_CODES_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_COUNTRY_CODES_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingCountryCodes: false,
      getCountryCodesError: payload,
    });
  });

  it('should handle SEND_MONEY/GET_TRANSFER_SERVICES', () => {
    const action = {
      type: 'SEND_MONEY/GET_TRANSFER_SERVICES',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingTransferServices: true,
    });
  });

  it('should handle SEND_MONEY/GET_TRANSFER_SERVICES_SUCCESS', () => {
    const action = {
      type: 'SEND_MONEY/GET_TRANSFER_SERVICES_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingTransferServices: false,
      transferServices: payload,
    });
  });

  it('should handle SEND_MONEY/GET_TRANSFER_SERVICES_FAILED', () => {
    const action = {
      type: 'SEND_MONEY/GET_TRANSFER_SERVICES_FAILED',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      isLoadingTransferServices: false,
      getTransferServicesError: payload,
    });
  });
});
