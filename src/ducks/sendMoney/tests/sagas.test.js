import {select} from 'redux-saga/effects';
import {expectSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import sendMoneyService from '@api/services/sendMoneyService';
import {getCustomerID, getUsername} from '@ducks/commonSelectors';
import {
  getRecipientAccountsSuccess,
  getSourceAccountsSuccess,
  getEnrolledContactsSuccess,
  getEnrolledContactDetailsSuccess,
  getBanksSuccess,
  getCountryCodesSuccess,
  getTransferServicesSuccess,
  submitSuccess,
} from '../actions';
import {
  getSourceAccountsWatcher,
  getRecipientAccountsWatcher,
  getDepositAccounts,
  getEnrolledContactsWatcher,
  getEnrolledContactDetailsWatcher,
  getBanksWatcher,
  getCountryCodesWatcher,
  getTransferServicesWatcher,
  submitWatcher,
} from '../sagas';
import {
  availableBalanceWithCurrency,
  byAlias,
  excludeSourceFromRecipients,
  excludeSourceFromEnrolledContacts,
  mockApiService,
  determineTransactionType,
} from '../utils';
import {generateHash, getDeviceInfo} from '@utils/commons';

describe('sendMoney sagas', () => {
  const customerId = 'mockCustomerId';
  const username = 'mockUsername';
  const mockDepositAccounts = [
    {
      alias: 'Deposit 1',
      availableBalance: 50,
      availableBalanceWithCurrency: 'PHP 50',
      currency: 'PHP',
      accountNo: '1',
      identifier: '1',
      isCpc: false,
    },
    {
      alias: 'Deposit 2',
      availableBalance: 75,
      availableBalanceWithCurrency: 'PHP 75',
      currency: 'PHP',
      accountNo: '2',
      identifier: '2',
      isCpc: false,
    },
  ];
  const mockCpcAccounts = [
    {
      alias: 'Cpc 1',
      availableBalance: 100,
      currency: 'PHP',
      cardSerNo: 'Cpc1',
      identifier: 'Cpc1',
      isCpc: true,
    },
    {
      alias: 'Cpc 2',
      availableBalance: 200,
      currency: 'PHP',
      cardSerNo: 'Cpc2',
      identifier: 'Cpc2',
      isCpc: true,
    },
    {
      alias: 'Cpc 3',
      availableBalance: 300,
      currency: 'PHP',
      cardSerNo: 'Cpc3',
      identifier: 'Cpc3',
      isCpc: true,
    },
  ];
  const mockEnrolledContacts = [
    {
      acctNumber: '1',
      alias: 'Contact 1',
      contactId: 'contactId1',
    },
    {
      acctNumber: '2',
      alias: 'Contact 2',
      contactId: 'contactId2',
    },
    {
      acctNumber: '3',
      alias: 'Contact 3',
      contactId: 'contactId3',
    },
  ];
  const mockEnrolledContactDetails = {
    acctNumber: '1',
    alias: 'Contact 1',
    contactId: 'contactId1',
  };

  // mock the transformations performed by the saga
  const mappedCpcAccounts = mockCpcAccounts.map((account) => ({
    ...account,
    availableBalanceValueWithCurrency: availableBalanceWithCurrency(
      account.availableBalance,
      account.currency,
    ),
  }));

  it('getSourceAccountsWatcher should run the expected effects', async () => {
    const expectedSourceAccounts = [
      ...mockDepositAccounts,
      ...mappedCpcAccounts,
    ].sort(byAlias);

    return (
      expectSaga(getSourceAccountsWatcher)
        // provide mocks
        .provide([
          [select(getCustomerID), customerId],
          [matchers.call.fn(getDepositAccounts), mockDepositAccounts],
          [
            matchers.call.fn(
              sendMoneyService.getSourceAccounts,
              customerId,
              'all',
            ),
            mockCpcAccounts,
          ],
        ])
        // assert that the put effect is implemented
        .put(getSourceAccountsSuccess(expectedSourceAccounts))
        .run()
    );
  });

  it('getRecipientAccountsWatcher should run the expected effects', async () => {
    const sourceAccount = mockDepositAccounts[0];
    const expectedRecipientAccounts = [...mockDepositAccounts]
      .sort(byAlias)
      .filter(excludeSourceFromRecipients(sourceAccount));

    return expectSaga(getRecipientAccountsWatcher, {payload: {sourceAccount}})
      .provide([[matchers.call.fn(getDepositAccounts), mockDepositAccounts]])
      .put(getRecipientAccountsSuccess(expectedRecipientAccounts))
      .run();
  });

  it('getEnrolledContactsWatcher should run the expected effects', async () => {
    const sourceAccount = mockEnrolledContacts[2];
    const expectedEnrolledContacts = [...mockEnrolledContacts]
      .sort(byAlias)
      .filter(excludeSourceFromEnrolledContacts(sourceAccount));

    return expectSaga(getEnrolledContactsWatcher, {payload: {sourceAccount}})
      .provide([
        [select(getCustomerID), customerId],
        [
          matchers.call.fn(sendMoneyService.getEnrolledContacts, customerId),
          mockEnrolledContacts,
        ],
      ])
      .put(getEnrolledContactsSuccess(expectedEnrolledContacts))
      .run();
  });

  it('getEnrolledContactDetailsWatcher should run the expected effects', async () => {
    const contactId = 'mockContactId';

    return expectSaga(getEnrolledContactDetailsWatcher, {
      payload: {contactId},
    })
      .provide([
        [select(getCustomerID), customerId],
        [
          matchers.call.fn(
            sendMoneyService.getEnrolledContactDetails,
            customerId,
            contactId,
          ),
          mockEnrolledContactDetails,
        ],
      ])
      .put(getEnrolledContactDetailsSuccess(mockEnrolledContactDetails))
      .run();
  });

  it('getBanksWatcher should run the expected effects', async () => {
    const mockData = [{bankName: 'bank 1'}, {bankName: 'bank 2'}];
    return expectSaga(getBanksWatcher)
      .provide([[matchers.call.fn(sendMoneyService.getBanks), mockData]])
      .put(getBanksSuccess(mockData))
      .run();
  });

  it('getCountryCodesWatcher should run the expected effects', async () => {
    const mockData = [{countryCode: 'PH'}, {countryCode: 'US'}];
    return expectSaga(getCountryCodesWatcher)
      .provide([[matchers.call.fn(sendMoneyService.getCountryCodes), mockData]])
      .put(getCountryCodesSuccess(mockData))
      .run();
  });

  it('getTransferServicesWatcher should run the expected effects', async () => {
    const mockData = [{serviceType: 'INSTAPAY'}, {serviceType: 'PESONET'}];
    return expectSaga(getTransferServicesWatcher)
      .provide([
        [matchers.call.fn(mockApiService.getTransferServices), mockData],
      ])
      .put(getTransferServicesSuccess(mockData))
      .run();
  });

  it('submitWatcher should run the expected effects', () => {
    const mockPayload = {
      accountId: 8631,
      amount: '1.00',
      contactId: 8674,
      currencyCode: 'PHP',
      customerId: '6237',
      targetId: 8674,
      scheduleType: 'SCHEDULE_TYPE_IMMEDIATE',
      recipientType: 'O',
    };

    const {scheduleType, recipientType, ...formValues} = mockPayload;

    const hash = generateHash({
      accountId: mockPayload.accountId,
      username,
      amount: mockPayload.amount,
      currencyCode: mockPayload.currencyCode,
      billerId: mockPayload.contactId,
    });

    const apiParams = {
      ...formValues,
      hash,
      customerId,
      customerDevice: getDeviceInfo(),
    };

    const transactionType = determineTransactionType(
      scheduleType,
      recipientType,
    );

    const mockResponse = {
      referenceNumber: '123456',
    };

    return expectSaga(submitWatcher, {
      payload: mockPayload,
    })
      .provide([
        [select(getCustomerID), customerId],
        [select(getUsername), username],
        [
          matchers.call.fn(sendMoneyService.submit, apiParams, transactionType),
          mockResponse,
        ],
      ])
      .put(submitSuccess(mockResponse))
      .run();
  });
});
