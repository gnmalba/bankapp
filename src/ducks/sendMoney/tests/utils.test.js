import {
  allowedAccountsFilter,
  excludeSourceFromEnrolledContacts,
  excludeSourceFromRecipients,
} from '../utils';

describe('utils', () => {
  it('allowedAccountsFilter excludes unwanted data', () => {
    const payBillsCode = 'PAY_BILLS';
    const sendMoneyCode = 'SEND_MONEY';
    const mockData = [
      {
        alias: 'mock 1',
        active: true,
        allowedTransactions: [
          {code: payBillsCode, enabled: true},
          {code: sendMoneyCode, enabled: true},
        ],
      },
      {
        alias: 'mock 2',
        active: true,
        allowedTransactions: [
          {code: payBillsCode, enabled: true},
          {code: sendMoneyCode, enabled: false},
        ],
      },
      {
        alias: 'mock 3',
        active: false,
        allowedTransactions: [
          {code: payBillsCode, enabled: true},
          {code: sendMoneyCode, enabled: true},
        ],
      },
      {
        alias: 'mock 4',
        active: true,
        allowedTransactions: [{code: payBillsCode, enabled: true}],
      },
    ];

    const result = mockData.filter(allowedAccountsFilter);

    expect(result).toEqual([
      {
        alias: 'mock 1',
        active: true,
        allowedTransactions: [
          {code: payBillsCode, enabled: true},
          {code: sendMoneyCode, enabled: true},
        ],
      },
    ]);
  });

  it('excludeSourceFromRecipients filters out the source account', () => {
    const recipients = [
      {identifier: 'cpc1'},
      {identifier: 'cpc2'},
      {identifier: 'deposit1'},
      {identifier: 'deposit2'},
    ];
    const source = {identifier: 'cpc2'};

    expect(recipients.filter(excludeSourceFromRecipients(source))).toEqual([
      {identifier: 'cpc1'},
      {identifier: 'deposit1'},
      {identifier: 'deposit2'},
    ]);
  });

  it('excludeSourceFromEnrolledContacts filters out the source account', () => {
    const recipients = [
      {acctNumber: 'cpc1'},
      {acctNumber: 'cpc2'},
      {acctNumber: 'deposit1'},
      {acctNumber: 'deposit2'},
    ];
    const source = {identifier: 'cpc2'};

    expect(
      recipients.filter(excludeSourceFromEnrolledContacts(source)),
    ).toEqual([
      {acctNumber: 'cpc1'},
      {acctNumber: 'deposit1'},
      {acctNumber: 'deposit2'},
    ]);
  });
});
