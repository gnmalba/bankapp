import {depositAccountsSelector, sourceAccountsSelector} from '../selectors';

describe('sendMoney selectors', () => {
  it('depositAccountsSelectors returns savingsAndDeposits and timeDeposits ', () => {
    const mockState = {
      deposits: {
        savingsAndDeposits: ['mock 1', 'mock 2'],
        timeDeposits: ['mock 3', 'mock 4'],
      },
    };

    expect(depositAccountsSelector.resultFunc(mockState.deposits)).toEqual([
      'mock 1',
      'mock 2',
      'mock 3',
      'mock 4',
    ]);
  });

  it('sourceAccountsSelector returns savingsAndDeposits and timeDeposits ', () => {
    const mockState = {
      sourceAccounts: ['test'],
      isLoadingSourceAccounts: false,
    };

    expect(sourceAccountsSelector.resultFunc(mockState)).toEqual({
      sourceAccounts: ['test'],
      isLoading: false,
    });
  });
});
