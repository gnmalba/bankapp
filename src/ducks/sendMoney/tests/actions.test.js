import * as actions from '../actions';

describe('getSourceAccounts actions', () => {
  const payload = ['mockPayload'];

  it('getSourceAccounts returns the correct action', () => {
    const action = actions.getSourceAccounts();
    const expectedAction = {
      type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getSourceAccountsSuccess returns the correct action', () => {
    const action = actions.getSourceAccountsSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getSourceAccountsFailed returns the correct action', () => {
    const action = actions.getSourceAccountsFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});

describe('getRecipientAccounts actions', () => {
  const payload = ['mockPayload'];

  it('getRecipientAccounts returns the correct action', () => {
    const action = actions.getRecipientAccounts();
    const expectedAction = {
      type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getRecipientAccountsSuccess returns the correct action', () => {
    const action = actions.getRecipientAccountsSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getRecipientAccountsFailed returns the correct action', () => {
    const action = actions.getRecipientAccountsFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});

describe('getEnrolledContacts actions', () => {
  const payload = ['mockPayload'];
  it('getEnrolledContacts returns the correct action', () => {
    const action = actions.getEnrolledContacts();
    const expectedAction = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACTS',
    };
    expect(action).toEqual(expectedAction);
  });
  it('getEnrolledContactsSuccess returns the correct action', () => {
    const action = actions.getEnrolledContactsSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACTS_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
  it('getEnrolledContactsFailed returns the correct action', () => {
    const action = actions.getEnrolledContactsFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACTS_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});

describe('getEnrolledContactDetails actions', () => {
  const payload = ['mockPayload'];

  it('getEnrolledContactDetails returns the correct action', () => {
    const customerId = 'mockId';
    const action = actions.getEnrolledContactDetails(customerId);
    const expectedAction = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS',
      payload: customerId,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getEnrolledContactDetailsSuccess returns the correct action', () => {
    const action = actions.getEnrolledContactDetailsSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getEnrolledContactDetailsFailed returns the correct action', () => {
    const action = actions.getEnrolledContactDetailsFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});

describe('getBanks actions', () => {
  const payload = ['mockPayload'];

  it('getBanks returns the correct action', () => {
    const action = actions.getBanks();
    const expectedAction = {
      type: 'SEND_MONEY/GET_BANKS',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getBanksSuccess returns the correct action', () => {
    const action = actions.getBanksSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_BANKS_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getBanksFailed returns the correct action', () => {
    const action = actions.getBanksFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_BANKS_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});

describe('getCountryCodes actions', () => {
  const payload = ['mockPayload'];

  it('getCountryCodes returns the correct action', () => {
    const action = actions.getCountryCodes();
    const expectedAction = {
      type: 'SEND_MONEY/GET_COUNTRY_CODES',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getCountryCodesSuccess returns the correct action', () => {
    const action = actions.getCountryCodesSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_COUNTRY_CODES_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getCountryCodesFailed returns the correct action', () => {
    const action = actions.getCountryCodesFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_COUNTRY_CODES_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});

describe('getTransferServices actions', () => {
  const payload = ['mockPayload'];

  it('getTransferServices returns the correct action', () => {
    const action = actions.getTransferServices();
    const expectedAction = {
      type: 'SEND_MONEY/GET_TRANSFER_SERVICES',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getTransferServicesSuccess returns the correct action', () => {
    const action = actions.getTransferServicesSuccess(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_TRANSFER_SERVICES_SUCCESS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('getTransferServicesFailed returns the correct action', () => {
    const action = actions.getTransferServicesFailed(payload);
    const expectedAction = {
      type: 'SEND_MONEY/GET_TRANSFER_SERVICES_FAILED',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});
