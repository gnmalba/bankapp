/**
 * @flow
 */

import type {
  ApiErrorResponse,
  Bank,
  Account,
  EnrolledContact,
  MobileNumberCode,
  SendMoneyAction,
} from './types';

export const getSourceAccounts = (): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS',
});

export const getSourceAccountsSuccess = (
  payload: Array<Account>,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS_SUCCESS',
  payload,
});

export const getSourceAccountsFailed = (
  payload: ApiErrorResponse,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_SOURCE_ACCOUNTS_FAILED',
  payload,
});

export const getRecipientAccounts = (payload: {
  sourceAccount: Account,
}): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS',
  payload,
});

export const getRecipientAccountsSuccess = (
  payload: Array<Account>,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_SUCCESS',
  payload,
});

export const getRecipientAccountsFailed = (
  payload: ApiErrorResponse,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_FAILED',
  payload,
});

export const getEnrolledContacts = (): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_ENROLLED_CONTACTS',
  // payload,
});

export const getEnrolledContactsSuccess = (
  payload: Array<EnrolledContact>,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_ENROLLED_CONTACTS_SUCCESS',
  payload,
});

export const getEnrolledContactsFailed = (
  payload: ApiErrorResponse,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_ENROLLED_CONTACTS_FAILED',
  payload,
});

export const getEnrolledContactDetails = (
  payload: string,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS',
  payload,
});

export const getEnrolledContactDetailsSuccess = (
  payload: EnrolledContact,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_SUCCESS',
  payload,
});

export const getEnrolledContactDetailsFailed = (
  payload: ApiErrorResponse,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_FAILED',
  payload,
});

export const getBanks = (): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_BANKS',
});

export const getBanksSuccess = (payload: Array<Bank>): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_BANKS_SUCCESS',
  payload,
});

export const getBanksFailed = (payload: ApiErrorResponse): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_BANKS_FAILED',
  payload,
});

export const getCountryCodes = (): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_COUNTRY_CODES',
});

export const getCountryCodesSuccess = (
  payload: MobileNumberCode,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_COUNTRY_CODES_SUCCESS',
  payload,
});

export const getCountryCodesFailed = (
  payload: ApiErrorResponse,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_COUNTRY_CODES_FAILED',
  payload,
});

export const getTransferServices = (): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_TRANSFER_SERVICES',
});

export const getTransferServicesSuccess = (payload: any): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_TRANSFER_SERVICES_SUCCESS',
  payload,
});

export const getTransferServicesFailed = (
  payload: ApiErrorResponse,
): SendMoneyAction => ({
  type: 'SEND_MONEY/GET_TRANSFER_SERVICES_FAILED',
  payload,
});

export const submit = (payload: any): SendMoneyAction => ({
  type: 'SEND_MONEY/SUBMIT',
  payload,
});

export const submitSuccess = (payload: any): SendMoneyAction => ({
  type: 'SEND_MONEY/SUBMIT_SUCCESS',
  payload,
});

export const submitFailed = (payload: any): SendMoneyAction => ({
  type: 'SEND_MONEY/SUBMIT_FAILED',
  payload,
});
