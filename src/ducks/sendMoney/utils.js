import {SCHEDULE_TYPE} from '@screens/private/SendMoney/constants';

export const allowedAccountsFilter = (depositAccount) =>
  depositAccount.active &&
  depositAccount.allowedTransactions.some(
    (allowedTransaction) =>
      allowedTransaction.code === 'SEND_MONEY' && allowedTransaction.enabled,
  );

/**
 * Compare function for sorting data
 */
export function byAlias(a, b) {
  var aliasA = a.alias.toUpperCase(); // ignore upper and lowercase
  var aliasB = b.alias.toUpperCase(); // ignore upper and lowercase
  if (aliasA < aliasB) {
    return -1;
  }
  if (aliasA > aliasB) {
    return 1;
  }

  // names must be equal
  return 0;
}

export function availableBalanceWithCurrency(
  availableBalance: string,
  currency: string,
): string {
  if (!currency || !availableBalance) {
    return 'BAD DATA';
  } else {
    return currency + ' ' + availableBalance;
  }
}

export const excludeSourceFromRecipients = (sourceAccount) => (
  recipientAccount,
) => {
  return recipientAccount.identifier !== sourceAccount.identifier;
};

export const excludeSourceFromEnrolledContacts = (sourceAccount) => (
  enrolledContact,
) => {
  return enrolledContact.acctNumber !== sourceAccount.identifier;
};

export const mockApiService = {
  getTransferServices: () => {
    return Promise.resolve([
      {
        serviceType: 'INSTAPAY',
        serviceName: 'InstaPay',
        transferFee: 0,
        checklist: [
          'Real time transfer',
          'PHP 50,000.00 daily transaction limit per source account',
          'PHP 0.00 fee per transaction.',
        ],
      },
      {
        serviceType: 'PESONET',
        serviceName: 'PESONet',
        transferFee: 0,
        checklist: [
          'Same banking day transfer if made before 12:00 NN (funds to be credited by 11:00PM)',
          'Next banking day transfer if made after 12:00 NN during weekends and holidays (funds to be credited by 11:00PM)',
          'PHP 500,000.00 daily transaction limit per source account',
          'PHP 0.00 fee per transaction',
        ],
      },
    ]);
  },
};

const transactionTypeMap = {
  [SCHEDULE_TYPE.SCHEDULED]: {
    O: 'SCHEDULED_OWN',
    E: 'SCHEDULED_ENROLLED',
    U: 'SCHEDULED_UNENROLLED',
  },
  [SCHEDULE_TYPE.IMMEDIATE]: {
    O: 'IMMEDIATE_OWN',
    E: 'IMMEDIATE_ENROLLED',
    U: 'IMMEDIATE_UNENROLLED',
  },
};

export function determineTransactionType(
  scheduleType = 'SCHEDULE_TYPE_IMMEDIATE',
  recipientType = 'O',
) {
  return transactionTypeMap[scheduleType][recipientType];
}
