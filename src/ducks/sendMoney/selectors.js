/**
 * @flow
 */

import {createSelector} from 'reselect';
import {depositsSelector} from '@ducks/deposit/selectors';

import type {DepositAccount, Account, SendMoneyState} from './types';

export const depositAccountsSelector: Array<DepositAccount> = createSelector(
  depositsSelector,
  (deposits) => [...deposits.savingsAndDeposits, ...deposits.timeDeposits],
);

export const sourceAccountsSelector: {
  sourceAccounts: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    sourceAccounts: sendMoney.sourceAccounts,
    isLoading: sendMoney.isLoadingSourceAccounts,
  }),
);

export const sendMoneySelector: SendMoneyState = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => sendMoney,
);

export const recipientAccountsSelector: {
  recipientAccounts: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    recipientAccounts: sendMoney.recipientAccounts,
    isLoading: sendMoney.isLoadingRecipientAccounts,
  }),
);

export const enrolledContactsSelector: {
  enrolledContacts: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    enrolledContacts: sendMoney.enrolledContacts,
    isLoading: sendMoney.isLoadingEnrolledContacts,
  }),
);

export const enrolledContactDetailsSelector: {
  enrolledContactDetails: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    enrolledContactDetails: sendMoney.enrolledContactDetails,
    isLoading: sendMoney.isLoadingEnrolledContactDetails,
  }),
);

export const banksSelector: {
  banks: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    banks: sendMoney.banks,
    isLoading: sendMoney.isLoadingBanks,
  }),
);

export const countryCodesSelector: {
  countryCodes: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    countryCodes: sendMoney.countryCodes,
    isLoading: sendMoney.isLoadingCountryCodes,
  }),
);

export const transferServicesSelector: {
  transferServices: Array<Account>,
  isLoading: boolean,
} = createSelector(
  (state) => state.sendMoney,
  (sendMoney) => ({
    transferServices: sendMoney.transferServices,
    isLoading: sendMoney.isLoadingTransferServices,
  }),
);
