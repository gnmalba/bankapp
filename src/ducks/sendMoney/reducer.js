/**
 * @flow
 */

import type {
  SendMoneyState,
  SendMoneyAction,
  Account,
  EnrolledContact,
  MobileNumberCode,
} from './types';

export const INITIAL_STATE: SendMoneyState = {
  sourceAccounts: [],
  recipientAccounts: [],
  enrolledContacts: [],
  banks: [],
  countryCodes: [],
  transferServices: [],
  enrolledContactDetails: null,
  isSubmitting: false,
  isLoadingSourceAccounts: false,
  isLoadingRecipientAccounts: false,
  isLoadingEnrolledContacts: false,
  isLoadingBanks: false,
  isLoadingCountryCodes: false,
  isLoadingTransferServices: false,
  isLoadingEnrolledContactDetails: false,
  getSourceAccountsError: null,
  getRecipientAccountsError: null,
  getEnrolledContactsError: null,
  getBanksError: null,
  getCountryCodesError: null,
  getTransferServicesError: null,
  getEnrolledContactDetailsError: null,
  submitError: null,
};

const actionMap = {
  'SEND_MONEY/GET_SOURCE_ACCOUNTS': (
    state: SendMoneyState,
    action: {type: string},
  ) => ({
    ...state,
    isLoadingSourceAccounts: true,
    getSourceAccountsError: null,
  }),
  'SEND_MONEY/GET_SOURCE_ACCOUNTS_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: Array<Account>},
  ) => ({
    ...state,
    sourceAccounts: action.payload,
    isLoadingSourceAccounts: false,
  }),
  'SEND_MONEY/GET_SOURCE_ACCOUNTS_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingSourceAccounts: false,
    getSourceAccountsError: action.payload,
  }),
  'SEND_MONEY/GET_RECIPIENT_ACCOUNTS': (
    state: SendMoneyState,
    action: {type: string},
  ) => ({
    ...state,
    isLoadingRecipientAccounts: true,
    getRecipientAccountsError: null,
  }),
  'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: Array<Account>},
  ) => ({
    ...state,
    recipientAccounts: action.payload,
    isLoadingRecipientAccounts: false,
  }),
  'SEND_MONEY/GET_RECIPIENT_ACCOUNTS_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingRecipientAccounts: false,
    getRecipientAccountsError: action.payload,
  }),
  'SEND_MONEY/GET_ENROLLED_CONTACTS': (
    state: SendMoneyState,
    action: {type: string},
  ) => ({
    ...state,
    isLoadingEnrolledContacts: true,
    getEnrolledContactsError: null,
  }),
  'SEND_MONEY/GET_ENROLLED_CONTACTS_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: Array<EnrolledContact>},
  ) => ({
    ...state,
    enrolledContacts: action.payload,
    isLoadingEnrolledContacts: false,
  }),
  'SEND_MONEY/GET_ENROLLED_CONTACTS_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingEnrolledContacts: false,
    getEnrolledContactsError: action.payload,
  }),
  'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS': (
    state: SendMoneyState,
    action: {type: string, payload: string},
  ) => ({
    ...state,
    isLoadingEnrolledContactDetails: true,
    getEnrolledContactDetailsError: null,
  }),
  'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: EnrolledContact},
  ) => ({
    ...state,
    enrolledContactDetails: action.payload,
    isLoadingEnrolledContactDetails: false,
  }),
  'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingEnrolledContactDetails: false,
    getEnrolledContactDetailsError: action.payload,
  }),
  'SEND_MONEY/GET_BANKS': (state: SendMoneyState, action: {type: string}) => ({
    ...state,
    isLoadingBanks: true,
    getBanksError: null,
  }),
  'SEND_MONEY/GET_BANKS_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: Array<EnrolledContact>},
  ) => ({
    ...state,
    banks: action.payload,
    isLoadingBanks: false,
  }),
  'SEND_MONEY/GET_BANKS_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingBanks: false,
    getBanksError: action.payload,
  }),
  'SEND_MONEY/GET_COUNTRY_CODES': (
    state: SendMoneyState,
    action: {type: string},
  ) => ({
    ...state,
    isLoadingCountryCodes: true,
    getCountryCodesError: null,
  }),
  'SEND_MONEY/GET_COUNTRY_CODES_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: Array<MobileNumberCode>},
  ) => ({
    ...state,
    countryCodes: action.payload,
    isLoadingCountryCodes: false,
  }),
  'SEND_MONEY/GET_COUNTRY_CODES_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingCountryCodes: false,
    getCountryCodesError: action.payload,
  }),
  'SEND_MONEY/GET_TRANSFER_SERVICES': (
    state: SendMoneyState,
    action: {type: string},
  ) => ({
    ...state,
    isLoadingTransferServices: true,
    getTransferServicesError: null,
  }),
  'SEND_MONEY/GET_TRANSFER_SERVICES_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: Array<EnrolledContact>},
  ) => ({
    ...state,
    transferServices: action.payload,
    isLoadingTransferServices: false,
  }),
  'SEND_MONEY/GET_TRANSFER_SERVICES_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isLoadingTransferServices: false,
    getTransferServicesError: action.payload,
  }),
  'SEND_MONEY/SUBMIT': (
    state: SendMoneyState,
    action: {type: string, payload: any},
  ) => ({
    ...state,
    isSubmitting: true,
    submitError: null,
  }),
  'SEND_MONEY/SUBMIT_SUCCESS': (
    state: SendMoneyState,
    action: {type: string, payload: any},
  ) => ({
    ...state,
    isSubmitting: false,
    payload: action.payload,
  }),
  'SEND_MONEY/SUBMIT_FAILED': (
    state: SendMoneyState,
    action: {type: string, payload: Error},
  ) => ({
    ...state,
    isSubmitting: false,
    submitError: action.payload,
  }),
};

const reducer = (
  state: SendMoneyState = INITIAL_STATE,
  action: SendMoneyAction,
): SendMoneyState => {
  if (actionMap[action.type]) {
    return actionMap[action.type](state, action);
  } else {
    return state;
  }
};

export default reducer;
