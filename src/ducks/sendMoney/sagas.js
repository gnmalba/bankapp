/**
 * @flow
 */

import {call, select, takeLatest, put} from 'redux-saga/effects';

import {getCustomerID, getUsername} from '@ducks/commonSelectors';
import sendMoneyService from '@api/services/sendMoneyService';
import {generateHash, getDeviceInfo} from '@utils/commons';

import {
  getBanksFailed,
  getBanksSuccess,
  getCountryCodesFailed,
  getCountryCodesSuccess,
  getEnrolledContactDetailsFailed,
  getEnrolledContactDetailsSuccess,
  getEnrolledContactsFailed,
  getEnrolledContactsSuccess,
  getRecipientAccountsFailed,
  getRecipientAccountsSuccess,
  getSourceAccountsFailed,
  getSourceAccountsSuccess,
  getTransferServicesFailed,
  getTransferServicesSuccess,
  submitFailed,
  submitSuccess,
} from './actions';
import {depositAccountsSelector} from './selectors';

import type {
  ApiErrorResponse,
  Account,
  CpcAccount,
  DepositAccount,
  EnrolledContact,
} from './types';
import {
  allowedAccountsFilter,
  availableBalanceWithCurrency,
  excludeSourceFromRecipients,
  byAlias,
  // excludeSourceFromEnrolledContacts,
  mockApiService,
  determineTransactionType,
} from './utils';

export function* getDepositAccounts(): Generator<
  any,
  Array<DepositAccount>,
  void,
> {
  const depositAccounts = yield select(depositAccountsSelector);
  if (depositAccounts) {
    return depositAccounts.filter(allowedAccountsFilter);
  } else {
    return [];
  }
}

export function* getSourceAccountsWatcher(): Generator<any, any, any> {
  try {
    const customerId = yield select(getCustomerID);
    const depositAccounts: Array<DepositAccount> = yield call(
      getDepositAccounts,
    );
    // cpcAccounts include credit, prepaid, and yazz
    const cpcAccounts: Array<CpcAccount> = yield call(
      sendMoneyService.getSourceAccounts,
      customerId,
      'all',
    );

    // apply data transformations to convert to DepositAccount or CpcAccount to Account type
    const mappedDepositAccounts: Array<Account> = depositAccounts.map(
      (account) => ({
        ...account,
        identifier: account.accountNo,
        isCpc: false,
      }),
    );
    const mappedCpcAccounts: Array<Account> = cpcAccounts.map((account) => ({
      ...account,
      identifier: account.cardSerNo,
      isCpc: true,
      availableBalanceValueWithCurrency: availableBalanceWithCurrency(
        account.availableBalance,
        account.currency,
      ),
    }));
    const sourceAccounts: Array<Account> = [
      ...mappedDepositAccounts,
      ...mappedCpcAccounts,
    ].sort(byAlias);

    yield put(getSourceAccountsSuccess(sourceAccounts));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getSourceAccountsFailed(error));
  }
}

export function* getRecipientAccountsWatcher(action: {
  payload: {sourceAccount: DepositAccount | CpcAccount},
}): Generator<any, any, any> {
  const sourceAccount: DepositAccount | CpcAccount =
    action.payload.sourceAccount;
  try {
    const depositAccounts: Array<DepositAccount> = yield call(
      getDepositAccounts,
    );
    const mappedPrepaidAccounts: Array<Account> = []; // TO-DO: include MB prepaid cards, coordinate with backend for steps moving forward.
    const mappedDepositAccounts: Array<Account> = depositAccounts.map(
      (account) => ({
        ...account,
        identifier: account.accountNo,
      }),
    );

    const recipientAccounts: Array<Account> = [
      ...mappedDepositAccounts,
      ...mappedPrepaidAccounts,
    ]
      .sort(byAlias)
      .filter(excludeSourceFromRecipients(sourceAccount));

    yield put(getRecipientAccountsSuccess(recipientAccounts));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getRecipientAccountsFailed(error));
  }
}

export function* getEnrolledContactsWatcher(): Generator<any, any, any> {
  try {
    const customerId = yield select(getCustomerID);
    const rawData: Array<EnrolledContact> = yield call(
      sendMoneyService.getEnrolledContacts,
      customerId,
    );
    const enrolledContacts: Array<EnrolledContact> = rawData;
    yield put(getEnrolledContactsSuccess(enrolledContacts));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getEnrolledContactsFailed(error));
  }
}

export function* getBanksWatcher(): Generator<any, any, any> {
  try {
    const banks = yield call(sendMoneyService.getBanks);

    yield put(getBanksSuccess(banks));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getBanksFailed(error));
  }
}

export function* getCountryCodesWatcher(): Generator<any, any, any> {
  try {
    const countryCodes = yield call(sendMoneyService.getCountryCodes);

    yield put(getCountryCodesSuccess(countryCodes));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getCountryCodesFailed(error));
  }
}
export function* getTransferServicesWatcher(): Generator<any, any, any> {
  try {
    const transferServices = yield call(mockApiService.getTransferServices); // TO-DO: replace mock API call when endpoint is ready

    yield put(getTransferServicesSuccess(transferServices));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getTransferServicesFailed(error));
  }
}

export function* getEnrolledContactDetailsWatcher(action: {
  payload: {contactId: string},
}): Generator<any, any, any> {
  try {
    const customerId = yield select(getCustomerID);
    const contactId = action.payload;

    const enrolledContactDetails: EnrolledContact = yield call(
      sendMoneyService.getEnrolledContactDetails,
      customerId,
      contactId,
    );

    yield put(getEnrolledContactDetailsSuccess(enrolledContactDetails));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(getEnrolledContactDetailsFailed(error));
  }
}

export function* submitWatcher(action: {
  payload: any,
}): Generator<any, any, any> {
  try {
    const {payload} = action;
    const {scheduleType, recipientType, isCpc, ...formValues} = payload;
    const transactionType = determineTransactionType(
      scheduleType,
      recipientType,
    );

    const username = yield select(getUsername);
    const customerId = yield select(getCustomerID);

    const hash = generateHash({
      accountId: payload.accountId,
      username,
      amount: payload.amount,
      currencyCode: payload.currencyCode,
      billerId: payload.contactId,
    });

    const apiParams = {
      ...formValues,
      hash,
      customerId,
      customerDevice: getDeviceInfo(),
    };

    const response = yield call(
      sendMoneyService.submit,
      apiParams,
      transactionType,
      isCpc,
    );

    yield put(submitSuccess(response));
  } catch (axiosError) {
    const error: ApiErrorResponse = axiosError.response.data;
    yield put(submitFailed(error));
  }
}

export default function* saga(): Generator<any, any, any> {
  yield takeLatest('SEND_MONEY/GET_SOURCE_ACCOUNTS', getSourceAccountsWatcher);
  yield takeLatest(
    'SEND_MONEY/GET_RECIPIENT_ACCOUNTS',
    getRecipientAccountsWatcher,
  );
  yield takeLatest(
    'SEND_MONEY/GET_ENROLLED_CONTACTS',
    getEnrolledContactsWatcher,
  );
  yield takeLatest('SEND_MONEY/GET_BANKS', getBanksWatcher);
  yield takeLatest('SEND_MONEY/GET_COUNTRY_CODES', getCountryCodesWatcher);
  yield takeLatest(
    'SEND_MONEY/GET_TRANSFER_SERVICES',
    getTransferServicesWatcher,
  );
  yield takeLatest(
    'SEND_MONEY/GET_ENROLLED_CONTACT_DETAILS',
    getEnrolledContactDetailsWatcher,
  );
  yield takeLatest('SEND_MONEY/SUBMIT', submitWatcher);
}
