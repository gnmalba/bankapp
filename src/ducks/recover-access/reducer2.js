// @flow

import {RECOVER_ACCESS_FAILED} from './actionTypes';
import reducer3 from './reducer3';

import {
  MAXIMUM_INVALID_CREDENTIALS,
  // MAXIMUM_INVALID_CREDENTIALS_MSG,
  MAXIMUM_INVALID_OTP,
  MAXIMUM_INVALID_OTP_MSG,
  // INVALID_PASSCODE,
} from './config';

import type {RecoverAccessStateType} from './flowtypes';

export default (state: RecoverAccessStateType, action: Object): any => {
  let newState: Object;
  if (action.type === RECOVER_ACCESS_FAILED) {
    newState = {
      ...state,
      loading: false,
      error: getAccessFailedError(state, action),
      invalidCredentialsCount: action.invalidCredentials
        ? state.invalidCredentialsCount + 1
        : state.invalidCredentialsCount,
      invalidPasscodeCount: action.invalidPasscode
        ? state.invalidPasscodeCount + 1
        : state.invalidPasscodeCount,
      invalidOtpCount: action.invalidOtp
        ? state.invalidOtpCount + 1
        : state.invalidOtpCount,
      proceedToPasscode: action.proceedToPasscode || false,
      proceedToPassword: false,
      proceedToOtp: action.proceedToOtp || false,
      mfaToken: action.mfaToken || state.mfaToken,
      passcodeErrorMsg: action.passcodeErrorMsg || state.error,
      mpin: action.clearMpin === false ? state.mpin : '',
      displayError: action.displayError || state.displayError,
    };
  } else {
    newState = reducer3(state, action);
  }
  return newState;
};

const getAccessFailedError = (state, action) => {
  let accessFailedError = action.error;
  if (
    action.invalidCredentials &&
    state.invalidCredentialsCount >= MAXIMUM_INVALID_CREDENTIALS
  ) {
    // accessFailedError = MAXIMUM_INVALID_CREDENTIALS_MSG;
    // accessFailedError = INVALID_PASSCODE;
  }
  if (action.invalidOtp && state.invalidOtpCount >= MAXIMUM_INVALID_OTP) {
    accessFailedError = MAXIMUM_INVALID_OTP_MSG;
  }
  return accessFailedError;
};
