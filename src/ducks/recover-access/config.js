// @flow

export const INVALID_OTP_REQUIRED = 'invalid_otp_required';
export const CONNECT_ERROR = 'connect_error';
export const MPIN = 'mpin';
export const INVALID_MPIN_EXCEEDED = 'invalid_mpin_exceeded';
export const INVALID_PASSCODE =
  'The code seems to be invalid. Please try again.';
export const MFA_CONTINUE = 'mfa_continue';
export const INVALID_OTP_INVALID = 'invalid_otp_invalid';
export const INVALID_USER_CREDENTIALS = 'Invalid user credentials.';
export const INVALID_OTP = 'The code seems to be invalid. Please try again.';
export const PASSCODE_ATTEMPTS_EXCEEDED =
  'Invalid passcode attempts exceeded, your account will be temporarily locked.';
export const PUBLIC_TOKEN_ERROR =
  'Sorry, something went wrong. Please contact customer service.';
export const EMAIL_VERIFICATION_REGEX: any = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const RECOVER_ACCESS_INVALID_DATA = 'recover_access_invalid_data';
export const MPIN_REQUIRED = 'mpin_required';
export const TYPE_USERNAME = 'username';
export const PASSCODE_ERROR = 'Accepts numeric characters only.';
export const INVALID_EMAIL_OR_USERNAME_MISSING =
  'invalid_email_or_username_missing';
export const MAXIMUM_INVALID_CREDENTIALS = 5;
export const MAXIMUM_INVALID_CREDENTIALS_MSG =
  'You have exceeded the maximum attempts to enter valid credentials. Please contact customer service for further assistance.';
export const MAXIMUM_INVALID_OTP = 15;
export const MAXIMUM_INVALID_OTP_MSG =
  'Invalid OTP attempts exceeded, your account will be temporarily locked.';
export const STORAGE_KEY = 'recoverAccessState';
export const MAXIMUM_INVALID_PASSCODE_COUNT = 3;
export const MAXIMUM_INVALID_CREDENTIALS_COUNT = 5;
