import {
  SET_PASSCODE,
  SET_RECOVER_OTP,
  SET_RECOVER_PASSWORD,
  SET_RECOVER_EMAIL,
  SET_RECOVER_USERNAME,
  RECOVER_ACCESS_FAILED,
  SET_RECOVER_LOADING,
  GET_RECOVER_OTP,
  SET_RECOVER_ERROR,
  SET_RECOVER_MPIN,
  SET_PROCEED_TO_PASSWORD,
  RECOVER_CONFIRM_PASSWORD,
  SET_PUBLIC_KEY,
  RECOVER_PASSWORD_SUCCESS,
  SET_PASSWORD_RECOVERED,
  SET_RECOVER_TYPE,
  SET_USERNAME_EMAIL,
  SET_PROCEED_TO_PASSCODE,
  SET_PROCEED_TO_LOGIN,
  SET_RECOVER_SEND_OTP,
} from '../actionTypes';

import {PASSCODE_ERROR} from '../config';

import recoverAccess, {INITIAL_STATE, saveToAsyncStorage} from '../reducers';
const should = 'should work properly';
const testEmail = 'test@test.com';

describe('Recover access reducers', () => {
  it('saveToAsyncStorage should work properly', () => {
    saveToAsyncStorage({});
  });
  it('Should cover defaults', () => {
    recoverAccess();
  });
  it('Should cover other defaults', () => {
    recoverAccess(
      {
        ...INITIAL_STATE,
        ...{
          invalidOtpCount: 100,
          invalidCredentialsCount: 100,
        },
      },
      {
        invalidCredentials: true,
        invalidOtpCount: 100,
        invalidCredentialsCount: 100,
        invalidOtp: true,
      },
    );
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PASSCODE,
      passcode: ' ',
    });
    expect(newState.passcodeErrorMsg).toEqual(PASSCODE_ERROR);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PASSCODE,
      passcode: '123456',
    });
    expect(newState.passcodeErrorMsg).toEqual('');
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_OTP,
      otp: '123456',
    });
    expect(newState.otp).toEqual('123456');
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_EMAIL,
      email: testEmail,
    });
    expect(newState.email).toEqual(testEmail);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_SEND_OTP,
      email: testEmail,
    });
    expect(newState.sendOtpCounter).toEqual(1);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_USERNAME,
      username: testEmail,
    });
    expect(newState.username).toEqual(testEmail);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_PASSWORD,
      password: 'test',
    });
    expect(newState.passwordHasEightCharacters).toEqual(false);
    expect(newState.passwordHasAnUpperCharacter).toEqual(false);
    expect(newState.passwordHasANumericCharacter).toEqual(false);
    expect(newState.passwordHasASpecialCharacter).toEqual(false);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: RECOVER_ACCESS_FAILED,
      passcodeErrorMsg: 'Error',
    });
    expect(newState.passcodeErrorMsg).toEqual('Error');
  });
  it.each([true, false])(should, (bools) => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: RECOVER_ACCESS_FAILED,
      invalidCredentials: bools,
      invalidPasscode: bools,
      invalidOtp: bools,
      passcodeErrorMsg: 'Error',
    });
    expect(newState.passcodeErrorMsg).toEqual('Error');
  });
  it.each([true, false])(should, (bools) => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_LOADING,
      status: bools,
      error: bools ? 'Error' : undefined,
    });
    expect(newState.loading).toEqual(bools);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: GET_RECOVER_OTP,
    });
    expect(newState.loading).toEqual(false);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_ERROR,
      error: 'Error',
    });
    expect(newState.error).toEqual('Error');
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_MPIN,
      mpin: '123456',
    });
    expect(newState.mpin).toEqual('123456');
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PROCEED_TO_PASSWORD,
      status: true,
    });
    expect(newState.proceedToPassword).toEqual(true);
  });
  it.each([true, false])(should, (bools) => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PROCEED_TO_LOGIN,
      status: bools,
      usernameSent: bools,
    });
    expect(newState.proceedToLogin).toEqual(bools);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PROCEED_TO_PASSCODE,
      proceedToPasscode: true,
    });
    expect(newState.proceedToPasscode).toEqual(true);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: RECOVER_CONFIRM_PASSWORD,
      confirmPassword: '123456',
    });
    expect(newState.confirmPassword).toEqual('123456');
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PUBLIC_KEY,
      publicKey: '123456',
    });
    expect(newState.publicKey).toEqual('123456');
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: RECOVER_PASSWORD_SUCCESS,
      status: true,
    });
    expect(newState.passwordRecovered).toEqual(true);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_PASSWORD_RECOVERED,
      status: true,
    });
    expect(newState.passwordRecovered).toEqual(true);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_RECOVER_TYPE,
      recoverType: '123456',
    });
    expect(newState.recoverType).toEqual('123456');
  });
  it.each(['123456', testEmail])(should, (email) => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: SET_USERNAME_EMAIL,
      usernameEmail: email,
    });
    expect(newState.usernameEmail).toEqual(email);
  });
  it(should, () => {
    const newState = recoverAccess(INITIAL_STATE, {
      type: 'MEEKS',
      usernameEmail: '123456',
    });
    expect(newState).toEqual(INITIAL_STATE);
  });
});
