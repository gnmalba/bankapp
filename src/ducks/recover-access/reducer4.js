// @flow

import {INITIAL_STATE} from './reducers';
import AsyncStorage from '@react-native-async-storage/async-storage';
import reducer5 from './reducer5';

import {
  RECOVER_PASSWORD_SUCCESS,
  SET_PROCEED_TO_LOGIN,
  SET_RECOVER_TYPE,
} from './actionTypes';

import {STORAGE_KEY} from './config';

let initialState;
AsyncStorage.getItem(STORAGE_KEY)
  .then((state) => {
    const jsonValue = JSON.parse(state);
    initialState = jsonValue;
  })
  .catch((error) => error);

import type {RecoverAccessStateType} from './flowtypes';

export default (state: RecoverAccessStateType, action: Object): any => {
  let newState: Object;
  if (action.type === SET_PROCEED_TO_LOGIN) {
    newState = {
      ...INITIAL_STATE,
      proceedToLogin: action.status,
      showUsernameSentSnackbar: action.status,
      usernameSent: action.usernameSent || state.usernameSent,
      successfulUsernameSent: action.usernameSent
        ? {
            ...state.successfulUsernameSent,
            ...{
              [`${state.email}`]: new Date().valueOf(),
            },
          }
        : state.successfulUsernameSent,
    };
  } else if (action.type === RECOVER_PASSWORD_SUCCESS) {
    newState = {
      ...INITIAL_STATE,
      passwordRecovered: true,
      successfulPasswordReset: {
        ...state.successfulPasswordReset,
        ...{
          [`${state.usernameEmail}`]: new Date().valueOf(),
        },
      },
    };
  } else if (action.type === SET_RECOVER_TYPE) {
    const initState = initialState ? initialState : INITIAL_STATE;
    newState = {
      ...INITIAL_STATE,
      recoverType: action.recoverType,
      invalidCredentialsCount:
        state.invalidCredentialsCount || initState.invalidCredentialsCount,
      invalidPasscodeCount:
        state.invalidPasscodeCount || initState.invalidPasscodeCount,
      invalidOtpCount: state.invalidOtpCount || initState.invalidOtpCount,
      sendOtpCounter: state.sendOtpCounter || initState.sendOtpCounter,
      successfulPasswordReset:
        state.successfulPasswordReset || initState.successfulPasswordReset,
      successfulUsernameSent:
        state.successfulUsernameSent || initState.successfulUsernameSent,
    };
  } else {
    newState = reducer5(state, action);
  }
  return newState;
};
