// @flow

export const SET_PASSCODE = '@recoveraccess/SET_PASSCODE';
export const SET_RECOVER_EMAIL = '@recoveraccess/SET_RECOVER_EMAIL';
export const SET_RECOVER_USERNAME = '@recoveraccess/SET_RECOVER_USERNAME';
export const RECOVER_PASSWORD = '@recoveraccess/RECOVER_PASSWORD';
export const RECOVER_CONFIRM_PASSWORD =
  '@recoveraccess/RECOVER_CONFIRM_PASSWORD';
export const RECOVER_USERNAME = '@recoveraccess/RECOVER_USERNAME';
export const SET_RECOVER_OTP = '@recoveraccess/SET_RECOVER_OTP';
export const SET_RECOVER_PASSWORD = '@recoveraccess/SET_RECOVER_PASSWORD';
export const RECOVER_ACCESS_FAILED = '@recoveraccess/RECOVER_ACCESS_FAILED';
export const SET_RECOVER_LOADING = '@recoveraccess/SET_RECOVER_LOADING';
export const GET_RECOVER_OTP = '@recoveraccess/GET_RECOVER_OTP';
export const SET_PROCEED_TO_OTP = '@recoveraccess/SET_PROCEED_TO_OTP';
export const SET_PROCEED_TO_PASSWORD = '@recoveraccess/SET_PROCEED_TO_PASSWORD';
export const SET_RECOVER_ERROR = '@recoveraccess/SET_RECOVER_ERROR';
export const RECOVER_SUBMIT_OTP = '@recoveraccess/RECOVER_SUBMIT_OTP';
export const SET_RECOVER_MPIN = '@recoveraccess/SET_RECOVER_MPIN';
export const SET_RECOVER_PUBLIC_TOKEN =
  '@recoveraccess/SET_RECOVER_PUBLIC_TOKEN';
export const SEND_NEW_PASSWORD = '@recoveraccess/SEND_NEW_PASSWORD';
export const SET_PUBLIC_KEY = '@recoveraccess/SET_PUBLIC_KEY';
export const RECOVER_PASSWORD_SUCCESS =
  '@recoveraccess/RECOVER_PASSWORD_SUCCESS';
export const SET_PASSWORD_RECOVERED = '@recoveraccess/SET_PASSWORD_RECOVERED';
export const SET_RECOVER_TYPE = '@recoveraccess/SET_RECOVER_TYPE';
export const SET_USERNAME_EMAIL = '@recoveraccess/SET_USERNAME_EMAIL';
export const START_RECOVER_PASSWORD = '@recoveraccess/START_RECOVER_PASSWORD';
export const SET_PROCEED_TO_PASSCODE = '@recoveraccess/SET_PROCEED_TO_PASSCODE';
export const START_RECOVER_USERNAME = '@recoveraccess/START_RECOVER_USERNAME';
export const SET_PROCEED_TO_LOGIN = '@recoveraccess/SET_PROCEED_TO_LOGIN';
export const CONTINUE_SEND_EMAIL = '@recoveraccess/CONTINUE_SEND_EMAIL';
export const SET_RECOVER_SEND_OTP = '@recoveraccess/SET_RECOVER_SEND_OTP';
export const RECOVER_RESEND_OTP = '@recoveraccess/RECOVER_RESEND_OTP';
export const SET_RECOVER_OTP_TIMER = '@recoveraccess/SET_RECOVER_OTP_TIMER';
export const SET_RECOVER_DEC_OTP_TIMER =
  '@recoveraccess/SET_RECOVER_DEC_OTP_TIMER';
export const START_NEW_PASSCODE = '@recoveraccess/START_NEW_PASSCODE';
export const RECOVER_UPDATE_STATE = '@recoveraccess/RECOVER_UPDATE_STATE';
