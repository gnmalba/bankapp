// @flow

import axios from 'axios';
import api from '@api/';
import {JSEncrypt} from 'jsencrypt';
import {LOGIN_API} from '@ducks/apiConstants';
import {takeLatest, call, put} from 'redux-saga/effects';
import {
  RECOVER_PASSWORD,
  RECOVER_ACCESS_FAILED,
  SET_RECOVER_LOADING,
  GET_RECOVER_OTP,
  RECOVER_SUBMIT_OTP,
  SET_RECOVER_MPIN,
  SET_PROCEED_TO_PASSWORD,
  SET_PUBLIC_KEY,
  SEND_NEW_PASSWORD,
  RECOVER_PASSWORD_SUCCESS,
  START_RECOVER_PASSWORD,
  SET_PROCEED_TO_PASSCODE,
  START_RECOVER_USERNAME,
  CONTINUE_SEND_EMAIL,
  SET_PROCEED_TO_LOGIN,
  RECOVER_RESEND_OTP,
  SET_RECOVER_SEND_OTP,
  RECOVER_UPDATE_STATE,
  START_NEW_PASSCODE,
} from './actionTypes';
import {
  INVALID_OTP_REQUIRED,
  CONNECT_ERROR,
  MPIN,
  INVALID_MPIN_EXCEEDED,
  INVALID_PASSCODE,
  INVALID_USER_CREDENTIALS,
  PASSCODE_ATTEMPTS_EXCEEDED,
  PUBLIC_TOKEN_ERROR,
  MFA_CONTINUE,
  INVALID_OTP_INVALID,
  INVALID_OTP,
  EMAIL_VERIFICATION_REGEX,
  RECOVER_ACCESS_INVALID_DATA,
  MPIN_REQUIRED,
  TYPE_USERNAME,
  INVALID_EMAIL_OR_USERNAME_MISSING,
} from './config';
import {BASE_URL} from 'react-native-dotenv';
import {headers} from '@utils/constants';

const encrypt = new JSEncrypt();

export const fnGetPublicKey = async (): Promise<any> => {
  try {
    return await api.get(LOGIN_API.AUTHENTICATION_PUBLIC_KEY);
  } catch (error) {
    return error;
  }
};

export const fnRegisterMpin = async (payload: Object): Promise<any> => {
  try {
    return await api.post(LOGIN_API.REGISTER_MPIN, payload);
  } catch (error) {
    return error;
  }
};

export const checkType = (usernameEmail: string): string => {
  return EMAIL_VERIFICATION_REGEX.test(usernameEmail) ? 'email' : 'username';
};

export const fnRecover = async (args: Object): Promise<any> => {
  var payload = JSON.stringify(args.payloadData);
  var config = {
    method: 'post',
    url: `${BASE_URL}${LOGIN_API.RECOVER_ACCESS}`,
    headers: {...headers, 'Content-Type': 'application/json'},
    data: payload,
    timeout: 10000,
  };
  try {
    return await axios(config);
  } catch (e) {
    return e;
  }
};

export function* fnRecoverPassword({payloadData}: Object): any {
  const {usernameEmail, passcode, mfaToken, type} = payloadData;
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
  });
  let publicKeyResponse;
  try {
    publicKeyResponse = yield call(fnGetPublicKey);
    yield put({
      type: SET_PUBLIC_KEY,
      publicKey: publicKeyResponse.data.data,
    });
  } catch (error) {
    yield put({
      type: RECOVER_ACCESS_FAILED,
      error: PUBLIC_TOKEN_ERROR,
    });
  }
  encrypt.setPublicKey(publicKeyResponse?.data?.data);
  const mpin = encrypt.encrypt(passcode);
  yield put({
    type: SET_RECOVER_MPIN,
    mpin,
  });
  const res = yield call(fnRecover, {
    payloadData: {
      mpin,
      type,
      mfaToken,
      [`${checkType(usernameEmail)}`]: usernameEmail,
    },
  });
  const otpResponseCode = res.response.data?.data?.errors[0]?.code;
  const errorMessage = res.response?.data?.data?.errors[0]?.message;
  switch (otpResponseCode) {
    case INVALID_EMAIL_OR_USERNAME_MISSING:
    case RECOVER_ACCESS_INVALID_DATA:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        passcodeErrorMsg: INVALID_OTP,
        error: '',
        invalidCredentials: true,
      });
      break;
    case CONNECT_ERROR:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        passcodeErrorMsg: INVALID_USER_CREDENTIALS,
        error: '',
        invalidCredentials: true,
      });
      break;
    case MPIN:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        passcodeErrorMsg: INVALID_PASSCODE,
        error: '',
        invalidPasscode: true,
      });
      break;
    case INVALID_MPIN_EXCEEDED:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: PASSCODE_ATTEMPTS_EXCEEDED,
        invalidPasscode: true,
      });
      break;
    case INVALID_OTP_REQUIRED:
      yield put({
        type: GET_RECOVER_OTP,
        generatorId: res.response.data?.data?.errors[0]?.data?.generatorId,
        mfaToken: res.response.data?.mfaToken,
        proceedToOtp: true,
      });
      break;
    default:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: errorMessage || PUBLIC_TOKEN_ERROR,
        displayError: true,
      });
      break;
  }
}

export function* fnRecoverSubmitOtp({payloadData}: Object): any {
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
    error: '',
  });
  const {
    usernameEmail,
    email,
    generatorId,
    mfaToken,
    mpin,
    otp,
    type,
  } = payloadData;
  const keyType = type === TYPE_USERNAME ? 'email' : 'username';
  const keyValue = type === TYPE_USERNAME ? email : usernameEmail;
  const res = yield call(fnRecover, {
    payloadData: {
      generatorId,
      mfaToken,
      mpin,
      otp,
      type,
      [`${keyType}`]: keyValue,
    },
  });
  const otpResponseCode = res.response.data?.data?.errors[0]?.code;
  switch (otpResponseCode) {
    case MFA_CONTINUE:
      yield put({
        type:
          type === TYPE_USERNAME
            ? CONTINUE_SEND_EMAIL
            : SET_PROCEED_TO_PASSWORD,
        status: true,
        mfaToken: res.response.data?.mfaToken,
        payloadData: {type, email},
      });
      break;
    case INVALID_OTP_INVALID:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: INVALID_OTP,
        invalidOtp: true,
        clearMpin: false,
        mfaToken: res.response.data?.mfaToken,
      });
      break;
    case RECOVER_ACCESS_INVALID_DATA:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: PUBLIC_TOKEN_ERROR,
        invalidCredentials: true,
      });
      break;
    default:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: PUBLIC_TOKEN_ERROR,
        invalidOtp: true,
      });
      break;
  }
}

export function* fnSendNewPassword({payloadData}: Object): any {
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
  });
  const {
    usernameEmail,
    generatorId,
    mfaToken,
    mpin,
    otp,
    type,
    password,
  } = payloadData;
  try {
    yield call(fnRecover, {
      payloadData: {
        [`${checkType(usernameEmail)}`]: usernameEmail,
        generatorId,
        mfaToken,
        mpin,
        otp,
        type,
        password: encrypt.encrypt(password),
      },
    });
    yield put({
      type: RECOVER_PASSWORD_SUCCESS,
    });
  } catch (e) {
    yield put({
      type: RECOVER_ACCESS_FAILED,
      error: e.response?.data?.data?.errors[0]?.code,
    });
  }
}

export function* fnStartRecoverPassword({payloadData}: Object): any {
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
  });
  const {type, usernameEmail} = payloadData;
  const res = yield call(fnRecover, {
    payloadData: {
      type,
      [`${checkType(usernameEmail)}`]: usernameEmail,
    },
  });
  const startResponse = res.response?.data?.data?.errors[0]?.code;
  const errorMessage = res.response?.data?.data?.errors[0]?.message;
  switch (startResponse) {
    case MPIN_REQUIRED:
      yield put({
        type: SET_PROCEED_TO_PASSCODE,
        proceedToPasscode: true,
        mfaToken: res.response.data?.mfaToken,
      });
      break;
    case MPIN:
    case RECOVER_ACCESS_INVALID_DATA:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        invalidCredentials: true,
        error: '',
        proceedToPasscode: true,
      });
      break;
    case INVALID_MPIN_EXCEEDED:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: PASSCODE_ATTEMPTS_EXCEEDED,
      });
      break;
    case 'recover_passcode_password':
    default:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: errorMessage || PUBLIC_TOKEN_ERROR,
        displayError: true,
      });
      break;
  }
}

export function* fnStartRecoverUsername({payloadData}: Object): any {
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
  });
  const res = yield call(fnRecover, {payloadData});
  const startResponse = res.response.data?.data?.errors[0]?.code;
  switch (startResponse) {
    case INVALID_OTP_REQUIRED:
      yield put({
        type: GET_RECOVER_OTP,
        generatorId: res.response.data?.data?.errors[0]?.data?.generatorId,
        mfaToken: res.response.data?.mfaToken,
        proceedToOtp: true,
      });
      break;
    case INVALID_EMAIL_OR_USERNAME_MISSING:
    case RECOVER_ACCESS_INVALID_DATA:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: '',
        invalidCredentials: true,
        proceedToOtp: true,
      });
      break;
    default:
      yield put({
        type: RECOVER_ACCESS_FAILED,
        error: '',
        invalidOtp: true,
        proceedToOtp: true,
      });
      break;
  }
}

export function* fnContinueSendEmail({payloadData, mfaToken}: Object): any {
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
  });
  const {type, email} = payloadData;
  const res = yield call(fnRecover, {
    payloadData: {
      type,
      email,
      mfaToken,
    },
  });
  if (res.status === 200) {
    yield put({
      type: SET_PROCEED_TO_LOGIN,
      status: true,
      usernameSent: true,
    });
  } else {
    yield put({
      type: RECOVER_ACCESS_FAILED,
      error: PUBLIC_TOKEN_ERROR,
    });
  }
}

export function* fnResendOtp({payloadData}: Object): any {
  yield put({
    type: SET_RECOVER_SEND_OTP,
  });
  const res = yield call(fnRecover, {payloadData});
  const startResponse = res.response.data?.data?.errors[0]?.code;
  if (startResponse !== INVALID_OTP_REQUIRED) {
    yield put({
      type: RECOVER_ACCESS_FAILED,
      error: 'OTP request error. Please contact support.',
    });
  } else {
    yield put({
      type: GET_RECOVER_OTP,
      generatorId: res.response.data?.data?.errors[0]?.data?.generatorId,
      mfaToken: res.response.data?.mfaToken,
      proceedToOtp: true,
    });
  }
}

export function* fnStartNewPasscode({payloadData}: Object): any {
  yield put({
    type: SET_RECOVER_LOADING,
    status: true,
  });
  encrypt.setPublicKey(payloadData.encryptionPublicKey);
  const mpin = encrypt.encrypt(payloadData.passcode);
  const payload = {
    customerId: payloadData.customerId,
    mpin,
  };
  const res = yield call(fnRegisterMpin, payload);
  yield put({
    type: SET_RECOVER_LOADING,
    status: false,
  });
  if (res.status === 200) {
    yield put({
      type: RECOVER_UPDATE_STATE,
      updatedState: {
        proceedToSuccess: true,
      },
    });
  } else {
    yield put({
      type: RECOVER_ACCESS_FAILED,
      error: 'The code seems to be invalid, please try again.',
    });
  }
}

export default function* watcher(): any {
  yield takeLatest(RECOVER_PASSWORD, fnRecoverPassword);
  yield takeLatest(RECOVER_SUBMIT_OTP, fnRecoverSubmitOtp);
  yield takeLatest(SEND_NEW_PASSWORD, fnSendNewPassword);
  yield takeLatest(START_RECOVER_PASSWORD, fnStartRecoverPassword);
  yield takeLatest(START_RECOVER_USERNAME, fnStartRecoverUsername);
  yield takeLatest(CONTINUE_SEND_EMAIL, fnContinueSendEmail);
  yield takeLatest(RECOVER_RESEND_OTP, fnResendOtp);
  yield takeLatest(START_NEW_PASSCODE, fnStartNewPasscode);
}
