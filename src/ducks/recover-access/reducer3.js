// @flow

import reducer4 from './reducer4';

import {
  SET_RECOVER_LOADING,
  GET_RECOVER_OTP,
  SET_RECOVER_ERROR,
  SET_RECOVER_MPIN,
  SET_PROCEED_TO_PASSWORD,
} from './actionTypes';

import type {RecoverAccessStateType} from './flowtypes';

export default (state: RecoverAccessStateType, action: Object): any => {
  let newState: Object;
  if (action.type === SET_RECOVER_LOADING) {
    newState = {
      ...state,
      loading: action.status,
      proceedToPasscode: action.status ? false : state.proceedToPasscode,
      proceedToPassword: action.status ? false : state.proceedToPasscode,
      proceedToOtp: action.status ? false : state.proceedToPasscode,
      error: action.error !== undefined ? action.error : state.error,
    };
  } else if (action.type === GET_RECOVER_OTP) {
    newState = {
      ...state,
      generatorId: action.generatorId,
      mfaToken: action.mfaToken,
      proceedToOtp: action.proceedToOtp,
      loading: false,
      error: '',
    };
  } else if (action.type === SET_RECOVER_ERROR) {
    newState = {
      ...state,
      error: action.error,
    };
  } else if (action.type === SET_RECOVER_MPIN) {
    newState = {
      ...state,
      mpin: action.mpin,
    };
  } else if (action.type === SET_PROCEED_TO_PASSWORD) {
    newState = {
      ...state,
      proceedToPassword: action.status,
      loading: false,
      mfaToken: action.mfaToken,
    };
  } else {
    newState = reducer4(state, action);
  }
  return newState;
};
