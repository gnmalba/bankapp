// @flow

import AsyncStorage from '@react-native-async-storage/async-storage';
import reducer2 from './reducer2';

import {
  SET_PASSCODE,
  SET_RECOVER_OTP,
  SET_RECOVER_PASSWORD,
  SET_RECOVER_EMAIL,
  SET_RECOVER_USERNAME,
  SET_RECOVER_SEND_OTP,
} from './actionTypes';

import {PASSCODE_ERROR, STORAGE_KEY} from './config';

import type {RecoverAccessStateType} from './flowtypes';

export const INITIAL_STATE: RecoverAccessStateType = {
  passcode: '',
  passcodeErrorMsg: '',
  usernameEmail: '',
  usernameEmailError: false,
  otp: '',
  otpErrorMessage: '',
  password: '',
  passwordHasNoSpace: true,
  passwordHasEightCharacters: false,
  passwordHasAnUpperCharacter: false,
  passwordHasALowerCaseCharacter: false,
  passwordHasANumericCharacter: false,
  passwordHasASpecialCharacter: false,
  passwordHasSpace: false,
  confirmPassword: '',
  username: '',
  email: '',
  emailError: '',
  error: '',
  loading: false,
  generatorId: '',
  mfaToken: '',
  proceedToPasscode: false,
  proceedToOtp: false,
  proceedToPassword: false,
  proceedToLogin: false,
  proceedToSuccess: false,
  invalidCredentialsCount: 0,
  invalidPasscodeCount: 0,
  invalidOtpCount: 0,
  mpin: '',
  publicKey: '',
  passwordRecovered: false,
  recoverType: '',
  showUsernameSentSnackbar: false,
  sendOtpCounter: 0,
  successfulPasswordReset: {},
  usernameSent: false,
  successfulUsernameSent: {},
  displayError: false,
};

export const saveToAsyncStorage = (currentState: Object) => {
  const {
    invalidCredentialsCount,
    invalidPasscodeCount,
    invalidOtpCount,
    sendOtpCounter,
  } = currentState;
  AsyncStorage.setItem(
    STORAGE_KEY,
    JSON.stringify({
      invalidCredentialsCount,
      invalidPasscodeCount,
      invalidOtpCount,
      sendOtpCounter,
    }),
  ).catch((error) => {
    return error;
  });
};

export default (
  state: RecoverAccessStateType = INITIAL_STATE,
  action: Object = {},
): any => {
  let newState: Object;
  if (action.type === SET_PASSCODE) {
    newState = {
      ...state,
      passcode: action.passcode,
      passcodeErrorMsg: /[._,-\s]/.test(action.passcode) ? PASSCODE_ERROR : '',
      error: '',
      mpin: '',
      mfaToken: '',
    };
  } else if (action.type === SET_RECOVER_OTP) {
    newState = {
      ...state,
      otp: action.otp,
      error: '',
    };
  } else if (action.type === SET_RECOVER_EMAIL) {
    newState = {
      ...state,
      email: action.email,
      proceedToOtp: false,
    };
  } else if (action.type === SET_RECOVER_SEND_OTP) {
    newState = {
      ...state,
      sendOtpCounter: state.sendOtpCounter + 1,
      otp: '',
    };
  } else if (action.type === SET_RECOVER_USERNAME) {
    newState = {
      ...state,
      username: action.username,
    };
  } else if (action.type === SET_RECOVER_PASSWORD) {
    newState = {
      ...state,
      password: action.password,
      passwordHasEightCharacters: action.password.length > 7,
      passwordHasNoSpace: !/\s/g.test(action.password),
      passwordHasALowerCaseCharacter: /[a-z]/.test(action.password),
      passwordHasAnUpperCharacter: /[A-Z]/.test(action.password),
      passwordHasANumericCharacter: /[0-9]/.test(action.password),
      passwordHasASpecialCharacter: /[ `!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/.test(
        action.password,
      ),
      passwordHasSpace: / /g.test(action.password),
    };
  } else {
    newState = reducer2(state, action);
  }
  saveToAsyncStorage(newState);
  return newState;
};
