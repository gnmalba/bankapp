// @flow

import {
  RECOVER_CONFIRM_PASSWORD,
  SET_PUBLIC_KEY,
  SET_PASSWORD_RECOVERED,
  SET_USERNAME_EMAIL,
  SET_PROCEED_TO_PASSCODE,
  RECOVER_UPDATE_STATE,
} from './actionTypes';

import type {RecoverAccessStateType} from './flowtypes';

export default (state: RecoverAccessStateType, action: Object): any => {
  let newState: Object;
  if (action.type === SET_USERNAME_EMAIL) {
    newState = {
      ...state,
      usernameEmail: action.usernameEmail,
      usernameEmailError:
        action.usernameEmail.length < 8 && action.usernameEmail.length > 0,
      passcode: '',
      passcodeErrorMsg: '',
    };
  } else if (action.type === SET_PASSWORD_RECOVERED) {
    newState = {
      ...state,
      passwordRecovered: action.status,
    };
  } else if (action.type === SET_PUBLIC_KEY) {
    newState = {
      ...state,
      publicKey: action.publicKey,
    };
  } else if (action.type === RECOVER_CONFIRM_PASSWORD) {
    newState = {
      ...state,
      confirmPassword: action.confirmPassword,
    };
  } else if (action.type === SET_PROCEED_TO_PASSCODE) {
    newState = {
      ...state,
      proceedToPasscode: action.proceedToPasscode,
      loading: false,
      mfaToken: action.mfaToken,
      error: '',
    };
  } else if (action.type === RECOVER_UPDATE_STATE) {
    newState = {
      ...state,
      ...action.updatedState,
    };
  } else {
    newState = state;
  }
  return newState;
};
