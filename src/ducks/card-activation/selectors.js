/**
 * @flow
 */
import type {State} from '../types';

import type {
  CardActivationListApiResponse,
  CardActivationState,
} from '@ducks/card-activation/types';

export const getCardsForActivation = (
  state: State,
): CardActivationListApiResponse => state.cardActivation.data;

export const getCardActivationState = (state: State): CardActivationState =>
  state.cardActivation;
