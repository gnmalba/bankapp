/**
 * @flow
 */

import type {
  CardActivationAction,
  CardActivationListApiResponse,
} from './types';

export const getActivationListRequested = (): CardActivationAction => ({
  type: 'CARD_ACTIVATION_LIST_REQUESTED',
});
export const getActivationListSuccess = (
  list: CardActivationListApiResponse,
): CardActivationAction => ({
  type: 'CARD_ACTIVATION_LIST_SUCCESS',
  payload: {list},
});

export const getActivationListFailed = (): CardActivationAction => ({
  type: 'CARD_ACTIVATION_LIST_FAILED',
});

export const submitCardForActivation = (
  cardSerialNo: string,
  passcode: string,
): CardActivationAction => ({
  type: 'ACTIVATE_CARD_REQUESTED',
  payload: {
    cardSerialNo,
    passcode,
  },
});

export const submitCardForActivationSuccess = (
  cardSerialNo: string,
): CardActivationAction => ({
  type: 'ACTIVATE_CARD_SUCCESS',
  payload: {
    cardSerialNo,
  },
});

export const submitCardForActivationFailed = (): CardActivationAction => ({
  type: 'ACTIVATE_CARD_FAILED',
});

export const resetSubmitError = (): CardActivationAction => ({
  type: 'RESET_SUBMISSION_ERROR',
});

export const setPasscodeExceeds = (): CardActivationAction => ({
  type: 'SET_PASSCODE_EXCEEDS',
});

export const setInvalidPasscode = (
  invalidPasscodeErrorMessage: string,
): CardActivationAction => ({
  type: 'SET_INVALID_PASSCODE',
  payload: {
    invalidPasscodeErrorMessage,
  },
});

export const resetPasscodeError = (): CardActivationAction => ({
  type: 'RESET_PASSCODE_ERROR',
});

export const resetActivationSuccess = (): CardActivationAction => ({
  type: 'RESET_SUBMISSION_SUCCESS',
});

export const refreshCardsForActivation = (): CardActivationAction => ({
  type: 'REFRESH_CARDS_FOR_ACTIVATION',
});
