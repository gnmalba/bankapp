/**
 * @flow
 */

import {select, call, put, takeLatest} from 'redux-saga/effects';

import {
  getCustomerID,
  getUsername,
  getEncryptionPublicKey,
} from '@ducks/commonSelectors';

import CreditService from '@api/services/creditService';

import encrypt from '@utils/encryptString';

import {
  getActivationListRequested,
  getActivationListSuccess,
  getActivationListFailed,
  setPasscodeExceeds,
  submitCardForActivationSuccess,
  submitCardForActivationFailed,
  setInvalidPasscode,
} from './actions';

const PASSCODE_DEFAULT_INVALID_MESSAGE =
  'Passcode is invalid. Please try again';

export function* fetchCardsForActivation(): Generator<any, void, any> {
  const customerId = yield select(getCustomerID);

  yield put(getActivationListRequested());

  try {
    const response = yield call(
      CreditService.getCreditCardsForActivation,
      customerId,
    );

    const {data} = response.data;

    yield put(getActivationListSuccess(data.listCards));
  } catch (error) {
    yield put(getActivationListFailed());
  }
}

export function* activateCardRequested({
  payload: {cardSerialNo, passcode},
}: {
  payload: {
    cardSerialNo: string,
    passcode: string,
  },
}): Generator<any, void, any> {
  const customerId = yield select(getCustomerID);
  const username = yield select(getUsername);
  const publicKey = yield select(getEncryptionPublicKey);

  try {
    yield call(CreditService.activateCreditCard, customerId, {
      username,
      cardSerialNo,
      passcode: encrypt(publicKey).encrypt(passcode),
    });
    yield put(submitCardForActivationSuccess(cardSerialNo));
  } catch (apiErrorResponse) {
    const response = apiErrorResponse.response;
    if (response.status === 400) {
      const error = response?.data?.data?.errors[0];
      if (error.code === 'invalid_mpin_exceeded') {
        yield put(setPasscodeExceeds());
      } else {
        yield put(setInvalidPasscode(PASSCODE_DEFAULT_INVALID_MESSAGE));
      }
    } else {
      yield put(submitCardForActivationFailed());
    }
  }
}

function* cardActivationWatcher(): Generator<any, void, any> {
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', fetchCardsForActivation);
  yield takeLatest('REFRESH_CARDS_FOR_ACTIVATION', fetchCardsForActivation);
  yield takeLatest('ACTIVATE_CARD_REQUESTED', activateCardRequested);
}

export default cardActivationWatcher;
