import {expectSaga} from 'redux-saga-test-plan';
import {throwError} from 'redux-saga-test-plan/providers';
import * as matchers from 'redux-saga-test-plan/matchers';

import cardActivationWatcher from '../sagas';
import {
  getActivationListSuccess,
  getActivationListFailed,
  submitCardForActivationSuccess,
} from '../actions';

import CreditService from '@api/services/creditService';

import {
  getCustomerID,
  getUsername,
  getEncryptionPublicKey,
} from '@ducks/commonSelectors';

describe('Activate Card Sagas', () => {
  let testCustomerId, testUsername, testPublickey;

  beforeAll(() => {
    testCustomerId = 'testCustomerId123';
    testUsername = 'testUsername123';
    testPublickey = 'testPublickey123445';
  });

  it('Should call fetchCardsForActivation success', () => {
    const fakeListCardsApiResponse = {
      data: {
        data: {
          listCards: [],
        },
      },
    };

    return expectSaga(cardActivationWatcher)
      .provide([
        [matchers.select(getCustomerID), testCustomerId],
        [
          matchers.call(
            CreditService.getCreditCardsForActivation,
            testCustomerId,
          ),
          fakeListCardsApiResponse,
        ],
      ])
      .put(
        getActivationListSuccess(fakeListCardsApiResponse.data.data.listCards),
      )
      .dispatch({
        type: 'GET_CUSTOMER_INFO_SUCCESS',
      })
      .run();
  });

  it('Should call fetchCardsForActivation failure', () => {
    const fakeApiError = new Error('testApiError');

    return expectSaga(cardActivationWatcher)
      .provide([
        [matchers.select(getCustomerID), testCustomerId],
        [
          matchers.call(
            CreditService.getCreditCardsForActivation,
            testCustomerId,
          ),
          throwError(fakeApiError),
        ],
      ])
      .put(getActivationListFailed())
      .dispatch({
        type: 'GET_CUSTOMER_INFO_SUCCESS',
      })
      .run();
  });

  it('Should call activateCardRequested success', () => {
    const testCardSerNo = 'cardSerNo123';
    const testPasscode = 'passcode123';

    return expectSaga(cardActivationWatcher)
      .provide([
        [matchers.select(getCustomerID), testCustomerId],
        [matchers.select(getUsername), testUsername],
        [matchers.select(getEncryptionPublicKey), testPublickey],
        [
          matchers.call(CreditService.activateCreditCard, testCustomerId, {
            username: testUsername,
            cardSerialNo: testCardSerNo,
            passcode: testPasscode,
          }),
        ],
      ])
      .put(submitCardForActivationSuccess(testCardSerNo))
      .dispatch({
        type: 'ACTIVATE_CARD_REQUESTED',
        payload: {
          cardSerialNo: testCardSerNo,
          passcode: testPasscode,
        },
      })
      .run();
  });
});
