import reducer from '../reducer';
import {CardActivationState} from '../types';

const INITIAL_STATE: CardActivationState = {
  data: [
    {
      cardSerNo: '123',
    },
  ],
  fetching: false,
  submitting: false,
  passcodeExceeds: false,
  submittingError: null,
  invalidPasscodeErrorMessage: null,
  activatedCard: null,
};
describe('Card Activation reducer', () => {
  test('CARD_ACTIVATION_LIST_REQUESTED', () => {
    const action = {
      type: 'CARD_ACTIVATION_LIST_REQUESTED',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      fetching: true,
    });
  });

  test('CARD_ACTIVATION_LIST_SUCCESS', () => {
    const action = {
      type: 'CARD_ACTIVATION_LIST_SUCCESS',
      payload: {list: {}},
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      fetching: INITIAL_STATE.fetching,
      data: {},
    });
  });

  test('CARD_ACTIVATION_LIST_FAILED', () => {
    const action = {
      type: 'CARD_ACTIVATION_LIST_FAILED',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      fetching: false,
    });
  });

  test('ACTIVATE_CARD_REQUESTED', () => {
    const action = {
      type: 'ACTIVATE_CARD_REQUESTED',
      payload: {
        cardSerialNo: '123',
        passcode: '1123',
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      submitting: true,
    });
  });

  test('ACTIVATE_CARD_SUCCESS', () => {
    const action = {
      type: 'ACTIVATE_CARD_SUCCESS',
      payload: {
        cardSerialNo: '123',
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      activatedCard: action.payload.cardSerialNo,
      data: INITIAL_STATE.data.filter(
        (card) => card.cardSerNo !== action.payload.cardSerialNo,
      ),
      submitting: false,
    });
  });

  test('ACTIVATE_CARD_FAILED', () => {
    const action = {
      type: 'ACTIVATE_CARD_FAILED',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      submittingError: 'API_ERROR',
      submitting: false,
    });
  });

  test('RESET_SUBMISSION_ERROR', () => {
    const action = {
      type: 'RESET_SUBMISSION_ERROR',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      submittingError: INITIAL_STATE.submittingError,
    });
  });

  test('SET_PASSCODE_EXCEEDS', () => {
    const action = {
      type: 'SET_PASSCODE_EXCEEDS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      submitting: false,
      passcodeExceeds: true,
    });
  });

  test('SET_INVALID_PASSCODE', () => {
    const action = {
      type: 'SET_INVALID_PASSCODE',
      payload: {
        invalidPasscodeErrorMessage: 'error',
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      submitting: false,
      invalidPasscodeErrorMessage: 'error',
    });
  });

  test('RESET_PASSCODE_ERROR', () => {
    const action = {
      type: 'RESET_PASSCODE_ERROR',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      passcodeExceeds: INITIAL_STATE.passcodeExceeds,
      invalidPasscodeErrorMessage: INITIAL_STATE.invalidPasscodeErrorMessage,
    });
  });

  test('RESET_SUBMISSION_SUCCESS', () => {
    const action = {
      type: 'RESET_SUBMISSION_SUCCESS',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      activatedCard: INITIAL_STATE.activatedCard,
    });
  });

  test('REFRESH_CARDS_FOR_ACTIVATION', () => {
    const action = {
      type: 'REFRESH_CARDS_FOR_ACTIVATION',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
    });
  });
});
