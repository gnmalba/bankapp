import {
  getActivationListRequested,
  getActivationListSuccess,
  getActivationListFailed,
  submitCardForActivation,
  submitCardForActivationSuccess,
  submitCardForActivationFailed,
  resetSubmitError,
  setPasscodeExceeds,
  setInvalidPasscode,
  resetPasscodeError,
  resetActivationSuccess,
  refreshCardsForActivation,
} from '../actions';

describe('Unit test for Card Activation Actions', () => {
  const actionCreators = [
    [
      getActivationListRequested(),
      {
        type: 'CARD_ACTIVATION_LIST_REQUESTED',
      },
    ],
    [
      getActivationListSuccess([1, 2, 3]),
      {
        type: 'CARD_ACTIVATION_LIST_SUCCESS',
        payload: {
          list: [1, 2, 3],
        },
      },
    ],
    [
      getActivationListFailed(),
      {
        type: 'CARD_ACTIVATION_LIST_FAILED',
      },
    ],
    [
      submitCardForActivation('testSerNo123', 'testPasscode123'),
      {
        type: 'ACTIVATE_CARD_REQUESTED',
        payload: {
          cardSerialNo: 'testSerNo123',
          passcode: 'testPasscode123',
        },
      },
    ],
    [
      submitCardForActivationSuccess('testSerNo123'),
      {
        type: 'ACTIVATE_CARD_SUCCESS',
        payload: {
          cardSerialNo: 'testSerNo123',
        },
      },
    ],
    [
      submitCardForActivationFailed(),
      {
        type: 'ACTIVATE_CARD_FAILED',
      },
    ],
    [
      resetSubmitError(),
      {
        type: 'RESET_SUBMISSION_ERROR',
      },
    ],
    [
      setPasscodeExceeds(),
      {
        type: 'SET_PASSCODE_EXCEEDS',
      },
    ],
    [
      setInvalidPasscode('testErrorMessage123'),
      {
        type: 'SET_INVALID_PASSCODE',
        payload: {
          invalidPasscodeErrorMessage: 'testErrorMessage123',
        },
      },
    ],
    [
      resetPasscodeError(),
      {
        type: 'RESET_PASSCODE_ERROR',
      },
    ],
    [
      resetActivationSuccess(),
      {
        type: 'RESET_SUBMISSION_SUCCESS',
      },
    ],
    [
      refreshCardsForActivation(),
      {
        type: 'REFRESH_CARDS_FOR_ACTIVATION',
      },
    ],
  ];

  it.each(actionCreators)(
    'Should create action creator %o.type',
    (actionCreator, expectedAction) => {
      expect(actionCreator).toEqual(expectedAction);
    },
  );
});
