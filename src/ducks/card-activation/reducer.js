/**
 * @flow
 */

import type {CardActivationState, CardActivationAction} from './types';

const INITIAL_STATE: CardActivationState = {
  data: [],
  fetching: false,
  submitting: false,
  passcodeExceeds: false,
  submittingError: null,
  invalidPasscodeErrorMessage: null,
  activatedCard: null,
};

export default (
  state: CardActivationState = INITIAL_STATE,
  action: CardActivationAction,
): CardActivationState => {
  switch (action.type) {
    case 'CARD_ACTIVATION_LIST_REQUESTED':
      return {
        ...state,
        fetching: true,
      };
    case 'CARD_ACTIVATION_LIST_SUCCESS':
      return {
        ...state,
        fetching: INITIAL_STATE.fetching,
        data: action.payload.list,
      };
    case 'CARD_ACTIVATION_LIST_FAILED':
      return {
        ...state,
        fetching: false,
      };
    case 'ACTIVATE_CARD_REQUESTED':
      return {
        ...state,
        submitting: true,
      };
    case 'ACTIVATE_CARD_SUCCESS':
      return {
        ...state,
        activatedCard: action.payload.cardSerialNo,
        data: state.data.filter(
          (card) => card.cardSerNo !== action.payload.cardSerialNo,
        ),
        submitting: false,
      };
    case 'RESET_SUBMISSION_SUCCESS':
      return {
        ...state,
        activatedCard: INITIAL_STATE.activatedCard,
      };
    case 'ACTIVATE_CARD_FAILED':
      return {
        ...state,
        submittingError: 'API_ERROR',
        submitting: false,
      };
    case 'RESET_SUBMISSION_ERROR':
      return {
        ...state,
        submittingError: INITIAL_STATE.submittingError,
      };
    case 'SET_PASSCODE_EXCEEDS':
      return {
        ...state,
        submitting: false,
        passcodeExceeds: true,
      };

    case 'SET_INVALID_PASSCODE':
      return {
        ...state,
        submitting: false,
        invalidPasscodeErrorMessage: action.payload.invalidPasscodeErrorMessage,
      };
    case 'RESET_PASSCODE_ERROR':
      return {
        ...state,
        passcodeExceeds: INITIAL_STATE.passcodeExceeds,
        invalidPasscodeErrorMessage: INITIAL_STATE.invalidPasscodeErrorMessage,
      };
    default:
      return state;
  }
};
