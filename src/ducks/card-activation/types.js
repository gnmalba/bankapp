/**
 * @flow
 */

export type InActiveCard = {
  cardNo: string,
  cardSerNo: string,
  cardEmbossedName: string,
  customerId: string,
  primary: number,
  flag: string,
  outstandingBalance: number,
  availableCredit: number,
  creditLimit: number,
  alias: string,
  currency: string,
  type: string,
};

export type CardActivationListApiResponse = Array<InActiveCard>;

export type CardActivationState = {|
  fetching: boolean,
  data: CardActivationListApiResponse,
  submitting: boolean,
  passcodeExceeds: boolean,
  activatedCard: ?string,
  invalidPasscodeErrorMessage: ?string,
  submittingError: ?string,
|};

export type CardActivationAction =
  | {
      type: 'CARD_ACTIVATION_LIST_REQUESTED',
    }
  | {
      type: 'CARD_ACTIVATION_LIST_SUCCESS',
      payload: {
        list: CardActivationListApiResponse,
      },
    }
  | {
      type: 'CARD_ACTIVATION_LIST_FAILED',
    }
  | {
      type: 'ACTIVATE_CARD_REQUESTED',
      payload: {
        cardSerialNo: string,
        passcode: string,
      },
    }
  | {
      type: 'ACTIVATE_CARD_SUCCESS',
      payload: {
        cardSerialNo: string,
      },
    }
  | {
      type: 'ACTIVATE_CARD_FAILED',
    }
  | {
      type: 'RESET_SUBMISSION_ERROR',
    }
  | {
      type: 'SET_PASSCODE_EXCEEDS',
    }
  | {
      type: 'SET_INVALID_PASSCODE',
      payload: {
        invalidPasscodeErrorMessage: string,
      },
    }
  | {
      type: 'RESET_PASSCODE_ERROR',
    }
  | {
      type: 'RESET_SUBMISSION_SUCCESS',
    }
  | {
      type: 'REFRESH_CARDS_FOR_ACTIVATION',
    };
