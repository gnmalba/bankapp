import reducer from '../reducer';

import {
  getLostCardRequested,
  getLostCardRequestedSuccess,
  getLostCardFailed,
  getLostCardDetailsRequested,
  getLostCardDetailsSuccess,
  getLostCardDetailsFailed,
  reportLostCardRequested,
  reportLostCardSuccess,
  reportLostCardFailed,
  getLostCardTermsRequested,
  getLostCardTermsSuccess,
  getLostCardTermsFailed,
  reportLostCardResetErrors,
} from '../actions';
import {expectSaga} from 'redux-saga-test-plan';
import watchGetReportLostCard, {
  getReportLostCard,
  getReportLostCardDetails,
  reportLostCard,
  getLostCardTerms,
} from '../saga';
const INITIAL_LC_STATE = {
  data: [],
  loading: false,
  error: false,
  card: {},
  terms: null,
};

describe('Unit test for Lost Card Actions', () => {
  it('Should create getLostCardRequest properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_REPORTS_REQUESTED',
    };

    expect(getLostCardRequested()).toEqual(expectedAction);
  });

  it('Should create getLostCardRequestedSuccess properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_REPORTS_SUCCESS',
      payload: {data: []},
    };

    expect(getLostCardRequestedSuccess([])).toEqual(expectedAction);
  });

  it('Should create getLostCardFailed properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_REPORTS_FAILED',
      payload: {data: []},
    };

    expect(getLostCardFailed([])).toEqual(expectedAction);
  });

  it('Should create getLostCardDetailsRequested properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_DETAILS_REQUESTED',
      payload: {data: {}},
    };

    expect(getLostCardDetailsRequested({})).toEqual(expectedAction);
  });

  it('Should create getLostCardDetailsSuccess properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_DETAILS_SUCCESS',
      payload: {data: {}},
    };

    expect(getLostCardDetailsSuccess({})).toEqual(expectedAction);
  });

  it('Should create getLostCardDetailsFailed properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_DETAILS_FAILED',
      payload: {data: []},
    };

    expect(getLostCardDetailsFailed([])).toEqual(expectedAction);
  });

  it('Should create reportLostCardRequested properly', () => {
    const expectedAction = {
      type: 'REPORT_LOST_CARD_REQUESTED',
      payload: {data: {}},
    };

    expect(reportLostCardRequested({})).toEqual(expectedAction);
  });

  it('Should create reportLostCardSuccess properly', () => {
    const expectedAction = {
      type: 'REPORT_LOST_CARD_SUCCESS',
    };

    expect(reportLostCardSuccess()).toEqual(expectedAction);
  });

  it('Should create reportLostCardFailed properly', () => {
    const expectedAction = {
      type: 'REPORT_LOST_CARD_FAILED',
      payload: {data: {}},
    };

    expect(reportLostCardFailed({})).toEqual(expectedAction);
  });

  it('Should create getLostCardTermsRequested properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_TERMS_REQUESTED',
      payload: {data: {}},
    };

    expect(getLostCardTermsRequested({})).toEqual(expectedAction);
  });

  it('Should create getLostCardTermsSuccess properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_TERMS_SUCCESS',
      payload: {data: {}},
    };

    expect(getLostCardTermsSuccess({})).toEqual(expectedAction);
  });

  it('Should create getLostCardTermsFailed properly', () => {
    const expectedAction = {
      type: 'GET_CARD_BLOCKED_TERMS_FAILED',
      payload: {data: {}},
    };

    expect(getLostCardTermsFailed({})).toEqual(expectedAction);
  });

  it('Should create reportLostCardResetErrors properly', () => {
    const expectedAction = {
      type: 'LOSTCARD_RESET_ERRORS',
      payload: {data: true},
    };

    expect(reportLostCardResetErrors({data: true})).toEqual(expectedAction);
  });
});

describe('Unit test for Lost Card Reducer', () => {
  const INITIAL_STATE = {
    data: [],
    loading: false,
    error: false,
    card: {},
    terms: null,
  };
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  it('should handle GET_CARD_BLOCKED_REPORTS_REQUESTED', () => {
    const action = {
      type: 'GET_CARD_BLOCKED_REPORTS_REQUESTED',
    };

    const expectedState = {
      data: [],
      loading: true,
      error: false,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle GET_CARD_BLOCKED_REPORTS_FAILED', () => {
    const action = {
      type: 'GET_CARD_BLOCKED_REPORTS_FAILED',
      payload: {data: []},
    };

    const expectedState = {
      data: [],
      loading: false,
      error: true,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle GET_CARD_BLOCKED_REPORTS_SUCCESS', () => {
    const action = {
      type: 'GET_CARD_BLOCKED_REPORTS_SUCCESS',
      payload: {data: []},
    };

    const expectedState = {
      data: [],
      loading: false,
      error: false,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle GET_CARD_BLOCKED_DETAILS_SUCCESS', () => {
    const action = {
      type: 'GET_CARD_BLOCKED_DETAILS_SUCCESS',
      payload: {data: {}},
    };

    const expectedState = {
      data: [],
      loading: false,
      error: false,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle REPORT_LOST_CARD_SUCCESS', () => {
    const action = {
      type: 'REPORT_LOST_CARD_SUCCESS',
    };

    const expectedState = {
      data: [],
      loading: false,
      error: false,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle GET_CARD_BLOCKED_TERMS_SUCCESS', () => {
    const action = {
      type: 'GET_CARD_BLOCKED_TERMS_SUCCESS',
      payload: {data: {}},
    };

    const expectedState = {
      data: [],
      loading: false,
      error: false,
      card: {},
      terms: {},
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle LOSTCARD_RESET_ERRORS', () => {
    const action = {
      type: 'LOSTCARD_RESET_ERRORS',
      payload: false,
    };

    const expectedState = {
      data: [],
      loading: false,
      error: false,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });

  it('should handle LOSTCARD_LOGOUT_RESET_STATE', () => {
    const action = {
      type: 'LOSTCARD_LOGOUT_RESET_STATE',
    };

    const expectedState = {
      data: [],
      loading: false,
      error: false,
      card: {},
      terms: null,
    };

    expect(reducer(INITIAL_STATE, action)).toEqual(expectedState);
  });
});

describe('Unit test for Lost Card Saga', () => {
  it('getReportLostCard should work', () => {
    return expectSaga(getReportLostCard)
      .withReducer(reducer)
      .hasFinalState({
        ...INITIAL_LC_STATE,
        error: true,
        data: undefined,
      })
      .run();
  });

  it('getReportLostCardDetails should work', () => {
    return expectSaga(getReportLostCardDetails)
      .withReducer(reducer)
      .hasFinalState({
        ...INITIAL_LC_STATE,
        error: true,
        data: undefined,
      })
      .run();
  });

  it('reportLostCard should work', () => {
    return expectSaga(reportLostCard, {payload: {}})
      .withReducer(reducer)
      .hasFinalState({
        ...INITIAL_LC_STATE,
        error: true,
        data: undefined,
      })
      .run();
  });

  it('getLostCardTerms should work', () => {
    return expectSaga(getLostCardTerms, {payload: {}})
      .withReducer(reducer)
      .hasFinalState({
        ...INITIAL_LC_STATE,
        error: true,
        data: undefined,
      })
      .run();
  });

  it('watchGetReportLostCard should work', () => {
    return expectSaga(watchGetReportLostCard).withReducer(reducer).run();
  });
});
