/**
 * @flow
 */
import type {LostCardActions, LostCardState} from './types';

const INITIAL_STATE: LostCardState = {
  data: [],
  loading: false,
  error: false,
  card: {},
  terms: null,
};

const lostCardReducer = (
  state: LostCardState = INITIAL_STATE,
  action: LostCardActions,
): LostCardState => {
  switch (action.type) {
    case 'GET_CARD_BLOCKED_REPORTS_REQUESTED':
    case 'GET_CARD_BLOCKED_DETAILS_REQUESTED':
    case 'REPORT_LOST_CARD_REQUESTED':
    case 'GET_CARD_BLOCKED_TERMS_REQUESTED':
      return {
        ...state,
        loading: true,
      };
    case 'GET_CARD_BLOCKED_REPORTS_SUCCESS':
      return {
        ...state,
        data: action.payload.data,
        loading: false,
      };
    case 'GET_CARD_BLOCKED_REPORTS_FAILED':
    case 'GET_CARD_BLOCKED_DETAILS_FAILED':
    case 'REPORT_LOST_CARD_FAILED':
    case 'GET_CARD_BLOCKED_TERMS_FAILED':
      return {
        ...state,
        loading: false,
        data: action.payload.data,
        error: true,
      };
    case 'GET_CARD_BLOCKED_DETAILS_SUCCESS':
      return {
        ...state,
        card: {...action.payload.data},
        loading: false,
      };
    case 'REPORT_LOST_CARD_SUCCESS':
      return {
        ...state,
        loading: false,
      };
    case 'GET_CARD_BLOCKED_TERMS_SUCCESS':
      return {
        ...state,
        loading: false,
        terms: {...action.payload.data},
      };
    case 'LOSTCARD_RESET_ERRORS':
      return {
        ...state,
        error: action.payload,
      };
    case 'LOSTCARD_LOGOUT_RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default lostCardReducer;
