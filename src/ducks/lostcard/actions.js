/**
 * @flow
 */

import type {LostCardActions, ListCards} from './types';

export const getLostCardRequested = (): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_REPORTS_REQUESTED',
});

export const getLostCardRequestedSuccess = (
  data: ListCards,
): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_REPORTS_SUCCESS',
  payload: {
    data,
  },
});

export const getLostCardFailed = (data: ListCards): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_REPORTS_FAILED',
  payload: {
    data,
  },
});

export const getLostCardDetailsRequested = (data: any): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_DETAILS_REQUESTED',
  payload: {
    data,
  },
});

export const getLostCardDetailsSuccess = (
  data: ListCards,
): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_DETAILS_SUCCESS',
  payload: {
    data,
  },
});

export const getLostCardDetailsFailed = (data: ListCards): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_DETAILS_FAILED',
  payload: {
    data,
  },
});

export const reportLostCardRequested = (data: any): LostCardActions => ({
  type: 'REPORT_LOST_CARD_REQUESTED',
  payload: {
    data,
  },
});

export const reportLostCardSuccess = (): LostCardActions => ({
  type: 'REPORT_LOST_CARD_SUCCESS',
});

export const reportLostCardFailed = (data: any): LostCardActions => ({
  type: 'REPORT_LOST_CARD_FAILED',
  payload: {
    data,
  },
});

export const getLostCardTermsRequested = (data: any): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_TERMS_REQUESTED',
  payload: {
    data,
  },
});

export const getLostCardTermsSuccess = (data: any): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_TERMS_SUCCESS',
  payload: {
    data,
  },
});

export const getLostCardTermsFailed = (data: any): LostCardActions => ({
  type: 'GET_CARD_BLOCKED_TERMS_FAILED',
  payload: {
    data,
  },
});

export const reportLostCardResetErrors = (data: boolean): LostCardActions => ({
  type: 'LOSTCARD_RESET_ERRORS',
  payload: data,
});
