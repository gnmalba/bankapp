/**
 * @flow
 */

export type Card = {
  cardNo: string,
  cardSerNo: string,
  embossingName: string,
  alias: string,
};

export type ListCards = Array<Card>;

export type ApiResponse = ListCards;

export type LostCardState = {
  data: ListCards,
  loading: boolean,
  error: boolean,
  card: Object,
};

export type LostCardActions =
  | {
      type: 'GET_CARD_BLOCKED_REPORTS_REQUESTED',
    }
  | {
      type: 'GET_CARD_BLOCKED_REPORTS_SUCCESS',
      +payload: {data: ListCards},
    }
  | {
      type: 'GET_CARD_BLOCKED_REPORTS_FAILED',
      +payload: {data: any},
    }
  | {
      type: 'GET_CARD_BLOCKED_DETAILS_REQUESTED',
      +payload: {data: any},
    }
  | {
      type: 'GET_CARD_BLOCKED_DETAILS_SUCCESS',
      +payload: {data: any},
    }
  | {
      type: 'GET_CARD_BLOCKED_DETAILS_FAILED',
      +payload: {data: any},
    }
  | {
      type: 'REPORT_LOST_CARD_REQUESTED',
      +payload: {data: any},
    }
  | {
      type: 'REPORT_LOST_CARD_SUCCESS',
    }
  | {
      type: 'REPORT_LOST_CARD_FAILED',
      +payload: {data: any},
    }
  | {
      type: 'GET_CARD_BLOCKED_TERMS_REQUESTED',
      +payload: {data: any},
    }
  | {
      type: 'GET_CARD_BLOCKED_TERMS_SUCCESS',
      +payload: {data: any},
    }
  | {
      type: 'GET_CARD_BLOCKED_TERMS_FAILED',
      +payload: {data: any},
    }
  | {
      type: 'LOSTCARD_RESET_ERRORS',
      +payload: boolean,
    }
  | {type: 'LOSTCARD_LOGOUT_RESET_STATE'};
