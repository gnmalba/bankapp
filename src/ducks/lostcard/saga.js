/**
 * @flow
 */

import {call, put, select, takeLatest} from 'redux-saga/effects';

import {
  getLostCardRequestedSuccess,
  getLostCardFailed,
  getLostCardDetailsSuccess,
  getLostCardDetailsFailed,
  reportLostCardSuccess,
  reportLostCardFailed,
  getLostCardTermsSuccess,
  getLostCardTermsFailed,
} from './actions';

import {refreshCardsForActivation} from '@ducks/card-activation/actions';

import reportLostCardService from '@api/services/reportLostCardService';
import {getCustomerID, getEncryptionPublicKey} from '@ducks/commonSelectors';
import encrypt from '@utils/encryptString';

export function* getReportLostCard(action: any): any {
  try {
    const customerId = yield select(getCustomerID);
    const {data} = yield call(reportLostCardService.get, customerId);
    yield put(getLostCardRequestedSuccess(data.data));
  } catch (error) {
    yield put(getLostCardFailed(error?.response?.data));
  }
}

export function* getReportLostCardDetails(action: any): any {
  try {
    const customerId = yield select(getCustomerID);
    const {data} = yield call(reportLostCardService.getCardDetails, {
      customerId,
      ...action.payload.data,
    });
    yield put(
      getLostCardDetailsSuccess({...data.data, ...action.payload.data}),
    );
  } catch (error) {
    yield put(getLostCardDetailsFailed(error?.response?.data));
  }
}

export function* reportLostCard({payload}: any): any {
  try {
    const encryptionPublicKey = yield select(getEncryptionPublicKey);
    const customerId = yield select(getCustomerID);
    const params = payload.data;
    yield call(reportLostCardService.reportCard, {
      customerId,
      ...params,
      passcode: encrypt(encryptionPublicKey).encrypt(params.passcode),
    });
    yield put(reportLostCardSuccess());
    yield put(refreshCardsForActivation());
    yield put({type: 'ENROLLMENT_CREDIT_SUCCESS'});
    params.navigation.navigate('ReportLostCardSuccess');
  } catch (error) {
    yield put(reportLostCardFailed(error?.response?.data));
  }
}

export function* getLostCardTerms({payload}: any): any {
  try {
    const customerId = yield select(getCustomerID);
    const {data} = yield call(reportLostCardService.getTermsAndConditions, {
      customerId,
      ...payload.data,
    });
    yield put(getLostCardTermsSuccess(data.data));
  } catch (error) {
    yield put(getLostCardTermsFailed(error?.response?.data));
  }
}

export function* watchGetReportLostCard(): Generator<any, any, any> {
  yield takeLatest('GET_CARD_BLOCKED_REPORTS_REQUESTED', getReportLostCard);
  yield takeLatest(
    'GET_CARD_BLOCKED_DETAILS_REQUESTED',
    getReportLostCardDetails,
  );
  yield takeLatest('REPORT_LOST_CARD_REQUESTED', reportLostCard);
  yield takeLatest('GET_CARD_BLOCKED_TERMS_REQUESTED', getLostCardTerms);
}

export default watchGetReportLostCard;
