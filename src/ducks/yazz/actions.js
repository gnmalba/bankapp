/**
 * @flow
 */

import type {YazzAccountAction, YazzCards, YazzAccountDetails} from './types';

export const getYazzAccountsRequested = (): YazzAccountAction => ({
  type: 'GET_YAZZ_CREDIT_CARDS_API_REQUESTED',
});

export const getYazzAccountsSuccess = (data: YazzCards): YazzAccountAction => ({
  type: 'GET_YAZZ_CREDIT_CARDS_API_SUCCESS',
  payload: {
    data,
  },
});

export const getYazzAccountsFailure = (): YazzAccountAction => ({
  type: 'GET_YAZZ_CREDIT_CARDS_API_FAILURE',
});

export const getYazzAccountsDetailsSuccess = (
  data: Array<YazzAccountDetails>,
): YazzAccountAction => ({
  type: 'GET_YAZZ_CARDS_DETAILS_SUCCESS',
  payload: {
    data,
  },
});

export const updateYazzList = (data: YazzCards): YazzAccountAction => ({
  type: 'UPDATE_YAZZ_LIST',
  payload: data,
});

export const changeAliasYazz = (
  data: Array<YazzAccountDetails>,
): YazzAccountAction => ({
  type: '@yazz/CHANGE_ALIAS',
  payload: data,
});
