/**
 * @flow
 */

import type {YazzAccountState, YazzAccountAction} from './types';

export const INITIAL_STATE: YazzAccountState = {
  loading: false,
  data: [],
  cardDetails: [],
  transaction: [],
  unFilteredData: [],
};

const yazz = (
  state: YazzAccountState = INITIAL_STATE,
  action: YazzAccountAction,
): YazzAccountState => {
  switch (action.type) {
    case 'GET_YAZZ_CREDIT_CARDS_API_REQUESTED':
    case 'GET_YAZZ_CARD_DETAILS_REQUESTED':
    case 'GET_YAZZ_TRANSACTION_HISTORY_REQUESTED':
      return {
        ...state,
        loading: true,
      };
    case 'GET_YAZZ_CREDIT_CARDS_API_SUCCESS':
      return {
        ...state,
        data: action.payload.data,
        loading: false,
      };
    case 'GET_YAZZ_CREDIT_CARDS_API_FAILURE':
    case 'GET_YAZZ_CARDS_DETAILS_FAILED':
    case 'GET_YAZZ_TRANSACTION_HISTORY_FAILED':
      return {
        ...state,
        loading: false,
      };
    case 'GET_YAZZ_CARDS_DETAILS_SUCCESS':
      return {
        ...state,
        loading: false,
        cardDetails: action.payload.data,
      };
    case 'GET_YAZZ_TRANSACTION_HISTORY_SUCCESS':
      return {
        ...state,
        loading: false,
        transaction: action.payload.transactionHistory,
        unFilteredData: action.payload.transactionHistory,
      };
    case 'FILTER_AMOUNT_DATE_YAZZ_SUCCESS':
      return {
        ...state,
        loading: false,
        transaction: action.payload,
      };

    case '@yazz/CHANGE_ALIAS':
      return {
        ...state,
        cardDetails: action.payload,
      };

    case 'UPDATE_YAZZ_LIST':
      return {
        ...state,
        data: action.payload,
      };

    case 'YAZZ_LOGOUT_RESET_STATE':
      return INITIAL_STATE;
    default:
      return state;
  }
};

export default yazz;
