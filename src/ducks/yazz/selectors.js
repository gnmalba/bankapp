/**
 * @flow
 */

import type {State} from '@ducks/types';
import type {YazzCards} from './types';

export const yazzSelector = (state: State): YazzCards => state.yazz.data;
export const yazzLoadingSelector = (state: State): boolean =>
  state.yazz.loading;
