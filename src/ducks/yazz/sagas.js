/**
 * @flow
 */

import {call, put, select, takeLatest, cancel, spawn} from 'redux-saga/effects';

import api from '@api/services/depositService';
import creditApi from '@api/services/creditService';

import type {
  YazzCards,
  YazzAccountDetails,
  ModifiedTHistory,
  CreditAccounts,
} from './types';

import {getCustomerID} from '@ducks/commonSelectors';
import {reconstructTransactionData} from '@ducks/credit/utils';

import {
  getYazzAccountsFailure,
  getYazzAccountsRequested,
  getYazzAccountsDetailsSuccess,
  getYazzAccountsSuccess,
} from './actions';

import {
  addPesoAccountByType,
  addForeignAccountByType,
  validateSelectedCurrencyByAccountType,
} from '@ducks/accounts-displayoptions/actions';

import moment from 'moment';

export function* addPesoAndForeignAccount(
  yazz: CreditAccounts,
): Generator<any, any, any> {
  if (yazz.length === 0) {
    // if we don't have credit accounts, terminate this generator
    yield cancel();
  }

  const pesoAccount: string = 'PHP';

  for (let i = 0; i < yazz.length; i++) {
    let account = yazz[i];

    if (account.currency === pesoAccount) {
      yield put(addPesoAccountByType(account.cardNo, 'prepaid'));
    } else {
      yield put(addForeignAccountByType(account.cardNo, 'prepaid'));
    }
  }

  yield put(validateSelectedCurrencyByAccountType('prepaid'));
}

export function* fetchYazzFromApi(): Generator<any, any, any> {
  const customerId = yield select(getCustomerID);

  yield put(getYazzAccountsRequested());

  try {
    const response = yield call(api.getYazz, customerId);
    const data: YazzCards = response.data.data;

    yield spawn(addPesoAndForeignAccount, data);
    yield put(getYazzAccountsSuccess(data));
  } catch (apiErrorResponse) {
    yield put(getYazzAccountsFailure());
  } finally {
    yield put({type: 'YAZZ_API_RESOLVED'});
  }
}

export function* fetchYazzCardDetails({
  payload,
}: any): Generator<any, void, any> {
  const customerId = yield select(getCustomerID);
  try {
    const response = yield call(
      creditApi.getCreditCardDetails,
      customerId,
      payload,
    );

    const responseObject: Array<YazzAccountDetails> = [response.data.data];

    yield put(getYazzAccountsDetailsSuccess(responseObject));
  } catch (error) {
    yield put({
      type: 'GET_YAZZ_CARDS_DETAILS_FAILED',
    });
  }
}

export function* fetchYazzTransactionHistory(
  action: any,
): Generator<any, void, any> {
  const customerId = yield select(getCustomerID);
  const url = new URLSearchParams({...action.payload, customerId});

  try {
    const response = yield call(creditApi.getCreditTransactionLast, url);
    const reconstructItem: ModifiedTHistory = reconstructTransactionData(
      response,
      false,
    );

    yield put({
      type: 'GET_YAZZ_TRANSACTION_HISTORY_SUCCESS',
      payload: {transactionHistory: reconstructItem},
    });
  } catch (error) {
    yield put({
      type: 'GET_YAZZ_TRANSACTION_HISTORY_FAILED',
    });
  }
}

export function* fnfiltering(transaction: Object, payload: Object): any {
  const {amountFrom, amountTo, dateFrom, dateTo} = payload;
  let filtered = [];

  for (const key in payload) {
    /* filter amount here */
    if (
      (key === 'amountFrom' || key === 'amountTo') &&
      payload[key] !== null &&
      payload[key].length > 0
    ) {
      filtered = transaction.filter((item) => {
        const parsedFrom = parseFloat(amountFrom.replace(/,/g, ''));
        const parsedTo = parseFloat(amountTo.replace(/,/g, ''));
        return (
          Math.abs(item.amount.value) >= parsedFrom &&
          Math.abs(item.amount.value) <= parsedTo
        );
      });
    }

    /* filter dates here */
    if (
      (key === 'dateFrom' || key === 'dateTo') &&
      payload[key] !== null &&
      payload[key].length > 0
    ) {
      filtered = transaction.filter((item) => {
        const transactionDate = moment(item.transactionDate);
        const momentFrom = moment(dateFrom);
        const momentTo = moment(dateTo);

        return moment(transactionDate).isBetween(
          momentFrom,
          momentTo,
          undefined,
          '[]',
        );
      });
    }
  }

  return filtered;
}

export function* fnFiltersYazz({payload}: any): Generator<any, any, any> {
  const {amountFrom, amountTo, dateFrom, dateTo} = payload;
  const {transaction, unFilteredData} = yield select((state) => state.yazz);

  if (
    amountFrom === null &&
    amountTo === null &&
    dateFrom === null &&
    dateTo === null
  ) {
    yield put({
      type: 'FILTER_AMOUNT_DATE_YAZZ_SUCCESS',
      payload: unFilteredData,
    });
  } else {
    let filtered = yield call(fnfiltering, transaction, payload);

    yield put({type: 'FILTER_AMOUNT_DATE_YAZZ_SUCCESS', payload: filtered});
  }
}

function* watcher(): Generator<void, void, void> {
  yield takeLatest('GET_CUSTOMER_INFO_SUCCESS', fetchYazzFromApi);
  yield takeLatest('ENROLLMENT_YAZZ_SUCCESS', fetchYazzFromApi);
  yield takeLatest('GET_YAZZ_CARD_DETAILS_REQUESTED', fetchYazzCardDetails);
  yield takeLatest('REQUEST_FILTER_AMOUNT_DATE_YAZZ', fnFiltersYazz);
  yield takeLatest(
    'GET_YAZZ_TRANSACTION_HISTORY_REQUESTED',
    fetchYazzTransactionHistory,
  );
}

export default watcher;
