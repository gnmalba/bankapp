/**
 * @flow
 */
import type {ShortCuts} from '@ducks/types';

/**
 * Types for api response
 */

type CreditCardType = 'prepaidcard' | 'creditcard';
type CreditCardFlag = 'NOT_ALLOWED' | ?number;
export type CreditAccount = {
  cardNo: string,
  cardSerNo: ?string,
  cardEmbossedName: ?string,
  customerId: ?string,
  primary: ?number,
  flag: CreditCardFlag,
  outstandingBalance: number,
  availableCredit: number,
  creditLimit: number,
  alias: string,
  currency: string,
  type: CreditCardType,
  shortcuts: ShortCuts,
};

export type YazzAccountDetails = {
  cardType: string,
  alias?: string,
  cardno: string,
  cardSerNo: ?string,
  embossingName: ?string,
  outstandingBalance: number,
  availableBalance: number,
  creditLimit: number,
  currency: string,
  primaryCardFlag: number,
  productName: string,
  productShortCode: string,
  activated: string,
  balanceAsOf: string,
  cardStatus: string,
  cardExpiryDate: number,
  supplementaryCards: Array<?String>,
  activationFlag: number,
  shortcuts: ShortCuts,
};

export type YazzTransactionHistoryDetails = {
  cardSerialNo: string,
  description: string,
  postingDate: string,
  transactionDate: string,
  transactionType: string,
  transactionCurrency: string,
  transactionAmount: number,
  billCurrency: string,
  billAmount: number,
  origMsgType: string,
  msgType: string,
  reasonCode: string,
  procCode: string,
  posted: string,
};

export type YazzTransactionHistory = {
  cardEmbossingName: string,
  cardNo: string,
  details: Array<YazzTransactionHistoryDetails>,
};

/**
 * End types for api response
 */

/**
 * Types for redux
 */

export type CreditAccounts = Array<CreditAccount>;

export type YazzCard = CreditAccount;
export type YazzCards = Array<YazzCard>;

type THistoryTitle = {
  title: boolean,
  cardEmbossingName: string,
  cardNo: string,
};

type THistoryDetails = {
  amount: {
    currencyCode: string,
    value: number,
  },
  description: {
    line1: string,
  },
  transactionDate: string,
  posted: boolean,
  transactionType: string,
  transactionAmount: number,
};

export type ModifiedTHistory = Array<THistoryTitle | THistoryDetails>;
export type YazzAccountState = {
  loading: boolean,
  data: YazzCards,
  cardDetails: Array<YazzAccountDetails>,
  transaction: ModifiedTHistory,
};

export type YazzAccountAction =
  | {
      type: 'GET_YAZZ_CREDIT_CARDS_API_REQUESTED',
    }
  | {
      type: 'GET_YAZZ_CREDIT_CARDS_API_SUCCESS',
      +payload: {
        data: YazzCards,
      },
    }
  | {
      type: 'GET_YAZZ_CREDIT_CARDS_API_FAILURE',
    }
  | {
      type: 'GET_YAZZ_CARD_DETAILS_REQUESTED',
    }
  | {
      type: 'GET_YAZZ_CARDS_DETAILS_SUCCESS',
      payload: {
        data: Array<YazzAccountDetails>,
      },
    }
  | {
      type: 'GET_YAZZ_CARDS_DETAILS_FAILED',
    }
  | {
      type: 'GET_YAZZ_TRANSACTION_HISTORY_REQUESTED',
    }
  | {
      type: 'GET_YAZZ_TRANSACTION_HISTORY_SUCCESS',
      payload: {
        transactionHistory: ModifiedTHistory,
      },
    }
  | {
      type: 'FILTER_AMOUNT_DATE_YAZZ_SUCCESS',
      payload: any,
    }
  | {
      type: 'GET_YAZZ_TRANSACTION_HISTORY_FAILED',
    }
  | {
      type: 'YAZZ_LOGOUT_RESET_STATE',
    }
  | {
      type: '@yazz/CHANGE_ALIAS',
      payload: Array<YazzAccountDetails>,
    }
  | {
      type: 'UPDATE_YAZZ_LIST',
      payload: YazzCards,
    };

/**
 * End types for redux
 */
