import {yazzSelector, yazzLoadingSelector} from '../selectors';

describe('Yazz selectors', () => {
  it('yazzSelector returns expected state', () => {
    const mockState = {
      yazz: {
        data: 'mock',
      },
    };

    expect(yazzSelector(mockState)).toEqual('mock');
  });

  it('yazzLoadingSelector returns expected state', () => {
    const mockState = {
      yazz: {
        loading: false,
      },
    };

    expect(yazzLoadingSelector(mockState)).toEqual(false);
  });
});
