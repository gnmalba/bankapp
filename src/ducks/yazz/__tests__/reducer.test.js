import reducer, {INITIAL_STATE} from '../reducer';

describe('Yazz reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE);
  });

  const payload = 'mockPayload';

  it('should handle GET_YAZZ_CREDIT_CARDS_API_REQUESTED', () => {
    const action = {
      type: 'GET_YAZZ_CREDIT_CARDS_API_REQUESTED',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  it('should handle GET_YAZZ_CREDIT_CARDS_API_SUCCESS', () => {
    const action = {
      type: 'GET_YAZZ_CREDIT_CARDS_API_SUCCESS',
      payload: {
        data: payload,
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      data: action.payload.data,
      loading: false,
    });
  });

  it('should handle GET_YAZZ_CREDIT_CARDS_API_FAILURE', () => {
    const action = {
      type: 'GET_YAZZ_CREDIT_CARDS_API_FAILURE',
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      loading: false,
    });
  });

  it('should handle GET_YAZZ_CARDS_DETAILS_SUCCESS', () => {
    const action = {
      type: 'GET_YAZZ_CARDS_DETAILS_SUCCESS',
      payload: {
        data: payload,
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      loading: false,
      cardDetails: action.payload.data,
    });
  });

  it('should handle GET_YAZZ_TRANSACTION_HISTORY_SUCCESS', () => {
    const action = {
      type: 'GET_YAZZ_TRANSACTION_HISTORY_SUCCESS',
      payload: {
        transactionHistory: payload,
      },
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      loading: false,
      transaction: action.payload.transactionHistory,
      unFilteredData: action.payload.transactionHistory,
    });
  });

  it('should handle FILTER_AMOUNT_DATE_YAZZ_SUCCESS', () => {
    const action = {
      type: 'FILTER_AMOUNT_DATE_YAZZ_SUCCESS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      loading: false,
      transaction: action.payload,
    });
  });

  it('should handle @yazz/CHANGE_ALIAS', () => {
    const action = {
      type: '@yazz/CHANGE_ALIAS',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      cardDetails: action.payload,
    });
  });

  it('should handle UPDATE_YAZZ_LIST', () => {
    const action = {
      type: 'UPDATE_YAZZ_LIST',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
      data: action.payload,
    });
  });

  it('should handle YAZZ_LOGOUT_RESET_STATE', () => {
    const action = {
      type: 'YAZZ_LOGOUT_RESET_STATE',
      payload,
    };
    expect(reducer(INITIAL_STATE, action)).toEqual({
      ...INITIAL_STATE,
    });
  });
});
