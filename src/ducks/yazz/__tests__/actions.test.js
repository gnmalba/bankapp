import * as actions from '../actions';

describe('Actions should return expected objects', () => {
  const payload = 'mockPayload';

  it('getYazzAccountsRequested returns the correct action', () => {
    const action = actions.getYazzAccountsRequested();
    const expectedAction = {
      type: 'GET_YAZZ_CREDIT_CARDS_API_REQUESTED',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getYazzAccountsSuccess returns the correct action', () => {
    const action = actions.getYazzAccountsSuccess(payload);
    const expectedAction = {
      type: 'GET_YAZZ_CREDIT_CARDS_API_SUCCESS',
      payload: {
        data: payload,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('getYazzAccountsFailure returns the correct action', () => {
    const action = actions.getYazzAccountsFailure();
    const expectedAction = {
      type: 'GET_YAZZ_CREDIT_CARDS_API_FAILURE',
    };
    expect(action).toEqual(expectedAction);
  });

  it('getYazzAccountsDetailsSuccess returns the correct action', () => {
    const action = actions.getYazzAccountsDetailsSuccess(payload);
    const expectedAction = {
      type: 'GET_YAZZ_CARDS_DETAILS_SUCCESS',
      payload: {
        data: payload,
      },
    };
    expect(action).toEqual(expectedAction);
  });

  it('updateYazzList returns the correct action', () => {
    const action = actions.updateYazzList(payload);
    const expectedAction = {
      type: 'UPDATE_YAZZ_LIST',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });

  it('changeAliasYazz returns the correct action', () => {
    const action = actions.changeAliasYazz(payload);
    const expectedAction = {
      type: '@yazz/CHANGE_ALIAS',
      payload,
    };
    expect(action).toEqual(expectedAction);
  });
});
