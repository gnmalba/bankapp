import {select} from 'redux-saga/effects';
import {expectSaga} from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import {getCustomerID} from '@ducks/commonSelectors';

import {
  addPesoAndForeignAccount,
  fetchYazzFromApi,
  fetchYazzCardDetails,
} from '../sagas';

import {
  addPesoAccountByType,
  getYazzAccountsRequested,
  getYazzAccountsFailure,
} from '../actions';

describe('Yazz saga tests', () => {
  // STARTS - addPesoAndForeignAccount
  describe('addPesoAndForeignAccount should run the expected effects', () => {
    it(' when value is PHP', async () => {
      return expectSaga(addPesoAndForeignAccount, [
        {
          currency: 'PHP',
          cardNo: '123123123',
        },
        {
          currency: 'PHP',
          cardNo: '123123123',
        },
      ])
        .provide([
          [matchers.call.fn(addPesoAccountByType), ('123123123', 'prepaid')],
        ])
        .run();
    });

    it('when no value', async () => {
      return expectSaga(addPesoAndForeignAccount, [])
        .provide([
          [matchers.call.fn(addPesoAccountByType), ('123123123', 'prepaid')],
        ])
        .run();
    });

    it('when value is USD', async () => {
      return expectSaga(addPesoAndForeignAccount, [
        {
          currency: 'USD',
          cardNo: '123123123',
        },
        {
          currency: 'USD',
          cardNo: '123123123',
        },
      ])
        .provide([
          [matchers.call.fn(addPesoAccountByType), ('123123123', 'prepaid')],
        ])
        .run();
    });
  });
  // END - addPesoAndForeignAccount

  // START - fetchYazzFromApi
  describe('fetchYazzFromApi effects should run expectedly', () => {
    const customerId = '123123';

    it('when negative test ', () => {
      jest.mock('@api/services/depositService', () => ({
        getYazz: () => ({
          data: {
            data: ['test'],
          },
        }),
      }));
      return expectSaga(fetchYazzFromApi)
        .provide([[select(getCustomerID), customerId]])
        .put(getYazzAccountsRequested())
        .put(getYazzAccountsFailure())
        .run();
    });
  });
  // END - fetchYazzFromApi

  // START - fetchYazzCardDetails
  describe('fetchYazzCardDetails effects should run expectedly', () => {
    const customerId = '123123';
    const payload = {};

    it('when negative test ', () => {
      return expectSaga(fetchYazzCardDetails, payload)
        .provide([[select(getCustomerID), customerId]])
        .put({
          type: 'GET_YAZZ_CARDS_DETAILS_FAILED',
        })
        .run();
    });
  });
  // END - fetchYazzCardDetails
});
