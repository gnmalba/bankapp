// @flow

import axios from 'axios';
import {JSEncrypt} from 'jsencrypt';
import {Platform} from 'react-native';
import {RSA} from 'react-native-rsa-native';
import SInfo from 'react-native-sensitive-info';
import ReactNativeBiometrics from 'react-native-biometrics';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {BASE_URL} from 'react-native-dotenv';

export const ANDROID_BEGINRSAPUBLICKEY = '-----BEGINRSAPUBLICKEY-----';
export const ANDROID_ENDRSAPUBLICKEY = '-----ENDRSAPUBLICKEY-----';
export const IOS_BEGINRSAPUBLICKEY = '-----BEGINPUBLICKEY-----';
export const IOS_ENDRSAPUBLICKEY = '-----ENDPUBLICKEY-----';
export const RSA_SHORT_PAD = 'MIIBCgKCAQEA';
export const RSA_LONG_PAD = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA';

export const sinfoOptions = {
  sharedPreferencesName: 'metroBankSharedPrefs',
  keychainService: 'metroBankKeychain',
};

import {LOGIN_API} from '../apiConstants';
import {headers} from '@utils/constants';

export const fnGenerateKeyPair = async (options: Object): Promise<any> => {
  try {
    const keyPair = await RSA.generateKeys(2048);
    let publicKey;
    if (Platform.OS === 'android') {
      publicKey = keyPair.public
        .replace(/\s/g, '')
        .replace(ANDROID_BEGINRSAPUBLICKEY, '')
        .replace(ANDROID_ENDRSAPUBLICKEY, '')
        .replace(RSA_SHORT_PAD, RSA_LONG_PAD);
    } else {
      publicKey = keyPair.public
        .replace(/\s/g, '')
        .replace(IOS_BEGINRSAPUBLICKEY, '')
        .replace(IOS_ENDRSAPUBLICKEY, '');
    }
    return {publicKey, privateKey: keyPair.private};
  } catch (error) {
    return error;
  }
};

export const fnBioRegistration = async (
  options: Object,
  publicKey: string,
): Promise<any> => {
  try {
    const {
      customerId,
      username,
      passcode,
      encryptionPublicKey,
      accessToken,
    } = options;
    const encrypt = new JSEncrypt();
    encrypt.setPublicKey(encryptionPublicKey);

    const data = JSON.stringify({
      customerId: customerId,
      username: username,
      publicKey: publicKey,
      mpin: encrypt.encrypt(passcode),
    });

    var config = {
      method: 'post',
      url: `${BASE_URL}${LOGIN_API.BIOMETRIC_REGISTRATION}`,
      headers: {
        ...headers,
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      data: data,
    };
    return axios(config);
  } catch (error) {
    return error;
  }
};

export const fnUnregisterBio = async (options: Object): Promise<any> => {
  try {
    const {
      customerId,
      username,
      passcode,
      accessToken,
      encryptionPublicKey,
    } = options;
    const encrypt = new JSEncrypt();
    encrypt.setPublicKey(encryptionPublicKey);

    const params = JSON.stringify({
      customerId: customerId,
      username: username,
      mpin: encrypt.encrypt(passcode),
    });

    var config = {
      method: 'post',
      url: `${BASE_URL}${LOGIN_API.BIOMETRIC_UNREGISTRATION}`,
      headers: {
        ...headers,
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
      },
      data: params,
    };

    await SInfo.deleteItem('BIOMETRIC_PRIVIATE_KEY', sinfoOptions);
    await SInfo.deleteItem('BIOMETRIC_TOKEN', sinfoOptions);
    await SInfo.deleteItem('BIOMETRIC_USEERNAME', sinfoOptions);
    await AsyncStorage.setItem('IS_BIOMETRICS_ENROLLED', 'false');

    return axios(config);
  } catch (e) {
    return e;
  }
};

export const checkBiometricSensor = async (): Promise<any> => {
  const biometricSensor = await ReactNativeBiometrics.isSensorAvailable();
  const {available, biometryType, error} = biometricSensor;
  let bioType = '';
  if (available) {
    switch (biometryType) {
      case ReactNativeBiometrics.TouchID:
        bioType = 'Touch ID';
        break;
      case ReactNativeBiometrics.FaceID:
        bioType = 'Face ID';
        break;
      case ReactNativeBiometrics.Biometrics:
        bioType = 'Fingerprint';
        break;
      default:
        break;
    }
  }
  const isBiometricsEnrolled = JSON.parse(
    await AsyncStorage.getItem('IS_BIOMETRICS_ENROLLED'),
  );
  return {bioType, error, isBiometricsEnrolled: isBiometricsEnrolled === true};
};

export const fnStoreBio = async (options: Object): Promise<any> => {
  try {
    const {privateKey, token, username} = options;
    await SInfo.setItem('BIOMETRIC_PRIVIATE_KEY', privateKey, sinfoOptions);
    await SInfo.setItem('BIOMETRIC_TOKEN', token, sinfoOptions);
    await SInfo.setItem('BIOMETRIC_USEERNAME', username, sinfoOptions);
    await AsyncStorage.setItem('IS_BIOMETRICS_ENROLLED', 'true');
    return true;
  } catch (error) {
    return error;
  }
};

export const fnSimplePrompt = async (): any => {
  try {
    return ReactNativeBiometrics.simplePrompt({
      promptMessage: 'Confirm fingerprint',
    });
  } catch (error) {
    return error;
  }
};

export const fnGetBioInfo = async (): any => {
  try {
    const privateKey = await SInfo.getItem(
      'BIOMETRIC_PRIVIATE_KEY',
      sinfoOptions,
    );
    const token = await SInfo.getItem('BIOMETRIC_TOKEN', sinfoOptions);
    const username = await SInfo.getItem('BIOMETRIC_USEERNAME', sinfoOptions);
    const decryptedToken = await RSA.decrypt(token, privateKey);
    const signToken = await RSA.signWithAlgorithm(
      decryptedToken,
      privateKey,
      'SHA512withRSA',
    );
    const signedToken = signToken.replace(/\s/g, '');
    return {signedToken, username};
  } catch (error) {
    return error;
  }
};

export const fnGetLinkDisabledStatus = async (): any => {
  try {
    return AsyncStorage.getItem('biometricsLinkDisabled');
  } catch (error) {
    return error;
  }
};
