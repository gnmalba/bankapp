// @flow

import qs from 'qs';
import axios from 'axios';
import {JSEncrypt} from 'jsencrypt';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getModel, getSystemVersion} from 'react-native-device-info';
import {LOGIN_API} from '../apiConstants';
import {
  BASE_URL,
  KEYCLOAK_RESOURCE,
  KEYCLOAK_CREDENTIAL_SECRET,
} from 'react-native-dotenv';
const encrypt = new JSEncrypt();
import {headers} from '@utils/constants';

export const fnGetPublicKey = async (): Promise<any> => {
  try {
    var config = {
      method: 'get',
      url: `${BASE_URL}${LOGIN_API.AUTHENTICATION_PUBLIC_KEY}`,
    };
    return axios(config);
  } catch (error) {
    return error;
  }
};

export const fnGetLogin = async (options: Object): Promise<any> => {
  try {
    const {
      username,
      password,
      encryptionPublicKey,
      totp = '',
      mfaToken = '',
      signedToken = '',
      refreshToken = '',
      rememberMe,
    } = options;
    encrypt.setPublicKey(encryptionPublicKey);
    const addPassToken =
      signedToken !== ''
        ? {signed_token: signedToken}
        : {password: encrypt.encrypt(password)};
    const grantType = refreshToken === '' ? 'password' : 'refresh_token';
    const data = !rememberMe
      ? qs.stringify({
          username,
          grant_type: grantType,
          client_id: KEYCLOAK_RESOURCE,
          client_secret: KEYCLOAK_CREDENTIAL_SECRET,
          refresh_token: refreshToken,
          totp,
          mfa_token: mfaToken,
          ...addPassToken,
        })
      : qs.stringify({
          username,
          grant_type: grantType,
          client_id: KEYCLOAK_RESOURCE,
          client_secret: KEYCLOAK_CREDENTIAL_SECRET,
          refresh_token: refreshToken,
          totp,
          mfa_token: mfaToken,
          remember_me: rememberMe,
          ...addPassToken,
        });
    var config = {
      method: 'post',
      url: `${BASE_URL}${LOGIN_API.TOKEN}`,
      headers: {
        ...headers,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache',
      },
      data: data,
    };
    return axios(config);
  } catch (error) {
    return error;
  }
};

export const fnLogout = async (options: Object): Promise<any> => {
  try {
    const {refreshToken} = options;
    const data = qs.stringify({
      client_id: KEYCLOAK_RESOURCE,
      client_secret: KEYCLOAK_CREDENTIAL_SECRET,
      refresh_token: refreshToken,
    });
    var config = {
      method: 'post',
      url: `${BASE_URL}${LOGIN_API.LOGOUT}`,
      headers: {
        ...headers,
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache',
      },
      data: data,
    };
    return axios(config);
  } catch (error) {
    return error;
  }
};

export const fnGetCustomerInfo = async (options: Object): Promise<any> => {
  try {
    const data = JSON.stringify({
      username: options.username,
      customerDevice: {
        brand: headers['X-OS'],
        model: getModel(),
        version: getSystemVersion(),
        deviceId: headers['X-DEVICE-ID'],
      },
      totp: null,
      generatorId: null,
    });
    const config = {
      method: 'post',
      url: `${BASE_URL}${LOGIN_API.CUSTOMER_INFO}`,
      headers: {
        ...headers,
        'Content-Type': 'application/json',
        Authorization: `Bearer ${options.accessToken}`,
      },
      data,
    };
    return axios(config);
  } catch (error) {
    return error;
  }
};

export const fnGetOnBoarded = async (): Promise<any> => {
  try {
    return AsyncStorage.getItem('onboarded');
  } catch (error) {
    return error;
  }
};
