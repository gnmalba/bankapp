import {fnGenerateKeyPair, fnGetBioInfo} from '@ducks/helpers/biometrics';

jest.useFakeTimers();
jest.mock('react-native-rsa-native', () => {
  return {};
});

describe('Biometrics function android', () => {
  it('fnGenerateKeyPair function should not work for android', async () => {
    await fnGenerateKeyPair();
  });

  it('fnGetBioInfo function should not work', async () => {
    await fnGetBioInfo();
  });
});
