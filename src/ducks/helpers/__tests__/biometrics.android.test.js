import axios from 'axios';
import mockAdapter from 'axios-mock-adapter';
import {LOGIN_API} from '@ducks/apiConstants';
import {BASE_URL} from 'react-native-dotenv';
import {
  fnGenerateKeyPair,
  fnBioRegistration,
  fnUnregisterBio,
  fnStoreBio,
  fnSimplePrompt,
  fnGetBioInfo,
  fnGetLinkDisabledStatus,
} from '@ducks/helpers/biometrics';

const mockAxios = new mockAdapter(axios);
jest.useFakeTimers();
jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  delete RN.Platform;
  RN.Platform = {
    OS: 'android',
  };
  return RN;
});

describe('Biometrics function android', () => {
  it('fnGenerateKeyPair function should work for android', async () => {
    const keys = await fnGenerateKeyPair();
    expect(keys.publicKey).toEqual('RandomPublic');
  });

  it('fnBioRegistration function should work', async () => {
    mockAxios
      .onPost(`${BASE_URL}${LOGIN_API.BIOMETRIC_REGISTRATION}`)
      .reply(200);
    await fnBioRegistration({});
  });
  it('fnBioRegistration function should not work', async () => {
    await fnBioRegistration();
  });

  it('fnUnregisterBio function should work', async () => {
    mockAxios
      .onPost(`${BASE_URL}${LOGIN_API.BIOMETRIC_UNREGISTRATION}`)
      .reply(200);
    await fnUnregisterBio({});
  });
  it('fnBioRegistration function should not not work', async () => {
    await fnUnregisterBio();
  });

  it('fnStoreBio function should work', async () => {
    await fnStoreBio({
      privateKey: 'test',
      token: 'test',
      username: 'test',
    });
  });
  it('fnStoreBio function should not work', async () => {
    await fnStoreBio();
  });

  it('fnSimplePrompt function should work', async () => {
    await fnSimplePrompt({});
  });

  it('fnGetBioInfo function should work', async () => {
    await fnGetBioInfo();
  });

  it('fnGetLinkDisabledStatus function should work', async () => {
    await fnGetLinkDisabledStatus();
  });
});
