import {
  fnGenerateKeyPair,
  checkBiometricSensor,
} from '@ducks/helpers/biometrics';

jest.useFakeTimers();
jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  delete RN.Platform;
  RN.Platform = {
    OS: 'ios',
  };
  return RN;
});

describe('Biometrics function android', () => {
  it('fnGenerateKeyPair function should work for ios', async () => {
    const keys = await fnGenerateKeyPair();
    expect(keys.publicKey).toEqual('RandomPublic');
  });

  it('checkBiometricSensor function should work for ios', async () => {
    await checkBiometricSensor();
  });
});
