import {getUpdatePaymentTermsState, handleUpdateAmount} from '../installments';

describe('installments function', () => {
  test.each(['BT', 'BC', 'C2GO', 'NP'])(
    'getUpdatePaymentTermsState should work properly',
    (type) => {
      getUpdatePaymentTermsState(
        type,
        {},
        {
          payload: {
            paymentTerms: null,
          },
        },
      );
    },
  );

  test.each(['BT', 'BC', 'C2GO', 'NP'])(
    'getUpdatePaymentTermsState should work properly',
    (type) => {
      handleUpdateAmount(
        {
          balanceTransfer: {
            amount: 1,
            paymentTerms: {},
          },
          balanceConversion: {
            amount: 1,
            paymentTerms: {},
          },
          cash2Go: {
            amount: 1,
            paymentTerms: {},
          },
        },
        {
          payload: {
            installmentType: type,
            data: 1,
          },
        },
      );
    },
  );
});
