import axios from 'axios';
import mockAdapter from 'axios-mock-adapter';
import {LOGIN_API} from '@ducks/apiConstants';
import {BASE_URL} from 'react-native-dotenv';
import {
  fnGetPublicKey,
  fnGetLogin,
  fnLogout,
  fnGetCustomerInfo,
  fnGetOnBoarded,
} from '@ducks/helpers/authentication';

const mockAxios = new mockAdapter(axios);

describe('Authentication helper positive test', () => {
  test('fnGetPublicKey function should pass positive testing', async () => {
    mockAxios
      .onGet(`${BASE_URL}${LOGIN_API.AUTHENTICATION_PUBLIC_KEY}`)
      .reply(200);
    const res = await fnGetPublicKey();
    expect(res.status).toEqual(200);
  });

  test('fnGetLogin function should pass positive testing', async () => {
    mockAxios.onPost(`${BASE_URL}${LOGIN_API.TOKEN}`).reply(200);
    const res = await fnGetLogin({
      username: 'test',
      password: 'test',
      encryptionPublicKey: 'testKey',
    });
    expect(res.status).toEqual(200);
  });
  test.each(['', 'test'])(
    'fnGetLogin function should pass positive testing',
    async (args) => {
      mockAxios.onPost(`${BASE_URL}${LOGIN_API.TOKEN}`).reply(200);
      const res = await fnGetLogin({
        username: 'test',
        password: 'test',
        encryptionPublicKey: 'testKey',
        signedToken: args,
        refreshToken: args,
      });
      expect(res.status).toEqual(200);
    },
  );
  test('fnGetLogin function should pass negative testing', async () => {
    await fnGetLogin();
  });

  test('fnLogout function should pass positive testing', async () => {
    mockAxios.onPost(`${BASE_URL}${LOGIN_API.LOGOUT}`).reply(200);
    const res = await fnLogout({
      refreshToken: 'testToken',
    });
    expect(res.status).toEqual(200);
  });
  test('fnLogout function should pass negative testing', async () => {
    await fnLogout();
  });

  test('fnGetCustomerInfo function should pass positive testing', async () => {
    mockAxios.onPost(`${BASE_URL}${LOGIN_API.CUSTOMER_INFO}`).reply(200);
    const res = await fnGetCustomerInfo({
      username: 'test',
      accessToken: 'testtoken',
    });
    expect(res.status).toEqual(200);
  });
  test('fnGetCustomerInfo function should pass negative testing', async () => {
    await fnGetCustomerInfo();
  });

  test('fnGetOnBoarded function should pass positive testing', async () => {
    await fnGetOnBoarded();
  });
});
