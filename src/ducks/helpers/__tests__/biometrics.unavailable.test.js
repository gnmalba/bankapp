import {checkBiometricSensor} from '@ducks/helpers/biometrics';

jest.mock('react-native-biometrics', () => ({
  isSensorAvailable: () => ({
    available: false,
    biometryType: 'Biometrics',
    error: '',
  }),
  TouchID: 'TouchID',
  FaceID: 'FaceID',
  Biometrics: 'Biometrics',
}));

describe('Biometrics function android', () => {
  it('checkBiometricSensor function should work for ios', async () => {
    await checkBiometricSensor();
  });
});
