// @flow

import type {
  InstallmentTypesString,
  InstallmentStateType,
} from '@ducks/installments/types';

export const getUpdatePaymentTermsState = (
  installmentType: InstallmentTypesString,
  state: any,
  action: any, // TODO: handle types for both state and action
): InstallmentStateType => {
  if (installmentType === 'BT') {
    return {
      ...state,
      balanceTransfer: {
        ...state.balanceTransfer,
        paymentTermsList: action.payload.paymentTerms,
      },
      formLoading: false,
    };
  } else if (installmentType === 'BC') {
    return {
      ...state,
      balanceConversion: {
        ...state.balanceConversion,
        paymentTermsList: action.payload.paymentTerms,
      },

      formLoading: false,
    };
  } else if (installmentType === 'C2GO') {
    return {
      ...state,
      cash2Go: {
        ...state.cash2Go,
        paymentTermsList: action.payload.paymentTerms,
      },
      formLoading: false,
    };
  }

  return {...state};
};

export const handleUpdateAmount = (
  state: any,
  action: any,
): InstallmentStateType => {
  const {installmentType, data} = action.payload;

  if (installmentType === 'BT') {
    return {
      ...state,
      balanceTransfer: {
        ...state.balanceTransfer,
        amount: data,
        paymentTerms:
          state.balanceTransfer.amount !== data
            ? null
            : state.balanceTransfer.paymentTerms,
      },
      isFormDirty: true,
    };
  } else if (installmentType === 'BC') {
    return {
      ...state,
      balanceConversion: {
        ...state.balanceConversion,
        amount: data,
        paymentTerms:
          state.balanceConversion.amount !== data
            ? null
            : state.balanceConversion.paymentTerms,
      },
      isFormDirty: true,
    };
  } else if (installmentType === 'C2GO') {
    return {
      ...state,
      cash2Go: {
        ...state.cash2Go,
        amount: data,
        paymentTerms:
          state.cash2Go.amount !== data ? null : state.cash2Go.paymentTerms,
      },
      isFormDirty: true,
    };
  }

  return state;
};
