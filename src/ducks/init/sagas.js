/**
 * @flow
 */

import {put, spawn} from 'redux-saga/effects';

import configProcessor from '@ducks/config/sagas/configProcessor';
import {getOnBoarded} from '@ducks/authentication/actions';

export default function* init(): Generator<any, any, any> {
  yield spawn(configProcessor);
  yield put(getOnBoarded());
}
