/**
 * @flow
 */

import type {ManageSettingsStateType, ManageSettingsAction} from './types';

const INITIAL_STATE: ManageSettingsStateType = {
  loading: false,
  devices: [],
  isUnlinkSuccess: false,
  unlinkError: null,
  isAppearanceSettingSuccess: false,
  mpinError: [],
  passwordError: [],
  passwordPolicy: [],
  error: false,
  temporaryTheme: null,
};

const manageSettings = (
  state: ManageSettingsStateType = INITIAL_STATE,
  action: ManageSettingsAction,
): ManageSettingsStateType => {
  switch (action.type) {
    case 'FETCH_DEVICES':
    case 'VALIDATE_MPIN_REQUEST':
    case 'UPDATE_MPIN_REQUEST':
    case 'CHANGE_PASSWORD_REQUEST':
    case 'GET_PASSWORD_POLICY_REQUEST':
      return {
        ...state,
        loading: true,
      };
    case 'FETCH_DEVICES_SUCCESS':
      return {...state, loading: false, devices: action.payload.deviceList};
    case 'FETCH_DEVICES_ERROR':
      return {...state, loading: false};
    case 'UNLINK_DEVICE':
      return {...state, loading: true, isUnlinkSuccess: false};
    case 'UNLINK_DEVICE_SUCCESS':
      return {...state, isUnlinkSuccess: true, unlinkError: null};
    case 'UNLINK_DEVICE_ERROR':
      return {...state, loading: false, unlinkError: action.payload.errorData};
    case 'SAVE_APPEARANCE_SETTING':
      return {...state, loading: true};
    case 'SAVE_APPEARANCE_SETTING_SUCCESS':
      return {...state, isAppearanceSettingSuccess: true, loading: false};
    case 'VALIDATE_MPIN_SUCCESS':
    case 'UPDATE_MPIN_SUCCESS':
    case 'CHANGE_PASSWORD_SUCCESS':
      return {...state, loading: false};
    case 'GET_PASSWORD_POLICY_SUCCESS':
      return {...state, passwordPolicy: action.payload.data, loading: false};
    case 'VALIDATE_MPIN_FAILED':
    case 'UPDATE_MPIN_FAILED':
      return {
        ...state,
        loading: false,
        mpinError: action.payload.error,
      };
    case 'SET_APPEARANCE_FLAG':
      return {
        ...state,
        isAppearanceSettingSuccess: action.payload.flag,
      };

    case 'SAVE_APPEARANCE_SETTING_ERROR':
      return {
        ...state,
        isAppearanceSettingSuccess: false,
        loading: false,
      };
    case 'CHANGE_PASSWORD_FAILED':
    case 'GET_PASSWORD_POLICY_FAILED':
      return {
        ...state,
        loading: false,
        passwordError: action.payload.error,
      };
    case 'SET_TEMPORARY_THEME':
      return {
        ...state,
        temporaryTheme: action.payload.theme,
      };
    default:
      return state;
  }
};

export default manageSettings;
