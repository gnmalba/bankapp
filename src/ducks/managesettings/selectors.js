// @flow
import type {State} from '@ducks/types';
import type {ManageSettingsStateType} from './types';

export const manageSettingsSelector = (state: State): ManageSettingsStateType =>
  state.manageSettings;
