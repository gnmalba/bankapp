// @flow

import type {THEME} from '@ducks/config/types';

export type DeviceListItemType = {
  brand: string,
  customerId: string,
  dateAdded: string,
  deviceId: string,
  lastUse: string,
  model: string,
  rememberMe: any,
};

export type UnlinkErrorType = {
  code: string,
  message: string,
};

export type ManageSettingsStateType = {|
  loading: boolean,
  devices: DeviceListItemType[],
  isUnlinkSuccess: boolean,
  unlinkError: ?UnlinkErrorType,
  isAppearanceSettingSuccess: boolean,
  mpinError: Array<mixed>,
  passwordError: Array<mixed>,
  passwordPolicy: ListPasswordPolicy,
  error: false,
  temporaryTheme: ?THEME,
|};

export type SaveDeviceSettingsQueryParams = {
  deviceId?: string,
  customerId: string,
  username: string,
};

export type Policy = {
  description: string,
  name: string,
  value: string,
};

export type ListPasswordPolicy = Array<Policy>;

export type ManageSettingsAction =
  | {type: 'FETCH_DEVICES'}
  | {
      type: 'FETCH_DEVICES_SUCCESS',
      payload: {
        deviceList: DeviceListItemType[],
      },
    }
  | {
      type: 'FETCH_DEVICES_ERROR',
    }
  | {
      type: 'UNLINK_DEVICE',
      payload: {
        deviceId: string,
        passcode: string,
      },
    }
  | {
      type: 'UNLINK_DEVICE_SUCCESS',
    }
  | {
      type: 'UNLINK_DEVICE_ERROR',
      payload: {
        errorData: UnlinkErrorType,
      },
    }
  | {
      type: 'SAVE_APPEARANCE_SETTING',
      payload: {
        theme: THEME,
      },
    }
  | {
      type: 'SAVE_APPEARANCE_SETTING_SUCCESS',
    }
  | {
      type: 'SAVE_APPEARANCE_SETTING_ERROR',
    }
  | {
      type: 'VALIDATE_MPIN_REQUEST',
      payload: {
        username: string,
        passcode: string,
      },
    }
  | {
      type: 'VALIDATE_MPIN_SUCCESS',
    }
  | {
      type: 'VALIDATE_MPIN_FAILED',
      payload: {error: any},
    }
  | {
      type: 'UPDATE_MPIN_REQUEST',
      payload: {
        username: string,
        passcode: string,
        oldMpin: string,
      },
    }
  | {
      type: 'UPDATE_MPIN_SUCCESS',
    }
  | {
      type: 'UPDATE_MPIN_FAILED',
      payload: {error: any},
    }
  | {
      type: 'SET_APPEARANCE_FLAG',
      payload: {flag: boolean},
    }
  | {
      type: 'CHANGE_PASSWORD_REQUEST',
      payload: {
        passcode: string,
        oldpassword: string,
        newpassword: string,
      },
    }
  | {
      type: 'CHANGE_PASSWORD_SUCCESS',
    }
  | {
      type: 'CHANGE_PASSWORD_FAILED',
      payload: {error: any},
    }
  | {
      type: 'GET_PASSWORD_POLICY_REQUEST',
    }
  | {
      type: 'GET_PASSWORD_POLICY_SUCCESS',
      payload: {data: ListPasswordPolicy},
    }
  | {
      type: 'GET_PASSWORD_POLICY_FAILED',
      payload: {error: any},
    }
  | {
      type: 'SET_TEMPORARY_THEME',
      payload: {
        theme: ?THEME,
      },
    };
