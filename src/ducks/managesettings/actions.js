// @flow
import type {
  ManageSettingsAction,
  DeviceListItemType,
  UnlinkErrorType,
  ListPasswordPolicy,
} from './types';

import type {THEME} from '@ducks/config/types';

export const fetchDevices = (): ManageSettingsAction => ({
  type: 'FETCH_DEVICES',
});

export const fetchDevicesError = (): ManageSettingsAction => ({
  type: 'FETCH_DEVICES_ERROR',
});

export const fetchDevicesSuccess = (
  deviceList: DeviceListItemType[],
): ManageSettingsAction => ({
  type: 'FETCH_DEVICES_SUCCESS',
  payload: {deviceList},
});

export const unlinkDevice = (
  deviceId: string,
  passcode: string,
): ManageSettingsAction => ({
  type: 'UNLINK_DEVICE',
  payload: {deviceId, passcode},
});

export const unlinkDeviceSuccess = (): ManageSettingsAction => ({
  type: 'UNLINK_DEVICE_SUCCESS',
});

export const unlinkDeviceError = (
  error: UnlinkErrorType,
): ManageSettingsAction => ({
  type: 'UNLINK_DEVICE_ERROR',
  payload: {errorData: error},
});

export const saveAppearanceSetting = (theme: THEME): ManageSettingsAction => ({
  type: 'SAVE_APPEARANCE_SETTING',
  payload: {theme},
});

export const saveAppearanceSettingSuccess = (): ManageSettingsAction => ({
  type: 'SAVE_APPEARANCE_SETTING_SUCCESS',
});

export const saveAppearanceSettingError = (): ManageSettingsAction => ({
  type: 'SAVE_APPEARANCE_SETTING_ERROR',
});

export const validateMPINRequest = (data: {
  username: string,
  passcode: string,
}): ManageSettingsAction => ({
  type: 'VALIDATE_MPIN_REQUEST',
  payload: data,
});

export const validateMPINSuccess = (): ManageSettingsAction => ({
  type: 'VALIDATE_MPIN_SUCCESS',
});

export const validateMPINFailed = (error: any): ManageSettingsAction => ({
  type: 'VALIDATE_MPIN_FAILED',
  payload: {
    error,
  },
});

export const updateMPINRequest = (data: {
  username: string,
  passcode: string,
  oldMpin: string,
}): ManageSettingsAction => ({
  type: 'UPDATE_MPIN_REQUEST',
  payload: data,
});

export const updateMPINSuccess = (): ManageSettingsAction => ({
  type: 'UPDATE_MPIN_SUCCESS',
});

export const updateMPINFailed = (error: any): ManageSettingsAction => ({
  type: 'UPDATE_MPIN_FAILED',
  payload: {
    error,
  },
});

export const setAppearanceFlag = (flag: boolean): ManageSettingsAction => ({
  type: 'SET_APPEARANCE_FLAG',
  payload: {
    flag,
  },
});

export const changePasswordRequest = (data: {
  passcode: string,
  oldpassword: string,
  newpassword: string,
}): ManageSettingsAction => ({
  type: 'CHANGE_PASSWORD_REQUEST',
  payload: data,
});

export const changePasswordSuccess = (): ManageSettingsAction => ({
  type: 'CHANGE_PASSWORD_SUCCESS',
});

export const changePasswordFailed = (error: any): ManageSettingsAction => ({
  type: 'CHANGE_PASSWORD_FAILED',
  payload: {
    error,
  },
});

export const getPasswordPolicyRequest = (): ManageSettingsAction => ({
  type: 'GET_PASSWORD_POLICY_REQUEST',
});

export const getPasswordPolicySuccess = (
  data: ListPasswordPolicy,
): ManageSettingsAction => ({
  type: 'GET_PASSWORD_POLICY_SUCCESS',
  payload: {
    data,
  },
});

export const getPasswordPolicyFailed = (error: any): ManageSettingsAction => ({
  type: 'GET_PASSWORD_POLICY_FAILED',
  payload: {
    error,
  },
});

export const setTemporaryTheme = (theme: ?THEME): ManageSettingsAction => ({
  type: 'SET_TEMPORARY_THEME',
  payload: {
    theme,
  },
});
