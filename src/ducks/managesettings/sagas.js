// @flow

import {
  getCustomerID,
  getEncryptionPublicKey,
  getUsername,
} from '@ducks/commonSelectors';
import {call, put, takeLatest} from '@redux-saga/core/effects';
import {select} from 'redux-saga-test-plan/matchers';
import ManageSettingsConfig from '@api/services/manageSettingsService';
import {
  fetchDevices,
  fetchDevicesError,
  fetchDevicesSuccess,
  saveAppearanceSettingSuccess,
  unlinkDeviceError,
  unlinkDeviceSuccess,
  validateMPINSuccess,
  validateMPINFailed,
  updateMPINSuccess,
  updateMPINFailed,
  saveAppearanceSettingError,
  changePasswordSuccess,
  changePasswordFailed,
  getPasswordPolicySuccess,
  getPasswordPolicyFailed,
} from './actions';
import encryptString from '@utils/encryptString';
import {getApiResponseError} from '@utils/helpers';
import {getUniqueId} from 'react-native-device-info';

import type {SaveDeviceSettingsQueryParams} from '@ducks/managesettings/types';
import {
  DEVICE_SETTINGS_CONSTANTS,
  setThemeOnAsyncStorage,
} from '@utils/manageSettings';
import {navigate} from '@navigations/service';

function* fetchDevicesApi(): any {
  const customerId = yield select(getCustomerID);

  try {
    const {data: apiResponse} = yield call(
      ManageSettingsConfig.getDevices,
      customerId,
    );
    yield put(fetchDevicesSuccess(apiResponse.data));
  } catch (apiErrorResponse) {
    console.log({apiErrorResponse});
    yield put(fetchDevicesError());
  }
}

function* unlinkDeviceApi(action: any): any {
  const {deviceId, passcode} = action.payload;

  const customerId = yield select(getCustomerID);
  const epk = yield select(getEncryptionPublicKey);
  const mpin = encryptString(epk).encrypt(passcode);

  const params = {
    customerId,
    deviceId,
    mpin,
  };

  try {
    const {data: apiResponse} = yield call(
      ManageSettingsConfig.unlinkDevice,
      params,
    );
    console.log(
      '%c UNLINK SUCCESS',
      'background-color: blue, font-weight: bold; font-size: 2rem',
    );
    yield put(unlinkDeviceSuccess());
    yield put(fetchDevices());
    console.log(apiResponse);
  } catch (apiErrorResponse) {
    const errorObj = getApiResponseError(apiErrorResponse);
    yield put(unlinkDeviceError(errorObj));
  }
}

function* saveAppearanceSettingApi(action: any): any {
  const customerId = yield select(getCustomerID);
  const username = yield select(getUsername);
  const deviceId = getUniqueId();

  const params: SaveDeviceSettingsQueryParams = {
    customerId,
    username,
    deviceId,
  };

  const {theme} = action.payload;

  const saveBody = {
    mode: DEVICE_SETTINGS_CONSTANTS.MODE,
    raw: {
      darkMode: theme,
    },
  };

  try {
    yield call(ManageSettingsConfig.saveDeviceSettings, params, saveBody);
    setThemeOnAsyncStorage(theme);

    yield put(saveAppearanceSettingSuccess());
    navigate('AccountSettings');
  } catch (apiErrorResponse) {
    const errorObj = getApiResponseError(apiErrorResponse);
    console.log({errorObj, apiErrorResponse});
    yield put(saveAppearanceSettingError());
  }
}

function* validateMPIN(action: any): any {
  const {passcode, username, setError} = action.payload;

  const customerId = yield select(getCustomerID);
  const epk = yield select(getEncryptionPublicKey);
  const mpin = encryptString(epk).encrypt(passcode);

  const params = {
    username,
    mpin,
    customerId,
  };

  try {
    yield call(ManageSettingsConfig.validateMPIN, params);
    yield put(validateMPINSuccess());
    navigate('NewPasscodeScreen', {
      oldpin: passcode,
    });
  } catch (error) {
    const errorResponse = error?.response?.data?.data?.errors[0];
    setError &&
      setError({
        code: errorResponse?.code || '',
        message: errorResponse?.message || '',
      });
    yield put(validateMPINFailed(error?.response));
  }
}

function* updateMPIN(action: any): any {
  const {passcode, oldMpin: oldpin, username, setError} = action.payload;

  const customerId = yield select(getCustomerID);
  const epk = yield select(getEncryptionPublicKey);
  const oldMPIN = encryptString(epk).encrypt(oldpin);
  const mpin = encryptString(epk).encrypt(passcode);

  const params = {
    username,
    customerId,
    oldMpin: oldMPIN,
    mpin,
  };

  try {
    yield call(ManageSettingsConfig.updateMPIN, params);
    yield put(updateMPINSuccess());
    navigate('AccountSettingsSuccessScreenPasscode');
  } catch (error) {
    const errorResponse = error?.response?.data?.data?.errors[0];
    setError &&
      setError({
        code: errorResponse?.code || '',
        message: errorResponse?.message || '',
      });
    yield put(updateMPINFailed(error?.response));
  }
}

function* changePassword(action: any): any {
  const {passcode, oldpassword, newpassword, setError} = action.payload;

  const customerId = yield select(getCustomerID);
  const epk = yield select(getEncryptionPublicKey);
  const mpin = encryptString(epk).encrypt(passcode);
  const username = yield select(getUsername);
  const oldMPIN = encryptString(epk).encrypt(oldpassword);
  const newMPIN = encryptString(epk).encrypt(newpassword);

  const params = {
    username,
    customerId,
    mpin,
    oldPassword: oldMPIN,
    password: newMPIN,
  };

  try {
    yield call(ManageSettingsConfig.changePassword, params);
    yield put(changePasswordSuccess());
    navigate('AccountSettingsSuccessScreenPassword');
  } catch (error) {
    const errorResponse = error?.response?.data?.data?.errors[0];
    setError &&
      setError({
        code: errorResponse?.code || '',
        message: errorResponse?.message || '',
      });
    yield put(changePasswordFailed(error?.response));
  }
}

function* getPasswordPolicy(): any {
  try {
    const {data} = yield call(ManageSettingsConfig.getPasswordPolicy);
    yield put(getPasswordPolicySuccess(data?.data));
  } catch (error) {
    yield put(getPasswordPolicyFailed(error?.response));
  }
}

export default function* watcher(): Generator<any, any, any> {
  yield takeLatest('FETCH_DEVICES', fetchDevicesApi);
  yield takeLatest('UNLINK_DEVICE', unlinkDeviceApi);
  yield takeLatest('SAVE_APPEARANCE_SETTING', saveAppearanceSettingApi);
  yield takeLatest('VALIDATE_MPIN_REQUEST', validateMPIN);
  yield takeLatest('UPDATE_MPIN_REQUEST', updateMPIN);
  yield takeLatest('CHANGE_PASSWORD_REQUEST', changePassword);
  yield takeLatest('GET_PASSWORD_POLICY_REQUEST', getPasswordPolicy);
}
