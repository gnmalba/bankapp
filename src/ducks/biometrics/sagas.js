/**
 * @flow
 */

import {takeLatest, call, put} from 'redux-saga/effects';
import DeviceInfo from 'react-native-device-info';
import * as types from '@ducks/biometrics/actionTypes';
import * as authTypes from '@ducks/authentication/actionTypes';
import {
  checkBiometricSensor,
  fnBioRegistration,
  fnGenerateKeyPair,
  fnStoreBio,
  fnSimplePrompt,
  fnGetBioInfo,
  fnGetLinkDisabledStatus,
  fnUnregisterBio,
} from '@ducks/helpers/biometrics';
import {fnGetLogin, fnGetPublicKey} from '@ducks/helpers/authentication';
import {Platform} from 'react-native';
import {touchIdDevices} from '@utils/constants';

export const fnGetAndroidSensor = async (): Promise<string> => {
  try {
    return DeviceInfo.getFingerprint();
  } catch (error) {
    return error;
  }
};

export function* fnBioGetSensor(): any {
  const {bioType, error, isBiometricsEnrolled} = yield call(
    checkBiometricSensor,
  );
  const linkStatus = yield call(fnGetLinkDisabledStatus);
  yield put({
    type: types.BIOMETRICS_UPDATE_STATE,
    newState: {
      deviceSensor: bioType,
      biometricsError: error || '',
      isBiometricsEnrolled,
      linkDisabled: linkStatus === 'false' ? false : true,
    },
  });
  if (Platform.OS === 'android') {
    const androidHasSensor = yield call(fnGetAndroidSensor);
    if (androidHasSensor !== 'unknown') {
      yield put({
        type: types.DEVICE_SENSOR,
        sensor: 'Fingerprint',
      });
    }
  } else if (Platform.OS === 'ios') {
    yield put({
      type: types.DEVICE_SENSOR,
      sensor:
        touchIdDevices.indexOf(DeviceInfo.getModel()) === -1
          ? 'Face ID'
          : 'Touch ID',
    });
  }
}

export function* fnBioStartRegister({payload}: Object): any {
  yield put({
    type: types.BIOMETRICS_UPDATE_STATE,
    newState: {
      loading: true,
    },
  });
  try {
    const pubKey = yield call(fnGetPublicKey);
    const {publicKey, privateKey} = yield call(fnGenerateKeyPair);
    const res = yield call(
      fnBioRegistration,
      {...payload, encryptionPublicKey: pubKey.data.data},
      publicKey,
    );
    const stored = yield call(fnStoreBio, {
      privateKey,
      token: res.data.data,
      username: payload.username,
    });
    if (stored) {
      yield put({
        type: types.BIOMETRICS_ENROLLED,
        status: true,
        enrolledUsername: payload.username,
      });
      yield put({
        type: types.BIOMETRICS_UPDATE_STATE,
        newState: {
          loading: false,
        },
      });
      if (payload?.successMessage) {
        payload.successMessage();
      }
    }
  } catch (error) {
    yield put({
      type: types.REGISTRATION_FAILED,
      status: true,
    });
    yield put({
      type: types.BIOMETRICS_UPDATE_STATE,
      newState: {
        loading: false,
        errorResponse: error?.response?.data?.data,
      },
    });
  }
}

export function* fnBioUnRegister({payload}: Object): any {
  yield put({
    type: types.BIOMETRICS_UPDATE_STATE,
    newState: {
      loading: true,
    },
  });
  try {
    const pubKey = yield call(fnGetPublicKey);
    yield call(fnUnregisterBio, {
      ...payload,
      encryptionPublicKey: pubKey.data.data,
    });
    yield put({
      type: types.BIOMETRICS_ENROLLED,
      status: false,
    });
    yield put({
      type: types.BIOMETRICS_UPDATE_STATE,
      newState: {
        loading: false,
      },
    });
    if (payload?.successMessage) {
      payload.successMessage();
    }
  } catch (error) {
    yield put({
      type: types.BIOMETRICS_UPDATE_STATE,
      newState: {
        loading: false,
        errorResponse: error?.response?.data?.data,
      },
    });
  }
}

export function* fnBioStartFlow({payload}: Object): any {
  const {
    isBiometricsEnrolled,
    biometricsError,
    deviceSensor,
    encryptionPublicKey,
  } = payload;
  const modalTxt =
    Platform.OS === 'ios'
      ? `Please go to the device settings to setup your ${deviceSensor}`
      : `Do you want to go to the device settings to setup your ${deviceSensor}?`;
  const common = {
    showModal: true,
    showPrimaryModalBtn: true,
    modalPrimaryBtnText: 'Yes',
    showSecondaryModalBtn: true,
    modalSecondaryBtnText: "No, i'll do it next time",
  };
  if (biometricsError !== '') {
    yield put({
      type: authTypes.AUTHENTICATION_UPDATE_STATE,
      newState: {
        ...common,
        error: modalTxt,
        modalPrimaryBtnAction: {
          type: types.BIOMETRICS_UPDATE_STATE,
          newState: {
            sendAndroidIntent: Platform.OS === 'android' ? true : false,
          },
        },
      },
    });
  } else if (!isBiometricsEnrolled) {
    yield put({
      type: authTypes.AUTHENTICATION_UPDATE_STATE,
      newState: {
        ...common,
        error: `Do you want to register your ${deviceSensor}`,
        modalPrimaryBtnAction: {
          type: authTypes.AUTHENTICATION_UPDATE_STATE,
          newState: {
            proceedToBioRegistration: true,
            showSecondaryModalBtn: false,
          },
        },
      },
    });
    yield put({
      type: types.BIOMETRICS_UPDATE_STATE,
      newState: {
        sendAndroidIntent: false,
        registerBiometrics: false,
      },
    });
  } else {
    try {
      const {success} = yield call(fnSimplePrompt);
      if (success) {
        yield put({
          type: types.BIOMETRICS_START_LOGIN,
        });
      }
    } catch (error) {
      const {username} = yield call(fnGetBioInfo);
      for (let i = 0; i < 3; i++) {
        fnGetLogin({
          username,
          encryptionPublicKey,
          password: '',
        }).catch((e) => {});
      }
      yield put({
        type: authTypes.AUTHENTICATION_UPDATE_STATE,
        newState: {
          showModal: true,
          showSecondaryModalBtn: false,
          error: error.message,
          modalPrimaryBtnAction: {
            type: authTypes.AUTHENTICATION_UPDATE_STATE,
            newState: {
              showModal: false,
              username: '',
              password: '',
            },
          },
        },
      });
    }
  }
}

export function* fnBioStartLogin({payload}: Object): any {
  try {
    const {signedToken, username} = yield call(fnGetBioInfo);
    yield put({
      type: authTypes.AUTHENTICATION_UPDATE_STATE,
      newState: {
        loginMethod: 'biometrics',
        signedToken,
        username,
      },
    });
    yield put({
      type: authTypes.AUTHENTICATION_START_LOGIN,
      payload: {
        username,
        signedToken,
      },
    });
  } catch (error) {
    yield put({
      type: authTypes.AUTHENTICATION_UPDATE_STATE,
      newState: {
        showModal: true,
        showSecondaryModalBtn: false,
        error: error.message,
        modalPrimaryBtnAction: {
          type: authTypes.AUTHENTICATION_UPDATE_STATE,
          newState: {
            showModal: false,
            username: '',
            password: '',
          },
        },
      },
    });
  }
}

export default function* watcher(): any {
  yield takeLatest(types.BIOMETRICS_GET_SENSOR, fnBioGetSensor);
  yield takeLatest(types.BIOMETRICS_START_REGISTER, fnBioStartRegister);
  yield takeLatest(types.BIOMETRICS_START_FLOW, fnBioStartFlow);
  yield takeLatest(types.BIOMETRICS_START_LOGIN, fnBioStartLogin);
  yield takeLatest(types.BIOMETRICS_UNREGISTER, fnBioUnRegister);
}
