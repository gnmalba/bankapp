// @flow

export const BIOMETRICS_ENROLLED = '@authentication/BIOMETRICS_ENROLLED';
export const DEVICE_SENSOR = '@authentication/DEVICE_SENSOR';
export const BIOMETRICS_ERROR = '@authentication/BIOMETRICS_ERROR';
export const BIOMETRICS_LINK_DISABLED =
  '@authentication/BIOMETRICS_LINK_DISABLED';
export const LOCK_BIOMETRICS = '@authentication/LOCK_BIOMETRICS';
export const REGISTRATION_FAILED = '@authentication/REGISTRATION_FAILED';
export const SET_REGISTER_PASSCODE = '@authentication/SET_REGISTER_PASSCODE';
export const SET_PASSCODE_ATTEMPT = '@authentication/SET_PASSCODE_ATTEMPT';
export const BIOMETRICS_GET_SENSOR = '@authentication/BIOMETRICS_GET_SENSOR';
export const REGISTER_BIOMETRICS = '@authentication/REGISTER_BIOMETRICS';
export const BIOMETRICS_START_REGISTER =
  '@authentication/BIOMETRICS_START_REGISTER';
export const BIOMETRICS_UNREGISTER = '@authentication/BIOMETRICS_UNREGISTER';
export const BIOMETRICS_UPDATE_STATE =
  '@authentication/BIOMETRICS_UPDATE_STATE';
export const BIOMETRICS_START_FLOW = '@authentication/BIOMETRICS_START_FLOW';
export const BIOMETRICS_START_LOGIN = '@authentication/BIOMETRICS_START_LOGIN';
export const SET_ENROLLED_USERNAME = '@authentication/SET_ENROLLED_USERNAME';
