// @flow

export type BioAction =
  | {type: 'BIOMETRICS_GET_SENSOR'}
  | {type: 'DEVICE_SENSOR'}
  | {type: 'BIOMETRICS_ERROR'};
