// @flow

import type {BiometricsType} from '../types';

import {
  BIOMETRICS_ENROLLED,
  DEVICE_SENSOR,
  BIOMETRICS_ERROR,
  BIOMETRICS_LINK_DISABLED,
  LOCK_BIOMETRICS,
  REGISTRATION_FAILED,
  SET_REGISTER_PASSCODE,
  SET_PASSCODE_ATTEMPT,
  REGISTER_BIOMETRICS,
  BIOMETRICS_UPDATE_STATE,
  SET_ENROLLED_USERNAME,
} from './actionTypes';

const INITIAL_STATE = {
  passcode: '',
  isBiometricsEnrolled: false,
  enrolledUsername: '',
  deviceSensor: '',
  biometricsError: '',
  linkDisabled: true,
  locked: false,
  unlockTime: null,
  lockedTimes: 0,
  registrationFailed: false,
  passcodeAttempts: 0,
  registerBiometrics: false,
  loading: false,
  sendAndroidIntent: false,
};

const biometrics = (
  state: BiometricsType = INITIAL_STATE,
  action: Object = {},
): BiometricsType => {
  switch (action.type) {
    case BIOMETRICS_ENROLLED:
      return {
        ...state,
        isBiometricsEnrolled: action.status,
        enrolledUsername: action.enrolledUsername,
      };

    case DEVICE_SENSOR:
      return {
        ...state,
        deviceSensor: action.sensor,
      };

    case BIOMETRICS_ERROR:
      return {
        ...state,
        biometricsError: action.error,
      };

    case BIOMETRICS_LINK_DISABLED:
      return {
        ...state,
        linkDisabled: action.disabled,
      };

    case LOCK_BIOMETRICS:
      return {
        ...state,
        locked: action.status,
        unlockTime: action.stamp,
        lockedTimes: action.times,
      };

    case REGISTRATION_FAILED:
      return {
        ...state,
        registrationFailed: action.status,
      };

    case SET_REGISTER_PASSCODE:
      return {
        ...state,
        passcode: action.passcode,
      };

    case SET_PASSCODE_ATTEMPT:
      return {
        ...state,
        passcodeAttempts: action.attempts,
      };

    case REGISTER_BIOMETRICS:
      return {
        ...state,
        registerBiometrics: action.status,
      };

    case BIOMETRICS_UPDATE_STATE:
      return {
        ...state,
        ...action.newState,
      };

    case SET_ENROLLED_USERNAME:
      return {
        ...state,
        enrolledUsername: action.enrolledUsername,
      };

    default:
      return state;
  }
};

export default biometrics;
