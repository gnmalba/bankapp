import {expectSaga} from 'redux-saga-test-plan';
import {
  fnGetAndroidSensor,
  fnBioStartRegister,
  fnBioUnRegister,
} from '../sagas';

jest.mock('react-native-device-info', () => {
  return {
    hasNotch: jest.fn(),
    getDeviceId: () => 'qwerty',
    getUniqueId: jest.fn(),
    getSystemVersion: jest.fn(),
    getModel: jest.fn(),
  };
});

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetPublicKey: () => Promise.reject({}),
}));

describe('saga', () => {
  test('fnGetAndroidSensor', () => {
    expectSaga(fnGetAndroidSensor).run();
  });

  test('fnBioStartRegister', async () => {
    const payload = {
      username: 'test',
      successMessage: () => {},
    };
    expectSaga(fnBioStartRegister, {payload}).run();
  });

  test('fnBioUnRegister', async () => {
    expectSaga(fnBioUnRegister, {}).run();
  });
});
