import {expectSaga} from 'redux-saga-test-plan';
import {fnBioStartFlow} from '../sagas';

jest.mock('react-native-device-info', () => {
  return {
    hasNotch: jest.fn(),
    getDeviceId: () => 'qwerty',
    getUniqueId: jest.fn(),
    getSystemVersion: jest.fn(),
    getModel: jest.fn(),
    getFingerprint: () => 'true',
  };
});

jest.mock('@ducks/helpers/biometrics', () => ({
  checkBiometricSensor: () => ({
    bioType: 'FaceID',
    error: '',
    isBiometricsEnrolled: true,
  }),
  fnGetLinkDisabledStatus: () => 'false',
  fnSimplePrompt: () => Promise.reject({}),
  fnGetBioInfo: () => ({
    username: 'test',
  }),
}));

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetLogin: () => {},
}));

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  delete RN.Platform;
  RN.Platform = {
    OS: 'ios',
  };
  return RN;
});

describe('saga', () => {
  test('fnBioStartFlow', async () => {
    const payload = {
      isBiometricsEnrolled: true,
      biometricsError: '',
    };
    expectSaga(fnBioStartFlow, {payload}).run();
  });
});
