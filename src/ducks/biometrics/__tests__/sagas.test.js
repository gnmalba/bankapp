import {expectSaga} from 'redux-saga-test-plan';
import watcher, {
  fnBioStartLogin,
  fnBioStartFlow,
  fnBioUnRegister,
  fnBioStartRegister,
  fnBioGetSensor,
  fnGetAndroidSensor,
} from '../sagas';

jest.mock('react-native-device-info', () => {
  return {
    hasNotch: jest.fn(),
    getDeviceId: () => 'qwerty',
    getUniqueId: jest.fn(),
    getSystemVersion: jest.fn(),
    getModel: jest.fn(),
    getFingerprint: () => 'true',
  };
});

jest.mock('@ducks/helpers/biometrics', () => ({
  checkBiometricSensor: () => ({
    bioType: 'FaceID',
    error: '',
    isBiometricsEnrolled: true,
  }),
  fnGetLinkDisabledStatus: () => 'false',
  fnGenerateKeyPair: () => ({
    publicKey: 'Public',
    privateKey: 'Private',
  }),
  fnBioRegistration: (options: Object, key: string) => ({
    data: {
      data: {},
    },
  }),
  fnStoreBio: (options: Object) => true,
  fnUnregisterBio: () => {},
  fnGetBioInfo: () => ({
    username: 'test',
    signedToken: 'test',
  }),
}));

jest.mock('@ducks/helpers/authentication', () => ({
  fnGetPublicKey: () => ({
    data: {
      data: 'test',
    },
  }),
}));

jest.mock('react-native', () => {
  const RN = jest.requireActual('react-native');
  delete RN.Platform;
  RN.Platform = {
    OS: 'android',
  };
  return RN;
});

describe('saga', () => {
  test('fnGetAndroidSensor', () => {
    expectSaga(fnGetAndroidSensor).run();
  });

  test('fnBioGetSensor', async () => {
    expectSaga(fnBioGetSensor).run();
  });

  test('fnBioStartRegister', async () => {
    const payload = {
      username: 'test',
      successMessage: () => {},
    };
    expectSaga(fnBioStartRegister, {payload}).run();
  });

  test('fnBioUnRegister', async () => {
    const payload = {
      username: 'test',
      successMessage: () => {},
    };
    expectSaga(fnBioUnRegister, {payload}).run();
  });

  test('fnBioStartFlow', async () => {
    const payload = {
      biometricsError: 'test',
    };
    expectSaga(fnBioStartFlow, {payload}).run();
  });

  test('fnBioStartLogin', async () => {
    expectSaga(fnBioStartLogin, {}).run();
  });

  test('watcher', async () => {
    expectSaga(watcher).run();
  });
});
