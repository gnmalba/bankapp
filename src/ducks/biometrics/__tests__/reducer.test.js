import biometrics from '../reducer';
import {
  BIOMETRICS_ENROLLED,
  DEVICE_SENSOR,
  BIOMETRICS_ERROR,
  BIOMETRICS_LINK_DISABLED,
  LOCK_BIOMETRICS,
  REGISTRATION_FAILED,
  SET_REGISTER_PASSCODE,
  SET_PASSCODE_ATTEMPT,
  REGISTER_BIOMETRICS,
  BIOMETRICS_UPDATE_STATE,
  SET_ENROLLED_USERNAME,
} from '../actionTypes';

const INITIAL_STATE = {
  passcode: '',
  isBiometricsEnrolled: false,
  enrolledUsername: '',
  deviceSensor: '',
  biometricsError: '',
  linkDisabled: false,
  locked: false,
  unlockTime: null,
  lockedTimes: 0,
  registrationFailed: false,
  passcodeAttempts: 0,
  registerBiometrics: false,
  loading: false,
  sendAndroidIntent: false,
};

describe('Biometrics reducer', () => {
  test('BIOMETRICS_ENROLLED', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: BIOMETRICS_ENROLLED,
      status: true,
      enrolledUsername: 'test',
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      isBiometricsEnrolled: true,
      enrolledUsername: 'test',
    });
  });

  test('DEVICE_SENSOR', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: DEVICE_SENSOR,
      sensor: 'test',
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      deviceSensor: 'test',
    });
  });

  test('BIOMETRICS_ERROR', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: BIOMETRICS_ERROR,
      error: 'test',
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      biometricsError: 'test',
    });
  });

  test('BIOMETRICS_LINK_DISABLED', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: BIOMETRICS_LINK_DISABLED,
      disabled: true,
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      linkDisabled: true,
    });
  });

  test('LOCK_BIOMETRICS', () => {
    biometrics(INITIAL_STATE, {
      type: LOCK_BIOMETRICS,
      status: true,
      stamp: '1111111111',
      times: 2,
    });
  });

  test('REGISTRATION_FAILED', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: REGISTRATION_FAILED,
      status: true,
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      registrationFailed: true,
    });
  });

  test('SET_REGISTER_PASSCODE', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: SET_REGISTER_PASSCODE,
      passcode: 123456,
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      passcode: 123456,
    });
  });

  test('SET_PASSCODE_ATTEMPT', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: SET_PASSCODE_ATTEMPT,
      attempts: 1,
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      passcodeAttempts: 1,
    });
  });

  test('REGISTER_BIOMETRICS', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: REGISTER_BIOMETRICS,
      status: true,
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      registerBiometrics: true,
    });
  });

  test('BIOMETRICS_UPDATE_STATE', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: BIOMETRICS_UPDATE_STATE,
      newState: {},
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
    });
  });

  test('SET_ENROLLED_USERNAME', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: SET_ENROLLED_USERNAME,
      enrolledUsername: 'test',
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
      enrolledUsername: 'test',
    });
  });

  test('BIOMETRICS_UPDATE_STATE_DEF', () => {
    const newState = biometrics(INITIAL_STATE, {
      type: 'BIOMETRICS_UPDATE_STATE_DEF',
      newState: {},
    });
    expect(newState).toEqual({
      ...INITIAL_STATE,
    });
  });
});
