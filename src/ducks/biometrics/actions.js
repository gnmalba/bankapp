/**
 * @flow
 */

import * as T from './actionTypes';

export const getBioConfig = (): any => ({
  type: T.BIOMETRICS_GET_SENSOR,
});
