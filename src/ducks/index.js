// @flow

import * as types from './actionTypes';
import store from './store';

export default {store, types};
