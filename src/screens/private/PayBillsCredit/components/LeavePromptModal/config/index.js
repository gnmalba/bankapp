/**
 * @format
 * @flow
 */

export const titleProps = {
  text: 'Are you sure you want to leave?',
};

export const subtitleProps = {
  text:
    'If you leave this screen, the information you entered will be erased and you will have to enter them again should you wish to continue with your transaction.',
};
