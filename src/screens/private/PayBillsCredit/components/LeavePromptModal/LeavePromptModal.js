/**
 * @format
 * @flow
 */

import React from 'react';

// Composition
import ModalWithCTA from '@composition/ModalWithCTA';

import {titleProps, subtitleProps} from './config';
import type {PropsType} from './types';

const LeavePromptModal = ({
  isVisible,
  handlePressGoBack,
  handlePressCancel,
}: PropsType): React$Node => {
  const positiveButtonProps = {
    buttonText: 'Go Back',
    callback: handlePressGoBack,
  };

  const negativeButtonProps = {
    buttonText: 'Cancel',
    callback: handlePressCancel,
  };

  return (
    <ModalWithCTA
      testID="paybillscreditleavePromptModal"
      isVisible={isVisible}
      titleProps={titleProps}
      subtitleProps={subtitleProps}
      positiveButtonProps={positiveButtonProps}
      negativeButtonProps={negativeButtonProps}
    />
  );
};

export default LeavePromptModal;
