import React from 'react';
import {render} from '@testing-library/react-native';
import LeavePromptModal from '../LeavePromptModal';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('LeavePromptModal', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <LeavePromptModal />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
