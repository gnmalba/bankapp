/**
 * @flow
 */

export type PropsType = {
  isVisible: boolean,
  handlePressGoBack: Function,
  handlePressCancel: Function,
};
