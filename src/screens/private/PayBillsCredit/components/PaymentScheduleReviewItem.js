/**
 * @format
 * @flow
 */
// React Modules
import React from 'react';

// Third-party libraries
import moment from 'moment';

// Components
import ReviewItem from '@composition/InstallmentDetail';

// Utils
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';
import {DATE_FORMAT_REVIEW_SCREEN} from '../constants/formats';
import endDateTimeComputation from '../utils/endDateTimeComputation';

type PropsType = {
  formControl: Object,
};

function PaymentScheduleReviewItem({formControl}: PropsType): React$Node {
  const {values} = formControl;
  const {
    scheduleType,
    occurrenceType,
    dateTime,
    frequencyType,
    startDate,
    endDate,
    endType,
    occurrences,
  } = values;

  if (scheduleType === 'Later') {
    if (occurrenceType === 'One Time') {
      return (
        <ReviewItem
          label={'PAYMENT DATE\nAND TIME'}
          value={toText(
            moment(dateTime).format(DATE_FORMAT_REVIEW_SCREEN),
            'PBCOneTime',
          )}
        />
      );
    }

    return (
      <>
        <ReviewItem
          label={'PAYMENT\nFREQUENCY'}
          value={toText(frequencyType.value, 'PBCFrequencyType')}
        />
        <ReviewItem
          label={'START DATE AND\nTIME'}
          value={toText(
            moment(startDate).format(DATE_FORMAT_REVIEW_SCREEN),
            'PBCStartDateTime',
          )}
        />
        <ReviewItem
          label="ENDS ON"
          value={
            endType === 'On'
              ? toText(
                  `${moment(endDate).format(DATE_FORMAT_REVIEW_SCREEN)}`,
                  'PBCEndTypeOn',
                )
              : toText(
                  endDateTimeComputation(startDate, occurrences, frequencyType),
                  'PBCEndTypeComputation',
                )
          }
        />
      </>
    );
  }
  return (
    <ReviewItem
      label={'PAYMENT\nSCHEDULE'}
      value={toText('Immediately', 'PBCImmediately')}
      {...testProps('PBCImmediately')}
    />
  );
}

export default PaymentScheduleReviewItem;
