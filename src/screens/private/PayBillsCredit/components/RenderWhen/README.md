## RenderWhen

RenderWhen component.

# Usage

```js
import RenderWhen from '@screens/private/PayBillsCredit/components';

# Props
```

| Prop      | Type    | Required | Description                                                 |
| --------- | ------- | -------- | ----------------------------------------------------------- |
| condition | boolean | true     | condition to be checked whether children should be rendered |
| children  | Node    | true     | React child node                                            |
