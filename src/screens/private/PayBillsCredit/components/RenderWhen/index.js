/**
 * @flow
 */
import React from 'react';
import type {Props} from './types';

import ProcessingScreen from '@screens/common/ProcessingScreen';

const isFullScreen = (isTrue: boolean = false) =>
  isTrue ? <ProcessingScreen label="" hideLabel={true} /> : null;

const RenderWhen = ({
  condition,
  children,
  isLoadingSreen,
}: Props): React$Node => {
  return condition ? children : isFullScreen(isLoadingSreen);
};

export default RenderWhen;
