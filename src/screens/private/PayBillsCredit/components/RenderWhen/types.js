/**
 * @flow
 */

export type Props = {
  condition: boolean,
  children: React$Node,
  isLoadingSreen?: boolean,
};
