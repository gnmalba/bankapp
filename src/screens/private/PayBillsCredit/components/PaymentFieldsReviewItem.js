/**
 * @format
 * @flow
 */

import React from 'react';

// Components
import ReviewItem from '@composition/InstallmentDetail';

// Utils
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';
import {sortByPosition, isSubscriberNumber} from '../utils';
import {maskedLast4digits} from '../EnrolledListScreen/utils';

type PropsType = {
  formControl: Object,
  shouldMask?: boolean,
};

const getValue = (field, value, shouldMask) => {
  const isMasked = ({val}: {val: any}): boolean => {
    return val?.includes('*');
  };
  if (shouldMask && isSubscriberNumber(field.name)) {
    return maskedLast4digits(value || field?.value);
  }

  if (!shouldMask && isMasked({val: value}) && isSubscriberNumber(field.name)) {
    return maskedLast4digits(value || field?.value);
  }
  return value || field?.value;
};

function PaymentFieldsReviewItem({
  formControl,
  shouldMask = false,
}: PropsType): React$Node {
  const {values} = formControl;
  const {biller, paymentFields} = values;
  const {fields} = biller;
  const {inputFields, labelFields} = fields;

  const fieldsToMap = [...inputFields, ...labelFields];
  const isPayCredit = false;

  if (fieldsToMap.length) {
    return fieldsToMap.sort(sortByPosition).map((field) => {
      const displayFields = {
        id: field.id,
        label:
          isSubscriberNumber(field.name) && isPayCredit
            ? 'Card No.'
            : field.label,
        value: field?.value || paymentFields[`biller_${field?.id.toString()}`],
      };
      return (
        <ReviewItem
          label={displayFields.label.toUpperCase()}
          value={toText(
            getValue(field, displayFields.value, shouldMask),
            'PBCConfirmNotes',
          )}
          {...testProps(`${displayFields.id}`)}
        />
      );
    });
  }
  return null;
}

export default PaymentFieldsReviewItem;
