/**
 * @flow
 */
// React, React Native modules
import React from 'react';

// 3rd Party Modules

// Components

// Global Components
import Layout from '@components/Layout';

// Local Stack Components
import {Form} from './components';

import {RenderWhen} from '../components';

// Component Flow Types
import type {PropTypes} from './types';

import {useInitialPayBillsCredit} from '../hooks';

const MainScreen = ({
  navigation,
  formControl,
  updateFormControl,
}: PropTypes): React$Node => {
  const {loading} = useInitialPayBillsCredit();

  return (
    <>
      <RenderWhen condition={!loading} isLoadingSreen={true}>
        <Layout>
          <Form
            formControl={formControl}
            updateFormControl={updateFormControl}
          />
        </Layout>
      </RenderWhen>
    </>
  );
};

export default MainScreen;
