/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const FieldLabelContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(12)}px;
`;

export const RadioGroupContainer: ComponentType<any> = styled.View`
  flex-direction: row;
`;
export const FieldContainer: ComponentType<any> = styled.View``;
