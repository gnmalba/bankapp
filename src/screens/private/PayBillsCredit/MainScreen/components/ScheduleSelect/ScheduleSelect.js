/**
 * @format
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import Checkbox from '@components/Checkbox';

import {SCHEDULE_TYPE} from '../../../constants/enums';

import type {PropsType} from './types';
import {
  FieldLabelContainer,
  RadioGroupContainer,
  FieldContainer,
} from './Styled';

const ScheduleSelect = ({formControl}: PropsType): React$Node => {
  const fieldName = 'scheduleType';

  const handlePress = (value) => () => {
    formControl.setFieldValue(fieldName, value);
    if (value === SCHEDULE_TYPE.IMMEDIATE) {
      formControl.resetFields([
        'occurrenceType',
        'frequencyType',
        'startDate',
        'endType',
        'endDate',
        'dateTime',
        'occurrences',
      ]);
    }
  };

  const isChecked = (value) => formControl?.values[fieldName] === value;

  return (
    <FieldContainer>
      <FieldLabelContainer>
        <Text mode="body2">Schedule</Text>
      </FieldLabelContainer>
      <RadioGroupContainer>
        <Checkbox
          title="Immediate"
          onPress={handlePress(SCHEDULE_TYPE.IMMEDIATE)}
          checked={isChecked(SCHEDULE_TYPE.IMMEDIATE)}
          isCheckbox={false}
          accessibilityLabel="MainScreenImmediateOption"
        />
        <Checkbox
          title="Later"
          onPress={handlePress(SCHEDULE_TYPE.LATER)}
          checked={isChecked(SCHEDULE_TYPE.LATER)}
          isCheckbox={false}
          accessibilityLabel="MainScreenLaterOption"
        />
      </RadioGroupContainer>
    </FieldContainer>
  );
};

export default ScheduleSelect;
