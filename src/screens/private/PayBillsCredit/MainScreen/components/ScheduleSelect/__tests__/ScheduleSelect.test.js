import React from 'react';
import {render} from '@testing-library/react-native';
import ScheduleSelect from '../ScheduleSelect';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('ScheduleSelect', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ScheduleSelect />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
