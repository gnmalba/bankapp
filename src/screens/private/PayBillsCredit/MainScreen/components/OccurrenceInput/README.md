## OccurrenceInput
OccurrenceInput component.

# Usage
```js
import OccurrenceInput from '@screens/private/OccurrenceInput';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
