/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Platform} from 'react-native';
import styled from 'styled-components/native';
import {scale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';
import Text from '@components/Text';
import BasicTextInput from '@components/BasicTextInput';

export const Container: ComponentType<any> = styled.View`
  flex-direction: column;
`;
export const OccurrenceContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const StyledBasicTextInput: ComponentType<any> = styled(
  BasicTextInput,
).attrs((props) => ({
  nativeProps: {
    style: {
      textAlign: 'center',
      width: IS_DEVICE_TABLET
        ? scale(25)
        : Platform.OS === 'android'
        ? scale(65)
        : scale(60),
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 0,
      paddingBottom: 0,
      color: props.endTypeAfter
        ? props.theme.color.TEXT_300_HIGH
        : props.theme.color.DISABLED_STATUS,
    },
  },
}))``;

export const OccurrenceText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    customColor: props.theme.textInput.placeholderTextColor,
    textAlign: 'center',
  }),
)`
  margin-left: 20px;
`;
