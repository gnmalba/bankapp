import React from 'react';
import {render} from '@testing-library/react-native';
import OccurrenceInput from '../OccurrenceInput';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('OccurrenceInput', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <OccurrenceInput
          formControl={{
            values: {
              endType: 'ON',
              occurrences: '',
            },
            touched: {
              occurrences: false,
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
