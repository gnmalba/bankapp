/**
 * @format
 * @flow
 */

import React, {useState} from 'react';

import Text from '@components/Text';
import Separator from '@components/Separator';

import {getFieldValue, getFieldError} from '../../../utils/formikUtils';

import RenderWhen from '../../../components/RenderWhen';

import type {PropsType} from './types';

import {
  Container,
  OccurrenceContainer,
  OccurrenceText,
  StyledBasicTextInput,
} from './Styled';

import {testProps} from '@utils/componentCustomProps';

const OccurrenceInput = ({formControl}: PropsType): React$Node => {
  const {values} = formControl;

  const fieldName = 'occurrences';
  const fieldValue = getFieldValue(formControl, fieldName);
  const fieldError = getFieldError(formControl, fieldName);

  const {endType} = values;
  const endTypeAfter = endType === 'After';
  const [inputState, setInputState] = useState('default');

  const handleBlur = () => {
    setInputState('default');
    formControl?.setFieldTouched(fieldName, true);
  };
  const handleFocus = () => {
    setInputState('active');
  };

  const handleInputChange = (value) => {
    formControl?.setFieldValue(fieldName, value);
  };

  return (
    <Container>
      <OccurrenceContainer>
        <StyledBasicTextInput
          placeholder="---"
          endTypeAfter={endTypeAfter}
          editable={endTypeAfter}
          state={endTypeAfter ? inputState : 'disabled'}
          keyboardType="numeric"
          onFocus={handleFocus}
          onBlur={handleBlur}
          value={fieldValue}
          onChangeText={handleInputChange}
        />
        <OccurrenceText>occurrences</OccurrenceText>
      </OccurrenceContainer>
      <Separator space={5} />
      <RenderWhen condition={!!fieldError}>
        <Text
          mode="inputerror"
          themeColor="RED_STATUS"
          {...testProps('OccurrenceError')}>
          {fieldError}
        </Text>
      </RenderWhen>
    </Container>
  );
};

export default OccurrenceInput;
