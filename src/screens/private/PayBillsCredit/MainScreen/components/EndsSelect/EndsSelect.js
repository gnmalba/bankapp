/**
 * @format
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import Checkbox from '@components/Checkbox';

import {ENDS_TYPE} from '../../../constants/enums';

import type {PropsType} from './types';
import {
  FieldContainer,
  FieldLabelContainer,
  RadioGroupContainer,
  CheckBoxInputContainer,
  CheckBoxContainer,
  EndDateContainer,
} from './Styled';
import OccurrenceInput from '../OccurrenceInput';
import EndDate from '../EndDate';

const EndsSelect = ({formControl}: PropsType): React$Node => {
  const fieldName = 'endType';

  const handlePress = (value) => () => {
    formControl.setFieldValue(fieldName, value);
    if (value === ENDS_TYPE.ON) {
      formControl.resetField('endDate');
    } else if (value === ENDS_TYPE.AFTER) {
      formControl.resetField('occurrences');
    }
  };

  const isChecked = (value) => formControl?.values[fieldName] === value;

  return (
    <FieldContainer>
      <FieldLabelContainer>
        <Text mode="body2">Ends</Text>
      </FieldLabelContainer>
      <RadioGroupContainer>
        <CheckBoxInputContainer>
          <CheckBoxContainer>
            <Checkbox
              title="On"
              onPress={handlePress(ENDS_TYPE.ON)}
              checked={isChecked(ENDS_TYPE.ON)}
              isCheckbox={false}
              accessibilityLabel="MainScreenEndsOnOption"
            />
          </CheckBoxContainer>
          <EndDateContainer>
            <EndDate formControl={formControl} />
          </EndDateContainer>
        </CheckBoxInputContainer>
        <CheckBoxInputContainer>
          <CheckBoxContainer>
            <Checkbox
              title="After"
              onPress={handlePress(ENDS_TYPE.AFTER)}
              checked={isChecked(ENDS_TYPE.AFTER)}
              isCheckbox={false}
              accessibilityLabel="MainScreenEndsAfterOption"
            />
          </CheckBoxContainer>
          <OccurrenceInput formControl={formControl} />
        </CheckBoxInputContainer>
      </RadioGroupContainer>
    </FieldContainer>
  );
};

export default EndsSelect;
