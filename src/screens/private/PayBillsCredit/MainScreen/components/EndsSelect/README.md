## EndsSelect
EndsSelect component.

# Usage
```js
import EndsSelect from '@screens/private/EndsSelect';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
