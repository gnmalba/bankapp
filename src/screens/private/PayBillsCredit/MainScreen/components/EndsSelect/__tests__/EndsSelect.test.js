import React from 'react';
import {render} from '@testing-library/react-native';
import EndsSelect from '../EndsSelect';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('EndsSelect', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <EndsSelect
          formControl={{
            values: {
              endType: 'ON',
              occurrences: '',
            },
            touched: {
              occurrences: false,
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
