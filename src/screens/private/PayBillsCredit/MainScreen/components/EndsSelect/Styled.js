/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const FieldLabelContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(12)}px;
`;

export const RadioGroupContainer: ComponentType<any> = styled.View`
  flex-direction: column;
`;
export const FieldContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(12)}px;
`;

export const CheckBoxInputContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center
  margin-vertical: ${verticalScale(5)}px;
`;
export const CheckBoxContainer: ComponentType<any> = styled.View`
  width: 30%;
`;
export const EndDateContainer: ComponentType<any> = styled.View`
  flex: 1;
`;
