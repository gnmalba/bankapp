/**
 * @flow
 */

export type PropsType = {
  field: Object,
  formControl: Object,
};
