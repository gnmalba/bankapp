/**
 * @format
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import FormikInput from '@composition/FormikInput';

import {getFieldKeyBoardType, getFieldProps} from '../../utils';
import type {PropsType} from './types';
import {FieldLabelContainer, FieldContainer} from './Styled';
const BillerTextInput = ({field, formControl}: PropsType): React$Node => {
  const {
    fieldLabel,
    fieldName,
    fieldPlaceholder,
    fieldValue,
    fieldError,
    fieldTouched,
    fieldInitialValue,
  } = getFieldProps(field, formControl);

  const handleSetFieldTouched = (name) => {
    formControl?.setFieldTouched(name, true);
  };

  const handleInputChange = (name) => (value) => {
    formControl?.setFieldValue(name, value);
  };

  const handleResetField = (name) => {
    formControl?.setFieldValue(name, fieldInitialValue);
  };

  const handleOnFocus = (name) => {
    formControl?.setFieldTouched(name, true);
  };

  const handleOnBlur = (name) => {
    formControl?.setFieldTouched(name, true);
  };

  return (
    <FieldContainer>
      <FieldLabelContainer>
        <Text mode="body2">{fieldLabel}</Text>
      </FieldLabelContainer>
      <FormikInput
        name={fieldName}
        value={fieldValue}
        placeholderText={fieldPlaceholder}
        handleSetFieldTouched={handleSetFieldTouched}
        handleInputChange={handleInputChange}
        handleOnFocus={handleOnFocus}
        handleOnBlur={handleOnBlur}
        handleResetField={handleResetField}
        keyboardType={getFieldKeyBoardType(field)}
        isTouched={fieldTouched}
        isValid={false}
        error={fieldTouched && fieldError}
      />
    </FieldContainer>
  );
};

export default BillerTextInput;
