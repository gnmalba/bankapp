/**
 * @flow
 */

import type {ComponentType} from 'react';

import styled from 'styled-components/native';

import Text from '@components/Text';
import {verticalScale} from '@utils/scale';

export const FieldLabelContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${verticalScale(12)}px;
`;

export const FieldContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(25)}px;
`;

export const FieldValueText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    customColor: props.theme.textInput.TEXT_300_HIGH,
  }),
)``;
