/**
 * @flow
 */

export type PropsType = {
  fields: Array<Object>,
  formControl: Object,
};
