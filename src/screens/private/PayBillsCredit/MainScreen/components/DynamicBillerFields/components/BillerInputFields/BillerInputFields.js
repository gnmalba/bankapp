/**
 * @format
 * @flow
 */

import React from 'react';

import BillerTextInput from '../BillerTextInput';

import type {PropsType} from './types';
import {Container} from './Styled';

const BillerInputFields = ({fields, formControl}: PropsType): React$Node => {
  const renderField = (field) => {
    return <BillerTextInput field={field} formControl={formControl} />;
  };

  return <Container>{fields.map(renderField)}</Container>;
};

export default BillerInputFields;
