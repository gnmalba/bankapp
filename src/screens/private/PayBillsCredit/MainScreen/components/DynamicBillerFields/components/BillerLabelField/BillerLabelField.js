/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

import startCase from 'lodash/startCase';

import Text from '@components/Text';

import type {PropsType} from './types';
import {FieldLabelContainer, FieldContainer, FieldValueText} from './Styled';
const BillerLabelField = ({field, formControl}: PropsType): React$Node => {
  const {id, label} = field;
  const fieldName = `paymentFields.biller_${id}`;
  const fieldLabel = startCase(`${label}`);
  const fieldValue = field.value;

  useEffect(() => {
    formControl.setFieldTouched(fieldName, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <FieldContainer>
      <FieldLabelContainer>
        <Text mode="body2">{fieldLabel}</Text>
      </FieldLabelContainer>
      <FieldValueText>{fieldValue}</FieldValueText>
    </FieldContainer>
  );
};

export default BillerLabelField;
