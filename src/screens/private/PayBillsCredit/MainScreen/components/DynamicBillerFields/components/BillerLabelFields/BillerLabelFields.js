/**
 * @format
 * @flow
 */

import React from 'react';

import BillerLabelField from '../BillerLabelField';

import type {PropsType} from './types';
import {Container} from './Styled';

const BillerLabelFields = ({fields, formControl}: PropsType): React$Node => {
  const renderField = (field) => {
    return <BillerLabelField field={field} formControl={formControl} />;
  };

  return <Container>{fields.map(renderField)}</Container>;
};

export default BillerLabelFields;
