/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import * as yup from 'yup';

import {usePayBillsCredit} from '../../../hooks';

import {RenderWhen} from '../../../components';
import {BillerInputFields, BillerLabelFields} from './components';

import {createPaymentFieldModel} from './utils';

import type {PropsType} from './types';
import {useSelector} from 'react-redux';

import {iterateEntries} from '@utils/commons';
import {maskingFormat} from '@utils/accountNumber';
import {BILLER_TYPE} from '@screens/private/PayBillsCredit/BillersScreen/constants';

const DynamicBillerFields = ({
  formControl,
  updateFormControl,
}: PropsType): React$Node => {
  const {selectedBiller, isPayCredit, billerType} = usePayBillsCredit();
  const selectedCredit = useSelector((state) => state.credit.selectedCredit);

  const cardNo = selectedCredit?.cardNo || selectedCredit?.cardno;

  const inputFields = selectedBiller?.fields.inputFields;
  const labelFields = selectedBiller?.fields.labelFields;

  const isPayCreditUnerolled =
    isPayCredit && billerType === BILLER_TYPE.allBillers;

  useEffect(() => {
    if (selectedBiller) {
      const {
        fieldModel: inputModel,
        initialValue: initialInputValues,
        value: inputValues,
      } = createPaymentFieldModel(inputFields, isPayCredit);
      const {
        fieldModel: labelModel,
        initialValue: initialLabelValues,
        value: labelValues,
      } = createPaymentFieldModel(labelFields, isPayCredit);

      const paymentFieldSchema = {
        paymentFields: yup
          .object({
            ...inputModel,
            ...labelModel,
          })
          .nullable(),
      };
      /*########## PAY CREDIT CONDITIONS ##########s*/
      const _initialInputValues = isPayCreditUnerolled
        ? iterateEntries(initialInputValues, () => cardNo)
        : initialInputValues;

      const _inputValues = isPayCreditUnerolled
        ? iterateEntries(inputValues, () => cardNo)
        : inputValues;

      /*########## PAY CREDIT CONDITIONS ##########s*/

      updateFormControl(
        paymentFieldSchema,
        {
          paymentFields: {
            ..._initialInputValues,
            ...initialLabelValues,
          },
        },
        {
          paymentFields: {
            ..._inputValues,
            ...labelValues,
          },
        },
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedBiller, inputFields, labelFields]);

  /*##########  START: PAY CREDIT CONDITIONS ##########s*/
  // Debt: Refactor
  if (isPayCredit) {
    const getfields = isPayCreditUnerolled ? inputFields : labelFields;

    const fields =
      getfields &&
      getfields.map((row: Object) => {
        return {
          id: row.id,
          label: 'Card No',
          value: maskingFormat(cardNo, true), //should masked
          name: row.name,
        };
      });

    return (
      <RenderWhen condition={!!selectedBiller}>
        <BillerLabelFields fields={fields} formControl={formControl} />
      </RenderWhen>
    );
  }
  /*########## END: PAY CREDIT CONDITIONS ##########s*/

  return (
    <RenderWhen condition={!!selectedBiller}>
      <BillerLabelFields fields={labelFields} formControl={formControl} />
      <BillerInputFields fields={inputFields} formControl={formControl} />
    </RenderWhen>
  );
};

export default DynamicBillerFields;
