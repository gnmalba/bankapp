/**
 * @flow
 */

export type PropsType = {
  formControl: Object,
  updateFormControl: Function,
};

export type IRootObject = {
  id: number,
  name: string,
  shortName: string,
  description: null,
  category: ICategory,
  type: IType,
  paymentEnabled: boolean,
  enrollmentEnabled: boolean,
  currencyCode: string,
  allowedChannel: string,
  mid: string,
  tid: string,
  restrictedForCC: boolean,
  utilityCode: string,
  bins: null,
  fields: BillerFieldType[],
};

export type ICategory = {
  id: number,
  code: string,
  name: string,
};

export type IType = {
  id: number,
  code: string,
  name: string,
};

export type BillerFieldType = {
  id: number | string,
  name: string,
  label: string,
  required?: boolean,
  forEnrollment?: boolean,
  forPayment?: boolean,
  regularExpression?: string,
  validation?: IValidationItem[],
  position?: number,
  placeholder?: string,
  value?: string,
};

export type IValidationItem = {
  ruleCategory: IRuleCategory,
  referenceValue: null,
};

export type IRuleCategory = {
  name: 'MIN_LENGTH' | 'MAX_LENGTH' | 'NUMERIC' | 'ALPHANUMBERIC',
  description: string,
  referenceValue?: string,
};
