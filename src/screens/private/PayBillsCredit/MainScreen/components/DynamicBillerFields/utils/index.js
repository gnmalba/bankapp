/**
 * @format
 * @flow
 */

import * as yup from 'yup';
import startCase from 'lodash/startCase';
import type {
  BillerFields,
  ValidationRule,
  RuleCategory,
} from '@ducks/billers/types';

export const isFieldNumberic = (field: BillerFields): boolean => {
  if (!field?.validation || !field?.validation.length) {
    return false;
  }

  return field.validation.some(
    (validation) => validation?.ruleCategory?.name === 'NUMERIC',
  );
};

export const getFieldProps = (field: any, formControl: Object): Object => {
  const {id, label, required, placeholder} = field;
  const fieldName = `paymentFields.biller_${id}`;
  const fieldValue =
    formControl?.values?.paymentFields &&
    formControl?.values?.paymentFields[`biller_${id}`];
  const fieldTouched =
    formControl?.touched?.paymentFields &&
    !!formControl?.touched?.paymentFields[`biller_${id}`];
  const fieldLabel = startCase(`${label} ${required ? '*' : ''}`);
  const fieldPlaceholder = placeholder ?? `Enter ${fieldLabel}`;
  const fieldError =
    formControl?.errors?.paymentFields &&
    formControl?.errors?.paymentFields[`biller_${id}`];
  const fieldInitialValue =
    formControl?.initialValues?.paymentFields &&
    formControl?.initialValues?.paymentFields[`biller_${id}`];

  return {
    fieldLabel,
    fieldName,
    fieldValue,
    fieldPlaceholder,
    fieldError,
    fieldTouched,
    fieldInitialValue,
  };
};

export const getFieldKeyBoardType = (field: BillerFields): string => {
  return isFieldNumberic(field) ? 'decimal-pad' : 'default';
};

const convertToYupValidation = (fieldModel, validationRules) => {
  let ruleCategoryDescription: string | null = null;

  if (validationRules && validationRules.length) {
    validationRules.forEach((rule: ValidationRule) => {
      const ruleCategory: RuleCategory = rule.ruleCategory;
      if (!ruleCategoryDescription) {
        ruleCategoryDescription = ruleCategory.description;
      }

      switch (ruleCategory.name) {
        case 'MIN_LENGTH':
          fieldModel = fieldModel.min(
            rule.referenceValue,
            ruleCategory.description,
          );
          break;

        case 'MAX_LENGTH':
          fieldModel = fieldModel.max(
            rule.referenceValue,
            ruleCategory.description,
          );
          break;

        default:
          break;
      }
    });
  }

  return fieldModel;
};

export const createYupModel = (field: any, isPayCredit: boolean): Object => {
  const validationRules = field.validation;
  let fieldModel = yup.string().nullable();

  if (!isPayCredit) {
    if (validationRules && validationRules.length) {
      fieldModel = convertToYupValidation(fieldModel, validationRules);
    }

    if (field.regularExpression) {
      fieldModel = fieldModel.matches(
        field.regularExpression,
        `Invalid ${field.label}.`,
      );
    }

    if (field.required) {
      fieldModel = fieldModel.required(
        `${startCase(field.label)} is required.`,
      );
    }
  }

  return {
    fieldModel: fieldModel,
    initialValue: '',
    value: field.value ?? null,
  };
};

export const createPaymentFieldModel = (
  billerFields: Array<any>,
  isPayCredit: boolean,
): Object => {
  return billerFields.reduce((paymentModel: Object, field) => {
    const key = `biller_${field.id}`;
    const {fieldModel, initialValue, value} = createYupModel(
      field,
      isPayCredit,
    );

    if (!paymentModel.fieldModel) {
      paymentModel.fieldModel = {};
    }

    if (!paymentModel.initialValue) {
      paymentModel.initialValue = {};
    }

    if (!paymentModel.value) {
      paymentModel.value = {};
    }

    paymentModel.fieldModel[key] = fieldModel;
    paymentModel.initialValue[key] = initialValue;
    paymentModel.value[key] = value;
    return paymentModel;
  }, {});
};
