/**
 * @format
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import FormikInput from '@composition/FormikInput';

import type {PropsType} from './types';
import {FieldLabelContainer, FieldContainer, OptionalText} from './Styled';
const NotesInput = ({formControl}: PropsType): React$Node => {
  const handleSetFieldTouched = (name) => {
    formControl?.setFieldTouched(name, true);
  };

  const handleInputChange = (name) => (value) => {
    formControl?.setFieldValue(name, value);
  };

  const handleResetField = (name) => {
    formControl?.setFieldValue(name, formControl?.initialValues[name]);
    formControl?.setFieldTouched(name, true);
  };

  return (
    <FieldContainer>
      <FieldLabelContainer>
        <Text mode="body2">Notes</Text>
        <OptionalText> (Optional)</OptionalText>
      </FieldLabelContainer>
      <FormikInput
        name="notes"
        value={formControl?.values?.notes}
        placeholderText="Enter Notes"
        handleSetFieldTouched={handleSetFieldTouched}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
      />
    </FieldContainer>
  );
};

export default NotesInput;
