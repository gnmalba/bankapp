import React from 'react';
import {render} from '@testing-library/react-native';
import PaymentDateTime from '../PaymentDateTime';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('PaymentDateTime', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <PaymentDateTime
          formControl={{
            values: {
              dateTime: '',
            },
            touched: {
              dateTime: false,
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
