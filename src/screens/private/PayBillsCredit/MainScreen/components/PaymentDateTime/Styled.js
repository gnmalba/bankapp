/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import {moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  margin-bottom: ${moderateScale(25)}px;
`;
