## PaymentDateTime

PaymentDateTime component.

# Usage

```js
import PaymentDateTime from '@screens/private/PaymentDateTime';

# Props
```

| Prop        | Type   | Required | Description          |
| ----------- | ------ | -------- | -------------------- |
| formControl | object | true     | formik's formControl |
