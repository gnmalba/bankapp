/**
 * @format
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import Checkbox from '@components/Checkbox';

import {OCCURRENCE_TYPE} from '../../../constants/enums';

import {usePayBillsCredit} from '../../../hooks';

import type {PropsType} from './types';
import {
  FieldLabelContainer,
  RadioGroupContainer,
  FieldContainer,
} from './Styled';

const OccurrenceSelect = ({formControl}: PropsType): React$Node => {
  const {isPayCredit} = usePayBillsCredit();

  const fieldName = 'occurrenceType';

  const handlePress = (value) => () => {
    formControl.setFieldValue(fieldName, value);
    if (value === OCCURRENCE_TYPE.ONE_TIME) {
      formControl.resetFields([
        'frequencyType',
        'startDate',
        'endType',
        'endDate',
      ]);
    } else if (value === OCCURRENCE_TYPE.REPEATING) {
      formControl.resetFields(['dateTime']);
    }
  };

  const isChecked = (value) => formControl?.values[fieldName] === value;

  return (
    <FieldContainer>
      <FieldLabelContainer>
        <Text mode="body2">Occurrence</Text>
      </FieldLabelContainer>
      <RadioGroupContainer>
        <Checkbox
          title="One Time"
          onPress={handlePress(OCCURRENCE_TYPE.ONE_TIME)}
          checked={isChecked(OCCURRENCE_TYPE.ONE_TIME)}
          isCheckbox={false}
          accessibilityLabel="MainScreenOneTimeOption"
        />
        <Checkbox
          title="Repeating"
          onPress={handlePress(OCCURRENCE_TYPE.REPEATING)}
          checked={isChecked(OCCURRENCE_TYPE.REPEATING)}
          isCheckbox={false}
          accessibilityLabel="MainScreenRepeatingOption"
          disabled={isPayCredit}
        />
      </RadioGroupContainer>
    </FieldContainer>
  );
};

export default OccurrenceSelect;
