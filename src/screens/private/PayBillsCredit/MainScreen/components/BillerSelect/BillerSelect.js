/**
 * @format
 * @flow
 */

import React, {useMemo} from 'react';
import {InteractionManager} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Text from '@components/Text';
import TouchableDisplay from '@composition/TouchableDisplay';
import {Display1} from '@composition/TouchableDisplay/Displays';

import RenderWhen from '../../../components/RenderWhen';

import {getFieldValue, getFieldError} from '../../../utils/formikUtils';

import {usePayBillsCredit} from '../../../hooks';

import type {PropsType} from './types';
import {Container, ErrorMessageContainer} from './Styled';
const BillerSelect = ({formControl}: PropsType): React$Node => {
  const navigation = useNavigation();
  const {isPayCredit} = usePayBillsCredit();

  const fieldName = 'biller';

  const fieldValue = getFieldValue(formControl, fieldName);
  const fieldError = getFieldError(formControl, fieldName);

  const content = useMemo(() => {
    if (fieldValue) {
      const title = fieldValue.alias || fieldValue.name;
      const subtitle = fieldValue.alias
        ? fieldValue.name
        : fieldValue.category?.name;
      return Display1(title, subtitle, {
        title: title,
        subtitle: subtitle,
      });
    }

    return null;
  }, [fieldValue]);

  const handlePress = () => {
    navigation.navigate('PayBillsCredit', {
      screen: 'BillersScreen',
    });

    InteractionManager.runAfterInteractions(() => {
      formControl.setFieldTouched(fieldName, true);
    });
  };

  return (
    <Container>
      <TouchableDisplay
        placeholder="Select a biller to pay from the billers list"
        label="To"
        onPress={handlePress}
        content={content}
        disabled={isPayCredit}
      />
      <RenderWhen condition={!!fieldError}>
        <ErrorMessageContainer>
          <Text mode="body2" themeColor="RED_STATUS">
            {fieldError}
          </Text>
        </ErrorMessageContainer>
      </RenderWhen>
    </Container>
  );
};

export default BillerSelect;
