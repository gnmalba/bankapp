/**
 * @flow
 */

export type PropsType = {
  formControl: Object,
};
