/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

// import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View``;

export const ErrorMessageContainer: ComponentType<any> = styled.View``;
