/**
 * @format
 * @flow
 */

import React, {useMemo} from 'react';
import {InteractionManager} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import Text from '@components/Text';
import TouchableDisplay from '@composition/TouchableDisplay';
import {Display1} from '@composition/TouchableDisplay/Displays';

import RenderWhen from '../../../components/RenderWhen';

import {getCardNumber, formatCardNumber} from '../../../utils';
import {getFieldValue, getFieldError} from '../../../utils/formikUtils';

import type {PropsType} from './types';
import {Container, ErrorMessageContainer} from './Styled';

import {usePayBillsCredit} from '../../../hooks';

const AccountSelect = ({formControl}: PropsType): React$Node => {
  const navigation = useNavigation();

  const fieldName = 'sourceAccount';

  const fieldValue = getFieldValue(formControl, fieldName);
  const fieldError = getFieldError(formControl, fieldName);
  const {selectedSourceAccount} = usePayBillsCredit();
  const content = useMemo(() => {
    if (fieldValue) {
      const {alias, accountType} = fieldValue;
      const cardNumber = formatCardNumber(
        accountType,
        getCardNumber(accountType, fieldValue),
      );
      return Display1(alias, `ACCOUNT NUMBER - ${cardNumber}`, {
        title: 'PBCMainScreenAlias',
        subtitle: 'PBCMainScreenCardNumber',
      });
    }

    if (selectedSourceAccount) {
      const {alias, accountType} = selectedSourceAccount;
      const cardNumber = formatCardNumber(
        accountType,
        getCardNumber(accountType, selectedSourceAccount),
      );
      return Display1(alias, `ACCOUNT NUMBER - ${cardNumber}`, {
        title: 'PBCMainScreenAlias',
        subtitle: 'PBCMainScreenCardNumber',
      });
    }
    return null;
  }, [fieldValue, selectedSourceAccount]);

  const handlePress = () => {
    navigation.navigate('PayBillsCredit', {
      screen: 'SourceAccountScreen',
    });

    InteractionManager.runAfterInteractions(() => {
      formControl.setFieldTouched('sourceAccount', true);
    });
  };

  return (
    <Container>
      <TouchableDisplay
        placeholder="Select an account to use for paying bills"
        label="From"
        onPress={handlePress}
        content={content}
      />
      <RenderWhen condition={!!fieldError}>
        <ErrorMessageContainer>
          <Text mode="body2" themeColor="RED_STATUS">
            {fieldError}
          </Text>
        </ErrorMessageContainer>
      </RenderWhen>
    </Container>
  );
};

export default AccountSelect;
