## StartDate

StartDate component.

# Usage

```js
import StartDate from '@screens/private/StartDate';

# Props
```

| Prop        | Type   | Required | Description          |
| ----------- | ------ | -------- | -------------------- |
| formControl | object | true     | Formik's formcontrol |
