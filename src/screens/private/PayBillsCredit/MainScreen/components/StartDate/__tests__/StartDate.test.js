import React from 'react';
import {render} from '@testing-library/react-native';
import StartDate from '../StartDate';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('StartDate', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <StartDate
          formControl={{
            values: {
              startDate: '',
            },
            touched: {
              startDate: false,
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
