/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';

import moment from 'moment';

import DateTimeInputPicker from '@composition/DateTimeInputPicker';

import {getDisabledDates} from '../../../utils/';
import {
  getFieldValue,
  getFieldError,
  getFieldTouched,
} from '../../../utils/formikUtils';
import {DATE_TIME_FORMAT} from '../../../constants/formats';

import type {PropsType} from './types';
import {Container} from './Styled';

const StartDate = ({formControl}: PropsType): React$Node => {
  const fieldName = 'startDate';
  const fieldValue = getFieldValue(formControl, fieldName);
  const fieldTouched = getFieldTouched(formControl, fieldName);
  const fieldError = getFieldError(formControl, fieldName);

  const [inputState, setInputState] = useState('default');
  const [showVisibleCalendarModal, setShowVisibleCalendarModal] = useState(
    false,
  );
  const [showVisibleTimePickerModal, setShowVisibleTimePickerModal] = useState(
    false,
  );

  const [date, setDate] = useState({
    dateInput: '',
    timeInput: null,
    dateTimeInput: '',
  });

  const setFormControlValue = () => {
    const time = date.timeInput || new Date();
    const dateTime = `${date.dateInput} ${moment(time).format('hh:mm A')}`;
    setDate({...date, timeInput: time, dateTimeInput: dateTime});
    formControl?.setFieldValue(fieldName, dateTime);
  };

  const handleOnChangeText = (value) => {
    formControl?.setFieldValue(fieldName, value);
  };

  const handleOnFocus = () => {
    if (!fieldTouched) {
      formControl?.setFieldTouched(fieldName, true);
    }
    formControl?.setFieldError(fieldName, '');
    setInputState('active');
  };

  const handleOnBlur = () => {
    if (fieldError) {
      setInputState('error');
    } else {
      setInputState('default');
    }

    fieldValue
      ? formControl?.setFieldValue(fieldName, fieldValue)
      : formControl?.setFieldValue(fieldName, '');
  };

  useEffect(() => {
    if (fieldError) {
      setInputState('error');
    }
  }, [fieldError]);

  return (
    <Container>
      <DateTimeInputPicker
        mode="dateTime"
        label="Start Date"
        icon="date"
        placeholder={DATE_TIME_FORMAT}
        textWidth="80%"
        iconPressHandler={() => setShowVisibleCalendarModal(true)}
        state={inputState}
        value={fieldValue}
        helperText={fieldError}
        onChangeText={handleOnChangeText}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        calendarModalProp={{
          showVisibleCalendarModal: showVisibleCalendarModal,
          handleCalendarOnApply: (calendar) => {
            setDate({...date, dateInput: calendar});
            setShowVisibleCalendarModal(false);
            setShowVisibleTimePickerModal(true);
          },
          handleCalendarOnCancel: () => {
            setShowVisibleCalendarModal(false);
          },
          calendarProps: {
            selectedStartDate: fieldValue,
            allowRangeSelection: false,
            initialDate: fieldValue || moment(),
            disabledDates: getDisabledDates,
          },
        }}
        timePickerModalProp={{
          timePickerLabel: 'Set Reminder Time',
          onBackdropPress: () => {
            setShowVisibleTimePickerModal(false);
            setFormControlValue();
          },
          showVisibleTimePickerModal: showVisibleTimePickerModal,
          timePickerProps: {
            date: date.timeInput || new Date(),
            onDateChange: (dateTime) => {
              setDate({...date, timeInput: dateTime});
            },
          },
        }}
      />
    </Container>
  );
};

export default StartDate;
