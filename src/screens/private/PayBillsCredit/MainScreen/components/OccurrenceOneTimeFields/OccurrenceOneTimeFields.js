/**
 * @format
 * @flow
 */

import React from 'react';

import PaymentDateTime from '../PaymentDateTime';

import type {PropsType} from './types';
import {Container} from './Styled';
const OccurrenceOneTimeFields = ({formControl}: PropsType): React$Node => {
  return (
    <Container>
      <PaymentDateTime formControl={formControl} />
    </Container>
  );
};

export default OccurrenceOneTimeFields;
