/**
 * @format
 * @flow
 */

import React from 'react';

import StartDate from '../StartDate';
import FrequencySelect from '../FrequencySelect';
import EndsSelect from '../EndsSelect';

import type {PropsType} from './types';

const OccurrenceRepeatingFields = ({formControl}: PropsType): React$Node => {
  return (
    <>
      <StartDate formControl={formControl} />
      <FrequencySelect formControl={formControl} />
      <EndsSelect formControl={formControl} />
    </>
  );
};

export default OccurrenceRepeatingFields;
