import React from 'react';
import {render} from '@testing-library/react-native';
import OccurrenceRepeatingFields from '../OccurrenceRepeatingFields';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('OccurrenceRepeatingFields', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <OccurrenceRepeatingFields
          formControl={{
            values: {
              endType: 'ON',
              occurrences: '',
            },
            errors: {
              frequencyType: null,
              startDate: null,
              endType: null,
              endDate: null,
              occurrences: null,
            },
            touched: {
              frequencyType: false,
              startDate: false,
              endType: false,
              endDate: false,
              occurrences: false,
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
