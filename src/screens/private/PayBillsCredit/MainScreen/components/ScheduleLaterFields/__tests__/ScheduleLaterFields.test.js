import React from 'react';
import {render} from '@testing-library/react-native';
import ScheduleLaterFields from '../ScheduleLaterFields';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
describe('ScheduleLaterFields', () => {
  const mockAppState = {
    paybillscredit: {
      accounts: {
        all: [],
        creditAndYazz: [],
        depositAndPrepaid: [],
      },
      selectedSourceAccount: null,
      loading: false,
      error: '',
      isInitial: true,
      payBillsAccounts: {
        all: [],
        creditAndYazz: [],
        depositAndPrepaid: [],
      },
      postRequestResponse: {
        referenceNumber: '',
        dateTime: '',
        error: {
          code: '',
          message: '',
        },
        errorType: '',
        status: null,
      },
      isPayBillsCredit: false,
    },
    billers: {
      loading: false,
      billers: {
        enrolledBillers: [],
        allBillers: [],
      },
      selectedBiller: {},
      allBillersByCategory: [],
      categories: [],
      payBillsSelectedBiller: null,
      payBillsAllBillers: {
        alphabetical: [],
        section: [],
      },
      payBillsEnrolledBiller: [],
      selectedBillerId: '',
      selectedBillerType: '', //allBillers === unenrolled billers
      payBillsSelectedCategory: '',
    },
  };

  it('Should work as expected', () => {
    const store = {
      ...mockAppState,
    };

    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ScheduleLaterFields />
        </ThemeProvider>
        ,
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
