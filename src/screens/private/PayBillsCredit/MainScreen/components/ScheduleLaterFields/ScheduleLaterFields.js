/**
 * @flow
 */
// React, React Native modules
import React from 'react';

// 3rd Party Modules
// Components

import {RenderWhen} from '../../../components';

import OccurrenceSelect from '../OccurrenceSelect';
import OccurrenceOneTimeFields from '../OccurrenceOneTimeFields';
import OccurrenceRepeatingFields from '../OccurrenceRepeatingFields';

// Component Flow Types
import type {PropsType} from './types';

const ScheduleLaterFields = ({formControl}: PropsType): React$Node => {
  const formValues = formControl?.values;

  return (
    <>
      <OccurrenceSelect formControl={formControl} />
      <RenderWhen condition={formValues?.occurrenceType === 'One Time'}>
        <OccurrenceOneTimeFields formControl={formControl} />
      </RenderWhen>
      <RenderWhen condition={formValues?.occurrenceType === 'Repeating'}>
        <OccurrenceRepeatingFields formControl={formControl} />
      </RenderWhen>
    </>
  );
};

export default ScheduleLaterFields;
