/**
 * @format
 * @flow
 */

import React, {useState} from 'react';

import SelectInput from '@components/SelectInput';

import {FREQUENCY_TYPE} from '../../../constants/enums';

import type {PropsType} from './types';

const frequencyOptions = Object.values(FREQUENCY_TYPE).map((option, index) => {
  return {
    id: index,
    value: option,
  };
});

const FrequencySelect = ({formControl}: PropsType): React$Node => {
  const [isOpen, setIsOpen] = useState(false);

  const handleOnSelect = (value) => {
    formControl?.setFieldValue('frequencyType', value);
  };

  return (
    <SelectInput
      label="Frequency"
      data={frequencyOptions}
      placeholder="Select payment frequency"
      isOpen={isOpen}
      openSetter={setIsOpen}
      setter={handleOnSelect}
    />
  );
};

export default FrequencySelect;
