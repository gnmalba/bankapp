/**
 * @flow
 */

export type PropTypes = {
  formControl: Object,
  updateFormControl: Function,
};
