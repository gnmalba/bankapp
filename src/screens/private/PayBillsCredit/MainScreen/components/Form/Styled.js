/**
 * @flow
 */

import type {ComponentType} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';

import {scale, verticalScale} from '@utils/scale';

export const StyledScrollView: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs(() => ({
  keyboardShouldPersistTaps: 'handled',
  bounces: false,
  showsVerticalScrollIndicator: false,
  enableOnAndroid: true,
  extraScrollHeight: 40,
  contentContainerStyle: {
    paddingBottom: verticalScale(72),
  },
  flex: 1,
}))``;

export const FieldsContainer: ComponentType<any> = styled.View`
  flex: 1;
  padding-horizontal: ${scale(32)}px;
  padding-top: ${verticalScale(25)}px;
`;
