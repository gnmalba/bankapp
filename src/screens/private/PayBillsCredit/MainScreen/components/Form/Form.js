/**
 * @flow
 */
// React, React Native modules
import React from 'react';

// 3rd Party Modules
// Components

// Global Components
import Layout from '@components/Layout';

import {SCHEDULE_TYPE} from '../../../constants/enums';

import {RenderWhen} from '../../../components';

import AccountSelect from '../AccountSelect';
import AmountInput from '../AmountInput';
import BillerSelect from '../BillerSelect';
import NotesInput from '../NotesInput';
import NextButton from '../NextButton';
import ScheduleSelect from '../ScheduleSelect';
import ScheduleLaterFields from '../ScheduleLaterFields';
import DynamicBillerFields from '../DynamicBillerFields';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import {StyledScrollView, FieldsContainer} from './Styled';
const Form = ({formControl, updateFormControl}: PropTypes): React$Node => {
  const {values} = formControl;

  return (
    <Layout>
      <StyledScrollView>
        <FieldsContainer>
          <AccountSelect formControl={formControl} />
          <AmountInput formControl={formControl} />
          <BillerSelect formControl={formControl} />
          <DynamicBillerFields
            formControl={formControl}
            updateFormControl={updateFormControl}
          />
          <ScheduleSelect formControl={formControl} />
          <RenderWhen condition={values.scheduleType === SCHEDULE_TYPE.LATER}>
            <ScheduleLaterFields formControl={formControl} />
          </RenderWhen>
          <NotesInput formControl={formControl} />
          <NextButton formControl={formControl} />
        </FieldsContainer>
      </StyledScrollView>
    </Layout>
  );
};

export default Form;
