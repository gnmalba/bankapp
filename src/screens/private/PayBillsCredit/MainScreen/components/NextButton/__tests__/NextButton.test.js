import React from 'react';
import {render} from '@testing-library/react-native';
import NextButton from '../NextButton';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('NextButton', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <NextButton
          formControl={{
            isValid: false,
            dirty: false,
            errors: {},
            status: {},
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
