/**
 * @format
 * @flow
 */

import React from 'react';

// 3rd Party Modules
import isEmpty from 'lodash/isEmpty';
import {useNavigation} from '@react-navigation/native';

// Global Components
import Button from '@components/Button';

import type {PropsType} from './types';

const NextButton = ({formControl}: PropsType): React$Node => {
  const navigation = useNavigation();

  const handlePressButton = () => {
    navigation.navigate('PayBillsCredit', {
      screen: 'ConfirmScreen',
    });
  };

  const {isValid, dirty, errors, status} = formControl;

  const isDisabled = !isValid || !dirty || !isEmpty(errors) || !isEmpty(status);

  return (
    <Button
      title="Next"
      state={isDisabled ? 'disabled' : 'default'}
      type="primary"
      size="large"
      onPress={handlePressButton}
    />
  );
};

export default NextButton;
