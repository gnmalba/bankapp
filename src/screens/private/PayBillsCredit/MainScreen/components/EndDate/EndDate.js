/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';

import moment from 'moment';

import DateTimeInputPicker from '@composition/DateTimeInputPicker';

import {getDisabledDates} from '../../../utils';
import {
  getFieldValue,
  getFieldError,
  getFieldTouched,
} from '../../../utils/formikUtils';

import {DATE_FORMAT} from '../../../constants/formats';

import type {PropsType} from './types';

const EndDate = ({formControl}: PropsType): React$Node => {
  const fieldName = 'endDate';
  const fieldValue = getFieldValue(formControl, fieldName);
  const fieldTouched = getFieldTouched(formControl, fieldName);
  const fieldError = getFieldError(formControl, fieldName);

  const [inputState, setInputState] = useState('default');
  const [showVisibleCalendarModal, setShowVisibleCalendarModal] = useState(
    false,
  );

  const handleOnChangeText = (value) => {
    formControl?.setFieldValue(fieldName, value);
  };

  const handleOnFocus = () => {
    if (!fieldTouched) {
      formControl?.setFieldTouched(fieldName, true);
    }
    formControl?.setFieldError(fieldName, '');
    setInputState('active');
  };

  const handleOnBlur = () => {
    if (fieldError) {
      setInputState('error');
    } else {
      setInputState('default');
    }
    fieldValue
      ? formControl?.setFieldValue(fieldName, fieldValue)
      : formControl?.setFieldValue(fieldName, '');
  };

  useEffect(() => {
    if (fieldError) {
      setInputState('error');
    }
  }, [fieldError]);
  return (
    <DateTimeInputPicker
      mode="date"
      icon="date"
      placeholder={DATE_FORMAT}
      iconPressHandler={() => setShowVisibleCalendarModal(true)}
      state={inputState}
      value={fieldValue}
      helperText={fieldError}
      onChangeText={handleOnChangeText}
      onFocus={handleOnFocus}
      onBlur={handleOnBlur}
      calendarModalProp={{
        showVisibleCalendarModal: showVisibleCalendarModal,
        handleCalendarOnApply: (calendar) => {
          formControl?.setFieldValue(fieldName, calendar);
          setShowVisibleCalendarModal(false);
        },
        handleCalendarOnCancel: () => {
          setShowVisibleCalendarModal(false);
        },
        calendarProps: {
          selectedStartDate: fieldValue,
          allowRangeSelection: false,
          initialDate: fieldValue || moment(),
          disabledDates: getDisabledDates,
        },
      }}
    />
  );
};

export default EndDate;
