import React from 'react';
import {render} from '@testing-library/react-native';
import EndDate from '../EndDate';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('EndDate', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <EndDate
          formControl={{
            values: {
              endDate: '',
            },
            touched: {
              endDate: false,
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
