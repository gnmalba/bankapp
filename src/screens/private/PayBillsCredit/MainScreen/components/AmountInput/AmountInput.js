/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

import CAmountInput from '@composition/AmountInput';

import {
  getFieldValue,
  getFieldError,
  getFieldTouched,
  getFieldInitialVal,
} from '../../../utils/formikUtils';

import {
  PB_NOT_ENOUGH_FUNDS,
  PC_NOT_ENOUGH_FUNDS,
} from '../../../constants/errorMessages';

import {usePayBillsCredit} from '../../../hooks';

import {getAvailableFunds} from '../../../utils';

import type {PropsType} from './types';
const AmountInput = ({formControl}: PropsType): React$Node => {
  const fieldName = 'amount';
  const fieldValue = getFieldValue(formControl, fieldName);
  const sourceAccount = getFieldValue(formControl, 'sourceAccount');
  const fieldTouched = getFieldTouched(formControl, fieldName);
  const fieldError = getFieldError(formControl, fieldName);

  const currencyCode = sourceAccount?.currencyCode ?? 'PHP';

  const {isPayCredit} = usePayBillsCredit();

  useEffect(() => {
    if (sourceAccount) {
      formControl?.setFieldValue(
        fieldName,
        getFieldInitialVal(formControl, fieldName),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sourceAccount]);

  const handleOnFocus = () => {
    if (!fieldTouched) {
      formControl?.setFieldTouched(fieldName, true);
    }
  };
  const handleOnBlur = (value) => {
    formControl?.setFieldValue(fieldName, value);

    const amount = Number(value.toString().replace(/,/g, ''));
    const accountFunds = getAvailableFunds(sourceAccount);

    if (value !== '') {
      if (sourceAccount && amount > accountFunds) {
        const errorMessage = isPayCredit
          ? PC_NOT_ENOUGH_FUNDS
          : PB_NOT_ENOUGH_FUNDS;

        formControl?.setStatus({
          amount: errorMessage,
        });
      } else {
        formControl?.setStatus({});
      }
    }
  };

  return (
    <CAmountInput
      placeholder="Enter amount"
      label="Amount"
      currency={currencyCode}
      dispatchAction={null}
      value={fieldValue}
      errorMessage={fieldError}
      onFocus={handleOnFocus}
      onBlur={handleOnBlur}
    />
  );
};

export default AmountInput;
