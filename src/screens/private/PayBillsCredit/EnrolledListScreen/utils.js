export const maskedLast4digits = (value = '') => {
  const last4digits = value?.slice(-4);
  let maskNumber = '';

  for (var i = value?.length - 4; i > 0; i--) {
    maskNumber += '*';
  }

  return `${maskNumber}${last4digits}`;
};
