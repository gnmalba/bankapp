/**
 * @flow
 */
import type {Biller} from '@ducks/billers/types';

export type PropTypes = {
  onPress(id: string): any,
  enrolledBillers: Array<Biller>,
};

export type CardListType = {
  list: any[],
  onPress?: (account: any) => void,
  dispatchAction?: any,
  balanceLabel: 'AVAILABLE BALANCE' | 'OUTSTANDING BALANCE',
  shouldMask?: boolean,
};
