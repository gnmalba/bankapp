/**
 * @flow
 */
// React, React Native modules
import React from 'react';
import {ScrollView} from 'react-native';

// 3rd Party Modules

// Components
import CardList from './components/CardList';

// Component Flow Types
import type {PropTypes} from './types';
// import {filterBySearchText} from './helpers';

// Component Style
import {MainContainer} from './Styled';
import type {Biller} from '@ducks/billers/types';

function EnrolledListScreen({onPress, enrolledBillers}: PropTypes): React$Node {
  const onPressItem = (account: Biller) => {
    onPress(account.billerId);
  };

  return (
    <MainContainer>
      <ScrollView showsVerticalScrollIndicator={false}>
        <CardList
          list={enrolledBillers}
          onPress={onPressItem}
          balanceLabel="AVAILABLE BALANCE"
        />
      </ScrollView>
    </MainContainer>
  );
}

export default EnrolledListScreen;
