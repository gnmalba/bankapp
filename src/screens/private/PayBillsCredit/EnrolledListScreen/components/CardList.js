/**
 * @format
 * @flow
 */

import Text from '@components/Text';
import React from 'react';
import {Pressable} from 'react-native';
import {
  CardListContainer,
  CardListItem,
  CardListItemBalanceLabel,
  CardListItemLeftSide,
  CardListItemName,
  CardListItemNumber,
  CardListItemRightSide,
  EmptyText,
} from '../Styled';
import type {CardListType} from '../types';
import {useDispatch} from 'react-redux';
// import {maskingFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import {maskedLast4digits} from '../utils';

const CardList = (props: CardListType): React$Node => {
  const {list, onPress, dispatchAction} = props;
  const dispatch = useDispatch();

  const handlePress = (item) => {
    if (onPress) {
      const payload: any = {
        alias: item.name,
        availableBalance: item.availableBalance,
        cardNumber: item.accountNo,
        ...item,
      };
      if (dispatchAction) {
        dispatch(dispatchAction(payload));
      }
      onPress(payload);
    }
  };

  return (
    <CardListContainer>
      {list.map((item, index) => {
        const biller = item.name;
        const maxBillerNameLength = 10;
        const maskedUniqueNumber = maskedLast4digits(item.contactId);
        const truncBillerName =
          biller?.length > maxBillerNameLength
            ? biller.substring(0, maxBillerNameLength - 3) + '...'
            : biller;
        return (
          <Pressable
            key={`${item.id}-${index}`}
            onPress={() => handlePress(item)}
            {...testProps('acctNumContainer_' + item.billerId)}>
            <CardListItem isLastItem={index === list.length - 1}>
              <CardListItemLeftSide>
                <CardListItemName
                  mode="body1"
                  numberOfLines={1}
                  {...testProps('acctNumName_' + item.billerId)}>
                  {item.alias}
                </CardListItemName>
                <CardListItemNumber
                  mode="caption"
                  {...testProps('acctNumMasked_' + item.billerId)}>
                  {`${truncBillerName}(${maskedUniqueNumber})`}
                </CardListItemNumber>
              </CardListItemLeftSide>
              <CardListItemRightSide>
                <CardListItemBalanceLabel
                  mode="overline2"
                  {...testProps('aactNumAvailBalLbl_' + item.billerId)}>
                  Last Transaction
                </CardListItemBalanceLabel>
                <Text
                  mode="body1"
                  {...testProps('acctNumBalValue_' + item.billerId)}>
                  01/01/2021
                </Text>
              </CardListItemRightSide>
            </CardListItem>
          </Pressable>
        );
      })}

      {!list.length && (
        <EmptyText {...testProps('MbCreditScreenSelectiontestID')}>
          No results found.
        </EmptyText>
      )}
    </CardListContainer>
  );
};

export default CardList;
