export const PB_NOT_ENOUGH_FUNDS =
  'You do not have enough funds for this payment.';
export const PC_NOT_ENOUGH_FUNDS =
  'The amount entered exceeds the balance of your current account. Please enter a different amount or choose a different account and try your transaction again.';
export const ELAPSED_DATE_TIME =
  'This date/time has already elapsed. Please enter a future date/time.';
export const ELAPSED_START_DATE_TIME =
  'This date/time has already elapsed. Please enter a different future date/time.';
export const EXCEEDS_THREE_YEARS =
  'This date/time exceeds 3 years from the current date. Please enter a date/time within the next 3 years only.';
export const EARLIER_THAN_START_DATE =
  'This date/time occurs before the start date. Please enter a date/time after the start date.';
export const EXCEEDS_MAXIMUM_OCCURRENCES =
  'The number of occurrences exceeds the maximum period of 3 years from current date.';
export const MINIMUM_OCCURRENCES = 'Please enter occurrence greater than 1.';
export const INVALID_OCCURRENCES_END_DATE =
  'Occurrences should be greater than 1';
export const INVALID_OCCURRENCES =
  'Invalid input. Enter the occurrences in the correct format.';
export const INVALID_ONE_TIME_DATE_TIME =
  'The user manually entered the date and/or time with invalid format';
export const INVALID_REPEATING_DATE_TIME =
  'The user manually entered the payment start date and/or time with invalid format';
export const INVALID_REPEATING_END_DATE =
  'The user manually entered the end date with invalid format';
export const REQUIRED_FIELD = 'is required.';
export const POSITIVE_AMOUNT = 'Please enter a positive amount';
export const INVALID_AMOUNT =
  'Invalid input. Enter the amount in the correct format.';
export const PASSCODE_EXCEED =
  'You have reached the maximum number of attempts. Please try again after (x) minutes.';
export const SYSTEM_ERROR_MESSAGE =
  "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen.";
export const INVALID_NOTES =
  'Invalid input. Enter the notes in the correct format.';
export const INVALID_PASSCODE = 'Passcode is invalid. Please try again.';
