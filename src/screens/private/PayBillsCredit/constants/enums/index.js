export const ACCOUNT_TYPE = {
  DEPOSIT: 0,
  MB_PREPAID: 1,
  CC: 2,
  YAZZ: 3,
};

export const SCHEDULE_TYPE = {
  IMMEDIATE: 'Immediate',
  LATER: 'Later',
};

export const OCCURRENCE_TYPE = {
  ONE_TIME: 'One Time',
  REPEATING: 'Repeating',
};

export const ENDS_TYPE = {
  ON: 'On',
  AFTER: 'After',
};

export const FREQUENCY_TYPE = {
  DAILY: 'Daily',
  WEEKLY: 'Weekly',
  MONTHLY: 'Monthly',
  QUARTERLY: 'Quarterly',
  SEMI_ANNUALLY: 'Semi-annually',
  ANNUALLY: 'Annually',
  // CUSTOM: "Custom",
};

export const FREQUENCY_MOMENT_TYPE = {
  Daily: 'days',
  Weekly: 'week',
  Monthly: 'month',
  Quarterly: 'quarter',
  'Semi-annually': 'year',
  Annually: 'year',
};

export const PRESS_FROM = {
  CREDIT_SOA: 'CreditSOA',
  PAYBILLS_SHORTCUT: 'PayBillsShortcut',
  PAYBILLS_MAIN_SCREEN: 'PayBillsMainScreen',
  BOTTOM_NAVIGATION: 'BottomNavigation',
  BILLER_SCREEN: 'BillerScreen',
};
