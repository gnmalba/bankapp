/**
 * @format
 */

export const DATE_TIME_FORMAT = 'MM/DD/YYYY 00:00 AM / PM';
export const DATE_FORMAT = 'MM/DD/YYYY';
export const DATE_FORMAT_REVIEW_SCREEN = 'MMM-DD-YYYY hh:mm A [(GMT]Z[)]';
