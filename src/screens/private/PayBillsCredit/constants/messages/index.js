export const NO_SAME_CURRENCY =
  'You don not have an account that matches the currency of your payment. Please link a valid account before trying your transaction again.';
export const NO_RESULTS_FOUND = 'No results found.';
export const NO_ENROLLED_BILLER_FOUND = 'No enrolled billers found.';
export const NO_ENROLLED_BILLER =
  'You can enroll a new biller or select the "All Billers" option to see the full list of billers.';
export const NO_APPLICABLE_BILLER =
  'You have no enrolled account that is applicable to pay this biller.';
export const NO_RESULTS_FOUND_BILLER =
  'No enrolled billers found. You can enroll a new biller';
