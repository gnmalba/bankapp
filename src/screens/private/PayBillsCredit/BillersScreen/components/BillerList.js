/**
 * @flow
 */
// React, React Native modules
import AlphabetList from '@composition/AlphabetList';
import BillerCategories from '@composition/BillerCategories';
import SectionList from '@composition/SectionList';
import React, {Fragment} from 'react';
import {BILLER_TYPE, LIST_TYPE} from '../constants';
import type {PbcBillers, Biller} from '@ducks/billers/types';
// import Button from '@components/Button';
import EnrolledListScreen from '../../EnrolledListScreen';

// import {handleListTypeChange} from '../utils';
// import {useBillerScreen} from '../../hooks';

const billerTypes = {
  allBillers: (
    categories,
    data,
    listType,
    onSelectBiller,
    enrolledBillers,
    onSelectCategory,
  ) => (
    <Fragment>
      {allBillers[listType](data, categories, onSelectBiller, onSelectCategory)}
    </Fragment>
  ),
  enrolledBillers: (
    billers,
    categories,
    listType,
    onSelectBiller,
    enrolledBillers,
  ) => (
    <Fragment>
      <EnrolledListScreen
        onPress={onSelectBiller}
        enrolledBillers={enrolledBillers}
      />
    </Fragment>
  ),
};

const allBillers: Object = {
  categoryList: (categories, data, onSelectBiller, onSelectCategory) => (
    <BillerCategories categories={categories} onPress={onSelectCategory} />
  ),
  alphabetList: (categories, data, onSelectBiller) => (
    <AlphabetList data={data.alphabetical} onPress={onSelectBiller} />
  ),
  sectionList: (categories, data, onSelectBiller) => (
    <SectionList data={data.section} onPress={onSelectBiller} />
  ),
};

export type BillerListProps = {
  billers: PbcBillers,
  categories: any,
  enrolledBillers: Array<Biller>,
  listType: $Values<typeof LIST_TYPE>,
  billerType: $Values<typeof BILLER_TYPE>,
  onSelectBiller(id: string): any,
  onSelectCategory(categoryId: string): any,
};

const BillerList = ({
  billers,
  categories,
  listType,
  billerType,
  enrolledBillers,
  onSelectBiller,
  onSelectCategory,
}: BillerListProps): React$Node =>
  billerTypes[billerType](
    billers,
    categories,
    listType,
    onSelectBiller,
    enrolledBillers,
    onSelectCategory,
  );

export default BillerList;
