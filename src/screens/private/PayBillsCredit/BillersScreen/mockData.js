import type {SectionData} from '@composition/AlphabetList/types';
const DATA: Array<SectionData> = [
  {
    title: 'A',
    data: [
      {id: '1', name: 'Test Item', isLastItem: false},
      {id: '2', name: 'Test Item', isLastItem: false},
      {id: '3', name: 'Test Item', isLastItem: false},
      {id: '4', name: 'Test Item', isLastItem: true},
    ],
  },
  {
    title: 'B',
    data: [{id: '5', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'C',
    data: [{id: '6', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'D',
    data: [{id: '7', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'E',
    data: [{id: '8', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'F',
    data: [{id: '9', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'G',
    data: [{id: '10', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'H',
    data: [{id: '11', name: 'Test Item', isLastItem: true}],
  },
  {
    title: 'I',
    data: [
      {id: '12', name: 'Test Item', isLastItem: false},
      {id: '13', name: 'Test Item', isLastItem: false},
      {id: '14', name: 'Test Item', isLastItem: false},
      {id: '15.', name: 'Test Item', isLastItem: true},
    ],
  },
];

const enrolled_billers_test = ['test'];

export {enrolled_billers_test, DATA};
