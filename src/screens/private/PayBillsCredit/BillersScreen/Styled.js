/**
 * @flow
 */

import styled from 'styled-components/native';
import {moderateScale} from '@utils/scale';

import type {ComponentType} from 'react';

const SearchBarContainer: ComponentType<any> = styled.View`
  margin: ${moderateScale(18)}px ${moderateScale(32)}px;
`;
const RadioButtonContainer: ComponentType<any> = styled.View`
  margin: ${moderateScale(20)}px ${moderateScale(32)}px;
`;

const HeaderContainer: ComponentType<any> = styled.View`
  margin: 0px ${moderateScale(32)}px;
  margin-top: ${moderateScale(40)}px;
  margin-bottom: ${moderateScale(22)}px;
`;

export {SearchBarContainer, RadioButtonContainer, HeaderContainer};
