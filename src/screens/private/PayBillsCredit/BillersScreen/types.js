/**
 * @flow
 */

export type PropTypes = {
  navigation: Object,
  formControl: Object,
};

export type BillerRadioProps = {
  onPress: (type: string) => void,
  billerType: string,
};
