/**
 * @flow
 */

import {LIST_TYPE, BILLER_TYPE} from './constants';

export const billerTypes = {
  allBillers: true,
  enrolledBillers: false,
};

const conditions = {
  showAlphabetList: (focused, text) => focused && text === '',
  showSectionList: (focused, text) => focused && text,
  enrolledIsEmpty: (billers) => billers.length <= 0,
  textIsNotEmpty: (props: CondProps) => (props.text ? true : false),
};
export const handleListTypeChange = (
  isFocus: boolean,
  searchText: string,
  setListType: any,
): void => {
  !conditions.textIsNotEmpty({text: searchText}) &&
    setListType(LIST_TYPE.alphabetList);
  conditions.showAlphabetList(isFocus, searchText) &&
    setListType(LIST_TYPE.alphabetList);
  conditions.showSectionList(isFocus, searchText) &&
    setListType(LIST_TYPE.sectionList);
};

export const setInitialBiller = (
  setBillerType: any,
  enrolledBiller: Array<any>,
): void => {
  conditions.enrolledIsEmpty
    ? setBillerType(BILLER_TYPE.allBillers)
    : setBillerType(BILLER_TYPE.enrolledBIllers);
};

type CondProps = {
  text?: string,
  billers?: Array<Object>,
};

const isLenghtNotZero = (_data: Object) => _data.data.length > 0;

export const handleSectionListSearch = ({
  text,
  data,
  setFilteredData,
  handleBlank,
  filterByField = 'name',
}: {
  text: string,
  data: Array<Object>,
  setFilteredData(data: Array<Object>): void,
  handleBlank(): void,
  filterByField?: string,
}) => {
  const filterData = () => {
    const newData = data
      .map((item) => {
        const newBillers = item.data.filter((i) => {
          const itemData = i[filterByField]
            ? i[filterByField]?.toUpperCase()
            : ''.toUpperCase();
          const textData = text?.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });
        return {
          ...item,
          data: newBillers,
        };
      })
      .filter(isLenghtNotZero);
    setFilteredData(newData);
  };
  conditions.textIsNotEmpty({text}) ? filterData() : handleBlank();
};

export const handleBillerSelect = (
  id: string,
  billerType: string,
  setBiller: any,
  setFieldValue: any,
): void => setBiller(id, billerType, setFieldValue);

export const handleOnChangeSearch = (
  onTextChange: any,
  setList: any,
  sourceData: Array<any>,
  billerType: string,
  value: string = '',
): void => {
  const searchConds = {
    allBillers: () =>
      handleSectionListSearch({
        text: value,
        data: sourceData,
        setFilteredData: (data) => {
          setList(data);
          onTextChange(value);
        },
        handleBlank: () => {
          setList(sourceData);
          onTextChange(value);
        },
      }),
    enrolledBillers: () => {
      onTextChange(value);
      setList(filterEnrolledBillers(sourceData, value));
    },
  };

  searchConds[billerType]();
};

export const filterEnrolledBillers = (
  enrolledBillers: any[],
  searchText: string = '',
): Object[] => {
  return enrolledBillers.filter((enrolledBiller: Object) => {
    if (searchText) {
      return enrolledBiller.alias
        .toLowerCase()
        .includes(searchText.toLowerCase());
    }

    return true;
  });
};
