export const BILLER_TYPE = {
  allBillers: 'allBillers',
  enrolledBillers: 'enrolledBillers',
};

export const LIST_TYPE = {
  sectionList: 'sectionList',
  alphabetList: 'alphabetList',
  categoryList: 'categoryList',
};
