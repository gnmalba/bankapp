/**
 * @flow
 */
// React, React Native modules
import React from 'react';

// 3rd Party Modules

// Components
import Layout from '@components/Layout';
// import Button from '@components/Button';
import Checkbox from '@components/Checkbox';
import SearchBox from '@composition/SearchBox';
import Text from '@components/Text';

// Third party libraries
// Component Flow Types
import type {BillerRadioProps, PropTypes} from './types';

// Component Style
import {
  SearchBarContainer,
  RadioButtonContainer,
  HeaderContainer,
} from './Styled';

import ProcessingScreen from '@screens/common/ProcessingScreen';

//utils
import {BILLER_TYPE, LIST_TYPE} from './constants';
import {BillerList} from './components';
import {useBillerScreen, useBillers} from '../hooks';

import type {Biller, AllBillers} from '@ducks/billers/types';
import {handleOnChangeSearch, handleBillerSelect} from './utils';
// Third party libraries

const Header = () => (
  <HeaderContainer>
    <Text mode="h2">Select a type of biller.</Text>
  </HeaderContainer>
);

//replace w/radio button
const SelectBillerType = ({onPress, billerType}: BillerRadioProps) => (
  <RadioButtonContainer>
    <Checkbox
      title="Enrolled"
      onPress={() => {
        onPress(BILLER_TYPE.enrolledBillers);
      }}
      checked={BILLER_TYPE.enrolledBillers === billerType} // always true
      isCheckbox={false}
      accessibilityLabel="BillerScreenEnrolledOption"
    />
    <Checkbox
      title="All Biller"
      onPress={() => {
        onPress(BILLER_TYPE.allBillers);
      }}
      checked={BILLER_TYPE.allBillers === billerType} // always true
      isCheckbox={false}
      accessibilityLabel="BillerScreenAllBillersOption"
    />
  </RadioButtonContainer>
);

const MainScreen = ({
  setBillerType,
  setListType,
  setSearchText,
  searchText,
  handleFocus,
  handleBlur,
  allBillers,
  categories,
  listType,
  billerType,
  onSelectBiller,
  enrolledBillers,
  onSelectCategory,
}: MainScreenProps) => (
  <Layout>
    <Header />
    <SelectBillerType onPress={setBillerType} billerType={billerType} />
    <SearchBarContainer>
      <SearchBox
        placeholder="Search"
        onChange={setSearchText}
        value={searchText}
        setValue={setSearchText}
        onFocus={handleFocus}
        onBlur={handleBlur}
      />
    </SearchBarContainer>
    <BillerList
      enrolledBillers={enrolledBillers}
      listType={listType}
      billers={allBillers}
      billerType={billerType}
      categories={categories}
      onSelectBiller={onSelectBiller}
      onSelectCategory={onSelectCategory}
    />
  </Layout>
);

export type MainScreenProps = {
  searchText: string,
  listType: $Values<typeof LIST_TYPE>,
  billerType: $Values<typeof BILLER_TYPE>,
  setSearchText(text: string): any,
  setBillerType: any,
  setListType: any,
  handleFocus: any,
  handleBlur: any,
  allBillers: AllBillers,
  categories: any,
  enrolledBillers: Array<Biller>,
  onSelectBiller(id: string): any,
  onSelectCategory(categoryId: string): any,
};

const renderConditions = {
  loading: (screenProps: MainScreenProps) => (
    <ProcessingScreen hideLabel={true} />
  ),
  mainScreen: (screenProps: MainScreenProps) => (
    <MainScreen
      enrolledBillers={screenProps.enrolledBillers}
      setBillerType={screenProps.setBillerType}
      setListType={screenProps.setListType}
      setSearchText={screenProps.setSearchText}
      searchText={screenProps.searchText}
      handleBlur={screenProps.handleBlur}
      handleFocus={screenProps.handleFocus}
      allBillers={screenProps.allBillers}
      categories={screenProps.categories}
      listType={screenProps.listType}
      billerType={screenProps.billerType}
      onSelectBiller={screenProps.onSelectBiller}
      onSelectCategory={screenProps.onSelectCategory}
    />
  ),
};
function BillersScreen({navigation, formControl}: PropTypes): React$Node {
  const {setFieldValue} = formControl;
  const {
    loading,
    allBillers,
    categories,
    enrolledBillers,
    setBiller,
    setCategory,
    categoryName,
  } = useBillers();

  const {
    allBillers: _allBllersBySection,
    enrolledBillers: _enrolledBillers,
    searchText,
    listType,
    billerType,
    setSearchText: onTextChange,
    setFocus,
    setBillerType,
    setListType,
    setAllBillers,
    setEnrolledBillers,
  } = useBillerScreen(
    allBillers.section,
    enrolledBillers,
    setCategory,
    categoryName,
  );

  const whatToRender = loading ? 'loading' : 'mainScreen';

  const handleFocus = () => setFocus(true);
  const handleBlur = () => setFocus(false);

  const newBillers: AllBillers = {
    alphabetical: allBillers.alphabetical,
    section: _allBllersBySection,
  };

  const sourceDataConds = {
    allBillers: allBillers.section,
    enrolledBillers: enrolledBillers,
  };
  const setDataListConds = {
    allBillers: setAllBillers,
    enrolledBillers: setEnrolledBillers,
  };

  const handleCategoryChange = (id: string) => {
    setCategory(id);
  };

  return renderConditions[whatToRender]({
    setBillerType,
    setListType,
    setSearchText: (text: string) =>
      handleOnChangeSearch(
        onTextChange,
        setDataListConds[billerType],
        sourceDataConds[billerType],
        billerType,
        text,
      ),
    searchText,
    handleFocus,
    handleBlur,
    allBillers: newBillers,
    categories,
    listType,
    billerType,
    enrolledBillers: _enrolledBillers,
    onSelectBiller: (id: string) =>
      handleBillerSelect(id, billerType, setBiller, setFieldValue),
    onSelectCategory: handleCategoryChange,
  });
}

export default BillersScreen;
