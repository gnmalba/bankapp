/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';

import {moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
`;

export const LinkText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const LinkTextHref: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 700;
  flex-wrap: wrap;
`;
