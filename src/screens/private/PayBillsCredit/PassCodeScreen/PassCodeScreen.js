/**
 * @flow
 */
// React, React Native modules
import React, {useState, useEffect} from 'react';
import {View} from 'react-native';

import Passcode from '@screens/common/Passcode';
import {testProps} from '@utils/componentCustomProps';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import {LinkTextHref, LinkText} from './Styled';

//hooks
import {usePayBillsCredit} from '../hooks';
import ProcessingScreen from '@screens/common/ProcessingScreen';
import {ERROR_STATUS} from '@ducks/pay-bills-credit/constants';

function PassCodeScreen({navigation, formControl}: PropTypes): React$Node {
  const [pinCode, setPinCode] = useState('');
  const {values} = formControl;
  const {
    payImmediate,
    payScheduled,
    selectedBiller,
    loading,
    selectedSourceAccount,
    postRequestResponse,
  } = usePayBillsCredit();
  const {error} = postRequestResponse;
  const [errorMessage, setErrorMessage] = useState('');

  const {message} = error || {message: ''};

  useEffect(() => {
    setErrorMessage(message);
  }, [error, message]);

  if (loading) {
    return <ProcessingScreen label="Processing Request" />;
  }

  const handleSetPasscode = (value: string = '') => {
    setPinCode(value);
    setErrorMessage('');
  };

  const scheduleTypeConds = {
    Immediate: () =>
      payImmediate({
        account: selectedSourceAccount,
        biller: selectedBiller,
        formValues: values,
        pinCode,
      }),
    Later: () =>
      payScheduled({
        account: selectedSourceAccount,
        biller: selectedBiller,
        formValues: values,
        pinCode,
      }),
  };

  const handleSubmit = () =>
    scheduleTypeConds[formControl.values.scheduleType]();

  return (
    <>
      <Passcode
        password
        passcodeError={errorMessage}
        passcode={pinCode}
        onTextChange={handleSetPasscode}
        onSubmit={handleSubmit}
        title="Please enter your passcode."
        description="To proceed, please enter your 6-digit passcode"
        btnTitle="Next"
        spaceFromBottom={220}
        helperContent={
          <View>
            <LinkText {...testProps('passcodeScreenMessage')}>
              If you forgot passcode, please call our hotline{' '}
              <LinkTextHref>{ERROR_STATUS.PHONE_NO}</LinkTextHref> for further
              assistance.
            </LinkText>
          </View>
        }
      />
    </>
  );
}

export default PassCodeScreen;
