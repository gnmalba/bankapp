// @flow
import Button from '@components/Button';
import {ERROR_TYPES} from '@ducks/pay-bills-credit/constants';
import ErrorScreen from '@screens/common/ErrorScreen';
import React from 'react';
import {ButtonHolder} from './Styled';

const MaximumPasscodeScreen = ({
  route,
  navigation,
}: {
  route: Object,
  navigation: Object,
}): React$Node => {
  const params = route?.params;

  const isSystemError = params.errorType === ERROR_TYPES.SYSTEM_ERROR;

  return (
    <>
      <ErrorScreen
        isSystemError={isSystemError}
        heading=" You have reached the maximum number of attempts. Please try again after(x) minutes."
        headingAlignCenter
      />
      <ButtonHolder>
        <Button
          title="Go back"
          type="primary"
          state="default"
          onPress={() => navigation.goBack()}
        />
      </ButtonHolder>
    </>
  );
};

export default MaximumPasscodeScreen;
