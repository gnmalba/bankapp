/**
 * @flow
 */

import {verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View``;

export const ButtonHolder: ComponentType<any> = styled.View`
  margin-top: 100%;
  padding: ${verticalScale(32)}px;
`;
