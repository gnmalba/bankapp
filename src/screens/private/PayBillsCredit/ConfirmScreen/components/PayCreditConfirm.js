/**
 *
 * @flow
 */
// React Modules
import React from 'react';

// Components
import ReviewItem from '@composition/InstallmentDetail';
import Divider from '@components/Divider';
import {PaymentScheduleReviewItem} from '../../components';

//Utils
import {toText, toCard} from '@utils/helpers';
import {getCardNumber} from '@utils/accounts';
import {accountNumberFormat} from '../../utils/';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatClean} from '@utils/numberInputs';
import {maskingFormat} from '@utils/accountNumber';

// Hooks

// Types
import type {PropsType} from './types';

// Component Style
import * as S from '../Styled';

function PayCreditConfirm({formControl}: PropsType): React$Node {
  const {values} = formControl;
  const {amount, biller, sourceAccount, notes, paymentFields} = values;
  const cardNo = Object.values(paymentFields) || '';
  return (
    <S.ContentContainer>
      <ReviewItem
        label="FROM"
        value={toCard(
          sourceAccount?.alias,
          accountNumberFormat(
            getCardNumber(sourceAccount?.accountType, sourceAccount),
            sourceAccount?.accountType !== 2 ? false : true,
          ),
          'body1',
          'caption',
          'PBCFrom',
        )}
        alignItems="flex-start"
        {...testProps('PBCFrom')}
      />

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <ReviewItem
        label="AMOUNT"
        value={toText(
          `${sourceAccount?.currencyCode || 'PHP'} ${numberFormatClean(
            amount,
          )}`,
          'PBCConfirmAmount',
        )}
        {...testProps('PBCConfirmAmount')}
      />

      <ReviewItem
        label="BILLER"
        value={toCard(
          biller?.alias || biller?.name,
          biller?.category?.name.toUpperCase(),
          'body1',
          'caption',
          'PBCFrom',
        )}
        alignItems="flex-start"
        {...testProps('PBCBiller')}
      />

      <ReviewItem
        label="CARD NO"
        value={toText(maskingFormat(cardNo[0], true), 'PBCConfirmAmount')}
        {...testProps('PBCConfirmAmount')}
      />

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <PaymentScheduleReviewItem formControl={formControl} />

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <ReviewItem
        label="NOTES"
        value={toText(notes, 'PBCConfirmNotes')}
        {...testProps('PBCConfirmNotes')}
      />
    </S.ContentContainer>
  );
}

export default PayCreditConfirm;
