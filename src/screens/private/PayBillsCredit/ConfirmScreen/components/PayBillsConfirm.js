/**
 *
 * @flow
 */
// React Modules
import React from 'react';

// Components
import ReviewItem from '@composition/InstallmentDetail';
import {
  PaymentScheduleReviewItem,
  PaymentFieldsReviewItem,
} from '../../components';

import Divider from '@components/Divider';

// Utils
import {toText, toCard} from '@utils/helpers';
import {getCardNumber} from '@utils/accounts';
import {accountNumberFormat} from '../../utils/';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatClean} from '@utils/numberInputs';

// Types
import type {PropsType} from './types';

// Component Style
import * as S from '../Styled';

function PayBillsConfirm({formControl}: PropsType): React$Node {
  const {values} = formControl;
  const {amount, sourceAccount, notes, biller} = values;

  return (
    <S.ContentContainer>
      {/* <Text>{JSON.stringify(values, null, 2)}</Text> */}

      <ReviewItem
        label="FROM"
        value={toCard(
          sourceAccount?.alias,
          accountNumberFormat(
            getCardNumber(sourceAccount?.accountType, sourceAccount),
            sourceAccount?.accountType !== 2 ? false : true,
          ),
          'body1',
          'caption',
          'PBCFrom',
        )}
        alignItems="flex-start"
        {...testProps('PBCFrom')}
      />

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <ReviewItem
        label="AMOUNT"
        value={toText(
          `${sourceAccount?.currencyCode || 'PHP'} ${numberFormatClean(
            amount,
          )}`,
          'PBCConfirmAmount',
        )}
        {...testProps('PBCConfirmAmount')}
      />

      {/* TO-DO: Integrate once given by BE*/}
      {/* <ReviewItem
        label="FEE"
        value={toText('0.00', 'PBCConfirmFee')}
        {...testProps('PBCConfirmFee')}
      /> */}

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <ReviewItem
        label="BILLER"
        value={toCard(
          biller?.alias || biller?.name,
          biller?.category?.name?.toUpperCase(),
          'body1',
          'caption',
          'PBCFrom',
        )}
        alignItems="flex-start"
        {...testProps('PBCFrom')}
      />
      <PaymentFieldsReviewItem formControl={formControl} />

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <PaymentScheduleReviewItem formControl={formControl} />

      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>

      <ReviewItem
        label="NOTES"
        value={toText(notes, 'PBCConfirmNotes')}
        {...testProps('PBCConfirmNotes')}
      />
    </S.ContentContainer>
  );
}

export default PayBillsConfirm;
