/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {ScrollView} from 'react-native';

// Third party libraries
import styled from 'styled-components/native';

// Utils
import {scale, verticalScale} from '@utils/scale';

export const ScrollContainer: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    showsVerticalScrollIndicator: false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
  }),
)``;

export const MainContainer: ComponentType<any> = styled.View`
  justify-content: center;
  padding-horizontal: ${scale(32)}px;
  padding-top: ${verticalScale(32)}px;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-vertical: ${verticalScale(32)}px;
`;

export const ContentContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(24)}px;
`;

export const TitleText: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(8)}px;
`;

export const DividerContainer: ComponentType<any> = styled.View`
  margin-vertical: ${verticalScale(8)}px;
`;
