/**
 * @flow
 */
// React, React Native modules
import React from 'react';

// 3rd Party Modules
import {useSelector} from 'react-redux';

// Components
import Layout from '@components/Layout';
import Button from '@components/Button';
import {PayBillsConfirm, PayCreditConfirm} from './components';
import Text from '@components/Text';
import RenderWhen from '../components/RenderWhen';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import * as S from './Styled';

// Utils
import {payBillsCreditSelector} from '@ducks/pay-bills-credit/selector';

function Header(): React$Node {
  return (
    <S.ContentContainer>
      <S.TitleText>
        <Text mode="h2">All Good?</Text>
      </S.TitleText>
      <Text mode="body2" themeColor="TEXT_100_HIGH">
        Please review your details.
      </Text>
    </S.ContentContainer>
  );
}
function ConfirmScreen({navigation, formControl}: PropTypes): React$Node {
  const navigate = (routeName) => () => {
    navigation.navigate(routeName);
  };
  const payBillsCreditState = useSelector(payBillsCreditSelector); // waiting for the isPayCredit state
  const {isPayBillsCredit} = payBillsCreditState;

  return (
    <Layout>
      <S.ScrollContainer>
        <S.MainContainer>
          <Header />

          <RenderWhen condition={isPayBillsCredit}>
            <PayCreditConfirm formControl={formControl} />
          </RenderWhen>
          <RenderWhen condition={!isPayBillsCredit}>
            <PayBillsConfirm formControl={formControl} />
          </RenderWhen>

          <S.ButtonContainer>
            <Button
              title="Confirm"
              state="default"
              type="primary"
              size="large"
              onPress={navigate('PassCodeScreen')}
            />
          </S.ButtonContainer>
        </S.MainContainer>
      </S.ScrollContainer>
    </Layout>
  );
}

export default ConfirmScreen;
