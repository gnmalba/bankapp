/**
 * @format
 * @flow
 */

import Text from '@components/Text';
import React from 'react';
import {Pressable} from 'react-native';
import {
  CardListContainer,
  CardListItem,
  CardListItemBalanceLabel,
  CardListItemLeftSide,
  CardListItemName,
  CardListItemNumber,
  CardListItemRightSide,
  EmptyText,
} from '../Styled';
import type {CardListType} from '../types';
import {useDispatch} from 'react-redux';
import {maskingFormat} from '@utils/accountNumber';
import {numberToCurrencyFormat} from '@ducks/deposit/utils';
import {testProps} from '@utils/componentCustomProps';
import {getLastFourDigits} from '@utils/getLastFourDigits';

const formatAmount = (currencyType: string, amount: number): string => {
  return `${currencyType || ''} ${numberToCurrencyFormat(amount || 0)}`;
};

const CardList = (props: CardListType): React$Node => {
  const {list, onPress, dispatchAction, shouldMask = true} = props;
  const dispatch = useDispatch();

  const handlePress = (item) => {
    if (onPress) {
      const payload: any = {
        alias: item.name,
        availableBalance: item.availableBalance,
        cardNumber: item.accountNo,
        ...item,
      };
      if (dispatchAction) {
        dispatch(dispatchAction(payload));
      }
      onPress(payload);
    }
  };

  return (
    <CardListContainer>
      {list.map((item, index) => {
        const cardNum = item.accountNo ? item.accountNo : item.cardNo;
        const lastFourDigits = getLastFourDigits(cardNum);
        const maskCardNo =
          item.accountType >= 2 ? maskingFormat(cardNum, shouldMask) : cardNum;
        return (
          <Pressable
            key={`${item.id}-${index}`}
            onPress={() => handlePress(item)}
            {...testProps('acctNumContainer_' + lastFourDigits)}>
            <CardListItem isLastItem={index === list.length - 1}>
              <CardListItemLeftSide>
                <CardListItemName
                  mode="body1"
                  numberOfLines={1}
                  {...testProps('acctNumName_' + lastFourDigits)}>
                  {item.alias}
                </CardListItemName>
                <CardListItemNumber
                  mode="caption"
                  {...testProps('acctNumMasked_' + lastFourDigits)}>
                  {maskCardNo}
                </CardListItemNumber>
              </CardListItemLeftSide>
              <CardListItemRightSide>
                <CardListItemBalanceLabel
                  mode="overline2"
                  {...testProps('aactNumAvailBalLbl_' + lastFourDigits)}>
                  {props.balanceLabel}
                </CardListItemBalanceLabel>
                <Text
                  mode="body1"
                  {...testProps('acctNumBalValue_' + lastFourDigits)}>
                  {formatAmount(
                    item.currencyCode,
                    item.availableCredit || item.availableBalance,
                  )}
                </Text>
              </CardListItemRightSide>
            </CardListItem>
          </Pressable>
        );
      })}

      {!list.length && (
        <EmptyText {...testProps('MbCreditScreenSelectiontestID')}>
          No results found.
        </EmptyText>
      )}
    </CardListContainer>
  );
};

export default CardList;
