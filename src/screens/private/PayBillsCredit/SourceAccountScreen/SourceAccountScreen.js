/**
 * @flow
 */
// React, React Native modules
import React, {useEffect, useState} from 'react';
import {ScrollView} from 'react-native';
import {useSelector} from 'react-redux';

// 3rd Party Modules

// Components
import CardList from './components/CardList';
import Layout from '@components/Layout';
import Separator from '@components/Separator';
import Text from '@components/Text';

import SearchBox from '@composition/SearchBox';

// Utils
import {installmentsSelector} from '@ducks/installments/selectors';
import type {Account} from '@ducks/pay-bills-credit/types';
// import {toCardList} from '@utils/installments';

// Component Flow Types
import type {PropTypes} from './types';
import {filterBySearchText} from './helpers';

// Component Style
import {HeaderContainer} from './Styled';

//hooks
import {usePayBillsCredit} from '../hooks';

function SourceAccountScreen({navigation, formControl}: PropTypes): React$Node {
  const {
    allSourceAccounts,
    isPayCredit,
    setSourceAccount,
  } = usePayBillsCredit();
  const [list, setList] = useState(allSourceAccounts);
  const [searchText, setSearchText] = useState('');
  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType} = installmentsState;
  const {setFieldValue} = formControl;

  useEffect(() => {
    if (isPayCredit) {
      filterSource();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPayCredit]);

  const onPress = (account: Account) => {
    setFieldValue('sourceAccount', account);
    setSourceAccount(account);
  };

  const handleOnChangeSearch = (value: string = ''): void => {
    setSearchText(value);
    setList(filterBySearchText(allSourceAccounts, value));
  };

  const filterSource = () => {
    const filterSourceAccounts = allSourceAccounts.filter(
      (item) => item.accountType !== 2,
    );
    setList(filterSourceAccounts);
  };

  return (
    <Layout padding={32}>
      <HeaderContainer marginBottom={41}>
        <Text mode="body1">Select a source account.</Text>
      </HeaderContainer>
      <SearchBox
        placeholder={'Search'}
        onChange={handleOnChangeSearch}
        setValue={handleOnChangeSearch}
        value={searchText}
      />
      <Separator space={12} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <CardList
          list={list}
          onPress={onPress}
          balanceLabel={
            currentInstallmentType === 'BC'
              ? 'OUTSTANDING BALANCE'
              : 'AVAILABLE BALANCE'
          }
        />
      </ScrollView>
    </Layout>
  );
}

export default SourceAccountScreen;
