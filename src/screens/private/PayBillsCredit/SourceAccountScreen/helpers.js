// @flow

export const filterBySearchText = (
  accounts: any[],
  searchText: string = '',
): Object[] => {
  return accounts.filter((account: Object) => {
    if (searchText) {
      const accountNo = account.accountNo
        ? account.accountNo.includes(searchText)
        : null;
      return (
        account.alias.toLowerCase().includes(searchText.toLowerCase()) ||
        accountNo
      );
    }

    return true;
  });
};
