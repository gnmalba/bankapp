/**
 * @flow
 */
// React, React Native modules
import React from 'react';

// 3rd Party Modules

import ProcessingScreen from '@screens/common/ProcessingScreen';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import {} from './Styled';

function LoadingScreen({navigation}: PropTypes): React$Node {
  return <ProcessingScreen label="Processing Request" />;
}

export default LoadingScreen;
