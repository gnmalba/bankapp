/**
 * @format
 * @flow
 */
// React modules
import React from 'react';

// Components
import Text from '@components/Text';
import Divider from '@components/Divider';
import ReviewItem from '@composition/InstallmentDetail';
import {
  PaymentScheduleReviewItem,
  PaymentFieldsReviewItem,
} from '../../components';

// Utils
import {toText, toCard} from '@utils/helpers';
import {getCardNumber} from '@utils/accounts';
import {accountNumberFormat} from '../../utils/';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatClean} from '@utils/numberInputs';

// Types
import type {PropTypes} from './types';

// Selectors
import {useSelector} from 'react-redux';
import {payBillsCreditResponseSelector} from '@ducks/pay-bills-credit/selector';

import * as S from '../Styled';

function PayCreditEndTransaction({formControl}: PropTypes): React$Node {
  const {values} = formControl;
  const {amount, sourceAccount, notes, biller} = values;
  const textStyle = {textAlign: 'center'};
  const {referenceNumber} = useSelector(payBillsCreditResponseSelector);

  return (
    <>
      <S.TextContainer>
        <Text mode="body2" themeColor="TEXT_100_HIGH" style={textStyle}>
          You will be notified once this transaction is successful. Thank you
          for using Metrobank Online.
        </Text>
      </S.TextContainer>
      <S.EndOfTransactionDetailsContainter>
        <ReviewItem
          label="FROM"
          value={toCard(
            sourceAccount?.alias,
            accountNumberFormat(
              getCardNumber(sourceAccount?.accountType, sourceAccount),
              true,
            ),
            'body1',
            'caption',
            'PBCFrom',
          )}
          alignItems="flex-start"
          {...testProps('PBCFrom')}
        />
        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          label="AMOUNT"
          value={toText(
            `${sourceAccount?.currencyCode || 'PHP'} ${numberFormatClean(
              amount,
            )}`,
            'PBCConfirmAmount',
          )}
          {...testProps('PBCConfirmAmount')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          label="BILLER"
          value={toCard(
            biller?.alias || biller?.name,
            biller?.category?.name?.toUpperCase(),
            'body1',
            'caption',
            'PBCFrom',
          )}
          alignItems="flex-start"
          {...testProps('PBCBiller')}
        />

        <PaymentFieldsReviewItem formControl={formControl} shouldMask={true} />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <PaymentScheduleReviewItem formControl={formControl} />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          label="NOTES"
          value={toText(notes, 'PBCConfirmNotes')}
          {...testProps('PBCConfirmNotes')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          label="REFERENCE NO."
          value={toText(referenceNumber, 'PBCReferenceNum')}
          {...testProps('PBCReferenceNum')}
        />
      </S.EndOfTransactionDetailsContainter>
    </>
  );
}

export default PayCreditEndTransaction;
