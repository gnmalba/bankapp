/**
 *
 * @flow
 */

export type PropTypes = {
  formControl: Object,
};
