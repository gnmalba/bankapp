/**
 * @flow
 */

export type PropTypes = {
  navigation: Object,
  formControl: Object,
};
