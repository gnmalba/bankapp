/**
 * @flow
 */
// React, React Native modules
import React from 'react';
import {Pressable, InteractionManager} from 'react-native';

// 3rd Party Modules
import {useSelector, useDispatch} from 'react-redux';

// Components
import Layout from '@components/Layout';
import Text from '@components/Text';
import RenderWhen from '../components/RenderWhen';
import {PayBillsEndTransaction, PayCreditEndTransaction} from './components/';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import * as S from './Styled';

// Utils
import {SHORTCUT_ICONS} from '@utils/constants';
import {payBillsCreditSelector} from '@ducks/pay-bills-credit/selector';

import {resetBillers} from '@ducks/billers/actions';
import {resetPayBillsCredit} from '@ducks/pay-bills-credit/actions';
import {selectedBillerTypeSelector} from '@ducks/billers/selector';

function Header({navigate}) {
  return (
    <S.HeaderContainer>
      <S.IconContainer>
        <Pressable onPress={navigate('dashboard')}>
          <S.CloseButton />
        </Pressable>
      </S.IconContainer>
      <S.TextContainer>
        <Text mode="h3">Your transaction is being processed.</Text>
      </S.TextContainer>
    </S.HeaderContainer>
  );
}

function EndOfTransactionScreen({
  navigation,
  formControl,
}: PropTypes): React$Node {
  const dispatch = useDispatch();
  const payBillsCreditState = useSelector(payBillsCreditSelector); // waiting for the isPayCredit state
  const selectedBillerType = useSelector(selectedBillerTypeSelector); // waiting for the isPayCredit state
  const {isPayBillsCredit} = payBillsCreditState;

  const navigate = (routeName) => () => {
    if (
      routeName === 'dashboard' &&
      selectedBillerType === 'allBillers' &&
      !payBillsCreditState.isPayBillsCredit
    ) {
      dispatch({type: 'SHOW_ENROLL_BILLER_PROMPT'});
    }

    dispatch(resetBillers());
    dispatch(resetPayBillsCredit());
    InteractionManager.runAfterInteractions(() => {
      formControl.resetForm({});
      navigation.navigate(routeName);
      navigation.popToTop();
    });
  };

  return (
    <Layout>
      <S.ScrollContainer>
        <S.Container>
          <Header navigate={navigate} />
          <RenderWhen condition={isPayBillsCredit}>
            <PayCreditEndTransaction formControl={formControl} />
          </RenderWhen>
          <RenderWhen condition={!isPayBillsCredit}>
            <PayBillsEndTransaction formControl={formControl} />
          </RenderWhen>
          <Pressable onPress={navigate('MainScreen')}>
            <S.PayAnotherBillContainer>
              <S.Icon source={SHORTCUT_ICONS.PAY_BILLS} />
              <Text mode="body2">Pay another bill</Text>
            </S.PayAnotherBillContainer>
          </Pressable>
        </S.Container>
      </S.ScrollContainer>
    </Layout>
  );
}

export default EndOfTransactionScreen;
