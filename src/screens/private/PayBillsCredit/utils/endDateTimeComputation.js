//Third-party libraries
import moment from 'moment';
import {FREQUENCY_TYPE} from '../constants/enums';
import {DATE_FORMAT_REVIEW_SCREEN} from '../constants/formats';

const getDate = (givenDate, occurrences, dateTypeToAdd) => {
  return `${moment(givenDate)
    .add(occurrences, dateTypeToAdd)
    .format(DATE_FORMAT_REVIEW_SCREEN)}`;
};

const endDateTimeComputation = (startDate, occurrences, frequencyType) => {
  switch (frequencyType.value) {
    case FREQUENCY_TYPE.DAILY:
      return getDate(startDate, occurrences, 'd');
    case FREQUENCY_TYPE.WEEKLY:
      return getDate(startDate, occurrences, 'w');
    case FREQUENCY_TYPE.MONTHLY:
      return getDate(startDate, occurrences, 'M');
    case FREQUENCY_TYPE.QUARTERLY:
      return getDate(startDate, occurrences, 'Q');
    case FREQUENCY_TYPE.SEMI_ANNUALLY:
      return getDate(startDate, 6 * occurrences, 'M');
    case FREQUENCY_TYPE.ANNUALLY:
      return getDate(startDate, occurrences, 'y');
    default:
      return '';
  }
};

export default endDateTimeComputation;
