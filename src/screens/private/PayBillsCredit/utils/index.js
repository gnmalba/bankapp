import Moment from 'moment';
import {extendMoment} from 'moment-range';
import {ACCOUNT_TYPE, FREQUENCY_MOMENT_TYPE} from '../constants/enums';

import {accountsFormat16, maskingFormat} from '@utils/accountNumber';

const moment = extendMoment(Moment);

export const getDisabledDates = (date) => {
  const startDate = moment().format('MM/DD/yyyy, 00:00:00').toString();
  const endDate = moment().add(3, 'y').toString();
  return !moment(date).isBetween(startDate, endDate);
};

export const getCardNumber = (type, account) => {
  if ([ACCOUNT_TYPE.DEPOSIT, ACCOUNT_TYPE.MB_PREPAID].includes(type)) {
    return account.accountNo;
  }
  return account.cardNo;
};

export const formatCardNumber = (accountType, cardNumber) => {
  if (accountType === ACCOUNT_TYPE.CC) {
    return accountsFormat16(cardNumber);
  } else if (accountType === ACCOUNT_TYPE.DEPOSIT) {
    return maskingFormat(cardNumber, true, false);
  }
  return cardNumber;
};

export const accountNumberFormat = (
  value = '',
  mask = false,
  isAccountDetail = false,
) => {
  if (mask) {
    const last4digits = value?.slice(-4);
    let maskNumber = '';

    for (var i = value?.length - 4; i > 0; i--) {
      maskNumber += '*';
    }

    const number = `${maskNumber}${last4digits}`;
    const chuncks = number.match(/.{1,4}/g) || [];

    return chuncks.join(' ');
  } else {
    if (isAccountDetail) {
      const chuncks = value.match(/.{1,4}/g) || [];
      return chuncks.join(' ');
    }

    const last4digits = value?.slice(-4);
    const chuncks = value?.slice(0, -4).match(/.{1,3}/g) || [];
    return `${chuncks.join(' ')} ${last4digits}`;
  }
};

export const sortByPosition = (fieldA, fieldB): number => {
  if (fieldA.position && fieldB.position) {
    return fieldB.position - fieldA.position;
  }

  return 0;
};
export const isSubscriberNumber = (name: string = '') => {
  return name.toLowerCase().includes('subscribernumber' || 'referencenumber');
};

export const getAvailableFunds = (sourceAccount): TSourceAccount | null => {
  if (sourceAccount) {
    const {accountType} = sourceAccount;

    if ([ACCOUNT_TYPE.DEPOSIT, ACCOUNT_TYPE.MB_PREPAID].includes(accountType)) {
      return sourceAccount.availableBalance;
    } else if ([ACCOUNT_TYPE.CC, ACCOUNT_TYPE.YAZZ].includes(accountType)) {
      return sourceAccount.availableCredit;
    }
  }

  return null;
};

export const isExceedsThreeYears = (date) => {
  const dateTodayPlusThreeYears = moment().add(3, 'y');

  if (moment(date).isValid()) {
    if (moment(date).isAfter(dateTodayPlusThreeYears)) {
      return true;
    }
    return false;
  }
  return false;
};

export const isElapsedDate = (date) => {
  const dateToday = moment().format('MM/DD/yyyy hh:mm A');

  if (moment(date).isValid()) {
    /*
     * *https://yondu.atlassian.net/browse/MMA-2508*
     * if date === date/time today consider it as elapsed date/time
     */
    if (moment(date).isBefore(dateToday) || moment(date).isSame(dateToday)) {
      return true;
    }
    return false;
  }
  return false;
};

export const checkFormatDateTimeInput = (type, dateTime) => {
  // console.log(moment(dateTime, DATE_FORMAT).format(DATE_FORMAT) === dateTime);
  const DATE_FORMAT = type === 'dateTime' ? 'MM/DD/yyyy hh:mm A' : 'MM/DD/yyyy';
  if (dateTime) {
    return moment(dateTime, DATE_FORMAT).format(DATE_FORMAT) === dateTime;
  }
  return true;
};
export const isEndDateBeforeStartDate = (startDate, endDate) => {
  if (moment(startDate).isValid() && moment(endDate).isValid()) {
    if (moment(moment(endDate)).isBefore(startDate)) {
      return true;
    }
    return false;
  }
  return false;
};

export const getMaxOccurrences = (
  frequencyType,
  startDateTime = null,
  endDate = null,
) => {
  let startDate = moment().format('MM/DD/yyyy, hh:mm A');
  let startDatePlusThreeYears = moment().add(3, 'y');
  const rangeBy = FREQUENCY_MOMENT_TYPE[frequencyType];
  const range = moment().range(
    moment(startDateTime).isValid() ? startDateTime : startDate,
    moment(endDate).isValid()
      ? moment(`${endDate}, ${moment(startDateTime).format('hh:mm A')}`)
      : startDatePlusThreeYears,
  );
  const occurrences = Array.from(range.by(rangeBy));

  if (frequencyType === 'Semi-annually') {
    return (occurrences?.length - 1) * 2;
  }
  return occurrences?.length - 1;
};
