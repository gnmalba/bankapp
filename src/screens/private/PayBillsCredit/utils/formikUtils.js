const ERROR_NULL_VALUE = '';

export const getFieldTouched = (formControl, fieldName) => {
  const {touched} = formControl;
  return !!touched[fieldName];
};

export const getFieldValue = (formControl, fieldName) => {
  const {values} = formControl;
  return values[fieldName];
};
export const getFieldError = (formControl, fieldName) => {
  const {status, errors} = formControl;
  const isTouched = getFieldTouched(formControl, fieldName);
  // return errors[fieldName] || status[fieldName];
  return isTouched ? errors[fieldName] || status[fieldName] : ERROR_NULL_VALUE;
};

export const getFieldInitialVal = (formControl, fieldName) => {
  const {initialValues} = formControl;
  return initialValues[fieldName];
};

export const getRequiredMessage = (field) => {
  return `${field} is required.`;
};
