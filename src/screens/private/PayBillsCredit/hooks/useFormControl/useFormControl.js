/**
 * @format
 * @flow
 */

// React and React Native modules
import {useState, useEffect, useMemo} from 'react';
import {InteractionManager} from 'react-native';
import {useNavigation} from '@react-navigation/core';

// Third Party Libraries
import {useFormik} from 'formik';

// Form Schema
import {initialValues as defaultValues, schema} from './schema';

const useFormControl = ({
  selectedSourceAccount,
  selectedBiller,
}: {
  selectedSourceAccount: Object,
  selectedBiller: Object,
}): Object => {
  const navigation = useNavigation();

  /*##### PRE-POPULATE INITIAL VALUES #####*/

  const initialDefaultValues = useMemo(() => {
    return {
      ...defaultValues,
      sourceAccount: selectedSourceAccount,
      biller: selectedBiller,
    };
  }, [selectedSourceAccount, selectedBiller]);

  const [initialValues, setInitialValues] = useState(initialDefaultValues);
  const [previousValues, setPreviousValues] = useState(initialDefaultValues);
  const [previousStatus, setPreviousStatus] = useState({});
  const [previousTouched, setPreviousTouched] = useState({});
  const [previousErrors, setPreviousErrors] = useState({});
  const [validationSchema, setValidationSchema] = useState(schema);

  const handleFormSubmit = (values) => {
    navigation.navigate('PayBillsCredit', {
      screen: 'ConfirmScreen',
    });
  };

  const formControl = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleFormSubmit,
    enableReinitialize: true,
    validateOnMount: false,
    initialStatus: {},
  });

  const updateFormControl = (newSchema, newInitialValues, newValues) => {
    const combinedInitialValues = {
      ...initialDefaultValues,
      ...newInitialValues,
    };

    setInitialValues(combinedInitialValues);
    setPreviousValues({
      ...formControl.values,
      ...newValues,
      biller: selectedBiller ?? null,
    });
    setPreviousTouched({...formControl.touched});
    setPreviousStatus(formControl.status);
    setPreviousErrors(formControl.errors);
    setValidationSchema(validationSchema.shape(newSchema));
  };

  useEffect(() => {
    formControl.setStatus(previousStatus);
    formControl.setValues(previousValues, false);
    formControl.setErrors(previousErrors);
    formControl.setTouched(previousTouched, true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    validationSchema,
    previousErrors,
    previousStatus,
    previousTouched,
    previousValues,
  ]);

  const isFormDirty = formControl.dirty;

  formControl.resetField = (fieldName) => {
    InteractionManager.runAfterInteractions(() => {
      formControl.setFieldValue(
        fieldName,
        formControl.initialValues[fieldName],
      );
      formControl.setFieldTouched(fieldName, false, true);
    });
  };

  formControl.resetFields = (fieldNames) => {
    fieldNames.map((name) => formControl.resetField(name));
  };

  return {isFormDirty, formControl, updateFormControl};
};

export default useFormControl;
