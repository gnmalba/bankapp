/**
 * @flow
 */

export {default} from './useFormControl';
