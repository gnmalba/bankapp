import * as yup from 'yup';

import {SCHEDULE_TYPE, OCCURRENCE_TYPE, ENDS_TYPE} from '../../constants/enums';

import {
  EARLIER_THAN_START_DATE,
  INVALID_ONE_TIME_DATE_TIME,
  INVALID_REPEATING_DATE_TIME,
  INVALID_REPEATING_END_DATE,
  // EXCEEDS_MAXIMUM_OCCURRENCES,
  // REQUIRED_FIELD,
  // INVALID_NOTES,
  MINIMUM_OCCURRENCES,
  INVALID_OCCURRENCES,
  EXCEEDS_THREE_YEARS,
  ELAPSED_DATE_TIME,
  ELAPSED_START_DATE_TIME,
  INVALID_OCCURRENCES_END_DATE,
} from '../../constants/errorMessages';

import {getRequiredMessage} from '../../utils/formikUtils';
import {
  checkFormatDateTimeInput,
  isElapsedDate,
  isExceedsThreeYears,
  isEndDateBeforeStartDate,
  getMaxOccurrences,
} from '../../utils';

export const initialValues = {
  sourceAccount: null,
  amount: null,
  biller: null,
  scheduleType: SCHEDULE_TYPE.IMMEDIATE,
  // Schedule Type: Immediate,
  // No conditional fields
  occurrenceType: OCCURRENCE_TYPE.ONE_TIME,
  // Schedule Type: Later, Occurrence Type: One Time
  dateTime: null,
  // Schedule Type: Later, Occurrence Type: Repeating
  frequencyType: null,
  startDate: null,
  endType: ENDS_TYPE.ON,
  // Schedule Type: Later, Occurrence Type: Repeating, End Type: On
  endDate: null,
  // Schedule Type: Later, Occurrence Type: Repeating, End Type: After
  occurrences: null,
  // Notes
  notes: null,
  // Dynamic Input
  paymentFields: null,
};

export const schema = yup.object().shape({
  sourceAccount: yup
    .object()
    .nullable()
    .required(getRequiredMessage('Source Account')),
  amount: yup.string().nullable(true).required(getRequiredMessage('Amount')),
  biller: yup.object().nullable().required(getRequiredMessage('Biller')),
  scheduleType: yup
    .string()
    .oneOf([SCHEDULE_TYPE.IMMEDIATE, SCHEDULE_TYPE.LATER])
    .required(getRequiredMessage('Schedule Type')),
  // Schedule Type: Immediate,
  // No conditional fields
  occurrenceType: yup
    .string()
    .oneOf([OCCURRENCE_TYPE.ONE_TIME, OCCURRENCE_TYPE.REPEATING])
    .when('scheduleType', {
      is: (value) => value === SCHEDULE_TYPE.LATER,
      then: yup.string().required(getRequiredMessage('Occurrence Type')),
    }),
  // Schedule Type: Later, Occurrence Type: One Time
  dateTime: yup
    .string()
    .nullable()
    .when(['scheduleType', 'occurrenceType'], {
      is: (scheduleType, occurrenceType) =>
        scheduleType === SCHEDULE_TYPE.LATER &&
        occurrenceType === OCCURRENCE_TYPE.ONE_TIME,
      then: yup
        .string()
        .nullable()
        .required(getRequiredMessage('Payment Date and Time'))
        .test(
          'invalid-format-one-time-date-time',
          INVALID_ONE_TIME_DATE_TIME,
          function (value) {
            return checkFormatDateTimeInput('dateTime', value);
          },
        )
        .test('date-time-elapsed-date', ELAPSED_DATE_TIME, function (value) {
          return !isElapsedDate(value);
        })
        .test(
          'date-time-exceeds-three-years',
          EXCEEDS_THREE_YEARS,
          function (value) {
            return !isExceedsThreeYears(value);
          },
        ),
    }),

  // Schedule Type: Later, Occurrence Type: Repeating
  frequencyType: yup
    .object()
    .nullable()
    .when(['scheduleType', 'occurrenceType'], {
      is: (scheduleType, occurrenceType) =>
        scheduleType === SCHEDULE_TYPE.LATER &&
        occurrenceType === OCCURRENCE_TYPE.REPEATING,
      then: yup.object().required(),
    }),
  startDate: yup
    .string()
    .nullable()
    .when(['scheduleType', 'occurrenceType'], {
      is: (scheduleType, occurrenceType) =>
        scheduleType === SCHEDULE_TYPE.LATER &&
        occurrenceType === OCCURRENCE_TYPE.REPEATING,
      then: yup
        .string()
        .nullable()
        .required(getRequiredMessage('Start Date'))
        .test(
          'invalid-format-repeating-date-time',
          INVALID_REPEATING_DATE_TIME,
          function (value) {
            return checkFormatDateTimeInput('dateTime', value);
          },
        )
        .test(
          'start-date-elapsed-date',
          ELAPSED_START_DATE_TIME,
          function (value) {
            return !isElapsedDate(value);
          },
        )
        .test(
          'start-date-exceeds-three-years',
          EXCEEDS_THREE_YEARS,
          function (value) {
            return !isExceedsThreeYears(value);
          },
        ),
    }),
  endType: yup
    .string()
    .oneOf([ENDS_TYPE.ON, ENDS_TYPE.AFTER])
    .when(['scheduleType', 'occurrenceType'], {
      is: (scheduleType, occurrenceType) =>
        scheduleType === SCHEDULE_TYPE.LATER &&
        occurrenceType === OCCURRENCE_TYPE.REPEATING,
      then: yup.string().required(getRequiredMessage('End Type')),
    }),

  // Schedule Type: Later, Occurrence Type: Repeating, End Type: On
  endDate: yup
    .string()
    .nullable()
    .when(['scheduleType', 'occurrenceType', 'endType'], {
      is: (scheduleType, occurrenceType, endType) =>
        scheduleType === SCHEDULE_TYPE.LATER &&
        occurrenceType === OCCURRENCE_TYPE.REPEATING &&
        endType === ENDS_TYPE.ON,
      then: yup
        .string()
        .nullable()
        .required(getRequiredMessage('End Date'))
        .test(
          'invalid-format-repeating-date',
          INVALID_REPEATING_END_DATE,
          function (value) {
            return checkFormatDateTimeInput('date', value);
          },
        )
        .test(
          'earlier-than-start-date',
          EARLIER_THAN_START_DATE,
          function (value) {
            return !isEndDateBeforeStartDate(this.parent.startDate, value);
          },
        )
        .test('end-date-elapsed-date', ELAPSED_DATE_TIME, function (value) {
          return !isElapsedDate(value);
        })
        .test(
          'end-date-exceeds-three-years',
          EXCEEDS_THREE_YEARS,
          function (value) {
            return !isExceedsThreeYears(value);
          },
        )
        .test(
          'end-date-occurrences',
          INVALID_OCCURRENCES_END_DATE,
          function (value) {
            if (this.parent.frequencyType) {
              const maxOccurrences = getMaxOccurrences(
                this.parent.frequencyType,
                this.parent.startDate,
                value,
              );

              if (maxOccurrences < 2) {
                return false;
              }
              return true;
            }
            return true;
          },
        ),
    }),

  // Schedule Type: Later, Occurrence Type: Repeating, End Type: After
  occurrences: yup
    .number()
    .nullable(true)
    .when(['scheduleType', 'occurrenceType', 'endType'], {
      is: (scheduleType, occurrenceType, endType) =>
        scheduleType === SCHEDULE_TYPE.LATER &&
        occurrenceType === OCCURRENCE_TYPE.REPEATING &&
        endType === ENDS_TYPE.AFTER,
      then: yup
        .number()
        .typeError(INVALID_OCCURRENCES)
        .min(2, MINIMUM_OCCURRENCES)
        .nullable(true)
        .required(getRequiredMessage('Occurrences')),
    }),
  // Notes
  notes: yup.string().nullable().notRequired(),
  // Dynamic Input
  paymentFields: yup.object().nullable(),
  additionalPaymentFields: yup.object(),
});
