/**
 * @flow
 */
import {useCallback} from 'react';
//ducks
import {initializePbc} from '@ducks/pay-bills-credit/actions';
import type {Account} from '@ducks/pay-bills-credit/types';
import type {State} from '@ducks/types';
//Third-party libraries
import {useFocusEffect} from '@react-navigation/native';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {getActiveWidgets} from '@ducks/config/selectors';

type SelectorReturnType = {
  loading: boolean,
  isInitial: boolean,
  selectedSourceAccount: Account,
};
const useInitialPayBillsCredit = (): {
  loading: boolean,
  isInitial: boolean,
  selectedSourceAccount: Account,
} => {
  const dispatch = useDispatch();
  const activeWidgets = useSelector(getActiveWidgets);

  /**
   * select loading state of all accounts
   */
  const {creditLoading, depositsLoading, yazzLoading} = useSelector(
    (state) => ({
      creditLoading: state.credit.creditList.loading,
      depositsLoading: state.deposit.fetching,
      yazzLoading: state.yazz.loading,
    }),
  );

  //this makes sure all accounts are loaded before initializing pay bills screen

  const cardLoadingStates = [
    creditLoading,
    depositsLoading,
    yazzLoading,
    ...activeWidgets.map((row) => row.loading),
  ];

  const isFalse = (currentValue) => currentValue === false;

  const areCardsNotLoading = cardLoadingStates.every(isFalse);

  const {isInitial, selectedSourceAccount}: SelectorReturnType = useSelector(
    (state: State) => ({
      isInitial: state.paybillscredit.isInitial,
      selectedSourceAccount: state.paybillscredit.selectedSourceAccount,
    }),
    shallowEqual,
  );

  /**
   **Check/Loads all Initial and Pre-selected Pbc values*
   */
  useFocusEffect(
    useCallback(() => {
      if (areCardsNotLoading && isInitial) {
        dispatch(initializePbc());
      }
    }, [dispatch, areCardsNotLoading, isInitial]),
  );
  //loading state if cards/accounts are still fetching
  const loading: boolean = !areCardsNotLoading;

  return {loading, isInitial, selectedSourceAccount};
};

export default useInitialPayBillsCredit;
