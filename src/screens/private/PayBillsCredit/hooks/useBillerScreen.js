/**
 * @flow
 */

/* eslint-disable react-hooks/exhaustive-deps */

import {useEffect, useState} from 'react';
import {Keyboard} from 'react-native';

import {handleListTypeChange} from '../BillersScreen/utils';
import {BILLER_TYPE, LIST_TYPE} from '../BillersScreen/constants';

import type {PbcBillers, Biller} from '@ducks/billers/types';

// @flow

export type ReturnProps = {
  searchText: string,
  listType: string,
  isFocus: boolean,
  billerType: string,
  setSearchText: any,
  setListType: any,
  setFocus: any,
  setBillerType: any,
  allBillers: Array<Object>,
  enrolledBillers: Array<Object>,
  setAllBillers: any,
  setEnrolledBillers: any,
};

const useBillerScreen = (
  allBillersSource: PbcBillers,
  enrolledBillersSource: Array<Biller>,
  setCategory: any,
  categoryName?: ?string,
): ReturnProps => {
  const [searchText, setSearchText] = useState('');
  const [allBillers, setAllBillers] = useState(allBillersSource);
  const [enrolledBillers, setEnrolledBillers] = useState(enrolledBillersSource);
  const [listType, setListType] = useState(LIST_TYPE.categoryList);
  const [isFocus, setFocus] = useState(false);
  const [billerType, setBillerType] = useState(BILLER_TYPE.enrolledBillers);

  useEffect(() => {
    if (categoryName) {
      setListType(LIST_TYPE.sectionList);
      if (listType !== LIST_TYPE.sectionList) {
        setAllBillers(allBillersSource);
      }
    } else {
      handleListTypeChange(isFocus, searchText, setListType);
    }
  }, [isFocus, searchText, categoryName]); //

  //reset all state when biller type change
  useEffect(() => {
    setFocus(false);
    setSearchText('');
    setListType(LIST_TYPE.categoryList);
    setCategory('');
    Keyboard.dismiss();
  }, [billerType]);

  return {
    enrolledBillers,
    setEnrolledBillers,
    allBillers,
    setAllBillers,
    searchText,
    listType,
    isFocus,
    billerType,
    setSearchText,
    setListType,
    setFocus,
    setBillerType,
  };
};

export default useBillerScreen;
