/**
 * @flow
 */

import {useCallback, useState} from 'react';

const useLeavePrompt = (): {
  isLeavePromptVisible: boolean,
  openLeavePrompt: Function,
  closeLeavePrompt: Function,
} => {
  const [isLeavePromptVisible, setIsLeavePromptVisible] = useState(false);

  const closeLeavePrompt = useCallback(() => {
    setIsLeavePromptVisible(false);
  }, []);

  const openLeavePrompt = useCallback(() => {
    setIsLeavePromptVisible(true);
  }, []);

  return {isLeavePromptVisible, openLeavePrompt, closeLeavePrompt};
};

export default useLeavePrompt;
