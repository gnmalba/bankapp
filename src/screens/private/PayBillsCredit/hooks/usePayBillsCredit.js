/**
 * @flow
 */ //ducks

import type {
  Account,
  PbcImmediateProps,
  ResponsePayLoad,
} from '@ducks/pay-bills-credit/types';
import type {Biller} from '@ducks/billers/types';
//Third-party libraries
import {useSelector, useDispatch} from 'react-redux';
import {
  setPbcSourceAccount,
  payBillsCreditImmediate,
  payBillsCreditScheduled,
} from '@ducks/pay-bills-credit/actions';

const usePayBillsCredit = (): ({
  allSourceAccounts: Array<Account>,
  setSourceAccount(account: Account): any,
  payImmediate(props: PbcImmediateProps): any,
  payScheduled(props: PbcImmediateProps): any,
  selectedSourceAccount: Account,
  selectedBiller: Biller,
  loading: boolean,
  isPayCredit: boolean,
  billerType: string,
  postRequestResponse: ResponsePayLoad,
}) => {
  const dispatch = useDispatch();

  //get selected biller and selectedAccount
  const {
    selectedSourceAccount,
    selectedBiller,
    accounts,
    loading,
    isPayCredit,
    billerType,
    postRequestResponse,
  } = useSelector((state) => ({
    selectedSourceAccount: state.paybillscredit?.selectedSourceAccount,
    selectedBiller: state.billers?.payBillsSelectedBiller,
    accounts: state.paybillscredit.payBillsAccounts,
    loading: state.paybillscredit.loading,
    isPayCredit: state.paybillscredit.isPayBillsCredit,
    billerType: state.billers.selectedBillerType,
    postRequestResponse: state.paybillscredit.postRequestResponse,
  }));

  const allSourceAccounts = accounts.all;

  const setSourceAccount = (account: Account): any =>
    dispatch(setPbcSourceAccount(account));

  const payImmediate = (props: PbcImmediateProps): any =>
    dispatch(payBillsCreditImmediate(props));
  const payScheduled = (props: PbcImmediateProps): any =>
    dispatch(payBillsCreditScheduled(props));

  return {
    allSourceAccounts,
    selectedBiller,
    selectedSourceAccount,
    setSourceAccount,
    payImmediate,
    payScheduled,
    loading,
    isPayCredit,
    billerType,
    postRequestResponse,
  };
};

export default usePayBillsCredit;
