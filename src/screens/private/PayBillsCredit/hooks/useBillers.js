/**
 * @flow
 */ //ducks
import type {State} from '@ducks/types';

//Third-party libraries
import {shallowEqual, useSelector, useDispatch} from 'react-redux';

import type {AllBillers, Biller} from '@ducks/billers/types';
import {setPbcBiller, selectPbcCategory} from '@ducks/billers/actions';

import {selectedCategorNameySelector} from '@ducks/billers/selector';
type SelectorReturnType = {
  allBillers: AllBillers,
  categories: any,
  loading: boolean,
  enrolledBillers: Array<Biller>,
  setBiller(id: string, billerType: string): any,
  setCategory(categoryId: string): any,
  categoryName?: ?string,
};
const useBillers = (): SelectorReturnType => {
  const dispatch = useDispatch();

  const categoryName = useSelector(selectedCategorNameySelector);

  const {
    allBillers: initialBillers,
    categories,
    loading,
    enrolledBillers,
  }: SelectorReturnType = useSelector(
    (state: State) => ({
      allBillers: state.billers.payBillsAllBillers,
      categories: state.billers.categories,
      loading: state.billers.loading,
      enrolledBillers: state.billers.payBillsEnrolledBiller,
    }),
    shallowEqual,
  );

  const setBiller = (id: string, billerType: string, setFieldValue: any): any =>
    dispatch(setPbcBiller(id, billerType, setFieldValue));
  const setCategory = (categoryId: string): any => {
    dispatch(selectPbcCategory(categoryId));
  };

  const allBillers = {
    alphabetical: initialBillers.alphabetical,
    section: categoryName
      ? initialBillers.section.filter((row) => row.title === categoryName)
      : initialBillers.section,
  };

  return {
    loading,
    allBillers,
    categories,
    setBiller,
    enrolledBillers,
    setCategory,
    categoryName,
  };
};

export default useBillers;
