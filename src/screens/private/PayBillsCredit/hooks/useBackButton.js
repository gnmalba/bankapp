/**
 * @flow
 */

import {useEffect, useCallback} from 'react';
import {BackHandler, InteractionManager} from 'react-native';
import {
  useRoute,
  useNavigation,
  getFocusedRouteNameFromRoute,
} from '@react-navigation/native';

import {useDispatch} from 'react-redux';

import {resetBillers} from '@ducks/billers/actions';
import {resetPayBillsCredit} from '@ducks/pay-bills-credit/actions';

type useBackButtonProps = {
  openLeavePrompt: Function,
  closeLeavePrompt: Function,
  formControl: Object,
  isFormDirty: boolean,
  isLeavePromptVisible: boolean,
};

const useBackHandler = (handleBackPress) => {
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      handleBackPress,
    );

    return () => {
      backHandler.remove();
    };
  }, [handleBackPress]);
};

const handleBackNavigation = (
  currentRoute,
  navigation,
  isFormDirty,
  openLeavePrompt,
  closeLeavePrompt,
  isLeavePromptVisible,
  formControl,
  dispatch,
) => {
  if (currentRoute === 'MainScreen') {
    if (isLeavePromptVisible || !isFormDirty) {
      closeLeavePrompt();
      dispatch(resetBillers());
      dispatch(resetPayBillsCredit());
      InteractionManager.runAfterInteractions(() => {
        formControl.resetForm({});
        navigation.goBack();
      });
    } else if (isFormDirty) {
      openLeavePrompt();
    }
  } else if (currentRoute === 'LoadingScreen') {
    navigation.navigate('PayBillsCredit', {
      screen: 'PassCodeScreen',
    });
  } else if (currentRoute === 'PassCodeScreen') {
    navigation.navigate('PayBillsCredit', {
      screen: 'ConfirmScreen',
    });
  } else {
    navigation.navigate('PayBillsCredit', {
      screen: 'MainScreen',
    });
  }
};

const useBackButton = ({
  openLeavePrompt,
  closeLeavePrompt,
  isFormDirty,
  formControl,
  isLeavePromptVisible,
}: useBackButtonProps): Array<Function> => {
  const navigation = useNavigation();
  const route = useRoute();

  const dispatch = useDispatch();

  // Handle Navigation to Previous Screen;
  // get current route then navigate accordingly
  const handleBackPress = useCallback(() => {
    const currentRoute = getFocusedRouteNameFromRoute(route) ?? 'MainScreen';
    handleBackNavigation(
      currentRoute,
      navigation,
      isFormDirty,
      openLeavePrompt,
      closeLeavePrompt,
      isLeavePromptVisible,
      formControl,
      dispatch,
    );

    return true;
  }, [
    navigation,
    route,
    isFormDirty,
    openLeavePrompt,
    isLeavePromptVisible,
    closeLeavePrompt,
    formControl,
    dispatch,
  ]);

  useBackHandler(handleBackPress);

  return [handleBackPress];
};

export default useBackButton;
