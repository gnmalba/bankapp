import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import ActivateCardPasscode from '../ActivateCardPasscode';

import {ThemeProvider} from 'styled-components/native';
import {NavigationContext} from '@react-navigation/native';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

const navContext = {
  isFocused: () => true,
  addListener: jest.fn(() => jest.fn()),
};
//
describe('ActivateCardPasscode', () => {
  const navigation = {
    navigate: (screen: string) => {},
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider
        store={{
          cardActivation: {
            submitting: false,
            submittingError: 'Something went wrong',
            passcodeExceeds: false,
            invalidPasscodeErrorMessage: null,
            activatedCard: null,
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <ActivateCardPasscode
              navigation={navigation}
              route={{params: 'Sample serno'}}
            />
          </NavigationContext.Provider>
        </ThemeProvider>
      </MockProvider>,
    );

    const smoothPinCode = all.getByTestId('smoothInputTestId');
    fireEvent(smoothPinCode, 'onChangeText', '111111');

    const passcodeComp = all.getByTestId('passcodeTestIdButton');

    fireEvent(passcodeComp, 'onPress');

    all.update(
      <MockProvider
        store={{
          cardActivation: {
            submitting: true,
            submittingError: null,
            passcodeExceeds: false,
            invalidPasscodeErrorMessage: 'Something went wrong',
            activatedCard: null,
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <ActivateCardPasscode
              route={{params: 'Sample serno'}}
              navigation={navigation}
            />
          </NavigationContext.Provider>
        </ThemeProvider>
      </MockProvider>,
    );
  });
});
