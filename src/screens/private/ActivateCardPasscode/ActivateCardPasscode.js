/**
 * @format
 * @flow
 */

import React, {useEffect, useState, useCallback} from 'react';
import {View} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import {useFocusEffect} from '@react-navigation/native';

import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import {LinkedText} from './Styled';

import Passcode from '@screens/common/Passcode';
import ProcessingScreen from '@screens/common/ProcessingScreen';
import Text from '@components/Text';

import {
  submitCardForActivation,
  resetSubmitError,
  resetPasscodeError,
} from '@ducks/card-activation/actions';
import {getCardActivationState} from '@ducks/card-activation/selectors';

const ActivateCardPasscode = ({navigation, route}: PropsType): React$Node => {
  const dispatch = useDispatch();
  const {
    submitting,
    submittingError,
    passcodeExceeds,
    invalidPasscodeErrorMessage,
    activatedCard,
  } = useSelector(getCardActivationState);

  const cardSerNo = route.params;

  const [passcodeText, setPasscodeText] = useState('');

  const handleValidatePasscode = (a) => {
    if (passcodeText) {
      dispatch(submitCardForActivation(cardSerNo, passcodeText));
    }
  };

  const handleOnPasscodeChange = (text) => {
    if (submittingError) {
      dispatch(resetSubmitError());
    }

    if (invalidPasscodeErrorMessage) {
      dispatch(resetPasscodeError());
    }

    setPasscodeText(text);
  };

  useEffect(() => {
    if (submittingError) {
      navigation.navigate('ActivationFailed');
      dispatch(resetSubmitError());
    }
  }, [submittingError, navigation, dispatch]);

  useEffect(() => {
    if (passcodeExceeds) {
      navigation.navigate('ActivateCardPasscodeExceeds');
      dispatch(resetPasscodeError());
    }
  }, [passcodeExceeds, navigation, dispatch]);

  useEffect(() => {
    if (activatedCard) {
      navigation.navigate('ActivateCardSuccess');
    }
  }, [activatedCard, navigation, dispatch]);

  useFocusEffect(
    useCallback(() => {
      dispatch(resetPasscodeError());
    }, [dispatch]),
  );

  if (submitting) {
    return <ProcessingScreen label="Processing Request" />;
  }

  return (
    <Passcode
      password
      enableAutomaticScroll={false}
      passcodeError={
        invalidPasscodeErrorMessage ? invalidPasscodeErrorMessage : ''
      }
      passcode={passcodeText}
      onTextChange={handleOnPasscodeChange}
      onSubmit={handleValidatePasscode}
      title="Please enter your passcode."
      description="To proceed, please enter your 6-digit passcode"
      btnTitle="Next"
      helperContent={
        <View>
          <Text {...testProps('passcodeScreenMessage')} mode="body2">
            If you forgot passcode, please call our hotline{' '}
            <LinkedText>(02) 88-700-700</LinkedText> for further assistance.
          </Text>
        </View>
      }
    />
  );
};

export default ActivateCardPasscode;
