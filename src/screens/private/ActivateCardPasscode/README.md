## ActivateCardPasscode
ActivateCardPasscode component.

# Usage
```js
import ActivateCardPasscode from '@screens/private/ActivateCardPasscode';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
