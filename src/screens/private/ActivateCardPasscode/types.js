/**
 * @flow
 */

export type PropsType = {
  navigation: {
    navigate: (route: string, params?: Object) => void,
    addListener: (listener: string, callback: () => void) => void,
  },
  route: {
    params: string,
  },
};
