/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View``;

export const LinkedText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
  customColor: props.theme.color.SECONDARY_DEFAULT,
}))`
  font-weight: 700;
`;
