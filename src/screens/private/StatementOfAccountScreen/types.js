/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  route: {
    params: {
      id: Object,
      overviewData: Object,
      handleGoBack: (route: any) => void,
    },
  },
  navigation: Object,
};
