/**
 * @format
 * @flow
 */

import React, {useCallback} from 'react';
import {BackHandler} from 'react-native';

import type {PropsType} from './types';
import Layout from '@components/Layout';
import Loader from '@composition/Loader';
import StatementOfAccountDetails from '@composition/StatementOfAccountDetails';

import {StyledTabNavigator, TabScreen} from '@components/TabNavigator/Styled';
import {useSelector} from 'react-redux';
import {FlatList} from 'react-native';
import {
  getFocusedRouteNameFromRoute,
  useFocusEffect,
} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const StatementOfAccountScreen = (props: PropsType): React$Node => {
  const {route} = props;
  const soaList = useSelector((state) => state.soa.soaList);
  const accountId = route?.params?.id;
  const currentTab = getFocusedRouteNameFromRoute(route);

  const loader = () => (
    <Layout>
      <Loader isVisible={true} />
    </Layout>
  );

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (route.params?.handleGoBack) {
          route.params.handleGoBack(route);

          return true;
        }

        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [route]),
  );

  const renderItem = () => (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="always"
      enableOnAndroid
      extraScrollHeight={160}>
      <StyledTabNavigator
        scrollEnabled={true}
        initialRouteName={soaList.lastIndex}>
        {soaList.data.length &&
          soaList.data.map((item, index) => {
            return (
              <TabScreen
                name={`${item.billingDate}`}
                key={`TAB-${index}`}
                options={{
                  tabBarTestID: `statementOfAccountTabButton_${index}`,
                  tabBarAccessibilityLabel: `statementOfAccountTabButton_${index}`,
                }}>
                {() => (
                  <FlatList
                    keyboardShouldPersistTaps="handled"
                    data={['soa']}
                    keyExtractor={(x) => x.toString()}
                    renderItem={() => (
                      <StatementOfAccountDetails
                        item={item}
                        lastIndex={soaList.lastIndex}
                        accountId={accountId}
                        route={route}
                        soaList={soaList}
                        navigation={props.navigation}
                        showButtons={
                          item?.billingDate === 'Incoming' ||
                          item?.billingDate === soaList.lastIndex
                        }
                        currentTab={currentTab}
                      />
                    )}
                  />
                )}
              </TabScreen>
            );
          })}
      </StyledTabNavigator>
    </KeyboardAwareScrollView>
  );

  return soaList.loading ? loader() : renderItem();
};

export default StatementOfAccountScreen;
