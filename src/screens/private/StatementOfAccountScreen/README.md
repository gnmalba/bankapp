## StatementOfAccountScreen
StatementOfAccountScreen component.

# Usage
```js
import StatementOfAccountScreen from '@screens/private/StatementOfAccountScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
