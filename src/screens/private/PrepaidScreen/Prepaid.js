/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import type {PropsType} from './types';
import {useSelector} from 'react-redux';
import * as S from './Styled';
import {maskingFormat} from '@utils/accountNumber';
import {Container} from '@components/Card/Styled';
import {ScrollContainer} from '@screens/private/DepositScreenOverview/Styled';

import Layout from '@components/Layout';
import EnrollButton from '@composition/EnrollButton';
import AccountCard from '@composition/AccountCard';
import EnrollSnackbar from '@composition/EnrollSnackbar';

import type {Prepaids} from '@ducks/deposit/types';
import {getPrepaidFromDeposit} from '@ducks/deposit/selectors';
import {yazzSelector} from '@ducks/yazz/selectors';
import type {YazzCards} from '@ducks/yazz/types';

import {numberToCurrencyFormat} from '@ducks/deposit/utils';
import {useIsFocused} from '@react-navigation/native';

import {CREDIT_FULL_PAGE_ERROR} from '@utils/constants';

const Prepaid = (props: PropsType): React$Node => {
  useIsFocused();

  const prepaids: Prepaids = useSelector(getPrepaidFromDeposit);
  const yazz: YazzCards = useSelector(yazzSelector);

  const {navigation} = props;
  const renderCardPrepaid = (account, index) => {
    return (
      <S.MainContainer key={`${account.id}-${index}`}>
        <Container>
          <AccountCard
            name={account.alias}
            accountNumber={maskingFormat(account.accountNo)}
            balanceLabel="AVAILABLE"
            balanceValue={account.availableBalanceValueWithCurrency}
            isPrimary={account.initialAccount}
            onClick={() => {
              navigation.navigate('PrepaidScreenOverview', {
                id: account.id,
                alias: account.alias,
              });
            }}
            testID={`${index + 1}Prepaid`}
            isInactive={!account.active}
          />
        </Container>
      </S.MainContainer>
    );
  };

  const renderYazz = (account, index) => {
    const isInactive = account.flag === null || account.flag === 'NOT_ALLOWED';
    const isPrimary = account.primary === null ? 0 : account.primary;
    return (
      <S.MainContainer key={`${account.cardNo}-${index}`}>
        <Container>
          <AccountCard
            name={account.alias}
            accountNumber={maskingFormat(account.cardNo, true)}
            balanceLabel="AVAILABLE"
            balanceValue={`${account.currency} ${numberToCurrencyFormat(
              account.outstandingBalance,
            )}`}
            isPrimary={isPrimary !== 0}
            onClick={() => {
              if (account.flag && account.flag === 'NOT_ALLOWED') {
                navigation.navigate('Error Screen', {
                  isSystemError: false,
                  heading: CREDIT_FULL_PAGE_ERROR.header,
                  details: CREDIT_FULL_PAGE_ERROR.body,
                  headingAlignCenter: true,
                });
              } else {
                navigation.navigate('YazzScreenOverview', {
                  id: account.cardSerNo,
                  alias: account.alias,
                });
              }
            }}
            isInactive={isInactive}
          />
        </Container>
      </S.MainContainer>
    );
  };

  return (
    <Layout padded>
      <ScrollContainer>
        {prepaids.length > 0 &&
          prepaids.map((account, index) => renderCardPrepaid(account, index))}
        {/* should refactor soon */}
        {yazz.length > 0 &&
          yazz.map((account, index) => renderYazz(account, index))}
      </ScrollContainer>
      <EnrollButton
        testId="PrepaidEnrollAccountButtonId"
        onPress={() => navigation && navigation.navigate('Enroll Account')}
      />
      <EnrollSnackbar testId="PrepaidSuccessEnrollmentSnackbarId" />
    </Layout>
  );
};

export default Prepaid;
