## Prepaid
Prepaid component.

# Usage
```js
import Prepaid from '@screens/private/Prepaid';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
