import React from 'react';
import {render} from '@testing-library/react-native';
import Prepaid from '../Prepaid';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import * as S from '../Styled';
describe('Prepaid', () => {
  it('Should work as expected', () => {
    const store = {
      deposit: {
        prepaids: [
          {
            accountNo: '23455233442345',
          },
          {
            accountNo: '23455233442344',
          },
        ],
      },
      yazz: {
        data: [
          {
            flag: true,
            accountNo: '23455233442345',
          },
          {
            flag: true,
            accountNo: '23455233442344',
          },
        ],
      },
      enrollment: {
        showSnackbar: false,
        snackbarMessage: 'test',
      },
    };
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <Prepaid />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
  it('get the card Container', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <S.CardContainer />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
