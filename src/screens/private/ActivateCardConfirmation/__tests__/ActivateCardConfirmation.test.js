import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import ActivateCardConfirmation from '../ActivateCardConfirmation';
import {DARK_THEME} from '@utils/constants';

describe('ActivateCardConfirmation', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ActivateCardConfirmation
          navigation={{navigate: () => {}}}
          route={{
            params: {
              embossingName: 'Test embossing name',
              cardNo: 'Test card No',
              cardSerNo: 'Test card ser no',
            },
          }}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
