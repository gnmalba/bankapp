## ActivateCardConfirmation
ActivateCardConfirmation component.

# Usage
```js
import ActivateCardConfirmation from '@screens/private/ActivateCardConfirmation';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
