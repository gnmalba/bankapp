/**
 * @flow
 */

export type PropsType = {
  navigation: {
    navigate: (route: string, params?: Object) => void,
    goBack: () => void,
  },
  route: {
    params: {
      alias: string,
      cardNo: string,
      cardSerNo: string,
      embossingName: string,
    },
  },
};
