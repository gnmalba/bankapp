/**
 * @format
 * @flow
 */

import React from 'react';

import {ImageBackground} from 'react-native';

import styled from 'styled-components/native';

import Layout from '@components/Layout';
import Text from '@components/Text';
import Button from '@components/Button';

import {StyledMainContainer} from '@screens/private/ActivateCardList/ActivateCardList';

import {scale, verticalScale} from '@utils/scale';
import {maskingFormat} from '@utils/accountNumber';

import type {PropsType} from './types';

const StyledConfirmationText = styled(Text).attrs((props) => ({
  mode: 'body1',
  customColor: props.theme.color.TEXT_100_HIGH,
}))``;

const StyledCreditBackgroundImage = styled(ImageBackground).attrs((props) => ({
  source: require('@assets/images/credit-card.png'),
  resizeMode: 'contain',
}))`
  width: ${scale(206)}px;
  height: ${scale(142)}px;
  display: flex;
  align-self: center;
  justify-content: space-between;
  margin-top: ${verticalScale(20)}px;
`;

const StyledContainerWithMargin = styled.View`
  margin-vertical: ${verticalScale(30)}px;
`;

const StyledHorizonalMargin = styled.View`
  height: ${verticalScale(15)}px;
`;

const StyledInfoContainer = styled.View`
  display: flex;
  margin: ${scale(20)}px;
`;

const PRIMARY_DEFAULT_TEXT = 'PRIMARY';

const StyledPrimaryText = styled(Text).attrs({
  mode: 'overline2',
  customColor: 'white',
})`
  opacity: 0.6;
`;

const StyledEmbossingNameText = styled(Text).attrs((props) => ({
  mode: 'body2',
  customColor: 'white',
}))``;

const StyledCardNoText = styled(Text).attrs((props) => ({
  mode: 'overline2',
  customColor: 'white',
}))``;

const CreditCardImageWithInfo = ({embossingName, cardNo}) => (
  <StyledCreditBackgroundImage>
    <StyledInfoContainer>
      <StyledEmbossingNameText>{embossingName}</StyledEmbossingNameText>
      <StyledCardNoText>{cardNo}</StyledCardNoText>
    </StyledInfoContainer>
    <StyledInfoContainer>
      <StyledPrimaryText>{PRIMARY_DEFAULT_TEXT}</StyledPrimaryText>
    </StyledInfoContainer>
  </StyledCreditBackgroundImage>
);

const ActivateCardConfirmation = ({
  navigation,
  route,
}: PropsType): React$Node => {
  const {embossingName, cardNo, cardSerNo} = route.params;
  return (
    <Layout>
      <StyledMainContainer>
        <CreditCardImageWithInfo
          embossingName={embossingName}
          cardNo={maskingFormat(cardNo, true)}
        />
        <StyledContainerWithMargin>
          <StyledConfirmationText>
            Please make sure that you or your supplementary is in the posession
            of this card.
          </StyledConfirmationText>
          <StyledHorizonalMargin />
          <StyledConfirmationText>
            Do you wish to continue?
          </StyledConfirmationText>
        </StyledContainerWithMargin>
        <Button
          state="default"
          title="Activate card now"
          type="primary"
          onPress={() => navigation.navigate('ActivateCardPasscode', cardSerNo)}
        />
        <Button
          title="Cancel"
          type="primary"
          state="reset"
          onPress={() => navigation.goBack()}
        />
      </StyledMainContainer>
    </Layout>
  );
};

export default ActivateCardConfirmation;
