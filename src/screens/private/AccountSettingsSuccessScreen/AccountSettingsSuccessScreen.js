/**
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';

// Components
import Button from '@components/Button';
import Layout from '@components/Layout';
import Text from '@components/Text';
import Separator from '@components/Separator';

// Utils
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';
import type {PropsType} from './types';
import * as S from './Styled';

const AccountSettingsSuccessScreen = (props: PropsType): React$Node => {
  return (
    <Layout padding={26}>
      <View>
        <Separator space={verticalScale(90)} />
        <S.CheckIconImage {...testProps('accountSettingsCheckImageTestId')} />
        <Text
          mode="h3"
          {...testProps('accountSettingsTextHeadingTestId')}
          style={[S.Styles.textCenter]}>
          {props?.isPasscode ? 'Passcode' : 'Password'} Changed
        </Text>
        <S.TextBody {...testProps('accountSettingsTextBodyTestId')}>
          {props?.isPasscode
            ? 'You have successfully changed your passcode. Never share this to anyone.'
            : 'You have successfully changed your Metrobank Online password. Never share this to anyone.'}
        </S.TextBody>
        <Separator space={verticalScale(16)} />
      </View>
      <View>
        <Button
          title="Go back to Dashboard"
          type="primary"
          state="default"
          {...testProps('loginSettingstBackBtn')}
          onPress={() => {
            props.navigation.navigate('Dashboard');
            props.navigation.popToTop();
          }}
        />
      </View>
    </Layout>
  );
};

export default AccountSettingsSuccessScreen;
