## AccountSettingsSuccessScreen
AccountSettingsSuccessScreen component.

# Usage
```js
import AccountSettingsSuccessScreen from '@screens/private/AccountSettingsSuccessScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
