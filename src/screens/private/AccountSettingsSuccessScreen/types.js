/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  isPasscode: boolean,
  navigation: {
    navigate: any,
    pop: any,
    popToTop: any,
  },
};
