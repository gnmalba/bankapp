/**
 * @flow
 */
import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {scale, moderateScale} from '@utils/scale';
import {Image, StyleSheet} from 'react-native';

export const CheckIconImage: ComponentType<any> = styled(Image).attrs(
  (props) => ({
    source: require('@assets/icons/circle-success-icon.png'),
  }),
)`
  width: ${moderateScale(50)}px;
  height: ${moderateScale(60)}px;
  resize-mode: contain;
  align-self: center;
  margin-vertical: ${scale(20)}px;
`;

export const TextBody: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(14)}px;
  text-align: center;
  margin-vertical: ${scale(8)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
`;

type StylesType = {
  textCenter: Object,
};

export const Styles: StylesType = StyleSheet.create({
  textCenter: {
    textAlign: 'center',
  },
});
