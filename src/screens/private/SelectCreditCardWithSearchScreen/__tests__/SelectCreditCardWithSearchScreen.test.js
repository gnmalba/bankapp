import {fireEvent, render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import CardList from '../components/CardList';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

describe('CardList', () => {
  const listProps = [
    {
      list: [],
      onPress: jest.fn(),
      dispatchAction: () => ({type: 'AD'}),
      balanceLabel: 'AVAILABLE_BALANCE',
      shouldMask: true,
      isCASA: true,
    },
    {
      list: [
        {
          availableBalanceValueWithCurrency: '',
          currency: 'qwe',
          availableCredit: '123',
          name: 'ASDSA',
          number: '1234123412341234',
        },
      ],
      onPress: jest.fn(),
      dispatchAction: () => ({type: 'AD'}),
      balanceLabel: 'AVAILABLE_BALANCE',
      shouldMask: true,
      isCASA: true,
    },
    {
      list: [
        {
          availableBalanceValueWithCurrency: '',
          currency: 'qwe',
          availableCredit: '123',
          name: 'ASDSA',
          number: '1234123412341234',
        },
      ],
      onPress: jest.fn(),
      dispatchAction: () => ({type: 'AD'}),
      balanceLabel: 'AVAILABLE_BALANCE',
      shouldMask: true,
      isCASA: false,
    },
    {
      list: [
        {
          availableBalanceValueWithCurrency: '',
          currency: 'qwe',
          availableCredit: '123',
          name: 'ASDSA',
          number: '1234123412341234',
        },
      ],
      onPress: jest.fn(),
      dispatchAction: () => ({type: 'AD'}),
      balanceLabel: 'OUTSTANDING BALANCE',
      shouldMask: true,
      isCASA: false,
    },
    {
      list: [
        {
          availableBalanceValueWithCurrency: '',
          availableCredit: '123',
          name: 'ASDSA',
          number: '1234124412341234',
        },
      ],
      onPress: jest.fn(),
      dispatchAction: () => ({type: 'AD'}),
      balanceLabel: 'AVAILABLE_BALANCE',
      isCASA: true,
    },
    {
      list: [
        {
          availableBalanceValueWithCurrency: '',
          availableCredit: '123',
          name: 'ASDSA',
          number: '1434123412341234',
        },
      ],
      dispatchAction: () => ({type: 'AD'}),
      balanceLabel: 'AVAILABLE_BALANCE',
      shouldMask: true,
      isCASA: false,
    },
    {
      list: [
        {
          availableBalanceValueWithCurrency: '',
          availableCredit: '123',
          name: 'ASDSA',
          number: '1234126412341234',
        },
      ],
      onPress: jest.fn(),
      balanceLabel: 'OUTSTANDING BALANCE',
      shouldMask: true,
      isCASA: false,
    },
  ];

  it.each(listProps)('Should work as expected', (props) => {
    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <CardList {...props} />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();

    if (props.list.length) {
      const el = all.getByTestId('acctNumContainer_1234');

      fireEvent(el, 'onPress');
    }
  });
});
