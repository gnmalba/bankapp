/**
 * @flow
 */

import Text from '@components/Text';
import {moderateScale, verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View``;

export const CardListContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(15)}px;
`;

export const CardListItem: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  padding: ${verticalScale(15)}px 0px;
  border-bottom-width: ${(props) => (props.isLastItem ? '0px' : '1px')};
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
  align-items: flex-end;
`;

export const CardListItemLeftSide: ComponentType<any> = styled.View`
  max-width: 100%;
`;

export const CardListItemLeftDetails: ComponentType<any> = styled.View`
  flex: 1;
  margin-right: ${verticalScale(24)}px;
`;

export const CardListItemRightSide: ComponentType<any> = styled.View`
  margin-left: auto;
  align-items: flex-end;
`;

export const CardListItemName: ComponentType<any> = styled(Text)`
  margin-bottom: ${verticalScale(4)}px;
`;

export const CardListItemNumber: ComponentType<any> = styled(Text)`
  font-family: 'MyriadPro-Semibold';
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const CardListItemBalanceLabel: ComponentType<any> = styled(Text)`
  font-weight: 700;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  text-transform: uppercase;
  margin-bottom: ${verticalScale(4)}px;
  letter-spacing: 0.85px;
`;

export const EmptyText: ComponentType<any> = styled(Text)`
  align-self: center;
  text-align: center;
  font-weight: 600;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const WrapperView: ComponentType<any> = styled.View`
  flex: 1;
`;

export const SelectCreditTitle: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(20)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;
