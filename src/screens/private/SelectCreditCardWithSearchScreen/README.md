## SelectCreditCardWithSearchScreen
SelectCreditCardWithSearchScreen component.

# Usage
```js
import SelectCreditCardWithSearchScreen from '@screens/private/SelectCreditCardWithSearchScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
