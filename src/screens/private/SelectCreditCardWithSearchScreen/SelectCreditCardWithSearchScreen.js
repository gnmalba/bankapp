/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {Keyboard, ScrollView, TouchableWithoutFeedback} from 'react-native';
import {useSelector} from 'react-redux';
import Layout from '@components/Layout';
import SearchBox from '@composition/SearchBox';
import {
  updateBalanceConversionFrom,
  updateBalanceTransferTo,
  updateCash2GoFrom,
} from '@ducks/installments/actions';
import {installmentsSelector} from '@ducks/installments/selectors';
import {HeaderContainer} from '@utils/commonStyles';
import {toCardList} from '@utils/installments';
import type {PropsType} from './types';
import {filterBySearchText} from './helpers';
import CardList from './components/CardList';
import {testProps} from '@utils/componentCustomProps';
import Separator from '@components/Separator';
import {WrapperView, SelectCreditTitle} from './Styled';

const SelectCreditCardWithSearchScreen = (props: PropsType): React$Node => {
  const credits = useSelector((state) => state.credit.creditList.data);
  const installmentsState = useSelector(installmentsSelector);

  const [searchText, setSearchText] = useState('');

  const {currentInstallmentType} = installmentsState;
  const [list, setList] = useState(toCardList(credits, currentInstallmentType));

  const onPress = (): void => {
    if (currentInstallmentType === 'BT') {
      props.navigation.navigate('BalanceTransferScreen');
    } else if (currentInstallmentType === 'BC') {
      props.navigation.navigate('BalanceConversionScreen');
    } else if (currentInstallmentType === 'C2GO') {
      props.navigation.navigate('Cash2GoScreen');
    }
  };

  const handleDispatchAction = (): Function | null => {
    if (currentInstallmentType === 'BT') {
      return updateBalanceTransferTo;
    } else if (currentInstallmentType === 'BC') {
      return updateBalanceConversionFrom;
    } else if (currentInstallmentType === 'C2GO') {
      return updateCash2GoFrom;
    }

    return null;
  };

  const handleOnChangeSearch = (value: string = ''): void => {
    setSearchText(value);
    setList(
      filterBySearchText(toCardList(credits, currentInstallmentType), value),
    );
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <WrapperView>
        <Layout padding={32}>
          <HeaderContainer marginBottom={41}>
            <SelectCreditTitle
              mode="body1"
              {...testProps('MbCreditScreenSelectionId')}>
              Select a credit card.
            </SelectCreditTitle>
          </HeaderContainer>
          <SearchBox
            placeholder={'Search'}
            onChange={handleOnChangeSearch}
            setValue={handleOnChangeSearch}
            value={searchText}
          />
          <Separator space={12} />
          <ScrollView showsVerticalScrollIndicator={false}>
            <CardList
              list={list}
              onPress={onPress}
              dispatchAction={handleDispatchAction()}
              balanceLabel={
                currentInstallmentType === 'BC'
                  ? 'OUTSTANDING BALANCE'
                  : 'AVAILABLE BALANCE'
              }
            />
          </ScrollView>
        </Layout>
      </WrapperView>
    </TouchableWithoutFeedback>
  );
};

export default SelectCreditCardWithSearchScreen;
