/**
 * @format
 * @flow
 */

import Text from '@components/Text';
import React from 'react';
import {Pressable} from 'react-native';
import {
  CardListContainer,
  CardListItem,
  CardListItemBalanceLabel,
  CardListItemLeftDetails,
  CardListItemName,
  CardListItemNumber,
  CardListItemRightSide,
  EmptyText,
} from '../Styled';
import type {CardListType} from '../types';
import {useDispatch} from 'react-redux';
import {maskingFormat} from '@utils/accountNumber';
import {numberToCurrencyFormat} from '@ducks/deposit/utils';
import {testProps} from '@utils/componentCustomProps';
import {getLastFourDigits} from '@utils/getLastFourDigits';
import {DEFAULT_CURRENCY} from '@utils/installments';

const formatAmount = (currencyType: string, amount: number): string => {
  return `${currencyType} ${numberToCurrencyFormat(amount || 0)}`;
};

const CardList = (props: CardListType): React$Node => {
  const {list, onPress, dispatchAction, shouldMask = true, isCASA} = props;
  const dispatch = useDispatch();

  const handlePress = (item) => {
    if (onPress) {
      if (dispatchAction) {
        const payload: any = {
          alias: item.name,
          cardNumber: item.number,
          ...item,
        };

        dispatch(dispatchAction(payload));
      }

      onPress();
    }
  };

  const renderBalance = (item) => {
    if (isCASA) {
      return item.availableBalanceValueWithCurrency;
    }

    if (props.balanceLabel === 'OUTSTANDING BALANCE') {
      return formatAmount(
        item.currency || DEFAULT_CURRENCY,
        item.outstandingBalance,
      );
    } else {
      return formatAmount(
        item.currency || DEFAULT_CURRENCY,
        item.availableCredit,
      );
    }
  };

  return (
    <CardListContainer>
      {list.map((item, index) => {
        const lastFourDigits = getLastFourDigits(item.number);
        return (
          <Pressable
            key={`${item.id}-${index}`}
            onPress={() => handlePress(item)}
            {...testProps('acctNumContainer_' + lastFourDigits)}>
            <CardListItem isLastItem={index === list.length - 1}>
              <CardListItemLeftDetails>
                <CardListItemName
                  mode="body1"
                  numberOfLines={1}
                  {...testProps('acctNumName_' + lastFourDigits)}>
                  {item.name}
                </CardListItemName>
                <CardListItemNumber
                  mode="caption"
                  {...testProps('acctNumMasked_' + lastFourDigits)}>
                  {maskingFormat(item.number, shouldMask)}
                </CardListItemNumber>
              </CardListItemLeftDetails>
              <CardListItemRightSide>
                <CardListItemBalanceLabel
                  mode="overline2"
                  {...testProps('aactNumAvailBalLbl_' + lastFourDigits)}>
                  {props.balanceLabel}
                </CardListItemBalanceLabel>
                <Text
                  mode="body1"
                  {...testProps('acctNumBalValue_' + lastFourDigits)}>
                  {renderBalance(item)}
                </Text>
              </CardListItemRightSide>
            </CardListItem>
          </Pressable>
        );
      })}

      {!list.length && (
        <EmptyText mode="h3" {...testProps('MbCreditScreenSelectiontestID')}>
          No results found.
        </EmptyText>
      )}
    </CardListContainer>
  );
};

export default CardList;
