// @flow

export const filterBySearchText = (
  accounts: any[],
  searchText: string = '',
): Object[] => {
  return accounts.filter((account: Object) => {
    if (searchText) {
      return (
        account.cardEmbossedName
          .toLowerCase()
          .includes(searchText.toLowerCase()) ||
        account.cardNo.includes(searchText)
      );
    }

    return true;
  });
};
