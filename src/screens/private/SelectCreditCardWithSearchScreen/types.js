/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  navigation: any,
};

export type CardListType = {
  list: any[],
  onPress?: () => void,
  dispatchAction?: any,
  balanceLabel: 'AVAILABLE BALANCE' | 'OUTSTANDING BALANCE',
  shouldMask?: boolean,
  isCASA?: boolean,
};
