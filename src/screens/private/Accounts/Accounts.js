/**
 * @format
 * @flow
 */

import React, {useCallback} from 'react';
import type {PropsType} from './types';
import {StyledTabNavigator, TabScreen} from '@components/TabNavigator/Styled';
import {testProps} from '@utils/componentCustomProps';

// components
import Deposit from '@screens/private/DepositScreen';
import Credit from '@screens/private/CreditScreen';
import Prepaid from '@screens/private/PrepaidScreen';

import ManageAccountSettingsSnackbar from '@screens/private/ManageAccountSettings/components/ManageAccountSettingsSnackbar/ManageAccountSettingsSnackbar';

import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/core';
import {BackHandler} from 'react-native';

const lengthList = (arr = []): boolean => arr.length > 0;
const MyAccounts = (props: PropsType): React$Node => {
  const {deposit, credit, yazz} = useSelector((state) => state);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (props.route?.params?.handleGoBack) {
          props.route.params.handleGoBack(props.route);

          return true;
        }

        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [props.route]),
  );

  return (
    <>
      <StyledTabNavigator>
        {(lengthList(deposit.deposits.savingsAndDeposits) ||
          lengthList(deposit.deposits.timeDeposits)) && (
          <TabScreen
            name="deposit"
            component={Deposit}
            options={{
              tabBarTestID: 'DepositTabTestId',
              tabBarAccessibilityLabel: 'DepositTabTestId',
              ...testProps('DepositTabTestId'),
            }}
          />
        )}

        {lengthList(credit.creditList.data) && (
          <TabScreen
            name="credit"
            component={Credit}
            options={{
              tabBarTestID: 'CreditTabTestId',
              tabBarAccessibilityLabel: 'CreditTabTestId',
              ...testProps('CreditTabTestId'),
            }}
          />
        )}

        {(lengthList(deposit.prepaids) || lengthList(yazz.data)) && (
          <TabScreen
            name="prepaid"
            component={Prepaid}
            options={{
              tabBarTestID: 'PrepaidTabTestId',
              tabBarAccessibilityLabel: 'PrepaidTabTestId',
              ...testProps('PrepaidTabTestId'),
            }}
          />
        )}
      </StyledTabNavigator>

      <ManageAccountSettingsSnackbar />
    </>
  );
};

export default MyAccounts;
