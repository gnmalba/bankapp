/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: any,
    pop: any,
  },
  route: {
    params: {
      oldpassword: string,
      newpassword: string,
    },
  },
  theme?: Object,
};

export type PasscodePropsType = {
  passcode: string,
  onTextChange: Function,
  passcodeError: string,
  handleNext: Function,
  error: string,
  setPasscodeError: Function,
};
