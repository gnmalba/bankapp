/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {View} from 'react-native';

// Screens
import Passcode from '@screens/common/Passcode';

// components
import Text from '@components/Text';

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {PasscodePropsType} from './types';
import * as S from './Styled';

const PasscodeScreen = (props: PasscodePropsType): React$Node => {
  const {
    passcode,
    error,
    onTextChange,
    passcodeError,
    handleNext,
    setPasscodeError,
  } = props;

  useEffect(() => {
    if (error) {
      setPasscodeError('Passcode is invalid. Please try again.');
    }
  }, [error, setPasscodeError]);

  return (
    <Passcode
      password
      enableAutomaticScroll={false}
      title="Please enter your passcode."
      description={
        <Text
          mode="body2"
          themeColor="TEXT_100_HIGH"
          {...testProps('lostCardTextBodyTestId')}>
          To proceed, please enter your 6-digit passcode.
        </Text>
      }
      btnTitle={'Next'}
      passcode={passcode}
      onTextChange={(text) => onTextChange(text)}
      passcodeError={passcodeError}
      helperContent={
        <View>
          <S.LinkText {...testProps('passcodeLinkTextTestId')}>
            If you forgot passcode, please call our hotline{' '}
            <S.LinkTextHref>(02) 88-700-700</S.LinkTextHref> for further
            assistance.
          </S.LinkText>
        </View>
      }
      onSubmit={handleNext}
    />
  );
};

export default PasscodeScreen;
