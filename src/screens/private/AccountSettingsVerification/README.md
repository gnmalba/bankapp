## AccountSettingsVerification
AccountSettingsVerification component.

# Usage
```js
import AccountSettingsVerification from '@screens/private/AccountSettingsVerification';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
