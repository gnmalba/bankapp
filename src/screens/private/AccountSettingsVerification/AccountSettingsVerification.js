/**
 * @format
 * @flow
 */

import React, {useState} from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Ducks
import {changePasswordRequest} from '@ducks/managesettings/actions';

// Components
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';
import Button from '@components/Button';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import PasscodeScreen from './PasscodeScreen';

const AccountSettingsVerification = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const manageSettings = useSelector((state) => state.manageSettings);
  const {oldpassword, newpassword} = props.route.params;

  const {loading} = manageSettings;

  const [passcode, setPasscode] = useState('');
  const [passcodeError, setPasscodeError] = useState('');
  const [error, setError] = useState({
    code: '',
    message: '',
  });
  const isMPINExceed = error && error?.code === 'invalid_mpin_exceeded';

  const handleTextChange = (text) => {
    setPasscode(text);
    if (text.length === 6) {
      setPasscodeError('');
    }
  };

  const handleSubmit = () => {
    const params = {
      passcode,
      oldpassword,
      newpassword,
      setError,
    };
    dispatch(changePasswordRequest(params));
  };

  const renderErrorScreen = () => (
    <ErrorScreen
      theme={theme}
      showBackButton
      headingAlignCenter={isMPINExceed}
      heading={
        isMPINExceed
          ? 'You have reached the maximum number of attempts. Please try again after (x) minutes.'
          : 'Oops! Something went wrong.'
      }
      details={
        error?.code
          ? error?.code === 'invalid_mpin_exceeded'
            ? null
            : error?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      isSystemError={error?.code ? false : true}
      handlePrimaryButton={() =>
        props.navigation.navigate('AccountSettingsChangePassword')
      }
      handleSecondaryButton={() => props.navigation.navigate('Drawer')}
      buttonContents={
        <Button
          title="Go back"
          type="primary"
          state="default"
          {...testProps('accountSettingsPasscodeErrorBtn')}
          onPress={() =>
            props.navigation.navigate('AccountSettingsChangePassword')
          }
        />
      }
    />
  );

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  if (error?.code && error?.code !== 'invalid_mpin') {
    return renderErrorScreen();
  }

  return (
    <PasscodeScreen
      error={error.message}
      passcode={passcode}
      passcodeError={passcodeError}
      setPasscodeError={setPasscodeError}
      onTextChange={(text) => handleTextChange(text)}
      handleNext={handleSubmit}
    />
  );
};

export default AccountSettingsVerification;
