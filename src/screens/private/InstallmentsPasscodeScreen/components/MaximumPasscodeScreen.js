// @flow
import Button from '@components/Button';
import ErrorScreen from '@screens/common/ErrorScreen';
import React from 'react';
import {ButtonHolder} from '../Styled';

const MaximumPasscodeScreen = (props: any): React$Node => {
  return (
    <>
      <ErrorScreen
        heading=" You have reached the maximum number of attempts. Please try again after(x) minutes."
        headingAlignCenter
      />
      <ButtonHolder>
        <Button
          title="Go back"
          type="primary"
          state="default"
          testID="backToConfirmScreenButton"
          onPress={() => props.navigation.navigate('ConfirmScreen')}
        />
      </ButtonHolder>
    </>
  );
};

export default MaximumPasscodeScreen;
