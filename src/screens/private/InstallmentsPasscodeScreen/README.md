## InstallmentsPasscodeScreen
InstallmentsPasscodeScreen component.

# Usage
```js
import InstallmentsPasscodeScreen from '@screens/private/InstallmentsPasscodeScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
