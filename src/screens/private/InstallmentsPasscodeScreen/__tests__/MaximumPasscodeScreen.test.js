import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MaximumPasscodeScreen from '../components/MaximumPasscodeScreen';
import MockProvider from '@utils/testFramework';

describe('MaximumPasscodeScreen', () => {
  it.each(['backToConfirmScreenButton', ''])(
    'Should work as expected',
    (testID) => {
      const all = render(
        <MockProvider store={{}}>
          <ThemeProvider theme={DARK_THEME}>
            <MaximumPasscodeScreen navigation={{navigate: () => {}}} />
          </ThemeProvider>
        </MockProvider>,
      );
      if (testID) {
        const btnEl = all.getByTestId(testID);
        fireEvent(btnEl, 'onPress');
      }
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
