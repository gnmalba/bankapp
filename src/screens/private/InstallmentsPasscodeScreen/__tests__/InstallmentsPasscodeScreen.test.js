import {fireEvent, render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import InstallmentsPasscodeScreen from '../InstallmentsPasscodeScreen';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

describe('InstallmentsPasscodeScreen', () => {
  const stores = [
    {
      installments: {
        currentInstallmentType: 'BT',
      },
    },
    {
      installments: {
        currentInstallmentType: 'BT',
        formLoading: true,
        saveInstallmentsError: {
          message: 'ASD',
          passcode: '223123',
        },
      },
    },
  ];

  it.each(stores)('Should work as expected', (store) => {
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <InstallmentsPasscodeScreen />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();

    if (!store.installments.formLoading) {
      const pc = all.getByTestId('pincodeInputTestId').children[0];
      fireEvent(pc, 'onTextChange');

      const button = all.getByTestId('passcodeTestIdButton');
      console.log(button);
      fireEvent(button, 'onClick');
    }
  });
});
