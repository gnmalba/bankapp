/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import * as S from '@screens/private/ReportLostCardPasscode/Styled';

import type {PropsType} from './types';
import {} from './Styled';
import Passcode from '@screens/common/Passcode';
import {useDispatch, useSelector} from 'react-redux';
import {prepareSavePayload} from '@utils/installments';
import {installmentsSelector} from '@ducks/installments/selectors';
import {
  saveBalanceConversion,
  saveBalanceTransfer,
  saveCash2Go,
} from '@ducks/installments/actions';
import {testProps} from '@utils/componentCustomProps';
import ProcessingScreen from '@screens/common/ProcessingScreen';

const InstallmentsPasscodeScreen = (props: PropsType): React$Node => {
  const dispatch = useDispatch();

  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType, saveInstallmentsError} = installmentsState;

  const [pcode, setPcode] = useState('');
  const [pcodeError, setpcodeError] = useState();

  useEffect(() => {
    if (saveInstallmentsError) {
      setpcodeError(saveInstallmentsError.message);
      setPcode(saveInstallmentsError.passcode);
    }
  }, [saveInstallmentsError]);

  const handleSetPasscode = (value: string = '') => {
    setpcodeError();
    setPcode(value);
  };

  const handleSubmit = () => {
    const payload = prepareSavePayload(
      currentInstallmentType,
      installmentsState,
      pcode,
    );

    if (currentInstallmentType === 'BT') {
      dispatch(saveBalanceTransfer(payload));
    } else if (currentInstallmentType === 'BC') {
      dispatch(saveBalanceConversion(payload));
    } else if (currentInstallmentType === 'C2GO') {
      dispatch(saveCash2Go(payload));
    }
  };

  return (
    <>
      {installmentsState.formLoading ? (
        <ProcessingScreen label="Processing request" route={{}} />
      ) : (
        <Passcode
          password
          enableAutomaticScroll={false}
          passcodeError={pcodeError}
          passcode={pcode}
          onTextChange={handleSetPasscode}
          onSubmit={handleSubmit}
          title="Please enter your passcode."
          description="To proceed, please enter your 6-digit passcode."
          btnTitle="Next"
          helperContent={
            <View>
              <S.LinkText {...testProps('passcodeScreenMessage')}>
                If you forgot passcode, please call our hotline{' '}
                <S.LinkTextHref>(02) 88-700-700</S.LinkTextHref> for further
                assistance.
              </S.LinkText>
            </View>
          }
        />
      )}
    </>
  );
};

export default InstallmentsPasscodeScreen;
