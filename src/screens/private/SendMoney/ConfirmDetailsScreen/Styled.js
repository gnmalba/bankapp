/**
 *
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {ScrollView} from 'react-native';

// Third party libraries
import styled from 'styled-components/native';

// Utils
import {verticalScale, scale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(24)}px;
`;

export const ScrollContainer: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    showsVerticalScrollIndicator: false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
  }),
)``;
export const HeaderContainer: ComponentType<any> = styled.View`
  align-items: center;
  justify-content: center;
  padding-vertical: ${verticalScale(16)}px;
  margin-top: ${verticalScale(16)}px;
`;

export const IconContainer: ComponentType<any> = styled.View`
  width: 100%;
`;

export const CloseButton: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/close.png'),
}))`
  align-self: flex-end;
  height: ${scale(30)}px;
  width: ${scale(30)}px;
  margin-bottom: ${scale(12)}px;
  tint-color: ${(props) => props.theme.topAppBar.iconColor};
`;

export const TextContainer: ComponentType<any> = styled.View`
  margin-vertical: ${scale(4)}px;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

export const EndOfTransactionDetailsContainter: ComponentType<any> = styled.View`
  margin-top: ${scale(16)}px;
  padding-horizontal: ${scale(16)}px;
  padding-vertical: ${scale(16)}px;
  padding-bottom: ${scale(8)}px;
  border-radius: 4px;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
`;

export const DividerContainer: ComponentType<any> = styled.View`
  margin-vertical: ${verticalScale(4)}px;
`;

export const TransacitonResultContainer: ComponentType<any> = styled.View`
  margin-vertical: ${verticalScale(30)}px;
  padding-vertical: ${scale(10)}px;
  padding-horizontal: ${scale(15)}px;
  flex-direction: row;
  align-items: center;
  align-self: center;
  justify-content: center;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
`;

export const Icon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: props.source,
}))`
  width: 11px;
  height: 14px;
  margin-right: 10px;
  tint-color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;
