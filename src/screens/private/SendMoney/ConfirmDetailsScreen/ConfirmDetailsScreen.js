/**
 * @format
 * @flow
 */
// React modules
import React from 'react';
import moment from 'moment';
import {useDispatch} from 'react-redux';

// Components
import Text from '@components/Text';
import Divider from '@components/Divider';
import ReviewItem from '@composition/InstallmentDetail';
import Layout from '@components/Layout';
import Separator from '@components/Separator';
import {ScrollView} from 'react-native';
import Button from '@components/Button';
import RenderChildrenWhen from '../RecipientScreen/components/RenderChildrenWhen';
import {RECIPIENT_TYPE, SCHEDULE_END_TYPE, SCHEDULE_TYPE} from '../constants';

// Component Style
import {HeaderContainer} from '@utils/commonStyles';

// Utils
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';
import {submit} from '@ducks/sendMoney/actions';

// Types
import * as S from './Styled';
import {useNavigation, useRoute} from '@react-navigation/core';
import {
  getEndDate,
  getNumberOfOccurrences,
  preparePayload,
} from '@screens/private/SendMoneyVerification/utils';

function ConfirmDetailsScreen(props: Object): React$Node {
  const navigation = useNavigation();
  const route = useRoute();
  const {formValues} = route.params;
  const {
    sourceAccount,
    recipientAccount,
    recipientType,
    amount,
    amountCurrency,
    notes,
    scheduleType,
    schedule,
    scheduleEndType,
    endDate,
  } = formValues;
  const fee = '0.00'; // Replace when API endpoint is available
  const dispatch = useDispatch();
  const handleConfirm = () => {
    if (recipientType === RECIPIENT_TYPE.OWN) {
      const payload = preparePayload(formValues);

      dispatch(submit(payload));

      navigation.navigate('SendMoneyStack', {
        screen: 'SendMoneyTransactionResult',
        params: {
          formValues,
        },
      });
    } else {
      navigation.navigate('SendMoneyStack', {
        screen: 'VerificationScreen',
        params: {
          formValues: {...formValues, fee},
        },
      });
    }
  };

  const formattedEndDate = moment(
    scheduleEndType === SCHEDULE_END_TYPE.ENDS_ON && endDate
      ? endDate
      : getEndDate(
          schedule.frequency,
          schedule.startDate,
          schedule.numberOfOccurrences,
        ),
  ).format('MM/DD/YYYY');

  const numberOfOccurrences =
    scheduleEndType === SCHEDULE_END_TYPE.ENDS_AFTER
      ? schedule.numberOfOccurrences
      : getNumberOfOccurrences(schedule.frequency, schedule.startDate, endDate);

  const ScheduleFields = () => {
    return (
      <>
        <ReviewItem
          small
          label="PAYMENT FREQUENCY"
          value={toText(schedule.frequency, 'SendMoneyFrequency', 'body2')}
          {...testProps('SendMoneyFrequency')}
          alignItems="flex-start"
        />

        <ReviewItem
          small
          label="START DATE"
          value={toText(schedule.startDate, 'SendMoneyDateAndTime', 'body2')}
          {...testProps('SendMoneyDateTime')}
          alignItems="flex-start"
        />

        <ReviewItem
          small
          label="ENDS ON"
          value={toText(formattedEndDate, 'SendMoneyDateAndTime', 'body2')}
          subValue={`${numberOfOccurrences} occurrences`}
          {...testProps('SendMoneyDateTime')}
          alignItems="flex-start"
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>
      </>
    );
  };

  return (
    <Layout padding={32}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <HeaderContainer marginBottom={41}>
          <Text mode="h2">All Good?</Text>
          <Separator space={8} />
          <Text mode="body2" themeColor="TEXT_100_HIGH">
            Please review your details.
          </Text>
        </HeaderContainer>

        <ReviewItem
          small
          label="FROM"
          value={toText(sourceAccount.alias, 'SendMoneyFrom', 'body2')}
          subValue={sourceAccount.identifier}
          alignItems="flex-start"
          {...testProps('SendMoneyFrom')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          small
          label="AMOUNT"
          value={toText(
            `${amountCurrency || 'PHP'} ${amount}`,
            'SendMoneyAmount',
            'body2',
          )}
          {...testProps('SendMoneyAmount')}
        />

        <ReviewItem
          small
          label="FEE"
          value={toText(
            `${amountCurrency || 'PHP'} ${fee}`,
            'SendMoneyFee',
            'body2',
          )}
          {...testProps('SendMoneyFee')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          small
          label="TO"
          value={toText(recipientAccount.alias, 'SendMoneyTo', 'body2')}
          subValue={'METROBANK'}
          alignItems="flex-start"
          {...testProps('SendMoneyTo')}
        />

        <ReviewItem
          small
          label="account no."
          value={toText(
            recipientAccount.identifier,
            'SendMoneyAccountNumber',
            'body2',
          )}
          alignItems="flex-start"
          {...testProps('SendMoneyAccountNo')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <RenderChildrenWhen
          condition={scheduleType === SCHEDULE_TYPE.SCHEDULED}>
          <ScheduleFields />
        </RenderChildrenWhen>

        <ReviewItem
          small
          label="NOTES"
          value={toText(notes, 'SendMoneyNotes', 'body2')}
          {...testProps('SendMoneyNotes')}
        />

        <Separator space={14} />

        <Button
          title="Confirm"
          state={'default'}
          type="primary"
          onPress={handleConfirm}
          labelTestID={{...testProps('Next')}.accessibilityLabel}
          {...testProps('SendMoneyConfirmButton')}
        />
      </ScrollView>
    </Layout>
  );
}

export default ConfirmDetailsScreen;
