/* eslint-disable react-hooks/rules-of-hooks */
/**
 * @flow
 */ //ducks

import {useEffect} from 'react';
import type {Account} from '@ducks/sendMoney/types';
//Third-party libraries
import {useSelector, useDispatch} from 'react-redux';
import {getEnrolledContacts} from '@ducks/sendMoney/actions';

const getEnrolledContactsList = (): {
  enrolledContacts: Array<Account>,
  enrolledContactsLoading: boolean,
} => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getEnrolledContacts());
  }, [dispatch]);

  const {accounts, enrolledContactsLoading} = useSelector((state) => ({
    accounts: state.sendMoney?.enrolledContacts,
    enrolledContactsLoading: state.sendMoney?.isLoadingEnrolledContacts,
  }));

  const enrolledContacts = accounts;

  return {
    enrolledContacts,
    enrolledContactsLoading,
  };
};

export default getEnrolledContactsList;
