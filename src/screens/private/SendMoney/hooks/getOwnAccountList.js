/* eslint-disable react-hooks/rules-of-hooks */
/**
 * @flow
 */ //ducks

import {useEffect} from 'react';
import type {Account} from '@ducks/sendMoney/types';
//Third-party libraries
import {useSelector, useDispatch} from 'react-redux';
import {getSourceAccounts} from '@ducks/sendMoney/actions';

const getOwnAccountList = (
  sourceAccount: Object,
): {
  allSourceAccounts: Array<Account>,
  sourceAccountsLoading: boolean,
} => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getSourceAccounts());
  }, [dispatch]);

  const {accounts, sourceAccountsLoading} = useSelector((state) => ({
    accounts: state.sendMoney?.sourceAccounts,
    sourceAccountsLoading: state.sendMoney?.isLoadingSourceAccounts,
  }));

  const allSourceAccounts = accounts.filter(
    (account) => account.identifier !== sourceAccount?.identifier,
  );

  return {
    allSourceAccounts,
    sourceAccountsLoading,
  };
};

export default getOwnAccountList;
