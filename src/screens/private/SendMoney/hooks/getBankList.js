/* eslint-disable react-hooks/rules-of-hooks */
/**
 * @flow
 */ //ducks

import {useEffect} from 'react';
import type {Account} from '@ducks/sendMoney/types';
//Third-party libraries
import {useSelector, useDispatch} from 'react-redux';
import {getBanks} from '@ducks/sendMoney/actions';

const getBankList = (): {
  bankList: Array<Account>,
  bankListLoading: boolean,
} => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBanks());
  }, [dispatch]);

  const {banks, bankListLoading} = useSelector((state) => ({
    banks: state.sendMoney?.enrolledContacts,
    bankListLoading: state.sendMoney?.isLoadingEnrolledContacts,
  }));

  const bankList = banks;

  return {
    bankList,
    bankListLoading,
  };
};

export default getBankList;
