/**
 * @flow
 */

export const SCHEDULE_TYPE = Object.freeze({
  IMMEDIATE: 'SCHEDULE_TYPE_IMMEDIATE',
  SCHEDULED: 'SCHEDULE_TYPE_SCHEDULED',
});

export const RECIPIENT_TYPE = Object.freeze({
  OWN: 'O',
  ENROLLED_CONTACT: 'E',
  UNENROLLED_CONTACT: 'U',
});

export const OCCURRENCE_TYPE = Object.freeze({
  ONE_TIME: 'ONE_TIME',
  REPEATING: 'REPEATING',
});

export const SCHEDULE_END_TYPE = Object.freeze({
  ENDS_ON: 'ENDS_ON',
  ENDS_AFTER: 'ENDS_AFTER',
});
