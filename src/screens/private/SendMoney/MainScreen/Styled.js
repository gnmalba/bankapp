/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {verticalScale} from '@utils/scale';

export const Content: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs(() => ({
  padding: 32,
  keyboardShouldPersistTaps: 'handled',
  bounces: false,
  showsVerticalScrollIndicator: false,
  enableOnAndroid: true,
  contentContainerStyle: {
    paddingBottom: verticalScale(72),
  },
  flex: 1,
}))``;
