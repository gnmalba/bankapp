/**
 * @flow
 */

import React from 'react';

import Layout from '@components/Layout';

import SendMoneyForm from './components/SendMoneyForm';

import {Content} from './Styled';

const MainScreen = (): React$Node => {
  return (
    <Layout>
      <Content>
        <SendMoneyForm />
      </Content>
    </Layout>
  );
};

export default MainScreen;
