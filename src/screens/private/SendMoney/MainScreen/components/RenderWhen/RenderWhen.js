/**
 * @flow
 */
type Props = {
  condition: boolean,
  children: React$Node,
};
/**
 * Component that renders its children when the condition prop is truthy
 * @example
 * <RenderWhen condition={isLoading}>
 *  <Loader />
 * </RenderWhen>
 */
const RenderWhen = (props: Props): React$Node | null => {
  const {condition, children} = props;

  return condition ? children : null;
};

export default RenderWhen;
