/**
 * @flow
 */

export type Option = {
  label: string,
  value: string,
};

export type Props = {
  label: string,
  error?: string,
  options: Array<Option>,
  selectedOption?: string,
  onSelect?: (option: string) => void,
};
