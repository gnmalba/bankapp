/**
 * @flow
 */

import React from 'react';

import FieldError from '../FieldError';
import FieldLabel from '../FieldLabel';
import RadioButton from '../RadioButton';

import {
  RootContainer,
  RadioButtonContainer,
  RadioButtonsContainer,
} from './Styled';
import type {Props} from './types';

const RadioButtonGroup = (props: Props): React$Node => {
  const {
    label = '',
    error = '',
    options = [],
    selectedOption = '',
    onSelect,
  } = props;
  return (
    <RootContainer>
      <FieldLabel label={label} />
      <RadioButtonsContainer>
        {options.map((option) => (
          <RadioButtonContainer>
            <RadioButton
              checked={selectedOption === option.value}
              label={option.label}
              onPress={onSelect ? () => onSelect(option.value) : undefined}
            />
          </RadioButtonContainer>
        ))}
      </RadioButtonsContainer>
      <FieldError error={error} />
    </RootContainer>
  );
};

export default RadioButtonGroup;
