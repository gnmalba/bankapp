/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const RootContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: column;
`;

export const RadioButtonsContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const RadioButtonContainer: ComponentType<any> = styled.View`
  flex: 50%;
  padding-vertical: 8px;
`;
