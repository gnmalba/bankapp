/**
 * @flow
 */

import React from 'react';
import {useFormikContext} from 'formik';

import {OCCURRENCE_TYPE} from '../../../constants';

import DateInput from '../DateInput';
import RadioButtonGroup from '../RadioButtonGroup';
import RenderWhen from '../RenderWhen';
import FrequencySelect from '../FrequencySelect';
import EndDateInput from '../EndDateInput';

import {Spacer} from './Styled';

const ScheduledFields = (): React$Node => {
  const formikContext = useFormikContext();
  const {values, setFieldValue} = formikContext;
  const isRepeating = values.occurrenceType === OCCURRENCE_TYPE.REPEATING;
  const dateInputLabel = isRepeating ? 'Start Date' : 'Payment Date and Time';

  return (
    <React.Fragment>
      <Spacer height={8} />
      <RadioButtonGroup
        label="Occurrence"
        options={[
          {label: 'One Time', value: OCCURRENCE_TYPE.ONE_TIME},
          {label: 'Repeating', value: OCCURRENCE_TYPE.REPEATING},
        ]}
        onSelect={(option) => setFieldValue('occurrenceType', option)}
        selectedOption={values.occurrenceType}
      />
      <Spacer height={12} />
      <DateInput
        label={dateInputLabel}
        placeholder="MM/DD/YYYY, hh:mm AM/PM"
        value={values.schedule.startDate}
        onChange={(newValue) => setFieldValue('schedule.startDate', newValue)}
        onClear={() => setFieldValue('schedule.startDate', '')}
      />
      <RenderWhen condition={isRepeating}>
        <FrequencySelect
          onChange={(frequency) =>
            setFieldValue('schedule.frequency', frequency.type)
          }
        />
        <EndDateInput label="Ends" formikContext={formikContext} />
      </RenderWhen>
    </React.Fragment>
  );
};

export default ScheduledFields;
