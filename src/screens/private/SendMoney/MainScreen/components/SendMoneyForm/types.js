/**
 * @flow
 */
import FREQUENCY_TYPE from '@enums/frequency';
import {
  SCHEDULE_TYPE,
  RECIPIENT_TYPE,
  OCCURRENCE_TYPE,
  SCHEDULE_END_TYPE,
} from '../../../constants';

export type AccountSelectNavigators = {
  selectSourceAccount: Function,
  selectRecipientAccount: Function,
};

// Not the official type definition for FormikContextType (formik uses TypeScript and does not have type definitions for Flow)
export type FormikContextType = {
  values: Object,
  setFieldValue: Function,
  handleSubmit: Function,
};

export type Account = {
  alias: string,
  identifier: string,
  isCpc: boolean,
  accountNo: string,
  availableBalanceValueWithCurrency: string,
  currencyCode: string,
  currentBalanceValueWithCurrency: string,
  customerId: string,
  id: number,
};

export type FormValues = {
  sourceAccount: Account | null,
  amount: string,
  recipientAccount: Account | null,
  scheduleType: $Values<typeof SCHEDULE_TYPE>,
  recipientType: $Values<typeof RECIPIENT_TYPE>,
  notes: string,
  amountCurrency: string,
  schedule: {
    frequency: $Values<typeof FREQUENCY_TYPE> | null,
    startDate: string,
    numberOfOccurrences: string | null,
  },
  occurrenceType: $Values<typeof OCCURRENCE_TYPE>,
  scheduleEndType: $Values<typeof SCHEDULE_END_TYPE>,
  endDate: string,
  fee: string,
};
