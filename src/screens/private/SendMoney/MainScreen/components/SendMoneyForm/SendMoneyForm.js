/**
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import {Formik, useFormikContext} from 'formik';
import {useNavigation} from '@react-navigation/native';

import Button from '@components/Button';
import Text from '@components/Text';
import TRANSFER_SERVICE from '@enums/transferService';

import {
  RECIPIENT_TYPE,
  SCHEDULE_TYPE,
  OCCURRENCE_TYPE,
  SCHEDULE_END_TYPE,
} from '../../../constants';

import AmountInput from '../AmountInput';
import AccountSelect from '../AccountSelect';
import RadioButtonGroup from '../RadioButtonGroup';
import TextField from '../TextField';
import RenderWhen from '../RenderWhen';
import TransferServices from '../TransferServices';

import ScheduledFields from './ScheduledFields';
import {useAccountSelectNavigators, useInitializer} from './hooks';
import mapToFormValues from './mapToFormValues';
import {Spacer} from './Styled';
import type {FormValues} from './types';

const SendMoneyForm = (props: any): React$Node => {
  useInitializer();
  const formikContext = useFormikContext();
  const {values, setFieldValue, handleSubmit} = formikContext;
  const {
    selectSourceAccount,
    selectRecipientAccount,
  } = useAccountSelectNavigators(formikContext);

  const nextButtonState = 'default';

  return (
    <View>
      <AccountSelect
        label="From"
        placeholder="Select source account"
        onPress={selectSourceAccount}
        value={values.sourceAccount}
      />
      <RenderWhen condition={values.sourceAccount?.isCpc}>
        <Text mode="caption">* Processing fee may vary for credit cards</Text>
      </RenderWhen>
      <AmountInput
        label="Amount"
        value={values.amount}
        onChange={(newValue) => setFieldValue('amount', newValue)}
      />
      <Spacer height={32} />
      <AccountSelect
        label="To"
        placeholder="Select an account to send money to"
        onPress={selectRecipientAccount}
        value={values.recipientAccount}
      />
      <RenderWhen
        condition={
          values.recipientType !== RECIPIENT_TYPE.OWN &&
          values.recipientBankName !== 'METROBANK'
        }>
        <TransferServices
          label="Transfer Service"
          value={values.transferService}
        />
      </RenderWhen>

      <RadioButtonGroup
        label="Schedule"
        options={[
          {label: 'Immediate', value: SCHEDULE_TYPE.IMMEDIATE},
          {label: 'Later', value: SCHEDULE_TYPE.SCHEDULED},
        ]}
        onSelect={(option) => setFieldValue('scheduleType', option)}
        selectedOption={values.scheduleType}
      />
      <RenderWhen condition={values.scheduleType === SCHEDULE_TYPE.SCHEDULED}>
        <ScheduledFields />
      </RenderWhen>
      <TextField
        label="Notes"
        optional
        placeholder="Enter Notes"
        value={values.notes}
        onChange={(newValue) => setFieldValue('notes', newValue)}
        onClear={() => setFieldValue('notes', '')}
      />
      <Spacer />
      <Button
        title="Next"
        state={nextButtonState}
        type="primary"
        size="large"
        onPress={handleSubmit}
      />
    </View>
  );
};

const defaultInitialValues: FormValues = {
  sourceAccount: null,
  amount: '',
  amountCurrency: 'PHP',
  recipientAccount: null,
  recipientType: RECIPIENT_TYPE.OWN,
  recipientBankName: '',
  scheduleType: SCHEDULE_TYPE.IMMEDIATE,
  transferService: TRANSFER_SERVICE.INSTAPAY,

  // Scheduled transaction fields
  schedule: {
    frequency: null,
    startDate: '',
    numberOfOccurrences: null,
  },
  occurrenceType: OCCURRENCE_TYPE.ONE_TIME,
  endDate: '',
  scheduleEndType: SCHEDULE_END_TYPE.ENDS_ON,
  notes: '',
  fee: '0.00',
};

const FormWrapper = (props: any): React$Node => {
  const {initialValues = defaultInitialValues} = props;
  const navigation = useNavigation();

  const onSubmit = (values) => {
    const formValues = mapToFormValues(values);

    navigation.navigate('SendMoneyStack', {
      screen: 'ConfirmDetailsScreen',
      params: {
        formValues,
      },
    });
  };

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      onSubmit={onSubmit}>
      {(childProps) => <SendMoneyForm {...childProps} />}
    </Formik>
  );
};

export default FormWrapper;
