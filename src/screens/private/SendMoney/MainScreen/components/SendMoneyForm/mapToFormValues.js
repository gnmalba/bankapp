function filterAccountProperties(account) {
  const {
    alias,
    availableBalanceValueWithCurrency,
    currencyCode,
    customerId,
    id,
    identifier,
    isCpc,
  } = account;

  return {
    alias,
    availableBalanceValueWithCurrency,
    currency: currencyCode,
    customerId,
    id,
    identifier,
    isCpc,
  };
}

export default function mapToFormValues(values) {
  const {sourceAccount, recipientAccount, ...other} = values;

  return {
    sourceAccount: filterAccountProperties(sourceAccount),
    recipientAccount: filterAccountProperties(recipientAccount),
    ...other,
  };
}
