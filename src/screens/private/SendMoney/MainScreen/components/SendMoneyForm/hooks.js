/**
 * @flow
 */

import {useEffect} from 'react';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import {getSourceAccounts, getTransferServices} from '@ducks/sendMoney/actions';

import type {AccountSelectNavigators, FormikContextType} from './types';

export function useInitializer(): void {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getSourceAccounts());
    dispatch(getTransferServices());
  }, [dispatch]);
}

export function useAccountSelectNavigators(
  formikContext: FormikContextType,
): AccountSelectNavigators {
  const {setFieldValue} = formikContext;
  const navigation = useNavigation();

  const updateField = (fieldName) => (account) => {
    setFieldValue(fieldName, account);
    navigation.pop();
  };
  const handleSelectSourceAccount = updateField('sourceAccount');
  const handleSelectRecipientAccount = updateField('recipientAccount');

  const selectSourceAccount = () =>
    navigation.navigate('SendMoneyStack', {
      screen: 'SourceAccountScreen',
      params: {
        formikContext,
        handleSelect: handleSelectSourceAccount,
      },
    });

  const selectRecipientAccount = () =>
    navigation.navigate('SendMoneyStack', {
      screen: 'RecipientScreen',
      params: {
        formikContext,
        handleSelect: handleSelectRecipientAccount,
      },
    });

  return {
    selectSourceAccount,
    selectRecipientAccount,
  };
}
