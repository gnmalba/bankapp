/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Spacer: ComponentType<any> = styled.View`
  height: ${(props) => props.height || 16}px;
`;
