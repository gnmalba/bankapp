/**
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import moment from 'moment';

import DateTimeInputPicker from '@composition/DateTimeInputPicker';
import {getDisabledDates} from '@screens/private/PayBillsCredit/utils/';

import {SCHEDULE_END_TYPE} from '../../../constants';

import FieldLabel from '../FieldLabel';
import OccurrenceInput from '../OccurrenceInput';
import RadioButton from '../RadioButton';

import {
  GridRow,
  LeftColumn,
  RightColumn,
  OccurrenceInputContainer,
} from './Styled';

const EndDateInput = (props: Object): React$Node => {
  const {formikContext, label = '', error = '', onFocus, onBlur} = props;
  const {values, setFieldValue} = formikContext;

  const [inputState, setInputState] = useState('default');
  const [showVisibleCalendarModal, setShowVisibleCalendarModal] = useState(
    false,
  );

  const handleOnFocus = () => {
    setInputState('active');
    if (onFocus) {
      onFocus();
    }
  };

  const handleOnBlur = () => {
    if (error) {
      setInputState('error');
    } else {
      setInputState('default');
    }
    if (onBlur) {
      onBlur();
    }
  };

  useEffect(() => {
    if (error) {
      setInputState('error');
    }
  }, [error]);

  return (
    <View>
      <FieldLabel label={label} />
      <GridRow>
        <LeftColumn>
          <RadioButton
            checked={values.scheduleEndType === SCHEDULE_END_TYPE.ENDS_ON}
            label="On"
            onPress={() =>
              setFieldValue('scheduleEndType', SCHEDULE_END_TYPE.ENDS_ON)
            }
          />
        </LeftColumn>
        <RightColumn>
          <DateTimeInputPicker
            mode="date"
            icon="date"
            placeholder="MM/DD/YYYY"
            iconPressHandler={() => setShowVisibleCalendarModal(true)}
            state={inputState}
            value={values.endDate}
            helperText={error}
            onChangeText={(newValue) => setFieldValue('endDate', newValue)}
            onFocus={handleOnFocus}
            onBlur={handleOnBlur}
            calendarModalProp={{
              showVisibleCalendarModal: showVisibleCalendarModal,
              handleCalendarOnApply: (newValue) => {
                setFieldValue('endDate', newValue);
                setShowVisibleCalendarModal(false);
              },
              handleCalendarOnCancel: () => {
                setShowVisibleCalendarModal(false);
              },
              calendarProps: {
                selectedStartDate: values.endDate,
                allowRangeSelection: false,
                initialDate: values.endDate || moment(),
                disabledDates: getDisabledDates,
              },
            }}
          />
        </RightColumn>
      </GridRow>
      <GridRow>
        <LeftColumn>
          <RadioButton
            checked={values.scheduleEndType === SCHEDULE_END_TYPE.ENDS_AFTER}
            label="After"
            onPress={() =>
              setFieldValue('scheduleEndType', SCHEDULE_END_TYPE.ENDS_AFTER)
            }
          />
        </LeftColumn>
        <RightColumn>
          <OccurrenceInputContainer>
            <OccurrenceInput
              disabled={values.scheduleEndType === SCHEDULE_END_TYPE.ENDS_ON}
              placeholder="--"
              value={values.schedule.numberOfOccurrences}
              onChange={(newValue) =>
                setFieldValue('schedule.numberOfOccurrences', newValue)
              }
            />
          </OccurrenceInputContainer>
        </RightColumn>
      </GridRow>
    </View>
  );
};

export default EndDateInput;
