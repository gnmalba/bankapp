/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import {scale} from '@utils/scale';

export const GridRow: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-bottom: 16px;
`;

export const LeftColumn: ComponentType<any> = styled.View`
  width: ${scale(84)}px;
  padding-right: 16px;
`;

export const RightColumn: ComponentType<any> = styled.View`
  flex: 1;
  width: 50px;
  padding-left: 10px;
`;

export const OccurrenceInputContainer: ComponentType<any> = styled.View`
  flex: 1;
`;
