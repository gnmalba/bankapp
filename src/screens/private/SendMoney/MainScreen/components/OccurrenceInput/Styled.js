/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import {scale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';
import BasicTextInput from '@components/BasicTextInput';
import Text from '@components/Text';

export const RootContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const InputContainer: ComponentType<any> = styled.View``;

export const DescriptionContainer: ComponentType<any> = styled.View``;

export const OccurrenceInputContainer: ComponentType<any> = styled.View``;

export const StyledBasicTextInput: ComponentType<any> = styled(
  BasicTextInput,
).attrs((props) => ({
  nativeProps: {
    style: {
      textAlign: 'center',
      width: IS_DEVICE_TABLET ? scale(25) : scale(62),
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 0,
      paddingBottom: 0,
      color: props.disabled
        ? props.theme.color.DISABLED_STATUS
        : props.theme.color.TEXT_300_HIGH,
    },
  },
}))``;

export const DescriptionText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    customColor: props.theme.textInput.placeholderTextColor,
    textAlign: 'center',
  }),
)`
  margin-left: 20px;
`;
