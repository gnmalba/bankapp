/**
 * @flow
 */

import React, {useState} from 'react';

import {testProps} from '@utils/componentCustomProps';

import {
  RootContainer,
  InputContainer,
  DescriptionContainer,
  StyledBasicTextInput,
  DescriptionText,
} from './Styled';
import type {OccurrenceInputProps} from './types';

const OccurrenceInput = (props: OccurrenceInputProps): React$Node => {
  const {
    value,
    onBlur,
    placeholder,
    onChange,
    onFocus,
    maxLength,
    testID = 'occurrenceInput',
    disabled = false,
  } = props;
  const description = 'occurrences';
  const [inputState, setInputState] = useState('default');

  const handleOnBlur = () => {
    setInputState('default');
    if (onBlur) {
      onBlur();
    }
  };

  const handleOnFocus = () => {
    setInputState('active');
    if (onFocus) {
      onFocus();
    }
  };

  return (
    <RootContainer>
      <InputContainer>
        <StyledBasicTextInput
          disabled={disabled}
          editable={!disabled}
          state={disabled ? 'disabled' : inputState}
          returnKeyType="done"
          onBlur={handleOnBlur}
          placeholder={placeholder}
          keyboardType="decimal-pad"
          value={value}
          onChangeText={onChange}
          onFocus={handleOnFocus}
          maxLength={maxLength}
          {...testProps(testID)}
        />
      </InputContainer>
      <DescriptionContainer>
        <DescriptionText mode="body2">{description}</DescriptionText>
      </DescriptionContainer>
    </RootContainer>
  );
};

export default OccurrenceInput;
