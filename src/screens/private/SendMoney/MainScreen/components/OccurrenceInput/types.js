/**
 * @flow
 */

export type OccurrenceInputProps = {
  value: string,
  onBlur: Function,
  placeholder: string,
  onChange: Function,
  onFocus?: Function,
  maxLength?: number,
  testID?: string,
  disabled?: boolean,
};
