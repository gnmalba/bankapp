/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {scale} from '@utils/scale';

export const RootContainer: ComponentType<any> = styled.View``;

export const TransferServiceContainer: ComponentType<any> = styled.View``;

export const ChecklistContainer: ComponentType<any> = styled.View`
  padding-vertical: 16px;
`;

export const ChecklistItemContainer: ComponentType<any> = styled.View`
  margin-left: 24px;
  display: flex;
  flex-direction: row;
  padding-vertical: 6px;
`;
export const ChecklistItemTextContainer: ComponentType<any> = styled.View`
  flex: 1;
`;
export const ChecklistItemIcon: ComponentType<any> = styled(
  Icon,
).attrs((props) => ({...props, name: 'done'}))`
  font-size: ${scale(16)}px;
  color: ${(props) => props.theme.color.HIGH_EMPHASIS};
  margin-right: 8px;
`;
