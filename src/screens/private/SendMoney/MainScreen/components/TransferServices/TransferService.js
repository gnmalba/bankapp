/**
 * @flow
 */

import React from 'react';

import RadioButton from '../RadioButton';

import Checklist from './Checklist';
import {TransferServiceContainer} from './Styled';

type Props = {
  serviceName: string,
  checklist: Array<any>,
  selected?: boolean,
};

const TransferService = (props: Props): React$Node => {
  const {serviceName = '', checklist = [], selected = false} = props;

  return (
    <TransferServiceContainer>
      <RadioButton label={serviceName} checked={selected} />
      <Checklist data={checklist} />
    </TransferServiceContainer>
  );
};

export default TransferService;
