/**
 * @flow
 */

import React from 'react';
import {useSelector} from 'react-redux';

import {transferServicesSelector} from '@ducks/sendMoney/selectors';

import FieldLabel from '../FieldLabel';

import TransferService from './TransferService';
import {RootContainer} from './Styled';

type Props = {
  label: string,
  optional?: boolean,
  testID?: string,
  value: string,
};

const TransferServices = (props: Props): React$Node => {
  const {
    label = '',
    optional = false,
    testID = 'TransferServices',
    value = '',
  } = props;
  const {transferServices: options} = useSelector(transferServicesSelector);
  return (
    <RootContainer>
      <FieldLabel label={label} optional={optional} testID={testID + 'Label'} />
      {options.map((option) => (
        <TransferService {...option} selected={option.serviceType === value} />
      ))}
    </RootContainer>
  );
};

export default TransferServices;
