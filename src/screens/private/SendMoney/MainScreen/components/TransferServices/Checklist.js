/**
 * @flow
 */

import Text from '@components/Text';
import React from 'react';
import {
  ChecklistContainer,
  ChecklistItemContainer,
  ChecklistItemIcon,
  ChecklistItemTextContainer,
} from './Styled';

type Props = {
  data: Array<any>,
};

const Checklist = (props: Props): React$Node => {
  const {data = []} = props;

  return (
    <ChecklistContainer>
      {data.map((listItem, index) => (
        <ChecklistItemContainer>
          <ChecklistItemIcon />
          <ChecklistItemTextContainer>
            <Text mode="caption" themeColor={'HIGH_EMPHASIS'}>
              {listItem}
            </Text>
          </ChecklistItemTextContainer>
        </ChecklistItemContainer>
      ))}
    </ChecklistContainer>
  );
};

export default Checklist;
