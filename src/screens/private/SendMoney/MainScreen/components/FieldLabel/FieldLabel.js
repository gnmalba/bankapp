/**
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import {testProps} from '@utils/componentCustomProps';

import {RootContainer} from './Styled';

type Props = {
  label: string,
  optional?: boolean,
  testID?: string,
};

const FieldLabel = (props: Props): React$Node => {
  const {label = '', optional = false, testID = 'FieldLabel'} = props;

  return (
    <RootContainer>
      <Text {...testProps(testID)}>{label}</Text>
      {optional ? <Text themeColor="TEXT_300_LOW">{' (Optional)'}</Text> : null}
    </RootContainer>
  );
};

export default FieldLabel;
