/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const RootContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  padding-vertical: 12px;
`;
