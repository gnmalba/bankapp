/**
 * @flow
 */

export type DateInputProps = {
  value: string,
  label: string,
  error?: string,
  testID?: string,
  placeholder: string,
  onChange: Function,
  onClear: Function,
  onFocus?: Function,
  onBlur?: Function,
  mode?: 'dateTime' | 'date',
  textWidth?: string,
};
