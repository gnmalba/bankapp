/**
 * @flow
 */

import React from 'react';
import DateTimeInputPicker from '@composition/DateTimeInputPicker';
import moment from 'moment';

import {getDisabledDates} from '@screens/private/PayBillsCredit/utils';

import FieldError from '../FieldError';

import {RootContainer} from './Styled';
import {useDateState, useInputState, useModalState} from './hooks';
import type {DateInputProps} from './types';

const DateInput = (props: DateInputProps): React$Node => {
  const {
    value,
    label,
    error = '',
    testID = 'DateInput',
    placeholder = '',
    onChange,
    onClear,
    onFocus,
    onBlur,
    mode = 'dateTime',
    textWidth = '80%',
  } = props;

  const {inputState, handleOnFocus, handleOnBlur} = useInputState(
    error,
    onFocus,
    onBlur,
  );

  const {
    showVisibleCalendarModal,
    setShowVisibleCalendarModal,
    showVisibleTimePickerModal,
    setShowVisibleTimePickerModal,
  } = useModalState();

  const {date, setDate, setFormControlValue} = useDateState(onChange);

  return (
    <RootContainer>
      <DateTimeInputPicker
        mode={mode}
        label={label}
        icon="date"
        placeholder={placeholder}
        textWidth={textWidth}
        iconPressHandler={() => setShowVisibleCalendarModal(true)}
        state={inputState}
        value={value}
        onChangeText={onChange}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        handleClear={onClear}
        calendarModalProp={{
          showVisibleCalendarModal: showVisibleCalendarModal,
          handleCalendarOnApply: (calendar) => {
            setDate({...date, dateInput: calendar});
            setShowVisibleCalendarModal(false);
            setShowVisibleTimePickerModal(true);
          },
          handleCalendarOnCancel: () => {
            setShowVisibleCalendarModal(false);
          },
          calendarProps: {
            selectedStartDate: value,
            allowRangeSelection: false,
            initialDate: value || moment(),
            disabledDates: getDisabledDates,
          },
        }}
        timePickerModalProp={{
          timePickerLabel: 'Set Reminder Time',
          onBackdropPress: () => {
            setShowVisibleTimePickerModal(false);
            setFormControlValue();
          },
          showVisibleTimePickerModal: showVisibleTimePickerModal,
          timePickerProps: {
            date: date.timeInput || new Date(),
            onDateChange: (dateTime) => {
              setDate({...date, timeInput: dateTime});
            },
          },
        }}
      />
      <FieldError error={error} testID={testID + 'Error'} />
    </RootContainer>
  );
};

export default DateInput;
