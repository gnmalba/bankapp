/**
 * @flow
 */

import {useState} from 'react';
import moment from 'moment';

type ModalState = {
  showVisibleCalendarModal: boolean,
  setShowVisibleCalendarModal: Function,
  showVisibleTimePickerModal: boolean,
  setShowVisibleTimePickerModal: Function,
};
export function useModalState(): ModalState {
  const [showVisibleCalendarModal, setShowVisibleCalendarModal] = useState(
    false,
  );
  const [showVisibleTimePickerModal, setShowVisibleTimePickerModal] = useState(
    false,
  );

  return {
    showVisibleCalendarModal,
    setShowVisibleCalendarModal,
    showVisibleTimePickerModal,
    setShowVisibleTimePickerModal,
  };
}

type InputState = {
  inputState: 'active' | 'error' | 'default',
  setInputState: Function,
  handleOnFocus: Function,
  handleOnBlur: Function,
};
export function useInputState(
  error: string,
  onFocus: Function,
  onBlur: Function,
): InputState {
  const [inputState, setInputState] = useState('default');
  const handleOnFocus = () => {
    setInputState('active');
    if (onFocus) {
      onFocus();
    }
  };
  const handleOnBlur = () => {
    setInputState(error ? 'error' : 'default');
    if (onBlur) {
      onBlur();
    }
  };

  return {
    inputState,
    setInputState,
    handleOnFocus,
    handleOnBlur,
  };
}

type DateState = {
  date: {
    dateInput: string | null,
    timeInput: string | Date | null,
    dateTimeInput: string | null,
  },
  setDate: Function,
  setFormControlValue: Function,
};
export function useDateState(onChange: Function): DateState {
  const [date, setDate] = useState({
    dateInput: '',
    timeInput: null,
    dateTimeInput: '',
  });

  const setFormControlValue = () => {
    const time = date.timeInput || new Date();
    const dateTime = `${date.dateInput} ${moment(time).format('hh:mm A')}`;

    setDate({...date, timeInput: time, dateTimeInput: dateTime});

    if (onChange) {
      onChange(dateTime);
    }
  };

  return {
    date,
    setDate,
    setFormControlValue,
  };
}
