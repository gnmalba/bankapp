/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const RootContainer: ComponentType<any> = styled.View`
  margin-top: 12px;
`;
