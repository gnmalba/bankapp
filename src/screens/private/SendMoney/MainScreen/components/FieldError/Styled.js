/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const ErrorContainer: ComponentType<any> = styled.View`
  margin-vertical: 16px;
`;
