/**
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import {testProps} from '@utils/componentCustomProps';

import RenderWhen from '../RenderWhen';

import {ErrorContainer} from './Styled';

type Props = {
  error: string,
  testID?: string,
};
const FieldError = (props: Props): React$Node => {
  const {error = '', testID = 'FieldError'} = props;
  return (
    <RenderWhen condition={error}>
      <ErrorContainer>
        <Text {...testProps(testID + 'Error')} themeColor="RED_STATUS">
          {error}
        </Text>
      </ErrorContainer>
    </RenderWhen>
  );
};

export default FieldError;
