/**
 * @flow
 */

import React, {useMemo} from 'react';
import {View} from 'react-native';

import Text from '@components/Text';
import TouchableDisplay from '@composition/TouchableDisplay';
import {Display1} from '@composition/TouchableDisplay/Displays';

import type {Account} from '@ducks/sendMoney/types';

import RenderWhen from '../RenderWhen';

type AccountSelectProps = {
  placeholder: string,
  label: string,
  onPress: Function,
  error?: string,
  value: Account,
};
const AccountSelect = (props: AccountSelectProps): React$Node => {
  const {placeholder = '', label = '', onPress, error = '', value} = props;
  const getInnerContent: React$Node = useMemo(
    () =>
      value
        ? Display1(value?.alias, value?.identifier, {
            title: 'SendMoneySelectSourceAlias',
            subtitle: 'SendMoneySelectSourceCardNumber',
          })
        : null,
    [value],
  );

  return (
    <View>
      <TouchableDisplay
        placeholder={placeholder}
        label={label}
        onPress={onPress}
        content={getInnerContent}
        customStyles={{rootView: {marginBottom: 16}}}
      />
      <RenderWhen condition={error}>
        <Text mode="body2" themeColor="RED_STATUS">
          {error}
        </Text>
      </RenderWhen>
    </View>
  );
};

export default AccountSelect;
