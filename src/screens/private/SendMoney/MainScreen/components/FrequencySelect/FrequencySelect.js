/**
 * @flow
 */

import React, {useState} from 'react';

import FREQUENCY_TYPE from '@enums/frequency';
import SelectInput from '@components/SelectInput';

import {RootContainer} from './Styled';

// Tech debt: due to the way SelectInput was written, the value property is actually just its label.
// The "actual" value is the type property.
const frequencyOptions = [
  {type: FREQUENCY_TYPE.DAILY, value: 'Daily'},
  {type: FREQUENCY_TYPE.WEEKLY, value: 'Weekly'},
  {type: FREQUENCY_TYPE.MONTHLY, value: 'Monthly'},
  {type: FREQUENCY_TYPE.QUARTERLY, value: 'Quarterly'},
  {type: FREQUENCY_TYPE.SEMI_ANNUALLY, value: 'Semi-annually'},
  {type: FREQUENCY_TYPE.ANNUALLY, value: 'Annually'},
];

const FrequencySelect = (props: Object): React$Node => {
  const {onChange} = props;
  const [isOpen, setIsOpen] = useState(false);

  return (
    <RootContainer>
      <SelectInput
        label="Frequency"
        data={frequencyOptions}
        placeholder="Select payment frequency"
        isOpen={isOpen}
        openSetter={setIsOpen}
        setter={onChange}
      />
    </RootContainer>
  );
};

export default FrequencySelect;
