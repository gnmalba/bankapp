/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const RootContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const RadioButtonsContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
`;

export const Radio: ComponentType<any> = styled.View.attrs((props) => {
  const {size = 10, checked = false, theme} = props;
  const radioColor = checked ? theme.color.SECONDARY_DEFAULT : 'transparent';
  return {
    ...props,
    size,
    radioColor,
  };
})`
  height: ${(props) => props.size}px;
  width: ${(props) => props.size}px;
  border-radius: ${(props) => props.size / 2};
  background-color: ${(props) => props.radioColor};
  border-color: ${(props) => props.radioColor};
  border-width: 2px;
`;

export const RadioOutline: ComponentType<any> = styled.View.attrs((props) => {
  const {size = 10, outlineOffset = 10} = props;
  const outlineSize = size + outlineOffset;
  return {
    ...props,
    outlineSize,
  };
})`
  height: ${(props) => props.outlineSize}px;
  width: ${(props) => props.outlineSize}px;
  border-radius: ${(props) => props.outlineSize / 2}px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-color: ${(props) =>
    props.checked
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.TEXT_300_LOW};
  border-width: 2px;
`;

export const LabelContainer: ComponentType<any> = styled.View`
  margin-left: 8px;
`;
