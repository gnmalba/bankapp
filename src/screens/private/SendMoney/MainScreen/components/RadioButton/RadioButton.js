/**
 * @flow
 */

import React from 'react';
import {Pressable} from 'react-native';

import Text from '@components/Text';
import {RootContainer, Radio, RadioOutline, LabelContainer} from './Styled';

type Props = {
  label: string,
  onPress: Function,
  disabled?: boolean,
  testID?: string,
  checked?: boolean,
};

const RadioButton = (props: Props): React$Node => {
  const {
    label = '',
    onPress,
    disabled = false,
    testID = 'AmountInput',
    checked = false,
  } = props;

  return (
    <RootContainer>
      <Pressable testID={testID} onPress={onPress} disabled={disabled}>
        <RadioOutline checked={checked}>
          <Radio checked={checked} />
        </RadioOutline>
      </Pressable>
      <LabelContainer>
        <Text testID={testID + 'Label'}>{label}</Text>
      </LabelContainer>
    </RootContainer>
  );
};

export default RadioButton;
