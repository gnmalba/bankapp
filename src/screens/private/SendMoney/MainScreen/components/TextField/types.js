/**
 * @flow
 */

export type Props = TextInputWithAccessoryProps & {
  label: string,
  optional?: boolean,
};

export type TextInputWithAccessoryProps = {
  error?: string,
  maxLength?: number,
  onBlur?: Function,
  onClear?: Function,
  onChange: Function,
  onFocus?: Function,
  placeholder: string,
  testID?: string,
  value: string,
};

export type AccessoryProps = {
  onPress?: Function,
  error?: string,
};
