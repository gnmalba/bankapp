/**
 * @flow
 */

import React from 'react';

import {testProps} from '@utils/componentCustomProps';

import FieldError from '../FieldError';
import FieldLabel from '../FieldLabel';

import {RootContainer} from './Styled';
import TextInputWithAccessory from './TextInputWithAccessory';
import type {Props} from './types';

const TextField = (props: Props): React$Node => {
  const {
    error = '',
    label = '',
    maxLength,
    onBlur,
    onChange,
    onFocus,
    onClear,
    optional = false,
    placeholder = '',
    testID = 'AmountInput',
    value = '',
  } = props;

  return (
    <RootContainer>
      <FieldLabel label={label} optional={optional} />
      <TextInputWithAccessory
        value={value}
        maxLength={maxLength}
        onBlur={onBlur}
        onChange={onChange}
        onFocus={onFocus}
        placeholder={placeholder}
        {...testProps(testID)}
        onClear={onClear}
        error={error}
      />
      <FieldError error={error} />
    </RootContainer>
  );
};

export default TextField;
