/**
 * @flow
 */

import React from 'react';

import {testProps} from '@utils/componentCustomProps';

import RenderWhen from '../RenderWhen';

import Accessory from './Accessory';
import {TextInput, InputContainer} from './Styled';
import type {TextInputWithAccessoryProps} from './types';

const TextInputWithAccessory = (
  props: TextInputWithAccessoryProps,
): React$Node => {
  const {
    error = '',
    maxLength,
    onBlur,
    onChange,
    onFocus,
    onClear,
    placeholder = '',
    testID = 'AmountInput',
    value = '',
  } = props;

  return (
    <InputContainer>
      <TextInput
        value={value}
        maxLength={maxLength}
        onBlur={onBlur}
        onChangeText={onChange}
        onFocus={onFocus}
        placeholder={placeholder}
        {...testProps(testID)}
      />
      <RenderWhen condition={value}>
        <Accessory onPress={onClear} error={error} />
      </RenderWhen>
    </InputContainer>
  );
};

export default TextInputWithAccessory;
