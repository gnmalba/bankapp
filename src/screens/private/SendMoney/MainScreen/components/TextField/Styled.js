/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Pressable} from 'react-native';
import styled from 'styled-components/native';

import BasicTextInput from '@components/BasicTextInput';

export const RootContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: column;
  padding-vertical: 12px;
`;

export const InputContainer: ComponentType<any> = styled.View`
  flex: 1;
  display: flex;
  flex-direction: row;
  align-items: center;
  border-radius: 4px;
  padding-horizontal: 16px;
  background-color: ${(props) => {
    const {state, theme} = props;
    return theme.textInput?.backgroundColor[state || 'default'];
  }};
`;

export const TextInput: ComponentType<any> = styled(BasicTextInput)`
  flex: 1;
  padding: 0;
`;

export const AccessoryButton: ComponentType<any> = styled(Pressable)`
  padding: 12px;
`;

export const AccessoryIcon: ComponentType<any> = styled.Image.attrs((props) => {
  const {error} = props;
  const source = props.error
    ? require('@assets/icons/warning.png')
    : require('@assets/icons/close.png');

  const tintColor = error
    ? props.theme.color.RED_STATUS
    : props.theme.textInput?.iconColor?.clear;
  return {
    source,
    resizeMode: 'cover',
    tintColor,
  };
})`
  width: 24px;
  height: 24px;
`;
