/**
 * @flow
 */

import React from 'react';

import {AccessoryButton, AccessoryIcon} from './Styled';
import type {AccessoryProps} from './types';

const Accessory = ({
  onPress,
  error = '',
}: AccessoryProps): React$Element<any> => {
  const handleOnPress = !error ? onPress : undefined;
  return (
    <AccessoryButton onPress={handleOnPress}>
      <AccessoryIcon error={error} />
    </AccessoryButton>
  );
};

export default Accessory;
