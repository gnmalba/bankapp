/**
 * @flow
 */

import React from 'react';
import {View} from 'react-native';

import Text from '@components/Text';
import {
  AmountContainer,
  CurrencyContainer,
  Divider,
  InputContainer,
} from './Styled';
import BasicTextInput from '@components/BasicTextInput';
import {testProps} from '@utils/componentCustomProps';

import FieldError from '../FieldError';
import FieldLabel from '../FieldLabel';

import type {Props} from './types';

/**
 * Wrapper for the reusable AmountInput composition
 */

const AmountInput = (props: Props): React$Node => {
  const {
    currency = 'PHP',
    error = '',
    label = '',
    maxLength,
    onBlur,
    onChange,
    onFocus,
    optional = false,
    placeholder = '',
    testID = 'AmountInput',
    value = '',
  } = props;

  return (
    <View>
      <FieldLabel label={label} optional={optional} />
      <InputContainer>
        <CurrencyContainer>
          <Text {...testProps(testID + 'Currency')}>{currency}</Text>
        </CurrencyContainer>
        <Divider {...testProps(testID + 'Divider')} />
        <AmountContainer>
          <BasicTextInput
            returnKeyType="done"
            onBlur={onBlur}
            placeholder={placeholder}
            keyboardType="decimal-pad"
            value={value}
            onChangeText={onChange}
            onFocus={onFocus}
            maxLength={maxLength}
            {...testProps(testID)}
          />
        </AmountContainer>
      </InputContainer>
      <FieldError error={error} testID={testID + 'Error'} />
    </View>
  );
};

export default AmountInput;
