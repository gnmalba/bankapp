/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const InputContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  border-radius: 4px;
  background-color: ${(props) => {
    const {state, theme} = props;
    return theme.textInput?.backgroundColor[state || 'default'];
  }};
`;

export const CurrencyContainer: ComponentType<any> = styled.View`
  padding: 16px;
`;

export const Divider: ComponentType<any> = styled.View`
  width: 1px;
  background-color: ${(props) => props.theme.color.TEXT_300_LOW};
  margin-vertical: 8px;
`;

export const AmountContainer: ComponentType<any> = styled.View`
  flex: 1;
`;
