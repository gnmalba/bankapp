/**
 * @flow
 */

export type Props = {
  currency: string,
  error?: string,
  label: string,
  maxLength?: number,
  onBlur?: Function,
  onChange: Function,
  onFocus?: Function,
  optional?: boolean,
  placeholder: string,
  testID?: string,
  value: string,
};
