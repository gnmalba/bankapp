/**
 * @flow
 */
// React, React Native modules
import React, {useState} from 'react';
import {useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/core';

// Components
import Layout from '@components/Layout';
import Text from '@components/Text';
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';
import {RadioButtonContainer} from './Styled';
import Checkbox from '@components/Checkbox';
import Unenrolled from './components/Unenrolled';
import RenderChildrenWhen from './components/RenderChildrenWhen';
import OwnAccounts from './components/OwnAccounts';
import Enrolled from './components/Enrolled';

// Utils
import {installmentsSelector} from '@ducks/installments/selectors';
import type {Account} from '@ducks/sendMoney/types';
import {filterBySearchText} from './helpers';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import {HeaderContainer} from '@utils/commonStyles';

//hooks
import {
  getOwnAccountList,
  getEnrolledContactsList,
  // getBankList,
} from '../hooks';

import {RECIPIENT_TYPE} from '../constants';

function RecipientScreen({navigation, formControl}: PropTypes): React$Node {
  const route = useRoute();
  const {handleSelect, formikContext} = route.params;
  const {values} = formikContext;
  const {allSourceAccounts, sourceAccountsLoading} = getOwnAccountList(
    values.sourceAccount,
  );
  const {enrolledContacts, enrolledContactsLoading} = getEnrolledContactsList();
  // const {bankList, bankListLoading} = getBankList();
  const [list, setList] = useState(allSourceAccounts);
  const [searchText, setSearchText] = useState('');
  const [selectedRecipientType, setSelectedRecipientType] = useState(
    RECIPIENT_TYPE.OWN,
  );
  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType} = installmentsState;

  const onPress = (account: Account) => {
    handleSelect(account);
  };

  const handleOnChangeSearch = (value: string = ''): void => {
    setSearchText(value);
    setList(filterBySearchText(list, value));
  };

  if (sourceAccountsLoading || enrolledContactsLoading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  return (
    <Layout padding={32}>
      <HeaderContainer marginBottom={41}>
        <Text mode="body1">Select a type of account to send money to.</Text>
      </HeaderContainer>

      <RadioButtonContainer>
        <Checkbox
          title="My accounts"
          onPress={() => {
            setSelectedRecipientType(RECIPIENT_TYPE.OWN);
            setSearchText('');
            setList(allSourceAccounts);
          }}
          checked={selectedRecipientType === RECIPIENT_TYPE.OWN}
          isCheckbox={false}
          accessibilityLabel="OwnAccountOption"
        />
        <Checkbox
          title="Enrolled contacts"
          onPress={() => {
            setSelectedRecipientType(RECIPIENT_TYPE.ENROLLED_CONTACT);
            setSearchText('');
            setList(enrolledContacts);
          }}
          checked={selectedRecipientType === RECIPIENT_TYPE.ENROLLED_CONTACT}
          isCheckbox={false}
          accessibilityLabel="EnrolledAccountOption"
        />
        <Checkbox
          title="Unenrolled account"
          onPress={() => {
            setSelectedRecipientType(RECIPIENT_TYPE.UNENROLLED_CONTACT);
          }}
          checked={selectedRecipientType === RECIPIENT_TYPE.UNENROLLED_CONTACT}
          isCheckbox={false}
          accessibilityLabel="UnenrolledAccountOption"
        />
      </RadioButtonContainer>
      <RenderChildrenWhen
        condition={selectedRecipientType === RECIPIENT_TYPE.OWN}>
        <OwnAccounts
          recipientType={selectedRecipientType}
          handleOnChangeSearch={handleOnChangeSearch}
          list={list}
          searchText={searchText}
          onPress={onPress}
          currentInstallmentType={currentInstallmentType}
        />
      </RenderChildrenWhen>
      <RenderChildrenWhen
        condition={selectedRecipientType === RECIPIENT_TYPE.ENROLLED_CONTACT}>
        <Enrolled
          recipientType={selectedRecipientType}
          handleOnChangeSearch={handleOnChangeSearch}
          list={list}
          searchText={searchText}
          onPress={onPress}
          currentInstallmentType={currentInstallmentType}
        />
      </RenderChildrenWhen>
      <RenderChildrenWhen
        condition={selectedRecipientType === RECIPIENT_TYPE.UNENROLLED_CONTACT}>
        <Unenrolled />
      </RenderChildrenWhen>
    </Layout>
  );
}

export default RecipientScreen;
