/**
 * @flow
 */

// import Text from '@components/Text';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';
import Text from '@components/Text';
import Feather from 'react-native-vector-icons/Feather';

export const EndButtonWrapper: ComponentType<any> = styled.View`
  margin-top: auto;
`;

export const HeaderContainer: ComponentType<any> = styled.View`
  margin-bottom: ${(props) => verticalScale(props.marginBottom || 0)}px;
  margin-top: ${(props) => verticalScale(props.marginTop || 0)}px;
  margin-left: ${(props) => verticalScale(props.marginLeft || 0)}px;
  margin-right: ${(props) => verticalScale(props.marginRight || 0)}px;
`;

export const CardListContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(15)}px;
`;

export const CardListItem: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  padding: ${verticalScale(15)}px 0px;
  border-bottom-width: ${(props) => (props.isLastItem ? '0px' : '1px')};
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
  align-items: flex-end;
`;

export const CardListItemLeftSide: ComponentType<any> = styled.View`
  max-width: 60%;
`;
export const CardListItemRightSide: ComponentType<any> = styled.View`
  margin-left: auto;
  align-items: flex-end;
  flex-direction: row;
`;

export const CardListViewContainer: ComponentType<any> = styled.View``;

export const CardListItemName: ComponentType<any> = styled(Text)`
  margin-bottom: ${verticalScale(4)}px;
`;

export const CardListItemNumber: ComponentType<any> = styled(Text)`
  font-family: 'MyriadPro-Semibold';
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const CardListItemBalanceLabel: ComponentType<any> = styled(Text)`
  font-weight: 700;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  text-transform: uppercase;
  margin-bottom: ${verticalScale(4)}px;
  letter-spacing: 0.85px;
`;

export const EmptyText: ComponentType<any> = styled(Text)`
  align-self: center;
  text-align: center;
  font-weight: 600;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const RadioButtonContainer: ComponentType<any> = styled.View``;

export const StyledIcon: ComponentType<any> = styled(Feather).attrs(
  (props) => ({
    name: 'chevron-right',
    color: props.theme.carousel.indicatorColor,
    size: 30,
  }),
)``;
