/**
 * @flow
 */

type Props = {
  condition: boolean,
  children: React$Element<any> | null,
};

const RenderChildrenWhen = (props: Props): React$Element<any> | null => {
  const {condition, children = null} = props;
  return condition ? children : null;
};

export default RenderChildrenWhen;
