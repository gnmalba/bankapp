import React from 'react';
import {ScrollView} from 'react-native';
import Separator from '@components/Separator';
import SelectInput from '@components/SelectInput';

const Unenrolled = () => (
  <ScrollView showsVerticalScrollIndicator={false}>
    <Separator space={12} />
    <SelectInput
      data={'test'}
      placeholder="Select bank"
      // setter={setBank}
      // selected={getBankByName(bankDataToSelectInput(banks), bankName)}
      label="Bank"
      // openSetter={setDropdownOpen}
      // isOpen={dropdownOpen}
    />
  </ScrollView>
);

export default Unenrolled;
