import React from 'react';
import {ScrollView} from 'react-native';
import Separator from '@components/Separator';
import SearchBox from '@composition/SearchBox';
import CardList from './CardList';

const OwnAccounts = (props): React$Node => {
  const {
    recipientType,
    handleOnChangeSearch,
    list,
    searchText,
    onPress,
    currentInstallmentType,
  } = props;
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <SearchBox
        placeholder={'Search'}
        onChange={handleOnChangeSearch}
        setValue={handleOnChangeSearch}
        value={searchText}
      />
      <Separator space={12} />
      <CardList
        recipientType={recipientType}
        list={list}
        onPress={onPress}
        balanceLabel={
          currentInstallmentType === 'BC'
            ? 'OUTSTANDING BALANCE'
            : 'AVAILABLE BALANCE'
        }
      />
    </ScrollView>
  );
};

export default OwnAccounts;
