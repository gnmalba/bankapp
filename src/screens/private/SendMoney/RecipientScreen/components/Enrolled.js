import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import Separator from '@components/Separator';
import SearchBox from '@composition/SearchBox';
import CardList from './CardList';
import RenderChildrenWhen from './RenderChildrenWhen';
import ReviewItem from '@composition/InstallmentDetail';
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';
import type {Account} from '@ducks/sendMoney/types';
import Button from '@components/Button';

const Enrolled = (props): React$Node => {
  const {
    recipientType,
    handleOnChangeSearch,
    list,
    searchText,
    currentInstallmentType,
  } = props;

  const [selectedEnrolledContact, setSelectedEnrolledContact] = useState('');

  const handlePress = (account: Account) => {
    console.log(`akunt: ${JSON.stringify(account)}`);
    setSelectedEnrolledContact(account);
  };

  const handleNext = (account: Account) => {
    console.log(account);
  };

  const CardListWithSearchBar = () => {
    return (
      <>
        <SearchBox
          placeholder={'Search'}
          onChange={handleOnChangeSearch}
          setValue={handleOnChangeSearch}
          value={searchText}
        />
        <Separator space={12} />
        <CardList
          recipientType={recipientType}
          list={list}
          onPress={handlePress}
          balanceLabel={
            currentInstallmentType === 'BC'
              ? 'OUTSTANDING BALANCE'
              : 'AVAILABLE BALANCE'
          }
        />
      </>
    );
  };

  const EnrolledContactFields = () => {
    return (
      <>
        <Separator space={12} />
        <ReviewItem
          label="CONTACT ALIAS"
          value={toText(
            selectedEnrolledContact.alias,
            'SendMoneyAlias',
            'body2',
          )}
          {...testProps('SendMoneyAlias')}
          alignItems="flex-start"
        />
        <ReviewItem
          label="BANK"
          value={toText(
            selectedEnrolledContact.bankName,
            'SendMoneyBankName',
            'body2',
          )}
          {...testProps('SendMoneyBankName')}
          alignItems="flex-start"
        />
        <ReviewItem
          label="ACCOUNT NO."
          value={toText(
            selectedEnrolledContact.acctNumber ||
              selectedEnrolledContact.accountNo,
            'SendMoneyAcctNo',
            'body2',
          )}
          {...testProps('SendMoneyAcctNo')}
          alignItems="flex-start"
        />
        <ReviewItem
          label="LAST NAME"
          value={toText(
            selectedEnrolledContact.lastName || '',
            'SendMoneyLastName',
            'body2',
          )}
          {...testProps('SendMoneyLastName')}
          alignItems="flex-start"
        />
        <ReviewItem
          label="FIRST NAME"
          value={toText(
            selectedEnrolledContact.firstName || '',
            'SendMoneyFirstName',
            'body2',
          )}
          {...testProps('SendMoneyFirstName')}
          alignItems="flex-start"
        />
        <ReviewItem
          label="MOBILE NO."
          value={toText(
            selectedEnrolledContact.mobileNumber,
            'SendMoneyMobileNo',
            'body2',
          )}
          {...testProps('SendMoneyMobileNo')}
          alignItems="flex-start"
        />
        <ReviewItem
          label="EMAIL ADDRESS"
          value={toText(
            selectedEnrolledContact.emailAddress,
            'SendMoneyEmailAddress',
            'body2',
          )}
          {...testProps('SendMoneyEmailAddress')}
          alignItems="flex-start"
        />
        <Separator space={24} />
        <Button
          title="Next"
          state={'default'}
          type="primary"
          onPress={handleNext}
          labelTestID={{...testProps('Next')}.accessibilityLabel}
          {...testProps('SendMoneyNextButton')}
        />
      </>
    );
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <RenderChildrenWhen condition={!selectedEnrolledContact}>
        <CardListWithSearchBar />
      </RenderChildrenWhen>
      <RenderChildrenWhen condition={selectedEnrolledContact}>
        <EnrolledContactFields />
      </RenderChildrenWhen>
    </ScrollView>
  );
};

export default Enrolled;
