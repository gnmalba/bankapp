/**
 * @format
 * @flow
 */

import Text from '@components/Text';
import React from 'react';
import {Pressable} from 'react-native';
import {
  CardListContainer,
  CardListItem,
  CardListItemBalanceLabel,
  CardListItemLeftSide,
  CardListItemName,
  CardListItemNumber,
  CardListItemRightSide,
  EmptyText,
  StyledIcon,
  CardListViewContainer,
} from '../Styled';
import type {CardListType} from '../types';
import {useDispatch} from 'react-redux';
import {testProps} from '@utils/componentCustomProps';
import {getLastFourDigits} from '@utils/getLastFourDigits';
import {numberFormatClean} from '@utils/numberInputs';
import {RECIPIENT_TYPE} from '../../constants';
import RenderChildrenWhen from './RenderChildrenWhen';

const formatAmount = (currencyType: string, amount: number): string => {
  return `${numberFormatClean(amount || 0)}`;
};

const CardList = (props: CardListType): React$Node => {
  const {list, onPress, dispatchAction, recipientType} = props;
  const dispatch = useDispatch();
  const accountNumberLabel = 'Account Number';

  const handlePress = (item) => {
    if (onPress) {
      const payload: any = {
        alias: item.name,
        availableBalance: item.availableBalanceValueWithCurrency,
        cardNumber: item.accountNo,
        ...item,
      };
      if (dispatchAction) {
        dispatch(dispatchAction(payload));
      }
      onPress(payload);
    }
  };

  return (
    <CardListContainer>
      {list.map((item, index) => {
        const cardNum = item.accountNo ? item.accountNo : item.acctNumber;
        const lastFourDigits = getLastFourDigits(cardNum);
        return (
          <Pressable
            key={`${item.id}-${index}`}
            onPress={() => handlePress(item)}
            {...testProps('acctNumContainer_' + lastFourDigits)}>
            <CardListItem isLastItem={index === list.length - 1}>
              <CardListItemLeftSide>
                <CardListItemName
                  mode="body1"
                  numberOfLines={1}
                  {...testProps('acctNumName_' + lastFourDigits)}>
                  {item.alias}
                </CardListItemName>
                <CardListItemNumber
                  mode="caption"
                  {...testProps('acctNumMasked_' + lastFourDigits)}>
                  {recipientType === RECIPIENT_TYPE.OWN
                    ? cardNum
                    : item.bankName}
                </CardListItemNumber>
              </CardListItemLeftSide>
              <CardListItemRightSide>
                <CardListViewContainer>
                  <CardListItemBalanceLabel
                    mode="overline2"
                    {...testProps('aactNumAvailBalLbl_' + lastFourDigits)}>
                    {recipientType === RECIPIENT_TYPE.OWN
                      ? props.balanceLabel
                      : accountNumberLabel}
                  </CardListItemBalanceLabel>
                  <Text
                    mode="body1"
                    {...testProps('acctNumBalValue_' + lastFourDigits)}>
                    {recipientType === RECIPIENT_TYPE.OWN
                      ? formatAmount(
                          item.currencyCode,
                          item.availableCredit ||
                            item.availableBalanceValueWithCurrency,
                        )
                      : cardNum}
                  </Text>
                </CardListViewContainer>
                <RenderChildrenWhen
                  condition={recipientType === RECIPIENT_TYPE.ENROLLED_CONTACT}>
                  <CardListViewContainer>
                    <StyledIcon />
                  </CardListViewContainer>
                </RenderChildrenWhen>
              </CardListItemRightSide>
            </CardListItem>
          </Pressable>
        );
      })}

      {!list.length && (
        <EmptyText {...testProps('MbCreditScreenSelectiontestID')}>
          No results found.
        </EmptyText>
      )}
    </CardListContainer>
  );
};

export default CardList;
