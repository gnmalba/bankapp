/**
 * @flow
 */
// React, React Native modules
import React, {useState} from 'react';
import {ScrollView} from 'react-native';
import {useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/core';

// Components
import CardList from './components/CardList';
import Layout from '@components/Layout';
import Separator from '@components/Separator';
import Text from '@components/Text';
import SearchBox from '@composition/SearchBox';
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';

// Utils
import {installmentsSelector} from '@ducks/installments/selectors';
import type {Account} from '@ducks/sendMoney/types';
import {filterBySearchText} from './helpers';

// Component Flow Types
import type {PropTypes} from './types';

// Component Style
import {HeaderContainer} from '@utils/commonStyles';

//hooks
import {getOwnAccountList} from '../hooks';

function SourceAccountScreen({navigation, formControl}: PropTypes): React$Node {
  const route = useRoute();
  const {handleSelect} = route.params;
  const {allSourceAccounts, sourceAccountsLoading} = getOwnAccountList();
  const [list, setList] = useState(allSourceAccounts);
  const [searchText, setSearchText] = useState('');
  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType} = installmentsState;

  const onPress = (account: Account) => {
    handleSelect(account);
  };

  const handleOnChangeSearch = (value: string = ''): void => {
    setSearchText(value);
    setList(filterBySearchText(allSourceAccounts, value));
  };

  if (sourceAccountsLoading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  return (
    <Layout padding={32}>
      <HeaderContainer marginBottom={41}>
        <Text mode="body1">Select a source account.</Text>
      </HeaderContainer>
      <SearchBox
        placeholder={'Search'}
        onChange={handleOnChangeSearch}
        setValue={handleOnChangeSearch}
        value={searchText}
      />
      <Separator space={12} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <CardList
          list={list}
          onPress={onPress}
          balanceLabel={
            currentInstallmentType === 'BC'
              ? 'OUTSTANDING BALANCE'
              : 'AVAILABLE BALANCE'
          }
        />
      </ScrollView>
    </Layout>
  );
}

export default SourceAccountScreen;
