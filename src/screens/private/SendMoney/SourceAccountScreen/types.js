/**
 * @flow
 */

export type PropTypes = {
  navigation: Object,
  formControl: Object,
};

export type CardListType = {
  list: any[],
  onPress?: (account: any) => void,
  dispatchAction?: any,
  balanceLabel: 'AVAILABLE BALANCE' | 'OUTSTANDING BALANCE',
  shouldMask?: boolean,
};
