/**
 * @format
 * @flow
 */

import Text from '@components/Text';
import React from 'react';
import {Pressable} from 'react-native';
import {
  CardListContainer,
  CardListItem,
  CardListItemBalanceLabel,
  CardListItemLeftSide,
  CardListItemName,
  CardListItemNumber,
  CardListItemRightSide,
  EmptyText,
} from '../Styled';
import type {CardListType} from '../types';
import {useDispatch} from 'react-redux';
// eslint-disable-next-line no-unused-vars
import {maskingFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import {getLastFourDigits} from '@utils/getLastFourDigits';
// eslint-disable-next-line no-unused-vars
import {numberFormatClean, removeCommas} from '@utils/numberInputs';

const formatAmount = (currencyType: string, amount: number): string => {
  return `${numberFormatClean(amount || 0)}`;
};

const CardList = (props: CardListType): React$Node => {
  // eslint-disable-next-line no-unused-vars
  const {list, onPress, dispatchAction, shouldMask = true} = props;
  const dispatch = useDispatch();

  const handlePress = (item) => {
    if (onPress) {
      const payload: any = {
        alias: item.name,
        availableBalance: item.availableBalanceValueWithCurrency,
        cardNumber: item.accountNo,
        ...item,
      };
      if (dispatchAction) {
        dispatch(dispatchAction(payload));
      }
      onPress(payload);
    }
  };

  return (
    <CardListContainer>
      {list.map((item, index) => {
        const cardNum = item.accountNo ? item.accountNo : item.cardNo;
        const lastFourDigits = getLastFourDigits(cardNum);
        return (
          <Pressable
            key={`${item.id}-${index}`}
            onPress={() => handlePress(item)}
            {...testProps('acctNumContainer_' + lastFourDigits)}>
            <CardListItem isLastItem={index === list.length - 1}>
              <CardListItemLeftSide>
                <CardListItemName
                  mode="body1"
                  numberOfLines={1}
                  {...testProps('acctNumName_' + lastFourDigits)}>
                  {item.alias}
                </CardListItemName>
                <CardListItemNumber
                  mode="caption"
                  {...testProps('acctNumMasked_' + lastFourDigits)}>
                  {/* {maskingFormat(cardNum, shouldMask)} */}
                  {cardNum}
                </CardListItemNumber>
              </CardListItemLeftSide>
              <CardListItemRightSide>
                <CardListItemBalanceLabel
                  mode="overline2"
                  {...testProps('aactNumAvailBalLbl_' + lastFourDigits)}>
                  {props.balanceLabel}
                </CardListItemBalanceLabel>
                <Text
                  mode="body1"
                  {...testProps('acctNumBalValue_' + lastFourDigits)}>
                  {formatAmount(
                    item.currencyCode,
                    item.availableCredit ||
                      item.availableBalanceValueWithCurrency,
                  )}
                </Text>
              </CardListItemRightSide>
            </CardListItem>
          </Pressable>
        );
      })}

      {!list.length && (
        <EmptyText {...testProps('MbCreditScreenSelectiontestID')}>
          No results found.
        </EmptyText>
      )}
    </CardListContainer>
  );
};

export default CardList;
