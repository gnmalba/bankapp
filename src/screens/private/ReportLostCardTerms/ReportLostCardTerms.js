/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';

// Third party libraries
import {useSelector} from 'react-redux';

// Components
import Button from '@components/Button';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Components
import Layout from '@components/Layout';

// Utils
import type {PropsType} from './types';
import {testProps} from '@utils/componentCustomProps';
import * as S from './Styled';

const ReportLostCardTerms = (props: PropsType): React$Node => {
  const {theme} = props;
  const {error, terms, data} = useSelector((state) => state.reportLostCard);

  const termDetails = terms?.Details?.replace(/\\t/g, '\t');

  const renderErrorScreen = () => (
    <ErrorScreen
      theme={theme}
      showBackButton={false}
      heading={'Oops! Something went wrong.'}
      details={
        data?.data?.errors
          ? data?.data?.errors[0]?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      handlePrimaryButton={() => props.navigation.pop()}
      handleSecondaryButton={() => props.navigation.navigate('Dashboard')}
      isSystemError={data?.data?.errors ? false : true}
      buttonContents={
        <Button
          title="Go back to Dashboard"
          type="primary"
          state="default"
          {...testProps('lostCardReportTermsErrorBtn')}
          onPress={() => props.navigation.navigate('Dashboard')}
        />
      }
    />
  );

  if (error) {
    return renderErrorScreen();
  }

  return (
    <Layout padded>
      <SafeAreaView>
        <ScrollView showsVerticalScrollIndicator>
          {terms && (
            <S.Container>
              <S.Title
                {...testProps(
                  'lostCardTermsDescriptiontestId',
                )}>{`${terms?.Description}`}</S.Title>
              <S.Content
                {...testProps(
                  'lostCardTermsContenttestId',
                )}>{`${termDetails?.replace(/\\n/g, '\n')}`}</S.Content>
            </S.Container>
          )}
        </ScrollView>
      </SafeAreaView>
    </Layout>
  );
};

export default ReportLostCardTerms;
