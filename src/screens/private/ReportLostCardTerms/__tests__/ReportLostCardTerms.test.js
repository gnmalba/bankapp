import React from 'react';
import {render} from '@testing-library/react-native';
import ReportLostCardTerms from '../ReportLostCardTerms';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('ReportLostCardTerms', () => {
  const mockStore = () => ({
    reportLostCard: {
      error: false,
      loading: false,
      terms: null,
      data: [],
    },
  });
  const all = render(
    <MockProvider store={mockStore()}>
      <ThemeProvider theme={DARK_THEME}>
        <ReportLostCardTerms />
      </ThemeProvider>
    </MockProvider>,
  );
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    expect(all.toJSON()).toMatchSnapshot();
  });
});
