## ReportLostCardTerms
ReportLostCardTerms component.

# Usage
```js
import ReportLostCardTerms from '@screens/private/ReportLostCardTerms';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
