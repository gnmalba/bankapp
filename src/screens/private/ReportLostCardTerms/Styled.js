/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {scale, verticalScale, moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  margin-horizontal: ${scale(32)}px;
  margin-vertical: ${verticalScale(40)}px;
`;

export const Title: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(20)}px;
  margin-bottom: ${verticalScale(35)}px;
  font-weight: bold;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
`;

export const Content: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(18)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
`;
