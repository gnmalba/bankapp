import {NavigationContext} from '@react-navigation/core';
import {fireEvent, render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import BalanceTransfer from '../BalanceTransfer';
import ConfirmDetails from '../components/ConfirmDetails';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

const MOCK_CARDNUMBER = '521822******7813';
const MOCK_AVAILABLECREDIT = '1000000.00';
const MOCK_NAME = 'JOHN PATRICK A VIRAY';

describe('Balance Transfer', () => {
  const props = {
    route: {
      params: {
        fromCC: true,
      },
    },
    navigation: {
      navigate: jest.fn(),
    },
  };
  const stores = [
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      installments: {
        balanceTransfer: {
          amount: 6000,
          transferFrom: {
            bankName: 'Test bank',
            cardNumber: '4111111111111111',
          },
          transferTo: {
            alias: 'MC Platinum BT D4',
            availableCredit: MOCK_AVAILABLECREDIT,
            cardEmbossedName: MOCK_NAME,
            cardNo: MOCK_CARDNUMBER,
            cardNumber: MOCK_CARDNUMBER,
            cardSerNo: '2686978',
            creditLimit: MOCK_AVAILABLECREDIT,
            currency: 'PHP',
            customerId: '5958',
            flag: '0',
            id: MOCK_CARDNUMBER,
            name: MOCK_NAME,
            number: MOCK_CARDNUMBER,
            outstandingBalance: '0.00',
            primary: 0,
          },
          paymentTerms: {
            annualRate: '12.6077%',
            interestRate: '0.58%',
            monthlyAmortization: '534.80',
            shortCode: 'BT5812M',
            term: '12',
            totalInstallmentAmount: '6417.60',
            transactionFee: '0',
          },
        },
      },
    },
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      installments: {
        balanceTransfer: {},
      },
    },
  ];

  const navContext = {
    isFocused: () => true,
    addListener: jest.fn(() => jest.fn()),
  };
  it.each(stores)('Should work as expected', (store) => {
    const component = render(
      <NavigationContext.Provider value={navContext}>
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <BalanceTransfer {...props} />
          </ThemeProvider>
        </MockProvider>
      </NavigationContext.Provider>,
    );

    expect(component.toJSON()).toMatchSnapshot();

    const nextButton = component.getByTestId('balanceTransferNextButton');
    fireEvent(nextButton, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const paymentTermsTO = component.getByTestId('TO Payment terms');
    fireEvent(paymentTermsTO, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const sourceTO = component.getByTestId('TO From');
    fireEvent(sourceTO, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const ccTO = component.getByTestId('TO To');
    fireEvent(ccTO, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const transferAmountField = component.getByTestId('Transfer amount Field');

    fireEvent(transferAmountField, 'onFocus');
    fireEvent.changeText(transferAmountField, '500');
    fireEvent(transferAmountField, 'onBlur');

    fireEvent.changeText(transferAmountField, '0');
    fireEvent(transferAmountField, 'onBlur');

    fireEvent.changeText(transferAmountField, '9999999');
    fireEvent(transferAmountField, 'onBlur');
  });
});

describe('Confirm Details', () => {
  const props = {
    navigation: {navigate: jest.fn()},
    state: {
      amount: 6000,
      transferFrom: {
        bankName: 'Test bank',
        cardNumber: '4111111111111111',
      },
      transferTo: {
        alias: 'MC Platinum BT D4',
        availableCredit: MOCK_AVAILABLECREDIT,
        cardEmbossedName: MOCK_NAME,
        cardNo: MOCK_CARDNUMBER,
        cardNumber: MOCK_CARDNUMBER,
        cardSerNo: '2686978',
        creditLimit: MOCK_AVAILABLECREDIT,
        currency: 'PHP',
        customerId: '5958',
        flag: '0',
        id: MOCK_CARDNUMBER,
        name: MOCK_NAME,
        number: MOCK_CARDNUMBER,
        outstandingBalance: '0.00',
        primary: 0,
      },
      paymentTerms: {
        annualRate: '12.6077%',
        interestRate: '0.58%',
        monthlyAmortization: '534.80',
        shortCode: 'BT5812M',
        term: '12',
        totalInstallmentAmount: '6417.60',
        transactionFee: '0',
      },
    },
  };

  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ConfirmDetails {...props} />
      </ThemeProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();
  });
});
