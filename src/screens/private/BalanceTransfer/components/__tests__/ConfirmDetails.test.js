import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

import ConfirmDetails from '../ConfirmDetails';

jest.mock('@react-navigation/native', () => {
  const nav = jest.requireActual('@react-navigation/native');
  nav.useNavigation = () => jest.fn();
  return nav;
});

describe('ConfirmDetails', () => {
  it('Should work as expected', () => {
    const mockProps = {
      state: {
        transferFrom: {cardNumber: '', bankName: ''},
        transferTo: {cardEmbossedName: '', cardNumber: ''},
        amount: '',
        paymentTerms: {
          monthlyAmortization: '',
          interestRate: '',
          annualRate: '',
          totalInstallmentAmount: '',
        },
      },
      navigation: {
        navigate: jest.fn(),
      },
    };
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.

    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ConfirmDetails {...mockProps} />
      </ThemeProvider>,
    );

    const el = all.getByTestId('BTReviewScreenFooterTermsConditionText');
    fireEvent(el, 'onPress');

    expect(all.toJSON()).toMatchSnapshot();
  });
});
