import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

import ReviewDetails from '../ReviewDetails';

describe('ReviewDetails', () => {
  it('Should work as expected', () => {
    const mockProps = {
      state: {
        transferFrom: {
          bankName: '',
          cardNumber: '',
        },
        transferTo: {
          cardEmbossedName: '',
          cardNumber: '',
        },
        amount: '',
        paymentTerms: {
          monthlyAmortization: '',
          interestRate: '',
          annualRate: '',
          totalInstallmentAmount: '',
          term: '',
        },
        responseData: {
          Code: '',
        },
        dateTime: '',
      },
    };
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ReviewDetails {...mockProps} />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
