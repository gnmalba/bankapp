/**
 * @format
 * @flow
 */

import {Divider} from '@components/Card/Card';
import Text from '@components/Text';
import InstallmentDetail from '@composition/InstallmentDetail';
import {accountsFormat16} from '@utils/accountNumber';
import {HeaderContainer as Container} from '@utils/commonStyles';
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';
import React from 'react';
import {View} from 'react-native';
import {numberFormatClean} from '@utils/numberInputs';
import {formatEAIR} from '@utils/installments';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';
import Separator from '@components/Separator';

export type PropTypes = {
  state: Object,
  navigation: Object,
};

const ConfirmDetails = ({state, navigation}: PropTypes): React$Node => {
  return (
    <>
      <View>
        <CardDetailWithLabel
          label="transfer from"
          name={state.transferFrom.bankName}
          cardNumber={accountsFormat16(state.transferFrom.cardNumber)}
          nameMode={'body1'}
          numberMode={'caption'}
          testID="BTReviewScreenTransferFromValue"
        />
        <Separator space={12} />
        <CardDetailWithLabel
          label="transfer to"
          name={state.transferTo.cardEmbossedName}
          cardNumber={accountsFormat16(state.transferTo.cardNumber)}
          nameMode={'body1'}
          numberMode={'caption'}
          testID="BTReviewScreenTransferFromValue"
        />
        <Divider {...testProps('BTReviewScreenDivider1')} />
        <InstallmentDetail
          label="principal amount"
          value={toText(
            'PHP ' + numberFormatClean(state.amount),
            'BTReviewScreenPrincipalAmount',
          )}
          {...testProps('BTReviewScreenPrincipalAmount')}
        />
        <InstallmentDetail
          label="monthly amortization"
          value={toText(
            'PHP ' + numberFormatClean(state.paymentTerms.monthlyAmortization),
            'BTReviewScreenMonthlyAmortization',
          )}
          {...testProps('BTReviewScreenMonthlyAmortization')}
        />
        <InstallmentDetail
          label="monthly add-on rate"
          value={toText(
            state.paymentTerms.interestRate,
            'BTReviewScreenInterestRate',
          )}
          {...testProps('BTReviewScreenInterestRate')}
        />
        <InstallmentDetail
          label="effective annual interest rate"
          value={toText(
            formatEAIR(state.paymentTerms.annualRate),
            'BTReviewScreenAnnualRate',
          )}
          {...testProps('BTReviewScreenAnnualRate')}
        />
        <InstallmentDetail
          label="payment period"
          value={toText(
            `${state.paymentTerms.term} months`,
            'BTReviewScreenPaymentPeriod',
          )}
          {...testProps('BTReviewScreenPaymentPeriod')}
        />
        <InstallmentDetail
          label="total installment amount"
          value={toText(
            'PHP ' +
              numberFormatClean(state.paymentTerms.totalInstallmentAmount),
            'BTReviewScreenTotalInstallmentAmount',
          )}
          {...testProps('BTReviewScreenTotalInstallmentAmount')}
        />
      </View>
      <Container
        marginTop={12}
        marginBottom={20}
        {...testProps('BTReviewScreenFooterMessageId')}>
        <Text mode="body2">
          By clicking ‘confirm’, you have read and agreed to the
          <Text
            testID="BTReviewScreenFooterTermsConditionText"
            mode="link1"
            themeColor="SECONDARY_DEFAULT"
            onPress={() => navigation.navigate('TermsConditionScreen')}>
            terms and conditions
          </Text>
          , payment, fees applied to this transaction.
        </Text>
      </Container>
    </>
  );
};

export default ConfirmDetails;
