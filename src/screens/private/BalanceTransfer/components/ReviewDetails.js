/**
 * @format
 * @flow
 */

import {Divider} from '@components/Card/Card';
import InstallmentDetail from '@composition/InstallmentDetail';
import {ReviewDetailsContainer} from '@screens/private/ReviewDetailsScreen/Styled';
import {accountsFormat16} from '@utils/accountNumber';
import {toText} from '@utils/helpers';
import {formatEAIR, renderSuccessDetails} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';
import React from 'react';
import {numberFormatClean} from '@utils/numberInputs';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';
import Separator from '@components/Separator';

export type PropTypes = {
  state: Object,
};

const ReviewDetails = ({state}: PropTypes): React$Node => {
  return (
    <>
      <ReviewDetailsContainer>
        <CardDetailWithLabel
          label="transfer from"
          name={state.transferFrom.bankName}
          nameMode="inputerror"
          cardNumber={accountsFormat16(state.transferFrom.cardNumber)}
          numberMode={'caption'}
          small
          testID="BTConfirmedScreenTransferFromValue"
        />
        <Separator space={12} />
        <CardDetailWithLabel
          label="transfer to"
          name={state.transferTo.cardEmbossedName}
          nameMode="inputerror"
          cardNumber={accountsFormat16(state.transferTo.cardNumber)}
          numberMode={'caption'}
          small
          testID="BTConfirmedScreenTransferToValue"
        />
        <Divider {...testProps('BTConfirmedScreenDivider1')} />
        <InstallmentDetail
          label="principal amount"
          value={toText(
            'PHP ' + numberFormatClean(state.amount),
            'BTConfirmedScreenPrincipalAmount',
            'inputerror',
          )}
          small
          {...testProps('BTConfirmedScreenPrincipalAmount')}
        />
        <InstallmentDetail
          label="monthly amortization"
          value={toText(
            'PHP ' + numberFormatClean(state.paymentTerms.monthlyAmortization),
            'BTConfirmedScreenMonthlyAmortization',
            'inputerror',
          )}
          small
          {...testProps('BTConfirmedScreenMonthlyAmortization')}
        />
        <InstallmentDetail
          label="monthly add-on rate"
          value={toText(
            state.paymentTerms.interestRate,
            'BTConfirmedScreenInterestRate',
            'inputerror',
          )}
          small
          {...testProps('BTConfirmedScreenInterestRate')}
        />
        <InstallmentDetail
          label="effective annual interest rate"
          value={toText(
            formatEAIR(state.paymentTerms.annualRate),
            'BTConfirmedScreenAnnualRate',
            'inputerror',
          )}
          small
          {...testProps('BTConfirmedScreenAnnualRate')}
        />
        <InstallmentDetail
          label="payment period"
          value={toText(
            `${state.paymentTerms.term} months`,
            'BTConfirmedScreenPaymentPeriod',
            'inputerror',
          )}
          small
          {...testProps('BTConfirmedScreenPaymentPeriod')}
        />
        <InstallmentDetail
          label="total installment amount"
          value={toText(
            'PHP ' +
              numberFormatClean(state.paymentTerms.totalInstallmentAmount),
            'BTConfirmedScreenTotalInstallmentAmount',
            'inputerror',
          )}
          small
          {...testProps('BTConfirmedScreenTotalInstallmentAmount')}
        />
        <Divider {...testProps('BTConfirmedScreenDivider2')} />
        {renderSuccessDetails(state, 'BTConfirmedScreen')}
      </ReviewDetailsContainer>
    </>
  );
};

export default ReviewDetails;
