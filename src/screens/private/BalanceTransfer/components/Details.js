// @flow
import React from 'react';
import type {BalanceTransferInstallmentType} from '@ducks/installments/types';
import Layout from '@components/Layout';
import InstallmentDetail from '@composition/InstallmentDetail';
import {toText} from '@utils/helpers';
import Divider from '@components/Divider';
import {accountsFormat16} from '@utils/accountNumber';
import moment from 'moment';
import {StatusText} from '@composition/InstallmentItem/Styled';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatClean} from '@utils/numberInputs';
import {strReplaceAll, formatEAIR} from '@utils/installments';
import {
  FlexGrowScrollView,
  FlexGrowScrollViewChildWrapper,
} from '@utils/commonStyles';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';

const Details = (props: any): React$Node => {
  const item: BalanceTransferInstallmentType = props.route.params.item;

  return (
    <Layout padding={32}>
      <FlexGrowScrollView bounces={false} showsVerticalScrollIndicator={false}>
        <FlexGrowScrollViewChildWrapper>
          <InstallmentDetail
            small
            label="status"
            value={
              <StatusText mode="overline2" status={item.STATUS}>
                {item.STATUS}
              </StatusText>
            }
            {...testProps('BTStatus')}
          />
          <Divider {...testProps('BTDivider1')} />
          <CardDetailWithLabel
            cardNumber={accountsFormat16(item.OTHERBANKCARDNUMBER)}
            label="transfer from"
            name={item.BANKNAME}
            nameMode="inputerror"
            numberMode="caption"
            small
            testID="BTTransferFromValue"
          />
          <CardDetailWithLabel
            cardNumber={accountsFormat16(item.CARDNUMBER)}
            label="transfer to"
            name={item.EmbossingName || 'N/A'}
            nameMode="inputerror"
            numberMode="caption"
            small
            testID="BTTransferToValue"
          />
          <Divider />
          <InstallmentDetail
            small
            label="principal amount"
            value={toText(
              'PHP ' + numberFormatClean(item.AMOUNT),
              'BTPrincipalAmount',
              'inputerror',
            )}
            {...testProps('BTPrincipalAmount')}
          />
          <InstallmentDetail
            small
            label="monthly amortization"
            value={toText(
              'PHP ' + numberFormatClean(item.MONTHLYAMORTIZATION),
              'BTMonthlyAmortization',
              'inputerror',
            )}
            {...testProps('BTMonthlyAmortization')}
          />
          <InstallmentDetail
            small
            label="monthly add-on rate"
            value={toText(item.RATE + ' %', 'BTMonthlyAddOnRate', 'inputerror')}
            {...testProps('BTMonthlyAddOnRate')}
          />
          <InstallmentDetail
            small
            label="effective annual interest rate"
            value={toText(
              formatEAIR(item.EAIR),
              'BTEffectiveAnnualInterestRate',
              'inputerror',
            )}
            {...testProps('BTEffectiveAnnualInterestRate')}
          />
          <InstallmentDetail
            small
            label="payment period"
            value={toText(
              item.TERM + ' months',
              'BTPaymentPeriod',
              'inputerror',
            )}
            {...testProps('BTPaymentPeriod')}
          />
          <InstallmentDetail
            small
            label="total installment amount"
            value={toText(
              'PHP ' + numberFormatClean(item.TOTALINSTALLMENTAMOUNT),
              'BTTotalInstallmentAMount',
              'inputerror',
            )}
            {...testProps('BTTotalInstallmentAMount')}
          />
          <Divider {...testProps('BTDivider2')} />
          <InstallmentDetail
            small
            label="reference number"
            value={toText(
              strReplaceAll(item.REFERENCENUMBER, '', '-'),
              'BTReferenceNumber',
              'inputerror',
            )}
            {...testProps('BTReferenceNumber')}
          />
          <InstallmentDetail
            small
            label="date and time"
            value={toText(
              moment(item.TRANSTATUS_DATECREATED).format(
                'MMM-DD-yyyy [\n]h:mm A [(GMT +8)]',
              ),
              'BTDateAndTime',
              'inputerror',
            )}
            {...testProps('BTDateAndTime')}
          />
        </FlexGrowScrollViewChildWrapper>
      </FlexGrowScrollView>
    </Layout>
  );
};

export default Details;
