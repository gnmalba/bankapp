/**
 * @format
 * @flow
 */

import React, {useCallback, useState, useEffect} from 'react';
import {BackHandler} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';

import type {PropsType} from './types';
import {StyledScrollView} from './Styled';
import Layout from '@components/Layout';
import TouchableDisplay from '@composition/TouchableDisplay';
import Button from '@components/Button';
import AmountInput from '@composition/AmountInput';
import {useDispatch, useSelector} from 'react-redux';
import {installmentsSelector} from '@ducks/installments/selectors';
import {Display1} from '@composition/TouchableDisplay/Displays';
import {
  initializeInstallment,
  setRootScreen,
  updateAmount,
} from '@ducks/installments/actions';
import {accountsFormat16, accountNumberInputMask} from '@utils/accountNumber';
import {
  AMOUNT_INPUT_ERRORS_INSTALLMENTS,
  getCleanCreditList,
  handleGoBack,
  MINIMUM_AMOUNT,
} from '@utils/installments';
import {numberFormatClean, removeCommas} from '@utils/numberInputs';
import {testProps} from '@utils/componentCustomProps';
import {creditSelector} from '@ducks/credit/selectors';
import KeyboardAwareView from '@components/KeyboardAwareView';

const isTransferToDisabled = (data: any[]): boolean => {
  return getCleanCreditList(data).length === 1;
};

const getSourceCreditCardContent = (transferFrom: Object | null): string => {
  if (transferFrom) {
    const cardNumberWithSpacing = accountNumberInputMask(
      transferFrom.cardNumber,
    );
    const subtitle = `CARD NUMBER - ${cardNumberWithSpacing}`;
    return Display1(transferFrom.bankName, subtitle, {
      title: 'From Acct Alias',
      subtitle: 'From Acct Card Num',
    });
  }

  return '';
};

const getToCreditCardContent = (transferTo: Object | null): string => {
  if (transferTo) {
    const subtitle = `CARD NUMBER - ${accountsFormat16(transferTo.cardNumber)}`;
    return Display1(transferTo.cardEmbossedName, subtitle, {
      title: 'To Acct Alias',
      subtitle: 'To Acct Card Num',
    });
  }

  return '';
};

const getPaymentTermsContent = (paymentTerms: Object | null): string => {
  if (paymentTerms) {
    const title = `PHP ${numberFormatClean(
      paymentTerms.monthlyAmortization,
    )} / month`;
    const subtitle = `for ${paymentTerms.term} months (${paymentTerms.interestRate} Monthly Add-on Rate)`;
    return Display1(title, subtitle, {
      title: 'Lbl Payment Terms Currency',
      subtitle: 'Lbl Payment Terms Rate',
    });
  }

  return '';
};

const BalanceTransfer = (props: PropsType): React$Node => {
  const {navigation, route} = props;
  const installmentsState = useSelector(installmentsSelector);
  const {data: creditList} = useSelector(creditSelector);

  const dispatch = useDispatch();

  const {balanceTransfer: btState} = installmentsState;
  const {transferFrom, transferTo, paymentTerms, amount} = btState;

  const [amountError, setAmountError] = useState();
  const [isAmountFocused, setAmountFocused] = useState(false);

  useEffect(() => {
    setAmountError('');
  }, [transferTo]);

  useFocusEffect(
    useCallback(() => {
      dispatch(initializeInstallment('BT', false));
      if (route?.params?.fromCC) {
        dispatch(setRootScreen('overview'));
      }
    }, [dispatch, route]),
  );

  const handleNext = () => {
    navigation.navigate('ConfirmScreen');
  };

  const navigateToPaymentTerms = () => {
    navigation.navigate('PaymentTermsScreen');
  };

  const navigateToSelectSourceScreen = () => {
    navigation.navigate('Installments', {
      screen: 'SelectSourceCardScreen',
    });
  };

  const navigateToSelectCardScreen = () => {
    navigation.navigate('SelectCardScreen');
  };

  const getAmount = amount ? amount : '';

  const onAmountBlur = (value) => {
    setAmountFocused(false);
    let MAX_AMOUNT = -1;
    if (transferTo) {
      MAX_AMOUNT = transferTo.creditLimit * 0.9 - transferTo.outstandingBalance;
    }

    const numValue = Number(removeCommas(value));

    if (numValue === 0) {
      setAmountError(AMOUNT_INPUT_ERRORS_INSTALLMENTS.EMPTY);
    } else if (numValue < MINIMUM_AMOUNT) {
      setAmountError('This value is below the minimum required amount.');
    } else if (numValue > MAX_AMOUNT) {
      setAmountError('This value is over the maximum required amount.');
    }
  };

  const handleFocus = () => {
    setAmountFocused(true);
    setAmountError('');
  };

  const getButtonDisabled = () =>
    !(transferFrom && transferTo && amount && paymentTerms && !isAmountFocused);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        handleGoBack({
          navigation: props.route?.params.navigation,
          route: props.route,
          installmentsState,
          dispatch,
        });

        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [props.route, installmentsState, dispatch]),
  );

  return (
    <>
      <Layout>
        <KeyboardAwareView>
          <StyledScrollView padding={32}>
            <TouchableDisplay
              placeholder="Select a source credit card"
              label="From"
              onPress={navigateToSelectSourceScreen}
              content={getSourceCreditCardContent(transferFrom)}
            />
            <TouchableDisplay
              placeholder="Select a credit card"
              label="To"
              onPress={navigateToSelectCardScreen}
              content={getToCreditCardContent(transferTo)}
              disabled={isTransferToDisabled(creditList)}
            />
            <AmountInput
              placeholder="Enter amount"
              label={'Transfer amount'}
              currency={'PHP'}
              otherProps={{installmentType: 'BT'}}
              dispatchAction={updateAmount}
              value={getAmount}
              errorMessage={amountError}
              onFocus={handleFocus}
              onBlur={onAmountBlur}
            />
            <TouchableDisplay
              placeholder="Select terms of payment"
              label="Payment terms"
              onPress={navigateToPaymentTerms}
              content={getPaymentTermsContent(paymentTerms)}
              disabled={
                !(
                  transferFrom &&
                  transferTo &&
                  amount &&
                  !amountError &&
                  !isAmountFocused
                )
              }
              overrideTestId={{chevron: {...testProps('Payment Terms Arrow')}}}
            />
            <Button
              title="Next"
              state={getButtonDisabled() ? 'disabled' : 'default'}
              type="primary"
              onPress={handleNext}
              labelTestID={{...testProps('Next')}.accessibilityLabel}
              {...testProps('balanceTransferNextButton')}
            />
          </StyledScrollView>
        </KeyboardAwareView>
      </Layout>
    </>
  );
};

export default BalanceTransfer;
