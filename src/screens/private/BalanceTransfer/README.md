## BalanceTransfer
BalanceTransfer component.

# Usage
```js
import BalanceTransfer from '@screens/private/BalanceTransfer';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
