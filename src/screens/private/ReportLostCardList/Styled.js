/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {scale, verticalScale, moderateScale} from '@utils/scale';
import {ImageBackground, TouchableOpacity, StyleSheet} from 'react-native';
import {ListItem} from 'react-native-elements';

const activeSrc = require('@assets/icons/active-circle-icon.png');
const inactiveSrc = require('@assets/icons/inactive-circle-icon.png');

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(32)}px;
  background-color: ${(props) => props.theme.layout.backgroundColor};
  border-radius: ${moderateScale(4)}px;
`;

export const RadioButtonImageBackground: ComponentType<any> = styled(
  ImageBackground,
).attrs((props) => ({
  source: props.isCheck ? activeSrc : inactiveSrc,
  imageStyle: {
    tintColor: props.isCheck
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.TEXT_100_HIGH,
  },
}))`
  width: ${moderateScale(23)}px;
  height: ${moderateScale(23)}px;
  resize-mode: contain;
`;

export const ListItemStyled: ComponentType<any> = styled(ListItem).attrs(
  (props) => ({
    key: props.key,
    bottomDivider: props.borderBottom,
    containerStyle: {
      backgroundColor: 'transparent',
      paddingHorizontal: 0,
      paddingVertical: verticalScale(10),
      borderColor: props.theme.color.TEXT_300_LOW,
    },
  }),
)``;

export const ListItemTouch: ComponentType<any> = styled(TouchableOpacity).attrs(
  (props) => ({
    ...props,
    onPress: props.onPress,
    key: props.key,
    activeOpacity: 1,
  }),
)``;

export const ListTitle: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(16)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const ListSubTitle: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(12)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
`;

type StylesType = {
  imageStyle: Object,
};

export const Styles: StylesType = StyleSheet.create({
  imageStyle: {
    width: scale(50),
    height: scale(34),
  },
});
