import React from 'react';
import {render} from '@testing-library/react-native';
import ReportLostCardList from '../ReportLostCardList';
import MockProvider from '@utils/testFramework';
import {NavigationContext} from '@react-navigation/native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('ReportLostCardList', () => {
  const mockStore = () => ({
    reportLostCard: {
      error: false,
      loading: false,
      card: {
        cardName: 'test',
        cardNumber: '1234',
        currencyCode: 'PHP',
        replacementFee: 2,
      },
      data: undefined,
    },
  });
  const mockStore1 = () => ({
    reportLostCard: {
      error: false,
      loading: true,
      card: {
        cardName: 'test',
        cardNumber: '1234',
        currencyCode: 'PHP',
        replacementFee: 2,
      },
      data: undefined,
    },
  });
  const mockStore2 = () => ({
    reportLostCard: {
      error: true,
      loading: false,
      card: {
        cardName: 'test',
        cardNumber: '1234',
        currencyCode: 'PHP',
        replacementFee: 2,
      },
      data: undefined,
    },
  });
  //mock navigation
  const navContext = {
    isFocused: () => true,
    addListener: jest.fn(() => jest.fn()),
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <ReportLostCardList />
          </NavigationContext.Provider>
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('test component for loading', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore1()}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <ReportLostCardList />
          </NavigationContext.Provider>
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('test component for error', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore2()}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <ReportLostCardList />
          </NavigationContext.Provider>
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
