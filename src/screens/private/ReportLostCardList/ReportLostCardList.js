/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useCallback} from 'react';
import {ScrollView, View, Image} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {ListItem} from 'react-native-elements';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Separator from '@components/Separator';
import Loader from '@composition/Loader';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Ducks
import {
  getLostCardRequested,
  getLostCardDetailsRequested,
  reportLostCardResetErrors,
} from '@ducks/lostcard/actions';

// Utils
import {verticalScale} from '@utils/scale';
import {maskingFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import * as S from './Styled';

const ReportLostCardList = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const {loading, error, data: card} = useSelector(
    (state) => state.reportLostCard,
  );

  const [selectedCard, setSelectedCard] = useState({
    selected: null,
    cardItem: null,
  });

  useFocusEffect(
    useCallback(() => {
      dispatch(reportLostCardResetErrors(false));
      dispatch(getLostCardRequested());
    }, [dispatch]),
  );

  const handleNext = () => {
    props.navigation.navigate('ReportLostCardConfirmation');
    dispatch(getLostCardDetailsRequested({...selectedCard.cardItem}));
    setSelectedCard({
      selected: null,
      cardItem: null,
    });
    // resetAction
  };

  const handleSelectCard = (index, cardI) => {
    setSelectedCard({
      selected: index,
      cardItem: cardI,
    });
  };

  const renderContent = () => (
    <View>
      <Text mode="body1" {...testProps('lostCardTextBody1testId')}>
        Please select a card to report as lost
      </Text>
      <Separator space={verticalScale(16)} />
      <Text
        mode="body2"
        themeColor="TEXT_100_HIGH"
        {...testProps('lostCardTextBody2testId')}>
        CREDIT CARDS
      </Text>
      <Separator space={verticalScale(16)} />
      <ScrollView style={{height: verticalScale(400)}}>
        <View>
          {card?.listCards?.map((cardItem, index) => (
            <S.ListItemTouch
              key={index}
              onPress={() => handleSelectCard(index, cardItem)}
              {...testProps(`${index}listLostCardBtn`)}>
              <S.ListItemStyled
                key={index}
                borderBottom={
                  index >= card?.listCards.length - 1 ? false : true
                }>
                <S.RadioButtonImageBackground
                  isCheck={selectedCard.selected === index}
                />
                <Image
                  {...testProps(`reportLostCardCardButtonIcon_${index}`)}
                  style={[S.Styles.imageStyle]}
                  source={require('@assets/images/credit-card-placeholder.png')}
                />
                <ListItem.Content>
                  <S.ListTitle {...testProps(`${index}EmbossingNametestId`)}>
                    {cardItem.embossingName}
                  </S.ListTitle>
                  <S.ListSubTitle {...testProps(`${index}cardNumbertestId`)}>
                    {maskingFormat(cardItem.cardNo, true)}
                  </S.ListSubTitle>
                </ListItem.Content>
              </S.ListItemStyled>
            </S.ListItemTouch>
          ))}
        </View>
      </ScrollView>
    </View>
  );

  const renderBottom = () => (
    <S.ButtonContainer>
      <Button
        size="large"
        type="primary"
        state={selectedCard.selected != null ? 'default' : 'disabled'}
        title={'Report'}
        {...testProps('lostCardReportBtn')}
        onPress={handleNext}
      />
    </S.ButtonContainer>
  );

  const renderErrorScreen = () => (
    <ErrorScreen
      theme={theme}
      showBackButton={false}
      heading={'Oops! Something went wrong.'}
      details={
        card?.data?.errors
          ? card?.data?.errors[0]?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      handlePrimaryButton={() => props.navigation.pop()}
      handleSecondaryButton={() => props.navigation.navigate('Dashboard')}
      isSystemError={card?.data?.errors ? false : true}
      buttonContents={
        <Button
          title="Go back to Dashboard"
          type="primary"
          state="default"
          {...testProps('lostCardReportErrorBtn')}
          onPress={() => {
            props.navigation.navigate('Dashboard');
          }}
        />
      }
    />
  );

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }
  if (error) {
    return renderErrorScreen();
  }
  return <SingleScreen contents={renderContent()} bottom={renderBottom()} />;
};

export default ReportLostCardList;
