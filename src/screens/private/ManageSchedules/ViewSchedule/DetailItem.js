/**
 * @flow
 */

import React from 'react';

import {
  DetailMainContainer,
  DetailValueContainer,
  DetailLabel,
  DetailValue,
  DetailExtra,
} from './Styled';

export type DetailProps = {
  label: string,
  value: ?string,
  extraValue?: string,
};

const renderExtraValue = (extraValue) =>
  (extraValue && <DetailExtra>{extraValue}</DetailExtra>) || <></>;

const DetailItem = (detail: DetailProps): React$Node => {
  const {label, value, extraValue} = detail;
  if (value) {
    return (
      <DetailMainContainer>
        <DetailLabel>{label}</DetailLabel>
        <DetailValueContainer>
          <DetailValue>{value}</DetailValue>
          {renderExtraValue(extraValue)}
        </DetailValueContainer>
      </DetailMainContainer>
    );
  }
  return <></>;
};

export default DetailItem;
