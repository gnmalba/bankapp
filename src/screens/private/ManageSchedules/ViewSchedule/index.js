import React from 'react';

// Component
import ScheduleHeader from './ScheduleHeader';
import Layout from '@components/Layout';

import {SectionLabel, Divider, Container, ScheduleScrollView} from './Styled';

import DetailItem from './DetailItem';

const ContactDetailsSection = ({overview = []}): React$Node => {
  return (
    <>
      <SectionLabel>Schedule Details</SectionLabel>
      {overview.map((item) => {
        const {showDivider, ...itemDetails} = item;
        return (
          <>
            <DetailItem {...itemDetails} />
            {showDivider && <Divider />}
          </>
        );
      })}
    </>
  );
};
const ViewSchedule = (props) => {
  const {
    route: {
      params: {data, overview, title},
    },
    navigation,
  } = props;

  const handleNavigateToEdit = () => {
    navigation.navigate('ViewSchedule', {
      schedule: data,
    });
  };

  return (
    <Layout>
      <Container>
        <ScheduleScrollView>
          <ScheduleHeader
            handleNavigateToEdit={handleNavigateToEdit}
            title={title}
          />
          <ContactDetailsSection overview={overview} />
        </ScheduleScrollView>
      </Container>
    </Layout>
  );
};

export default ViewSchedule;
