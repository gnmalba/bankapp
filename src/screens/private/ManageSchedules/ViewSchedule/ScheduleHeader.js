/**
 * @flow
 */

import React from 'react';

// INTERNAL COMPONENTS
import {
  ProfileBox,
  NameBox,
  ContactName,
  EditButton,
  EditIcon,
  EditLabel,
} from './Styled';

// FLOW TYPE ANNOTATIONS
import type {ScheduleHeaderProps} from './types';

const EditContact = (props) => {
  return (
    <EditButton {...props}>
      <EditIcon />
      <EditLabel>Edit Details</EditLabel>
    </EditButton>
  );
};

const ScheduleHeader = (props: ScheduleHeaderProps): React$Node => {
  const {handleNavigateToEdit, title} = props;
  return (
    <ProfileBox>
      <NameBox>
        <ContactName>{title}</ContactName>
        <EditContact onPress={handleNavigateToEdit} />
      </NameBox>
    </ProfileBox>
  );
};

export default ScheduleHeader;
