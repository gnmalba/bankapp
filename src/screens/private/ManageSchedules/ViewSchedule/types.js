/**
 * @flow
 */

export type ScheduleHeaderProps = {
  title: string,
  handleNavigateToEdit: () => {},
};
