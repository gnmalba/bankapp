/**
 * @flow
 */

import styled from 'styled-components/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Text from '@components/Text';

import type {ComponentType} from 'react';

import {moderateScale, scale} from '@utils/scale';

export const Container: any = styled.View`
  display: flex;
  flex: 1;
`;

export const ScheduleScrollView: ComponentType<any> = styled.ScrollView.attrs(
  (props) => ({
    ...props,
  }),
)`
  padding: 24px;
`;

export const SectionLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'h3',
  themeColor: 'TEXT_300_HIGH',
}))``;

export const Divider: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.dividerBgColor};
  height: ${scale(1)}px;
  margin: 16px 0;
`;

export const ProfileBox: ComponentType<any> = styled.View`
  flex-direction: row;
  padding-horizontal: 60px;
  margin-top: 30px;
  margin-bottom: 40px;
  justify-content: center;
  align-items: center;
`;

export const NameBox: ComponentType<any> = styled.View`
  justify-content: center;
`;

export const ContactName: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'h2',
  themeColor: 'TEXT_300_HIGH',
}))`
  margin-bottom: 8px;
`;

export const EditButton: ComponentType<any> = styled.TouchableOpacity`
  flex-direction: row;
  align-content: center;
`;

export const EditIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'edit',
    size: 16,
    color: props.theme?.color.SECONDARY_DEFAULT,
  }),
)`
  margin-right: 10px;
`;

export const EditLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'subtitle',
  themeColor: 'SECONDARY_DEFAULT',
}))``;

/* DETAILS SECTION */

export const DetailMainContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: flex-start;
  margin-bottom: 16px;
  width: 100%;
`;

export const DetailValueContainer: ComponentType<any> = styled.View`
  padding-left: 16px;
`;

export const DetailLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  numberOfLines: 1,
  mode: 'overline1',
  themeColor: 'TEXT_100_HIGH',
}))`
  width: 40%;
`;

export const DetailValue: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'body1',
  themeColor: 'TEXT_300_HIGH',
}))`
  line-height: ${moderateScale(18)}px;
`;

export const DetailExtra: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'overline1',
  themeColor: 'TEXT_100_HIGH',
}))``;
