/**
 * @flow
 */

export type FieldItemProps = {
  label: string,
  value: string,
  additionalValue?: string,
};
