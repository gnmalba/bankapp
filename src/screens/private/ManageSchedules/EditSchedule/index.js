import React from 'react';

// Component
import Layout from '@components/Layout';
import Button from '@components/Button';

import {ContactScrollView, CancelButtonContainer} from './Styled';

import FieldItem from './FieldItem';

const EditSchedule = (overview) => {
  return (
    <Layout>
      <ContactScrollView>
        {overview.map((item) => {
          return (
            <>
              <FieldItem {...item} />
            </>
          );
        })}
        <CancelButtonContainer>
          <Button
            type="primary"
            state={'default'}
            title="Apply"
            onPress={() => console.log('Cancel Button')}
          />
        </CancelButtonContainer>
      </ContactScrollView>
    </Layout>
  );
};

export default EditSchedule;
