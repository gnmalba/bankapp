/**
 * @flow
 */

import React from 'react';

// Types
import type {FieldItemProps} from './types';

// Styled
import {
  FieldItemContainer,
  FieldItemLabel,
  FieldItemValue,
  FieldItemExtraValue,
} from './Styled';

const FieldItem = (props: FieldItemProps): React$Node => {
  const {label, value, additionalValue} = props;
  return (
    <FieldItemContainer>
      <FieldItemLabel>{label}</FieldItemLabel>
      <FieldItemValue>{value}</FieldItemValue>
      {additionalValue && (
        <FieldItemExtraValue>{additionalValue}</FieldItemExtraValue>
      )}
    </FieldItemContainer>
  );
};

export default FieldItem;
