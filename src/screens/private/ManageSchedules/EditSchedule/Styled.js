/**
 * @flow
 */

import {scale, verticalScale} from '@utils/scale';
import styled from 'styled-components/native';
import Text from '@components/Text';

import type {ComponentType} from 'react';

export const Container: any = styled.View`
  flex: 1;
`;

export const FieldItemContainer: any = styled.View`
  margin-bottom: ${verticalScale(32)}px;
`;

export const ContactScrollView: ComponentType<any> = styled.ScrollView.attrs(
  (props) => ({
    ...props,
  }),
)`
  flex: 1;
  padding-vertical: ${verticalScale(40)}px;
  padding-horizontal: ${scale(32.5)}px;
`;

export const FieldItemLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'body2',
  }),
)`
  padding-bottom: 8px;
`;

export const FieldItemValue: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'body1',
  }),
)`
  padding-bottom: 4px;
`;

export const FieldItemExtraValue: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'overline1',
    themeColor: 'TEXT_100_HIGH',
  }),
)``;

export const CancelButtonContainer: any = styled.View`
  margin-bottom: ${verticalScale(60)}px;
`;
