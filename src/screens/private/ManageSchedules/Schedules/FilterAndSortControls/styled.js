/**
 * @flow
 */

import styled from 'styled-components/native';
import IconTooltip from '@components/IconTooltip';
import Text from '@components/Text';

import type {ComponentType} from 'react';

import {verticalScale} from '@utils/scale';
const RADIO_ICON = require('@assets/icons/active-circle-icon.png');
const UNCHECKED_RADIO_ICON = require('@assets/icons/inactive-circle-icon.png');

export const Container: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-end;
  padding-top: ${verticalScale(14)}px;
`;

export const Radio: ComponentType<any> = styled(IconTooltip).attrs((props) => ({
  ...props,
  height: 20,
  width: 20,
  color: props.selected
    ? props.theme.color.SECONDARY_DEFAULT
    : props.theme.color.HIGH_EMPHASIS,
  source: props.selected ? RADIO_ICON : UNCHECKED_RADIO_ICON,
}))`
  margin-right: 10px;
`;

export const RadioLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'body2',
  themeColor: 'TEXT_300_HIGH',
}))``;

export const DropdownOption: ComponentType<any> = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-bottom: 12px;
`;
