/**
 * @flow
 */

import React from 'react';

import DropdownPopover from '@components/DropdownPopover';

import {Container, DropdownOption, Radio, RadioLabel} from './styled';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  filterOption?: string,
  sortOption?: string,
  setFilterOption?: any,
  setSortOption?: any,
};

const FilterOptions = (props: PropsType) => {
  const {filterOption, setFilterOption} = props;
  const options = [
    {value: 'all', label: 'All'},
    {value: 'pending', label: 'Pending'},
    {value: 'in_progress', label: 'In Progress'},
    {value: 'completed', label: 'Completed'},
  ];

  return options.map((option) => {
    return (
      <DropdownOption
        key={`filter-${option.value}`}
        onPress={() => setFilterOption && setFilterOption(option.value)}>
        <Radio selected={filterOption === option.value} />
        <RadioLabel>{option.label}</RadioLabel>
      </DropdownOption>
    );
  });
};

const SortOptions = (props: PropsType) => {
  const {sortOption, setSortOption} = props;
  const options = [
    {value: 'byDateLatestToOldest', label: 'Date: Latest to Oldest'},
    {value: 'byDateOldestToLatest', label: 'Date: Oldest to Latest'},
    {value: 'byNameAtoZ', label: 'Alphabetical: A to Z'},
    {value: 'byNameZtoA', label: 'Alphabetical: Z to A'},
  ];

  return options.map((option) => {
    return (
      <DropdownOption
        key={`sort-${option.value}`}
        onPress={() => setSortOption && setSortOption(option.value)}>
        <Radio selected={sortOption === option.value} />
        <RadioLabel>{option.label}</RadioLabel>
      </DropdownOption>
    );
  });
};

const FilterAndSortControls = (props: PropsType): React$Node => {
  return (
    <Container>
      <DropdownPopover
        dropdownTitle="filter by"
        buttonLabel={'Filter'}
        onClose={() => {}}
        dropdownItems={<FilterOptions {...props} />}
      />
      <DropdownPopover
        dropdownTitle="sort by"
        buttonLabel={'Sort'}
        onClose={() => {}}
        dropdownItems={<SortOptions {...props} />}
      />
    </Container>
  );
};

export default FilterAndSortControls;
