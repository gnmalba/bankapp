/**
 * @flow
 */

import React from 'react';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import {ManageSchedulesTabs} from './Styled';

const ScheduleTab = createMaterialTopTabNavigator().Screen;

import ScheduledSendMoney from './ScheduledSendMoney';
import ScheduledPayBill from './ScheduledPayBill';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: Object,
  route: Object,
};

const Schedules = (props: PropsType): React$Node => {
  return (
    <ManageSchedulesTabs>
      <ScheduleTab name={'Send Money'}>
        {(tabNavProps) => (
          <ScheduledSendMoney {...props} tabNavProps={tabNavProps} />
        )}
      </ScheduleTab>
      <ScheduleTab name={'Pay Bill'}>
        {(tabNavProps) => (
          <ScheduledPayBill {...props} tabNavProps={tabNavProps} />
        )}
      </ScheduleTab>
    </ManageSchedulesTabs>
  );
};

export default Schedules;
