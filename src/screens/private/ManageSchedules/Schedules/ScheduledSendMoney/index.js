/**
 * @flow
 */

import React, {useEffect, useState} from 'react';

import {useSelector, useDispatch} from 'react-redux';

import Layout from '@components/Layout';
import EntityInfoCard from '@composition/EntityInfoCard';
import EntityInfoPlaceholder from '@composition/EntityInfoCard/EntityInfoPlaceholder';

import {
  Container,
  SchedulesList,
  EmptyListContainer,
  EmptyListLabel,
  EmptyListMessage,
  Spacer,
} from '../Styled';
import FilterAndSortControls from '../FilterAndSortControls';

import {isEmpty} from 'lodash';

import type {ScheduledSendMoneyItem} from '@ducks/schedules/types';

const ITEM_PLACEHOLDER = [1, 2, 3];

export type PropsType = {
  navigation: Object,
  route: {
    params: {
      refetch?: Boolean,
    },
  },
};

type RenderItemProps = {
  item: ScheduledSendMoneyItem,
  index: number,
};

const ListEmpty = (props) => {
  const {schedulesList, searchTerm} = props;
  const emptyMessage =
    isEmpty(searchTerm) && schedulesList.length < 1
      ? 'there are no schedules enrolled on your account. Try enrolling a new schedule'
      : '';

  return (
    <EmptyListContainer>
      <EmptyListLabel>No schedules found</EmptyListLabel>
      <EmptyListMessage>{emptyMessage}</EmptyListMessage>
    </EmptyListContainer>
  );
};

const renderListItem = (item, index, props) => {
  const {isLoading, navigation} = props;

  if (isLoading) {
    return (
      <>
        {index === 0 && <Spacer />}
        <EntityInfoPlaceholder />
      </>
    );
  }
  return (
    <>
      {index === 0 && <Spacer />}
      <EntityInfoCard
        {...item}
        onPressCard={() =>
          navigation.navigate('ViewSchedule', {
            schedule: item,
          })
        }
      />
    </>
  );
};

const List = (props) => {
  const dispatch = useDispatch();
  const {schedulesList, isLoading, page, setPage, filterOption} = props;
  const listData = !isLoading ? schedulesList : ITEM_PLACEHOLDER;
  // const listData = !isLoading ? [1, 2, 3] : ITEM_PLACEHOLDER;

  return (
    <SchedulesList
      keyExtractor={(_item, index) => index.toString()}
      data={listData}
      refreshing={false}
      onRefresh={() =>
        dispatch({
          type: 'GET_SCHEDULED_SEND_MONEY_REQUEST',
          payload: {
            pageNumber: 0,
            pageSize: 10,
            status: filterOption,
          },
        })
      }
      renderItem={({item, index}: RenderItemProps) =>
        renderListItem(item, index, props)
      }
      onEndReachedThreshold={1}
      onEndReached={() => setPage(page + 1)}
      ListEmptyComponent={<ListEmpty {...props} />}
    />
  );
};

const SchedulesLayout = (props) => {
  return (
    <Layout>
      <Container>{props.children}</Container>
    </Layout>
  );
};

// when selecting new filter, should also set page to default
const handleFilterOptionChange = ({
  dropdownValue,
  setFilterOption,
  setPage,
}) => {
  setFilterOption(dropdownValue);
  setPage(0);
};

const Schedules = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const schedulesState = useSelector((state) => state.schedules);
  const [filterOption, setFilterOption] = useState('all');
  const [sortOption, setSortOption] = useState('byDateLatestToOldest');
  const [page, setPage] = useState(0);

  const {isLoading, scheduledSendMoneyList} = schedulesState;

  // on filter or page change
  useEffect(() => {
    dispatch({
      type: 'GET_SCHEDULED_SEND_MONEY_REQUEST',
      payload: {
        pageNumber: page,
        pageSize: 10,
        status: filterOption,
      },
    });
  }, [dispatch, page, filterOption]);

  return (
    <SchedulesLayout navigation={props.navigation}>
      <FilterAndSortControls
        filterOption={filterOption}
        setFilterOption={(dropdownValue) =>
          handleFilterOptionChange({
            dropdownValue,
            setFilterOption,
            setPage,
          })
        }
        sortOption={sortOption}
        setSortOption={setSortOption}
      />
      <List
        {...props}
        {...schedulesState}
        page={page}
        filterOption={filterOption}
        isLoading={isLoading}
        schedulesList={scheduledSendMoneyList}
        setPage={setPage}
      />
    </SchedulesLayout>
  );
};

export default Schedules;
