/**
 * @flow
 */
import {FlatList} from 'react-native';

import styled from 'styled-components/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

const Tab = createMaterialTopTabNavigator();
const TabNavigator = Tab.Navigator;
const TabScreen = Tab.Screen;

import Text from '@components/Text';

import Layout from '@components/Layout';

import type {ComponentType} from 'react';
import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  padding-horizontal: ${verticalScale(24)}px;
`;

export const Spacer: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(24)}px;
`;

export const EmptyListContainer: ComponentType<any> = styled.View`
  flex-grow: 1;
  margin-top: ${verticalScale(-24)}px;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export const ListLoaderContainer: ComponentType<any> = styled.View`
  margin: ${verticalScale(24)}px;
`;

export const SchedulesList: ComponentType<any> = styled(FlatList).attrs(
  (props) => ({
    contentContainerStyle: {
      flexGrow: 1,
      backgroundColor: 'transparent',
    },
  }),
)`
  width: 100%;
`;

export const SchedulesLayout: ComponentType<any> = styled(Layout).attrs(
  (props) => ({
    padding: verticalScale(24),
  }),
)`
  padding-bottom: 0px;
`;

export const EmptyListLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'h3',
    themeColor: 'TEXT_300_HIGH',
  }),
)``;

export const EmptyListMessage: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'body2',
    themeColor: 'TEXT_100_HIGH',
  }),
)`
  margin-top: ${verticalScale(16)}px;
  text-align: center;
  padding-horizontal: 20%;
`;

export const FilterSortControls: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(16)}px;
  align-items: flex-end;
`;

export const ManageSchedulesTabs: ComponentType<any> = styled(
  TabNavigator,
).attrs((props) => {
  const {theme} = props;
  return {
    ...props,
    tabBarOptions: {
      allowFontScaling: false,
      activeTintColor: theme.color.TEXT_300_HIGH,
      inactiveTintColor: theme.color.TEXT_100_HIGH,
      labelStyle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 12,
      },
      indicatorContainerStyle: {
        borderBottomColor: theme.color.TEXT_300_LOW,
        borderBottomWidth: 2,
      },
      indicatorStyle: {
        borderBottomColor: theme.isDarkTheme
          ? theme.color.SECONDARY_DEFAULT
          : theme.color.PRIMARY_DEFAULT,
        borderBottomWidth: 2,
        position: 'absolute',
        bottom: -2,
      },
      style: {
        backgroundColor: props.theme.color.BACKGROUND_200,
      },
    },
  };
})`
  background-color: ${(props) => props.theme.color.BACKGROUND_200};
`;

export const ScheduleTab: ComponentType<any> = styled(
  TabScreen,
).attrs((props) => ({...props}))``;
