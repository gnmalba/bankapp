/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: (route: string, any) => void,
  },
  theme?: Object,
};
