/**
 * @format
 * @flow
 */

import React from 'react';
import * as S from './Styled';
import {useSelector} from 'react-redux';
import type {PropsType} from './types';
import {Container} from '@components/Card/Styled';
import {ScrollContainer} from '@screens/private/DepositScreenOverview/Styled';

import Layout from '@components/Layout';
import EnrollButton from '@composition/EnrollButton';
import AccountCard from '@composition/AccountCard';
import EnrollSnackbar from '@composition/EnrollSnackbar';
import ManageAccountSettingsSnackbar from '@screens/private/ManageAccountSettings/components/ManageAccountSettingsSnackbar/ManageAccountSettingsSnackbar';

// temprarily add format here
import {numberToCurrencyFormat} from '@ducks/deposit/utils';
import {maskingFormat} from '@utils/accountNumber';
import {useIsFocused} from '@react-navigation/native';
import {CREDIT_FULL_PAGE_ERROR} from '@utils/constants';

const Credit = (props: PropsType): React$Node => {
  useIsFocused();

  const {navigation} = props;
  const credits = useSelector((state) => state.credit.creditList.data);
  const renderCard = (account, index) => {
    const uniqueIndex = index + 1;
    const isInactive = account.flag === null || account.flag === 'NOT_ALLOWED';
    const isNullFlag = account.flag === null;
    return (
      <S.MainContainer key={`${account.cardSerNo}-${index}`}>
        <Container>
          <AccountCard
            name={account.alias}
            accountNumber={maskingFormat(account.cardNo, true)}
            balanceLabel="OUTSTANDING"
            balanceValue={`${account.currency} ${numberToCurrencyFormat(
              account.outstandingBalance,
            )}`}
            isPrimary={!isInactive && account.primary !== 0}
            onClick={() => {
              if (account.flag === 'NOT_ALLOWED') {
                navigation.navigate('Error Screen', {
                  isSystemError: false,
                  heading: CREDIT_FULL_PAGE_ERROR.header,
                  details: CREDIT_FULL_PAGE_ERROR.body,
                  headingAlignCenter: true,
                });

                return;
              }

              navigation.navigate('CreditScreenOverview', {
                id: account.cardSerNo,
                alias: account.alias,
              });
            }}
            testID={`${uniqueIndex}CreditSavings`}
            isInactive={isInactive}
            disabled={isNullFlag}
            cardType="credit"
          />
        </Container>
      </S.MainContainer>
    );
  };

  return (
    <Layout padded>
      <ScrollContainer>
        {credits.length > 0 &&
          credits.map((account, index) => renderCard(account, index))}
      </ScrollContainer>
      <EnrollButton
        testId="CreditEnrollAccountButtonId"
        onPress={() => navigation && navigation.navigate('Enroll Account')}
      />
      <EnrollSnackbar testId="CreditSuccessEnrollmentSnackbarId" />

      <ManageAccountSettingsSnackbar />
    </Layout>
  );
};

export default Credit;
