/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: column;
  margin-top: ${verticalScale(10)}px;
`;

export const CardContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: 8px;
  padding: 16px;
`;
