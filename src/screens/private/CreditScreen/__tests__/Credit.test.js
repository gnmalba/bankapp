import React from 'react';
import {render} from '@testing-library/react-native';
import Credit from '../Credit';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {NavigationContainer} from '@react-navigation/native';

describe('Credit', () => {
  const store = {
    credit: {
      creditList: {
        data: [],
      },
    },
    manageAccountSettings: {
      snackbar: {
        state: true,
        message: 'test',
      },
    },
    enrollment: {
      showSnackbar: true,
      snackbarMessage: 'test',
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <NavigationContainer>
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <Credit />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
