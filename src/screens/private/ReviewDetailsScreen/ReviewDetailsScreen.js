/**
 * @format
 * @flow
 */

import React, {useCallback} from 'react';
import {ScrollView, BackHandler} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useTheme} from 'styled-components/native';
import {CommonActions, useFocusEffect} from '@react-navigation/native';
import Fontisto from 'react-native-vector-icons/Fontisto';

import Layout from '@components/Layout';
import Button from '@components/Button';
import {installmentsSelector} from '@ducks/installments/selectors';
import {
  clearInstallments,
  setShowReviewHeader,
} from '@ducks/installments/actions';
import {HeaderContainer, Subtitle} from '@utils/commonStyles';
import {scale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import {CloseIconHolder, DetailContainer, Title} from './Styled';
import {TitleContainer} from '../ConfirmDetailsScreen/Styled';
import C2GReviewDetails from '../Cash2Go/components/ReviewDetails';
import BCReviewDetails from '../BalanceConversion/components/ReviewDetails';
import BTReviewDetails from '../BalanceTransfer/components/ReviewDetails';
import {getReviewScreenMessage} from '@utils/installments';

const ReviewDetailsScreen = (props: PropsType): React$Node => {
  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType} = installmentsState;

  const dispatch = useDispatch();

  const handleNext = () => {
    dispatch(setShowReviewHeader(false));

    props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'ViewInstallmentsScreen', params: {fromReview: true}}],
      }),
    );
  };

  const handleClose = () => {
    if (installmentsState.rootScreen === 'soa') {
      props.navigation.navigate('Accounts', {
        screen: 'StatementOfAccountScreen',
      });
    } else if (installmentsState.rootScreen === 'overview') {
      props.navigation.navigate('CreditScreenOverview');
    } else {
      props.navigation.navigate('Dashboard');
      props.navigation.reset({
        index: 0,
        key: null,
        routes: [{name: 'Dashboard'}],
      });
    }

    dispatch(clearInstallments());
  };

  const renderDetails = (): React$Node => {
    if (currentInstallmentType === 'BT') {
      return <BTReviewDetails state={installmentsState.balanceTransfer} />;
    } else if (currentInstallmentType === 'BC') {
      return <BCReviewDetails state={installmentsState.balanceConversion} />;
    } else if (currentInstallmentType === 'C2GO') {
      return <C2GReviewDetails state={installmentsState.cash2Go} />;
    }

    return <></>;
  };

  const theme = useTheme();

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, []),
  );

  return (
    <Layout padding={32}>
      <DetailContainer>
        <ScrollView showsVerticalScrollIndicator={false}>
          <CloseIconHolder>
            <Fontisto
              name="close-a"
              size={scale(12)}
              color={theme.color.TEXT_300_HIGH}
              onPress={handleClose}
              {...testProps('ConfirmedScreenClose')}
            />
          </CloseIconHolder>
          <HeaderContainer marginBottom={10} marginTop={15}>
            <TitleContainer alignItems={'center'}>
              <Title
                mode="h3"
                {...testProps('ConfirmedScreenApplicationSentId')}>
                Application sent!
              </Title>
            </TitleContainer>
            <Subtitle
              mode="body2"
              textAlign="center"
              {...testProps('ConfirmedScreenMessageId')}>
              {getReviewScreenMessage(currentInstallmentType)}
            </Subtitle>
          </HeaderContainer>
          {renderDetails()}
          <Button
            title="View all applications"
            type="primary"
            state="default"
            onPress={handleNext}
            {...testProps('ConfirmedScreenViewAllApplicationsButton')}
          />
        </ScrollView>
      </DetailContainer>
    </Layout>
  );
};

export default ReviewDetailsScreen;
