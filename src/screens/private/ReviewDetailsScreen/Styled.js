/**
 * @flow
 */

import Text from '@components/Text';
import {verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View``;

export const Title: ComponentType<any> = styled(Text)`
  font-weight: 600;
  font-family: 'MyriadPro-Semibold';
`;

export const ReviewDetailsContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  padding: ${verticalScale(16)}px;
  border-radius: 4px;
  margin-bottom: ${verticalScale(24)}px;
  margin-top: ${verticalScale(12)}px;
`;

export const CloseIconHolder: ComponentType<any> = styled.View`
  display: flex;
  align-items: flex-end;
`;

export const DetailContainer: ComponentType<any> = styled.View`
  padding-vertical: ${verticalScale(20)}px;
`;
