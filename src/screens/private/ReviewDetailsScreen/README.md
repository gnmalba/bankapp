## ReviewDetailsScreen
ReviewDetailsScreen component.

# Usage
```js
import ReviewDetailsScreen from '@screens/private/ReviewDetailsScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
