/**
 * @flow
 */
import React from 'react';
import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {scale, verticalScale, moderateScale} from '@utils/scale';
import {TouchableOpacity} from 'react-native';
import {ListItem, Icon} from 'react-native-elements';

export const MenuContainer: ComponentType<any> = styled.View.attrs(() => ({
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.6,
  elevation: 4,
}))`
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  padding-horizontal: ${scale(16)}px;
  border-radius: 8px;
  margin: ${scale(3)}px ${scale(3)}px;
`;

export const ListItemStyled: ComponentType<any> = styled(ListItem).attrs(
  (props) => ({
    key: props.key,
    bottomDivider: props.borderBottom,
    containerStyle: {
      backgroundColor: 'transparent',
      paddingHorizontal: 0,
      paddingVertical: verticalScale(16),
      borderColor: props.theme.color.TEXT_300_LOW,
    },
  }),
)``;

export const ListItemTouch: ComponentType<any> = styled(TouchableOpacity).attrs(
  (props) => ({
    ...props,
    onPress: props.onPress,
    key: props.key,
    activeOpacity: 0.2,
  }),
)``;

export const ListTitle: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(18)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const ArrowIcon: ComponentType<any> = styled((props) => (
  <Icon
    size={26}
    type="octicons"
    name="chevron-right"
    color={
      props.theme === 'dark'
        ? props.theme.color.TEXT_100_HIGH
        : props.theme.color.TEXT_300_LOW
    }
    {...props}
  />
))``;
