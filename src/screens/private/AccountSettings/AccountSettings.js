/**
 * @format
 * @flow
 */

// React modules
import React, {useEffect, useState} from 'react';
import {withTheme} from 'styled-components';
import {ListItem} from 'react-native-elements';

// Third party libraries

// Components
import SingleScreen from '@composition/SingleScreen';

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import * as S from './Styled';
import Snackbar from '@components/Snackbar';
import {timeSnackbar} from '@components/Snackbar/helpers';
import {DEVICE_SETTINGS_CONSTANTS} from '@utils/manageSettings';
import {useSelector} from 'react-redux';
import {manageSettingsSelector} from '@ducks/managesettings/selectors';

const navigations = [
  {
    title: 'Change Password',
    screen: 'AccountSettingsChangePassword',
  },
  {
    title: 'Change Passcode',
    screen: 'ChangePasscodeScreen',
  },
  {
    title: 'Login Settings',
    screen: 'LoginSettingsScreen',
  },
  {
    title: 'Manage Devices',
    screen: 'ManageDevicesScreen',
  },
  {
    title: 'Manage Notifications',
    screen: 'ManageNotificationScreen',
  },
  {
    title: 'Appearance Settings',
    screen: 'AppearanceSettingsScreen',
  },
];

const AccountSettings = (props: PropsType): React$Node => {
  const [snackBarVisible, setSnackbarVisible] = useState(false);
  const {isAppearanceSettingSuccess} = useSelector(manageSettingsSelector);

  useEffect(() => {
    if (isAppearanceSettingSuccess) {
      timeSnackbar(3000, setSnackbarVisible);
    }
  }, [isAppearanceSettingSuccess]);

  const renderContent = () => (
    <S.MenuContainer>
      {navigations.map((navigation, index) => (
        <S.ListItemTouch
          key={index}
          onPress={() => props.navigation.navigate(navigation.screen)}
          {...testProps(`${index}accountSettingsBtn`)}>
          <S.ListItemStyled
            key={index}
            borderBottom={index >= navigations.length - 1 ? false : true}>
            <ListItem.Content>
              <S.ListTitle {...testProps(`${index}AccountSettingTitletestId`)}>
                {navigation.title}
              </S.ListTitle>
            </ListItem.Content>
            <S.ArrowIcon theme={props.theme} />
          </S.ListItemStyled>
        </S.ListItemTouch>
      ))}
    </S.MenuContainer>
  );

  return (
    <>
      <SingleScreen contents={renderContent()} />
      <Snackbar
        visible={snackBarVisible}
        message={DEVICE_SETTINGS_CONSTANTS.SAVE_APPEARANCE_SUCCESS_MESSAGE}
      />
    </>
  );
};

export default (withTheme(AccountSettings): any);
