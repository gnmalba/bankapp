## CreditScreenOverview
CreditScreenOverview component.

# Usage
```js
import CreditScreenOverview from '@screens/private/CreditScreenOverview';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
