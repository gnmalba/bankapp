/**
 * @format
 * @flow
 */

import React, {useCallback} from 'react';

import * as S from './Styled';
import type {PropsType} from './types';
import {View, FlatList, Platform, BackHandler} from 'react-native';
import {numberFormat} from '@utils/accountNumber';

import Layout from '@components/Layout';
import Text from '@components/Text';
import moment from 'moment';

import Loader from '@composition/Loader';
import CardImageBackground from '@composition/CardImageBackground';
import CreditCardDetails from '@composition/CreditCardDetails';
import ActionShortcuts from '@composition/ActionShortcuts';
import CreditInstallment from '@composition/CreditInstallment';
import CreditLatestStatement from '@composition/CreditLatestStatement';
import TransactionHistory from '@composition/TransactionHistory';

import {useDispatch, useSelector} from 'react-redux';
import {CREDIT_SHORTCUTS_DEFAULT} from '@utils/constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {testProps} from '@utils/componentCustomProps';
import {
  getCreditCardDetails,
  getCreditTransactionCurrent,
  getCreditSOALastDetails,
} from '@ducks/credit/actions';

import type {Account} from '@ducks/pay-bills-credit/types';
import {setPbcSourceAccount} from '@ducks/pay-bills-credit/actions';
import {formatAccountObject} from '@ducks/pay-bills-credit/utils';
import {useFocusEffect} from '@react-navigation/core';

const CreditScreenOverview = (props: PropsType): React$Node => {
  const {navigation, route} = props;
  const accountId = route?.params?.id;
  const accountAlias = route?.params?.alias;
  const fromSuppleOverview = route?.params?.fromSuppleOverview || false;

  const dispatch = useDispatch();
  const [activeSlide, setActiveSlide] = React.useState(0);

  const creditDetails = useSelector((state) => state.credit.creditDetails);
  const statementDetails = useSelector((state) => state.soa.soaLastestDetails);
  const creditTransaction = useSelector(
    (state) => state.credit.crediTransactionCurrent,
  );
  const selectedCredit = useSelector((state) => state.credit.selectedCredit);

  React.useEffect(() => {
    if (!fromSuppleOverview) {
      dispatch(getCreditCardDetails(accountId));
      dispatch(getCreditTransactionCurrent(accountId));
      dispatch(getCreditSOALastDetails(accountId));
    }
  }, [accountId, dispatch, fromSuppleOverview]);

  const currency = creditDetails?.data?.currency;
  const balanceAsOf = moment(creditDetails?.data?.balanceAsOf).format('LL');
  const outstandingBalance = `${currency} ${numberFormat(
    creditDetails?.data?.outstandingBalance,
  )}`;

  /*###### PAY BILLS CREDIT ACTIONS #######*/
  const setSourceAccount = (account: Account): any =>
    dispatch(setPbcSourceAccount(account));

  const actionConditions = {
    PAY_BILLS: (item, objectDetails) =>
      setSourceAccount(formatAccountObject(objectDetails)),
    //add other action codes here ex: SEND_MONEY: (item, objectDetails) => {function here}
  };

  const handleActionPress = (item: Object) => {
    actionConditions[item.code] &&
      actionConditions[item.code](item, selectedCredit);
  };

  const detailsScreen = () => (
    <S.DetailsContainer>
      <Text
        mode="overline1"
        themeColor="TEXT_100_HIGH"
        {...testProps('overviewOutstandingBalanceLabel')}>
        Outstanding Balance
      </Text>
      <S.DividerContainer
        {...testProps('overviewOutstandingBalanceLabelDivider')}
      />
      <Text mode="h2" {...testProps('overviewOutstandingBalanceValue')}>
        {outstandingBalance}
      </Text>
      <S.DateLabel {...testProps('overviewAsOfDate')}>
        as of {balanceAsOf}
      </S.DateLabel>
    </S.DetailsContainer>
  );

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (navigation) {
          navigation.goBack();
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation]),
  );

  const renderScreen = () => {
    return (
      <>
        <S.Container>
          <S.CardCarousel
            data={creditDetails.carouselCards}
            setActiveSlide={setActiveSlide}
            renderCard={(item) => (
              <CardImageBackground
                cardType="credit"
                navigation={navigation}
                data={item}
                onPress={() => {
                  item?.index > 0 &&
                    navigation &&
                    navigation.navigate('Accounts', {
                      screen: 'SupplementaryScreen',
                      supplementary: {alias: accountAlias},
                      params: {
                        id: item.item?.cardSerNo,
                        primaryId: accountId,
                      },
                    });
                }}
              />
            )}
          />
          <S.CardPagination
            activeSlide={activeSlide}
            data={creditDetails.carouselCards}
          />

          <View>{detailsScreen()}</View>
          <CreditCardDetails data={creditDetails.data} />
          <ActionShortcuts
            cardType="credit"
            data={CREDIT_SHORTCUTS_DEFAULT}
            onShortCutPress={handleActionPress}
          />
          <CreditLatestStatement
            navigation={navigation}
            accountId={accountId}
            statementDetails={statementDetails}
            overviewData={creditDetails.carouselCards}
          />
          <CreditInstallment navigation={navigation} />
        </S.Container>
        <TransactionHistory
          cardType="credit"
          titleTransaction="Transactions after last statement"
          data={creditDetails.data}
          overviewData={creditDetails.carouselCards}
          transaction={creditTransaction}
          accountId={accountId}
        />
      </>
    );
  };

  return (
    <KeyboardAwareScrollView
      extraHeight={Platform.select({
        ios: 170,
        android: 90,
      })}
      keyboardShouldPersistTaps="always"
      enableOnAndroid>
      <Layout>
        <FlatList
          keyboardShouldPersistTaps="handled"
          data={creditDetails.loader}
          renderItem={renderScreen}
          keyExtractor={(item, index) => index.toString()}
          ListEmptyComponent={() => <Loader isVisible={true} />}
        />
      </Layout>
    </KeyboardAwareScrollView>
  );
};

export default CreditScreenOverview;
