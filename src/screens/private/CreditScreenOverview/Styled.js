/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {ScrollView} from 'react-native';
import {scale, verticalScale} from '@utils/scale';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {WIDTH as screenWidth} from '@utils/constants';

export const MainDetails: ComponentType<any> = styled.View``;
export const Container: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(24)}px;
`;

export const ScrollContainer: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    showsVerticalScrollIndicator: false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
  }),
)``;

export const DetailsContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: ${scale(16)}px;
`;

export const DateLabel: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(14)}px;
  line-height: ${scale(17)}px;
  letter-spacing: ${scale(0.8)}px;
  margin-top: ${verticalScale(5)}px;
  margin-bottom: ${verticalScale(21)}px;
  color: ${(props) => props.theme.color.TEXT_200_MEDIUIM};
`;

export const DividerContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(5)}px;
`;

export const CardCarousel: ComponentType<any> = styled(Carousel).attrs(
  (props) => ({
    scrollEnabled: true,
    data: props.data,
    inactiveSlideOpacity: 1,
    inactiveSlideScale: 1,
    onSnapToItem: (index) => props.setActiveSlide(index),
    renderItem: props.renderCard,
    sliderWidth: screenWidth - scale(30),
    itemWidth: scale(220),
  }),
)``;

export const CardPagination: ComponentType<any> = styled(Pagination).attrs(
  (props) => ({
    activeDotIndex: props.activeSlide,
    dotsLength: props.data.length,
    dotContainerStyle: {marginRight: 0},
    inactiveDotScale: 1,
    dotStyle: {
      width: scale(7),
      height: scale(7),
      borderRadius: scale(3.5),
      backgroundColor: props.theme.color.SECONDARY_DEFAULT,
    },
    inactiveDotOpacity: 1,
    inactiveDotStyle: {backgroundColor: props.theme.carousel.indicatorColor},
  }),
)``;
