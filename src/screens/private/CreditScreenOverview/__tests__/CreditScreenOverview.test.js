import React from 'react';
import {render} from '@testing-library/react-native';
import CreditScreenOverview from '../CreditScreenOverview';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {NavigationContext} from '@react-navigation/native';

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
  };
});

const navContext = {
  isFocused: () => true,
  // addListener returns an unscubscribe function.
  addListener: jest.fn(() => jest.fn()),
};

describe('CreditScreenOverview', () => {
  const store = {
    credit: {
      creditDetails: {},
    },
    soa: {
      soaLastestDetails: {},
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <NavigationContext.Provider value={navContext}>
        {/* ... */}
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <CreditScreenOverview />
          </ThemeProvider>
        </MockProvider>
      </NavigationContext.Provider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
