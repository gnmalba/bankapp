/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: any,
    pop: any,
  },
  route: {
    params: {
      oldpin: string,
    },
  },
  theme?: Object,
};

export type PasscodePropsType = {
  passcode: string,
  onTextChange: Function,
  passcodeError: string,
  handleNext: Function,
  error: string,
  setPasscodeError: Function,
  title?: string,
  subtitle?: string,
  onCancel: Function,
  btnTitle?: string,
  password?: boolean,
};
