## AccountSettingsChangePasscode
AccountSettingsChangePasscode component.

# Usage
```js
import AccountSettingsChangePasscode from '@screens/private/AccountSettingsChangePasscode';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
