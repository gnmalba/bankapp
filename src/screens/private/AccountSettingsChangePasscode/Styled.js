/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {verticalScale, moderateScale} from '@utils/scale';

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(32)}px;
  background-color: ${(props) => props.theme.layout.backgroundColor};
  border-radius: ${moderateScale(4)}px;
`;
