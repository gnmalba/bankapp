/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

// Screens
import Passcode from '@screens/common/Passcode';

// components
import Text from '@components/Text';

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {PasscodePropsType} from './types';

const PasscodeScreen = (props: PasscodePropsType): React$Node => {
  const {
    passcode,
    error,
    onTextChange,
    passcodeError,
    handleNext,
    setPasscodeError,
    title = 'Change Passcode',
    subtitle = 'To change your passcode, please input your old one.',
    onCancel,
    btnTitle = 'Next',
    password = true,
  } = props;

  useEffect(() => {
    if (error) {
      setPasscodeError('Passcode is invalid. Please try again.');
    }
  }, [error, setPasscodeError]);

  return (
    <Passcode
      password={password}
      enableAutomaticScroll={false}
      title={title}
      description={
        <Text
          mode="body2"
          themeColor="TEXT_100_HIGH"
          {...testProps('accountSettingsTextBodyTestId')}>
          {subtitle}
        </Text>
      }
      btnTitle={btnTitle}
      passcode={passcode}
      onTextChange={(text) => onTextChange(text)}
      passcodeError={passcodeError}
      helperContent={<></>}
      onSubmit={handleNext}
      onCancel={onCancel}
    />
  );
};

export default PasscodeScreen;
