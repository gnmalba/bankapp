/**
 * @format
 * @flow
 */

import React, {useCallback, useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/core';
import {BackHandler} from 'react-native';

import type {PropsType} from './types';
import type {ViewInstallmentsRequestType} from '@ducks/installments/types';
import {} from './Styled';
import Layout from '@components/Layout';
import {StyledTabNavigator, TabScreen} from '@components/TabNavigator/Styled';
import {useSelector, useDispatch} from 'react-redux';
import {getInstallments} from '@ducks/installments/actions';
import {
  getCardSerNo,
  getCardSerNoCreditState,
  handleGoBack,
} from '@utils/installments';
import {installmentsSelector} from '@ducks/installments/selectors';
import ListScreen from './components/TabScreen';
import {creditSelector} from '@ducks/credit/selectors';
import Loader from './components/Loader';

const ViewInstallmentsScreen = (props: PropsType): React$Node => {
  const dispatch = useDispatch();

  const installmentsState = useSelector(installmentsSelector);
  const creditState = useSelector(creditSelector);
  const selectedCredit = useSelector((state) => state.credit.selectedCredit);
  const {
    balanceTransfer,
    balanceConversion,
    cash2Go,
    currentInstallmentType,
    allInstallmentsLoading,
    currentFilterInstallmentType,
  } = installmentsState;

  const getState = () => {
    switch (currentInstallmentType) {
      case 'BT':
        return balanceTransfer;
      case 'BC':
        return balanceConversion;
      case 'C2GO':
        return cash2Go;
      default:
        return {};
    }
  };

  const getInitialRouteName = () => {
    switch (currentFilterInstallmentType) {
      case 'BT':
        return 'BALANCE TRANSFER';
      case 'BC':
        return 'BALANCE CONVERSION';
      case 'C2GO':
        return 'CASH2GO';
      default:
        return {};
    }
  };

  useEffect(() => {
    if (!props.route?.params?.fromCC) {
      const viewInstallmentsParams: ViewInstallmentsRequestType = {
        cardSerNo: getCardSerNo(getState()),
        installmentType: 'BC',
      };
      dispatch(getInstallments(viewInstallmentsParams));
    } else {
      const viewInstallmentsParams: ViewInstallmentsRequestType = {
        cardSerNo: getCardSerNoCreditState(creditState.data),
        installmentType: 'BC',
      };
      dispatch(getInstallments(viewInstallmentsParams));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        handleGoBack({
          navigation: props.route?.params.navigation,
          route: props.route,
          installmentsState,
          dispatch,
          selectedCredit,
        });

        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [props.route, installmentsState, dispatch, selectedCredit]),
  );

  const BLList = () => (
    <ListScreen installmentType="BT" navigation={props.navigation} />
  );
  const BCList = () => (
    <ListScreen installmentType="BC" navigation={props.navigation} />
  );
  const C2GList = () => (
    <ListScreen installmentType="C2GO" navigation={props.navigation} />
  );

  return (
    <Layout>
      {allInstallmentsLoading ? (
        <Loader isVisible />
      ) : (
        <StyledTabNavigator initialRouteName={getInitialRouteName()}>
          <TabScreen
            name="BALANCE TRANSFER"
            component={BLList}
            options={{
              tabBarTestID: 'BalanceTransferTabTestId',
              tabBarAccessibilityLabel: 'BalanceTransferTabTestId',
            }}
          />
          <TabScreen
            name="BALANCE CONVERSION"
            component={BCList}
            options={{
              tabBarTestID: 'BalanceConversionTabTestId',
              tabBarAccessibilityLabel: 'BalanceConversionTabTestId',
            }}
          />
          <TabScreen
            name="CASH2GO"
            component={C2GList}
            options={{
              tabBarTestID: 'Cash2GoTabTestId',
              tabBarAccessibilityLabel: 'Cash2GoTabTestId',
            }}
          />
        </StyledTabNavigator>
      )}
    </Layout>
  );
};

export default ViewInstallmentsScreen;
