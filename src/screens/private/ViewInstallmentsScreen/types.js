/**
 * @flow
 */

import type {InstallmentTypesString} from '@ducks/installments/types';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  navigation: Object,
  route?: Object,
};

export type TabScreenProps = {
  installmentType: InstallmentTypesString,
  navigation: Object,
};
