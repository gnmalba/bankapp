/**
 * @flow
 */

import {scale, verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View``;

export const EmptyContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: 60%;
  margin-left: ${verticalScale(32)}px
  margin-right: ${verticalScale(32)}px
`;

/* FOR SCROLLVIEW */

export const ScrollBarContainer: ComponentType<any> = styled.View`
  position: absolute;
  align-self: flex-end;
  justify-content: flex-end;
  right: ${scale(10)}px;
  top: ${verticalScale(42)}px;
`;

export const ScrollLineContainer: ComponentType<any> = styled.View`
  border-top-width: 0;
  position: absolute;
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
  margin-top: ${verticalScale(25)}px;
`;

export const ScrollLine: ComponentType<any> = styled.View`
  position: absolute;
  width: ${scale(4)}px;
  border-radius: 16px;
  z-index: 11;
  background-color: ${(props) => props.theme.color.TEXT_300_LOW};
`;

export const ScrollIndicator: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
  border-radius: 16px;
`;
