import Loader from '@composition/Loader';
import IosModalManager from '@components/IOSModalManager';
import {Platform} from 'react-native';

const Component = Platform.OS === 'android' ? Loader : IosModalManager(Loader);

export default Component;
