// @flow
import Layout from '@components/Layout';
import FilterAndSortInstallments from '@composition/FilterAndSortInstallments';
import InstallmentItem from '@composition/InstallmentItem';
import {creditSelector} from '@ducks/credit/selectors';
import {installmentsSelector} from '@ducks/installments/selectors';
import {getFilterCount} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';
import moment from 'moment';
import React from 'react';
import {FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import type {TabScreenProps} from '../types';
import EmptyState from './EmptyState';
import {
  ScrollBarContainer,
  ScrollIndicator,
  ScrollLine,
  ScrollLineContainer,
} from '../Styled';

const TabScreen = (props: TabScreenProps): React$Node => {
  const installmentsState = useSelector(installmentsSelector);
  const creditState = useSelector(creditSelector);
  const {allInstallments, filters} = installmentsState;

  const [contentOffset, setContentOffset] = React.useState({x: 0, y: 0});
  const [scrollSize, setScrollSize] = React.useState(0);
  const [scrollViewHeight, setScrollViewHeight] = React.useState(4);
  const scrollViewHeightPercentage = 12;
  const scrollPerc =
    (contentOffset.y / (scrollSize - scrollViewHeight)) *
    (100 - scrollViewHeightPercentage);

  const handleCardPress = (item) => {
    if (props.navigation) {
      switch (props.installmentType) {
        case 'BT': {
          props.navigation.navigate('BTDetailsScreen', {item});
          break;
        }
        case 'BC': {
          props.navigation.navigate('BCDetailsScreen', {item});
          break;
        }
        case 'C2GO': {
          props.navigation.navigate('C2GODetailsScreen', {item});
          break;
        }
      }
    }
  };

  const showFilters = (): boolean => {
    const data: any[] = getInstallments();
    const currentFilters = getInstallmentsFilters();
    if (!data) {
      return false;
    }

    if (!currentFilters && data.length === 0) {
      return false;
    }

    return true;
  };

  const renderItem = ({item, index}) => {
    return (
      <InstallmentItem
        onPress={() => handleCardPress(item)}
        amount={item.AMOUNT}
        monthlyAmortization={item.MONTHLYAMORTIZATION}
        duration={item.TERM}
        status={item.STATUS}
        date={moment(item.TRANSTATUS_DATECREATED).format('MMMM DD, YYYY')}
        {...testProps(index + props.installmentType)}
      />
    );
  };

  const getInstallments = (): any[] => {
    switch (props.installmentType) {
      case 'BT':
        return allInstallments.bt;
      case 'BC':
        return allInstallments.bc;
      case 'C2GO':
        return allInstallments.c2g;
      default:
        return [];
    }
  };

  const getInstallmentsFilters = () => {
    if (filters) {
      switch (props.installmentType) {
        case 'BT':
          return filters.bt;
        case 'BC':
          return filters.bc;
        case 'C2GO':
          return filters.c2g;
        default:
          return {};
      }
    }

    return {};
  };

  return (
    <Layout>
      {showFilters() && (
        <FilterAndSortInstallments
          installmentType={props.installmentType}
          creditList={creditState.data}
          numberOfFilters={getFilterCount(getInstallmentsFilters())}
        />
      )}

      <FlatList
        data={getInstallments()}
        renderItem={renderItem}
        keyExtractor={(item) => item.REFERENCENUMBER}
        showsVerticalScrollIndicator={false}
        ListEmptyComponent={<EmptyState />}
        onScroll={({nativeEvent}) => {
          setContentOffset(nativeEvent.contentOffset);
        }}
        onContentSizeChange={(_, height) => {
          setScrollSize(height);
        }}
        onLayout={({nativeEvent}) => {
          setScrollViewHeight(nativeEvent.layout.height);
        }}
        bounces={false}
      />

      <ScrollBarContainer>
        <ScrollLineContainer>
          <ScrollLine style={{height: scrollViewHeight}}>
            <ScrollIndicator
              style={{
                top: `${Number(scrollPerc || 0).toFixed(0)}%`,
                height: `${scrollViewHeightPercentage}%`,
              }}
            />
          </ScrollLine>
        </ScrollLineContainer>
      </ScrollBarContainer>
    </Layout>
  );
};

export default TabScreen;
