import Separator from '@components/Separator';
import Text from '@components/Text';
import React from 'react';
import {EmptyContainer} from '../Styled';

const EmptyState = (): React$Node => {
  const textStyle = {
    textAlign: 'center',
  };
  return (
    <EmptyContainer>
      <Text mode="h3" style={textStyle}>
        No installments found
      </Text>
      <Separator space={12} />
      <Text mode="body2" themeColor="TEXT_100_HIGH" style={textStyle}>
        There are no available installments under this category.
      </Text>
    </EmptyContainer>
  );
};

export default EmptyState;
