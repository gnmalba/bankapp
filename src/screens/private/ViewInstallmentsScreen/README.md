## ViewInstallmentsScreen
ViewInstallmentsScreen component.

# Usage
```js
import ViewInstallmentsScreen from '@screens/private/ViewInstallmentsScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
