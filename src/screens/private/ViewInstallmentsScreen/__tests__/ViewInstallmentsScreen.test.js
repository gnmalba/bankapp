import {render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import TabScreen from '../components/TabScreen';
jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

const MOCKS = {
  name: 'JOHN PATRICK A VIRAY',
};

const stores = [
  {
    installments: {
      formLoading: false,
      currentFilterInstallmentType: 'BT',
      allInstallments: {
        bt: [
          {
            ACQUIRERID: '544651',
            AMOUNT: '6000.0',
            BANKNAME: 'Philippine National Bank',
            CARDNUMBER: '5464971832330369',
            CARDSERNO: '2686911',
            CREDITLIMIT: '1000000',
            DATECREATED: '2021-04-26T21:03:56.257',
            DATEOFBIRTH: '1995-01-15',
            EAIR: '14.7350407761365%',
            EXPIRYDATE: '01/31',
            FIRSTNAME: 'JOHN PATRICK',
            INSTALLMENTSHORTCODE: 'BT6812M',
            ISBUSINESSRULEPASS: 'true',
            LASTNAME: 'VIRAY',
            MCC: '6012',
            MERCHANTCOUNTRY: '608',
            MERCHANTNUMBER: '000009000000002',
            MIDDLENAME: 'A',
            MONTHLYAMORTIZATION: '540.8',
            OTHERBANKACCOUNTNAME: MOCKS.name,
            OTHERBANKCARDNUMBER: '1234 1234 1234 1234',
            OUTSTANDINGBALANCE: '-10431.92',
            RATE: '0.68',
            REFERENCENUMBER: 'DCBD7B71-532311DD',
            SERNO: '11733',
            SOURCEAPP: 'MBMBT',
            STATUS: 'IN PROGRESS',
            TERM: '12',
            TERMINALID: '10055102',
            TOTALINSTALLMENTAMOUNT: '6489.6',
            TRANSACTIONFEE: '0',
            TRANSTATUSSERNO: '11733',
            TRANSTATUS_DATECREATED: '2021-04-26T21:03:56.257',
          },
        ],
        bc: [
          {
            AMOUNT: '6000.0',
            CARDNUMBER: '4055984960844349',
            CARDSERNO: '2687272',
            CURRENCY: 'PHP',
            CustomerID: '5958',
            DATECREATED: '2021-05-19T14:57:35.587',
            EAIR: '17.0602207077098%',
            EmbossingName: MOCKS.name,
            INSTALLMENTSHORTCODE: 'BLCON7912M',
            ISBUSINESSRULEPASS: '0',
            MONTHLYAMORTIZATION: '547.4',
            RATE: '0.79',
            REASONCODE: 'BINC',
            REFERENCENUMBER: 'D14B502F-D34C018E',
            SERNO: '21076',
            SOURCEAPP: 'MBMBC',
            STATUS: 'DECLINED',
            TERM: '12',
            TOTALINSTALLMENTAMOUNT: '6568.8',
            TRANSTATUSSERNO: '21076',
            TRANSTATUS_DATECREATED: '2021-05-19T14:57:35.587',
          },
        ],
        c2g: [
          {
            ACCOUNTNAME: 'dare123',
            ACCOUNTNUMBER: '1234123412341',
            ACQUIRERID: '544651',
            AMOUNT: '12345.0',
            BANKNAME: 'Metropolitan Bank and Trust Company',
            BRANCHNAME: '',
            CARDHOLDERNAME: MOCKS.name,
            CARDNUMBER: '5464985947005526',
            CARDSERNO: '2687290',
            CURRENCY: 'PHP',
            DATECREATED: '2021-02-09T00:00:00',
            DISBURSTMENTOPTION: 'U',
            EAIR: '26.5782876451873%',
            EXPIRYDATE: '',
            INSTALLMENTSHORTCODE: 'C2GOREG24M',
            ISBUSINESSRULEPASS: 'true',
            MCC: '6012',
            MERCHANTCOUNTRY: '608',
            MERCHANTNUMBER: '9200000049',
            MONTHLYAMORTIZATION: '668.69',
            RATE: '1.25',
            REFERENCENUMBER: '58A4610B-ED36F28B',
            SERNO: '20747',
            SOURCEAPP: 'MBMC2GO',
            STATUS: 'IN PROGRESS',
            TERM: '24',
            TERMINALID: '12000007',
            TOTALINSTALLMENTAMOUNT: '16048.56',
            TRANSACTIONFEE: '0',
            TRANSTATUSSERNO: '30745',
            TRANSTATUS_DATECREATED: '2021-02-09T16:11:38.69',
            USERID: '5958',
          },
        ],
      },
    },
    credit: {
      creditList: {
        data: [],
      },
    },
  },
  {
    installments: {
      formLoading: false,
      currentFilterInstallmentType: 'BT',
      allInstallments: {
        bt: [],
        c2g: [],
        bc: [],
      },
    },
    credit: {
      creditList: {
        data: [],
      },
    },
  },
];

const props = {
  installmentType: 'BT',
  navigation: {
    navigate: jest.fn(),
  },
  route: {
    params: {
      navigation: {
        navigate: jest.fn(),
      },
    },
  },
};

describe('TabScreen', () => {
  it.each(stores)('Should work properly', (store) => {
    const component = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <TabScreen {...props} />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(component.toJSON()).toMatchSnapshot();
  });
});
