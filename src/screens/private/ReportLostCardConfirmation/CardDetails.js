/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {View} from 'react-native';

import {maskingFormat, numberFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import type {CardDetailsType} from './types';
import * as S from './Styled';

const CardDetails = (props: CardDetailsType): React$Node => {
  const {card, isResize} = props;
  return (
    <View>
      <S.CardContainer>
        <S.CardLeftContent>
          <S.CardLabel {...testProps('lostCardImageLabeltestId')}>
            CARD IMAGE
          </S.CardLabel>
        </S.CardLeftContent>
        <S.CardRightContent>
          <S.CardDetailImage height={isResize ? '35' : '30'} />
        </S.CardRightContent>
      </S.CardContainer>
      <S.CardContainer>
        <S.CardLeftContent>
          <S.CardLabel {...testProps('lostCardNameLabeltestId')}>
            CARD NAME
          </S.CardLabel>
        </S.CardLeftContent>
        <S.CardRightContent>
          <S.CardValue {...testProps('cardNametestId')}>
            {card.cardName}
          </S.CardValue>
        </S.CardRightContent>
      </S.CardContainer>
      <S.CardContainer>
        <S.CardLeftContent>
          <S.CardLabel {...testProps('lostCardNumberLabeltestId')}>
            CARD NUMBER
          </S.CardLabel>
        </S.CardLeftContent>
        <S.CardRightContent>
          <S.CardValue {...testProps('cardNumbertestId')}>
            {maskingFormat(card.cardNumber, true)}
          </S.CardValue>
        </S.CardRightContent>
      </S.CardContainer>
      <S.CardContainer>
        <S.CardLeftContent>
          <S.CardLabel {...testProps('lostCardReplacementrLabeltestId')}>
            REPLACEMENT FEE
          </S.CardLabel>
        </S.CardLeftContent>
        <S.CardRightContent>
          <S.CardValue {...testProps('replacementFeetestId')}>
            {' '}
            {`${card?.currencyCode} ${numberFormat(card?.replacementFee)}`}
          </S.CardValue>
        </S.CardRightContent>
      </S.CardContainer>
    </View>
  );
};

export default CardDetails;
