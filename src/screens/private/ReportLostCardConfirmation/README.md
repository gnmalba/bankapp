## ReportLostCardConfirmation
ReportLostCardConfirmation component.

# Usage
```js
import ReportLostCardConfirmation from '@screens/private/ReportLostCardConfirmation';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
