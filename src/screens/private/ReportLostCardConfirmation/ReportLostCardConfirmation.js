/**
 * @format
 * @flow
 */

// React modules
import React, {useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {View} from 'react-native';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Separator from '@components/Separator';
import CardDetails from './CardDetails';
import Loader from '@composition/Loader';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Ducks
import {
  getLostCardTermsRequested,
  reportLostCardResetErrors,
} from '@ducks/lostcard/actions';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Utils
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';
import type {PropsType} from './types';
import * as S from './Styled';

const ReportLostCardConfirmation = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const reportLostCard = useSelector((state) => state.reportLostCard);

  const {error, loading, card, data} = reportLostCard;
  const errorCode = data?.data?.errors;

  useFocusEffect(
    useCallback(() => {
      dispatch(reportLostCardResetErrors(false));
    }, [dispatch]),
  );
  const handleNext = () => {
    props.navigation.navigate('ReportLostCardPasscode');
  };
  const renderContent = () => (
    <View>
      <Text mode="h2" {...testProps('lostCardHeadingtestId')}>
        Confirm details
      </Text>
      <Separator space={verticalScale(8)} />
      <S.TextBody {...testProps('lostCardTextBodytestId')}>
        Please review your card details before proceeding.
      </S.TextBody>
      <Separator space={verticalScale(30)} />
      <CardDetails isResize card={card} />
    </View>
  );
  const renderBottom = () => (
    <View>
      <S.LinkText>
        {"By clicking 'confirm', you have read and agreed to the "}
        <S.LinkTextHref
          {...testProps('lostCardReportTermsLinkTextBtn')}
          onPress={() => {
            dispatch(getLostCardTermsRequested({...card}));
            props.navigation.navigate('ReportLostCardTerms');
          }}>
          terms and conditions.
        </S.LinkTextHref>
      </S.LinkText>
      <S.ButtonReport>
        <Button
          size="large"
          type="primary"
          state={'default'}
          title={'Report'}
          {...testProps('lostCardReportDetailsBtn')}
          onPress={handleNext}
        />
      </S.ButtonReport>
      <S.ButtonCancel>
        <Button
          type="secondary"
          title="Cancel"
          titleThemeColor="TEXT_300_HIGH"
          {...testProps('lostCardCancelDetailsBtn')}
          onPress={() => props.navigation.pop()}
        />
      </S.ButtonCancel>
    </View>
  );

  const renderErrorScreen = () => (
    <ErrorScreen
      theme={theme}
      showBackButton={false}
      heading={'Oops! Something went wrong.'}
      details={
        errorCode
          ? errorCode[0]?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      handlePrimaryButton={() => props.navigation.pop()}
      handleSecondaryButton={() => props.navigation.navigate('Dashboard')}
      isSystemError={errorCode ? false : true}
      buttonContents={
        <Button
          title="Go back to Dashboard"
          type="primary"
          state="default"
          {...testProps('lostCardDetailErrorBtn')}
          onPress={() => props.navigation.navigate('Dashboard')}
        />
      }
    />
  );

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }
  if (error) {
    return renderErrorScreen();
  }
  return (
    <SingleScreen
      contents={renderContent()}
      bottom={renderBottom()}
      spaceFromBottom={260}
    />
  );
};

export default ReportLostCardConfirmation;
