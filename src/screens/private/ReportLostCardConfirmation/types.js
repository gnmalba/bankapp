/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: any,
    pop: any,
  },
  theme: Object,
};

export type CardDetailsType = {
  card: Object,
  isResize?: boolean,
};
