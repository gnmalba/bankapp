/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {scale, verticalScale, moderateScale} from '@utils/scale';
import {Image} from 'react-native';

export const ButtonReport: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(18)}px;
  background-color: ${(props) => props.theme.layout.backgroundColor};
  border-radius: ${moderateScale(4)}px;
`;

export const ButtonCancel: ComponentType<any> = styled.View`
  padding: ${verticalScale(18)}px ${verticalScale(0)}px;
  align-items: center;
`;

export const TextBody: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const CardContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  margin: ${scale(8)}px;
  align-items: center;
`;

export const CardLeftContent: ComponentType<any> = styled.View`
  flex: 1;
`;
export const CardRightContent: ComponentType<any> = styled.View`
  flex: 1;
  margin-left: ${scale(8)}px;
`;

export const CardLabel: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(12)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 700;
  flex-wrap: wrap;
  letter-spacing: 0.85;
`;

export const CardValue: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(14)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const LinkText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const LinkTextHref: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 700;
  flex-wrap: wrap;
`;

export const CardDetailImage: ComponentType<any> = styled(Image).attrs(
  (props) => ({
    source: require('@assets/images/credit-card-placeholder.png'),
    resizeMode: 'contain',
  }),
)`
  height: ${(props) => scale(props.height)}px;
  width: ${(props) => scale(50)}px;
`;
