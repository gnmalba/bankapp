/* eslint-disable react-hooks/exhaustive-deps */

/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

// Types
import type {PropsType} from './types';

// Utils
import {ERROR_CODES, INVALID_OTP_ATTEMPTS} from './constants';
import {
  NAVIGATION_ENROLLMENT_FORM,
  NAVIGATION_ENROLLMENT_OTP,
  NAVIGATION_ENROLLMENT_SUCCESS,
  NAVIGATION_ENROLLMENT_ERROR,
} from '@utils/navigationRoutes';

const EnrollAccountResponseHandler = (props: PropsType): React$Node => {
  const navigation = useNavigation();
  const enrollment = useSelector((state) => state.enrollment);
  const {apiResponse, invalidOTPRetries, enrolledAccountFetching} = enrollment;

  const cpcInvalidOTPCondition = !!(
    apiResponse?.errorCode === ERROR_CODES.CPC.GENERAL_ERROR &&
    apiResponse?.message.includes(ERROR_CODES.CPC.INVALID_OTP_MESSAGE)
  );

  React.useEffect(() => {
    if (apiResponse) {
      if (apiResponse.status === 200 && !enrolledAccountFetching) {
        navigation.navigate(NAVIGATION_ENROLLMENT_SUCCESS);
      }

      if (apiResponse.status !== 200 && !enrolledAccountFetching) {
        if (
          (apiResponse?.errorCode === ERROR_CODES.CASA.INVALID_OTP &&
            invalidOTPRetries !== INVALID_OTP_ATTEMPTS) ||
          cpcInvalidOTPCondition ||
          apiResponse?.errorCode === ERROR_CODES.CASA.OTP_REQUIRED ||
          apiResponse?.errorCode === ERROR_CODES.CPC.OTP_REQUIRED
        ) {
          navigation.navigate(NAVIGATION_ENROLLMENT_OTP);
        } else if (
          apiResponse?.errorCode === ERROR_CODES.CPC.MISMATCH_DATA &&
          apiResponse?.mismatchedData === 'nameOnCard'
        ) {
          navigation.navigate(NAVIGATION_ENROLLMENT_FORM);
        } else {
          navigation.navigate(NAVIGATION_ENROLLMENT_ERROR);
        }
      }
    }
  }, [enrollment?.apiResponse, enrollment?.enrolledAccountFetching]);

  return null;
};

export default EnrollAccountResponseHandler;
