export const ERRORS = {
  ACCOUNT_NAME: {
    REQUIRED: 'Please enter account name.',
    SPECIAL_CHARS: 'Invalid input. Special characters are not allowed.',
  },
  ACCOUNT_NUMBER: {
    REQUIRED: 'Please enter your 13 or 16-digit account number.',
    CORRECT_FORMAT: 'Invalid input. Enter the number in the correct format.',
    INVALID_ACCOUNT_NUMBER: 'Please enter a valid Metrobank account number.',
    INVALID_ACCOUNT_NUMBER_NON_MB: 'Please enter a valid account number.',
    INVALID_SAVINGS_CHECKING_NUMBER:
      'Invalid input. Please enter a valid Metrobank savings or checking account number.',
  },
  ALIAS: {
    REQUIRED: 'Create an alias for your account (Ex. "My Savings").',
    CORRECT_FORMAT: 'Invalid input. Enter the alias in the correct format.',
  },
  NAME_ON_CARD: {
    REQUIRED: 'Please enter the name as shown on your card.',
  },
  DATE_OF_BIRTH: {
    REQUIRED: 'Please enter your date of birth.',
    IS_BELOW_18: 'Must be above 18 years old.',
  },
  OTP: {
    INVALID: 'Verification code is invalid. Please try again.',
  },
  AMOUNT: {
    SPECIAL_CHARS: 'Invalid input. Special characters are not allowed.',
  },
};

export const INVALID_OTP_ATTEMPTS = 5;
export const ERROR_CODES = {
  CASA: {
    OTP_REQUIRED: 'invalid_otp_required',
    INVALID_OTP: 'invalid_otp_invalid',
  },
  CPC: {
    OTP_REQUIRED: 'otp_required',
    GENERAL_ERROR: 'ws_error',
    INVALID_OTP_MESSAGE: 'Invalid OTP Code',
    MAXIMUM_OTP_ATTEMPTS_MESSAGE:
      'Maximum number of validation tries have been reached',
    MISMATCH_DATA: 'MB-1604',
  },
  DUPLICATE_ALIAS: 'duplicate_alias',
};
