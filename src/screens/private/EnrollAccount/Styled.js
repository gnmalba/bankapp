/**
 * @flow
 */
// React modules
import type {ComponentType} from 'react';
import {Platform} from 'react-native';

// Third party libraries
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';

// Utils
import {verticalScale} from '@utils/scale';
import {
  HEIGHT,
  GUIDELINE_BASE_HEIGHT,
  IS_DEVICE_TABLET,
  ACCOUNT_TYPES_ENUM,
} from '@utils/constants';

export const FormContainer: ComponentType<any> = styled.View`
  flex: 1;
  height: ${() => {
    const formHeight = HEIGHT;
    return formHeight + 'px';
  }};
`;

export const FieldsContainer: ComponentType<any> = styled.View``;

export const InputContainer: ComponentType<any> = styled.View`
  /* margin-top: ${verticalScale(25)}px; */
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(40)}px;
`;

export const CancelButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const KeyboardAwareContainer: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs((props) => ({
  keyboardShouldPersistTaps: 'handled',
  bounces: false,
  showsVerticalScrollIndicator: false,
  enableOnAndroid: true,
  contentContainerStyle: {
    justifyContent: 'space-between',
  },
  extraHeight: Platform.select({
    ios: HEIGHT > GUIDELINE_BASE_HEIGHT ? 220 : 170,
    android: HEIGHT > GUIDELINE_BASE_HEIGHT ? 160 : 140,
  }),
}))``;

export const KeyboardAwareFieldsContainer: ComponentType<any> = styled.View`
  flex: 1;
  height: ${(props) => {
    const iosHeight = IS_DEVICE_TABLET ? HEIGHT / 1.23 : HEIGHT / 1.28;
    const androidHeight =
      HEIGHT > GUIDELINE_BASE_HEIGHT ? HEIGHT / 1.2 : HEIGHT / 1.3;
    const formHeight = Platform.OS === 'ios' ? iosHeight : androidHeight;
    return props.cardType === ACCOUNT_TYPES_ENUM.CREDIT && !IS_DEVICE_TABLET
      ? -1 + 'px'
      : formHeight + 'px';
  }};
`;

export const KeyboardAwareButtonContainer: ComponentType<any> = styled.View`
  flex: 1;
  justify-content: flex-end;
`;
