import React from 'react';
import {render} from '@testing-library/react-native';

import MockProvider from '@utils/testFramework';
import EnrollAccountResponseHandler from '../EnrollAccountResponseHandler';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import {ERROR_CODES} from '../constants';

const mockStore = {
  enrollment: {},
};

describe('EnrollAccountResponseHandler', () => {
  it.each([
    [200, null, {enrolledAccountFetching: false}],
    [400, ERROR_CODES.CASA.INVALID_OTP, {enrolledAccountFetching: false}],
    [400, ERROR_CODES.CASA.OTP_REQUIRED, {enrolledAccountFetching: false}],
    [
      400,
      ERROR_CODES.CPC.GENERAL_ERROR,
      {
        enrolledAccountFetching: false,
        apiResponse: {message: ERROR_CODES.CPC.INVALID_OTP_MESSAGE},
      },
    ],
    [400, ERROR_CODES.CPC.OTP_REQUIRED, {enrolledAccountFetching: false}],
    [
      400,
      ERROR_CODES.CPC.MISMATCH_DATA,
      {
        enrolledAccountFetching: false,
        apiResponse: {mismatchedData: 'nameOnCard'},
      },
    ],
    [400, 'DIFFERENT_ERROR', {enrolledAccountFetching: false}],
  ])(
    'Should be able to navigate to the correct Enrollment Stack Screen',
    (status, errorCode, additionalData) => {
      const updatedStore = {
        enrollment: {
          ...mockStore.enrollment,
          ...additionalData,
          apiResponse: {
            ...additionalData.apiResponse,
            status: status,
            errorCode: errorCode,
          },
        },
      };
      const all = render(
        <ThemeProvider theme={DARK_THEME}>
          <MockProvider store={updatedStore}>
            <EnrollAccountResponseHandler />
          </MockProvider>
        </ThemeProvider>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
