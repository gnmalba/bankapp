import React from 'react';
import {render} from '@testing-library/react-native';
import MockProvider from '@utils/testFramework';
import EnrollAccount from '../EnrollAccount';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
const mockStore = () => ({
  enrollment: {
    loading: false,
    yazzCardBins: [],
  },
});

describe('EnrollAccount', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <MockProvider store={mockStore}>
          <EnrollAccount />
        </MockProvider>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
