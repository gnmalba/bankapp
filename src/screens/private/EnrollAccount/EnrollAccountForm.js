/* eslint-disable react-hooks/exhaustive-deps */
// React modules
import React, {useEffect, useState} from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';
import {useFormik} from 'formik';
import {isValid as isValidCC} from 'cc-validate';

// Compositions
import AccountNumber from '@composition/EnrollAccount/AccountNumber';
import Alias from '@composition/EnrollAccount/Alias';
import NameOnCard from '@composition/EnrollAccount/NameOnCard';
import DateOfBirth from '@composition/EnrollAccount/DateOfBirth';
import EnrollButton from '@composition/EnrollAccount/EnrollButton';
import CancelButton from '@composition/EnrollAccount/CancelButton';
import {
  KeyboardAwareContainer,
  KeyboardAwareFieldsContainer,
  KeyboardAwareButtonContainer,
  ButtonContainer,
} from './Styled';

// Ducks
import {
  enrollmentDeposit,
  enrollmentCredit,
  enrollmentYazz,
  updateEnrollmentState,
  setEnrollmentCardType,
} from '@ducks/enrollment/actions';

// Utils
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';
import {ERRORS} from './constants';
import {initialValues, validationSchema} from './validationSchema';

// Types
import type {PropsType} from './types';

const EnrollAccountForm = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const enrollment = useSelector((state) => state.enrollment);
  const {yazzCardBins, cardType, apiResponse} = enrollment;
  const [additionalErrors, setAdditionalError] = useState({
    accountNo: null,
    dateOfBirth: null,
  });

  const formik = useFormik({
    initialValues: initialValues,
    enableReinitialize: true,
    validateOnMount: false,
    validateOnBlur: true,
    validationSchema: validationSchema(cardType),
    onSubmit: (values) => handleOnSubmit(values),
  });

  const {
    values,
    errors,
    touched,
    isValid,
    setFieldValue,
    setFieldError,
    setFieldTouched,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    if (values?.accountNo?.length >= 16) {
      checkIsYazz(values.accountNo);
    } else {
      if (values?.accountNo?.length === 13) {
        dispatch(setEnrollmentCardType(ACCOUNT_TYPES_ENUM.DEPOSIT));
      } else {
        dispatch(setEnrollmentCardType(null));
        if (values?.nameOnCard || values?.dateOfBirth) {
          handleResetField('nameOnCard');
          handleResetField('dateOfBirth');
        }
      }
    }
  }, [values.accountNo]);

  useEffect(() => {
    if (apiResponse?.mismatchedData) {
      setFieldError(
        apiResponse.mismatchedData,
        'Please enter the name as shown on your card.',
      );
    }
  }, [apiResponse?.mismatchedData]);

  const removeSpace = (value) => {
    return value.replace(/\s+/g, '');
  };
  const checkIsYazz = (accountNo) => {
    const firstSixDigits = accountNo.substring(0, 6);

    const isYazz = yazzCardBins.some((list) => list === firstSixDigits);
    if (isYazz) {
      dispatch(setEnrollmentCardType(ACCOUNT_TYPES_ENUM.YAZZ));
    } else {
      if (!additionalErrors.accountNo) {
        dispatch(setEnrollmentCardType(ACCOUNT_TYPES_ENUM.CREDIT));
      }
    }
  };

  const hasRequiredFields = () => {
    switch (cardType) {
      case ACCOUNT_TYPES_ENUM.DEPOSIT:
      case ACCOUNT_TYPES_ENUM.YAZZ:
        return values.accountNo && values.alias;
      case ACCOUNT_TYPES_ENUM.CREDIT:
        return (
          values.accountNo &&
          values.alias &&
          values.nameOnCard &&
          values.dateOfBirth &&
          !additionalErrors.dateOfBirth &&
          !additionalErrors.accountNo
        );
    }
    return null;
  };

  const handleInputChange = (name) => (value) => {
    if (name === 'accountNo') {
      let accountNo = value;
      if (value > 16) {
        accountNo = value.substring(0, 16);
      }
      const unformattedAccountNo = removeSpace(accountNo);
      setFieldValue(name, unformattedAccountNo);

      if (unformattedAccountNo.length === 16) {
        validateCreditCard(accountNo);
      } else {
        setAdditionalError({accountNo: null});
        return;
      }
    } else {
      setFieldValue(name, value);
    }
  };

  const validateCreditCard = (cardNumber) => {
    const isValidCard = isValidCC(cardNumber).isValid;
    if (!isValidCard) {
      setAdditionalError({
        accountNo: ERRORS.ACCOUNT_NUMBER.INVALID_ACCOUNT_NUMBER_NON_MB,
      });
    } else {
      setAdditionalError({accountNo: null});
    }
  };

  const handleResetField = (name) => {
    setFieldValue(name, initialValues[name]);
  };

  const handleCancel = () => {
    resetForm(initialValues);
    props.navigation.navigate('deposit');
  };

  const handleOnSubmit = (formValues) => {
    switch (cardType) {
      case ACCOUNT_TYPES_ENUM.DEPOSIT:
        handleDepositEnrollment(formValues);
        break;
      case ACCOUNT_TYPES_ENUM.CREDIT:
        handleCreditEnrollment(formValues);
        break;
      case ACCOUNT_TYPES_ENUM.YAZZ:
        handleYazzEnrollment(formValues);
        break;
    }
  };

  const handleDepositEnrollment = (formValues) => {
    const {accountNo, alias} = formValues;
    dispatch(
      updateEnrollmentState({
        state: 'formValues',
        value: formValues,
      }),
    );
    dispatch(
      enrollmentDeposit({
        accountNo: accountNo,
        alias: alias,
      }),
    );
  };

  const handleCreditEnrollment = (formValues) => {
    const {accountNo, alias, nameOnCard, dateOfBirth} = formValues;
    dispatch(
      updateEnrollmentState({
        state: 'formValues',
        value: formValues,
      }),
    );
    dispatch(
      enrollmentCredit({
        accountNo: accountNo,
        alias: alias,
        nameOnCard: nameOnCard,
        dateOfBirth: dateOfBirth,
      }),
    );
  };

  const handleYazzEnrollment = (formValues) => {
    const {accountNo, alias} = formValues;
    dispatch(
      updateEnrollmentState({
        state: 'formValues',
        value: formValues,
      }),
    );
    dispatch(
      enrollmentYazz({
        accountNo: accountNo,
        alias: alias,
      }),
    );
  };

  return (
    <>
      <KeyboardAwareContainer>
        <KeyboardAwareFieldsContainer cardType={cardType}>
          <AccountNumber
            accountNo={values?.accountNo}
            handleInputChange={handleInputChange}
            handleResetField={handleResetField}
            handleSetFieldTouched={setFieldTouched}
            error={errors?.accountNo || additionalErrors.accountNo}
            isTouched={touched?.accountNo}
            isValid={
              !errors?.accountNo && values?.accountNo && touched?.accountNo
            }
            theme={theme}
          />
          {cardType === ACCOUNT_TYPES_ENUM.CREDIT &&
            !additionalErrors.accountNo && (
              <>
                <NameOnCard
                  nameOnCard={values?.nameOnCard}
                  handleInputChange={handleInputChange}
                  handleResetField={handleResetField}
                  handleSetFieldTouched={setFieldTouched}
                  error={errors?.nameOnCard}
                  isTouched={touched?.nameOnCard}
                />
                <DateOfBirth
                  setFieldValue={setFieldValue}
                  dateOfBirth={values?.dateOfBirth}
                  handleInputChange={handleInputChange}
                  handleSetFieldTouched={setFieldTouched}
                  hasAdditionalError={(hasError) =>
                    setAdditionalError({dateOfBirth: hasError})
                  }
                  error={errors?.dateOfBirth}
                  isTouched={touched?.dateOfBirth}
                />
              </>
            )}
          <Alias
            alias={values?.alias}
            handleInputChange={handleInputChange}
            handleResetField={handleResetField}
            handleSetFieldTouched={setFieldTouched}
            error={errors?.alias}
            isTouched={touched?.alias}
          />
          <KeyboardAwareButtonContainer>
            <ButtonContainer>
              <EnrollButton
                handleSubmit={handleSubmit}
                isValid={isValid && hasRequiredFields()}
              />
              <CancelButton handleCancel={handleCancel} />
            </ButtonContainer>
          </KeyboardAwareButtonContainer>
        </KeyboardAwareFieldsContainer>
      </KeyboardAwareContainer>
    </>
  );
};

export default EnrollAccountForm;
