## EnrollAccount
EnrollAccount component.

# Usage
```js
import EnrollAccount from '@screens/private/EnrollAccount';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
