/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

// React modules
import React, {useEffect} from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Compositions
import Loader from '@composition/Loader';
import EnrollAccountForm from './EnrollAccountForm';
import FormLayout from '@composition/FormLayout';

// Components
import EnrollAccountResponseHandler from './EnrollAccountResponseHandler';

// Ducks
import {
  fetchYazzBins,
  fetchEnrollmentPublicKey,
  partialResetEnrollment,
} from '@ducks/enrollment/actions';

// Utils

import type {PropsType} from './types';
import {} from './Styled';

const EnrollAccount = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const enrollment = useSelector((state) => state.enrollment);
  const {navigation} = props;
  const {loading, showProcessingScreen, yazzCardBins, publicKey} = enrollment;

  useEffect(() => {
    !yazzCardBins.length && dispatch(fetchYazzBins());
    !publicKey && dispatch(fetchEnrollmentPublicKey());
    return () => {
      dispatch(partialResetEnrollment());
    };
  }, []);

  return (
    <FormLayout customMarginTop={0}>
      <Loader isVisible={loading && !showProcessingScreen} />
      <EnrollAccountForm navigation={navigation} />
      <EnrollAccountResponseHandler navigation={navigation} />
    </FormLayout>
  );
};

export default EnrollAccount;
