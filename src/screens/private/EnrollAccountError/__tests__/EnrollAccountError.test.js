import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';

import MockProvider from '@utils/testFramework';
import {testProps} from '@utils/componentCustomProps';

import EnrollAccountError from '../EnrollAccountError';
import {ERROR_CODES, INVALID_OTP_ATTEMPTS} from '../../EnrollAccount/constants';
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';

describe('EnrollAccountError', () => {
  const mockStore = {
    enrollment: {
      apiResponse: {},
      invalidOTPRetries: INVALID_OTP_ATTEMPTS,
      cardType: ACCOUNT_TYPES_ENUM.DEPOSIT,
    },
  };

  it.each([
    [ERROR_CODES.CASA.INVALID_OTP, {}, ACCOUNT_TYPES_ENUM.CREDIT],
    [ERROR_CODES.CASA.INVALID_OTP, {}, ACCOUNT_TYPES_ENUM.DEPOSIT],
    [
      ERROR_CODES.CPC.GENERAL_ERROR,
      {message: ERROR_CODES.CPC.MAXIMUM_OTP_ATTEMPTS_MESSAGE},
      ACCOUNT_TYPES_ENUM.YAZZ,
    ],
    [ERROR_CODES.DUPLICATE_ALIAS, {}, null],
    ['DIFFERENT_ERROR_CODE', null, null],
  ])(
    'Should render the correct Error Screen',
    (errorCode, additionalData, cardType) => {
      const updatedStore = {
        enrollment: {
          ...mockStore.enrollment,
          cardType: cardType || mockStore.enrollment.cardType,
          apiResponse: {...additionalData, errorCode: errorCode},
        },
      };

      const all = render(
        <MockProvider store={updatedStore}>
          <EnrollAccountError {...testProps('enrollAccountErrorTestId')} />
        </MockProvider>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );

  it.each([ERROR_CODES.DUPLICATE_ALIAS, 'DIFFERENT_ERROR_CODE'])(
    'Should be able to press the Check Details Button and navigate back to Enrollment Form',
    async (errorCode) => {
      const updatedStore = {
        enrollment: {
          ...mockStore.enrollment,
          apiResponse: {
            errorCode: errorCode,
          },
        },
      };

      const {queryByTestId} = render(
        <MockProvider store={updatedStore}>
          <EnrollAccountError {...testProps('enrollAccountErrorTestId')} />
        </MockProvider>,
      );
      const checkDetailsBtn = queryByTestId('errorCheckDetailsButton');
      expect(checkDetailsBtn).not.toBeNull();
      fireEvent.press(checkDetailsBtn);
    },
  );

  it('Should be able to press the Logout Button and dispatch the Logout action', () => {
    const updatedStore = {
      enrollment: {
        ...mockStore.enrollment,
        apiResponse: {
          errorCode: ERROR_CODES.CPC.GENERAL_ERROR,
          message: ERROR_CODES.CPC.MAXIMUM_OTP_ATTEMPTS_MESSAGE,
        },
      },
    };

    const {queryByTestId} = render(
      <MockProvider store={updatedStore}>
        <EnrollAccountError {...testProps('enrollAccountErrorTestId')} />
      </MockProvider>,
    );
    const logoutBtn = queryByTestId('errorLogoutButton');
    expect(logoutBtn).not.toBeNull();
    fireEvent.press(logoutBtn);
  });
});
