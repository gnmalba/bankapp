## EnrollAccountError
EnrollAccountError component.

# Usage
```js
import EnrollAccountError from '@screens/private/EnrollAccountError';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
