/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/core';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Components
import Button from '@components/Button';

// Ducks
import {updateEnrollmentState} from '@ducks/enrollment/actions';
import {AUTHENTICATION_START_LOGOUT} from '@ducks/authentication/actionTypes';

// Utils
import {testProps} from '@utils/componentCustomProps';
import {NAVIGATION_ENROLLMENT_FORM} from '@utils/navigationRoutes';
import {ERROR_CODES, INVALID_OTP_ATTEMPTS} from '../EnrollAccount/constants';
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';

// Types
import type {PropsType} from './types';

const EnrollAccountError = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const enrollment = useSelector((state) => state.enrollment);

  const {apiResponse, invalidOTPRetries, cardType} = enrollment;

  const handlePrimaryButton = () => {
    dispatch(
      updateEnrollmentState({
        state: 'apiResponse',
        value: null,
      }),
    );
    navigation.navigate(NAVIGATION_ENROLLMENT_FORM);
  };

  const handleLogout = () => {
    dispatch({
      type: AUTHENTICATION_START_LOGOUT,
    });
  };

  const renderDefaultErrorMessage = () => (
    <ErrorScreen
      heading="Something went wrong."
      details={
        'Make sure you have correct information of the Metrobank account you want to enroll.\nPlease try again.'
      }
      buttonContents={
        <Button
          title="Check Details"
          type="primary"
          state="default"
          onPress={() => handlePrimaryButton()}
          {...testProps('errorCheckDetailsButton')}
        />
      }
    />
  );

  const cpcMaxAttemptOTP = !!(
    apiResponse?.errorCode === ERROR_CODES.CPC.GENERAL_ERROR &&
    apiResponse?.message.includes(ERROR_CODES.CPC.MAXIMUM_OTP_ATTEMPTS_MESSAGE)
  );

  const renderErrorScreen = () => {
    if (
      (apiResponse?.errorCode === ERROR_CODES.CASA.INVALID_OTP &&
        invalidOTPRetries === INVALID_OTP_ATTEMPTS) ||
      cpcMaxAttemptOTP
    ) {
      const message =
        cardType === ACCOUNT_TYPES_ENUM.CREDIT ||
        cardType === ACCOUNT_TYPES_ENUM.YAZZ
          ? apiResponse?.message
          : 'You have reached the maximum number of attempts. You will be logged out of Metrobank Online. For assistance, call (02) 88-700-700.';

      return (
        <ErrorScreen
          theme={theme}
          showBackButton={false}
          details={message}
          buttonContents={
            <Button
              title="Logout"
              type="primary"
              state="default"
              onPress={() => handleLogout()}
              {...testProps('errorLogoutButton')}
            />
          }
        />
      );
    } else if (apiResponse?.errorCode === ERROR_CODES.DUPLICATE_ALIAS) {
      return (
        <ErrorScreen
          theme={theme}
          details={'This alias is already in use. Please create another.'}
          buttonContents={
            <Button
              title="Check Details"
              type="primary"
              state="default"
              onPress={() => handlePrimaryButton()}
              {...testProps('errorCheckDetailsButton')}
            />
          }
        />
      );
    } else {
      return renderDefaultErrorMessage();
    }
  };

  return renderErrorScreen();
};

export default EnrollAccountError;
