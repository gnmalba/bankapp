/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/core';

// Compositions
import SingleScreen from '@composition/SingleScreen';

// Components
import Button from '@components/Button';
import Reminder from '@components/Reminder';
import {
  Container,
  StyledSuccessIcon,
  StyledH3Text,
  StyledBody2Text,
  DetailsContainer,
  ContentContainer,
  ContentNameContainer,
  ContentValueContainer,
  Divider,
  SecondaryButtonContainer,
} from './Styled';

// Ducks
import {showEnrollmentSnackbar} from '@ducks/enrollment/actions';

// Utils
import {maskingFormat} from '@utils/accountNumber';
import {ACCOUNT_TYPES_ENUM, MB_PREPAID_PRODUCT_TYPE} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropsType} from './types';

export const formatMobileNo = (mobile: string): string => {
  const lastFourDigit = mobile.substr(mobile.length - 4);

  return `**** *** ${lastFourDigit}`;
};

const EnrollAccountSuccess = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const enrollment = useSelector((state) => state.enrollment);
  const {
    formValues: {accountNo, alias},
    mobileNumber,
    cardType,
    apiResponse,
  } = enrollment;

  const navigateBackToAccountsTab = () => {
    switch (cardType) {
      case ACCOUNT_TYPES_ENUM.DEPOSIT:
        if (apiResponse.data.productType === MB_PREPAID_PRODUCT_TYPE) {
          navigation.navigate('prepaid');
        } else {
          navigation.navigate('deposit');
        }
        break;
      case ACCOUNT_TYPES_ENUM.CREDIT:
        navigation.navigate('credit');
        break;
      case ACCOUNT_TYPES_ENUM.YAZZ:
        navigation.navigate('prepaid');
        break;
    }
  };

  const handleGoToMyAccounts = () => {
    dispatch(
      showEnrollmentSnackbar({
        showSnackbar: true,
        snackbarMessage: "You've successfully enrolled your account!",
      }),
    );
    navigateBackToAccountsTab();
  };

  const handleViewAccountDetails = () => {
    navigateBackToAccountsTab();
    switch (cardType) {
      case ACCOUNT_TYPES_ENUM.DEPOSIT:
        if (apiResponse.data.productType === MB_PREPAID_PRODUCT_TYPE) {
          navigation.navigate('PrepaidScreenOverview', {
            id: apiResponse.data.id,
            alias: apiResponse.data.alias,
          });
        } else {
          navigation.navigate('DepositScreenOverview', {
            alias: apiResponse.data.alias,
            id: apiResponse.data.id,
          });
        }
        break;
      case ACCOUNT_TYPES_ENUM.CREDIT:
        navigation.navigate('CreditScreenOverview', {
          id: apiResponse.data.cardSerialNo,
          alias: alias,
        });
        break;
      case ACCOUNT_TYPES_ENUM.YAZZ:
        navigation.navigate('YazzScreenOverview', {
          id: apiResponse.data.cardSerialNo,
          alias: alias,
        });
        break;
    }
  };

  const renderContent = () => {
    return (
      <Container>
        <StyledSuccessIcon
          theme={theme}
          {...testProps('enrollmentSuccessIcon')}
        />
        <StyledH3Text theme={theme} {...testProps('enrollmentSuccessTitle')}>
          {'You’ve successfully enrolled \n your account!'}
        </StyledH3Text>
        <StyledBody2Text
          theme={theme}
          {...testProps('enrollmentSuccessDescription')}>
          Congratulations! You can now access your account using this app.
        </StyledBody2Text>
        <DetailsContainer theme={theme}>
          <ContentContainer cardType={cardType}>
            <ContentNameContainer
              theme={theme}
              cardType={cardType}
              hasMarginBottom={true}
              {...testProps('enrollmentSuccessAccountNumberLabel')}>
              Account No.
            </ContentNameContainer>
            <ContentValueContainer
              theme={theme}
              cardType={cardType}
              hasMarginBottom={true}
              {...testProps('enrollmentSuccessAccountNumbervalue')}>
              {maskingFormat(accountNo, true)}
            </ContentValueContainer>
          </ContentContainer>
          <ContentContainer cardType={cardType} hasMarginBottom={true}>
            <ContentNameContainer
              theme={theme}
              {...testProps('enrollmentSuccessAccountAliasLabel')}>
              Account Alias
            </ContentNameContainer>
            <ContentValueContainer
              theme={theme}
              {...testProps('enrollmentSuccessAccountAliasValue')}>
              {alias}
            </ContentValueContainer>
          </ContentContainer>
          {cardType === ACCOUNT_TYPES_ENUM.DEPOSIT && mobileNumber && (
            <>
              <Divider
                theme={theme}
                {...testProps('enrollmentSuccessDivider')}
              />
              <ContentContainer hasMarginBottom={true} cardType={cardType}>
                <ContentNameContainer
                  theme={theme}
                  {...testProps('enrollmentSuccessMobileNumberLabel')}>
                  Mobile No.
                </ContentNameContainer>
                <ContentValueContainer
                  theme={theme}
                  {...testProps('enrollmentSuccessMobileNumberValue')}>
                  {formatMobileNo(mobileNumber)}
                </ContentValueContainer>
              </ContentContainer>
              <Reminder
                theme={theme}
                colorTheme={'yellow'}
                text="All SMS notifications of advisories and your transactions will be sent to this number"
              />
            </>
          )}
        </DetailsContainer>
      </Container>
    );
  };

  return (
    <SingleScreen
      contents={renderContent()}
      bottom={
        <>
          <Button
            type="primary"
            state="default"
            title="View Account Details"
            onPress={() => handleViewAccountDetails()}
            {...testProps('viewAccountDetailsButton')}
          />
          <SecondaryButtonContainer>
            <Button
              type="secondary"
              title="Go to My Accounts"
              titleThemeColor="TEXT_300_HIGH"
              onPress={() => handleGoToMyAccounts()}
              {...testProps('goToMyAccountsButton')}
            />
          </SecondaryButtonContainer>
        </>
      }
    />
  );
};

export default EnrollAccountSuccess;
