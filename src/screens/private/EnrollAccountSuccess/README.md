## EnrollAccountSuccess
EnrollAccountSuccess component.

# Usage
```js
import EnrollAccountSuccess from '@screens/private/EnrollAccountSuccess';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
