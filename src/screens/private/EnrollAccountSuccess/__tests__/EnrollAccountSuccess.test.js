import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';

import MockProvider from '@utils/testFramework';
import {testProps} from '@utils/componentCustomProps';

import EnrollAccountSuccess, {formatMobileNo} from '../EnrollAccountSuccess';
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';

describe('EnrollAccountSuccess', () => {
  const mockStore = {
    enrollment: {
      formValues: {
        accountNo: '1112223334444',
        alias: 'User 1',
      },
      mobileNumber: '*******1234',
      cardType: 0,
      apiResponse: {
        data: {},
      },
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore}>
        <EnrollAccountSuccess {...testProps('enrollAccountSuccessTestId')} />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should be able to press the Go to My Account Button and navigate to My Accounts tab', () => {
    // Edit the args inside render and provide props.
    const {queryByTestId} = render(
      <MockProvider store={mockStore}>
        <EnrollAccountSuccess {...testProps('enrollAccountSuccessTestId')} />
      </MockProvider>,
    );

    const goToMyAccountsBtn = queryByTestId('goToMyAccountsButton');
    expect(goToMyAccountsBtn).not.toBeNull();
    fireEvent.press(goToMyAccountsBtn);
  });

  it.each([
    ['Deposit', ACCOUNT_TYPES_ENUM.DEPOSIT, {}],
    ['MB Prepaid', ACCOUNT_TYPES_ENUM.DEPOSIT, {productType: '769'}],
    ['Credit', ACCOUNT_TYPES_ENUM.CREDIT, {}],
    ['Yazz', ACCOUNT_TYPES_ENUM.YAZZ, {}],
  ])(
    'Should be able to press the View Account Details Button and navigate to %s Overview',
    async (accountTypeName, cardType, productType) => {
      const updatedStore = {
        enrollment: {
          ...mockStore.enrollment,
          cardType: cardType,
          apiResponse: {
            data: productType,
          },
        },
      };

      const {queryByTestId} = render(
        <MockProvider store={updatedStore}>
          <EnrollAccountSuccess {...testProps('enrollAccountSuccessTestId')} />
        </MockProvider>,
      );
      const viewAccountDetailsBtn = queryByTestId('viewAccountDetailsButton');
      expect(viewAccountDetailsBtn).not.toBeNull();
      fireEvent.press(viewAccountDetailsBtn);
    },
  );

  it('Should be able to return the mobile number in XXXX XXX 1234 format', () => {
    expect(formatMobileNo(mockStore.enrollment.mobileNumber)).toEqual(
      '**** *** 1234',
    );
  });
});
