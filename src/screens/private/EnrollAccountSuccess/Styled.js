/* eslint-disable sonarjs/no-identical-functions */
/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {scale, verticalScale} from '@utils/scale';
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';
import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  align-items: center;
`;

export const StyledSuccessIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'checkmark-circle-sharp',
    size: scale(64),
    color: props.theme.color.GREEN_STATUS,
  }),
)`
  margin-bottom: 20px;
`;

export const StyledH3Text: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  customColor: props.theme.color.TEXT_300_HIGH,
}))`
  font-family: 'MyriadPro-Semibold';
  font-weight: 600;
  text-align: center;
  margin-bottom: 20px;
`;

export const StyledBody2Text: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body2',
    customColor: props.theme.color.TEXT_100_HIGH,
  }),
)`
  text-align: center;
  margin-bottom: 20px;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  flex: 1;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  padding: 16px;
  width: 100%;
`;

export const ContentContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${(props) =>
    props.cardType === ACCOUNT_TYPES_ENUM.DEPOSIT &&
    props.hasMarginBottom === true
      ? verticalScale(16) + 'px'
      : -1 + 'px'};
`;

export const ContentNameContainer: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'overline2',
    customColor: props.theme.color.TEXT_100_HIGH,
  }),
)`
  margin-bottom: ${(props) =>
    props.cardType === ACCOUNT_TYPES_ENUM.DEPOSIT ||
    props.hasMarginBottom === true
      ? verticalScale(16) + 'px'
      : -1 + 'px'};
  width: 40%;
`;

export const ContentValueContainer: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'buttonlink',
    customColor: props.theme.color.TEXT_300_HIGH,
    numberOfLines: 1,
  }),
)`
  width: 60%;
  margin-bottom: ${(props) =>
    props.cardType === ACCOUNT_TYPES_ENUM.DEPOSIT ||
    props.hasMarginBottom === true
      ? verticalScale(16) + 'px'
      : -1 + 'px'};
`;

export const Divider: ComponentType<any> = styled.View`
  border-top-width: 1px;
  margin-bottom: ${verticalScale(16)}px;
  border-color: ${(props) => props.theme.textInput.dividerColor};
  max-width: ${scale(279)}px;
`;

export const SecondaryButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;
