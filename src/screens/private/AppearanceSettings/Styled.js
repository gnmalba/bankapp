/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Image} from 'react-native';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View`
  align-items: center;
`;

export const PhoneImage: ComponentType<any> = styled(Image)``;

export const CheckboxContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 70%;
`;
