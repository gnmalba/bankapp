/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  navigation: {
    goBack: Function,
    pop: Function,
    navigate: Function,
  },
};
