/**
 * @format
 * @flow
 */

import React, {useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';

import type {PropsType} from './types';
import {CheckboxContainer, Container, PhoneImage} from './Styled';
import Layout from '@components/Layout';
import {APPEARANCE_SETTINGS, UI} from '@utils/manageSettings';
import {EndButtonWrapper, SemiboldText} from '@utils/commonStyles';
import Separator from '@components/Separator';
import Text from '@components/Text';
import Checkbox from '@components/Checkbox';
import Button from '@components/Button';
import {apiUpdateTheme} from '@ducks/config/actions';
import {useDispatch, useSelector} from 'react-redux';
import {getConfig, getThemeForMainApp} from '@ducks/config/selectors';
import {setTemporaryTheme} from '@ducks/managesettings/actions';
import Loader from '@composition/Loader';
import {manageSettingsSelector} from '@ducks/managesettings/selectors';

const PHONE_IMAGE = {
  dark: require('@assets/images/light_phone.png'),
};

const AppearanceSettings = ({navigation}: PropsType): React$Node => {
  const dispatch = useDispatch();

  const {theme: CurrentTheme} = useSelector(getConfig);
  const theme = useSelector(getThemeForMainApp);
  const {loading, temporaryTheme} = useSelector(manageSettingsSelector);

  const wasThemeChanged = (): boolean =>
    temporaryTheme && temporaryTheme !== CurrentTheme;

  useFocusEffect(
    useCallback(() => {
      return () => {
        dispatch(setTemporaryTheme(null));
      };
    }, [dispatch]),
  );

  const handleRadioPress = (value) => dispatch(setTemporaryTheme(value));

  const handleCancel = () => navigation.goBack();

  const handleSave = (selectedTheme) => {
    dispatch(apiUpdateTheme(selectedTheme));

    navigation.goBack();
  };

  return (
    <Layout padding={UI.PADDINGS.layout}>
      {loading ? (
        <Loader visible />
      ) : (
        <>
          <SemiboldText mode="h3">Dashboard Setup</SemiboldText>
          <Separator space={UI.MARGINS.separator_8 * 2} />
          <Text mode="body2" themeColor="TEXT_100_HIGH">
            You can choose a theme for the dashboard by selecting either Light
            or Dark Mode.
          </Text>
          <Separator space={UI.MARGINS.separator_40 * 2} />
          <Container>
            <PhoneImage source={PHONE_IMAGE.dark} />
            <Separator space={UI.MARGINS.separator_8 * 3} />
            <CheckboxContainer>
              <Checkbox
                accessibilityLabel="cbdark"
                checked={theme === APPEARANCE_SETTINGS.DARK_VALUE}
                title="Dark"
                isCheckbox={false}
                onPress={() => handleRadioPress(APPEARANCE_SETTINGS.DARK_VALUE)}
              />
              <Checkbox
                accessibilityLabel="cblight"
                checked={theme === APPEARANCE_SETTINGS.LIGHT_VALUE}
                title="Light"
                isCheckbox={false}
                onPress={() =>
                  handleRadioPress(APPEARANCE_SETTINGS.LIGHT_VALUE)
                }
              />
            </CheckboxContainer>
          </Container>
          <EndButtonWrapper>
            <Button
              type="primary"
              title="Save"
              state={wasThemeChanged() ? 'default' : 'disabled'}
              onPress={() => handleSave(temporaryTheme)}
            />
            <Button
              type="primary"
              title="Cancel"
              titleThemeColor="CANCEL_BUTTON"
              onPress={handleCancel}
            />
          </EndButtonWrapper>
        </>
      )}
    </Layout>
  );
};

export default AppearanceSettings;
