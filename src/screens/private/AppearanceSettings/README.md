## AppearanceSettings
AppearanceSettings component.

# Usage
```js
import AppearanceSettings from '@screens/private/AppearanceSettings';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
