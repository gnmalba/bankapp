import React from 'react';
import {render} from '@testing-library/react-native';
import AppearanceSettings from '../AppearanceSettings';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {APPEARANCE_SETTINGS} from '@utils/manageSettings';
import {NavigationContext} from '@react-navigation/native';

const navContext = {
  isFocused: () => true,
  addListener: jest.fn(() => jest.fn()),
};

describe('AppearanceSettings', () => {
  it('Should work as expected', () => {
    const store = {
      config: {
        theme: APPEARANCE_SETTINGS.DARK_VALUE,
        temporaryTheme: APPEARANCE_SETTINGS.DARK_VALUE,
      },
      manageSettings: {
        loading: false,
      },
    };

    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <AppearanceSettings />
          </NavigationContext.Provider>
        </ThemeProvider>
        ,
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
