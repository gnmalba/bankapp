## SendMoneyTransactionResult
SendMoneyTransactionResult component.

# Usage
```js
import SendMoneyTransactionResult from '@screens/private/SendMoneyTransactionResult';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
