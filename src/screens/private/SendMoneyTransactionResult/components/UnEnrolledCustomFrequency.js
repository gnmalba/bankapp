/**
 * @format
 * @flow
 */
// React modules
import React from 'react';

// Components
import Divider from '@components/Divider';
import ReviewItem from '@composition/InstallmentDetail';

// Utils
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropTypes} from './types';
import * as S from '../Styled';

function UnEnrolledCustomFrequency({data}: PropTypes): React$Node {
  return (
    <>
      <ReviewItem
        small
        label="TO"
        value={toText(data.toAlias, 'SendMoneyTo', 'body2')}
        subValue={data.toBankName.toUpperCase()}
        alignItems="flex-start"
        {...testProps('SendMoneyTo')}
      />
      <ReviewItem
        small
        label="account no."
        value={toText(data.accountNo, 'SendMoneyAccountNumber', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAccountNo')}
      />
      <ReviewItem
        small
        label="address"
        value={toText(data.address, 'SendMoneyAddress', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAddress')}
      />
      <ReviewItem
        small
        label="mobile no."
        value={toText(data.mobile, 'SendMoneyMobileNumber', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAddress')}
      />
      <ReviewItem
        small
        label="email address"
        value={toText(data.email, 'SendMoneyEmailAdress', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAddress')}
      />
      <S.DividerContainer>
        <Divider />
      </S.DividerContainer>
      <ReviewItem
        small
        label="repeats every"
        value={toText(data.repeatsEvery, 'SendMoneyPaymentFrequency', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAddress')}
      />
      <ReviewItem
        small
        label="start date"
        value={toText(data.startDate, 'SendMoneyStartDate', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAddress')}
      />

      <ReviewItem
        small
        label="ends after"
        value={toText(data.endsAfter, 'SendMoneyFrom', 'body2')}
        subValue={data.endsAfterSub}
        alignItems="flex-start"
        {...testProps('SendMoneyFrom')}
      />
    </>
  );
}

export default UnEnrolledCustomFrequency;
