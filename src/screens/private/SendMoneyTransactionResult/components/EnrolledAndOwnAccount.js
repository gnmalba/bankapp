/**
 * @format
 * @flow
 */
// React modules
import React from 'react';

// Components
import ReviewItem from '@composition/InstallmentDetail';

// Utils
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropTypes} from './types';

function EnrolledAndOwnAccount({data}: PropTypes): React$Node {
  return (
    <>
      <ReviewItem
        small
        label="TO"
        value={toText(data.toAlias, 'SendMoneyTo', 'body2')}
        subValue={data.toBankName.toUpperCase()}
        alignItems="flex-start"
        {...testProps('SendMoneyTo')}
      />

      <ReviewItem
        small
        label="account no."
        value={toText(data.accountNo, 'SendMoneyAccountNumber', 'body2')}
        alignItems="flex-start"
        {...testProps('SendMoneyAccountNo')}
      />
    </>
  );
}

export default EnrolledAndOwnAccount;
