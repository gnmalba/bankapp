/**
 * @format
 * @flow
 */
// React modules
import React from 'react';

// Components
import Text from '@components/Text';
import Divider from '@components/Divider';
import ReviewItem from '@composition/InstallmentDetail';
import EnrolledAndOwnAccount from './EnrolledAndOwnAccount';
import UnEnrolledImmediate from './UnEnrolledImmediate';
import UnEnrolledOneTime from './UnEnrolledOneTime';
import UnEnrolledMonthlyFrequency from './UnEnrolledMonthlyFrequency';
import UnEnrolledCustomFrequency from './UnEnrolledCustomFrequency';

// Utils
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropTypes} from './types';
import * as S from '../Styled';

function SendMoneyTransaction({data}: PropTypes): React$Node {
  const textStyle = {textAlign: 'center'};

  const renderAccountSection = (accountType: string) => {
    switch (accountType) {
      case 'EnrolledAndOwnAccount':
        return <EnrolledAndOwnAccount data={data} />;
      case 'UnEnrolledImmediate':
        return <UnEnrolledImmediate data={data} />;
      case 'UnEnrolledOneTime':
        return <UnEnrolledOneTime data={data} />;
      case 'UnEnrolledMonthlyFrequency':
        return <UnEnrolledMonthlyFrequency data={data} />;
      case 'UnEnrolledCustomFrequency':
        return <UnEnrolledCustomFrequency data={data} />;
      default:
        return null;
    }
  };

  return (
    <>
      <S.TextContainer>
        <Text mode="body2" themeColor="TEXT_100_HIGH" style={textStyle}>
          You’ll be notified via email and SMS once this transaction is
          successful.
        </Text>
      </S.TextContainer>
      <S.EndOfTransactionDetailsContainter>
        <ReviewItem
          small
          label="FROM"
          value={toText(data.fromAlias, 'SendMoneyFrom', 'body2')}
          subValue={data.cardNo}
          alignItems="flex-start"
          {...testProps('SendMoneyFrom')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          small
          label="AMOUNT"
          value={toText(
            `${data.currency || 'PHP'} ${data.amount}`,
            'SendMoneyAmount',
            'body2',
          )}
          {...testProps('SendMoneyAmount')}
        />

        <ReviewItem
          small
          label="FEE"
          value={toText(
            `${data.currency || 'PHP'} ${data.fee}`,
            'SendMoneyFee',
            'body2',
          )}
          {...testProps('SendMoneyFee')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        {renderAccountSection('EnrolledAndOwnAccount')}

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          small
          label="NOTES"
          value={toText(data.notes, 'SendMoneyNotes', 'body2')}
          {...testProps('SendMoneyNotes')}
        />

        <S.DividerContainer>
          <Divider />
        </S.DividerContainer>

        <ReviewItem
          small
          label="reference no."
          value={toText(data.referenceNo, 'SendMoneyReferenceNumber', 'body2')}
          {...testProps('SendMoneyReferenceNo')}
          alignItems="flex-start"
        />

        <ReviewItem
          small
          label="date and time"
          value={toText(data.dateAndTime, 'SendMoneyDateAndTime', 'body2')}
          {...testProps('SendMoneyDateTime')}
          alignItems="flex-start"
        />
      </S.EndOfTransactionDetailsContainter>
    </>
  );
}

export default SendMoneyTransaction;
