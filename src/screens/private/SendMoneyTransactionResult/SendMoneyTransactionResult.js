/**
 * @flow
 */
// React, React Native modules
import React from 'react';
import {Pressable} from 'react-native';

// 3rd Party Modules
import {useDispatch} from 'react-redux';

// Components
import Layout from '@components/Layout';
import Text from '@components/Text';

// Component Flow Types
import type {PropsType} from './types';
import {SendMoneyTransaction} from './components';

// Component Style
import * as S from './Styled';

// Utils
import {SHORTCUT_ICONS} from '@utils/constants';
import {useRoute} from '@react-navigation/core';

function Header({navigate}) {
  return (
    <S.HeaderContainer>
      <S.IconContainer>
        <Pressable onPress={navigate('dashboard')}>
          <S.CloseButton />
        </Pressable>
      </S.IconContainer>
      <S.TextContainer>
        <Text mode="h3">Your transaction is being processed.</Text>
      </S.TextContainer>
    </S.HeaderContainer>
  );
}

function SendMoneyTransactionResult({
  navigation,
  isAccountUnEnroll = true,
}: PropsType): React$Node {
  const dispatch = useDispatch();

  const navigate = (routeName) => () => {
    if (isAccountUnEnroll) {
      dispatch({
        type: 'SHOW_ENROLL_BILLER_PROMPT',
        payload: {
          accountNo: '3211125512355',
          alias: 'Denver Gomez',
          firstName: 'Denver',
          lastName: 'Gomez',
          emailAddress: 'rmendeja@gmail.com',
          bank: {
            name: 'METROBANK',
          },
          mobileNumber: '9476458333',
          countryCode: {phoneCode: '+63', image: 'PH'},
          businessName: 'BusinessName',
        },
      });
    }
    navigation.navigate(routeName);
  };
  const route = useRoute();
  const {formValues} = route.params;
  const {
    sourceAccount,
    recipientAccount,
    amount,
    amountCurrency,
    fee,
    notes,
    paymentDate,
    finalPaymentDate,
    frequency,
    occurrence,
  } = formValues;

  const mappedFormValues = {
    fromAlias: sourceAccount.alias,
    cardNo: sourceAccount.identifier,
    amount,
    currency: amountCurrency,
    fee,
    toAlias: recipientAccount.alias,
    toBankName: 'METROBANK',
    accountNo: recipientAccount.identifier,
    notes,
    referenceNo: '-',
    dateAndTime: '-',

    address: '-',
    mobile: '-',
    email: '-',
    paymentDate: paymentDate,
    paymentFrequency: frequency,
    startDate: paymentDate,
    endsOn: finalPaymentDate,
    repeatsEvery: frequency,
    endsAfter: occurrence,
    endsAfterSub: '-',
  };

  return (
    <Layout>
      <S.ScrollContainer>
        <S.Container>
          <Header navigate={navigate} />
          <SendMoneyTransaction data={mappedFormValues} />
          <Pressable onPress={() => {}}>
            <S.TransacitonResultContainer>
              <S.Icon source={SHORTCUT_ICONS.SEND_MONEY} />
              <Text mode="body2">Save to device</Text>
            </S.TransacitonResultContainer>
          </Pressable>
        </S.Container>
      </S.ScrollContainer>
    </Layout>
  );
}

export default SendMoneyTransactionResult;
