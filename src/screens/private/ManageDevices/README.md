## ManageDevices
ManageDevices component.

# Usage
```js
import ManageDevices from '@screens/private/ManageDevices';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
