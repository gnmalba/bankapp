import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import ManageDevices from '../ManageDevices';
import MockProvider from '@utils/testFramework';

describe('ManageDevices', () => {
  it('Should work as expected', () => {
    const store = {
      manageSettings: {
        loading: false,
        devices: [],
      },
    };

    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageDevices />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
