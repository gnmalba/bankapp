/**
 * @flow
 */

import {UI} from '@utils/manageSettings';
import {verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import IconTooltip from '@components/IconTooltip';

export const Container: ComponentType<any> = styled.View``;

export const DeviceItemRow: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: ${verticalScale(UI.MARGINS.separator_8 * 1.5)}px 0;
`;

export const DeviceItemUnlinkButton: ComponentType<any> = styled.View`
  margin-left: auto;
  flex: 1;
`;

export const DeviceItemDetails: ComponentType<any> = styled.View`
  flex: 5;
  margin-left: ${verticalScale(UI.MARGINS.separator_8)};
`;

export const DeviceItemIcon: ComponentType<any> = styled.View`
  align-items: center
  flex: 1;
  margin-left: ${verticalScale(-UI.MARGINS.separator_8 * 1)}
`;

export const DeviceItemIconTooltip: ComponentType<any> = styled(
  IconTooltip,
).attrs((props) => ({color: props.theme.deviceIcon}))``;
