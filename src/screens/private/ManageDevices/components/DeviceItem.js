import Separator from '@components/Separator';
import Text from '@components/Text';
import {DEVICE_ICONS, formatDate, isMobile, UI} from '@utils/manageSettings';
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {
  DeviceItemDetails,
  DeviceItemIcon,
  DeviceItemIconTooltip,
  DeviceItemRow,
  DeviceItemUnlinkButton,
} from '../Styled';
import type {DeviceItemType} from '../types';

const DeviceItem = (props: DeviceItemType): React$Node => {
  const {onUnlinkPress, deviceItem: item} = props;
  const isMobileDevice = isMobile(item.brand);

  const handlePress = () => {
    if (onUnlinkPress) {
      onUnlinkPress(item);
    }
  };

  return (
    <DeviceItemRow>
      <DeviceItemIcon>
        <DeviceItemIconTooltip
          source={isMobileDevice ? DEVICE_ICONS.mobile : DEVICE_ICONS.pc}
          width={isMobileDevice ? 15 : 25}
          height={isMobileDevice ? 25 : 40}
        />
      </DeviceItemIcon>
      <DeviceItemDetails>
        <Text mode="body2">{item.model}</Text>
        <Separator space={UI.MARGINS.separator_8 / 2} />
        <Text mode="subBody" themeColor="TEXT_100_HIGH">
          Last login: {formatDate(item.lastUse)}
        </Text>
      </DeviceItemDetails>
      <DeviceItemUnlinkButton>
        <TouchableOpacity onPress={handlePress}>
          <Text mode="link1" themeColor="SECONDARY_DEFAULT">
            Unlink
          </Text>
        </TouchableOpacity>
      </DeviceItemUnlinkButton>
    </DeviceItemRow>
  );
};

export default DeviceItem;
