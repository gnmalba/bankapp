/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';

import type {PropsType} from './types';
import type {
  DeviceListItemType,
  ManageSettingsStateType,
} from '@ducks/managesettings/types';
import {} from './Styled';
import Layout from '@components/Layout';
import Text from '@components/Text';
import {buildUnlinkSuccessMessage, ERRORS, UI} from '@utils/manageSettings';
import Separator from '@components/Separator';
import {fetchDevices, unlinkDevice} from '@ducks/managesettings/actions';
import Loader from '@composition/Loader';
import {useDispatch, useSelector} from 'react-redux';
import {manageSettingsSelector} from '@ducks/managesettings/selectors';
import {FlatList} from 'react-native';
import DeviceItem from './components/DeviceItem';
import Snackbar from '@components/Snackbar';
import {timeSnackbar} from '@components/Snackbar/helpers';
import ErrorModal from '@components/ErrorModal';
import ModalPasscode from '@components/ModalPasscode';
import {getDeviceInfo} from '@utils/commons';

// eslint-disable-next-line sonarjs/cognitive-complexity
const ManageDevices = (props: PropsType): React$Node => {
  const dispatch = useDispatch();

  const manageSettingsState: ManageSettingsStateType = useSelector(
    manageSettingsSelector,
  );
  const {loading, devices, isUnlinkSuccess, unlinkError} = manageSettingsState;

  const [selectedDevice, setSelectedDevice] = useState();
  const [passcodeModalVisible, setPasscodeModalVisible] = useState(false);
  const [snackBarVisible, setSnackbarVisible] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errorModalProps, setErrorModalProps] = useState({
    errorText: '',
    onPressButton: () => {},
    buttonTitle: 'Go back',
  });

  const [passcode, setPasscode] = useState('');

  const onUnlinkPress = (device: DeviceListItemType) => {
    const currDevice = getDeviceInfo();
    if (devices.length === 1) {
      setErrorModalVisible(true);
      setErrorModalProps({
        ...errorModalProps,
        errorText: 'You cannot unlink all your devices.',
        onPressButton: closeErrorModal,
      });
    } else if (device.deviceId === currDevice.deviceId) {
      setErrorModalVisible(true);
      setErrorModalProps({
        ...errorModalProps,
        errorText: 'You cannot unlink your current device.',
        onPressButton: closeErrorModal,
      });
    } else {
      setSelectedDevice(device);
      setPasscodeModalVisible(true);
    }
  };

  const closeErrorModal = () => setErrorModalVisible(false);

  const handleSubmit = () => {
    if (selectedDevice) {
      setPasscodeModalVisible(false);
      dispatch(unlinkDevice(selectedDevice.deviceId, passcode));
    }
  };

  const renderDevice = ({item}) => {
    return <DeviceItem deviceItem={item} onUnlinkPress={onUnlinkPress} />;
  };
  useEffect(() => {
    dispatch(fetchDevices());
  }, [dispatch]);

  useEffect(() => {
    if (isUnlinkSuccess && !loading) {
      timeSnackbar(3000, setSnackbarVisible);
      setPasscode('');
    }
  }, [isUnlinkSuccess, loading]);

  useEffect(() => {
    if (unlinkError) {
      if (unlinkError.code === ERRORS.CODES.MAX_PASSCODE_ATTEMPTS) {
        setErrorModalVisible(true);
        setErrorModalProps({
          ...errorModalProps,
          errorText: ERRORS.MESSAGES.MAX_PASSCODE_ATTEMPTS,
          onPressButton: () => setErrorModalVisible(false),
        });
      } else {
        setErrorModalVisible(true);
        setErrorModalProps({
          ...errorModalProps,
          errorText: unlinkError.message,
          onPressButton: closeErrorModal,
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [unlinkError]);

  return (
    <Layout padding={UI.PADDINGS.layout}>
      <Text mode="h2">Manage Devices</Text>
      <Separator space={UI.MARGINS.separator_8} />
      <Text mode="body2" themeColor="TEXT_100_HIGH">
        Your Metrobank Online account are currently logged in in these devices
      </Text>
      <Separator space={UI.MARGINS.separator_40} />
      <Text mode="overline2" themeColor="TEXT_100_HIGH">
        device name
      </Text>
      <Separator space={UI.MARGINS.separator_40 / 2} />
      {loading ? (
        <Loader isVisible />
      ) : (
        <FlatList
          data={devices}
          renderItem={renderDevice}
          keyExtractor={(item, index) => item.deviceId + index}
          showsVerticalScrollIndicator={false}
        />
      )}
      <ModalPasscode
        isVisible={passcodeModalVisible}
        mainButtonLabel="Unlink my device"
        negativeCallback={() => setPasscodeModalVisible(false)}
        positiveCallback={handleSubmit}
        passcodeError=""
        passcode={passcode}
        setPasscode={setPasscode}
      />
      <Snackbar
        visible={snackBarVisible}
        message={buildUnlinkSuccessMessage(
          selectedDevice ? selectedDevice.model : '',
        )}
      />
      <ErrorModal isVisible={errorModalVisible} {...errorModalProps} />
    </Layout>
  );
};

export default ManageDevices;
