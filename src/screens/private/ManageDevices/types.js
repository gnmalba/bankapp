/**
 * @flow
 */

import type {DeviceListItemType} from '@ducks/managesettings/types';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
};

export type DeviceItemType = {
  isMobile: boolean,
  name: string,
  lastLogin: string,
  onUnlinkPress: Function,
  deviceItem: DeviceListItemType,
};
