/**
 * @format
 * @flow
 */

import React, {useState, useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/native';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Components
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Ducks
import {
  reportLostCardRequested,
  reportLostCardResetErrors,
} from '@ducks/lostcard/actions';

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import PasscodeScreen from './PasscodeScreen';

const ReportLostCardPasscode = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => state.authentication);
  const reportLostCard = useSelector((state) => state.reportLostCard);
  const {loading, card, data, error} = reportLostCard;
  const errorCode = data?.data?.errors;
  const isMPINExceed =
    errorCode && errorCode[0]?.code === 'invalid_mpin_exceeded';
  const [passcode, setPasscode] = useState('');
  const [passcodeError, setPasscodeError] = useState('');

  useFocusEffect(
    useCallback(() => {
      dispatch(reportLostCardResetErrors(false));
    }, [dispatch]),
  );

  const handleSubmit = () => {
    const params = {
      cardSerialNo: card?.cardSerNo,
      username: userInfo?.username,
      passcode,
      navigation: props.navigation,
    };
    dispatch(reportLostCardRequested(params));
  };

  const handleTextChange = (text) => {
    setPasscode(text);
    if (text.length === 6) {
      setPasscodeError('');
    }
  };

  const renderErrorScreen = () => (
    <ErrorScreen
      theme={theme}
      showBackButton
      headingAlignCenter={isMPINExceed}
      heading={
        isMPINExceed
          ? 'You have reached the maximum number of attempts. Please try again after (x) minutes.'
          : 'Oops! Something went wrong.'
      }
      details={
        errorCode
          ? errorCode[0]?.code === 'invalid_mpin_exceeded'
            ? null
            : errorCode[0]?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      isSystemError={errorCode ? false : true}
      handlePrimaryButton={() => props.navigation.pop()}
      handleSecondaryButton={() => props.navigation.navigate('Drawer')}
      buttonContents={
        <Button
          title="Go back"
          type="primary"
          state="default"
          {...testProps('lostCardPasscodeErrorBtn')}
          onPress={() => props.navigation.pop()}
        />
      }
    />
  );

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  if (error && errorCode && errorCode[0].code !== 'mpin') {
    return renderErrorScreen();
  }

  return (
    <PasscodeScreen
      error={error}
      passcode={passcode}
      passcodeError={passcodeError}
      setPasscodeError={setPasscodeError}
      onTextChange={(text) => handleTextChange(text)}
      handleNext={handleSubmit}
    />
  );
};

export default ReportLostCardPasscode;
