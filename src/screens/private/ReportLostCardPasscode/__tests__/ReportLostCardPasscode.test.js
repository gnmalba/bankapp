import React from 'react';
import {render} from '@testing-library/react-native';
import ReportLostCardPasscode from '../ReportLostCardPasscode';
import MockProvider from '@utils/testFramework';
import {NavigationContext} from '@react-navigation/native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('ReportLostCardPasscode', () => {
  const mockStore = () => ({
    reportLostCard: {
      error: false,
      loading: false,
      card: {
        cardName: 'test',
        cardNumber: '1234',
        currencyCode: 'PHP',
        replacementFee: 2,
      },
      data: undefined,
    },
  });
  //mock navigation
  const navContext = {
    isFocused: () => true,
    addListener: jest.fn(() => jest.fn()),
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <NavigationContext.Provider value={navContext}>
            <ReportLostCardPasscode />
          </NavigationContext.Provider>
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
