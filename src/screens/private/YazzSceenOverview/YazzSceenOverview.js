/**
 * @format
 * @flow
 */

import React from 'react';
import {useDispatch, useSelector} from 'react-redux';

import type {PropsType} from './types';
import Layout from '@components/Layout';
import {FlatList} from 'react-native';
import {Container} from './Styled';

import CardImageBackground from '@composition/CardImageBackground';
import AccountDetails from '@composition/AccountDetails';
import ActionShortcuts from '@composition/ActionShortcuts';
import TransactionHistory from '@composition/TransactionHistory';
import Loader from '@composition/Loader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {aliasSelector} from '@ducks/manageAccountSettings/selectors';
import type {Account} from '@ducks/pay-bills-credit/types';
import {setPbcSourceAccount} from '@ducks/pay-bills-credit/actions';
import {formatAccountObject} from '@ducks/pay-bills-credit/utils';

const YazzSceenOverview = (props: PropsType): React$Node => {
  const {route} = props;
  const accountId = route?.params?.id;
  const dispatch = useDispatch();
  const yazzTransaction = useSelector((state) => state.yazz.transaction);
  const yazzDetails = useSelector((state) => state.yazz.cardDetails);
  const selectedYazzAccount = useSelector((state) => state.yazz.data);
  const alias = useSelector(aliasSelector);
  const currencyCode = yazzDetails[0]?.currency;
  const newYazzDetails = yazzDetails[0];

  if (newYazzDetails) {
    newYazzDetails.alias = alias;
  }

  React.useEffect(() => {
    dispatch({type: 'GET_YAZZ_CARD_DETAILS_REQUESTED', payload: accountId});
    dispatch({
      type: 'GET_YAZZ_TRANSACTION_HISTORY_REQUESTED',
      payload: {cardSerialNo: accountId},
    });
  }, [accountId, dispatch]);

  /*###### PAY BILLS CREDIT ACTIONS #######*/
  const setSourceAccount = (account: Account): any =>
    dispatch(setPbcSourceAccount(account));

  const actionConditions = {
    PAY_BILLS: (item, objectDetails) =>
      setSourceAccount(formatAccountObject(objectDetails)),
    //add other action codes here ex: SEND_MONEY: (item, objectDetails) => {function here}
  };

  const handleActionPress = (item: Object) => {
    actionConditions[item.code] &&
      actionConditions[item.code](item, selectedYazzAccount[0]);
  };

  const renderScreen = () => {
    return (
      <>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          enableOnAndroid>
          <Container>
            <CardImageBackground cardType="yazz" data={yazzDetails[0]} />
            <AccountDetails cardType="yazz" data={newYazzDetails} />
            <ActionShortcuts
              cardType="yazz"
              accountData={yazzDetails[0]}
              data={yazzDetails[0]}
              onShortCutPress={handleActionPress}
            />
          </Container>
          <TransactionHistory
            cardType="yazz"
            data={yazzDetails[0]}
            transaction={{
              data: {
                transactionHistory: yazzTransaction,
              },
            }}
            titleTransaction="Transaction History"
            accountId={accountId}
            currencyCode={currencyCode}
          />
        </KeyboardAwareScrollView>
      </>
    );
  };

  return (
    <Layout>
      <FlatList
        keyboardShouldPersistTaps="handled"
        data={yazzDetails}
        renderItem={renderScreen}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={() => <Loader isVisible={true} />}
      />
    </Layout>
  );
};

export default YazzSceenOverview;
