import React from 'react';
import {render} from '@testing-library/react-native';
import YazzSceenOverview from '../YazzSceenOverview';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {testProps} from '@utils/componentCustomProps';

describe('YazzSceenOverview', () => {
  const store = {
    yazz: {
      transaction: [],
      data: [],
      cardDetails: [
        {
          currency: 'PHP',
        },
      ],
    },
    manageAccountSettings: {
      snackbar: false,
      selectedAccount: {
        alias: 'Gab savings',
      },
    },
  };
  it('Should work as expected', () => {
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <YazzSceenOverview {...testProps('yazzSceenOverviewTestId')} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
