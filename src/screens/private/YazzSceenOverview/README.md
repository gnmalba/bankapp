## YazzSceenOverview
YazzSceenOverview component.

# Usage
```js
import YazzSceenOverview from '@screens/private/YazzSceenOverview';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
