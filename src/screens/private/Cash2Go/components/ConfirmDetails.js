/**
 * @format
 * @flow
 */

import Text from '@components/Text';
import {HeaderContainer as Container} from '@utils/commonStyles';
import React from 'react';
import {View} from 'react-native';
import InstallmentDetail from '@composition/InstallmentDetail';
import {toText} from '@utils/helpers';
import {accountsFormat16} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import Divider from '@components/Divider';
import {numberFormatClean} from '@utils/numberInputs';
import {formatEAIR} from '@utils/installments';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';
import Separator from '@components/Separator';

export type PropTypes = {
  state: Object,
  navigation: Object,
};

const ConfirmDetails = ({state, navigation}: PropTypes): React$Node => {
  const renderDepositTo = () => {
    if (state.to.name) {
      return (
        <CardDetailWithLabel
          label="deposit to"
          name={state.to.name}
          cardNumber={accountsFormat16(state.to.number)}
          nameMode="body1"
          numberMode="caption"
          testID="C2GOReviewScreenDepositToValue"
        />
      );
    }
    if (!state.to.destinationBankName) {
      return (
        <CardDetailWithLabel
          label="deposit to"
          name={state.to.destinationAccountName}
          cardNumber={accountsFormat16(state.to.destinationAccountNumber)}
          nameMode="body1"
          numberMode="caption"
          testID="C2GOReviewScreenDepositToValue"
        />
      );
    } else {
      return (
        <>
          <CardDetailWithLabel
            label="deposit to"
            name={state.to.destinationAccountName}
            cardNumber={state.to.destinationBankName}
            nameMode="body1"
            numberMode="caption"
            testID="C2GOReviewScreenDepositToValue"
          />
          <InstallmentDetail
            label="account no"
            value={toText(
              accountsFormat16(state.to.destinationAccountNumber),
              'C2GOReviewScreenAccountNo',
            )}
            {...testProps('C2GOReviewScreenAccountNo')}
          />
        </>
      );
    }
  };

  return (
    <>
      <Container marginTop={12} marginBottom={20}>
        <View>
          <CardDetailWithLabel
            label="from"
            cardNumber={accountsFormat16(state.from.cardNumber)}
            name={state.from.cardEmbossedName}
            nameMode="body1"
            numberMode="caption"
            testID="C2GOReviewScreenTransferFromValue"
          />
          <Separator space={12} />
          <Divider {...testProps('C2GOReviewScreenDivider1')} />
          <InstallmentDetail
            label="principal amount"
            value={toText(
              'PHP ' + numberFormatClean(state.amount),
              'C2GOReviewScreenPrincipalAmount',
            )}
            {...testProps('C2GOReviewScreenPrincipalAmount')}
          />
          <InstallmentDetail
            label="monthly amortization"
            value={toText(
              'PHP ' +
                numberFormatClean(state.paymentTerms.monthlyAmortization),
              'C2GOReviewScreenMonthlyAmortization',
            )}
            {...testProps('C2GOReviewScreenMonthlyAmortization')}
          />
          <InstallmentDetail
            label="monthly add-on rate"
            value={toText(
              state.paymentTerms.interestRate,
              'C2GOReviewScreenMonthlyAddOnRate',
            )}
            {...testProps('C2GOReviewScreenMonthlyAddOnRate')}
          />
          <InstallmentDetail
            label="effective annual interest rate"
            value={toText(
              formatEAIR(state.paymentTerms.annualRate),
              'C2GOReviewScreenEffectiveAnnualInterestRate',
            )}
            {...testProps('C2GOReviewScreenEffectiveAnnualInterestRate')}
          />
          <InstallmentDetail
            label="payment period"
            value={toText(
              state.paymentTerms.term + ' months',
              'C2GOReviewScreenPaymentPeriod',
            )}
            {...testProps('C2GOReviewScreenPaymentPeriod')}
          />
          <InstallmentDetail
            label="total installment amount"
            value={toText(
              'PHP ' +
                numberFormatClean(state.paymentTerms.totalInstallmentAmount),
              'C2GOReviewScreenTotalInstallmentAmount',
            )}
            {...testProps('C2GOReviewScreenTotalInstallmentAmount')}
          />
          {renderDepositTo()}
          <Separator space={12} />
          <Divider {...testProps('C2GOReviewScreenDivider2')} />
        </View>
        <Container marginTop={12}>
          <Text
            mode="body2"
            themeColor="TEXT_100_HIGH"
            {...testProps('C2GOReviewScreenMonthlyMessageId')}>
            * Final monthly amortization amount may change when value is rounded
            off. Additional processing fees may be charged.
          </Text>
        </Container>
        <Container marginTop={12}>
          <Text mode="body2" {...testProps('C2GOReviewScreenFooterMessageId')}>
            By clicking ‘confirm’, you have read and agreed to the{' '}
            <Text
              mode="link1"
              themeColor="SECONDARY_DEFAULT"
              onPress={() => navigation.navigate('TermsConditionScreen')}>
              terms and conditions
            </Text>
            , payment, fees applied to this transaction.
          </Text>
        </Container>
      </Container>
    </>
  );
};

export default ConfirmDetails;
