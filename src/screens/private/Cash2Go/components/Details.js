// @flow
import React from 'react';
import type {Cash2GoInstallmentType} from '@ducks/installments/types';
import Layout from '@components/Layout';
import InstallmentDetail from '@composition/InstallmentDetail';
import {toText} from '@utils/helpers';
import Divider from '@components/Divider';
import {accountsFormat16} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import moment from 'moment';
import {StatusText} from '@composition/InstallmentItem/Styled';
import {
  DISBURSEMENT_OPTIONS,
  formatEAIR,
  strReplaceAll,
} from '@utils/installments';
import {numberFormatClean} from '@utils/numberInputs';
import {
  FlexGrowScrollView,
  FlexGrowScrollViewChildWrapper,
} from '@utils/commonStyles';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';

const Details = (props: any): React$Node => {
  const item: Cash2GoInstallmentType = props.route.params.item;

  const renderDepositTo = () => {
    if (
      item.DISBURSTMENTOPTION === DISBURSEMENT_OPTIONS.ENROLLED ||
      item.DISBURSTMENTOPTION === DISBURSEMENT_OPTIONS.UNENROLLED
    ) {
      return (
        <CardDetailWithLabel
          small
          label="deposit to"
          name={item.ACCOUNTNAME}
          cardNumber={accountsFormat16(item.ACCOUNTNUMBER)}
          nameMode="inputerror"
          numberMode="caption"
          testID="C2GODepositToValue"
        />
      );
    } else {
      return (
        <>
          <CardDetailWithLabel
            small
            label="to"
            name={item.ACCOUNTNAME}
            cardNumber={item.BANKNAME}
            nameMode="inputerror"
            numberMode="caption"
            testID="C2GODepositToValue"
          />

          <InstallmentDetail
            label="account no"
            value={toText(
              accountsFormat16(item.ACCOUNTNUMBER),
              'C2GOAccountNo',
              'inputerror',
            )}
            {...testProps('C2GOAccountNo')}
          />
        </>
      );
    }
  };

  return (
    <Layout padding={32}>
      <FlexGrowScrollView>
        <FlexGrowScrollViewChildWrapper>
          <InstallmentDetail
            small
            label="status"
            value={
              <StatusText mode="overline2" status={item.STATUS}>
                {item.STATUS}
              </StatusText>
            }
            {...testProps('C2GOStatus')}
          />
          <Divider {...testProps('C2GODivider1')} />

          <CardDetailWithLabel
            small
            label="from"
            name={item.CARDHOLDERNAME || 'N/A'}
            cardNumber={accountsFormat16(item.CARDNUMBER)}
            nameMode="inputerror"
            numberMode="caption"
            testID="C2GOFromValue"
          />
          <Divider {...testProps('C2GODivider2')} />
          <InstallmentDetail
            small
            label="principal amount"
            value={toText(
              'PHP ' + numberFormatClean(item.AMOUNT),
              'C2GOPrincipalAmount',
              'inputerror',
            )}
            {...testProps('C2GOPrincipalAmount')}
          />
          <InstallmentDetail
            small
            label="monthly amortization"
            value={toText(
              'PHP ' + numberFormatClean(item.MONTHLYAMORTIZATION),
              'C2GOMonthlyAmortization',
              'inputerror',
            )}
            {...testProps('C2GOMonthlyAmortization')}
          />
          <InstallmentDetail
            small
            label="monthly add-on rate"
            value={toText(
              item.RATE + ' %',
              'C2GOMonthlyAddOnRate',
              'inputerror',
            )}
            {...testProps('C2GOMonthlyAddOnRate')}
          />
          <InstallmentDetail
            small
            label="effective annual interest rate"
            value={toText(
              formatEAIR(item.EAIR),
              'C2GOEffectiveAnnualInterestRate',
              'inputerror',
            )}
            {...testProps('C2GOEffectiveAnnualInterestRate')}
          />
          <InstallmentDetail
            small
            label="payment period"
            value={toText(
              `${item.TERM} months`,
              'C2GOPaymentPeriod',
              'inputerror',
            )}
            {...testProps('C2GOPaymentPeriod')}
          />
          <InstallmentDetail
            small
            label="total installment amount"
            value={toText(
              'PHP ' + numberFormatClean(item.TOTALINSTALLMENTAMOUNT),
              'C2GOTotalInstallmentAmount',
              'inputerror',
            )}
            {...testProps('C2GOTotalInstallmentAmount')}
          />
          {renderDepositTo()}
          <Divider {...testProps('C2GODivider3')} />
          <InstallmentDetail
            small
            label="reference number"
            value={toText(
              strReplaceAll(item.REFERENCENUMBER, '', '-'),
              'C2GOReferenceNumber',
              'inputerror',
            )}
            {...testProps('C2GOReferenceNumber')}
          />
          <InstallmentDetail
            small
            label="date and time"
            value={toText(
              moment(item.TRANSTATUS_DATECREATED).format(
                'MMM-DD-yyyy [\n]h:mm A [(GMT +8)]',
              ),
              'C2GODateAndTime',
              'inputerror',
            )}
            {...testProps('C2GODateAndTime')}
          />
        </FlexGrowScrollViewChildWrapper>
      </FlexGrowScrollView>
    </Layout>
  );
};

export default Details;
