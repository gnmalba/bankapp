// @flow
import InstallmentItem from '@composition/InstallmentItem';
import {installmentsSelector} from '@ducks/installments/selectors';
import moment from 'moment';
import React from 'react';
import {FlatList} from 'react-native';
import {useSelector} from 'react-redux';

const List = (): React$Node => {
  const installmentsState = useSelector(installmentsSelector);
  const {allInstallments} = installmentsState;

  const renderItem = ({item}) => {
    return (
      <InstallmentItem
        amount={item.AMOUNT}
        monthlyAmortization={item.MONTHLYAMORTIZATION}
        duration={item.TERM}
        status={item.STATUS}
        date={moment(item.TRANSTATUS_DATECREATED).format('MMMM DD, YYYY')}
        onPress={() => {}}
      />
    );
  };

  return (
    <>
      <FlatList data={allInstallments.c2g} renderItem={renderItem} />
    </>
  );
};

export default List;
