/**
 * @format
 * @flow
 */

import {ReviewDetailsContainer} from '@screens/private/ReviewDetailsScreen/Styled';
import React from 'react';
import InstallmentDetail from '@composition/InstallmentDetail';
import {toText} from '@utils/helpers';
import {accountsFormat16} from '@utils/accountNumber';
import {formatEAIR, renderSuccessDetails} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';
import Divider from '@components/Divider';
import {numberFormatClean} from '@utils/numberInputs';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';
import Separator from '@components/Separator';

const ReviewDetails = ({state}: any): React$Node => {
  const renderDepositTo = () => {
    if (state.to.name) {
      return (
        <CardDetailWithLabel
          small
          label="deposit to"
          name={state.to.name}
          cardNumber={accountsFormat16(state.to.number)}
          nameMode="inputerror"
          numberMode="caption"
          testID="C2GOReviewScreenDepositToValue"
        />
      );
    }
    if (!state.to.destinationBankName) {
      return (
        <CardDetailWithLabel
          small
          label="deposit to"
          name={state.to.destinationAccountName}
          cardNumber={accountsFormat16(state.to.destinationAccountNumber)}
          nameMode="inputerror"
          numberMode="caption"
          testID="C2GOReviewScreenDepositToValue"
        />
      );
    } else {
      return (
        <>
          <CardDetailWithLabel
            small
            label="deposit to"
            name={state.to.destinationAccountName}
            cardNumber={state.to.destinationBankName}
            nameMode="inputerror"
            numberMode="caption"
            testID="C2GOReviewScreenDepositToValue"
          />
          <InstallmentDetail
            small
            label="account no"
            value={toText(
              accountsFormat16(state.to.destinationAccountNumber),
              'C2GOReviewScreenAccountNo',
              'inputerror',
            )}
            {...testProps('C2GOReviewScreenAccountNo')}
          />
        </>
      );
    }
  };
  return (
    <>
      <ReviewDetailsContainer>
        <CardDetailWithLabel
          small
          label="from"
          name={state.from.cardEmbossedName}
          cardNumber={accountsFormat16(state.from.cardNumber)}
          nameMode="inputerror"
          numberMode="caption"
          testID="C2GOReviewScreenFromValue"
        />
        <Separator space={12} />
        <Divider {...testProps('C2GOReviewScreenDivider1')} />
        <InstallmentDetail
          small
          label="principal amount"
          value={toText(
            'PHP ' + numberFormatClean(state.amount),
            'C2GOReviewScreenPrincipalAmount',
            'inputerror',
          )}
          {...testProps('C2GOReviewScreenPrincipalAmount')}
        />
        <InstallmentDetail
          small
          label="monthly amortization"
          value={toText(
            'PHP ' + numberFormatClean(state.paymentTerms.monthlyAmortization),
            'C2GOReviewScreenMonthlyAmortization',
            'inputerror',
          )}
          {...testProps('C2GOReviewScreenMonthlyAmortization')}
        />
        <InstallmentDetail
          small
          label="monthly add-on rate"
          value={toText(
            state.paymentTerms.interestRate,
            'C2GOReviewScreenInterestRate',
            'inputerror',
          )}
          {...testProps('C2GOReviewScreenInterestRate')}
        />
        <InstallmentDetail
          small
          label="effective annual interest rate"
          value={toText(
            formatEAIR(state.paymentTerms.annualRate),
            'C2GOReviewScreenAnnualRate',
            'inputerror',
          )}
          {...testProps('C2GOReviewScreenAnnualRate')}
        />
        <InstallmentDetail
          small
          label="payment period"
          value={toText(
            state.paymentTerms.term + ' months',
            'C2GOReviewScreenPaymentPeriod',
            'inputerror',
          )}
          {...testProps('C2GOReviewScreenPaymentPeriod')}
        />
        <InstallmentDetail
          small
          label="total installment amount"
          value={toText(
            'PHP ' +
              numberFormatClean(state.paymentTerms.totalInstallmentAmount),
            'C2GOReviewScreenTotalInstallmentAmount',
            'inputerror',
          )}
          {...testProps('C2GOReviewScreenTotalInstallmentAmount')}
        />
        {renderDepositTo()}
        <Separator space={12} />
        <Divider {...testProps('C2GOReviewScreenDivider2')} />
        {renderSuccessDetails(state, 'C2GOReviewScreen')}
      </ReviewDetailsContainer>
    </>
  );
};

export default ReviewDetails;
