import {NavigationContext} from '@react-navigation/core';
import {fireEvent, render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import Cash2Go from '../Cash2Go';
import ConfirmDetails from '../components/ConfirmDetails';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

const MOCK_CARDNUMBER = '521822******7813';
const MOCK_AVAILABLECREDIT = '1000000.00';
const MOCK_NAME = 'JOHN PATRICK A VIRAY';

describe('Cash2Go', () => {
  const props = {
    route: {
      params: {
        fromCC: true,
      },
    },
    navigation: {
      navigate: jest.fn(),
    },
  };
  const stores = [
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      installments: {
        cash2Go: {
          amount: 6000,
          to: {
            destinationAccountName: 'jm',
            destinationAccountNumber: '1234123412341',
            destinationBankName: null,
            disbursementOption: 'U',
          },
          from: {
            alias: 'MC Platinum BT D1',
            availableCredit: MOCK_AVAILABLECREDIT,
            cardEmbossedName: MOCK_NAME,
            cardNo: MOCK_CARDNUMBER,
            cardNumber: MOCK_CARDNUMBER,
            cardSerNo: '2686978',
            creditLimit: MOCK_AVAILABLECREDIT,
            currency: 'PHP',
            customerId: '5958',
            flag: '0',
            id: MOCK_CARDNUMBER,
            name: MOCK_NAME,
            number: MOCK_CARDNUMBER,
            outstandingBalance: '0.00',
            primary: 0,
          },
          paymentTerms: {
            annualRate: '12.6077%',
            interestRate: '0.58%',
            monthlyAmortization: '534.80',
            shortCode: 'BT5812M',
            term: '12',
            totalInstallmentAmount: '6417.60',
            transactionFee: '0',
          },
        },
      },
    },
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      installments: {
        cash2Go: {
          amount: 6000,
          to: {
            destinationAccountName: 'jm',
            destinationAccountNumber: '1234123412341',
            disbursementOption: 'N',
            destinationBankName: 'ASD',
          },
          from: {
            alias: 'MC Platinum BT D2',
            availableCredit: MOCK_AVAILABLECREDIT,
            cardEmbossedName: MOCK_NAME,
            cardNo: MOCK_CARDNUMBER,
            cardNumber: MOCK_CARDNUMBER,
            cardSerNo: '2686978',
            creditLimit: MOCK_AVAILABLECREDIT,
            currency: 'PHP',
            customerId: '5958',
            flag: '0',
            id: MOCK_CARDNUMBER,
            name: MOCK_NAME,
            number: MOCK_CARDNUMBER,
            outstandingBalance: '0.00',
            primary: 0,
          },
          paymentTerms: {
            annualRate: '12.6077%',
            interestRate: '0.58%',
            monthlyAmortization: '534.80',
            shortCode: 'BT5812M',
            term: '12',
            totalInstallmentAmount: '6417.60',
            transactionFee: '0',
          },
        },
      },
    },
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      installments: {
        cash2Go: {
          amount: 6000,
          to: {
            destinationAccountName: 'jm',
            destinationAccountNumber: '1234123412341',
            destinationBankName: null,
            cardNumber: '1231231232',
            alias: 'ASDSA',
          },
          from: {
            alias: 'MC Platinum BT D4',
            availableCredit: MOCK_AVAILABLECREDIT,
            cardEmbossedName: MOCK_NAME,
            cardNo: MOCK_CARDNUMBER,
            cardNumber: MOCK_CARDNUMBER,
            cardSerNo: '2686978',
            creditLimit: MOCK_AVAILABLECREDIT,
            currency: 'PHP',
            customerId: '5958',
            flag: '0',
            id: MOCK_CARDNUMBER,
            name: MOCK_NAME,
            number: MOCK_CARDNUMBER,
            outstandingBalance: '0.00',
            primary: 0,
          },
          paymentTerms: {
            annualRate: '12.6077%',
            interestRate: '0.58%',
            monthlyAmortization: '534.80',
            shortCode: 'BT5812M',
            term: '12',
            totalInstallmentAmount: '6417.60',
            transactionFee: '0',
          },
        },
      },
    },
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      installments: {
        cash2Go: {},
      },
    },
  ];

  const navContext = {
    isFocused: () => true,
    addListener: jest.fn(() => jest.fn()),
  };
  it.each(stores)('Should work as expected', (store) => {
    const component = render(
      <NavigationContext.Provider value={navContext}>
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <Cash2Go {...props} />
          </ThemeProvider>
        </MockProvider>
      </NavigationContext.Provider>,
    );

    expect(component.toJSON()).toMatchSnapshot();

    const nextButton = component.getByTestId('C2GNextButton');
    fireEvent(nextButton, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const paymentTermsTO = component.getByTestId('TO Payment Terms');
    fireEvent(paymentTermsTO, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const sourceTO = component.getByTestId('TO From');
    fireEvent(sourceTO, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const ccTO = component.getByTestId('TO Deposit To');
    fireEvent(ccTO, 'onPress');
    expect(props.navigation.navigate).toHaveBeenCalled();

    const amountField = component.getByTestId('Principal amount Field');

    fireEvent(amountField, 'onFocus');
    fireEvent.changeText(amountField, '500');
    fireEvent(amountField, 'onBlur');

    fireEvent.changeText(amountField, '0');
    fireEvent(amountField, 'onBlur');

    fireEvent.changeText(amountField, '9999999');
    fireEvent(amountField, 'onBlur');
  });
});

describe('Confirm Details', () => {
  const listProps = [
    {
      navigation: {navigate: jest.fn()},
      state: {
        amount: 6000,
        to: {
          destinationAccountName: 'jm',
          destinationAccountNumber: '1234123412341',
          destinationBankName: null,
          disbursementOption: 'U',
        },
        from: {
          alias: 'MC Platinum BT D4',
          availableCredit: MOCK_AVAILABLECREDIT,
          cardEmbossedName: MOCK_NAME,
          cardNo: MOCK_CARDNUMBER,
          cardNumber: MOCK_CARDNUMBER,
          cardSerNo: '2686978',
          creditLimit: MOCK_AVAILABLECREDIT,
          currency: 'PHP',
          customerId: '5958',
          flag: '0',
          id: MOCK_CARDNUMBER,
          name: MOCK_NAME,
          number: MOCK_CARDNUMBER,
          outstandingBalance: '0.00',
          primary: 0,
        },
        paymentTerms: {
          annualRate: '12.6077%',
          interestRate: '0.58%',
          monthlyAmortization: '534.80',
          shortCode: 'BT5812M',
          term: '12',
          totalInstallmentAmount: '6417.60',
          transactionFee: '0',
        },
      },
    },
    {
      navigation: {navigate: jest.fn()},
      state: {
        amount: 6000,
        to: {
          destinationAccountName: 'jm',
          destinationAccountNumber: '1234123412341',
          destinationBankName: 'asdsa',
          disbursementOption: 'N',
        },
        from: {
          alias: 'MC Platinum BT D6',
          availableCredit: MOCK_AVAILABLECREDIT,
          cardEmbossedName: MOCK_NAME,
          cardNo: MOCK_CARDNUMBER,
          cardNumber: MOCK_CARDNUMBER,
          cardSerNo: '2686978',
          creditLimit: MOCK_AVAILABLECREDIT,
          currency: 'PHP',
          customerId: '5958',
          flag: '0',
          id: MOCK_CARDNUMBER,
          name: MOCK_NAME,
          number: MOCK_CARDNUMBER,
          outstandingBalance: '0.00',
          primary: 0,
        },
        paymentTerms: {
          annualRate: '12.6077%',
          interestRate: '0.58%',
          monthlyAmortization: '534.80',
          shortCode: 'BT5812M',
          term: '12',
          totalInstallmentAmount: '6417.60',
          transactionFee: '0',
        },
      },
    },
    {
      navigation: {navigate: jest.fn()},
      state: {
        amount: 6000,
        to: {
          destinationAccountName: 'jm',
          destinationAccountNumber: '1234123412341',
          destinationBankName: 'asdsa',
          name: 'QEW',
          number: '4421312',
        },
        from: {
          alias: 'MC Platinum BT DB',
          availableCredit: MOCK_AVAILABLECREDIT,
          cardEmbossedName: MOCK_NAME,
          cardNo: MOCK_CARDNUMBER,
          cardNumber: MOCK_CARDNUMBER,
          cardSerNo: '2686978',
          creditLimit: MOCK_AVAILABLECREDIT,
          currency: 'PHP',
          customerId: '5958',
          flag: '0',
          id: MOCK_CARDNUMBER,
          name: MOCK_NAME,
          number: MOCK_CARDNUMBER,
          outstandingBalance: '0.00',
          primary: 0,
        },
        paymentTerms: {
          annualRate: '12.6077%',
          interestRate: '0.58%',
          monthlyAmortization: '534.80',
          shortCode: 'BT5812M',
          term: '12',
          totalInstallmentAmount: '6417.60',
          transactionFee: '0',
        },
      },
    },
  ];

  it.each(listProps)('Should work as expected', (props) => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ConfirmDetails {...props} />
      </ThemeProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();
  });
});
