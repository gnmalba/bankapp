/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {ScrollView} from 'react-native';

import {verticalScale} from '@utils/scale';

export const ButtonCotainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(15)}px;
`;

export const StyledScrollView: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    contentContainerStyle: {
      paddingBottom: 80,
    },
    showsVerticalScrollIndicator: props?.showsVerticalScrollIndicator || false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
    padding: props.padding || 16,
  }),
)``;
