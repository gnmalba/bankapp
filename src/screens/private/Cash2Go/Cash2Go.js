/**
 * @format
 * @flow
 */

import React, {useCallback, useState, useEffect} from 'react';
import {BackHandler} from 'react-native';
import {useFocusEffect} from '@react-navigation/core';

import type {PropsType} from './types';
import {ButtonCotainer, StyledScrollView} from './Styled';

import Button from '@components/Button';
import Layout from '@components/Layout';
import TouchableDisplay from '@composition/TouchableDisplay';

import AmountInput from '@composition/AmountInput';
import {useDispatch, useSelector} from 'react-redux';
import {
  updateAmount,
  initializeInstallment,
  setRootScreen,
} from '@ducks/installments/actions';
import {installmentsSelector} from '@ducks/installments/selectors';
import {accountsFormat16, maskingFormat} from '@utils/accountNumber';
import {Display1, Display2} from '@composition/TouchableDisplay/Displays';
import {
  AMOUNT_INPUT_ERRORS_INSTALLMENTS,
  getCleanCreditList,
  handleGoBack,
  MINIMUM_AMOUNT,
} from '@utils/installments';
import {numberFormatClean, removeCommas} from '@utils/numberInputs';
import {creditSelector} from '@ducks/credit/selectors';
import {testProps} from '@utils/componentCustomProps';

const CARDNUMBER_STRING = 'CARD NUMBER - ';

const getFromCreditCardContent = (from): string => {
  if (from) {
    const subtitle = CARDNUMBER_STRING + accountsFormat16(from.cardNumber);
    return Display1(from.cardEmbossedName, subtitle, {
      title: 'From Acct Alias',
      subtitle: 'From Acct Card Num',
    });
  }

  return '';
};

const getPaymentTermsContent = (paymentTerms): string => {
  if (paymentTerms) {
    const title = `PHP ${numberFormatClean(
      paymentTerms.monthlyAmortization,
    )} / month`;
    const subtitle = `for ${paymentTerms.term} months (${paymentTerms.interestRate} Monthly Add-on Rate)`;
    return Display1(title, subtitle, {
      title: 'Term Monthly Armortization',
      subtitle: 'Term Interest Rate',
    });
  }

  return '';
};

const getDepositToContent = (to): string => {
  if (to) {
    let subtitle: string;
    if (to.disbursementOption === 'U') {
      subtitle =
        CARDNUMBER_STRING + accountsFormat16(to.destinationAccountNumber);
      return Display1(to.destinationAccountName, subtitle, {
        title: 'To Acct Alias',
        subtitle: 'To Acct Card Num',
      });
    } else if (to.disbursementOption === 'N') {
      const s1: string = `BANK NAME- ${to.destinationBankName}`;
      const s2: string = `ACCOUNT NUMBER - ${accountsFormat16(
        to.destinationAccountNumber,
      )}`;
      return Display2(to.destinationAccountName, s1, s2);
    } else {
      subtitle = CARDNUMBER_STRING + maskingFormat(to.cardNumber, false);
      return Display1(to.alias, subtitle, {
        title: 'To Acct Alias',
        subtitle: 'To Acct Card Num',
      });
    }
  }

  return '';
};

const Cash2Go = (props: PropsType): React$Node => {
  const installmentsState = useSelector(installmentsSelector);
  const creditData = useSelector(creditSelector);
  const {cash2Go} = installmentsState;
  const {from, amount, paymentTerms, to} = cash2Go;

  const [amountError, setAmountError] = useState('');
  const [isAmountFocused, setAmountFocused] = useState();

  const dispatch = useDispatch();

  useFocusEffect(
    useCallback(() => {
      dispatch(initializeInstallment('C2GO', false));
      if (props.route?.params?.fromCC) {
        dispatch(setRootScreen('overview'));
      }
    }, [dispatch, props.route]),
  );

  useEffect(() => {
    setAmountError('');
  }, [from]);

  const isFromDisabled = () => getCleanCreditList(creditData.data).length === 1;
  const navigateToSelectBankScreen = () => {
    props.navigation.navigate('SelectBankAccountScreen');
  };

  const navigateToSelectCardScreen = () => {
    props.navigation.navigate('SelectCardScreen');
  };

  const navigateToPaymentTerms = () => {
    props.navigation.navigate('PaymentTermsScreen');
  };

  const handleNext = () => {
    props.navigation.navigate('ConfirmScreen');
  };

  const handleFocus = () => {
    setAmountFocused(true);
    setAmountError('');
  };

  const onAmountBlur = (value) => {
    setAmountFocused(false);
    let MAX_AMOUNT = -1;
    if (from) {
      MAX_AMOUNT = from.creditLimit * 0.9 - from.outstandingBalance;
    }

    const numValue = Number(removeCommas(value));

    if (numValue === 0) {
      setAmountError(AMOUNT_INPUT_ERRORS_INSTALLMENTS.EMPTY);
    } else if (numValue < MINIMUM_AMOUNT) {
      setAmountError('This value is below the minimum required amount.');
    } else if (numValue > MAX_AMOUNT) {
      setAmountError('This value is over the maximum required amount.');
    }
  };

  const getAmount = amount ? amount : '';

  const isButtonDisabled = () =>
    !(amount && from && paymentTerms && to && !isAmountFocused);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        handleGoBack({
          navigation: props.route?.params.navigation,
          route: props.route,
          installmentsState,
          dispatch,
        });

        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [props.route, installmentsState, dispatch]),
  );

  return (
    <Layout>
      <StyledScrollView padding={32}>
        <TouchableDisplay
          placeholder="Select a credit card"
          label="From"
          onPress={navigateToSelectCardScreen}
          content={getFromCreditCardContent(from)}
          disabled={isFromDisabled()}
        />
        <AmountInput
          value={getAmount}
          label="Principal amount"
          placeholder="Enter amount"
          currency="PHP"
          otherProps={{installmentType: 'C2GO'}}
          dispatchAction={updateAmount}
          errorMessage={amountError}
          onFocus={handleFocus}
          onBlur={onAmountBlur}
        />
        <TouchableDisplay
          disabled={!(amount && from && !amountError && !isAmountFocused)}
          placeholder="Select terms of payment"
          label="Payment Terms"
          onPress={navigateToPaymentTerms}
          content={getPaymentTermsContent(paymentTerms)}
        />
        <TouchableDisplay
          placeholder="Select a bank account"
          label="Deposit To"
          onPress={navigateToSelectBankScreen}
          content={getDepositToContent(to)}
          disabled={!amount || isAmountFocused}
        />

        <ButtonCotainer>
          <Button
            title="Next"
            state={isButtonDisabled() ? 'disabled' : 'default'}
            type="primary"
            onPress={handleNext}
            {...testProps('C2GNextButton')}
          />
        </ButtonCotainer>
      </StyledScrollView>
    </Layout>
  );
};

export default Cash2Go;
