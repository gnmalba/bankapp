## Cash2Go
Cash2Go component.

# Usage
```js
import Cash2Go from '@screens/private/Cash2Go';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
