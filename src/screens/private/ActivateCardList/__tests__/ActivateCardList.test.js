import React from 'react';
import {render} from '@testing-library/react-native';
import ActivateCardList from '../ActivateCardList';

import {ThemeProvider} from 'styled-components/native';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

describe('ActivateCardList', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider
        store={{
          cardActivation: {
            data: [],
            fetching: false,
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ActivateCardList />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
