/**
 * @format
 * @flow
 */

import React from 'react';

import type {ComponentType} from 'react';

import {Image, FlatList, View} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';

import styled from 'styled-components/native';

import type {PropsType} from './types';

import Layout from '@components/Layout';
import Divider from '@components/Divider';
import Text from '@components/Text';
import Button from '@components/Button';

import {moderateScale, scale} from '@utils/scale';
import {maskingFormat} from '@utils/accountNumber';

import {
  getCardsForActivation,
  getCardActivationState,
} from '@ducks/card-activation/selectors';

import {refreshCardsForActivation} from '@ducks/card-activation/actions';

const StyledListItemContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-vertical: ${scale(20)}px;
`;

const StyledInfoContainer = styled.View`
  display: flex;
  justify-content: space-between;
  margin-left: ${scale(15)}px;
`;

const StyledImageInfoContainer = styled.View`
  display: flex;
  flex-direction: row;
`;

const StyledCardImage = styled(Image).attrs((props) => ({
  source: require('@assets/images/credit-card-placeholder.png'),
}))`
  width: ${moderateScale(46)}px;
  height: ${moderateScale(32)}px;
`;

export const StyledMainContainer: ComponentType<any> = styled.View`
  flex: 1;
  padding-horizontal: ${scale(30)}px;
`;

const StyledActivateButton = styled(Button).attrs({
  state: 'default',
  title: 'Activate',
  type: 'primary',
  size: 'regular',
})`
  padding-horizontal: ${scale(5)}px;
`;

/**
 * TODO: Should remove this if there's not card loaded
 */
const StyledCreditCardsHeader = styled(Text).attrs((props) => ({
  mode: 'overline1',
  customColor: props.theme.color.TEXT_200_MEDIUIM,
  children: 'CREDIT CARDS',
}))`
  margin-top: ${scale(30)}px;
`;

const StyledDividerForEmptyComponent = styled(Divider).attrs({
  marginVertical: scale(10),
})``;

const StyledCardListEmptyTextContainer = styled.View`
  align-items: center;
  margin-top: ${scale(15)}px;
`;

const StyledCardListEmptyText = styled(Text).attrs((props) => ({
  mode: 'body1',
  customColor: props.theme.color.TEXT_200_MEDIUIM,
}))``;

const CardListEmptyComponent = () => (
  <View>
    <StyledDividerForEmptyComponent />
    <StyledCardListEmptyTextContainer>
      <StyledCardListEmptyText>There are no inactive</StyledCardListEmptyText>
      <StyledCardListEmptyText>CREDIT cards</StyledCardListEmptyText>
    </StyledCardListEmptyTextContainer>
  </View>
);

const ActivateCardList = ({navigation}: PropsType): React$Node => {
  const dispatch = useDispatch();
  const cardsForActivation = useSelector(getCardsForActivation);
  const {fetching} = useSelector(getCardActivationState);

  const handleOnRefresh = () => dispatch(refreshCardsForActivation());

  const renderItem = ({item}) => {
    return (
      <StyledListItemContainer>
        <StyledImageInfoContainer>
          <StyledCardImage />
          <StyledInfoContainer>
            <Text mode="body2">{item.embossingName}</Text>
            <Text mode="overline2">{maskingFormat(item.cardNo, true)}</Text>
          </StyledInfoContainer>
        </StyledImageInfoContainer>
        <StyledActivateButton
          onPress={() => navigation.navigate('ActivateCardConfirmation', item)}
        />
      </StyledListItemContainer>
    );
  };

  return (
    <Layout>
      <StyledMainContainer>
        <FlatList
          data={cardsForActivation}
          refreshing={fetching}
          onRefresh={handleOnRefresh}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={Divider}
          ListHeaderComponent={StyledCreditCardsHeader}
          ListEmptyComponent={CardListEmptyComponent}
        />
      </StyledMainContainer>
    </Layout>
  );
};

export default ActivateCardList;
