## ActivateCardList
ActivateCardList component.

# Usage
```js
import ActivateCardList from '@screens/private/ActivateCardList';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
