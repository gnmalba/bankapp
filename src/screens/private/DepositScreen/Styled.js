/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: column;
`;

export const DepositDivider: ComponentType<any> = styled.View`
  border: 1px solid #445870;
  margin-top: ${verticalScale(8)}px;
`;

export const MainText: ComponentType<any> = styled.View`
  margin-vertical: ${verticalScale(10)}px;
`;
