import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import Deposit from '../Deposit';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('Deposit', () => {
  const navigation = {
    navigate: jest.fn(() => {}),
  };

  const store = {
    deposit: {
      deposits: {
        savingsAndDeposits: [
          {
            id: 'TestId1',
            alias: 'TestAlias',
            availableBalanceValueWithCurrency: 5000,
            initialAccount: false,
            active: false,
          },
          {
            id: 'TestId2',
            alias: 'TestAlias',
            availableBalanceValueWithCurrency: 5000,
            initialAccount: false,
            active: false,
          },
        ],
        timeDeposits: [
          {
            id: 'TestId3',
            alias: 'TestAlias',
            availableBalanceValueWithCurrency: 5000,
            initialAccount: false,
            active: false,
          },
          {
            id: 'TestId4',
            alias: 'TestAlias',
            availableBalanceValueWithCurrency: 5000,
            initialAccount: false,
            active: false,
          },
        ],
      },
    },
    enrollment: {
      showSnackbar: true,
      snackbarMessage: 'test',
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <Deposit navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('AccountCard onPress should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <Deposit navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );

    const el = all.getByTestId(
      '1depositScreenAccountCardTestId1-0widgetItemNavigateButton',
    );
    fireEvent(el, 'onPress');

    expect(all.toJSON()).toMatchSnapshot();
  });
});
