/**
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import {useSelector} from 'react-redux';
import type {PropsType} from './types';
import * as S from './Styled';
import {Container, Divider} from '@components/Card/Styled';
import {ScrollContainer} from '@screens/private/DepositScreenOverview/Styled';

import Layout from '@components/Layout';
import EnrollButton from '@composition/EnrollButton';
import AccountCard from '@composition/AccountCard';
import EnrollSnackbar from '@composition/EnrollSnackbar';
import Text from '@components/Text';

import type {Deposits} from '@ducks/deposit/types';
import {depositsSelector} from '@ducks/deposit/selectors';
import {testProps} from '@utils/componentCustomProps';
import {maskingFormat} from '@utils/accountNumber';

const renderCard = (accounts, onCardClick) => {
  const lastAccount = accounts[accounts.length - 1];
  return accounts.map((account, index) => {
    const uniqueIndex = index + 1;
    return (
      <View key={`${account.id}-${index}`}>
        <AccountCard
          name={account.alias}
          accountNumber={maskingFormat(account.accountNo)}
          balanceLabel="AVAILABLE"
          balanceValue={account.availableBalanceValueWithCurrency}
          isPrimary={account.initialAccount}
          onClick={() => onCardClick(account.alias, account.id)}
          testID={`depositScreenAccountCard${account.id}-${index}`}
          uniqueIndex={uniqueIndex}
          isInactive={!account.active}
        />
        {lastAccount.id !== account.id && (
          <Divider {...testProps(uniqueIndex + 'PesoSavingsDivider')} />
        )}
      </View>
    );
  });
};

const Deposit = (props: PropsType): React$Node => {
  const {navigation} = props;
  const {savingsAndDeposits, timeDeposits}: Deposits = useSelector(
    depositsSelector,
  );
  const renderCards = (title, cards) => (
    <>
      <S.MainText>
        <Text mode="overline1" themeColor="TEXT_100_HIGH" {...testProps(title)}>
          {title}
        </Text>
      </S.MainText>
      <Container>
        {renderCard(cards, (alias: string, id: number) => {
          navigation.navigate('DepositScreenOverview', {
            alias,
            id,
          });
        })}
      </Container>
    </>
  );

  return (
    <Layout padded>
      <ScrollContainer>
        <S.MainContainer>
          {savingsAndDeposits.length > 0 &&
            renderCards('Checking & Savings', savingsAndDeposits)}
          {timeDeposits.length > 0 &&
            renderCards('Time Deposits', timeDeposits)}
        </S.MainContainer>
      </ScrollContainer>
      <EnrollButton
        testId="DepositEnrollAccountButtonId"
        onPress={() => navigation && navigation.navigate('Enroll Account')}
      />
      <EnrollSnackbar testId="DepositSuccessEnrollmentSnackbarId" />
    </Layout>
  );
};

export default Deposit;
