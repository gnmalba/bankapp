import React from 'react';
import {render} from '@testing-library/react-native';
import LoginSettings from '../LoginSettings';

import MockProvider from '@utils/testFramework';
const mockStore = (err) => ({
  authentication: {
    username: 'Test',
    password: 'Test',
    usernameError: 'Err',
    passwordError: 'Err',
    loading: err === true,
    data: {},
    encryptionPublicKey: 'asdfasdfa',
    accessToken: 'asdfasdfasdf',
  },
});

describe('LoginSettings', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore(true)}>
        <LoginSettings />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
