/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Layout from '@components/Layout';
import Button from '@components/Button';

import * as types from '@ducks/biometrics/actionTypes';

import type {PropsType} from './types';
import {} from './Styled';

const LoginSettings = (props: PropsType): React$Node => {
  const authentication = useSelector((state) => state.authentication);
  const {data, username, encryptionPublicKey, accessToken} = authentication;
  const {customerId} = data;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: types.BIOMETRICS_UPDATE_STATE,
      newState: {
        registerBiometrics: false,
      },
    });
  }, []);

  return (
    <Layout>
      <Button
        type="primary"
        size="large"
        state="default"
        title="Register"
        onPress={() => {
          dispatch({
            type: types.BIOMETRICS_START_REGISTER,
            payload: {
              username,
              customerId,
              passcode: '123456',
              encryptionPublicKey,
              accessToken,
            },
          });
        }}
      />
    </Layout>
  );
};

export default LoginSettings;
