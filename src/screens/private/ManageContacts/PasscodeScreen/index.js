/**
 * @flow
 */

import React, {useEffect, useState} from 'react';

// Compositions
import Layout from '@components/Layout';
import Loader from '@composition/Loader';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import Passcode from '@screens/common/Passcode';
import ErrorScreen from '@screens/common/ErrorScreen';

import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

// Redux
import {addContactRequest, clearApiResponse} from '@ducks/contacts/actions';
import {useDispatch, useSelector} from 'react-redux';

// Enums
import {INVALID_MPIN_CODE} from './enums';

export type PropsType = {
  navigation: Object,
  route: Object,
};

const PasscodeScreen = (props: PropsType): React$Node => {
  const {values} = props.route.params;
  const theme = useTheme() || DARK_THEME;
  const dispatch = useDispatch();
  const [passcode, setPasscode] = useState('');

  // Redux
  const apiResponse = useSelector((state) => state.contacts.apiResponse);

  useEffect(() => {
    // 200 Means the request is a success
    if (apiResponse?.data?.code === 200) {
      dispatch(clearApiResponse());
      props.navigation.navigate('SuccessScreen', {...apiResponse.data.data});
    }
  }, [apiResponse, dispatch, props]);

  const handleSubmitPasscode = () => {
    dispatch(
      addContactRequest({
        ...props.route.params,
        values: {...values, mpin: passcode},
      }),
    );
    setPasscode('');
  };

  const handleGoBack = () => {
    props.navigation.pop(2);
    dispatch(clearApiResponse());
  };

  useEffect(() => {
    if (apiResponse?.error === INVALID_MPIN_CODE.message && passcode) {
      dispatch(clearApiResponse());
    }
  }, [apiResponse.error, dispatch, passcode]);

  if (apiResponse?.loading) {
    return (
      <Layout>
        <Loader isVisible={true} />
      </Layout>
    );
  }

  if (apiResponse?.error && apiResponse.error !== INVALID_MPIN_CODE.message) {
    return (
      <ErrorScreen
        theme={theme}
        showBackButton={false}
        heading={'Something went wrong.'}
        details={apiResponse.error}
        buttonContents={
          <Button
            title="Go Back"
            type="primary"
            state="default"
            onPress={handleGoBack}
          />
        }
      />
    );
  }

  return (
    <Passcode
      password
      passcode={passcode}
      onTextChange={setPasscode}
      onSubmit={handleSubmitPasscode}
      title="Please enter your passcode."
      description="to proceed, please enter your 6-digit passcode."
      helperContent={
        <Text mode="body2">
          "If you forgot passcode, please call our hotline (02) 88-700-700 for
          further assistance."
        </Text>
      }
      passcodeError={apiResponse?.error}
      btnTitle="Continue"
    />
  );
};

export default PasscodeScreen;
