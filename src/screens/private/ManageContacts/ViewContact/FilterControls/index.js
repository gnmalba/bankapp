/**
 * @flow
 */

// REACT
import React from 'react';

// COMPONENTS
import {
  AmountAndDateRangeModal,
  SearchWithFilterModalToggle,
} from '@composition/FilterModals';

// TYPES
import type {ContactTransactionsFilterProps} from './types';

import {getFiltersCount} from '@composition/FilterModals/utils';

// CONTACT OVERVIEW SCREEN
const ViewContact = (props: ContactTransactionsFilterProps): React$Node => {
  const {
    filters,
    filterCount,
    searchTerm,
    showFilterModal,
    applyFilters,
    setFilterCount,
    setSearchTerm,
    toggleFilterModal,
  } = props;

  React.useEffect(() => {
    setFilterCount(getFiltersCount(filters));
  }, [filters, applyFilters, setFilterCount]);

  return (
    <>
      <SearchWithFilterModalToggle
        value={searchTerm}
        filterCount={filterCount}
        setSearchTerm={setSearchTerm}
        onPress={() => toggleFilterModal(true)}
      />
      <AmountAndDateRangeModal
        visible={showFilterModal}
        filters={filters}
        applyFilters={applyFilters}
        onCloseModal={() => toggleFilterModal(false)}
      />
    </>
  );
};

export default ViewContact;
