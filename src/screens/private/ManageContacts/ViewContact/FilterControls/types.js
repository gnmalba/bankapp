/**
 * @flow
 */

import type {AmountAndDateRangeFilters} from '@composition/FilterModals/AmountAndDateRangeModal/types';

export type ContactTransactionsFilterProps = {
  filters: AmountAndDateRangeFilters,
  filterCount: ?number,
  setFilterCount: any,
  searchTerm: string,
  showFilterModal: boolean,
  applyFilters: any,
  setSearchTerm: any,
  toggleFilterModal: any,
};
