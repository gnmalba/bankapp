/**
 * @flow
 */

import styled from 'styled-components/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Text from '@components/Text';

// import {moderateScale, scale} from '@utils/scale';

import type {ComponentType} from 'react';

/* HEADER SECTION */

export const ProfileBox: ComponentType<any> = styled.View`
  flex-direction: row;
  padding-horizontal: 60px;
  margin-top: 30px;
  margin-bottom: 40px;
  justify-content: center;
  align-items: center;
`;

export const NameBox: ComponentType<any> = styled.View`
  justify-content: center;
  margin-left: 24px;
`;

export const ContactName: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'h2',
  themeColor: 'TEXT_300_HIGH',
}))`
  margin-bottom: 8px;
`;

export const EditButton: ComponentType<any> = styled.TouchableOpacity`
  flex-direction: row;
  align-content: center;
`;

export const EditIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'edit',
    size: 16,
    color: props.theme?.color.SECONDARY_DEFAULT,
  }),
)`
  margin-right: 10px;
`;

export const EditLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'subtitle',
  themeColor: 'SECONDARY_DEFAULT',
}))``;
