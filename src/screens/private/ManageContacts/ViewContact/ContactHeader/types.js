/**
 * @flow
 */
import type {ContactItemProps} from '../types';

export type ContactHeaderProps = {
  contact: ContactItemProps,
  navigation: any,
};
