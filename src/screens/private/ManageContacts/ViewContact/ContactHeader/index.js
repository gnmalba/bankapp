/**
 * @flow
 */

import React from 'react';

// CONTACTS
import Avatar from '@components/Avatar';

// INTERNAL COMPONENTS
import {
  ProfileBox,
  NameBox,
  ContactName,
  EditButton,
  EditIcon,
  EditLabel,
} from './styled';

// FLOW TYPE ANNOTATIONS
import type {ContactHeaderProps} from './types';

const EditContact = (props) => {
  return (
    <EditButton {...props}>
      <EditIcon />
      <EditLabel>Edit Details</EditLabel>
    </EditButton>
  );
};

const handleOnPressEdit = (props) => {
  const {contact, navigation} = props;
  // upon navigate, contact data is on props.route.params.contact.data
  return navigation.navigate('Edit Contact', {
    data: contact.data,
  });
};

const ContactHeader = (props: ContactHeaderProps): React$Node => {
  const {title} = props.contact;
  return (
    <ProfileBox>
      <Avatar size="medium" title={title} />
      <NameBox>
        <ContactName>{title}</ContactName>
        {/* replace with edit btn if available */}
        <EditContact onPress={() => handleOnPressEdit(props)} />
      </NameBox>
    </ProfileBox>
  );
};

export default ContactHeader;
