/**
 * @flow
 */

export type DetailProps = {
  label: string,
  value: ?string,
  extraValue?: string,
};
