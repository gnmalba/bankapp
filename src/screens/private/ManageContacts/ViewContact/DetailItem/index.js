/**
 * @flow
 */

import React from 'react';

import {
  DetailMainContainer,
  DetailValueContainer,
  DetailLabel,
  DetailValue,
  DetailExtra,
} from './styled';

import type {DetailProps} from './types';

const renderExtraValue = (extraValue) =>
  (extraValue && <DetailExtra>{extraValue}</DetailExtra>) || <></>;

const DetailItem = (detail: DetailProps): React$Node => {
  const {label, value, extraValue} = detail;
  if (value) {
    return (
      <DetailMainContainer>
        <DetailLabel>{label}</DetailLabel>
        <DetailValueContainer>
          <DetailValue>{value}</DetailValue>
          {renderExtraValue(extraValue)}
        </DetailValueContainer>
      </DetailMainContainer>
    );
  }
  return <></>;
};

export default DetailItem;
