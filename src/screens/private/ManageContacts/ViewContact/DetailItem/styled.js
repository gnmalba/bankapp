/**
 * @flow
 */

import styled from 'styled-components/native';

import Text from '@components/Text';

import {moderateScale} from '@utils/scale';

import type {ComponentType} from 'react';

/* DETAILS SECTION */

export const DetailMainContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: flex-start;
  margin-bottom: 16px;
  width: 100%;
`;

export const DetailValueContainer: ComponentType<any> = styled.View`
  padding-left: 16px;
`;

export const DetailLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  numberOfLines: 1,
  mode: 'overline1',
  themeColor: 'TEXT_100_HIGH',
}))`
  width: 40%;
`;

export const DetailValue: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'body1',
  themeColor: 'TEXT_300_HIGH',
}))`
  line-height: ${moderateScale(18)}px;
`;

export const DetailExtra: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'overline1',
  themeColor: 'TEXT_100_HIGH',
}))``;
