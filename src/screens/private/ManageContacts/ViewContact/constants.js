export const CONTACT_ACTION_SHORTCUTS = [
  {name: 'Send Money', icon: 'SEND_MONEY'},
  /**
   * The following are for future releases:
   * {name: 'Send Load', icon: 'Send Load'},
   * {name: 'Send Request', icon: 'Send Request'},
   */
];

export const TRANSACTIONS_DEFAULT_FILTERS = {
  dateFrom: null,
  dateTo: null,
  amountFrom: null,
  amountTo: null,
};
