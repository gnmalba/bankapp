/**
 * @flow
 */

import type {
  ContactDataFromApi,
  ContactTransactionItem,
} from '@ducks/contacts/types';
import type {AmountAndDateRangeFilters} from '@composition/FilterModals/AmountAndDateRangeModal/types';

export type ContactTransactionListProps = {
  contactData: ContactDataFromApi,
  filters: AmountAndDateRangeFilters,
  searchTerm: string,
};

export type ContactTransactionItemProps = {
  item: $Shape<ContactTransactionItem>,
  index: number,
};
