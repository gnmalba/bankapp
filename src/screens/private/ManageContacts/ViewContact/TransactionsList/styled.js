/**
 * @flow
 */

import styled from 'styled-components/native';

import Text from '@components/Text';
import {Records, RecordItem} from '@components/Records';

import type {ComponentType} from 'react';

import {verticalScale} from '@utils/scale';
import {SCREEN_WIDTH} from '@utils/constants';

export const HalfSection: ComponentType<any> = styled.View`
  flex: ${(props) => props.flex || 1};
  padding-left: ${(props) => props.paddingLeft || 0};
`;

export const Name: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'body1',
  themeColor: 'TEXT_300_HIGH',
}))``;

export const Subtitle: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'body2',
  themeColor: 'TEXT_100_HIGH',
}))``;

export const Value: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'body1',
  themeColor: props.status === 'Sent Money' ? 'RED_STATUS' : 'GREEN_STATUS',
}))``;

export const EmptyListContainer: ComponentType<any> = styled.View`
  flex-grow: 1;
  margin-top: ${verticalScale(24)}px;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export const EmptyListLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'h3',
    themeColor: 'TEXT_300_HIGH',
  }),
)``;

export const EmptyListMessage: ComponentType<any> = styled(Subtitle)`
  margin-top: ${verticalScale(16)}px;
  margin-bottom: ${verticalScale(38)}px;
  text-align: center;
  padding-horizontal: 20%;
`;

export const TransactionsRecords: ComponentType<any> = styled(Records)`
  margin-left: ${-24}px;
  width: ${SCREEN_WIDTH}px;
`;

export const TransactionItem: ComponentType<any> = styled(RecordItem)``;

export const Spacer: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(24)}px;
`;
