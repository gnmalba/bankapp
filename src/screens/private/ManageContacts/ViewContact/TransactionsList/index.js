/**
 * @flow
 */

// REACT
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {ActivityIndicator} from 'react-native';
// COMPONENTS
// import {Records} from '@components/Records';
import {
  TransactionItem,
  TransactionsRecords,
  HalfSection,
  Name,
  Subtitle,
  Value,
  EmptyListContainer,
  EmptyListLabel,
  EmptyListMessage,
  Spacer,
} from './styled';

// ACTIONS
import {getContactTransactionsRequest} from '@ducks/contacts/actions';

// TYPES
import type {
  ContactTransactionListProps,
  ContactTransactionItemProps,
} from './types';

// UTILS
import {getFetchParams, isEmptyString} from './utils';

// RENDER HELPER
const renderRecordItem = ({item, index}: ContactTransactionItemProps) => {
  const leftHalfFlexValue = 2;
  const leftHalfLeftPadding = 24;

  return (
    <TransactionItem index={index}>
      <HalfSection
        customFlex={leftHalfFlexValue}
        paddingLeft={leftHalfLeftPadding}>
        <Name>{item.name || ''}</Name>
        <Subtitle>{item.date || ''}</Subtitle>
      </HalfSection>
      <HalfSection>
        <Value status={item.transactionType}>{item.value || ''}</Value>
        <Subtitle>{item.label || ''}</Subtitle>
      </HalfSection>
    </TransactionItem>
  );
};

const ListEmpty = (props) => {
  const {contactTransactions, searchTerm} = props;
  const emptyMessage =
    isEmptyString(searchTerm) && contactTransactions.length < 1
      ? 'There are no results that match you filter(s).'
      : '';
  return (
    <EmptyListContainer>
      <EmptyListLabel>No results found.</EmptyListLabel>
      <EmptyListMessage>{emptyMessage}</EmptyListMessage>
    </EmptyListContainer>
  );
};

const TransactionList = (props: ContactTransactionListProps): React$Node => {
  const {contactData, filters, searchTerm} = props;

  const [page, setPage] = useState(0);
  const dispatch = useDispatch();
  const isLoadingTXN = useSelector((state) => state.contacts.isLoadingTXN);

  const contactTransactions =
    useSelector((state) => {
      return state.contacts.contactsTransactions.filter(
        (transaction) => transaction.contactId === contactData.contactId,
      );
    })[0]?.transactions || [];

  const handleSeeMoreItems = () => {
    setPage(page + 1);
  };

  // will run fetch on first load and when there's change in filters and page number
  useEffect(() => {
    dispatch(
      getContactTransactionsRequest({
        contactId: contactData.contactId,
        page: page,
        ...getFetchParams({filters, searchTerm}),
      }),
    );
  }, [dispatch, page, filters, searchTerm, contactData.contactId]);

  return (
    <>
      {isLoadingTXN ? (
        <ActivityIndicator size="large" />
      ) : (
        <TransactionsRecords
          refreshing={isLoadingTXN}
          data={contactTransactions}
          renderItem={renderRecordItem}
          onPressSeeMore={handleSeeMoreItems}
          ListEmptyComponent={
            <ListEmpty {...props} contactTransactions={contactTransactions} />
          }
        />
      )}
      <Spacer />
    </>
  );
};

export default TransactionList;
