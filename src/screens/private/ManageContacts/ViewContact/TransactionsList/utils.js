/**
 * @flow
 */
import {formatDate} from '@utils/date';

const DATE_FORMAT_FOR_API = 'MM-DD-YYYY';
export const isEmptyString = (value: string): boolean => {
  return !value || value === undefined || value === '' || value.length === 0;
};
export const removeEmptyObjectProps = (object: Object): Object => {
  return Object.fromEntries(
    Object.entries(object).filter(([_, v]) => v !== null),
  );
};

export const formatDateForFetchParams = (filters: Object): Object => {
  if (filters && filters?.dateFrom?._d && filters?.dateTo?._d) {
    return {
      ...filters,
      dateFrom: formatDate(filters.dateFrom._d, DATE_FORMAT_FOR_API),
      dateTo: formatDate(filters.dateTo._d, DATE_FORMAT_FOR_API),
    };
  }
  return filters;
};

export const getFetchParams = ({searchTerm, filters}: Object): Object => {
  const combinedFetchParams = {
    ...formatDateForFetchParams(filters),
    searchKeywords: searchTerm,
  };

  return removeEmptyObjectProps(combinedFetchParams);
};
