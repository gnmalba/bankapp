/**
 * @flow
 */

import styled from 'styled-components/native';

import Text from '@components/Text';

import {scale} from '@utils/scale';

import type {ComponentType} from 'react';

/* MAIN */

export const Container: any = styled.View`
  display: flex;
  flex: 1;
`;

export const ContactScrollView: ComponentType<any> = styled.ScrollView.attrs(
  (props) => ({
    ...props,
  }),
)`
  padding: 24px;
  padding-bottom: 54px;
`;

export const SectionLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'h3',
  themeColor: 'TEXT_300_HIGH',
}))``;

export const Divider: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.dividerBgColor};
  height: ${scale(1)}px;
  margin: 16px 0;
`;

/* ACTIONS SECTION */

export const ActionBox: ComponentType<any> = styled.View`
  flex-direction: row;
  margin-bottom: 24px;
`;
