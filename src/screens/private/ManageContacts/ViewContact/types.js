/**
 * @flow
 */

import type {ContactItem} from '@ducks/contacts/types';
import type {AmountAndDateRangeFilters} from '@composition/FilterModals/AmountAndDateRangeModal/types';

export type ContactItemProps = $Shape<ContactItem>;

export type ContactOverviewProps = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: any,
  route: {
    params: {
      contact: ContactItem,
    },
  },
};

export type ContactDetailProps = {
  label: string,
  value: ?string,
};

export type ContactHeaderProps = {
  contact: ContactItem,
  navigation: any,
};

export type ContactTransactionsFilterProps = {
  filters: AmountAndDateRangeFilters,
  filterCount: ?number,
  setFilterCount: any,
  searchTerm: string,
  showFilterModal: boolean,
  applyFilters: any,
  setSearchTerm: any,
  toggleFilterModal: any,
};
