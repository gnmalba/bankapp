/**
 * @flow
 */

// REACT
import React from 'react';

import {pick} from 'lodash';

// COMPONENTS
import Layout from '@components/Layout';
import ShortcutIconButton from '@composition/ShortcutIconButton';
import FilterControls from './FilterControls';
import TransactionsList from './TransactionsList';

// INTERNAL COMPONENTS
import {
  ActionBox,
  Container,
  ContactScrollView,
  SectionLabel,
  Divider,
} from './Styled';

import DetailItem from './DetailItem';
import ContactHeader from './ContactHeader';

// UTILS
import {SHORTCUT_ICONS_CODE} from '@utils/constants';
// import {getFiltersCount} from '@composition/FilterModals/utils';

// TYPES
import type {ContactOverviewProps, ContactDetailProps} from './types';

// CONSTANTS
import {
  CONTACT_ACTION_SHORTCUTS,
  TRANSACTIONS_DEFAULT_FILTERS,
} from './constants';

// RENDER HELPERS
const renderDetails = (details: Array<ContactDetailProps>): React$Node => {
  return details.map((detail, index) => {
    return (
      <DetailItem
        key={`${index}: ${detail.label}`}
        label={detail.label}
        value={detail.value}
      />
    );
  });
};

const renderContactActions = (): React$Node => {
  return CONTACT_ACTION_SHORTCUTS.map((action, index) => {
    return (
      <ShortcutIconButton
        key={`${index}: ${action.name}`}
        name={action.name}
        icon={SHORTCUT_ICONS_CODE[action.icon]}
        onClick={() => handleOnPressContactAction(action.name)}
      />
    );
  });
};

// HANDLE THE SCENARIO ON PRESS ACTION
const handleOnPressContactAction = (actionName: string) => {
  // TODO: for send money team, update this for your usage
};

// DECONSTRUCTED SCREEN PARTS
const ActionsSection = (): React$Node => {
  return (
    <>
      <SectionLabel>Actions</SectionLabel>
      <Divider />
      <ActionBox>{renderContactActions()}</ActionBox>
    </>
  );
};

const ContactDetailsSection = ({details}): React$Node => {
  return (
    <>
      <SectionLabel>Contact Details</SectionLabel>
      <Divider />
      {renderDetails(details)}
    </>
  );
};

const TransactionHistorySection = (props) => {
  return (
    <>
      <SectionLabel>Transaction History</SectionLabel>
      <Divider />
      <FilterControls {...props} />
      <TransactionsList
        {...pick(props, ['contactData', 'searchTerm', 'filters'])}
      />
    </>
  );
};

// CONTACT OVERVIEW SCREEN
const ViewContact = (props: ContactOverviewProps): React$Node => {
  const {contact} = props.route.params;
  const {data, overview} = contact;

  const [filters, applyFilters] = React.useState(TRANSACTIONS_DEFAULT_FILTERS);
  const [filterCount, setFilterCount] = React.useState(0);
  const [searchTerm, setSearchTerm] = React.useState('');
  const [showFilterModal, toggleFilterModal] = React.useState(false);

  return (
    <Layout>
      <Container>
        <ContactScrollView>
          <ContactHeader contact={contact} navigation={props.navigation} />
          <ActionsSection />
          <ContactDetailsSection details={overview} />
          <TransactionHistorySection
            showFilterModal={showFilterModal}
            searchTerm={searchTerm}
            visible={showFilterModal}
            filters={filters}
            filterCount={filterCount}
            applyFilters={applyFilters}
            setSearchTerm={setSearchTerm}
            setFilterCount={setFilterCount}
            toggleFilterModal={toggleFilterModal}
            contactData={data}
          />
        </ContactScrollView>
      </Container>
    </Layout>
  );
};

export default ViewContact;
