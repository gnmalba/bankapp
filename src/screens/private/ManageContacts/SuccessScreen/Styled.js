/**
 * @flow
 */

import type {ComponentType} from 'react';
import {ScrollView} from 'react-native';

import styled from 'styled-components/native';
import {verticalScale, scale} from '@utils/scale';

import Text from '@components/Text';
import Button from '@components/Button';

export const HeaderText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
}))`
  padding-bottom: ${verticalScale(16)}px;
`;

export const SubHeaderText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body2',
    themeColor: 'TEXT_100_HIGH',
  }),
)`
  padding-bottom: ${verticalScale(16)}px;
  text-align: center;
`;

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  align-items: center;
  padding-vertical: ${verticalScale(15)}px;
  padding-horizontal: ${scale(32)}px;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  padding-vertical: ${verticalScale(24)}px;
  padding-horizontal: ${scale(16)}px;
  width: 100%;
  align-items: center;
`;

export const Divider: ComponentType<any> = styled.View`
  width: 100%;
  border-bottom-width: 1px;
  border-bottom-color: ${(props) => props.theme.color.TEXT_300_LOW};
  margin-vertical: ${verticalScale(16)}px;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(24)}px;
  width: 100%;
`;

export const SendMoneyButton: ComponentType<any> = styled(Button).attrs(
  (props) => ({
    title: 'Send Money',
    type: 'primary',
    titleCustomColor: '#FFFFFF',
    state: 'default',
    ...props,
  }),
)``;

export const CancelButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;
export const DashboardButton: ComponentType<any> = styled(Button).attrs(
  (props) => ({
    title: 'Go To Dashboard',
    type: 'secondary',
    titleCustomColor: '#FFFFFF',
    state: 'default',
    ...props,
  }),
)`
  align-items: center;
`;

export const CloseIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/close.png'),
}))`
  align-self: flex-end;
  height: ${scale(30)}px;
  width: ${scale(30)}px;
  padding: ${verticalScale(16)}px;
  margin-top: ${verticalScale(12)}px;
  margin-horizontal: ${scale(12)}px;
  tint-color: ${(props) => props.theme.topAppBar.iconColor};
`;

export const ScrollContainer: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    showsVerticalScrollIndicator: false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
  }),
)``;
