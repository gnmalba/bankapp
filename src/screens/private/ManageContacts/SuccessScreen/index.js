/**
 * @flow
 */

import React from 'react';
import {Pressable} from 'react-native';

// Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';
import moment from 'moment';

// Compositions
import Layout from '@components/Layout';
import SuccessItem from '@composition/ManageContacts/SuccessItem';

// Components
import Text from '@components/Text';
import Circle from '@components/Circle';

// Styled Components
import {
  HeaderText,
  SubHeaderText,
  Container,
  DetailsContainer,
  Divider,
  ButtonContainer,
  SendMoneyButton,
  DashboardButton,
  CancelButtonContainer,
  CloseIcon,
  ScrollContainer,
} from './Styled';

import {useNavigation} from '@react-navigation/native';
import Header from '@components/Header';

export type PropsType = {
  navigation: Object,
  route: Object,
};

const CloseButton = ({navigation}) => {
  return (
    <Pressable onPress={() => navigation.navigate('Contacts')}>
      <CloseIcon />
    </Pressable>
  );
};

const SuccessScreen = (props: PropsType): React$Node => {
  const navigation = useNavigation();
  const theme = useTheme() || DARK_THEME;

  const {
    bankName,
    accountNumber,
    firstName,
    lastName,
    businessName,
    mobileNumber,
    emailAddress,
    alias,
    createDate,
  } = props.route.params;

  const getTimeCreated = () => {
    const date = createDate;
    const formattedDate = moment(date).format('MMM-DD-YYYY hh:mm A');

    return `${formattedDate} (GMT+8)`;
  };

  return (
    <Layout>
      <Header
        isTransparent
        title=" "
        rightComponent={<CloseButton {...props} />}
      />
      <ScrollContainer>
        <Container>
          <HeaderText>Contact successfully added!</HeaderText>
          <SubHeaderText>
            You can now quickly set this contact as your recipient when doing
            transactions.
          </SubHeaderText>

          <DetailsContainer theme={theme}>
            <Circle size={64}>
              <Text mode="h1">B</Text>
            </Circle>

            {bankName ? (
              <>
                <SuccessItem label="bank" value={bankName} />
                <SuccessItem label="account no." value={accountNumber} />

                <Divider theme={theme} />
              </>
            ) : (
              <SuccessItem label="mobile no." value={mobileNumber} />
            )}

            {bankName && businessName ? (
              <SuccessItem label="business name" value={businessName} />
            ) : (
              <>
                <SuccessItem label="first name" value={firstName} />
                <SuccessItem label="last name" value={lastName} />
              </>
            )}

            {bankName && mobileNumber && (
              <SuccessItem label="mobile no." value={mobileNumber} />
            )}

            <SuccessItem label="email address" value={emailAddress} />

            <SuccessItem label="contact alias" value={alias} />
            <Divider theme={theme} />
            <SuccessItem label="date and time" value={getTimeCreated()} />
          </DetailsContainer>

          <ButtonContainer>
            <SendMoneyButton onPress={() => console.log('Send')} />
            <CancelButtonContainer>
              <DashboardButton
                onPress={() => navigation.navigate('Dashboard')}
              />
            </CancelButtonContainer>
          </ButtonContainer>
        </Container>
      </ScrollContainer>
    </Layout>
  );
};

export default SuccessScreen;
