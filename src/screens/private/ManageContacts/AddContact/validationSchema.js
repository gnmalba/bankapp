import {ORGANIZATION_TYPE} from '@composition/ManageContacts/AccountType/constants';
import {BANK_TYPE} from '@composition/ManageContacts/ContactType/constants';
import * as yup from 'yup';
import {ERRORS} from './constants';

export const initialValues = {
  accountNo: null,
  alias: null,
  firstName: null,
  lastName: null,
  emailAddress: null,
  bank: null,
  mobileNumber: null,
  countryCode: {phoneCode: '+63', image: 'PH'},
  businessName: null,
};

export const validationSchema = (types) => {
  const {contactType, accountType} = types;
  const schema = {
    alias: yup
      .string()
      .required(ERRORS.ALIAS.REQUIRED)
      .nullable()
      .test('has-emoji', ERRORS.ALIAS.CORRECT_FORMAT, (value) => {
        const regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/gi; // NOSONAR
        return value && !value.match(regex);
      }),
    emailAddress: yup
      .string()
      .email('Please enter a valid email address')
      .nullable(),
    mobileNumber: yup
      .string()
      .nullable()
      .when('is-mobile-number-type', {
        is: () => contactType !== BANK_TYPE.value,
        then: yup.string().nullable().required(ERRORS.MOBILE.REQUIRED),
        otherwise: yup.string().nullable(),
      })
      .test('is-only-numeric', ERRORS.MOBILE.NUMERIC_FORMAT, (value) => {
        const regex = /^[0-9]*$/.test(value);
        return (value && regex) || true;
      })
      .length(10, ERRORS.MOBILE.CORRECT_FORMAT),
  };

  if (contactType === BANK_TYPE.value) {
    schema.accountNo = yup
      .string()
      .required(ERRORS.ACCOUNT_NUMBER.REQUIRED)
      .nullable()
      .min(13, ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT)
      .max(16, ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT)
      .test(
        'is-valid-credit-card',
        ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT,
        (value) => {
          if (value?.length === 16) {
            return true;
          } else {
            if (value?.length === 13) {
              return true;
            }
          }
        },
      );
  }
  if (
    contactType === BANK_TYPE.value &&
    accountType === ORGANIZATION_TYPE.value
  ) {
    schema.businessName = yup
      .string()
      .required(ERRORS.BUSINESS_NAME.REQUIRED)
      .nullable();
  } else {
    schema.firstName = yup
      .string()
      .required(ERRORS.FIRST_NAME.REQUIRED)
      .nullable();
    schema.lastName = yup
      .string()
      .required(ERRORS.LAST_NAME.REQUIRED)
      .nullable();
  }

  return yup.object().shape(schema);
};
