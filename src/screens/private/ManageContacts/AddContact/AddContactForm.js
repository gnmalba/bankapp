// React modules
import React, {useEffect, useState} from 'react';
import {Platform} from 'react-native';

// Third party libraries
import {useFormik} from 'formik';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

// Compositions
import AccountNumber from '@composition/ManageContacts/AccountNumber';
import Alias from '@composition/ManageContacts/Alias';
import FirstName from '@composition/ManageContacts/FirstName';
import LastName from '@composition/ManageContacts/LastName';
import BusinessName from '@composition/ManageContacts/BusinessName';
import EmailAddress from '@composition/ManageContacts/EmailAddress';
import SelectInput from '@composition/ManageContacts/SelectInput';
import MobileNumberInput from '@composition/ManageContacts/MobileNumberInput';
import ContactType from '@composition/ManageContacts/ContactType';
import AccountType from '@composition/ManageContacts/AccountType';
import EnrollButton from '@composition/ManageContacts/EnrollButton';
import CancelButton from '@composition/ManageContacts/CancelButton';

// Constaants
import {BANK_TYPE} from '@composition/ManageContacts/ContactType/constants';
import {
  INDIVIDUAL_TYPE,
  ORGANIZATION_TYPE,
} from '@composition/ManageContacts/AccountType/constants';

// Utils
import {initialValues, validationSchema} from './validationSchema';

// Types
import type {PropsType} from './types';

//  Styled Components
import {ButtonContainer} from './Styled';

// Redux
import {getBankList} from '@ducks/contacts/actions';
import {useDispatch, useSelector} from 'react-redux';

const countryList = [
  {phoneCode: '+63', country: 'PH'},
  {phoneCode: '+1', country: 'NA'},
  {phoneCode: '+22', country: 'EU'},
];

const AddContactForm = (props: PropsType): React$Node => {
  const {navigation, initialValuesSendMoney = null} = props;

  const [contactType, setContactType] = useState(BANK_TYPE.value);
  const [accountType, setAccountType] = useState(INDIVIDUAL_TYPE.value);

  // Redux Implementation
  const dispatch = useDispatch();
  const bankList = useSelector((state) => state.contacts.bankList);

  const formik = useFormik({
    initialValues: initialValuesSendMoney
      ? initialValuesSendMoney
      : initialValues,
    enableReinitialize: true,
    validateOnMount: false,
    validateOnBlur: true,
    validationSchema: validationSchema({contactType, accountType}),
  });

  const {
    values,
    errors,
    touched,
    isValid,
    setFieldValue,
    // setFieldError,
    setFieldTouched,
    // handleSubmit,
    // resetForm,
    validateForm,
  } = formik;

  useEffect(() => {
    validateForm();
    getBankList();
  }, [contactType, accountType, validateForm]);

  useEffect(() => {
    dispatch(getBankList());
  }, [dispatch]);

  const removeSpace = (value) => {
    return value.replace(/\s+/g, '');
  };

  const handleInputChange = (name) => (value) => {
    if (name === 'accountNo') {
      setFieldValue(name, removeSpace(value));
    } else {
      setFieldValue(name, value);
    }
  };

  const handleResetField = (name) => {
    setFieldValue(name, initialValues[name]);
  };

  if (bankList.loading) {
    return null;
  }

  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps={'handled'}
      showsVerticalScrollIndicator={false}
      enableOnAndroid
      extraHeight={Platform.select({
        ios: 170,
        android: 90,
      })}>
      <ContactType value={contactType} setContactType={setContactType} />
      {contactType === BANK_TYPE.value && (
        <AccountType value={accountType} setAccountType={setAccountType} />
      )}
      {contactType === BANK_TYPE.value ? (
        <>
          <SelectInput
            label="Bank"
            placeholder="Select your contact's bank"
            options={bankList.data || []}
            selectedOption={values?.bank?.name}
            setSelectedOption={setFieldValue}
          />
          <AccountNumber
            value={values?.accountNo}
            handleInputChange={handleInputChange}
            handleResetField={handleResetField}
            handleSetFieldTouched={setFieldTouched}
            error={errors?.accountNo}
            isTouched={touched?.accountNo}
          />
        </>
      ) : (
        <MobileNumberInput
          label="Mobile Number"
          placeholder="10-digit mobile number"
          options={countryList}
          countryCode={values?.countryCode}
          value={values?.mobileNumber}
          handleResetField={handleResetField}
          handleSetFieldTouched={setFieldTouched}
          error={errors?.mobileNumber}
          isTouched={touched?.mobileNumber}
          setFieldValue={setFieldValue}
        />
      )}

      {accountType === ORGANIZATION_TYPE.value &&
      contactType === BANK_TYPE.value ? (
        <BusinessName
          value={values?.businessName}
          handleInputChange={handleInputChange}
          handleResetField={handleResetField}
          handleSetFieldTouched={setFieldTouched}
          error={errors?.businessName}
          isTouched={touched?.businessName}
        />
      ) : (
        <>
          <FirstName
            value={values?.firstName}
            handleInputChange={handleInputChange}
            handleResetField={handleResetField}
            handleSetFieldTouched={setFieldTouched}
            error={errors?.firstName}
            isTouched={touched?.firstName}
          />
          <LastName
            value={values?.lastName}
            handleInputChange={handleInputChange}
            handleResetField={handleResetField}
            handleSetFieldTouched={setFieldTouched}
            error={errors?.lastName}
            isTouched={touched?.lastName}
          />
        </>
      )}

      {contactType === BANK_TYPE.value && (
        <MobileNumberInput
          label="Mobile Number"
          placeholder="10-digit mobile number"
          options={countryList}
          countryCode={values?.countryCode}
          value={values?.mobileNumber}
          handleResetField={handleResetField}
          handleSetFieldTouched={setFieldTouched}
          error={errors?.mobileNumber}
          isTouched={touched?.mobileNumber}
          setFieldValue={setFieldValue}
        />
      )}
      <EmailAddress
        value={values?.emailAddress}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={setFieldTouched}
        error={errors?.emailAddress}
        isTouched={touched?.emailAddress}
      />
      <Alias
        value={values?.alias}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={setFieldTouched}
        error={errors?.alias}
        isTouched={touched?.alias}
      />
      <ButtonContainer>
        <EnrollButton
          handleSubmit={() =>
            navigation.navigate('ConfirmDetails', {
              values,
              contactType,
              accountType,
            })
          }
          isValid={isValid}
        />
        <CancelButton handleCancel={() => navigation.goBack()} />
      </ButtonContainer>
    </KeyboardAwareScrollView>
  );
};

export default AddContactForm;
