/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

// Compositions
import Layout from '@components/Layout';

export const Container: any = styled.View`
  background-color: ${(props) => props.theme.layout.backgroundColor};
  display: flex;
  flex: 1;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(40)}px;
`;

export const StyledLayout: ComponentType<any> = styled(Layout)`
  padding-horizontal: 40px;
`;
