/**
 * @flow
 */

import React from 'react';

// Compositions
import FormLayout from '@composition/FormLayout';

// Components
import AddContactForm from './AddContactForm';

export type PropsType = {
  navigation: Object,
  route: Object,
};

const AddContact = (props: PropsType): React$Node => {
  const sendMoneyNewBillerAccount =
    props.route?.params?.sendMoneyNewBillerAccount;

  return (
    <FormLayout isFullScreen>
      <AddContactForm
        navigation={props.navigation}
        initialValuesSendMoney={sendMoneyNewBillerAccount}
      />
    </FormLayout>
  );
};

export default AddContact;
