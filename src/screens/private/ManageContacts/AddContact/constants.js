export const ERRORS = {
  ACCOUNT_NUMBER: {
    REQUIRED: 'Please enter your 13 or 16-digit account number.',
    CORRECT_FORMAT: 'Invalid input. Enter the number in the correct format.',
    INVALID_ACCOUNT_NUMBER: 'Please enter a valid Metrobank account number.',
    INVALID_ACCOUNT_NUMBER_NON_MB: 'Please enter a valid account number.',
    INVALID_SAVINGS_CHECKING_NUMBER:
      'Invalid input. Please enter a valid Metrobank savings or checking account number.',
  },
  ALIAS: {
    REQUIRED: 'Create an alias for for your contact',
    CORRECT_FORMAT: 'Invalid input. Enter the alias in the correct format.',
  },
  MOBILE: {
    REQUIRED: 'Please enter your mobile number',
    CORRECT_FORMAT:
      'Invalid input. Enter the mobile number in the correct format.',
    NUMERIC_FORMAT: 'Invalid input. Mobile number only accepts numeric values',
  },
  FIRST_NAME: {
    REQUIRED: 'Please enter your first name',
  },
  LAST_NAME: {
    REQUIRED: 'Please enter your last name',
  },
  BUSINESS_NAME: {
    REQUIRED: 'Please enter your business name',
  },
};
