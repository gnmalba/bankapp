/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

import Text from '@components/Text';

export const Container: any = styled.View`
  background-color: ${(props) => props.theme.layout.backgroundColor};
  display: flex;
  flex: 1;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(40)}px;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  flex: 1;
`;

export const HeaderText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h2',
}))`
  font-family: Myriad Pro;
  margin-bottom: ${verticalScale(8)}px;
`;

export const SubHeaderText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body2',
  }),
)`
  font-family: Myriad Pro;
  margin-bottom: ${verticalScale(40)}px;
`;
