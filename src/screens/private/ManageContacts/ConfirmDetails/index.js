/**
 * @flow
 */

import React from 'react';

// Compositions
import Layout from '@components/Layout';
import ConfirmItem from '@composition/ManageContacts/ConfirmItem';

// Components
import {HeaderText, SubHeaderText, DetailsContainer} from './Styled';
import Button from '@components/Button';

import {BANK_TYPE} from '@composition/ManageContacts/ContactType/constants';
import {ORGANIZATION_TYPE} from '@composition/ManageContacts/AccountType/constants';

export type PropsType = {
  navigation: Object,
  route: Object,
};

const ConfirmDetails = (props: PropsType): React$Node => {
  const {values, contactType, accountType} = props.route.params;

  const handleConfirm = () => {
    props.navigation.navigate('PasscodeScreen', props.route.params);
  };

  return (
    <Layout padding={32} plain={false}>
      <DetailsContainer>
        <HeaderText>All Good?</HeaderText>
        <SubHeaderText themeColor="TEXT_100_HIGH">
          Please review your details.
        </SubHeaderText>

        {contactType === BANK_TYPE.value ? (
          <>
            <ConfirmItem label="bank" value={values.bank?.name} />
            <ConfirmItem label="account no." value={values.accountNo} />
          </>
        ) : (
          <ConfirmItem
            label="mobile no."
            value={`${values.countryCode?.phoneCode} ${values.mobileNumber}`}
          />
        )}

        {contactType === BANK_TYPE.value &&
        accountType === ORGANIZATION_TYPE.value ? (
          <ConfirmItem label="business name" value={values.businessName} />
        ) : (
          <>
            <ConfirmItem label="first name" value={values.firstName} />
            <ConfirmItem label="last name" value={values.lastName} />
          </>
        )}
        {contactType === BANK_TYPE.value && values.mobileNumber && (
          <ConfirmItem
            label="mobile no."
            value={`${values.countryCode?.phoneCode} ${values.mobileNumber}`}
          />
        )}
        <ConfirmItem label="email address" value={values.emailAddress} />
        <ConfirmItem label="contact alias" value={values.alias} />
      </DetailsContainer>

      <Button
        type="primary"
        state="default"
        title="Confirm"
        onPress={handleConfirm}
      />
    </Layout>
  );
};

export default ConfirmDetails;
