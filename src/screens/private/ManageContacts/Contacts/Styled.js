/**
 * @flow
 */
import {FlatList} from 'react-native';

import styled from 'styled-components/native';

import Text from '@components/Text';

import Layout from '@components/Layout';

import type {ComponentType} from 'react';
import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  padding-horizontal: ${verticalScale(24)}px;
  padding-top: ${verticalScale(24)}px;
`;

export const Spacer: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(24)}px;
`;

export const EmptyListContainer: ComponentType<any> = styled.View`
  flex-grow: 1;
  margin-top: ${verticalScale(-24)}px;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export const ListLoaderContainer: ComponentType<any> = styled.View`
  margin: ${verticalScale(24)}px;
`;

export const ContactsList: ComponentType<any> = styled(FlatList).attrs(
  (props) => ({
    contentContainerStyle: {
      flexGrow: 1,
      backgroundColor: 'transparent',
    },
  }),
)`
  width: 100%;
`;

export const ContactsLayout: ComponentType<any> = styled(Layout).attrs(
  (props) => ({
    padding: verticalScale(24),
  }),
)`
  padding-bottom: 0px;
`;

export const EmptyListLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'h3',
    themeColor: 'TEXT_300_HIGH',
  }),
)``;

export const EmptyListMessage: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'body2',
    themeColor: 'TEXT_100_HIGH',
  }),
)`
  margin-top: ${verticalScale(16)}px;
  text-align: center;
  padding-horizontal: 20%;
`;
