/**
 * @flow
 */

import React, {useEffect, useState} from 'react';

import {useSelector, useDispatch} from 'react-redux';

import Layout from '@components/Layout';
import EntityInfoCard from '@composition/EntityInfoCard';
import EntityInfoPlaceholder from '@composition/EntityInfoCard/EntityInfoPlaceholder';
import EnrollButton from '@composition/EnrollButton';
import SearchBar from '@composition/SearchBar';
import Snackbar from '@components/Snackbar';
import {
  Container,
  ContactsList,
  EmptyListContainer,
  EmptyListLabel,
  EmptyListMessage,
  Spacer,
} from './Styled';

import {isEmpty, filterContacts} from './utils';

import type {ContactItem} from '@ducks/contacts/types';

const ITEM_PLACEHOLDER = [1, 2, 3];
export type PropsType = {
  navigation: Object,
  route: {
    params: {
      refetch?: Boolean,
    },
  },
};

type RenderItemProps = {
  item: ContactItem,
  index: number,
};

const ListEmpty = (props) => {
  const {contactsList, searchTerm} = props;
  const emptyMessage =
    isEmpty(searchTerm) && contactsList.length < 1
      ? 'there are no contacts enrolled on your account. Try enrolling a new contact'
      : '';
  return (
    <EmptyListContainer>
      {/* tech debt: create a empty list placeholder */}
      <EmptyListLabel>No contacts found</EmptyListLabel>
      <EmptyListMessage>{emptyMessage}</EmptyListMessage>
    </EmptyListContainer>
  );
};

const renderListItem = (item, index, props) => {
  const {isLoading, navigation} = props;

  if (isLoading) {
    return (
      <>
        {index === 0 && <Spacer />}
        <EntityInfoPlaceholder showAvatar />
      </>
    );
  }
  return (
    <>
      {index === 0 && <Spacer />}
      <EntityInfoCard
        {...item}
        onPressCard={() =>
          navigation.navigate('View Contact', {
            contact: item,
          })
        }
      />
    </>
  );
};

const handleOnPressEnroll = (props) => {
  const {navigation} = props;
  return navigation.navigate('Add Contact');
};

const List = (props) => {
  const dispatch = useDispatch();
  const {contactsList, isLoading} = props;
  const listData = !isLoading ? contactsList : ITEM_PLACEHOLDER;

  return (
    <ContactsList
      keyExtractor={(_item, index) => index.toString()}
      data={listData}
      refreshing={false}
      onRefresh={() => dispatch({type: 'GET_CONTACTS_REQUEST'})}
      renderItem={({item, index}: RenderItemProps) =>
        renderListItem(item, index, props)
      }
      ListEmptyComponent={<ListEmpty {...props} />}
    />
  );
};

const ContactsLayout = (props) => {
  return (
    <Layout>
      <Container>{props.children}</Container>
      <EnrollButton
        testId="Enroll Contact"
        onPress={() => handleOnPressEnroll(props)}
      />
    </Layout>
  );
};

const Contacts = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const contactsState = useSelector((state) => state.contacts);

  const {isLoading, contactsList} = contactsState;

  const [filteredContacts, setFilteredContacts] = useState(null);
  const [isTyping, setIsTyping] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');

  const [isSnackbarVisible, setIsSnackbarVisible] = useState(false);

  const data = isEmpty(searchTerm) ? contactsList : filteredContacts;

  useEffect(() => {
    dispatch({
      type: 'GET_CONTACTS_REQUEST',
    });
  }, [dispatch]);

  useEffect(() => {
    if (props.route?.params?.refetch) {
      dispatch({
        type: 'GET_CONTACTS_REQUEST',
      });
      props.navigation.setParams({refetch: false});
      setIsSnackbarVisible(true);

      setTimeout(() => {
        setIsSnackbarVisible(false);
      }, 5000);
    }
  }, [props, dispatch]);

  useEffect(() => {
    const filteredList = filterContacts(searchTerm, contactsList);

    setFilteredContacts(filteredList);
    setIsTyping(false);
  }, [contactsList, searchTerm]);

  return (
    <ContactsLayout navigation={props.navigation}>
      <SearchBar
        placeholder="Search by name or account no."
        value={searchTerm}
        setSearchTerm={setSearchTerm}
      />
      <List
        {...props}
        {...contactsState}
        searchTerm={searchTerm}
        isLoading={isTyping || isLoading}
        contactsList={data}
      />
      <Snackbar
        visible={isSnackbarVisible}
        message="You have successfuly deleted a contact."
      />
    </ContactsLayout>
  );
};

export default Contacts;
