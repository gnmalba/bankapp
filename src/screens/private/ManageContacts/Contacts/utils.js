import {filter, includes} from 'lodash';

export const isEmpty = (value) => {
  return !value || value === undefined || value === '' || value.length === 0;
};
export const filterContacts = (searchTerm, contacts = []) => {
  return filter(contacts, (item) => {
    const hasMatchOnName = includes(item.title, searchTerm);
    const hasMatchOnAcctNumber = includes(item.subtitle, searchTerm);
    const hasMatchOnMobile = includes(item.data.mobileNumber, searchTerm);

    return hasMatchOnName || hasMatchOnAcctNumber || hasMatchOnMobile;
  });
};
