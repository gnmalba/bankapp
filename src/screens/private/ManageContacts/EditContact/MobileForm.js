/**
 * @flow
 */

import React from 'react';
// Compositions
import DisabledField from '@composition/ManageContacts/DisabledField';

export type PropsType = {
  data: Object,
};

const EditContact = ({data}: PropsType): React$Node => {
  const {firstName, lastName, mobileNumber, emailAddress} = data;
  return (
    <>
      <DisabledField label="Mobile Number" value={mobileNumber} />
      <DisabledField label="First Name" value={firstName} />
      <DisabledField label="Last Name" value={lastName} />
      <DisabledField label="Email Address" value={emailAddress} />
    </>
  );
};

export default EditContact;
