/**
 * @flow
 */

import React from 'react';
// Compositions
import DisabledField from '@composition/ManageContacts/DisabledField';

export type PropsType = {
  data: Object,
};

const EditContact = ({data}: PropsType): React$Node => {
  const {
    bankName,
    acctNumber,
    businessName,
    firstName,
    lastName,
    mobileNumber,
    emailAddress,
  } = data;
  return (
    <>
      <DisabledField label="Bank" value={bankName} />
      <DisabledField label="Account Number" value={acctNumber} />
      {businessName ? (
        <DisabledField label="Business Name" value={businessName} />
      ) : (
        <>
          <DisabledField label="First Name" value={firstName} />
          <DisabledField label="Last Name" value={lastName} />
        </>
      )}
      <DisabledField label="Mobile Number" value={mobileNumber} />
      <DisabledField label="Email Address" value={emailAddress} />
    </>
  );
};

export default EditContact;
