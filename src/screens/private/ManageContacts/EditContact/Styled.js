/**
 * @flow
 */

import type {ComponentType} from 'react';

import styled from 'styled-components/native';
import {ScrollView} from 'react-native';
import {scale, verticalScale} from '@utils/scale';
import Button from '@components/Button';

export const Container: any = styled(ScrollView).attrs((props) => ({
  showsVerticalScrollIndicator: false,
  bounces: false,
  contentContainerStyle: {
    paddingHorizontal: scale(32.5),
    paddingVertical: verticalScale(40),
  },
}))`
  background-color: ${(props) => props.theme.layout.backgroundColor};
  display: flex;
  flex: 1;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(24)}px;
  width: 100%;
`;

export const SaveChangesButton: ComponentType<any> = styled(Button).attrs(
  (props) => ({
    title: 'Save Changes',
    type: 'primary',
    titleCustomColor: '#FFFFFF',
    state: 'default',
    ...props,
  }),
)``;

export const CancelButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;
export const DeleteContact: ComponentType<any> = styled(Button).attrs(
  (props) => ({
    title: 'Delete Contact',
    type: 'secondary',
    titleCustomColor: '#FFFFFF',
    state: 'default',
    ...props,
  }),
)`
  align-items: center;
`;
