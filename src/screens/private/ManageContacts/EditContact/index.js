/**
 * @flow
 */

import React, {useState, useEffect, useCallback} from 'react';
import {useFocusEffect} from '@react-navigation/core';
import {BackHandler} from 'react-native';

// Third party libraries
import {useFormik} from 'formik';

// Compositions
import ContactType from '@composition/ManageContacts/ContactType';
import Alias from '@composition/ManageContacts/Alias';

import {validationSchema} from './validationSchema';

import PasscodeModal from '@components/PasscodeModal';

import {
  BANK_TYPE,
  NUMBER_TYPE,
} from '@composition/ManageContacts/ContactType/constants';

// Components
import {
  Container,
  ButtonContainer,
  SaveChangesButton,
  DeleteContact,
  CancelButtonContainer,
} from './Styled';
import BankForm from './BankForm';
import MobileForm from './MobileForm';
export type PropsType = {
  navigation: Object,
  route: Object,
};

// Redux
import {
  editContactRequest,
  deleteContactRequest,
  clearApiResponse,
} from '@ducks/contacts/actions';
import {useDispatch, useSelector} from 'react-redux';

const EditContact = (props: PropsType): React$Node => {
  const {data} = props.route.params;
  const [isVisible, setIsVisible] = useState(false);
  const [forEdit, setForEdit] = useState(false);
  const [buttonState, setButtonState] = useState('disabled');

  // Redux Implementation
  const dispatch = useDispatch();
  const apiResponse = useSelector((state) => state.contacts.apiResponse);

  const {error, data: apiData} = apiResponse;

  const formik = useFormik({
    initialValues: {...data},
    enableReinitialize: true,
    validateOnMount: false,
    validateOnBlur: true,
    validationSchema: validationSchema(),
  });

  const {
    values,
    errors,
    touched,
    isValid,
    setFieldValue,
    setFieldTouched,
  } = formik;

  const toggleModal = () => {
    dispatch(clearApiResponse());
    setIsVisible(!isVisible);
  };

  const handleEditButton = () => {
    toggleModal();
    setForEdit(true);
  };

  const handleDeleteButton = () => {
    toggleModal();
    setForEdit(false);
  };

  const handlePasscode = (mpin) => {
    if (forEdit) {
      const params = {
        contactId: data.contactId,
        mpin,
        alias: values.alias,
      };
      dispatch(editContactRequest({...params}));
    } else {
      const params = {
        contactId: data.contactId,
        mpin,
      };
      dispatch(deleteContactRequest({...params}));
    }
  };

  const handleInputChange = (name) => (value) => {
    setFieldValue(name, value);
  };

  const handleResetField = (name) => {
    setFieldValue(name, data[name]);
  };

  useEffect(() => {
    if (isValid && values.alias !== data.alias) {
      setButtonState('default');
    } else {
      setButtonState('disabled');
    }
  }, [isValid, values, data]);

  useEffect(() => {
    if (apiData?.code === 200) {
      dispatch(clearApiResponse());
      if (forEdit) {
        props.navigation.goBack();
      } else {
        props.navigation.navigate('Contacts', {refetch: true});
      }
    }
  }, [apiData, props, dispatch, forEdit]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        props.navigation.pop();
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [props.navigation]),
  );

  return (
    <Container>
      <ContactType
        value={data.acctNumber ? BANK_TYPE.value : NUMBER_TYPE.value}
        setContactType={() => {}}
      />
      {data.bankName ? <BankForm data={data} /> : <MobileForm data={data} />}
      <Alias
        value={values?.alias}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={setFieldTouched}
        error={errors?.alias}
        isTouched={touched?.alias}
      />
      <ButtonContainer>
        <SaveChangesButton state={buttonState} onPress={handleEditButton} />
        <CancelButtonContainer>
          <DeleteContact onPress={handleDeleteButton} />
        </CancelButtonContainer>
      </ButtonContainer>
      <PasscodeModal
        isVisible={isVisible}
        mainButtonLabel="Proceed"
        passcodeError={error}
        positiveCallback={handlePasscode}
        negativeCallback={toggleModal}
        password
      />
    </Container>
  );
};

export default EditContact;
