## AccountSettingsChangePassword
AccountSettingsChangePassword component.

# Usage
```js
import AccountSettingsChangePassword from '@screens/private/AccountSettingsChangePassword';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
