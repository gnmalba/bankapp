/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: any,
    pop: any,
  },
  route: {
    params: {
      oldpassword: string,
    },
  },
  theme?: Object,
};

export type PasscodePropsType = {
  passcode: string,
  onTextChange: Function,
  passcodeError: string,
  handleNext: Function,
  error: Boolean,
  setPasscodeError: Function,
};
