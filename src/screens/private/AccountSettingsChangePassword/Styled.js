/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {verticalScale, moderateScale, scale} from '@utils/scale';
import {Image, TouchableOpacity} from 'react-native';

export const ButtonContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.layout.backgroundColor};
  border-radius: ${moderateScale(4)}px;
`;

export const StyledVisibilityImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    const liteVisible = require('@assets/icons/visibility-light-active.png');
    const darkVisible = require('@assets/icons/visibility-light.png');
    const darkActive = require('@assets/icons/visibility-dark-active.png');
    const successIcon = require('@assets/icons/success-icon.png');
    const mode = props.theme?.passwordInput?.mode;
    let source =
      props.state === 'active' && mode === 'dark' && props.value.length > 0
        ? darkActive
        : props.theme?.passwordInput?.src;

    if (props.visible) {
      source = mode === 'dark' ? darkVisible : liteVisible;
    }

    if (props.isFromNewPassword) {
      source = successIcon;
    }

    return {
      source: source,
      resizeMode: 'cover',
    };
  },
)`
  width: ${(props) => moderateScale(props.isFromNewPassword ? 15 : 24)}px;
  height: ${(props) => moderateScale(props.isFromNewPassword ? 10 : 22.7)}px;
`;

export const PasswordContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(24)}px;
`;

export const CheckIconImage: ComponentType<any> = styled(Image).attrs(
  (props) => ({
    tintColor: props.isValid ? '#84D268' : '#445870',
    source: require('@assets/icons/success-icon.png'),
  }),
)`
  width: ${moderateScale(16)}px;
  height: ${moderateScale(16)}px;
  resize-mode: contain;
  align-self: center;
  margin-right: ${scale(5)}px;
`;

export const LinkText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const TextValidationContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-vertical: ${verticalScale(4)}px;
`;

export const ForgotPasswordTouch: ComponentType<any> = styled(
  TouchableOpacity,
).attrs((props) => ({
  ...props,
  onPress: props.onPress,
  activeOpacity: 1,
}))``;

export const ForgotPasswordContainer: ComponentType<any> = styled.View`
  align-self: flex-end;
  margin-vertical: ${scale(8)}px;
`;

export const ForgotPassText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(13.8)}px;
  color: ${(props) => props.theme.defaultColor};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;
