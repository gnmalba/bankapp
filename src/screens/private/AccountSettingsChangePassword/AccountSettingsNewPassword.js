/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useEffect} from 'react';
import {View} from 'react-native';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Ducks
import {getPasswordPolicyRequest} from '@ducks/managesettings/actions';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Separator from '@components/Separator';
import Loader from '@composition/Loader';
import {CancelButton} from '@composition/EnrollAccount';
import PasswordInputValidation from '@composition/PasswordInputValidation';

// Utils
import {verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import * as S from './Styled';

const AccountSettingsNewPassword = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const manageSettings = useSelector((state) => state.manageSettings);
  const oldPassword = props.route?.params?.oldpassword;
  const {loading} = manageSettings;

  const [inputState, setInputState] = useState('default');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [isNewPasswordValid, setNewPasswordValid] = useState(false);

  useEffect(() => {
    dispatch(getPasswordPolicyRequest());
  }, [dispatch]);

  const handleSubmit = () => {
    props.navigation.navigate('AccountSettingsPasscode', {
      oldpassword: oldPassword,
      newpassword: password,
    });
  };

  const onCancel = () => {
    props.navigation.pop();
  };

  const checkForErrors = () => {
    const errors = password.length === 0 ? 'Please enter your password.' : '';

    setPasswordError(errors);
    setInputState(errors === '' ? 'default' : 'error');
  };

  const renderContent = () => (
    <View>
      <Text mode="h2" {...testProps('accountSettingsh2testId')}>
        Change Password
      </Text>
      <Separator space={verticalScale(10)} />
      <Text
        mode="body2"
        themeColor="TEXT_100_HIGH"
        {...testProps('accountSettingsTextBody2testId')}>
        Please fill in the details.
      </Text>
      <Separator space={verticalScale(16)} />
      <S.PasswordContainer>
        <PasswordInputValidation
          {...testProps('passwordInputId')}
          label="Old Password"
          placeHolder="Enter Old Password"
          state={'correct'}
          isFromNewPassword
          helperText={passwordError}
          value={oldPassword}
          textWidth="78%"
          onChangeText={(text) => {
            setPassword(text);
          }}
          handleClear={() => {
            setPassword('');
          }}
          onFocus={() => {
            setPasswordError('');
            setInputState('active');
          }}
          onBlur={() => {
            checkForErrors();
          }}
        />
      </S.PasswordContainer>
      <S.PasswordContainer>
        <PasswordInputValidation
          {...testProps('passwordInputId')}
          label="New Password"
          placeholder="Enter New Password"
          state={inputState}
          showTextValidation={setNewPasswordValid}
          helperText={passwordError}
          value={password}
          textWidth="78%"
          onChangeText={(text) => {
            setPassword(text);
          }}
          handleClear={() => {
            setPassword('');
          }}
          onFocus={() => {
            setPasswordError('');
            setInputState('active');
          }}
          onBlur={() => {
            setInputState('default');
          }}
          setError={setPasswordError}
        />
      </S.PasswordContainer>
    </View>
  );

  const renderBottom = () => (
    <>
      <S.ButtonContainer>
        <Button
          size="large"
          type="primary"
          state={isNewPasswordValid ? 'default' : 'disabled'}
          title={'Save'}
          {...testProps('accountSettingsNewPasswordReportBtn')}
          onPress={handleSubmit}
        />
      </S.ButtonContainer>
      <CancelButton handleCancel={onCancel} />
    </>
  );

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  return <SingleScreen contents={renderContent()} bottom={renderBottom()} />;
};

export default AccountSettingsNewPassword;
