/**
 * @format
 * @flow
 */

// React modules
import React, {useState} from 'react';
import {View} from 'react-native';

// Third party libraries
import {useSelector} from 'react-redux';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Separator from '@components/Separator';
import Loader from '@composition/Loader';
import PasswordInput from '@composition/PasswordInput';

// Utils
import {verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import * as S from './Styled';
import {ERRORS} from '@utils/manageSettings';

const AccountSettingsChangePassword = (props: PropsType): React$Node => {
  const manageSettings = useSelector((state) => state.manageSettings);
  const {loading} = manageSettings;

  const [inputState, setInputState] = useState('default');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const handleSubmit = () => {
    props.navigation.navigate('AccountSettingsNewPassword', {
      oldpassword: password,
    });
  };

  const checkForErrors = () => {
    let errors = '';
    if (password.length === 0) {
      errors = ERRORS.MESSAGES.EMPTY_PASSWORD;
    }
    if (password.length > 0 && password.length < 8) {
      errors = ERRORS.MESSAGES.MINIMUM_CHARACTERS;
    }
    setPasswordError(errors);
    setInputState(errors === '' ? 'default' : 'error');
  };

  const renderContent = () => (
    <View>
      <Text mode="h2" {...testProps('accountSettingsh2testId')}>
        Change Password
      </Text>
      <Separator space={verticalScale(10)} />
      <Text
        mode="body2"
        themeColor="TEXT_100_HIGH"
        {...testProps('accountSettingsTextBody2testId')}>
        Please fill in the details.
      </Text>
      <Separator space={verticalScale(16)} />
      <S.PasswordContainer>
        <PasswordInput
          {...testProps('passwordInputId')}
          label="Old Password"
          placeholder="Enter Old Password"
          state={inputState}
          helperText={passwordError}
          value={password}
          textWidth="78%"
          onChangeText={(text) => {
            setPassword(text);
          }}
          handleClear={() => {
            setPassword('');
          }}
          onFocus={() => {
            setPasswordError('');
            setInputState('active');
          }}
          onBlur={() => {
            checkForErrors();
          }}
        />
      </S.PasswordContainer>
    </View>
  );

  const renderBottom = () => (
    <S.ButtonContainer>
      <Button
        size="large"
        type="primary"
        state={password && passwordError === '' ? 'default' : 'disabled'}
        title={'Submit'}
        {...testProps('lostCardReportBtn')}
        onPress={handleSubmit}
      />
    </S.ButtonContainer>
  );

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  return <SingleScreen contents={renderContent()} bottom={renderBottom()} />;
};

export default AccountSettingsChangePassword;
