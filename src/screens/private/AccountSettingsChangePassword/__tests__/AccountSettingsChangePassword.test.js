import React from 'react';
import {render} from '@testing-library/react-native';
import AccountSettingsChangePassword from '../AccountSettingsChangePassword';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('AccountSettingsChangePassword', () => {
  const mockStore = () => ({
    manageSettings: {
      loading: false,
      data: undefined,
    },
  });
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <AccountSettingsChangePassword />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
