/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: any,
    pop: any,
  },
  theme: Object,
  errorResponse?: Object,
  checkLinkedDevices?: boolean,
};

export type ListItemWidgetPropsType = {
  testID?: string,
  accessibilityLabel?: string,
  onToggleSwtich: () => void,
  deviceSensor?: string,
  isEnabled: boolean,
};
