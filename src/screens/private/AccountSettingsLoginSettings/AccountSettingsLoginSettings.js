/* eslint-disable sonarjs/cognitive-complexity */
/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useCallback, useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {View, Platform} from 'react-native';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Components
import Loader from '@composition/Loader';
import Layout from '@components/Layout';
import Text from '@components/Text';
import Separator from '@components/Separator';
import PasscodeModal from '@components/PasscodeModal';
import Snackbar from '@components/Snackbar';
import ListItemWidget from './ListItemWidget';
import Modal from '@screens/public/RecoverAccess/components/UsernameEmailModal';

// Screens
import LoginSettinsErrorScreen from './ErrorScreen';

import {
  BIOMETRICS_START_REGISTER,
  BIOMETRICS_UNREGISTER,
  BIOMETRICS_UPDATE_STATE,
} from '@ducks/biometrics/actionTypes';

// Utils
import {verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';
import * as S from './Styled';
import ManageSettingsConfig from '@api/services/manageSettingsService';

const AccountSettingsLosingSettings = (props: PropsType): React$Node => {
  const {theme, checkLinkedDevices} = props;
  const dispatch = useDispatch();
  const [selectedSettings, setSelectedSettings] = useState('');
  const [showSnackbar, setShowSnackbar] = useState(false);
  const [isVisible, setVisible] = useState(false);

  const [biometrics, authentication] = useSelector((state) => [
    state.biometrics,
    state.authentication,
  ]);
  const {data, username, encryptionPublicKey, accessToken} = authentication;
  const {loading, errorResponse, biometricsError} = biometrics;
  const deviceSensoir = biometrics?.deviceSensor;

  const isBioEnrolledAndSameUser =
    biometrics?.isBiometricsEnrolled &&
    biometrics?.enrolledUsername === authentication?.username
      ? true
      : false;

  const [loginSettings, setLoginSettings] = useState({
    iOSID_Face: isBioEnrolledAndSameUser,
    touchID: isBioEnrolledAndSameUser,
  });
  const [showBioMsg, setShowBioMsg] = useState(false);

  useFocusEffect(
    useCallback(() => {
      dispatch({
        type: BIOMETRICS_UPDATE_STATE,
        newState: {errorResponse: null, registerBiometrics: false},
      });
    }, [dispatch]),
  );

  useEffect(() => {
    if (checkLinkedDevices) {
      ManageSettingsConfig.getDevices(authentication.data.customerId)
        .then((res) => {
          if (res.data?.data?.length > 1) {
            setShowBioMsg(true);
          }
        })
        .catch((error) => {});
    }
  }, [checkLinkedDevices, authentication.data.customerId]);

  const onToggleSwtich = (loginSetting: string): void => {
    setVisible(true);
    setSelectedSettings(loginSetting);
  };

  const onProceed = (passcode) => {
    const params = {
      customerId: data?.customerId,
      username,
      accessToken,
      encryptionPublicKey,
      successMessage,
    };

    if (isBioEnrolledAndSameUser) {
      dispatch({
        type: BIOMETRICS_UNREGISTER,
        payload: {
          ...params,
          passcode,
        },
      });
    } else {
      dispatch({
        type: BIOMETRICS_START_REGISTER,
        payload: {
          ...params,
          passcode,
        },
      });
    }
  };

  const onCancel = () => {
    setVisible(false);
    dispatch({
      type: BIOMETRICS_UPDATE_STATE,
      newState: {errorResponse: null},
    });
  };

  const successMessage = (): void => {
    if (selectedSettings) {
      setLoginSettings((prevState) => ({
        ...prevState,
        [selectedSettings]: !prevState[selectedSettings],
      }));
      setVisible(false);
      setShowSnackbar(true);
      setTimeout(() => {
        setShowSnackbar(false);
      }, 4000);
    }
  };

  const renderErrorScreen = () => (
    <LoginSettinsErrorScreen
      theme={theme}
      errorResponse={errorResponse}
      navigation={props.navigation}
    />
  );

  const renderContent = () => (
    <View>
      <Separator space={verticalScale(15)} />
      <Text
        mode="h2"
        themeColor="TEXT_300_HIGH"
        {...testProps('lostCardTextBody1testId')}>
        Login Settings
      </Text>
      <Separator space={verticalScale(10)} />
      <S.TextBody {...testProps('lostCardTextBodytestId')}>
        Select how you want to login to your Metrobank app.
      </S.TextBody>
      <Separator space={verticalScale(5)} />
      {deviceSensoir && biometricsError === '' && (
        <>
          {Platform.OS === 'ios' ? (
            <ListItemWidget
              deviceSensor={deviceSensoir}
              isEnabled={loginSettings.iOSID_Face}
              onToggleSwtich={() => onToggleSwtich('iOSID_Face')}
            />
          ) : (
            <ListItemWidget
              deviceSensor={deviceSensoir}
              isEnabled={loginSettings.touchID}
              onToggleSwtich={() => onToggleSwtich('touchID')}
            />
          )}
        </>
      )}
      {isVisible && (
        <PasscodeModal
          isVisible={isVisible}
          password
          mainButtonLabel={'Proceed'}
          passcodeError={
            errorResponse?.errors[0]?.code === 'invalid_mpin'
              ? 'Passcode is invalid. Try again'
              : ''
          }
          positiveCallback={onProceed}
          negativeCallback={onCancel}
        />
      )}
    </View>
  );

  if (errorResponse && errorResponse?.errors[0]?.code !== 'invalid_mpin') {
    return renderErrorScreen();
  }

  return (
    <Layout padding={26}>
      <>{renderContent()}</>
      <Loader isVisible={loading} />
      <Modal
        visible={showBioMsg}
        txt={
          'If you continue, all biometrics registered on other devices will be unregistered.'
        }
        btnTxt="Okay"
        onPress={() => {
          setShowBioMsg(false);
        }}
      />
      <Snackbar
        visible={showSnackbar}
        message={'You have successfuly changed your login settings'}
        {...testProps('loginSettingsSnackBartestId')}
      />
    </Layout>
  );
};

export default AccountSettingsLosingSettings;
