/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {verticalScale, moderateScale} from '@utils/scale';
import {TouchableOpacity} from 'react-native';
import {ListItem} from 'react-native-elements';

export const ListItemStyled: ComponentType<any> = styled(ListItem).attrs(
  (props) => ({
    key: props.key,
    bottomDivider: props.borderBottom,
    containerStyle: {
      backgroundColor: 'transparent',
      paddingHorizontal: 0,
      paddingVertical: verticalScale(16),
      borderColor: props.theme.color.TEXT_300_LOW,
    },
  }),
)``;

export const ListItemTouch: ComponentType<any> = styled(TouchableOpacity).attrs(
  (props) => ({
    ...props,
    onPress: props.onPress,
    key: props.key,
    activeOpacity: 0.8,
  }),
)``;

export const ListTitle: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(18)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;

export const TextBody: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(14)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  flex-wrap: wrap;
`;
