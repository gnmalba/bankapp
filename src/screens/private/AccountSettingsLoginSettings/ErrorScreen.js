/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Components
import Button from '@components/Button';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';

const LoginSettinsErrorScreen = (props: PropsType): React$Node => {
  const {theme, navigation, errorResponse} = props;
  const isMPINExceed =
    errorResponse?.errors[0]?.code === 'invalid_mpin_exceeded';

  const renderErrorScreen = () => (
    <ErrorScreen
      theme={theme}
      showBackButton
      headingAlignCenter={isMPINExceed}
      heading={
        isMPINExceed
          ? 'You have reached the maximum number of attempts. Please try again after (x) minutes.'
          : 'Oops! Something went wrong.'
      }
      details={
        errorResponse
          ? isMPINExceed
            ? null
            : errorResponse?.errors[0]?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      isSystemError={errorResponse ? false : true}
      handlePrimaryButton={() => navigation.pop()}
      handleSecondaryButton={() => navigation.navigate('Drawer')}
      buttonContents={
        <Button
          title="Go back"
          type="primary"
          state="default"
          {...testProps('loginSettingsErrorBtn')}
          onPress={() => navigation.pop()}
        />
      }
    />
  );

  return renderErrorScreen();
};

export default LoginSettinsErrorScreen;
