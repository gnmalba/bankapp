/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {ListItem} from 'react-native-elements';
import {Switch} from 'react-native';

// Components

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {ListItemWidgetPropsType} from './types';
import * as S from './Styled';

const ListItemWidget = (props: ListItemWidgetPropsType): React$Node => {
  const {onToggleSwtich, deviceSensor = '', isEnabled} = props;

  const renderContent = () => (
    <S.ListItemTouch
      onPress={onToggleSwtich}
      {...testProps('LoginSettingsToggleBtn')}>
      <S.ListItemStyled borderBottom={false}>
        <ListItem.Content>
          <S.ListTitle {...testProps('AccountSettingTitletestId')}>
            {deviceSensor}
          </S.ListTitle>
        </ListItem.Content>
        <Switch
          testID="ToggleBtn"
          trackColor={{false: '#51667C', true: '#4a7d56'}}
          thumbColor={isEnabled ? '#84D268' : '#A2BDDB'}
          ios_backgroundColor="transparent"
          onValueChange={onToggleSwtich}
          style={{transform: [{scaleX: 0.85}, {scaleY: 0.85}]}}
          value={isEnabled}
        />
      </S.ListItemStyled>
    </S.ListItemTouch>
  );

  return renderContent();
};

export default ListItemWidget;
