## AccountSettingsLosingSettings
AccountSettingsLosingSettings component.

# Usage
```js
import AccountSettingsLosingSettings from '@screens/private/AccountSettingsLosingSettings';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
