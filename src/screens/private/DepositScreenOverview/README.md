## DepositOverview
DepositOverview component.

# Usage
```js
import DepositOverview from '@composition/DepositOverview';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
