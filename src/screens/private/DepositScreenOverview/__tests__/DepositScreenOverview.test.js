import React from 'react';
import {render} from '@testing-library/react-native';
import DepositOverview from '../DepositScreenOverview';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('DepositOverview', () => {
  const store = {
    depositprepaidoverview: {
      depositPrepaidDetails: {},
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <DepositOverview />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
