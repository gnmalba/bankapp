/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {FlatList} from 'react-native';
import * as S from './Styled';

import Layout from '@components/Layout';
import CardImageBackground from '@composition/CardImageBackground';
import AccountDetails from '@composition/AccountDetails';
import ActionShortcuts from '@composition/ActionShortcuts';
import TransactionHistory from '@composition/TransactionHistory';
import Loader from '@composition/Loader';

import {useDispatch, useSelector} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import type {Account} from '@ducks/pay-bills-credit/types';
import {setPbcSourceAccount} from '@ducks/pay-bills-credit/actions';
import {formatAccountObject} from '@ducks/pay-bills-credit/utils';
import {DEPOSIT_PREPAID_SHORTCUTS_DEFAULT} from '@utils/constants';

const DepositOverview = (props: PropsType): React$Node => {
  const {route} = props;
  const accountId = route?.params?.id;

  const dispatch = useDispatch();
  const depositPrepaidDetails = useSelector(
    (state) => state.depositprepaidoverview.depositPrepaidDetails,
  );
  const TrasactionsdepositPrepaid = useSelector(
    (state) => state.depositprepaidoverview.transactionDepositPrepaid,
  );
  const currencyCode = depositPrepaidDetails.data?.depositAccount?.currencyCode;

  React.useEffect(() => {
    dispatch({type: 'GET_DEPOSIT_PREPAID_API_REQUESTED', payload: accountId});
    dispatch({
      type: 'GET_DEPOSIT_PREPAID_HISTORY_REQUESTED',
      payload: accountId,
    });
  }, [accountId, dispatch]);

  /*###### PAY BILLS CREDIT ACTIONS #######*/
  const setSourceAccount = (account: Account): any =>
    dispatch(setPbcSourceAccount(account));

  const actionConditions = {
    PAY_BILLS: (item, objectDetails) => {
      setSourceAccount(
        formatAccountObject({
          ...objectDetails.depositAccount,
          availableBalance: objectDetails.availableBalance.value,
          currencyCode: objectDetails.availableBalance.currencyCode,
        }),
      );
    },
    //add other action codes here ex: SEND_MONEY: (item, objectDetails) => {function here}
  };

  const handleActionPress = (item: Object) => {
    actionConditions[item.code] &&
      actionConditions[item.code](item, depositPrepaidDetails.data);
  };

  const renderScreen = () => {
    return (
      <>
        <KeyboardAwareScrollView
          keyboardShouldPersistTaps="always"
          enableOnAndroid>
          <S.Container>
            <CardImageBackground
              cardType="deposit"
              data={depositPrepaidDetails.data}
            />
            <AccountDetails
              cardType="deposit"
              data={depositPrepaidDetails.data}
            />

            <ActionShortcuts
              cardType="deposit"
              accountData={depositPrepaidDetails.data}
              data={DEPOSIT_PREPAID_SHORTCUTS_DEFAULT}
              onShortCutPress={handleActionPress}
            />
          </S.Container>
          <TransactionHistory
            cardType="deposit"
            data={depositPrepaidDetails.data}
            transaction={TrasactionsdepositPrepaid}
            titleTransaction="Transaction History"
            currencyCode={currencyCode}
          />
        </KeyboardAwareScrollView>
      </>
    );
  };

  return (
    <Layout>
      <FlatList
        keyboardShouldPersistTaps="handled"
        data={depositPrepaidDetails.loader}
        renderItem={renderScreen}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={() => <Loader isVisible={true} />}
      />
    </Layout>
  );
};

export default DepositOverview;
