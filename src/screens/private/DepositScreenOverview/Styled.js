/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {ScrollView} from 'react-native';
import {scale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(24)}px;
`;

export const ScrollContainer: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    showsHorizontalScrollIndicator: false,
    showsVerticalScrollIndicator: false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
  }),
)``;
