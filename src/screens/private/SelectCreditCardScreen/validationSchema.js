import * as yup from 'yup';
import {ERRORS} from './constants';
import {isValid as isValidCC} from 'cc-validate';

export const initialValues = {
  accountNo: null,
};

export const maxLength = 19;
export const minLength = 14;

export const validationSchema = (values) => {
  return yup.object().shape({
    accountNo: yup
      .string()
      .required(ERRORS.ACCOUNT_NUMBER.REQUIRED)
      .nullable()
      .min(minLength, ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT)
      .max(maxLength, ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT)
      .test(
        'is-valid-credit-card',
        ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT,
        (value) => {
          if (value) {
            value = value.replace(/\s+/g, ''); // remove spacing added by auto spacing on input
            if (value?.length >= minLength) {
              return true;
            }
          }
          return false;
        },
      )
      .test(
        'is-mod10-validated',
        ERRORS.ACCOUNT_NUMBER.REQUIRED,
        (value) => isValidCC(value).isValid,
      ),
  });
};
