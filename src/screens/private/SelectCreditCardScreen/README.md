## SelectCreditCardScreen
SelectCreditCardScreen component.

# Usage
```js
import SelectCreditCardScreen from '@screens/private/SelectCreditCardScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
