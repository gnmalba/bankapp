/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Platform} from 'react-native';
import Text from '@components/Text';
import styled from 'styled-components/native';
import {moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View``;

export const CreditCardContainer: ComponentType<any> = styled.View`
  flex: 2;
  z-index: ${() => Platform.select({android: 0, ios: -1})};
`;

export const HeaderText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
}))`
  font-weight: 400;
  font-size: ${moderateScale(20)}px;
`;
