/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';

import type {PropsType} from './types';
import type {BalanceTransferFromType} from '@ducks/installments/types';
import {CreditCardContainer, HeaderText} from './Styled';
import Layout from '@components/Layout';
import SelectInput from '@components/SelectInput';
import CreditCardNumberInput from '@composition/CreditCardNumberInput';
import Button from '@components/Button';
import {
  EndButtonWrapperAbsolute,
  HeaderContainer,
  FlexContainer,
  Flex2Container,
} from '@utils/commonStyles';
import {useDispatch, useSelector} from 'react-redux';
import {
  getBankListRequested,
  updateBalanceTransferFrom,
} from '@ducks/installments/actions';
import {installmentsSelector} from '@ducks/installments/selectors';
import {bankDataToSelectInput, MAX_CC_LENGTH} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';
import {useFormik} from 'formik';
import {initialValues, validationSchema} from './validationSchema';
import {accountNumberInputMask} from '@utils/accountNumber';
import KeyboardDismissibleView from '@composition/KeyboardDismissibleView';
import Loader from '@composition/Loader';
import {ERRORS} from './constants';

const removeSpace = (value) => {
  return value.replace(/\s+/g, '');
};

const isButtonDisabled = (disabled: boolean): 'disabled' | 'default' => {
  return disabled ? 'disabled' : 'default';
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const SelectCreditCardScreen = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const installmentsState = useSelector(installmentsSelector);
  const [additionalErrors, setAdditionalError] = useState({
    accountNo: null,
  });
  const [selectedBank, setSelectedBank] = useState<any>();
  const [cardNumber, setCreditNumber] = useState<string>('');
  const [banks, setBanks] = useState([]);
  const formik = useFormik({
    initialValues: initialValues,
    enableReinitialize: true,
    validateOnMount: false,
    validateOnBlur: true,
    validationSchema: validationSchema(),
  });

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const {values, errors, touched, setFieldValue, setFieldTouched} = formik;
  const accountNumberLabel = 'Credit card number';
  const placeholderText = 'Enter card number';
  const maxLength = 19;

  useEffect(() => {
    dispatch(getBankListRequested('BT'));

    if (installmentsState.balanceTransfer.transferFrom) {
      setCreditNumber(
        installmentsState.balanceTransfer.transferFrom.cardNumber,
      );
      setFieldValue(
        'accountNo',
        accountNumberInputMask(
          installmentsState.balanceTransfer.transferFrom.cardNumber,
        ),
      );
      setSelectedBank({
        value: installmentsState.balanceTransfer.transferFrom.bankName,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (installmentsState.banks) {
      const bankToSelectInput = bankDataToSelectInput(installmentsState.banks);
      setBanks(bankToSelectInput);
    }
  }, [installmentsState.banks]);

  const handleChangeCreditNumber = (name: any) => (value) => {
    if (name === 'accountNo') {
      const unformattedAccountNo = removeSpace(value);
      setFieldValue(name, accountNumberInputMask(value));
      setCreditNumber(unformattedAccountNo);
      if (unformattedAccountNo.length !== MAX_CC_LENGTH) {
        setAdditionalError({accountNo: null});
        return;
      }
    } else {
      setFieldValue(name, value);
    }
  };

  const handleOnBlur = () => {
    const {accountNo} = formik.values;
    if (!accountNo) {
      setAdditionalError({accountNo: ERRORS.ACCOUNT_NUMBER.REQUIRED});
    }
  };

  const handleSubmit = () => {
    const payload: BalanceTransferFromType = {
      bankName: selectedBank.value,
      cardNumber,
    };

    dispatch(updateBalanceTransferFrom(payload));
    props.navigation.navigate('BalanceTransferScreen');
  };
  const handleResetField = (name) => {
    setFieldValue(name, initialValues[name]);
  };

  return (
    <>
      <KeyboardDismissibleView onPress={() => setDropdownOpen(false)}>
        <Layout padding={32}>
          {installmentsState.formLoading ? (
            <Loader isVisible />
          ) : (
            <FlexContainer>
              <Flex2Container>
                <HeaderContainer marginBottom={41}>
                  <HeaderText {...testProps('SelectCardScreenSelectionId')}>
                    Select a source credit card.
                  </HeaderText>
                </HeaderContainer>

                <FlexContainer>
                  <SelectInput
                    data={banks}
                    placeholder="Select issuing bank"
                    setter={setSelectedBank}
                    selected={selectedBank}
                    isOpen={dropdownOpen}
                    openSetter={setDropdownOpen}
                    label="Bank"
                  />
                </FlexContainer>

                <CreditCardContainer>
                  <CreditCardNumberInput
                    accountNo={values.accountNo}
                    handleInputChange={handleChangeCreditNumber}
                    handleResetField={handleResetField}
                    handleSetFieldTouched={setFieldTouched}
                    error={errors?.accountNo || additionalErrors.accountNo}
                    isTouched={touched?.accountNo}
                    label={accountNumberLabel}
                    placeholderText={placeholderText}
                    maxLength={maxLength}
                    handleOnBlur={handleOnBlur}
                  />
                </CreditCardContainer>
              </Flex2Container>

              <EndButtonWrapperAbsolute>
                <Button
                  title="Confirm"
                  type="primary"
                  state={isButtonDisabled(
                    !(cardNumber && selectedBank && !errors?.accountNo),
                  )}
                  onPress={handleSubmit}
                  {...testProps('SelectConfirmButton')}
                  labelTestID="id_confirm"
                />
              </EndButtonWrapperAbsolute>
            </FlexContainer>
          )}
        </Layout>
      </KeyboardDismissibleView>
    </>
  );
};

export default SelectCreditCardScreen;
