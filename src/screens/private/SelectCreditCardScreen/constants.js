export const ERRORS = {
  ACCOUNT_NUMBER: {
    REQUIRED: 'Please enter a valid credit card number.',
    CORRECT_FORMAT: 'Invalid input. Enter the number in the correct format.',
    INVALID_ACCOUNT_NUMBER: 'Please enter a valid Metrobank account number.',
    INVALID_ACCOUNT_NUMBER_NON_MB: 'Please enter a valid account number.',
    INVALID_SAVINGS_CHECKING_NUMBER:
      'Invalid input. Please enter a valid Metrobank savings or checking account number.',
  },
};
