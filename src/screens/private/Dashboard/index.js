/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @flow
 */

import React, {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SInfo from 'react-native-sensitive-info';

import Modal from '@composition/Modal';
import Text from '@components/Text';
import Button from '@components/Button';
import styled from 'styled-components/native';
import {scale} from '@utils/scale';
import {toggleRemoveLimit} from '@ducks/config/actions';
import {sinfoOptions} from '@ducks/helpers/biometrics';

import {useSelector, useDispatch} from 'react-redux';

import {WidgetsContainer, Divider} from './Styled';
import AnotherSessionClosedModal from './components/AnotherSessionClosedModal';

import Loader from '@composition/Loader';

import * as B from '@ducks/biometrics/actionTypes';

import type {AuthenticationType} from '@ducks/types';

import CustomizeWidgets from '@composition/CustomizeWidgets';
import TourCustomizeWidgets from '@composition/GuidedTourTooltip/TourCustomizeWidgets';
import ThemeOptions from '@composition/ThemeOptions';
import {StyledSnackbar} from './components/StyledSnackbar';

import {getActiveWidgets, getToggleState} from '@ducks/config/selectors';

import Widget, {WidgetLoader} from '@composition/Widget';
import TourCustomizeDashboard from '@composition/GuidedTourTooltip/TourCustomizeDashboard';

const StyledButton = styled(Button).attrs((props) => ({
  title: 'Go Back',
  titleCustomColor: props.theme.color.SECONDARY_DEFAULT,
}))``;

/**
 * We will migrate this if we wanted to reuse this pop up in other screens
 */

const StyledModalContainer = styled.View`
  display: flex;
  align-items: center;
`;

const StyledHorizontalSpace = styled.View`
  height: ${scale(15)}px;
`;

const StyledText = styled(Text).attrs({
  mode: 'body2',
})`
  text-align: center;
`;

const WidgetUnableToRemoveModal = ({visible}) => {
  const dispatch = useDispatch();
  return (
    <Modal visible={visible}>
      <StyledModalContainer>
        <StyledText>
          There should be at least one widget present in the dashboard
        </StyledText>
        <StyledHorizontalSpace />
        <StyledButton onPress={() => dispatch(toggleRemoveLimit(false))} />
      </StyledModalContainer>
    </Modal>
  );
};

type DashboardPropTypes = {
  navigation: {
    toggleDrawer: () => void,
    navigate: (route: string, params?: Object) => void,
  },
  dashboard: Object,
  authentication: AuthenticationType,
};

function Dashboard(props: DashboardPropTypes): React$Node {
  const dispatch = useDispatch();
  const [dashboardTourShown, setDashboardTourShown] = useState(false);
  const [dashboardTourWidgetsShown, setDashboardTourWidgetsShown] = useState(
    false,
  );

  const activeWidgets = useSelector(getActiveWidgets);
  const {loading, isFirstLogin} = useSelector((state) => state.authentication);

  const {isCustomizeWidgets, isCustomizeDashboard, isQuickLinks} = useSelector(
    (state) => state.dashboard,
  );

  const isWidgetLimitDeletionModalShown = useSelector(getToggleState);

  useEffect(() => {
    AsyncStorage.setItem('biometricsLinkDisabled', 'false');
    dispatch({
      type: B.BIOMETRICS_UPDATE_STATE,
      newState: {
        linkDisabled: false,
      },
    });
    SInfo.getItem('BIOMETRIC_USEERNAME', sinfoOptions)
      .then((username) => {
        dispatch({
          type: B.BIOMETRICS_UPDATE_STATE,
          newState: {
            enrolledUsername: username || '',
          },
        });
      })
      .catch(() => {});
  }, []);

  useEffect(() => {
    if (!isQuickLinks && isCustomizeDashboard) {
      setDashboardTourShown(true);
    }
  }, [isQuickLinks, isCustomizeDashboard]);

  useEffect(() => {
    if (!isCustomizeDashboard && isCustomizeWidgets) {
      setDashboardTourWidgetsShown(true);
    }
  }, [isCustomizeDashboard, isCustomizeWidgets]);

  const handleDashboardCustomizeWTourClose = () => {
    setDashboardTourShown(false);
    dispatch({
      type: 'UPDATE_GUIDE_CUSTOMIZE_DASHBOARD',
    });
  };

  const handleDashboardWidgetMoveClose = () => {
    setDashboardTourWidgetsShown(false);
    dispatch({
      type: 'UPDATE_GUIDE_CUSTOMIZE_WIDGETS',
    });
  };

  return (
    <>
      <ThemeOptions isVisible={isFirstLogin} />
      <WidgetUnableToRemoveModal visible={isWidgetLimitDeletionModalShown} />
      {dashboardTourShown && (
        <TourCustomizeDashboard
          isVisible={dashboardTourShown}
          onClose={handleDashboardCustomizeWTourClose}
        />
      )}
      {dashboardTourWidgetsShown && (
        <TourCustomizeWidgets
          isVisible={dashboardTourWidgetsShown}
          onClose={handleDashboardWidgetMoveClose}
        />
      )}
      <WidgetsContainer
        ListHeaderComponent={() => (
          <CustomizeWidgets
            onCustomizeDashboardOpen={() => {
              props.navigation.navigate('CustomizeWidgetScreen');
              dispatch({
                type: 'TOGGLE_CUSTOMIZE_WIDGET',
              });
            }}
          />
        )}
        data={activeWidgets}
        renderItem={({item}) => {
          return item.loading ? (
            <WidgetLoader />
          ) : (
            <Widget
              widgetUniqueRef={item.ref}
              navigation={props.navigation}
              label={item.label}
            />
          );
        }}
        keyExtractor={(item) => item.ref.toString()}
        ItemSeparatorComponent={Divider}
      />
      <StyledSnackbar testID="DeviceLinkingSnackbarId" />
      {loading ? <Loader isVisible={loading} /> : <></>}
      <AnotherSessionClosedModal />
    </>
  );
}

export default Dashboard;
