/**
 * @format
 * @flow
 */
// React Modules
import React from 'react';

// 3rd Party Libraries
import {useDispatch, useSelector} from 'react-redux';
import * as NavigationService from '@navigations/service';

// Composition
import ModalWithCTA from '@composition/ModalWithCTA';

type EnrollBillerModalType = {
  visible: boolean,
};

const EnrollBillerModal = ({visible}: EnrollBillerModalType): React$Node => {
  const dispatch = useDispatch();
  const dashboardState = useSelector((state) => state.dashboard);
  const handlePressEnroll = () => {
    NavigationService.navigate('ManageContactsStack', {
      screen: 'Add Contact',
      params:
        {
          sendMoneyNewBillerAccount: dashboardState?.billerAccount,
        } || {},
    });
    dispatch({type: 'HIDE_ENROLL_BILLER_PROMPT'});
  };
  const handlePressCancel = () => {
    dispatch({type: 'HIDE_ENROLL_BILLER_PROMPT'});
  };

  const titleProps = {
    text: dashboardState?.billerAccount
      ? `Enroll ${dashboardState.billerAccount?.alias} as a contact?`
      : 'Enroll the biller from your previous transaction?',
  };

  const subtitleProps = {
    text:
      'You can enroll the biller from your previous transaction for a faster payment next time.',
  };

  const positiveButtonProps = {
    buttonText: 'Enroll',
    callback: handlePressEnroll,
  };

  const negativeButtonProps = {
    buttonText: 'No, Thanks',
    callback: handlePressCancel,
  };

  return (
    <ModalWithCTA
      testID="enrollBillerModal"
      isVisible={visible}
      titleProps={titleProps}
      subtitleProps={subtitleProps}
      positiveButtonProps={positiveButtonProps}
      negativeButtonProps={negativeButtonProps}
    />
  );
};

export default EnrollBillerModal;
