import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import LogoutModal from '../LogoutModal';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('Dashboard', () => {
  const store = {};
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <LogoutModal />
        </ThemeProvider>
      </MockProvider>,
    );
    const el = all.getByTestId('logOutModalLogoutButton');
    fireEvent(el, 'onPress');
    const el2 = all.getByTestId('logOutModalCancelButton');
    fireEvent(el2, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });
});
