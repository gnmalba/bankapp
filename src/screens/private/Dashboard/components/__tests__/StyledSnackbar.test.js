import StyledSnackbar from '../StyledSnackbar';
import React from 'react';
import {render} from '@testing-library/react-native';
import MockProvider from '@utils/testFramework';

describe('StyledSnackbar', () => {
  test('StyledSnackbar', () => {
    expect(
      render(
        <MockProvider
          store={{
            authentication: {showSnackbar: true, snackbarMessage: 'test'},
          }}>
          <StyledSnackbar />
        </MockProvider>,
      ),
    ).toMatchSnapshot();
  });
});
