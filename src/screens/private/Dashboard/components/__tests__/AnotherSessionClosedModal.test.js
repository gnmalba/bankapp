import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import AnotherSessionClosedModal from '../AnotherSessionClosedModal';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('Dashboard', () => {
  const store = {
    authentication: {
      isFirstLogin: false,
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <AnotherSessionClosedModal />
        </ThemeProvider>
      </MockProvider>,
    );
    const el = all.getByTestId('loggedOutOkayBtn');
    fireEvent(el, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });
});
