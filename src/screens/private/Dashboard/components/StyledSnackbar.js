/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Snackbar from '@components/Snackbar';
import * as types from '@ducks/authentication/actionTypes';

import type {AuthenticationType, State} from '@ducks/types';
import {testProps} from '@utils/componentCustomProps';

type PropTypes = {
  testID?: string,
};

export const StyledSnackbar = (props: PropTypes): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const {testID} = props;
  const {showSnackbar, snackbarMessage} = authentication;
  const dispatch = useDispatch();

  useEffect(() => {
    const timeHandler = setTimeout(() => {
      dispatch({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {
          showSnackbar: false,
        },
      });
    }, 5000);
    return () => {
      clearTimeout(timeHandler);
    };
  }, [showSnackbar]);

  return (
    <Snackbar
      visible={showSnackbar}
      message={snackbarMessage}
      {...testProps(testID || 'snackBarTestId')}
    />
  );
};

export default StyledSnackbar;
