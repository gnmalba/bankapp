// @flow

import React, {useEffect, useState} from 'react';
import {View} from 'react-native';

import Modal from '@composition/Modal';
import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';
import {useDispatch, useSelector} from 'react-redux';

import {ModalActionContainer} from '../Styled';
import {verticalScale} from '@utils/scale';

import {AUTHENTICATION_UPDATE_STATE} from '@ducks/authentication/actionTypes';
import {testProps} from '@utils/componentCustomProps';

const centerText = {
  textAlign: 'center',
};

const AnotherSessionClosedModal = (): React$Node => {
  const dispatch = useDispatch();
  const [shouldBeVisible, setShouldBeVisible] = useState(false);
  const {isFirstLogin, anotherSessionClosed} = useSelector(
    (state) => state.authentication,
  );

  useEffect(() => {
    let timeout;
    if (anotherSessionClosed && !isFirstLogin) {
      // NOTE: Delay is needed for iOS modal issues
      timeout = setTimeout(() => {
        setShouldBeVisible(true);
      }, 500);
    } else {
      setShouldBeVisible(false);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [isFirstLogin, anotherSessionClosed]);

  return (
    <Modal visible={shouldBeVisible}>
      <View {...testProps('loggedOutOnlineSessionContainer')}>
        <Text
          mode="h3"
          themeColor="TEXT_300_HIGH"
          style={centerText}
          {...testProps('loggedOutOnlineSessionText')}>
          A session on another browser or device has ended.
        </Text>
        <Separator space={verticalScale(34)} />
        <ModalActionContainer>
          <Button
            {...testProps('loggedOutOkayBtn')}
            type="secondary"
            title="Okay"
            titleThemeColor="SECONDARY_DEFAULT"
            onPress={() => {
              dispatch({
                type: AUTHENTICATION_UPDATE_STATE,
                newState: {anotherSessionClosed: false},
              });
            }}
          />
        </ModalActionContainer>
      </View>
    </Modal>
  );
};

export default AnotherSessionClosedModal;
