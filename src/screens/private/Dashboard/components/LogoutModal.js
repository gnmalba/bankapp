// @flow

import React from 'react';

import Modal from '@composition/Modal';
import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';
import {useDispatch} from 'react-redux';

import {ModalActionContainer} from '../Styled';
import {verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

import {
  AUTHENTICATION_START_LOGOUT,
  AUTHENTICATION_UPDATE_STATE,
} from '@ducks/authentication/actionTypes';

type ModalProps = {
  testID?: string,
  accessibilityLabel?: string,
  visible?: boolean,
};

const LogoutModal = (props: ModalProps): React$Node => {
  const {visible} = props;
  const dispatch = useDispatch();
  return (
    <Modal visible={visible}>
      <Text mode="h3" {...testProps('logOutModalTitle')}>
        Are you sure you want to Logout?
      </Text>
      <Separator space={verticalScale(16)} />
      <Text
        mode="body2"
        themeColor="TEXT_100_HIGH"
        {...testProps('logOutModalMessage')}>
        Tapping 'Logout' will send you back to the login screen.
      </Text>
      <Separator space={verticalScale(24)} />
      <ModalActionContainer>
        <Button
          type="secondary"
          title="Logout"
          titleThemeColor="SECONDARY_DEFAULT"
          onPress={() => {
            dispatch({
              type: AUTHENTICATION_START_LOGOUT,
            });
          }}
          {...testProps('logOutModalLogoutButton')}
        />
        <Separator space={verticalScale(12)} />
        <Button
          type="secondary"
          title="Cancel"
          titleThemeColor="TEXT_300_HIGH"
          onPress={() => {
            dispatch({
              type: AUTHENTICATION_UPDATE_STATE,
              newState: {showLogoutModal: false},
            });
          }}
          {...testProps('logOutModalCancelButton')}
        />
      </ModalActionContainer>
    </Modal>
  );
};

export default LogoutModal;
