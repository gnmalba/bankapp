import React from 'react';
import {render} from '@testing-library/react-native';
import Dashboard from '../index';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

jest.useFakeTimers();

describe('Dashboard', () => {
  const store = {
    config: {
      theme: 'dark',
      widgets: [
        {
          ref: 1,
          label: 'Test Label A',
          loading: false,
        },
        {
          ref: 2,
          label: 'Test Label B',
          loading: false,
        },
      ],
      removedWidgets: [1, 2],
      checked: [],
      limitShown: false,
      customizedWidgets: [1, 2],
    },
    dashboard: {
      guidedTour: {},
    },
    authentication: {
      showLogoutModal: false,
      anotherSessionClosed: false,
      loading: false,
    },
    credit: {
      loading: false,
      data: [],
      error: false,
      errors: [],
    },
    recoverAccess: {
      showUsernameSentSnackbar: false,
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <Dashboard />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});
