/**
 * @flow
 */

import {Divider as RNEDivider} from 'react-native-elements';

import {IS_DEVICE_TABLET} from '@utils/constants';

/**
 * @flow
 */

import {FlatList, Platform} from 'react-native';
import styled from 'styled-components/native';
import {scale, moderateScale} from '@utils/scale';

export const Divider: any = styled(RNEDivider)`
  background-color: transparent;
  height: ${scale(16)}px;
`;

export const WidgetsContainer: typeof FlatList = styled(FlatList).attrs(
  (props) => ({
    ...Platform.select({
      ios: {
        alwaysBounceHorizontal: false,
        alwaysBounceVertical: false,
        //disabled bounces to avoid showing of white background
        bounces: false,
      },
      android: {
        // remove ripple effect in dashboard scroll view
        overScrollMode: 'never',
      },
    }),
    contentContainerStyle: {
      padding: moderateScale(IS_DEVICE_TABLET ? 60 : 15),
      paddingTop: 0,
      backgroundColor: props.theme.layout.backgroundColor,
    },
  }),
)`
  background-color: ${(props) => props.theme.layout.backgroundColor};
`;

export const ModalActionContainer: any = styled.View`
  justify-content: center;
  align-items: center;
`;
