## PrepaidScreenOverview
PrepaidScreenOverview component.

# Usage
```js
import PrepaidScreenOverview from '@screens/private/PrepaidScreenOverview';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
