import React from 'react';
import {render} from '@testing-library/react-native';
import PrepaidScreenOverview from '../PrepaidScreenOverview';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('PrepaidScreenOverview', () => {
  const store = {
    depositprepaidoverview: {
      depositPrepaidDetails: {
        loader: undefined,
      },
    },
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <PrepaidScreenOverview />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
