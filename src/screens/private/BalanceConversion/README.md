## BalanceConversion
BalanceConversion component.

# Usage
```js
import BalanceConversion from '@screens/private/BalanceConversion';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
