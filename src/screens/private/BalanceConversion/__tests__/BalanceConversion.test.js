import {NavigationContext} from '@react-navigation/core';
import {fireEvent, render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import BalanceConversion from '../BalanceConversion';
import ConfirmDetails from '../components/ConfirmDetails';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

const MOCK_CARDNUMBER = '521822******7813';
const MOCK_AVAILABLECREDIT = '1000000.00';
const MOCK_NAME = 'JOHN PATRICK A VIRAY';

describe('Balance Conversion', () => {
  const propsAndStore = [
    {
      store: {
        credit: {
          creditList: {
            data: [],
          },
        },
        installments: {
          balanceConversion: {
            amount: 6000,
            from: {
              alias: 'MC Platinum BT D4',
              availableCredit: MOCK_AVAILABLECREDIT,
              cardEmbossedName: MOCK_NAME,
              cardNo: MOCK_CARDNUMBER,
              cardNumber: MOCK_CARDNUMBER,
              cardSerNo: '2686978',
              creditLimit: MOCK_AVAILABLECREDIT,
              currency: 'PHP',
              customerId: '5958',
              flag: '0',
              id: MOCK_CARDNUMBER,
              name: MOCK_NAME,
              number: MOCK_CARDNUMBER,
              outstandingBalance: '0.00',
              primary: 0,
            },
            paymentTerms: {
              annualRate: '12.6077%',
              interestRate: '0.58%',
              monthlyAmortization: '534.80',
              shortCode: 'BT5812M',
              term: '12',
              totalInstallmentAmount: '6417.60',
              transactionFee: '0',
            },
          },
        },
      },
      props: {
        route: {
          params: {
            fromCC: true,
          },
        },
        navigation: {
          navigate: jest.fn(),
        },
      },
    },
    {
      store: {
        credit: {
          creditList: {
            data: [],
          },
        },
        installments: {
          balanceConversion: {},
        },
      },
      props: {
        route: {
          params: {
            fromSOA: true,
          },
        },
        navigation: {
          navigate: jest.fn(),
        },
      },
    },
  ];

  const navContext = {
    isFocused: () => true,
    addListener: jest.fn(() => jest.fn()),
  };

  it.each(propsAndStore)('Should work as expected', ({store, props}) => {
    const component = render(
      <NavigationContext.Provider value={navContext}>
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <BalanceConversion {...props} />
          </ThemeProvider>
        </MockProvider>
      </NavigationContext.Provider>,
    );

    const nextButton = component.getByTestId('BCinitialScreenNext');
    fireEvent(nextButton, 'onPress');

    if (Object.keys(store.installments.balanceConversion).length) {
      expect(props.navigation.navigate).toHaveBeenCalled();
    }

    const paymentTermsTO = component.getByTestId('TO Payment Terms');
    fireEvent(paymentTermsTO, 'onPress');

    if (Object.keys(store.installments.balanceConversion).length) {
      expect(props.navigation.navigate).toHaveBeenCalled();
    }

    const sourceTO = component.getByTestId('TO From');
    fireEvent(sourceTO, 'onPress');

    if (Object.keys(store.installments.balanceConversion).length) {
      expect(props.navigation.navigate).toHaveBeenCalled();
    }

    const amountField = component.getByTestId('Convert amount Field');

    fireEvent(amountField, 'onFocus');
    fireEvent.changeText(amountField, '500');
    fireEvent(amountField, 'onBlur');

    fireEvent.changeText(amountField, '0');
    fireEvent(amountField, 'onBlur');

    fireEvent.changeText(amountField, '9999999');
    fireEvent(amountField, 'onBlur');

    expect(component.toJSON()).toMatchSnapshot();
  });
});

describe('Confirm Details', () => {
  const props = {
    navigation: {navigate: jest.fn()},
    state: {
      amount: 6000,
      from: {
        alias: 'MC Platinum BT D4',
        availableCredit: MOCK_AVAILABLECREDIT,
        cardEmbossedName: MOCK_NAME,
        cardNo: MOCK_CARDNUMBER,
        cardNumber: MOCK_CARDNUMBER,
        cardSerNo: '2686978',
        creditLimit: MOCK_AVAILABLECREDIT,
        currency: 'PHP',
        customerId: '5958',
        flag: '0',
        id: MOCK_CARDNUMBER,
        name: MOCK_NAME,
        number: MOCK_CARDNUMBER,
        outstandingBalance: '0.00',
        primary: 0,
      },
      paymentTerms: {
        annualRate: '12.6077%',
        interestRate: '0.58%',
        monthlyAmortization: '534.80',
        shortCode: 'BT5812M',
        term: '12',
        totalInstallmentAmount: '6417.60',
        transactionFee: '0',
      },
    },
  };

  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ConfirmDetails {...props} />
      </ThemeProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();
  });
});
