/**
 * @flow
 */

export type PropTypes = {
  state: Object,
  navigation: Object,
};
