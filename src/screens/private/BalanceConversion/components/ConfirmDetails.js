/**
 * @format
 * @flow
 */

import {Divider} from '@components/Card/Card';
import Text from '@components/Text';
import InstallmentDetail from '@composition/InstallmentDetail';
// import {navigationOption} from '@navigations/Stack/Login/config';
import {accountsFormat16} from '@utils/accountNumber';
import {HeaderContainer as Container} from '@utils/commonStyles';
import {toText} from '@utils/helpers';
import {testProps} from '@utils/componentCustomProps';
import React from 'react';
import {View} from 'react-native';
import {numberFormatClean} from '@utils/numberInputs';
import {formatEAIR} from '@utils/installments';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';

export type PropTypes = {
  state: Object,
  navigation: Object,
};

const ConfirmDetails = ({state, navigation}: PropTypes): React$Node => {
  return (
    <>
      <View>
        <CardDetailWithLabel
          cardNumber={accountsFormat16(state.from.cardNumber)}
          label="from"
          name={state.from.cardEmbossedName}
          nameMode="body1"
          numberMode="caption"
          testID="BCReviewScreenTransferFromValue"
        />
        <Divider {...testProps('BCReviewScreenDivider1')} />
        <InstallmentDetail
          label="principal amount"
          value={toText(
            'PHP ' + numberFormatClean(state.amount),
            'BCReviewScreenTransferTo',
          )}
          {...testProps('BCReviewScreenTransferTo')}
        />
        <InstallmentDetail
          label="monthly amortization"
          value={toText(
            'PHP ' + numberFormatClean(state.paymentTerms.monthlyAmortization),
            'BCReviewScreenMonthlyAmortization',
          )}
          {...testProps('BCReviewScreenMonthlyAmortization')}
        />
        <InstallmentDetail
          label="monthly add-on rate"
          value={toText(
            state.paymentTerms.interestRate,
            'BCReviewScreenInterestRate',
          )}
          {...testProps('BCReviewScreenInterestRate')}
        />
        <InstallmentDetail
          label="effective annual interest rate"
          value={toText(
            formatEAIR(state.paymentTerms.annualRate),
            'BCReviewScreenEffectiveAnnualInterestRate',
          )}
          {...testProps('BCReviewScreenEffectiveAnnualInterestRate')}
        />
        <InstallmentDetail
          label="payment period"
          value={toText(
            state.paymentTerms.term + ' months',
            'BCReviewScreenPaymentPeriod',
          )}
          {...testProps('BCReviewScreenPaymentPeriod')}
        />
        <InstallmentDetail
          label="total installment amount"
          value={toText(
            'PHP ' +
              numberFormatClean(state.paymentTerms.totalInstallmentAmount),
            'BCReviewScreenTotalInstallmentAmount',
          )}
          {...testProps('BCReviewScreenTotalInstallmentAmount')}
        />
      </View>
      <Container marginTop={12} marginBottom={20}>
        <Text mode="body2" {...testProps('BCReviewScreenFooterMessageId')}>
          By clicking ‘confirm’, you have read and agreed to the{' '}
          <Text
            {...testProps('BCReviewScreenFooterTermsConditionText')}
            mode="link1"
            themeColor="SECONDARY_DEFAULT"
            onPress={() => navigation.navigate('TermsConditionScreen')}>
            terms and conditions
          </Text>
          , payment, fees applied to this transaction.
        </Text>
      </Container>
    </>
  );
};

export default ConfirmDetails;
