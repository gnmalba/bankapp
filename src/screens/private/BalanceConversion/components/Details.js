// @flow
import React from 'react';
import moment from 'moment';

import Layout from '@components/Layout';
import Divider from '@components/Divider';
import {StatusText} from '@composition/InstallmentItem/Styled';
import InstallmentDetail from '@composition/InstallmentDetail';

import type {BalanceConversionInstallmentType} from '@ducks/installments/types';
import {toText} from '@utils/helpers';
import {accountsFormat16} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatClean} from '@utils/numberInputs';
import {formatEAIR, strReplaceAll} from '@utils/installments';
import {
  FlexGrowScrollView,
  FlexGrowScrollViewChildWrapper,
} from '@utils/commonStyles';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';

const Details = (props: any): React$Node => {
  const item: BalanceConversionInstallmentType = props.route.params.item;

  return (
    <Layout padding={32}>
      <FlexGrowScrollView>
        <FlexGrowScrollViewChildWrapper>
          <InstallmentDetail
            small
            label="status"
            value={
              <StatusText mode="overline2" status={item.STATUS}>
                {item.STATUS}
              </StatusText>
            }
            {...testProps('BCStatus')}
          />

          <Divider {...testProps('BCDivider1')} />
          <CardDetailWithLabel
            label="from"
            numberMode="caption"
            small
            name={item.EmbossingName || 'N/A'}
            nameMode="inputerror"
            cardNumber={accountsFormat16(item.CARDNUMBER)}
            testID="BCFromValue"
          />
          <Divider {...testProps('BCDivider2')} />
          <InstallmentDetail
            small
            label="principal amount"
            value={toText(
              'PHP ' + numberFormatClean(item.AMOUNT),
              'BCPrincipalAmount',
              'inputerror',
            )}
            {...testProps('BCPrincipalAmount')}
          />
          <InstallmentDetail
            small
            label="monthly amortization"
            value={toText(
              'PHP ' + numberFormatClean(item.MONTHLYAMORTIZATION),
              'BCMonthlyAmortization',
              'inputerror',
            )}
            {...testProps('BCMonthlyAmortization')}
          />
          <InstallmentDetail
            small
            label="monthly add-on rate"
            value={toText(item.RATE + ' %', 'BCMonthlyAddOnRate', 'inputerror')}
            {...testProps('BCMonthlyAddOnRate')}
          />
          <InstallmentDetail
            small
            label="effective annual interest rate"
            value={toText(
              formatEAIR(item.EAIR),
              'BCEffectiveAnnualInterestRate',
              'inputerror',
            )}
            {...testProps('BCEffectiveAnnualInterestRate')}
          />
          <InstallmentDetail
            small
            label="payment period"
            value={toText(
              `${item.TERM} months`,
              'BCEffectiveAnnualInterestRate',
              'inputerror',
            )}
            {...testProps('BCPaymentPeriod')}
          />
          <InstallmentDetail
            small
            label="total installment amount"
            value={toText(
              'PHP ' + numberFormatClean(item.TOTALINSTALLMENTAMOUNT),
              'BCTotalInstallmentAmount',
              'inputerror',
            )}
            {...testProps('BCTotalInstallmentAmount')}
          />
          <Divider {...testProps('BCDivider3')} />
          <InstallmentDetail
            small
            label="reference number"
            value={toText(
              strReplaceAll(item.REFERENCENUMBER, '', '-'),
              'BCReferenceNumber',
              'inputerror',
            )}
            {...testProps('BCReferenceNumber')}
          />
          <InstallmentDetail
            small
            label="date and time"
            value={toText(
              moment(item.TRANSTATUS_DATECREATED).format(
                'MMM-DD-yyyy [\n]h:mm A [(GMT +8)]',
              ),
              'BCDateAndTime',
              'inputerror',
            )}
            {...testProps('BCDateAndTime')}
          />
        </FlexGrowScrollViewChildWrapper>
      </FlexGrowScrollView>
    </Layout>
  );
};

export default Details;
