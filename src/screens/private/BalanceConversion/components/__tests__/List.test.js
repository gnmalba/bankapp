import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

import List from '../List';

describe('List', () => {
  it('Should work as expected', () => {
    const mockStore = {
      installments: {
        balanceTransfer: {
          paymentTermsList: [],
        },
        balanceConversion: {
          paymentTermsList: [],
        },
        cash2Go: {
          paymentTermsList: [],
        },
        banks: [],
        formLoading: false,
        allInstallmentsLoading: false,
        allInstallments: {},
        sortedBy: {bt: 0, bc: 0, c2g: 0},
        filters: {},
        information: {
          visible: false,
          loading: false,
          data: null,
          isInitial: false,
        },
        currentInstallmentType: '',
        currentFilterInstallmentType: '',
        initialized: false,
        leavePromptVisible: false,
        isFormDirty: false,
        saveInstallmentsError: null,
        paymentTermsError: false,
        showReviewScreen: false,
        selectedCredit: null,
        rootScreen: '',
      },
    };

    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <MockProvider store={mockStore}>
        <ThemeProvider theme={DARK_THEME}>
          <List />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
