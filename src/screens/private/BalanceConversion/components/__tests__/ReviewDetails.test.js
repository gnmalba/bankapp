import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

import ReviewDetails from '../ReviewDetails';

describe('ReviewDetails', () => {
  it('Should work as expected', () => {
    const mockProps = {
      state: {
        from: {cardNumber: '', cardEmbossedName: ''},
        paymentTerms: {
          monthlyAmortization: '',
          interestRate: '',
          annualRate: '',
          term: '',
          totalInstallmentAmount: '',
        },
        subject: '',
        amount: '',
        dateTime: '',
        responseData: {
          Code: '',
        },
      },
    };
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ReviewDetails {...mockProps} />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
