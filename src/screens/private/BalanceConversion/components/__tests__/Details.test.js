import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

import Details from '../Details';

describe('Details', () => {
  it('Should work as expected', () => {
    const mockProps = {
      route: {
        params: {
          item: {
            AMOUNT: '',
            CARDNUMBER: '',
            CARDSERNO: '',
            CURRENCY: '',
            DATECREATED: '',
            EAIR: '',
            EmbossingName: '',
            INSTALLMENTSHORTCODE: '',
            ISBUSINESSRULEPASS: '',
            MONTHLYAMORTIZATION: '',
            RATE: '',
            REASONCODE: '',
            REFERENCENUMBER: '',
            SERNO: '',
            SOURCEAPP: '',
            STATUS: '',
            TERM: '',
            TOTALINSTALLMENTAMOUNT: '',
            TRANSTATUSSERNO: '',
            TRANSTATUS_DATECREATED: '',
          },
        },
      },
    };

    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <Details {...mockProps} />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
