/**
 * @format
 * @flow
 */

import {Divider} from '@components/Card/Card';
import InstallmentDetail from '@composition/InstallmentDetail';
import {ReviewDetailsContainer} from '@screens/private/ReviewDetailsScreen/Styled';
import {accountsFormat16} from '@utils/accountNumber';
import {toText} from '@utils/helpers';
import {formatEAIR, renderSuccessDetails} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';
import React from 'react';
import {numberFormatClean} from '@utils/numberInputs';
import CardDetailWithLabel from '@composition/CardDetailWithLabel';

export type PropTypes = {
  state: Object,
};

const ReviewDetails = ({state}: PropTypes): React$Node => {
  return (
    <>
      <ReviewDetailsContainer>
        <CardDetailWithLabel
          label="from"
          name={state.from.cardEmbossedName}
          cardNumber={accountsFormat16(state.from.cardNumber)}
          nameMode="inputerror"
          numberMode="caption"
          small
          testID="BCConfirmedScreenTransferFromValue"
        />
        <Divider {...testProps('BCConfirmedScreenDivider1')} />
        <InstallmentDetail
          label="principal amount"
          value={toText(
            'PHP ' + numberFormatClean(state.amount),
            'BCConfirmedScreenTransferFrom',
            'inputerror',
          )}
          small
          {...testProps('BCConfirmedScreenTransferFrom')}
        />
        <InstallmentDetail
          label="monthly amortization"
          value={toText(
            'PHP ' + numberFormatClean(state.paymentTerms.monthlyAmortization),
            'BCConfirmedScreenMonthlyAmortization',
            'inputerror',
          )}
          small
          {...testProps('BCConfirmedScreenMonthlyAmortization')}
        />
        <InstallmentDetail
          label="monthly add-on rate"
          value={toText(
            state.paymentTerms.interestRate,
            'BCConfirmedScreenInterestRate',
            'inputerror',
          )}
          small
          {...testProps('BCConfirmedScreenInterestRate')}
        />
        <InstallmentDetail
          label="effective annual interest rate"
          value={toText(
            formatEAIR(state.paymentTerms.annualRate),
            'BCConfirmedScreenEffectiveAnnualInterestRate',
            'inputerror',
          )}
          {...testProps('BCConfirmedScreenEffectiveAnnualInterestRate')}
        />
        <InstallmentDetail
          label="payment period"
          value={toText(
            `${state.paymentTerms.term} months`,
            'BCConfirmedScreenPaymentPeriod',
            'inputerror',
          )}
          small
          {...testProps('BCConfirmedScreenPaymentPeriod')}
        />
        <InstallmentDetail
          label="total installment amount"
          value={toText(
            'PHP ' +
              numberFormatClean(state.paymentTerms.totalInstallmentAmount),
            'BCConfirmedScreenMonthlyTotalInstallmentAmount',
            'inputerror',
          )}
          small
          {...testProps('BCConfirmedScreenMonthlyTotalInstallmentAmount')}
        />
        <Divider {...testProps('BCConfirmedScreenDivider2')} />
        {renderSuccessDetails(state, 'BCConfirmedScreen')}
      </ReviewDetailsContainer>
    </>
  );
};

export default ReviewDetails;
