/**
 * @format
 * @flow
 */

import React, {useCallback, useEffect, useState} from 'react';
import {BackHandler} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/core';

import type {PropsType} from './types';
import {StyledScrollView} from './Styled';

import Button from '@components/Button';
import Divider from '@components/Divider';
import Layout from '@components/Layout';

import TouchableDisplay from '@composition/TouchableDisplay';
import AmountInput from '@composition/AmountInput';
import {Display1} from '@composition/TouchableDisplay/Displays';
import {
  TextHolder,
  TotalAmountLabel,
  TotalAmountValue,
} from '../PaymentTermsScreen/Styled';
import {
  initializeInstallment,
  setRootScreen,
  updateAmount,
} from '@ducks/installments/actions';
import {installmentsSelector} from '@ducks/installments/selectors';
import {accountsFormat16} from '@utils/accountNumber';
import {
  AMOUNT_INPUT_ERRORS_INSTALLMENTS,
  getCleanCreditList,
  handleGoBack,
  MINIMUM_AMOUNT,
} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatClean, removeCommas} from '@utils/numberInputs';
import {creditSelector} from '@ducks/credit/selectors';

const isFromDisabled = (data: any[]): boolean => {
  return getCleanCreditList(data).length === 1;
};

const getFromCreditCardContent = (from): React$Node | null => {
  if (from) {
    const subtitle = 'CARD NUMBER - ' + accountsFormat16(from.cardNumber);
    return Display1(from.cardEmbossedName, subtitle, {
      title: 'From Acct Alias',
      subtitle: 'From Acct Card Num',
    });
  }

  return null;
};

const getPaymentTermsContent = (paymentTerms): React$Node | null => {
  if (paymentTerms) {
    const title = `PHP ${numberFormatClean(
      paymentTerms.monthlyAmortization,
    )} / month`;
    const subtitle = `for ${paymentTerms.term} months (${paymentTerms.interestRate} Monthly Add-on Rate)`;
    return Display1(title, subtitle, {
      title: 'Lbl Payment Terms Currency',
      subtitle: 'Lbl Payment Terms Rate',
    });
  }

  return null;
};

const BalanceConversion = (props: PropsType): React$Node => {
  const {navigation, route} = props;
  const installmentsState = useSelector(installmentsSelector);
  const {data: creditList} = useSelector(creditSelector);

  const {balanceConversion: bcState} = installmentsState;
  const {from, amount, paymentTerms} = bcState;

  const [amountError, setAmountError] = useState();
  const [isAmountFocused, setAmountFocused] = useState(false);

  const dispatch = useDispatch();

  useFocusEffect(
    useCallback(() => {
      dispatch(initializeInstallment('BC', false));
      if (route?.params?.fromSOA) {
        dispatch(setRootScreen('soa'));
      } else if (route?.params?.fromCC) {
        dispatch(setRootScreen('overview'));
      }
    }, [dispatch, route]),
  );

  useEffect(() => {
    setAmountError('');
  }, [from]);

  const handleNext = () => {
    navigation.navigate('ConfirmScreen');
  };

  const navigateToSelectCardScreen = () => {
    navigation.navigate('SelectCardScreen');
  };

  const handleFocus = () => {
    setAmountFocused(true);
    setAmountError('');
  };

  const navigateToPaymentTerms = () => {
    navigation.navigate('PaymentTermsScreen');
  };

  const getAmount = amount ? amount : '';
  const getTotalInstallmentAmount = paymentTerms
    ? 'PHP ' + numberFormatClean(paymentTerms.totalInstallmentAmount)
    : 'PHP';

  const isPaymentTermsDisabled = () => {
    if (from && amount && !amountError && !isAmountFocused) {
      return false;
    }
    return true;
  };

  const isButtonDisabled = () => {
    return !(from && amount && paymentTerms && !isAmountFocused);
  };

  const onAmountBlur = (value) => {
    setAmountFocused(false);

    const numValue = Number(removeCommas(value));

    if (numValue === 0) {
      setAmountError(AMOUNT_INPUT_ERRORS_INSTALLMENTS.EMPTY);
    } else if (numValue < MINIMUM_AMOUNT) {
      setAmountError('This value is below the minimum required amount.');
    } else if (from && numValue > from.outstandingBalance) {
      setAmountError(
        'This value is greater than your current outstanding balance.',
      );
    }
  };

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        handleGoBack({
          navigation: props.route?.params.navigation,
          route: props.route,
          installmentsState,
          dispatch,
        });

        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [props.route, installmentsState, dispatch]),
  );

  return (
    <Layout>
      <StyledScrollView padding={32}>
        <TouchableDisplay
          placeholder="Select a source credit card"
          onPress={navigateToSelectCardScreen}
          label={'From'}
          content={getFromCreditCardContent(from)}
          disabled={isFromDisabled(creditList)}
        />
        <AmountInput
          placeholder="Enter amount"
          label="Convert amount"
          currency="PHP"
          otherProps={{installmentType: 'BC'}}
          value={getAmount}
          dispatchAction={updateAmount}
          errorMessage={amountError}
          onFocus={handleFocus}
          onBlur={onAmountBlur}
        />
        <TouchableDisplay
          disabled={isPaymentTermsDisabled()}
          placeholder="Select terms of payment"
          label="Payment Terms"
          onPress={navigateToPaymentTerms}
          content={getPaymentTermsContent(paymentTerms)}
        />
        <Divider marginVertical={24} {...testProps('BCinitialScreenDivider')} />
        <TextHolder>
          <TotalAmountLabel
            mode="overline1"
            {...testProps('BCTotalInstallmentAmount')}>
            TOTAL INSTALLMENT{'\n'}AMOUNT
          </TotalAmountLabel>
          <TotalAmountValue
            mode="body1"
            {...testProps('BCTotalInstallmentAmountValue')}>
            {getTotalInstallmentAmount}
          </TotalAmountValue>
        </TextHolder>
        <Button
          title="Next"
          state={isButtonDisabled() ? 'disabled' : 'default'}
          type="primary"
          onPress={handleNext}
          {...testProps('BCinitialScreenNext')}
        />
      </StyledScrollView>
    </Layout>
  );
};

export default BalanceConversion;
