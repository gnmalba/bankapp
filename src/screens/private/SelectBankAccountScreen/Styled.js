/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View``;

export const StyledKeyboardAwareScrollView: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs((props) => ({
  showsVerticalScrollIndicator: false,
  keyboardShouldPersistTaps: 'handled',
}))`
  z-index: -1;
  margin-top: -12;
`;

export const ReminderContainer: ComponentType<any> = styled.View`
  height: ${verticalScale(100)}px;
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
  justify-content: center;
`;
