## SelectBankAccountScreen
SelectBankAccountScreen component.

# Usage
```js
import SelectBankAccountScreen from '@screens/private/SelectBankAccountScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
