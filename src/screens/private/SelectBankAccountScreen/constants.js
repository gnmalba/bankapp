export const ERRORS = {
  ACCOUNT_NUMBER: {
    REQUIRED: 'Please enter a valid Metrobank account number.',
    CORRECT_FORMAT_NON_MB:
      'Invalid input. Please enter a valid savings or checking account number.',
    CORRECT_FORMAT_MB:
      'Invalid input. Please enter a valid Metrobank account number.',
    INVALID_ACCOUNT_NUMBER: 'Please enter a valid Metrobank account number.',
    INVALID_ACCOUNT_NUMBER_NON_MB: 'Please enter a valid account number.',
    INVALID_SAVINGS_CHECKING_NUMBER:
      'Invalid input. Please enter a valid Metrobank savings or checking account number.',
  },
  ACCOUNT_NAME: {
    REQUIRED: 'Please enter account name',
    CORRECT_FORMAT: 'Invalid input. Special characters are not allowed.',
  },
};
