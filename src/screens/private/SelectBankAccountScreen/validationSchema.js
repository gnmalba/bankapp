import * as yup from 'yup';
import {ERRORS} from './constants';

export const initialValues = {
  accountNo: null,
  accountNameInput: null,
};

export const maxLengthCase1 = 17;
export const minLengthCase2 = 11;
export const maxLengthCase2 = 19;

export const validationSchema = (selectedValue) => {
  return yup.object().shape({
    accountNo: yup
      .string()
      .required(
        selectedValue === 1
          ? ERRORS.ACCOUNT_NUMBER.REQUIRED
          : ERRORS.ACCOUNT_NUMBER.INVALID_ACCOUNT_NUMBER_NON_MB,
      )
      .nullable()
      .max(
        selectedValue === 1 ? maxLengthCase1 : maxLengthCase2,
        ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT,
      )
      .test(
        'is-valid-credit-card',
        selectedValue === 1
          ? ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT_MB
          : ERRORS.ACCOUNT_NUMBER.CORRECT_FORMAT_NON_MB,
        (value) => {
          if (value?.length === maxLengthCase1 && selectedValue === 1) {
            return true;
          } else {
            if (selectedValue === 2 && value?.length >= minLengthCase2) {
              return true;
            }
          }
        },
      ),
    accountNameInput: yup
      .string()
      .required(ERRORS.ACCOUNT_NAME.REQUIRED)
      .nullable()
      .test('has-emoji', ERRORS.ACCOUNT_NAME.CORRECT_FORMAT, (value) => {
        // const regex = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/; // NOSONAR
        const regex = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>?]+/; // NOSONAR
        return value && !value.match(regex);
      }),
  });
};
