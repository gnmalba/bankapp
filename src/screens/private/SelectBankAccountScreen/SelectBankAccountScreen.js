/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';

import type {PropsType} from './types';
import type {Cash2GoDepositToType} from '@ducks/installments/types';

import {StyledKeyboardAwareScrollView, ReminderContainer} from './Styled';
import Layout from '@components/Layout';
import {EndButtonWrapper, HeaderContainer} from '@utils/commonStyles';
import Text from '@components/Text';
import Checkbox from '@components/Checkbox';
import {useDispatch, useSelector} from 'react-redux';
import {
  bankDataToSelectInput,
  DISBURSEMENT_OPTIONS_RADIO,
  filterOnlyPHP,
  getBankByName,
  getDepositToC2G,
} from '@utils/installments';
import CreditCardNumberInput from '@composition/CreditCardNumberInput';
import CreditCardAccountName from '@composition/CreditCardAccountName';
import SelectInput from '@components/SelectInput';
import Button from '@components/Button';
import {Separator} from '@components/Separator/Styled';
import {
  getBankListRequested,
  updateCash2GoTo,
} from '@ducks/installments/actions';
import {installmentsSelector} from '@ducks/installments/selectors';
import {depositsSelector} from '@ducks/deposit/selectors';
import {testProps} from '@utils/componentCustomProps';

import {useFormik} from 'formik';
import {initialValues, validationSchema} from './validationSchema';
import {accountNumberInputMask} from '@utils/accountNumber';
import EnrolledAccountsOption from './components/EnrolledAccountsOption';
import KeyboardDismissibleView from '@composition/KeyboardDismissibleView';
import Loader from '@composition/Loader';
import Reminder from '@components/Reminder';
import {
  CASH2GO_EXCEEDED_CAP_WARNING,
  CASH2GO_NON_METRO_MAX_CAP_AMOUNT,
} from '@utils/constants';
import KeyboardAwareView from '@components/KeyboardAwareView';

const getDisbursementOption = (selectedValue: number) => {
  switch (selectedValue) {
    case 0:
      return 'E';
    case 1:
      return 'U';
    case 2:
      return 'N';
    default:
      return 'N/A';
  }
};

const BankOptions = ({
  valueSetter,
  isChecked,
  setFieldValue,
  formik,
  hasExceededCapAmount,
  dispatch,
  navigation,
  allEnrolledAccounts,
}: {
  valueSetter: Function,
  isChecked: Function,
  setFieldValue: Function,
  formik: Object,
  hasExceededCapAmount: boolean,
  dispatch: Function,
  navigation: Object,
  allEnrolledAccounts: any[],
}): React$Node => {
  const optionsTitles: string[] = [
    'Enrolled Account/s',
    'Other Metrobank Account',
    'Non-Metrobank Account',
  ];

  const handleOneEnrolledAccount = (index) => {
    if (
      index === DISBURSEMENT_OPTIONS_RADIO.ENROLLED &&
      allEnrolledAccounts.length === 1
    ) {
      const onlyAcc = allEnrolledAccounts[0];

      const payload: any = {
        alias: onlyAcc.alias,
        cardNumber: onlyAcc.accountNo,
        ...onlyAcc,
      };

      dispatch(updateCash2GoTo(payload));
      navigation.navigate('Cash2GoScreen');
    }
  };

  const isRadioDisabled = (index) => {
    if (index > DISBURSEMENT_OPTIONS_RADIO.ENROLLED) {
      return hasExceededCapAmount;
    }

    if (index === DISBURSEMENT_OPTIONS_RADIO.ENROLLED) {
      return allEnrolledAccounts.length === 0;
    }

    return false;
  };

  return (
    <>
      {optionsTitles.map((title, index) => (
        <Checkbox
          title={title}
          isCheckbox={false}
          onPress={() => {
            handleOneEnrolledAccount(index);
            setFieldValue('accountNo', null);
            setFieldValue('accountNameInput', null);
            formik.resetForm();
            valueSetter(index);
          }}
          checked={isChecked(index)}
          {...testProps(`${index + 1}SelectBankRadioButton`)}
          disabled={isRadioDisabled(index)}
        />
      ))}
    </>
  );
};

const getCurrentBankName = (bank: any): string => {
  if (bank) {
    return bank.Name;
  }
  return '';
};

const createRequest = ({
  selectedValue,
  accountNameU,
  accountNameN,
  accountNumberU,
  accountNumberN,
  bank,
}): Cash2GoDepositToType => {
  return {
    disbursementOption: getDisbursementOption(selectedValue),
    destinationAccountName: selectedValue === 1 ? accountNameU : accountNameN,
    destinationAccountNumber:
      selectedValue === 1 ? accountNumberU : accountNumberN,
    destinationBankName: selectedValue === 2 ? getCurrentBankName(bank) : null,
  };
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const SelectBankAccountScreen = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const installmentsState = useSelector(installmentsSelector);

  const {banks, cash2Go, formLoading} = installmentsState;
  const {
    selectedValue: sValue,
    bankName,
    accountName,
    accountNumber,
  } = getDepositToC2G(cash2Go);

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [selectedValue, setSelectedValue] = useState(-1);
  const depositState = useSelector(depositsSelector);

  const [accountNameU, setAccountNameU] = useState('');
  const [accountNumberU, setAccountNumberU] = useState('');
  const [accountNameN, setAccountNameN] = useState('');
  const [accountNumberN, setAccountNumberN] = useState('');

  const [bank, setBank] = useState();

  const maxLengthCase1 = 17;
  const maxLengthCase2 = 19;
  const hasExceededCapAmount =
    cash2Go?.amount > CASH2GO_NON_METRO_MAX_CAP_AMOUNT;

  const allEnrolledAccounts = filterOnlyPHP(depositState.savingsAndDeposits);

  const formik = useFormik({
    initialValues: initialValues,
    enableReinitialize: true,
    validateOnMount: false,
    validateOnBlur: true,
    validationSchema: validationSchema(selectedValue),
  });

  const {values, errors, touched, setFieldValue, setFieldTouched} = formik;
  const accountNumberLabel = 'Account Number';
  const accountNumberPlaceholder = 'Enter 13-digit account number';
  const accountNameLabel = 'Account Name';
  const accountNamePlaceholder = 'Enter account name';

  useEffect(() => {
    dispatch(getBankListRequested('C2GO'));

    setSelectedValue(sValue);
    setAccountNameN(accountName);
    setAccountNameU(accountName);

    setAccountNumberN(accountNumber);
    setAccountNumberU(accountNumber);

    setFieldValue('accountNo', accountNumberInputMask(accountNumber));
    setFieldValue('accountNameInput', accountName);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const isButtonDisabled = () => {
    if (selectedValue === 1) {
      return !(
        accountNameU &&
        accountNumberU &&
        !errors?.accountNameInput &&
        !errors?.accountNo
      );
    } else if (selectedValue === 2) {
      return !(
        bank &&
        accountNumberN &&
        accountNameN &&
        !errors?.accountNameInput &&
        !errors?.accountNo
      );
    } else {
      return false;
    }
  };

  const onPress = () => {
    props.navigation.navigate('Cash2GoScreen');
  };

  const handleSubmit = () => {
    const request = createRequest({
      selectedValue,
      accountNameU,
      accountNameN,
      accountNumberU,
      accountNumberN,
      bank,
    });

    dispatch(updateCash2GoTo(request));
    props.navigation.navigate('Cash2GoScreen');
  };

  const removeSpace = (value) => {
    return value.replace(/\s+/g, '');
  };

  const handleChangeCreditNumber = (name: any) => (value) => {
    if (name === 'accountNo') {
      const unformattedAccountNo = removeSpace(value);
      setFieldValue(name, accountNumberInputMask(value));
      if (selectedValue === 1) {
        setAccountNumberU(unformattedAccountNo);
      } else if (selectedValue === 2) {
        setAccountNumberN(unformattedAccountNo);
      }
    } else {
      setFieldValue(name, value);
    }
  };

  const handleChangeAccountName = (name: any) => (value) => {
    if (name === 'accountNameInput') {
      setFieldValue(name, value);
      if (selectedValue === 1) {
        setAccountNameU(value);
      } else if (selectedValue === 2) {
        setAccountNameN(value);
      }
    } else {
      setFieldValue(name, value);
    }
  };

  const handleResetField = (name) => {
    setFieldValue(name, initialValues[name]);
  };

  const renderForm = () => {
    switch (selectedValue) {
      case 0:
        return (
          <EnrolledAccountsOption
            depositState={depositState}
            onPress={onPress}
            dispatchAction={updateCash2GoTo}
            navigation={props.navigation}
          />
        );

      case 1:
        return (
          <>
            <CreditCardAccountName
              accountNameInput={values.accountNameInput}
              label={accountNameLabel}
              placeholderText={accountNamePlaceholder}
              handleInputChange={handleChangeAccountName}
              handleResetField={handleResetField}
              handleSetFieldTouched={setFieldTouched}
              error={errors?.accountNameInput}
              isTouched={touched?.accountNameInput}
            />
            <Separator space={25} />
            <CreditCardNumberInput
              accountNo={values.accountNo}
              label={accountNumberLabel}
              placeholderText={accountNumberPlaceholder}
              handleInputChange={handleChangeCreditNumber}
              handleResetField={handleResetField}
              handleSetFieldTouched={setFieldTouched}
              error={errors?.accountNo}
              isTouched={touched?.accountNo}
              maxLength={maxLengthCase1}
            />
          </>
        );

      case 2:
        return (
          <>
            <SelectInput
              data={bankDataToSelectInput(banks)}
              placeholder="Select bank"
              setter={setBank}
              selected={getBankByName(bankDataToSelectInput(banks), bankName)}
              label="Bank"
              openSetter={setDropdownOpen}
              isOpen={dropdownOpen}
            />

            <StyledKeyboardAwareScrollView>
              <CreditCardAccountName
                accountNameInput={values.accountNameInput}
                label={accountNameLabel}
                placeholderText={accountNamePlaceholder}
                handleInputChange={handleChangeAccountName}
                handleResetField={handleResetField}
                handleSetFieldTouched={setFieldTouched}
                error={errors?.accountNameInput}
                isTouched={touched?.accountNameInput}
              />
              <Separator space={25} />
              <CreditCardNumberInput
                accountNo={values.accountNo}
                label={accountNumberLabel}
                placeholderText={accountNumberPlaceholder}
                handleInputChange={handleChangeCreditNumber}
                handleResetField={handleResetField}
                handleSetFieldTouched={setFieldTouched}
                error={errors?.accountNo}
                isTouched={touched?.accountNo}
                maxLength={maxLengthCase2}
              />
            </StyledKeyboardAwareScrollView>
          </>
        );

      default:
        return <></>;
    }
  };

  const isChecked = (currentKey: number) => selectedValue === currentKey;
  return (
    <Layout padding={32}>
      {formLoading ? (
        <Loader isVisible />
      ) : (
        <KeyboardAwareView>
          <KeyboardDismissibleView>
            <HeaderContainer marginBottom={41}>
              <Text mode="body1" {...testProps('SelectBankAccountTitle')}>
                Select a bank account.
              </Text>
            </HeaderContainer>
            <BankOptions
              valueSetter={setSelectedValue}
              isChecked={isChecked}
              formik={formik}
              setFieldValue={setFieldValue}
              hasExceededCapAmount={hasExceededCapAmount}
              dispatch={dispatch}
              allEnrolledAccounts={allEnrolledAccounts}
              navigation={props.navigation}
            />

            {hasExceededCapAmount && (
              <ReminderContainer>
                <Reminder
                  colorTheme={'yellow'}
                  text={CASH2GO_EXCEEDED_CAP_WARNING}
                  {...testProps('ExceededCapWarningReminder')}
                />
                <Separator space={20} />
              </ReminderContainer>
            )}

            {renderForm()}
            <Separator space={20} />
            {selectedValue !== 0 && selectedValue !== -1 && (
              <EndButtonWrapper>
                <Button
                  title="Confirm"
                  state={isButtonDisabled() ? 'disabled' : 'default'}
                  type="primary"
                  onPress={handleSubmit}
                  {...testProps('ConfirmBtn')}
                  labelTestID="id_confirm"
                />
              </EndButtonWrapper>
            )}
          </KeyboardDismissibleView>
        </KeyboardAwareView>
      )}
    </Layout>
  );
};

export default SelectBankAccountScreen;
