// @flow

import React, {useState} from 'react';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import SearchBox from '@composition/SearchBox';
import CardList from '@screens/private/SelectCreditCardWithSearchScreen/components/CardList';
import {filterOnlyPHP, toCardListDeposits} from '@utils/installments';
import type {Deposits} from '@ducks/deposit/types';
import {Separator} from '@components/Separator/Styled';

type PropTypes = {
  depositState: Deposits,
  onPress: Function,
  dispatchAction: Function,
  navigation: {
    navigate: (screen: string) => void,
  },
};

const EnrolledAccountsOption = (props: PropTypes): React$Node => {
  const {depositState, onPress, dispatchAction} = props;
  const allAccounts = toCardListDeposits(
    filterOnlyPHP(depositState.savingsAndDeposits),
  );
  const [value, setValue] = useState();
  const [list, setList] = useState(allAccounts);

  const handleChange = (text) => {
    setValue(text);
    const filteredList = allAccounts.filter((acc) => {
      if (text === '') {
        return true;
      }
      if (
        acc.alias.toUpperCase().includes(text.toUpperCase()) ||
        acc.accountNo.includes(text)
      ) {
        return true;
      }
      return false;
    });

    setList(filteredList);
  };

  return (
    <>
      <SearchBox
        placeholder="Search"
        value={value}
        setValue={handleChange}
        onChange={handleChange}
      />

      <Separator space={15} />
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <CardList
          list={list}
          onPress={onPress}
          dispatchAction={dispatchAction}
          balanceLabel={'AVAILABLE BALANCE'}
          shouldMask={false}
          isCASA
        />
      </KeyboardAwareScrollView>
    </>
  );
};

export default EnrolledAccountsOption;
