## ActivateCardSuccess
ActivateCardSuccess component.

# Usage
```js
import ActivateCardSuccess from '@screens/private/ActivateCardSuccess';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
