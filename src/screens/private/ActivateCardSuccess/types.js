/**
 * @flow
 */

export type PropsType = {
  navigation: {
    navigate: (route: string, params?: Object) => void,
  },
};
