/**
 * @format
 * @flow
 */

import React from 'react';

import {Image} from 'react-native';

import styled from 'styled-components/native';

import Layout from '@components/Layout';
import Text from '@components/Text';
import Button from '@components/Button';

import {scale, verticalScale} from '@utils/scale';

import type {PropsType} from './types';
import {useSelector, useDispatch} from 'react-redux';

import {getCardActivationState} from '@ducks/card-activation/selectors';
import {creditSelector} from '@ducks/credit/selectors';
import {resetActivationSuccess} from '@ducks/card-activation/actions';
import {testProps} from '@utils/componentCustomProps';
import {navigate} from '@navigations/service';

const checkedIcon = require('@assets/images/checked.png');

const StyledCardActivationDetailsText = styled(Text).attrs((props) => ({
  mode: 'body4',
  customColor: props.theme.color.TEXT_200_MEDIUIM,
}))``;

/**
 * TBC
 * Should change to svg
 */
const StyledCheckedImage = styled(Image).attrs((props) => ({
  source: checkedIcon,
  resizeMode: 'contain',
}))`
  height: 70px;
  width: 58px;
  align-self: center;
`;

const StyledMainContainer = styled.View`
  flex: 1;
  justify-content: center;
  padding-horizontal: ${scale(20)}px;
`;

const StyledTextContainer = styled.View`
  padding: ${scale(20)}px;
  display: flex;
  align-items: center;
  margin: ${verticalScale(10)}px;
`;

const StyledTextTitle = styled(Text).attrs({
  mode: 'h2',
})`
  margin-bottom: ${verticalScale(5)}px;
`;

const ActivateCardSuccess = ({navigation}: PropsType): React$Node => {
  const {activatedCard} = useSelector(getCardActivationState);
  const credit = useSelector(creditSelector);

  const dispatch = useDispatch();
  return (
    <Layout>
      <StyledMainContainer>
        <StyledCheckedImage />
        <StyledTextContainer>
          <StyledTextTitle>Card Activated!</StyledTextTitle>
          <StyledCardActivationDetailsText>
            You’ve successfully activated your
          </StyledCardActivationDetailsText>
          <StyledCardActivationDetailsText>
            Metrobank card!
          </StyledCardActivationDetailsText>
        </StyledTextContainer>
        <Button
          title="Go to my card details"
          type="primary"
          state="default"
          {...testProps('activateCardSuccessCardDetailsId')}
          onPress={() => {
            if (activatedCard) {
              const account = credit.data.find(
                (row) => row.cardSerNo === activatedCard,
              );

              navigate('CreditScreenOverview', {
                id: account.cardSerNo,
                alias: account.alias,
              });

              dispatch(resetActivationSuccess());
            }
          }}
        />
        <Button
          title="Go back to dashboard"
          type="primary"
          state="reset"
          {...testProps('activateCardSuccessGobackId')}
          onPress={() => navigation.navigate('Dashboard')}
        />
      </StyledMainContainer>
    </Layout>
  );
};

export default ActivateCardSuccess;
