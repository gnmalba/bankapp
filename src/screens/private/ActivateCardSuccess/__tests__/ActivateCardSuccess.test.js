import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import ActivateCardSuccess from '../ActivateCardSuccess';

import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

describe('ActivateCardSuccess', () => {
  it('Should work as expected', () => {
    const navigation = {navigate: jest.fn()};
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider
        store={{
          cardActivation: {
            activatedCard: null,
          },
          credit: {
            creditList: {
              data: [],
            },
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ActivateCardSuccess navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('should work when cliking Go back', async () => {
    const navigation = {navigate: jest.fn()};
    // Edit the args inside render and provide props.
    const {getByTestId} = render(
      <MockProvider
        store={{
          cardActivation: {
            activatedCard: null,
          },
          credit: {
            creditList: {
              data: [],
            },
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ActivateCardSuccess navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    const button = await getByTestId('activateCardSuccessGobackId');
    expect(button).toBeTruthy();
    fireEvent(button, 'press');
  });
  it('should work when clik on Card Details ', async () => {
    const navigation = {navigate: jest.fn()};
    // Edit the args inside render and provide props.
    const {getByTestId} = render(
      <MockProvider
        store={{
          cardActivation: {
            activatedCard: null,
          },
          credit: {
            creditList: {
              data: [],
            },
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ActivateCardSuccess navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    const button = await getByTestId('activateCardSuccessCardDetailsId');
    // fireEvent.press(getByText('activateCardSuccessGobackId'));
    expect(button).toBeTruthy();
    fireEvent(button, 'press');
  });
});
