/**
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity} from 'react-native';

// Components
import Header from '@components/Header';
import Text from '@components/Text';

// Types
import type {PropsType} from './types';
export default function BillerDetailsScreen(props: PropsType): React$Node {
  return (
    <>
      <Header
        title="Biller Details"
        onBackClick={() => props.navigation.pop()}
        isBackButton
      />

      <TouchableOpacity
        onPress={() => props.navigation.navigate('EditBillerDetails')}>
        <Text>BillerDetailsScreen</Text>
      </TouchableOpacity>
    </>
  );
}
