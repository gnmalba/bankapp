/**
 * @flow
 */

// React modules
import React from 'react';
import {View} from 'react-native';

// Components
import Header from '@components/Header';
import Text from '@components/Text';

// Types
import type {PropsType} from './types';

export default function EditBillerDetailsScreen(props: PropsType): React$Node {
  return (
    <>
      <Header
        title="Edit Details"
        onBackClick={() => props.navigation.pop()}
        isBackButton={true}
      />

      <View>
        <Text>EditBillerDetailsScreen</Text>
      </View>
    </>
  );
}
