/**
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import type {PropsType} from './types';
import {} from './Styled';

// Components
import Layout from '@components/Layout';
import Text from '@components/Text';

const AllBillersTab = (props: PropsType): React$Node => {
  return (
    <Layout padded>
      <View>
        <Text>AllBillersTab</Text>
      </View>
    </Layout>
  );
};

export default AllBillersTab;
