## AllBillersTab
AllBillersTab component.

# Usage
```js
import AllBillersTab from '@screens/private/AllBillersTab';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
