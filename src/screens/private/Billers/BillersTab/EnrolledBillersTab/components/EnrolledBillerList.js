/**
 * @format
 * @flow
 */

// React modules
import React, {useState} from 'react';
import {TouchableOpacity, View} from 'react-native';

// Components
import Text from '@components/Text';
import Separator from '@components/Separator';
import ListEmptyContent from './ListEmptyContent';

// Styles
import {
  CardContainer,
  CircleItem,
  StyledIcon,
  ScrollBarContainer,
  ScrollIndicator,
  ScrollLine,
  ScrollLineContainer,
  IconAndDetailsContainer,
  IconAndDetailsSubContainer,
  LastTransactionContainer,
  SubLabel,
  IconContainer,
  BillerList,
} from '../Styled';

// Types
import type {Biller} from '@ducks/billers/types';
type PropsType = {
  data: Array<Biller>,
  navigation: Object,
};

// Utils
import {formatDate} from '@utils/date';

export default function EnrolledBillerList({
  data,
  navigation,
}: PropsType): React$Node {
  //TODO: Map icons

  /* start - Custom scrollbar variables */
  const [contentOffset, setContentOffset] = useState({x: 0, y: 0});
  const [scrollSize, setScrollSize] = useState(0);
  const [scrollViewHeight, setScrollViewHeight] = useState(4);
  const scrollViewHeightPercentage = 12;
  const scrollPerc =
    (contentOffset.y / (scrollSize - scrollViewHeight)) *
    (100 - scrollViewHeightPercentage);
  /* end - Custom scrollbar variables */

  const renderLastTrnxDate = (date) =>
    date ? formatDate(date, 'MM/DD/YYYY') : 'N/A';

  const _renderItem = ({item, index}: {item: Object, index: number}) => {
    // const _renderItem = ({item, index}: {item: Biller, index: number}) => {
    console.log('item: ', item);
    return (
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => {
          //TODO: dispatch selected biller
          navigation.navigate('ManageBillersStack');
        }}>
        <CardContainer>
          <IconAndDetailsContainer>
            <IconAndDetailsSubContainer>
              <CircleItem size={32}>
                <Text>I</Text>
              </CircleItem>

              <View>
                <Text mode="body1">{item.billerAlias}</Text>

                <SubLabel mode="overline2">{item.category.name}</SubLabel>
              </View>
            </IconAndDetailsSubContainer>

            <IconContainer>
              <StyledIcon />
            </IconContainer>
          </IconAndDetailsContainer>

          <Separator space={14} />

          <LastTransactionContainer>
            <SubLabel>LAST TRANSACTION</SubLabel>

            <Text mode="body2">
              {renderLastTrnxDate(item.lastTransactiondate)}
            </Text>
          </LastTransactionContainer>
        </CardContainer>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <BillerList
        data={data}
        renderItem={_renderItem}
        ListEmptyComponent={<ListEmptyContent />}
        onScroll={({nativeEvent}) => {
          setContentOffset(nativeEvent.contentOffset);
        }}
        onContentSizeChange={(_, height) => {
          setScrollSize(height);
        }}
        onLayout={({nativeEvent}) => {
          setScrollViewHeight(nativeEvent.layout.height);
        }}
      />

      <ScrollBarContainer>
        <ScrollLineContainer>
          <ScrollLine height={scrollViewHeight}>
            <ScrollIndicator
              top={`${Number(scrollPerc || 0).toFixed(0)}%`}
              height={`${scrollViewHeightPercentage}%`}
            />
          </ScrollLine>
        </ScrollLineContainer>
      </ScrollBarContainer>
    </>
  );
}
