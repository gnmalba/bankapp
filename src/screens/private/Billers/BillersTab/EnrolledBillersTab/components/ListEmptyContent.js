/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Components
import Text from '@components/Text';
import Separator from '@components/Separator';

// Styles
import {
  EmptyContentContainer,
  EmptyContentSubLabelContainer,
  EmptyContentSubLabel,
} from '../Styled';

export default function ListEmptyContent(): React$Node {
  return (
    <EmptyContentContainer>
      <Text mode="h3">No billers found.</Text>

      <Separator space={14} />

      <EmptyContentSubLabelContainer>
        <EmptyContentSubLabel>
          There are no billers enrolled to your account. Try enrolling a new
          biller or select the ‘All billers’ option to see the full list of
          billers.
        </EmptyContentSubLabel>
      </EmptyContentSubLabelContainer>
    </EmptyContentContainer>
  );
}
