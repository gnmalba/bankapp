/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

// Styles
import {CardContainer, StyledSkeletonPlaceholder} from '../Styled';

export default function ListLoader(): React$Node {
  // TODO: Animate loader
  const length = [1, 2, 3];

  return length.map(() => (
    <CardContainer>
      <StyledSkeletonPlaceholder>
        <SkeletonPlaceholder.Item flexDirection="row" alignItems="center">
          <SkeletonPlaceholder.Item width={32} height={32} borderRadius={50} />

          <SkeletonPlaceholder.Item marginLeft={20}>
            <SkeletonPlaceholder.Item
              width={130}
              height={16}
              borderRadius={2}
            />

            <SkeletonPlaceholder.Item
              marginTop={6}
              width={155}
              height={8}
              borderRadius={2}
            />
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder.Item>

        <SkeletonPlaceholder.Item alignItems="flex-end" marginTop={22}>
          <SkeletonPlaceholder.Item width={116} height={8} borderRadius={2} />

          <SkeletonPlaceholder.Item
            width={76}
            height={12}
            borderRadius={2}
            marginTop={8}
          />
        </SkeletonPlaceholder.Item>
      </StyledSkeletonPlaceholder>
    </CardContainer>
  ));
}
