## EnrolledBillersTab
EnrolledBillersTab component.

# Usage
```js
import EnrolledBillersTab from '@screens/private/EnrolledBillersTab';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
