/**
 * @format
 * @flow
 */

// React Modules
import React from 'react';

// Third party libraries
import {useSelector} from 'react-redux';

// Components
import Layout from '@components/Layout';
import SearchBox from '@composition/SearchBox';
import EnrolledBillerList from './components/EnrolledBillerList';
import Separator from '@components/Separator';
import ListLoader from './components/ListLoader';

// Selectors
import {enrolledBillerSelector} from '@ducks/billers/selector';

// Types
import type {Biller} from '@ducks/billers/types';
type PropsType = {
  navigation: Object,
  billerListRef?: Object,
};

const EnrolledBillersTab = ({
  navigation,
  billerListRef,
}: PropsType): React$Node => {
  //TODO: Filter by search

  const isLoading = false; //TODO: get state from reducer
  const enrolledBillers: Array<Biller> = useSelector(enrolledBillerSelector);

  return (
    <Layout padding={24}>
      <SearchBox
        placeholder="Search"
        value=""
        maxLength={200}
        onChange={(text) => {}}
        setValue={() => {}}
      />

      <Separator space={24} />

      {isLoading ? (
        <ListLoader />
      ) : (
        <EnrolledBillerList data={enrolledBillers} navigation={navigation} />
      )}
    </Layout>
  );
};

export default EnrolledBillersTab;
