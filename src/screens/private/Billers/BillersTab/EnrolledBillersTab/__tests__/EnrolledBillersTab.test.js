import React from 'react';
import {render} from '@testing-library/react-native';
import EnrolledBillersTab from '../EnrolledBillersTab';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

const BILLERS = [
  {
    id: '4808',
    billerId: '1863',
    biller: 'AAA SOUTHEAST EQUITIES, INC.',
    billerIcon: '10',
    referenceNo: '210407141608765c4',
    subscriberName: 'GAB TEST',
    billerAlias: 'GAB TEST',
    // lastTransactiondate: '2021-04-07T06:16:08.843+0000',
    lastTransactiondate: null,
    currencyCode: 'PHP',
    reminder: null,
    createdDate: '2021-04-07T06:16:08.843+0000',
    updateDate: null,
    allowedChannel: 'MOBILE',
    category: {
      id: '193',
      name: 'Investments',
      code: 'INVESTMENTS',
    },
    fields: [
      {
        id: 5787,
        customerId: '6167',
        billerId: '1863',
        contactId: '210407141608765c4',
        fieldId: '6909',
        fieldLabel: 'Subscriber/ Account No.',
        fieldValue: '12345678901',
      },
      {
        id: 5788,
        customerId: '6167',
        billerId: '1863',
        contactId: '210407141608765c4',
        fieldId: '6910',
        fieldLabel: 'Reference No.',
        fieldValue: '12345678901',
      },
      {
        id: 5789,
        customerId: '6167',
        billerId: '1863',
        contactId: '210407141608765c4',
        fieldId: '6911',
        fieldLabel: 'Phone No.',
        fieldValue: '12345678901',
      },
    ],
  },
];
describe('EnrolledBillersTab', () => {
  const mockStore = () => ({
    billers: {
      billers: {
        enrolledBillers: BILLERS,
      },
    },
  });
  it('Should work as expected', () => {
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <EnrolledBillersTab />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
