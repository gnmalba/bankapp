/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {FlatList} from 'react-native';

// Third party libraries
import Entypo from 'react-native-vector-icons/Entypo';
import styled from 'styled-components/native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

// Components
import Text from '@components/Text';

// Utils
import {moderateScale, verticalScale, scale} from '@utils/scale';

export const CardContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: 8px;
  padding: ${moderateScale(18)}px;
  margin: 0px 0px ${verticalScale(16)}px 0px;
`;

export const CircleItem: ComponentType<any> = styled.View`
  height: ${(props) => scale(props.size)}px;
  width: ${(props) => scale(props.size)}px;
  border-radius: ${(props) => scale(props.size / 2)}px;
  background-color: ${(props) =>
    props.bgColor ? props.bgColor : props.theme.color.TEXT_300_LOW};
  justify-content: center;
  align-items: center;
  margin: 0px ${moderateScale(12)}px 0px 0px;
`;

export const StyledIcon: ComponentType<any> = styled(Entypo).attrs((props) => ({
  name: 'chevron-small-right',
  size: moderateScale(32),
  color: props?.theme?.card.availableBalance,
}))``;

export const IconAndDetailsContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const IconAndDetailsSubContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  flex: 1;
`;

export const StyledBillerLoader: ComponentType<any> = styled.View`
  background-color: #1e3b5a; /* TODO: Confirm light mode color */
  border-radius: 2;
  height: ${(props) => moderateScale(props.height)};
  width: ${(props) => moderateScale(props.width)};
`;

export const LastTransactionContainer: ComponentType<any> = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
`;

export const SubLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'overline2',
}))`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const IconContainer: ComponentType<any> = styled.View`
  align-items: flex-end;
  margin-right: ${moderateScale(-8)};
`;

export const EmptyContentContainer: ComponentType<any> = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  margin-top: ${verticalScale(-120)}px;
`;

export const EmptyContentSubLabelContainer: ComponentType<any> = styled.View`
  margin-right: ${moderateScale(40)};
  margin-left: ${moderateScale(40)};
`;

export const EmptyContentSubLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body2',
  }),
)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  text-align: center;
`;

export const BillerList: ComponentType<any> = styled(FlatList).attrs(
  (props) => ({
    contentContainerStyle: {flexGrow: 1},
    bounces: false,
    showsVerticalScrollIndicator: false,
  }),
)``;

export const StyledSkeletonPlaceholder: ComponentType<any> = styled(
  SkeletonPlaceholder,
).attrs((props) => ({
  highlightColor: props.theme.skeletonPlaceholder.highlightColor,
  backgroundColor: props.theme.skeletonPlaceholder.backgroundColor,
  borderRadius: '2px',
}))``;

/* start - SCROLLVIEW */
export const ScrollBarContainer: ComponentType<any> = styled.View`
  position: absolute;
  justify-content: flex-end;
  right: ${scale(10)}px;
  top: ${verticalScale(80)}px;
`;

export const ScrollLineContainer: ComponentType<any> = styled.View`
  border-top-width: 0;
  position: absolute;
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
  margin-top: ${verticalScale(25)}px;
`;

export const ScrollLine: ComponentType<any> = styled.View`
  position: absolute;
  width: ${scale(4)}px;
  border-radius: 16px;
  z-index: 11;
  background-color: ${(props) => props.theme.color.TEXT_300_LOW};
  height: ${(props) => props.height};
`;

export const ScrollIndicator: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
  border-radius: 16px;
  top: ${(props) => props.top};
  height: ${(props) => props.height};
`;
/* end - SCROLLVIEW */
