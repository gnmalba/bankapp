/**
 * @flow
 */
import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {scale, verticalScale, moderateScale} from '@utils/scale';
import {Image, StyleSheet} from 'react-native';

export const CheckIconImage: ComponentType<any> = styled(Image).attrs(
  (props) => ({
    source: require('@assets/icons/lost-card-check-icon.png'),
  }),
)`
  width: ${moderateScale(50)}px;
  height: ${moderateScale(60)}px;
  resize-mode: contain;
  align-self: center;
  margin-vertical: ${scale(20)}px;
`;

export const ButtonReport: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(18)}px;
  background-color: ${(props) => props.theme.layout.backgroundColor};
  border-radius: ${moderateScale(4)}px;
`;

export const ButtonCancel: ComponentType<any> = styled.View`
  padding: ${verticalScale(18)}px ${verticalScale(0)}px;
  align-items: center;
`;

export const TextBody: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(14)}px;
  text-align: center;
  margin-horizontal: ${scale(50)}px;
  margin-vertical: ${scale(8)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
`;

type StylesType = {
  textCenter: Object,
};

export const Styles: StylesType = StyleSheet.create({
  textCenter: {
    textAlign: 'center',
  },
});
