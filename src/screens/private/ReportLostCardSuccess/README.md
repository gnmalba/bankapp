## ReportLostCardSuccess
ReportLostCardSuccess component.

# Usage
```js
import ReportLostCardSuccess from '@screens/private/ReportLostCardSuccess';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
