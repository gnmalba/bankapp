import React from 'react';
import {render} from '@testing-library/react-native';
import ReportLostCardSuccess from '../ReportLostCardSuccess';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('ReportLostCardSuccess', () => {
  const mockStore = () => ({
    reportLostCard: {
      error: false,
      loading: false,
      card: {
        cardName: 'test',
        cardNumber: '1234',
        currencyCode: 'PHP',
        replacementFee: 2,
      },
      data: undefined,
    },
  });
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <ReportLostCardSuccess />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
