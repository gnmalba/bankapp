/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {View} from 'react-native';

// Third party libraries
import {useSelector} from 'react-redux';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Separator from '@components/Separator';

// Screens
import CardDetails from '../ReportLostCardConfirmation/CardDetails';

// Ducks

// Utils
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';
import type {PropsType} from './types';
import * as S from './Styled';

const ReportLostCardSuccess = (props: PropsType): React$Node => {
  const reportLostCard = useSelector((state) => state.reportLostCard);

  const {card} = reportLostCard;

  const handleReportOtherCard = () => {
    props.navigation.navigate('ReportLostCardList');
  };

  const renderContent = () => (
    <View>
      <S.CheckIconImage {...testProps('lostCardCheckImageTestId')} />
      <Text
        mode="h2"
        {...testProps('lostCardTextHeadingTestId')}
        style={[S.Styles.textCenter]}>
        Lost card report submitted
      </Text>
      <S.TextBody {...testProps('lostCardTextBodyTestId')}>
        {"You've successfully tagged the following card as lost"}
      </S.TextBody>
      <Separator space={verticalScale(16)} />
      <CardDetails card={card} />
    </View>
  );

  const renderBottom = () => (
    <View>
      <S.ButtonReport>
        <Button
          size="large"
          type="primary"
          state={'default'}
          title={'Report another lost card'}
          {...testProps('lostCardSuccesReportAnotherBtn')}
          onPress={handleReportOtherCard}
        />
      </S.ButtonReport>
      <S.ButtonCancel>
        <Button
          type="secondary"
          title="Go back to dashboard"
          titleThemeColor="TEXT_300_HIGH"
          {...testProps('lostCardSuccessGobackBtn')}
          onPress={() => props.navigation.navigate('Dashboard')}
        />
      </S.ButtonCancel>
    </View>
  );
  return (
    <SingleScreen
      contents={renderContent()}
      bottom={renderBottom()}
      spaceFromBottom={240}
    />
  );
};

export default ReportLostCardSuccess;
