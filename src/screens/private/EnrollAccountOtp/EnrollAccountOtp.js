/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useEffect} from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';

// Composition
import Passcode from '@screens/common/Passcode';
import ResendOtp from '@composition/EnrollAccount/ResendOtp';

// Components

// Ducks
import {
  enrollmentDeposit,
  enrollmentCredit,
  enrollmentYazz,
  updateEnrollmentState,
} from '@ducks/enrollment/actions';

// Utils
import {ACCOUNT_TYPES_ENUM} from '@utils/constants';
import {ERROR_CODES, ERRORS} from '../EnrollAccount/constants';
import type {PropsType} from './types';

const EnrollAccountOtp = (props: PropsType): React$Node => {
  const {theme} = props;
  const dispatch = useDispatch();
  const enrollment = useSelector((state) => state.enrollment);
  const {
    formValues: {accountNo, alias, nameOnCard, dateOfBirth},
    generatorId,
    mobileNumber,
    apiResponse,
    cardType,
  } = enrollment;

  const [otp, setOTP] = useState('');
  const [otpError, setOTPError] = useState('');

  const cpcInvalidOTPCondition = !!(
    apiResponse?.errorCode === ERROR_CODES.CPC.GENERAL_ERROR &&
    apiResponse?.message.includes(ERROR_CODES.CPC.INVALID_OTP_MESSAGE)
  );

  useEffect(() => {
    if (
      apiResponse?.errorCode === ERROR_CODES.CASA.INVALID_OTP ||
      cpcInvalidOTPCondition
    ) {
      setOTPError(ERRORS.OTP.INVALID);
    }
  }, [apiResponse]);

  const handleOnSubmit = (isResend = false) => {
    if (isResend) {
      setOTP('');
    }

    switch (cardType) {
      case ACCOUNT_TYPES_ENUM.DEPOSIT:
        handleDepositEnrollment(isResend);
        break;
      case ACCOUNT_TYPES_ENUM.CREDIT:
        handleCreditEnrollment(isResend);
        break;
      case ACCOUNT_TYPES_ENUM.YAZZ:
        handleYazzEnrollment(isResend);
        break;
    }

    if (!isResend) {
      dispatch(
        updateEnrollmentState({
          state: 'showProcessingScreen',
          value: true,
        }),
      );
      props.navigation.navigate('Processing Screen');
    }
  };

  const handleDepositEnrollment = (isResend) => {
    const otpValue = isResend ? '' : otp;
    dispatch(
      enrollmentDeposit({
        accountNo: accountNo,
        alias: alias,
        generatorId: generatorId,
        otp: otpValue,
      }),
    );
  };

  const handleCreditEnrollment = (isResend) => {
    const otpValue = isResend ? '' : otp;
    dispatch(
      enrollmentCredit({
        accountNo: accountNo,
        alias: alias,
        nameOnCard: nameOnCard,
        dateOfBirth: dateOfBirth,
        generatorId: generatorId,
        otp: otpValue,
      }),
    );
  };

  const handleYazzEnrollment = (isResend) => {
    const otpValue = isResend ? '' : otp;
    dispatch(
      enrollmentYazz({
        accountNo: accountNo,
        alias: alias,
        generatorId: generatorId,
        otp: otpValue,
      }),
    );
  };

  return (
    <Passcode
      enableAutomaticScroll={false}
      title="Verify your mobile number."
      description={`Please enter the 6-digit verification code we sent to the mobile number ${
        mobileNumber || ''
      } linked to your account being enrolled.`}
      btnTitle="Next"
      passcode={otp}
      onTextChange={(text) => {
        setOTP(text);
        setOTPError('');
      }}
      passcodeError={otpError}
      helperContent={
        <ResendOtp
          theme={theme}
          handleResendCode={() => handleOnSubmit(true)}
        />
      }
      onSubmit={() => {
        handleOnSubmit();
      }}
    />
  );
};

export default EnrollAccountOtp;
