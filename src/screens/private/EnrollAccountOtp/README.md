## EnrollAccountOtp
EnrollAccountOtp component.

# Usage
```js
import EnrollAccountOtp from '@screens/private/EnrollAccountOtp';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
