import React from 'react';
import {render, fireEvent, act, waitFor} from '@testing-library/react-native';
import EnrollAccountOtp from '../EnrollAccountOtp';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME, ACCOUNT_TYPES_ENUM} from '@utils/constants';

import {ERROR_CODES} from '../../EnrollAccount/constants';

jest.mock(
  'react-native/Libraries/Components/Touchable/TouchableOpacity',
  () => 'TouchableOpacity',
);

jest.mock(
  'react-native/Libraries/Components/Touchable/TouchableWithoutFeedback',
  () => 'TouchableWithoutFeedback',
);

const navigation = {
  navigate: jest.fn(() => {}),
};

const mockAppState = {
  enrollment: {
    formValues: {
      accountNo: '1112223334444',
      alias: 'User 1',
    },
    cardType: null,
    yazzCardBins: [],
    enrolledAccountFetching: false,
    loading: false,
    showProcessingScreen: false,
    isInitialCall: true,
    mobileNumber: null,
    apiResponse: null,
    publicKey: null,
    generatorId: null,
    showSnackbar: false,
    snackbarMessage: null,
    invalidOTPRetries: 0,
  },
};
describe('EnrollAccountOtp', () => {
  it('Should work as expected', () => {
    const store = {
      ...mockAppState,
    };
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <EnrollAccountOtp />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('onSubmit should work as expected', async () => {
    const store = {
      ...mockAppState,
    };
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <EnrollAccountOtp navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );

    await act(async () => {
      const pincodeComp = all.getByTestId('smoothInputTestId');
      await waitFor(() => fireEvent(pincodeComp, 'onChangeText', '111111'));
    });
    const button = all.getByTestId('passcodeTestIdButton');
    fireEvent.press(button);
  });

  it.each([
    {cardType: ACCOUNT_TYPES_ENUM.DEPOSIT},
    {cardType: ACCOUNT_TYPES_ENUM.CREDIT},
    {cardType: ACCOUNT_TYPES_ENUM.YAZZ},
  ])(
    'SendAnotherCode should render correctly when %o',
    async (additionalProps) => {
      const store = {
        ...mockAppState,
        enrollment: {
          ...mockAppState.enrollment,
          ...additionalProps,
        },
      };

      const all = render(
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <EnrollAccountOtp navigation={navigation} />
          </ThemeProvider>
        </MockProvider>,
      );

      const button = all.getByTestId('resendOTPSendAnotherCodeButton');
      fireEvent.press(button);
    },
  );

  it.each([
    {cardType: ACCOUNT_TYPES_ENUM.DEPOSIT},
    {cardType: ACCOUNT_TYPES_ENUM.CREDIT},
    {cardType: ACCOUNT_TYPES_ENUM.YAZZ},
  ])('should render correctly when %o', async (additionalProps) => {
    const store = {
      ...mockAppState,
      enrollment: {
        ...mockAppState.enrollment,
        ...additionalProps,
      },
    };

    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <EnrollAccountOtp navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    await act(async () => {
      const pincodeComp = all.getByTestId('smoothInputTestId');
      await waitFor(() => fireEvent(pincodeComp, 'onChangeText', '111111'));
    });
    const button = all.getByTestId('passcodeTestIdButton');
    fireEvent.press(button);
  });

  it.each([
    {
      apiResponse: {
        errorCode: ERROR_CODES.CPC.GENERAL_ERROR,
        message: ERROR_CODES.CPC.INVALID_OTP_MESSAGE,
      },
    },
    {
      apiResponse: null,
    },
  ])('should render correctly when %o', async (additionalProps) => {
    const store = {
      ...mockAppState,
      enrollment: {
        ...mockAppState.enrollment,
        ...additionalProps,
      },
    };

    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <EnrollAccountOtp navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
