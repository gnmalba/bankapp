import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';

import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

import ConfirmDetailsScreen from '../ConfirmDetailsScreen';

describe('ConfirmDetailsScreen', () => {
  it('Should work as expected', () => {
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <MockProvider
        store={{
          installments: {
            currentInstallmentType: '',
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ConfirmDetailsScreen {...testProps('confirmDetailsScreenTestId')} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

// describe('ConfirmDetailsScreen', () => {
//   it('Should work as expected', () => {});
// });
