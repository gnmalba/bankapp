/**
 * @format
 * @flow
 */

import React from 'react';
import {ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Layout from '@components/Layout';
import Text from '@components/Text';
import Button from '@components/Button';
import {installmentsSelector} from '@ducks/installments/selectors';
import {HeaderContainer, Subtitle} from '@utils/commonStyles';
import {testProps} from '@utils/componentCustomProps';

import BTConfirmDetails from '../BalanceTransfer/components/ConfirmDetails';
import BCConfirmDetails from '../BalanceConversion/components/ConfirmDetails';
import type {PropsType} from './types';
import {TitleContainer} from './Styled';
import C2GConfirmDetails from '../Cash2Go/components/ConfirmDetails';
import Separator from '@components/Separator';
import {clearInstallmentErrors} from '@ducks/installments/actions';
// import {
//   BalTranstermsConditionRequest,
//   BalConverttermsConditionRequest,
//   C2gotermsConditionRequest,
// } from '@ducks/termscondition/actions';
const ConfirmDetailsScreen = (props: PropsType): React$Node => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType} = installmentsState;

  const navigateToPasscodeScreen = () => {
    dispatch(clearInstallmentErrors());
    navigation.navigate('PasscodeScreen');
  };

  const handleCancel = () => {
    navigation.goBack();
  };

  // useEffect(() => {
  //   if (currentInstallmentType === 'BT') {
  //     dispatch(
  //       BalTranstermsConditionRequest({
  //         customerId,
  //         cardSerNo,
  //         installmentType,
  //       }),
  //     );
  //   } else if (currentInstallmentType === 'BC') {
  //     dispatch(
  //       BalConverttermsConditionRequest({
  //         customerId,
  //         cardSerNo,
  //         installmentType,
  //       }),
  //     );
  //   } else if (currentInstallmentType === 'C2GO') {
  //     dispatch(
  //       C2gotermsConditionRequest({customerId, cardSerNo, installmentType}),
  //     );
  //   }
  // }, [currentInstallmentType, dispatch]);

  const renderDetails = (): React$Node | null => {
    if (currentInstallmentType === 'BT') {
      return (
        <BTConfirmDetails
          state={installmentsState.balanceTransfer}
          navigation={navigation}
        />
      );
    } else if (currentInstallmentType === 'BC') {
      return (
        <BCConfirmDetails
          state={installmentsState.balanceConversion}
          navigation={navigation}
        />
      );
    } else if (currentInstallmentType === 'C2GO') {
      return (
        <C2GConfirmDetails
          state={installmentsState.cash2Go}
          navigation={navigation}
        />
      );
    }

    return null;
  };

  return (
    <Layout padding={32}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <HeaderContainer marginBottom={8}>
          <TitleContainer>
            <Text
              mode="h2"
              {...testProps(
                currentInstallmentType.toLowerCase() + 'ReviewScreenTitleId',
              )}>
              All Good?
            </Text>
          </TitleContainer>
          <Subtitle
            mode="body2"
            {...testProps(currentInstallmentType + 'ReviewScreenMessageId')}>
            Please review your application details before processing
          </Subtitle>
        </HeaderContainer>
        <Separator space={32} />
        {renderDetails()}

        <Button
          type="primary"
          title="Confirm"
          state="default"
          onPress={navigateToPasscodeScreen}
          {...testProps(currentInstallmentType + 'ReviewScreenConfirmButtonId')}
          labelTestID="id_confirm"
        />
        <Button
          onPress={handleCancel}
          type="primary"
          title="Cancel"
          titleThemeColor="CANCEL_BUTTON"
          {...testProps(currentInstallmentType + 'ReviewScreenCancelButtonId')}
          labelTestID="id_cancel"
        />
      </ScrollView>
    </Layout>
  );
};

export default ConfirmDetailsScreen;
