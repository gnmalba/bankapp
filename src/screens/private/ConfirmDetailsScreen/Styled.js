/**
 * @flow
 */

import {verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View``;

export const TitleContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(12)}px;
  align-items: ${(props) =>
    props.alignItems ? props.alignItems : 'flex-start'};
`;
