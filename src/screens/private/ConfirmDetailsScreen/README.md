## ConfirmDetailsScreen
ConfirmDetailsScreen component.

# Usage
```js
import ConfirmDetailsScreen from '@screens/private/ConfirmDetailsScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
