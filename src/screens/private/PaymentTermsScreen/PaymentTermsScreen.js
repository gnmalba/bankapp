/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {Pressable} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import Layout from '@components/Layout';
import Text from '@components/Text';
import RadioButton from '@components/Checkbox';
import Button from '@components/Button';

import {getPaymentTerms, updatePaymentTerms} from '@ducks/installments/actions';
import type {
  PaymentTermsRequest,
  PaymentTermsType,
} from '@ducks/installments/types';
import {installmentsSelector} from '@ducks/installments/selectors';

import {preparePaymentTermsRequest} from '@utils/installments';
import {HeaderContainer} from '@utils/commonStyles';
import {testProps} from '@utils/componentCustomProps';

import {
  Footer,
  PaymentTermItem,
  TermDetails,
  TermsContainer,
  TermTitle,
  TextContainer,
  Container,
  TextHolder,
  TotalAmountLabel,
  TotalAmountValue,
  RadioButtonContainer,
} from './Styled';
import type {PropsType} from './types';
import Loader from '@composition/Loader';
import {numberFormatClean} from '@utils/numberInputs';

// eslint-disable-next-line sonarjs/cognitive-complexity
const PaymentTermsScreen = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const installmentsState = useSelector(installmentsSelector);
  const {
    balanceTransfer,
    balanceConversion,
    cash2Go,
    currentInstallmentType,
    formLoading,
    paymentTermsError,
  } = installmentsState;

  const [selectedTerm, setSelectedTerm] = useState<PaymentTermsType | null>(
    null,
  );

  const getCurrentInstallmentsState = () => {
    if (currentInstallmentType === 'BT') {
      return balanceTransfer;
    } else if (currentInstallmentType === 'BC') {
      return balanceConversion;
    } else if (currentInstallmentType === 'C2GO') {
      return cash2Go;
    }
    return {};
  };

  const currentInstallmentsState = getCurrentInstallmentsState();

  useEffect(() => {
    const requestParams: PaymentTermsRequest = {
      installmentType: installmentsState.currentInstallmentType,
      ...preparePaymentTermsRequest(installmentsState),
    };

    dispatch(getPaymentTerms(requestParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (currentInstallmentsState.paymentTermsList.length > 0) {
      setSelectedTerm(currentInstallmentsState.paymentTermsList[0]);
    }
  }, [currentInstallmentsState.paymentTermsList]);

  // For pre-selecting payment term
  useEffect(() => {
    if (
      currentInstallmentsState.paymentTerms &&
      currentInstallmentsState.paymentTermsList
    ) {
      setSelectedTerm(currentInstallmentsState.paymentTerms);
    }
  }, [
    currentInstallmentsState.paymentTermsList,
    currentInstallmentsState.paymentTerms,
  ]);

  const handleCheckboxPress = (item) => {
    setSelectedTerm(item);
  };

  const isSelectedTerm = (currentKey) => {
    if (!selectedTerm) {
      return false;
    }

    return JSON.stringify(currentKey) === JSON.stringify(selectedTerm);
  };

  const handleSubmit = () => {
    if (selectedTerm) {
      dispatch(updatePaymentTerms(currentInstallmentType, selectedTerm));

      if (currentInstallmentType === 'BT') {
        props.navigation.navigate('BalanceTransferScreen');
      } else if (currentInstallmentType === 'BC') {
        props.navigation.navigate('BalanceConversionScreen');
      } else if (currentInstallmentType === 'C2GO') {
        props.navigation.navigate('Cash2GoScreen');
      }
    }
  };

  const getTotalInstallmentAmount = (): string => {
    if (selectedTerm) {
      return `PHP ${numberFormatClean(selectedTerm.totalInstallmentAmount)}`;
    }

    return '';
  };

  return (
    <>
      <Layout padding={32}>
        {formLoading ? (
          <Loader isVisible />
        ) : (
          <Container>
            <HeaderContainer marginBottom={41}>
              <Text mode="normalSemibold" {...testProps('PaymentTermsTitle')}>
                Select terms of payment.
              </Text>
            </HeaderContainer>
            {paymentTermsError ? (
              <>
                <Text
                  themeColor="TEXT_100_HIGH"
                  mode="body2"
                  {...testProps('NoPaymentTerms')}>
                  There are no payment terms.
                </Text>
              </>
            ) : (
              <TermsContainer>
                {currentInstallmentsState.paymentTermsList.map((item, i) => {
                  return (
                    <Pressable
                      key={i}
                      onPress={() => handleCheckboxPress(item)}
                      {...testProps('PaymentTermsRdoBtn_' + i)}>
                      <PaymentTermItem isSelected={isSelectedTerm(item)}>
                        <RadioButtonContainer>
                          <RadioButton
                            {...testProps('RdoBtnIcon_' + i)}
                            onPress={() => handleCheckboxPress(item)}
                            checked={isSelectedTerm(item)}
                            title=""
                            isCheckbox={false}
                          />
                        </RadioButtonContainer>
                        <TextContainer>
                          <TermTitle
                            isSelected={isSelectedTerm(item)}
                            mode="body1"
                            {...testProps('MonthlyAmmortizationValue_' + i)}>
                            PHP {numberFormatClean(item.monthlyAmortization)} /
                            month
                          </TermTitle>
                          <TermDetails
                            isSelected={isSelectedTerm(item)}
                            mode="caption"
                            {...testProps('AddOnRateValue_' + i)}>
                            for {item.term} months | {item.interestRate} Monthly
                            Add-on Rate
                          </TermDetails>
                        </TextContainer>
                      </PaymentTermItem>
                    </Pressable>
                  );
                })}
              </TermsContainer>
            )}
          </Container>
        )}
      </Layout>
      {!formLoading && !paymentTermsError && (
        <Footer>
          <TextHolder>
            <TotalAmountLabel
              mode="overline1"
              {...testProps('TotalInstallmentAmtLbl')}>
              TOTAL INSTALLMENT{'\n'}AMOUNT
            </TotalAmountLabel>
            <TotalAmountValue
              mode="body1"
              {...testProps('TotalInstallmentAmtValue')}>
              {getTotalInstallmentAmount()}
            </TotalAmountValue>
          </TextHolder>
          <Button
            title="Confirm"
            type="primary"
            state={selectedTerm ? 'default' : 'disabled'}
            onPress={handleSubmit}
            {...testProps('ConfirmBtn')}
            labelTestID="id_confirm"
          />
        </Footer>
      )}
    </>
  );
};

export default PaymentTermsScreen;
