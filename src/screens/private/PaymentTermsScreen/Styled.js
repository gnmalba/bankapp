/**
 * @flow
 */

import Text from '@components/Text';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {moderateScale, verticalScale} from '@utils/scale';
/**
 *   font-style: normal;
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${scale(20)}px;
  line-height: 24px;
 */

export const Container: ComponentType<any> = styled.View`
  height: 78%;
`;

export const TermsContainer: ComponentType<any> = styled.ScrollView``;

export const RadioButtonContainer: ComponentType<any> = styled.View`
  justify-content: center;
  top: ${verticalScale(8)}px;
`;

export const PaymentTermItem: ComponentType<any> = styled.View`
  padding: ${verticalScale(16)}px ${verticalScale(10)}px;
  border-width: 1px;
  margin: 0px 0px ${verticalScale(10)}px 0px;
  border-radius: 4px;
  border-color: ${(props) =>
    props.isSelected
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.TEXT_300_LOW};
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const TextContainer: ComponentType<any> = styled.View``;

export const TermTitle: ComponentType<any> = styled(Text).attrs((props) => ({
  customColor: props.isSelected
    ? props.theme.text.textColor
    : props.theme.color.TEXT_100_HIGH,
}))`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 400;
  font-size: ${moderateScale(18)}px;
  line-height: ${moderateScale(21.6)}px;
`;

export const TermDetails: ComponentType<any> = styled(Text)`
  font-style: normal;
  font-weight: 600;
  font-size: ${moderateScale(12)}px;
  line-height: ${moderateScale(15.1)}px;
  color: ${(props) =>
    props.isSelected
      ? props.theme.color.TEXT_100_HIGH
      : props.theme.color.TEXT_300_LOW};
`;

export const Footer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  padding: ${verticalScale(32)}px ${verticalScale(24)}px;
  position: absolute;
  width: 100%;
  bottom: 0;
`;

export const TextHolder: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  margin-bottom: ${verticalScale(25)}px;
`;

export const TotalAmountLabel: ComponentType<any> = styled(Text)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 700;
  font-size: ${moderateScale(12)}px;
  line-height: ${moderateScale(14.4)}px;
`;

export const TotalAmountValue: ComponentType<any> = styled(Text)`
  margin-left: auto;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;
