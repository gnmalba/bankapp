## PaymentTermsScreen
PaymentTermsScreen component.

# Usage
```js
import PaymentTermsScreen from '@screens/private/PaymentTermsScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
