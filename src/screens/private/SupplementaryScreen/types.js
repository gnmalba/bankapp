/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  route: {
    params: {
      id: string,
      primaryId: string,
      handleGoBack: (route: any) => void,
    },
  },
};
