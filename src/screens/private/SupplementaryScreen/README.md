## SupplementaryScreen
SupplementaryScreen component.

# Usage
```js
import SupplementaryScreen from '@screens/private/SupplementaryScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
