// React modules
import React from 'react';

// Third party libraries
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {NavigationContext} from '@react-navigation/native';

// Components
import SupplementaryScreen from '../SupplementaryScreen';

// Utils
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

jest.mock('@react-navigation/native', () => {
  return {
    ...jest.requireActual('@react-navigation/native'),
    useNavigation: () => ({
      navigate: jest.fn(),
    }),
  };
});

const navContext = {
  isFocused: () => true,
  addListener: jest.fn(() => jest.fn()),
};

describe('SupplementaryScreen', () => {
  const store = {
    credit: {
      supplementaryDetails: {
        data: {},
      },
    },
  };

  it('Should work as expected', () => {
    const all = render(
      <NavigationContext.Provider value={navContext}>
        <MockProvider store={store}>
          <ThemeProvider theme={DARK_THEME}>
            <SupplementaryScreen />
          </ThemeProvider>
        </MockProvider>
      </NavigationContext.Provider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
