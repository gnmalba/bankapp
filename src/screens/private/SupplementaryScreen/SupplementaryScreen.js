/**
 * @format
 * @flow
 */

import React, {useCallback, useState} from 'react';
import {BackHandler} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useFocusEffect} from '@react-navigation/core';

import type {PropsType} from './types';
import * as S from './Styled';
import Layout from '@components/Layout';
import {FlatList} from 'react-native';

import CardImageBackground from '@composition/CardImageBackground';
import CreditCardDetails from '@composition/CreditCardDetails';
import TransactionHistory from '@composition/TransactionHistory';
import Loader from '@composition/Loader';

const SupplementaryScreen = (props: PropsType): React$Node => {
  const {route} = props;
  const [refreshTime, setRefreshTime] = useState(0);
  const id = route?.params?.id;
  const primaryId = route?.params?.primaryId;
  const handleGoBack = route?.params?.handleGoBack;

  const dispatch = useDispatch();
  const supplementaryDetails = useSelector(
    (state) => state.credit.supplementaryDetails,
  );

  const supplementaryTransaction = useSelector(
    (state) => state.credit.supplementaryTransactionCurrent,
  );

  React.useEffect(() => {
    dispatch({
      type: 'REQUEST_SUPPLEMENTARY_DETAILS',
      payload: id,
    });
    dispatch({
      type: 'REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST',
      payload: {
        cardSerialNo: primaryId,
        cardOwners: id,
      },
    });
  }, [id, primaryId, dispatch]);

  useFocusEffect(
    useCallback(() => {
      setRefreshTime(Date.now());
      const onBackPress = () => {
        if (handleGoBack) {
          route.params.handleGoBack(route);

          return true;
        }

        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [route, handleGoBack]),
  );

  const renderScreen = () => (
    <>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps="always"
        enableOnAndroid>
        <S.MainContainer>
          <S.Container>
            <CardImageBackground
              data={supplementaryDetails.data}
              cardType="credit"
            />
            <CreditCardDetails data={supplementaryDetails.data?.data} />
          </S.Container>
        </S.MainContainer>
        <S.MainContainer>
          <TransactionHistory
            cardType="credit"
            data={supplementaryDetails.data.data}
            transaction={supplementaryTransaction}
            titleTransaction="Transaction History"
            creditSupplementary={true}
            primaryId={primaryId}
            id={id}
            refreshTime={refreshTime}
          />
        </S.MainContainer>
      </KeyboardAwareScrollView>
    </>
  );

  return (
    <Layout>
      <FlatList
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        data={supplementaryDetails.loader}
        renderItem={renderScreen}
        keyExtractor={(item, index) => index.toString()}
        ListEmptyComponent={() => <Loader isVisible={true} />}
      />
    </Layout>
  );
};

export default SupplementaryScreen;
