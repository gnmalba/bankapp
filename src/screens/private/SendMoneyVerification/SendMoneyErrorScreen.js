/**
 * @format
 * @flow
 */

import React from 'react';

// Components
import Button from '@components/Button';

// Screens
import ErrorScreen from '@screens/common/ErrorScreen';

// Utils
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';

const SendMoneyErrorScreen = (props: PropsType): React$Node => {
  const {error, theme} = props;
  const isMPINExceed = error && error?.code === 'invalid_mpin_exceeded';

  return (
    <ErrorScreen
      theme={theme}
      showBackButton
      headingAlignCenter={isMPINExceed}
      heading={
        isMPINExceed
          ? 'You have reached the maximum number of attempts. Please try again after (x) minutes.'
          : 'Oops! Something went wrong.'
      }
      details={
        error?.code
          ? error?.code === 'invalid_mpin_exceeded'
            ? null
            : error?.message
          : "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen."
      }
      isSystemError={error?.code ? false : true}
      handlePrimaryButton={() => props.navigation.pop()}
      handleSecondaryButton={() => props.navigation.navigate('Drawer')}
      buttonContents={
        <Button
          title="Go back"
          type="primary"
          state="default"
          {...testProps('sendMoneyVerificationErrorBtn')}
          onPress={() => props.navigation.pop()}
        />
      }
    />
  );
};

export default SendMoneyErrorScreen;
