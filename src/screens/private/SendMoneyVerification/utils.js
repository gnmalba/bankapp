/**
 * @flow
 */
import moment from 'moment';
import {extendMoment} from 'moment-range';

import FREQUENCY_TYPE from '@enums/frequency';

import {
  SCHEDULE_TYPE,
  SCHEDULE_END_TYPE,
  OCCURRENCE_TYPE,
} from '@screens/private/SendMoney/constants';
import type {FormValues} from '@screens/private/SendMoney/MainScreen/components/SendMoneyForm/types';

import {
  CONVERT_DAY_TO_DAYS_FACTOR,
  CONVERT_WEEK_TO_DAYS_FACTOR,
  CONVERT_MONTH_TO_DAYS_FACTOR,
  CONVERT_QUARTER_TO_DAYS_FACTOR,
  CONVERT_HALF_YEAR_TO_DAYS_FACTOR,
  CONVERT_YEAR_TO_DAYS_FACTOR,
} from './constants';

extendMoment(moment);

export const FREQUENCY_UNIT_OF_MEASUREMENT = {
  DAILY: 'days',
  WEEKLY: 'week',
  MONTHLY: 'month',
  QUARTERLY: 'quarter',
  SEMI_ANNUALLY: 'year',
  ANNUALLY: 'year',
};

export const getConversionFactor = (
  unit: $Values<typeof FREQUENCY_UNIT_OF_MEASUREMENT>,
): number => {
  switch (unit) {
    case FREQUENCY_UNIT_OF_MEASUREMENT.DAILY:
      return CONVERT_DAY_TO_DAYS_FACTOR;
    case FREQUENCY_UNIT_OF_MEASUREMENT.WEEKLY:
      return CONVERT_WEEK_TO_DAYS_FACTOR;
    case FREQUENCY_UNIT_OF_MEASUREMENT.MONTHLY:
      return CONVERT_MONTH_TO_DAYS_FACTOR;
    case FREQUENCY_UNIT_OF_MEASUREMENT.QUARTERLY:
      return CONVERT_QUARTER_TO_DAYS_FACTOR;
    case FREQUENCY_UNIT_OF_MEASUREMENT.SEMI_ANNUALLY:
      return CONVERT_HALF_YEAR_TO_DAYS_FACTOR;
    case FREQUENCY_UNIT_OF_MEASUREMENT.ANNUALLY:
      return CONVERT_YEAR_TO_DAYS_FACTOR;
    default:
      return 0;
  }
};

export const getNumberOfOccurrences = (
  frequency: $Keys<typeof FREQUENCY_UNIT_OF_MEASUREMENT> | null,
  startDateTime: string | null,
  endDate: string,
): number => {
  let startDate = moment().format('MM/DD/yyyy, hh:mm A');
  let startDatePlusThreeYears = moment().add(3, 'y');
  const unitOfMeasurement = FREQUENCY_UNIT_OF_MEASUREMENT[
    frequency || FREQUENCY_TYPE.DAILY
  ].toLowerCase();

  const range = moment().range(
    moment(startDateTime).isValid() ? startDateTime : startDate,
    moment(endDate).isValid()
      ? moment(`${endDate}, ${moment(startDateTime).format('hh:mm A')}`)
      : startDatePlusThreeYears,
  );

  const occurrences = Array.from(range.by(unitOfMeasurement));

  if (frequency === FREQUENCY_TYPE.SEMI_ANNUALLY) {
    return (occurrences?.length - 1) * 2;
  }
  return occurrences?.length - 1;
};

export const getEndDate = (
  frequency: $Keys<typeof FREQUENCY_UNIT_OF_MEASUREMENT> | null,
  startDateTime: string | null,
  numberOfOccurrences: number,
): string => {
  const startDate = moment(startDateTime);
  const unitOfMeasurement =
    FREQUENCY_UNIT_OF_MEASUREMENT[frequency || FREQUENCY_TYPE.DAILY];
  const numberOfOccurrencesInDays =
    (numberOfOccurrences - 1) * getConversionFactor(unitOfMeasurement); // We subtract 1 from the number of occurrences because the first payment occurs immediately on the start date

  return startDate.add(numberOfOccurrencesInDays, 'days').format('LLL');
};

export const preparePayload = (formValues: FormValues): Object => {
  const {
    sourceAccount,
    recipientAccount,
    amount,
    amountCurrency,
    recipientType,
    scheduleType,
    schedule,
    scheduleEndType,
    endDate,
    occurrenceType,
  } = formValues;

  const shouldComputeNumberOfOccurrences =
    occurrenceType === OCCURRENCE_TYPE.REPEATING &&
    scheduleEndType === SCHEDULE_END_TYPE.ENDS_ON;

  const formattedSchedule = {
    ...schedule,
    startDate: moment(schedule.startDate).format('LLL'),
    ...(shouldComputeNumberOfOccurrences && {
      numberOfOccurrences: getNumberOfOccurrences(
        schedule.frequency,
        schedule.startDate,
        endDate,
      ),
    }),
  };

  return {
    accountId: sourceAccount?.id,
    amount: amount,
    contactId: recipientAccount?.id,
    currencyCode: amountCurrency,
    customerId: sourceAccount?.customerId,
    targetId: recipientAccount?.id,
    scheduleType: scheduleType,
    recipientType: recipientType,

    // Conditionally insert the schedule property; API will return an error if passed a payload with a schedule property whose startDate is not a valid format/empty
    ...(scheduleType === SCHEDULE_TYPE.SCHEDULED && {
      schedule: formattedSchedule,
    }),
  };
};
