## SendMoneyVerification

SendMoneyVerification component.

# Usage

```js
import SendMoneyVerification from '@screens/private/SendMoneyVerification';

# Props
```

| Prop               | Type   | Required | Description                 |
| ------------------ | ------ | -------- | --------------------------- |
| testID             | string | true     | The testID prop             |
| accessibilityLabel | string | true     | The accessibilityLabel prop |
