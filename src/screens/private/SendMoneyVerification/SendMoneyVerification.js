/**
 * @format
 * @flow
 */

import React, {useState} from 'react';

// Third party libraries
import {useDispatch, useSelector} from 'react-redux';
import {useRoute} from '@react-navigation/core';

// Components
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';

import {submit} from '@ducks/sendMoney/actions';

import type {PropsType} from './types';
import PasscodeScreen from './PasscodeScreen';
import SendMoneyErrorScreen from './SendMoneyErrorScreen';
import {preparePayload} from './utils';

const SendMoneyVerfication = (props: PropsType): React$Node => {
  const route = useRoute();
  const {formValues} = route.params;

  const manageSettings = useSelector((state) => state.manageSettings);

  const {loading} = manageSettings;

  const [passcode, setPasscode] = useState('');
  const [passcodeError, setPasscodeError] = useState('');
  const [error, setError] = useState({
    code: '',
    message: '',
  });
  const handleTextChange = (text) => {
    setPasscode(text);
    if (text.length === 6) {
      setPasscodeError('');
    }
  };

  const dispatch = useDispatch();
  const handleSubmit = () => {
    // setError({
    //   code: 'invalid_mpin',
    //   message: 'invalid passcode'
    // })
    console.log(setError); // need for redux integration
    const payload = preparePayload(formValues);

    dispatch(submit(payload));
    props.navigation.navigate('SendMoneyStack', {
      screen: 'SendMoneyTransactionResult',
      params: {
        formValues,
      },
    });
  };

  if (loading) {
    return <SingleScreen contents={<Loader isVisible />} />;
  }

  if (error?.code && error?.code !== 'invalid_mpin') {
    return <SendMoneyErrorScreen error={error} navigation={props.navigation} />;
  }

  return (
    <PasscodeScreen
      error={error.message}
      passcode={passcode}
      passcodeError={passcodeError}
      setPasscodeError={setPasscodeError}
      onTextChange={(text) => handleTextChange(text)}
      handleNext={handleSubmit}
    />
  );
};

export default SendMoneyVerfication;
