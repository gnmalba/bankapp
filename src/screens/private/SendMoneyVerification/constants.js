export const CONVERT_DAY_TO_DAYS_FACTOR = 1;
export const CONVERT_WEEK_TO_DAYS_FACTOR = 7;
export const CONVERT_MONTH_TO_DAYS_FACTOR = 30.4167;
export const CONVERT_QUARTER_TO_DAYS_FACTOR = 121.667;
export const CONVERT_HALF_YEAR_TO_DAYS_FACTOR = 182.5;
export const CONVERT_YEAR_TO_DAYS_FACTOR = 365;
