// @flow

export const getInputState = (
  alias: string,
  isInputDirty: boolean,
  error: string,
  isInputActive: boolean,
): 'default' | 'error' | 'active' => {
  if ((alias === '' && isInputDirty) || error) {
    return 'error';
  } else if (isInputActive) {
    return 'active';
  }
  return 'default';
};

export const getInputHelperTextState = (
  alias: string,
  isInputDirty: boolean,
  error: string,
): string => {
  if (alias === '' && isInputDirty) {
    return 'Please enter an alias.';
  } else if (error) {
    return error;
  }
  return '';
};
