//@flow
/* eslint-disable react-hooks/exhaustive-deps */

// React modules
import React, {useState, useEffect} from 'react';
import {TouchableOpacity} from 'react-native';

// Third party libraries
import {useSelector, useDispatch} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';

// Components
import Layout from '@components/Layout';
import Text from '@components/Text';
import Divider from '@components/Divider';
import ChangeAliasModal from './components/ChangeAliasModal/ChangeAliasModal';
import PasscodeModal from '@components/PasscodeModal';
import Snackbar from './components/ManageAccountSettingsSnackbar/ManageAccountSettingsSnackbar';
import ManageAccountSettingsResponseHandler from './components/ResponseHandler';
import ErrorModal from '@components/ErrorModal';
import {testProps} from '@utils/componentCustomProps';
import Header from '@components/Header';

// Composition
import ModalWithCTA from '@composition/ModalWithCTA';

// Styles
import * as S from './Styled';

// Constants
import {DELETE_MODAL_PROPS} from './constants';

// Types
import type {PropsType} from './types';
import type {AccountDetails} from '@ducks/manageAccountSettings/types';

// Actions
import {
  changeAliasRequestedCPC,
  deleteAccountRequestedCPC,
  changeAliasRequestedCASA,
  deleteAccountRequestedCASA,
  clearError,
} from '@ducks/manageAccountSettings/actions';

import selector from './selector';

export default function ManageAccountSettings({
  navigation,
}: PropsType): React$Node {
  const {manageAccountSettings} = useSelector(selector);
  const {
    alias,
    cardType,
  }: AccountDetails = manageAccountSettings.selectedAccount;
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [modalVisible, setModalVisible] = useState(null);

  useEffect(() => {
    const {error} = manageAccountSettings;
    if (
      isFocused &&
      error &&
      error !== 'This alias is already in use. Please create another.'
    ) {
      setModalVisible('ERROR');
    }
  }, [isFocused, manageAccountSettings.error]);

  const handlePressEdit = () => setModalVisible('CHANGE_ALIAS');
  const handlePressDelete = () => setModalVisible('DELETE');
  const showModalPincode = () => setModalVisible('PINCODE');
  const handlePressConfirmEdit = (text = '') => {
    if (
      cardType === 'DEPOSIT' ||
      cardType === 'PREPAID' ||
      cardType === 'TIME_DEPOSIT'
    ) {
      actionSelector(text, 'EDIT', changeAliasRequestedCASA);
    } else if (cardType === 'CREDIT' || cardType === 'YAZZ') {
      actionSelector(text, 'EDIT', changeAliasRequestedCPC);
    }
    setModalVisible(null);
  };

  const hideModalAndClearError = () => {
    setModalVisible(null);
    dispatch(clearError());
  };
  const handleErrorModalCallback = () => {
    if (manageAccountSettings.error === 'Passcode is invalid.') {
      hideModalAndClearError();
      setTimeout(() => {
        // NOTE: Need this for iOS modal issue
        setModalVisible('PINCODE');
      }, 150);
    } else {
      hideModalAndClearError();
    }
  };

  const actionSelector = (
    text: string,
    typeOfAction: 'EDIT' | 'DELETE',
    action: (params: Object) => Object,
    // action: (params: EditAliasParams | DeleteAccountParams) => ManageAccountSettingsAction,
  ) => {
    if (typeOfAction === 'EDIT') {
      dispatch(action({alias: text}));
    } else if (typeOfAction === 'DELETE') {
      dispatch(action({mpin: text}));
    }
  };

  const passcodeModalCallback = (passcode): void => {
    if (
      cardType === 'DEPOSIT' ||
      cardType === 'PREPAID' ||
      cardType === 'TIME_DEPOSIT'
    ) {
      actionSelector(passcode, 'DELETE', deleteAccountRequestedCASA);
    } else if (cardType === 'CREDIT' || cardType === 'YAZZ') {
      actionSelector(passcode, 'DELETE', deleteAccountRequestedCPC);
    }
    setModalVisible(null);
  };

  const renderModal = (): React$Node => {
    switch (modalVisible) {
      case 'CHANGE_ALIAS':
        return (
          <ChangeAliasModal
            isVisible
            alias={alias}
            positiveCallback={handlePressConfirmEdit}
            negativeCallback={hideModalAndClearError}
          />
        );

      case 'DELETE':
        return (
          <ModalWithCTA
            testID="manageAccountSettingsDeleteAccount"
            isVisible
            titleProps={DELETE_MODAL_PROPS.titleProps}
            subtitleProps={DELETE_MODAL_PROPS.subtitleProps}
            positiveButtonProps={{
              ...DELETE_MODAL_PROPS.positiveButtonProps,
              callback: showModalPincode,
            }}
            negativeButtonProps={{
              ...DELETE_MODAL_PROPS.negativeButtonProps,
              callback: hideModalAndClearError,
            }}
          />
        );

      case 'PINCODE':
        return (
          <PasscodeModal
            isVisible
            password
            mainButtonLabel="Proceed"
            passcodeError=""
            positiveCallback={passcodeModalCallback}
            negativeCallback={hideModalAndClearError}
          />
        );

      case 'ERROR':
        const errorMsg =
          manageAccountSettings.error === 'Passcode is invalid.'
            ? 'Try again'
            : 'Go back';
        return (
          <ErrorModal
            isVisible
            buttonTitle={errorMsg}
            onPressButton={handleErrorModalCallback}
            errorText={manageAccountSettings.error}
          />
        );
      default:
        return null;
    }
  };

  return (
    <>
      <Header title="My Accounts" onBackClick={() => navigation.goBack()} />
      <Layout padding={32}>
        <S.MainContainer>
          <S.FlexContainer>
            <S.FlexContainer>
              <Text {...testProps('manageAccountSettingsHeaderID')} mode="h2">
                Account Settings
              </Text>

              <S.LabelContainer>
                <S.LabelText
                  {...testProps('manageAccountSettingsLabelID')}
                  mode="body2">
                  Customize how you want to use your account
                </S.LabelText>
              </S.LabelContainer>

              <Divider
                {...testProps('manageAccountSettingsUpperLine')}
                marginVertical={16}
              />

              <S.RowDetailsContainer>
                <S.TitleContainer>
                  <Text
                    {...testProps('manageAccountSettingsAliasLabel')}
                    mode="body1">
                    Alias
                  </Text>
                </S.TitleContainer>

                <S.DetailsContainer>
                  <S.AliasContainer>
                    <Text
                      {...testProps('manageAccountSettingsAliasValue')}
                      numberOfLines={2}
                      mode="body1">
                      {alias}
                    </Text>
                  </S.AliasContainer>

                  <S.ViewDivider />

                  <TouchableOpacity
                    onPress={handlePressEdit}
                    activeOpacity={0.8}
                    {...testProps('manageAccountSettingsEditButtonTouch')}>
                    <S.EditButton
                      {...testProps('manageAccountSettingsEditButton')}
                      mode="body2">
                      edit
                    </S.EditButton>
                  </TouchableOpacity>
                </S.DetailsContainer>
              </S.RowDetailsContainer>

              <Divider
                {...testProps('manageAccountSettingsLowerLine')}
                marginVertical={16}
              />
            </S.FlexContainer>
            <S.DeleteAccountContainer>
              <TouchableOpacity
                {...testProps('manageAccountSettingsDeleteButtonTouch')}
                onPress={handlePressDelete}
                activeOpacity={0.8}>
                <Text
                  {...testProps('manageAccountSettingsDeleteButton')}
                  mode="subtitle">
                  Delete Account
                </Text>
              </TouchableOpacity>
            </S.DeleteAccountContainer>
          </S.FlexContainer>
        </S.MainContainer>
        {renderModal()}

        <Snackbar />

        <ManageAccountSettingsResponseHandler navigation={navigation} />
      </Layout>
    </>
  );
}
