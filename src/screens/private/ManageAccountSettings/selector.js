const selector = ({
  deposit,
  credit,
  depositprepaidoverview,
  manageAccountSettings,
}) => {
  return {deposit, credit, depositprepaidoverview, manageAccountSettings};
};

export default selector;
