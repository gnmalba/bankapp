export const DELETE_MODAL_PROPS = {
  titleProps: {
    text: 'Are you sure you want to delete this account?',
  },
  subtitleProps: {
    text:
      'All details of this account, including scheduled transactions and activity history, will be removed from Metrobank Online',
  },
  positiveButtonProps: {
    buttonText: 'Delete',
  },
  negativeButtonProps: {
    buttonText: 'Cancel',
  },
};
