/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Snackbar from '@components/Snackbar';
import {toggleSnackbar} from '@ducks/manageAccountSettings/actions';
import {snackbarSelector} from '@ducks/manageAccountSettings/selectors';

export const ManageAccountSettingSnackbar = (): React$Node => {
  const snackbar = useSelector(snackbarSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    const timeHandler = setTimeout(() => {
      dispatch(
        toggleSnackbar({
          state: false,
          message: '',
        }),
      );
    }, 3000);
    return () => {
      clearTimeout(timeHandler);
    };
  }, [snackbar.state]);

  return <Snackbar visible={snackbar.state} message={snackbar.message} />;
};

export default ManageAccountSettingSnackbar;
