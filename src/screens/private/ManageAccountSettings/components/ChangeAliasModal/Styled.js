/**
 * @flow
 */

import type {ComponentType} from 'react';
import {TouchableOpacity} from 'react-native';
import {verticalScale, scale} from '@utils/scale';
// Third party libraries
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import styled from 'styled-components/native';

export const ModalContentContainer: ComponentType<any> = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.8);
  justify-content: flex-end;
`;

export const BodyContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_200};
  padding-top: ${verticalScale(32)}px;
  padding-bottom: ${verticalScale(32)}px;
  padding-left: ${scale(32)}px;
  padding-right: ${scale(32)}px;
  border-radius: 8px;
`;

export const MarginBottom: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(16)}px;
`;

export const ScrollView: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs((props) => ({
  contentContainerStyle: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  extraHeight: 220,
  bounces: false,
  keyboardShouldPersistTaps: 'always',
  scrollEnabled: false,
}))``;

export const TitleContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-bottom: ${verticalScale(32)}px;
`;

export const ButtonsContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(8)}px;
`;

export const CancelContainer: ComponentType<any> = styled(TouchableOpacity)`
  align-self: center;
`;
