//@flow
// React modules
import React, {useState} from 'react';
import {View, Modal} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';
// Style
import * as S from './Styled';

// Components
import Text from '@components/Text';
import Button from '@components/Button';
import TextInput from '@composition/TextInput';

// Utils
import {getInputState, getInputHelperTextState} from '../../utils';
import {testProps} from '@utils/componentCustomProps';

import {errorSelector} from '@ducks/manageAccountSettings/selectors';

export default function ChangeAliasModal({
  alias = 'Gab Savings',
  positiveCallback,
  negativeCallback,
}: {
  alias: string,
  positiveCallback: (alias: string | typeof undefined) => void,
  negativeCallback: () => void,
}): React$Node {
  const dispatch = useDispatch();
  const error = useSelector(errorSelector);
  const [aliasText, setAliasText] = useState('');
  const handleTextChange = (text) => setAliasText(text);
  const handlePressCallback = () => positiveCallback(aliasText);
  const [isInputDirty, setIsInputDirty] = useState(false);
  const [isInputActive, setIsInputActive] = useState(false);

  const getButtonState = (): 'default' | 'disabled' => {
    if (error) {
      return 'disabled';
    } else if (aliasText !== '') {
      return 'default';
    }
    return 'disabled';
  };

  const handleChangeText = (text = '') => {
    handleTextChange(text);
    setIsInputDirty(true);
    dispatch({type: 'CHECK_DUPLICATE_ALIAS', payload: text});
  };

  const handleInputFocus = () => setIsInputActive(true);
  const handleInputBlur = () => setIsInputActive(false);
  const handleInputClear = () => setAliasText('');

  return (
    <Modal
      animationType="none"
      transparent
      visible
      onRequestClose={negativeCallback}>
      <S.ScrollView>
        <S.ModalContentContainer>
          <S.BodyContainer>
            <S.TitleContainer>
              <Text
                {...testProps('manageAccountSettingsChangeAliasHeader')}
                mode="h3">
                Change Account Alias
              </Text>
            </S.TitleContainer>

            <View>
              <S.MarginBottom>
                <Text
                  {...testProps('manageAccountSettingsChangeAliasFromLabel')}
                  mode="body2">
                  From
                </Text>
              </S.MarginBottom>

              <S.MarginBottom>
                <Text
                  {...testProps('manageAccountSettingsChangeAliasFromValue')}
                  mode="body1">
                  {alias}
                </Text>
              </S.MarginBottom>

              <S.MarginBottom>
                <Text
                  {...testProps('manageAccountSettingsChangeAliasToLabel')}
                  mode="body2">
                  To
                </Text>
              </S.MarginBottom>

              <S.MarginBottom>
                <TextInput
                  {...testProps('manageAccountSettingsChangeAliasToValue')}
                  placeholder="Enter your new alias"
                  value={aliasText}
                  onChangeText={handleChangeText}
                  onFocus={handleInputFocus}
                  onBlur={handleInputBlur}
                  helperText={getInputHelperTextState(
                    aliasText,
                    isInputDirty,
                    error,
                  )}
                  state={getInputState(
                    aliasText,
                    isInputDirty,
                    error,
                    isInputActive,
                  )}
                  handleClear={handleInputClear}
                />
              </S.MarginBottom>
            </View>

            <S.ButtonsContainer>
              <S.MarginBottom>
                <Button
                  {...testProps('manageAccountSettingsChangeAliasApplyButton')}
                  type="primary"
                  state={getButtonState()}
                  size="large"
                  title="Apply"
                  onPress={handlePressCallback}
                />
              </S.MarginBottom>

              <S.CancelContainer
                {...testProps('manageAccountSettingsChangeAliasCancelButton')}
                onPress={negativeCallback}>
                <Text mode="subtitle">Cancel</Text>
              </S.CancelContainer>
            </S.ButtonsContainer>
          </S.BodyContainer>
        </S.ModalContentContainer>
      </S.ScrollView>
    </Modal>
  );
}
