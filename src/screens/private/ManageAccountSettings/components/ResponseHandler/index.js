import React, {Component} from 'react';
import {connect} from 'react-redux';

/**
 * This component is used for navigation
 * logic for manage account settings
 *
 * Navigation is passed on the saga for the
 * navigationHandler generator to handle
 * navigation logic
 */
class ManageAccountSettingsResponseHandler extends Component {
  componentDidUpdate(prevProps) {
    if (
      prevProps.manageAccountSettings.isLoading !==
      this.props.manageAccountSettings.isLoading
    ) {
      this.props.passNavigation(this.props.navigation);
    }
  }
  render() {
    return <></>;
  }
}

const mapStateToProps = (state) => ({
  manageAccountSettings: state.manageAccountSettings,
});

export const mapDispatchToProps = {
  passNavigation: (navigation) => ({
    type: 'PASS_NAVIGATION',
    payload: navigation,
  }),
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ManageAccountSettingsResponseHandler);
