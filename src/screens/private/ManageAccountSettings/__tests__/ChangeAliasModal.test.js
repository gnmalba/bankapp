import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import ChangeAliasModal from '../components/ChangeAliasModal/ChangeAliasModal';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('ChangeAliasModal', () => {
  const store = {
    manageAccountSettings: {
      selectedAccount: {
        alias: 'test',
      },
      snackbar: {
        state: undefined,
        message: 'test',
      },
      error: null,
    },
  };

  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ChangeAliasModal
            alias="Gab savings"
            positiveCallback={() => {}}
            negativeCallback={() => {}}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    const toAliasTextInput = all.getByTestId(
      'manageAccountSettingsChangeAliasToValue',
    );
    fireEvent(toAliasTextInput, 'focus');
    fireEvent(toAliasTextInput, 'onChangeText', 'Gab savings acct');
    fireEvent(toAliasTextInput, 'blur');

    const applyButton = all.getByTestId(
      'manageAccountSettingsChangeAliasApplyButton',
    );
    fireEvent(applyButton, 'onPress');

    expect(all.toJSON()).toMatchSnapshot();
  });

  it('should handle error ', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider
        store={{
          ...store,
          manageAccountSettings: {
            ...store.manageAccountSettings,
            error: 'Something went wrong',
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ChangeAliasModal
            alias="Gab savings"
            positiveCallback={() => {}}
            negativeCallback={() => {}}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();
  });
});
