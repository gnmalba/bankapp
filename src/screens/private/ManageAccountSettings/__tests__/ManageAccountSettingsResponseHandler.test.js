import React from 'react';
import {render} from '@testing-library/react-native';
import ManageAccountSettingsResponseHandler, {
  mapDispatchToProps,
} from '../components/ResponseHandler';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('ManageAccountSettingsResponseHandler', () => {
  const store = {
    manageAccountSettings: {
      loading: false,
    },
  };
  //
  const navigation = {
    navigate: (screen: string) => {},
  };

  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettingsResponseHandler navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();

    const newStore = {
      manageAccountSettings: {
        loading: true,
      },
    };

    all.update(
      <MockProvider store={newStore}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettingsResponseHandler
            passNavigation={navigation}
            navigation={navigation}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(mapDispatchToProps.passNavigation(navigation)).toBeTruthy();
  });
});
