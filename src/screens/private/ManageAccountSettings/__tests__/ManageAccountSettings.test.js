import React from 'react';
import {render, fireEvent, cleanup} from '@testing-library/react-native';
import ManageAccountSettings from '../ManageAccountSettings';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('ManageAccountSettings', () => {
  afterEach(cleanup);
  const store = {
    deposit: {},
    credit: {},
    depositprepaidoverview: {},
    manageAccountSettings: {
      selectedAccount: {
        alias: 'test',
        cardType: 'DEPOSIT',
      },
      snackbar: {
        state: undefined,
        message: 'test',
      },
    },
  };
  const navigation = {
    navigate: () => {},
  };

  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettings navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should render all modals correctly in CASA accounts', () => {
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettings navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );

    // buttons
    const editButton = all.getByTestId('manageAccountSettingsEditButtonTouch');
    fireEvent(editButton, 'onPress');

    const textInput = all.getByTestId(
      'manageAccountSettingsChangeAliasToValue',
    );

    fireEvent(textInput, 'onChangeText', 'GAB SAVINGS ACCOUNT');

    const changeAliasModalApply = all.getByTestId(
      'manageAccountSettingsChangeAliasApplyButton',
    );

    fireEvent(changeAliasModalApply, 'onPress');
    fireEvent(editButton, 'onPress');

    const changAliasModalCancel = all.getByTestId(
      'manageAccountSettingsChangeAliasCancelButton',
    );
    fireEvent(changAliasModalCancel, 'onPress');

    const deleteButton = all.getByTestId(
      'manageAccountSettingsDeleteButtonTouch',
    );
    fireEvent(deleteButton, 'onPress');

    const deleteModalCancelButton = all.getByAccessibilityLabel(
      'negativeButtonLabel',
    );

    fireEvent(deleteModalCancelButton, 'onPress');
    fireEvent(deleteButton, 'onPress');

    const deleteModalDeleteButton = all.getByAccessibilityLabel(
      'positiveButtonLabel',
    );

    fireEvent(deleteModalDeleteButton, 'onPress');

    const pinCodeComp = all.getByTestId('smoothInputTestId');

    fireEvent(pinCodeComp, 'onChangeText', '111111');

    const passcodeModalProceedButton = all.getByTestId('buttonTestId');
    fireEvent(passcodeModalProceedButton, 'onPress');
  });

  it('Should render all modals correctly in CPC accounts', () => {
    const all = render(
      <MockProvider
        store={{
          ...store,
          manageAccountSettings: {
            ...store.manageAccountSettings,
            selectedAccount: {
              alias: 'test',
              cardType: 'CREDIT',
            },
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettings navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );

    // buttons
    const editButton = all.getByTestId('manageAccountSettingsEditButtonTouch');
    fireEvent(editButton, 'onPress');

    const textInput = all.getByTestId(
      'manageAccountSettingsChangeAliasToValue',
    );

    fireEvent(textInput, 'onChangeText', 'GAB SAVINGS ACCOUNT');

    const changeAliasModalApply = all.getByTestId(
      'manageAccountSettingsChangeAliasApplyButton',
    );

    fireEvent(changeAliasModalApply, 'onPress');
    fireEvent(editButton, 'onPress');

    const changAliasModalCancel = all.getByTestId(
      'manageAccountSettingsChangeAliasCancelButton',
    );
    fireEvent(changAliasModalCancel, 'onPress');

    const deleteButton = all.getByTestId(
      'manageAccountSettingsDeleteButtonTouch',
    );
    fireEvent(deleteButton, 'onPress');

    const deleteModalCancelButton = all.getByAccessibilityLabel(
      'negativeButtonLabel',
    );

    fireEvent(deleteModalCancelButton, 'onPress');
    fireEvent(deleteButton, 'onPress');

    const deleteModalDeleteButton = all.getByAccessibilityLabel(
      'positiveButtonLabel',
    );

    fireEvent(deleteModalDeleteButton, 'onPress');

    const pinCodeComp = all.getByTestId('smoothInputTestId');

    fireEvent(pinCodeComp, 'onChangeText', '111111');

    const passcodeModalProceedButton = all.getByTestId('buttonTestId');
    fireEvent(passcodeModalProceedButton, 'onPress');
  });

  it('Should render error modal', () => {
    const all = render(
      <MockProvider
        store={{
          ...store,
          manageAccountSettings: {
            ...store.manageAccountSettings,
            error: 'Something went wrong',
          },
        }}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettings navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );

    const errorModalTryAgainButton = all.getByTestId('buttonTestId');
    fireEvent(errorModalTryAgainButton, 'onPress');
  });
});
