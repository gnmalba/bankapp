import React from 'react';
import {render} from '@testing-library/react-native';
import ManageAccountSettingsSnackbar from '../components/ManageAccountSettingsSnackbar/ManageAccountSettingsSnackbar';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('ManageAccountSettingsSnackbar', () => {
  const store = {
    manageAccountSettings: {
      snackbar: {
        state: false,
        message: '',
      },
    },
  };

  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ManageAccountSettingsSnackbar />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
