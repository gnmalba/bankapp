## ManageAccountSettings

ManageAccountSettings component.

# Usage

```js
import ManageAccountSettings from '@screens/private/ManageAccountSettings';

# Props
```

| Prop               | Type   | Required | Description                 |
| ------------------ | ------ | -------- | --------------------------- |
| testID             | string | false    | The testID prop             |
| accessibilityLabel | string | false    | The accessibilityLabel prop |
| alias              | string | true     | alias of the selected acct  |
