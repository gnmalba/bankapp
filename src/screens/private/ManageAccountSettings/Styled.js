/**
 * @flow
 */
// React modules
import type {ComponentType} from 'react';

// Third party libraries
import styled from 'styled-components/native';

// Components
import Text from '@components/Text';

// Utils
import {verticalScale} from '@utils/scale';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  margin-top: ${verticalScale(8)}px;
  margin-bottom: ${verticalScale(80)}px;
`;

export const EditButton: ComponentType<any> = styled(Text)`
  color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
`;

export const LabelText: ComponentType<any> = styled(Text)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const LabelContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(8)}px;
`;

export const DeleteAccountContainer: ComponentType<any> = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

export const FlexContainer: ComponentType<any> = styled.View`
  flex: 1;
`;

export const TitleContainer: ComponentType<any> = styled.View`
  flex: 4;
`;

export const RowDetailsContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  flex: 4;
  justify-content: space-between;
  align-items: center;
`;

export const AliasContainer: ComponentType<any> = styled.View`
  flex: 4;
`;
export const ViewDivider: ComponentType<any> = styled.View`
  flex: 0.5;
`;
