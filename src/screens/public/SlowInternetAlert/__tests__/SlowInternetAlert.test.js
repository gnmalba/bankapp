import React from 'react';
import {render} from '@testing-library/react-native';
import SlowInternetAlert from '../SlowInternetAlert';

describe('SlowInternetAlert', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<SlowInternetAlert />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
