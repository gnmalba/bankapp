/**
 * @format
 * @flow
 */

import React from 'react';
import AlertScreen from '@screens/common/AlertScreen';
import type {PropsType} from './types';

const DeviceSecurityAlert = (props: PropsType): React$Node => {
  return (
    <AlertScreen
      icon={require('@assets/icons/wifi.png')}
      title="Slow internet connection"
      description1={'The connection that you’re on seems slow.'}
      description2={
        'Your information might take a while to load. Please connect to a faster and more stable connection.'
      }
      navigation={props.navigation}
    />
  );
};

export default DeviceSecurityAlert;
