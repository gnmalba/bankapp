## SlowInternetAlert
SlowInternetAlert component.

# Usage
```js
import SlowInternetAlert from '@screens/public/SlowInternetAlert';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
