## BiometricsRegistration
BiometricsRegistration component.

# Usage
```js
import BiometricsRegistration from '@screens/public/BiometricsRegistration';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
