/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import {verticalScale, scale} from '@utils/scale';

export const DescriptionContainer: ComponentType<any> = styled.View``;

export const BottomContainer: ComponentType<any> = styled.View.attrs(
  (props) => ({
    top:
      Dimensions.get('window').height -
      verticalScale(props.spaceFromBottom || 210),
  }),
)`
  position: absolute;
  width: 100%;
  left: ${scale(36)}px;
`;
