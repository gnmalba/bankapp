/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {StatusBar, View, Platform} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import Loader from '@composition/Loader';
import type {PropsType} from './types';
import * as Q from './components';
import * as K from '@screens/public/LoginScreen/components';
import {BottomContainer} from './Styled';
import type {State, AuthenticationType} from '@ducks/types';
import FormLayout from '@composition/FormLayout';
import {scale} from '@utils/scale';

import * as types from '@ducks/biometrics/actionTypes';
import * as authTypes from '@ducks/authentication/actionTypes';

import {testProps} from '@utils/componentCustomProps';

const LoginScreen = (props: PropsType): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const {loading} = authentication;
  const {theme} = props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: authTypes.AUTHENTICATION_UPDATE_STATE,
      newState: {
        modalPrimaryBtnAction: {
          type: authTypes.AUTHENTICATION_UPDATE_STATE,
          newState: {
            showModal: false,
            username: '',
            password: '',
          },
        },
        proceedToBioRegistration: false,
      },
    });
  }, []);

  return (
    <FormLayout>
      <KeyboardAwareScrollView
        keyboardShouldPersistTaps={'handled'}
        enableOnAndroid
        extraHeight={Platform.select({
          ios: 170,
          android: 90,
        })}>
        <StatusBar
          barStyle={theme?.topAppBar.barStyle}
          backgroundColor={
            loading
              ? theme?.topAppBar.loading
              : theme?.topAppBar.backgroundColor
          }
        />
        <Q.Header />
        <K.StyledUsername />
        <View style={{height: scale(200)}}>
          <K.StyledPassword />
        </View>
        <K.StyledModal />
        <Loader
          {...testProps('loaderTestId')}
          showLabel="Test label"
          isVisible={loading}
        />
      </KeyboardAwareScrollView>
      <BottomContainer>
        <K.StyledLoginButton
          title="Proceed"
          onPress={() => {
            dispatch({
              type: types.REGISTER_BIOMETRICS,
              status: true,
            });
          }}
        />
      </BottomContainer>
    </FormLayout>
  );
};

export default LoginScreen;
