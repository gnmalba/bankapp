import React from 'react';
import {render} from '@testing-library/react-native';
import BiometricsRegistration from '../BiometricsRegistration';
import MockProvider from '@utils/testFramework';

const mockStore = (err) => ({
  authentication: {
    username: 'Test',
    password: 'Test',
    usernameError: 'Err',
    passwordError: 'Err',
    loading: err === true,
  },
  biometrics: {
    deviceSensor: 'FingerPrint',
  },
});

describe('BiometricsRegistration', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore(true)}>
        <BiometricsRegistration />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
