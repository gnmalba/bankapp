// @flow

import React from 'react';
import {useSelector} from 'react-redux';

import Text from '@components/Text';
import Separator from '@components/Separator';

import type {State, BiometricsType} from '@ducks/types';
import * as S from '../Styled';

export const Header = (): React$Node => {
  const biometrics: BiometricsType = useSelector(
    (state: State) => state.biometrics,
  );
  const {deviceSensor} = biometrics;

  return (
    <S.DescriptionContainer>
      <Text mode="h2">{`${deviceSensor} Registration`}</Text>
      <Separator space={8} />
      <Text mode="body2" themeColor="TEXT_100_HIGH">
        {`Please login your app credentials to proceed. Activating this feature will allow you to login via ${deviceSensor} (in supported devices)`}
      </Text>
    </S.DescriptionContainer>
  );
};

export default Header;
