import React from 'react';
import {render} from '@testing-library/react-native';
import DeviceSecurityAlert from '../DeviceSecurityAlert';

jest.mock('jail-monkey', () => ({
  isJailBroken: () => true,
}));

describe('DeviceSecurityAlert', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<DeviceSecurityAlert />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
