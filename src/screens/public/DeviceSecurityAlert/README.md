## DeviceSecurityAlert
DeviceSecurityAlert component.

# Usage
```js
import DeviceSecurityAlert from '@screens/public/DeviceSecurityAlert';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
