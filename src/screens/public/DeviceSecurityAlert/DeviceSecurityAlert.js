/**
 * @format
 * @flow
 */

import React from 'react';
import AlertScreen from '@screens/common/AlertScreen';
import type {PropsType} from './types';

const DeviceSecurityAlert = (props: PropsType): React$Node => {
  return (
    <AlertScreen
      icon={require('@assets/icons/unlock.png')}
      title="Device security alert"
      description1={
        'It seems like you’re using either a rooted or jailbroken device. This makes you vulnerable to bad software, spying of personal information, or slowing down of your phone’s performance. We don’t recommend using a rooted or jailbroken device to do banking activities. By continuing, you understand that we are not responsible if something goes wrong while using a modified device.'
      }
      description2={
        'We don’t recommend using a rooted or jailbroken device to do bankingactivities.'
      }
      description3={
        'By continuing, you understand that we are not responsible if something goes wrong while using a modified device.'
      }
      navigation={props.navigation}
    />
  );
};

export default DeviceSecurityAlert;
