import api from '@api/';
import {LOGIN_API} from '@ducks/apiConstants';
import store from '@ducks/store';
import {AUTHENTICATION_LOGIN_FAILED} from '@ducks/authentication/actionTypes';

export const fnGetAdvisory = async (setNotification) => {
  try {
    const {data} = await api.get(LOGIN_API.ADVISORY);
    setNotification((prevState) => ({
      ...prevState,
      data: data.data,
      loading: false,
    }));
  } catch (err) {
    setNotification((prevState) => ({
      ...prevState,
      loading: false,
    }));
    store.dispatch({
      type: AUTHENTICATION_LOGIN_FAILED,
      additionalState: {
        proceedToFullPageError: true,
        error: err.message,
      },
    });
  }
};
