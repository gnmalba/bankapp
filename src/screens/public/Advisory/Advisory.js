// @flow

import React from 'react';
import {FlatList} from 'react-native';
import Loader from '@composition/Loader';

import AdvisoryItem from '@composition/AdvisoryItem';
import FormLayout from '@composition/FormLayout';
import {fnGetAdvisory} from './helpers';
import {KEY_INBOX} from './config';
import {Container} from './Styled';

const NotificationScreen = (): React$Element<any> => {
  const [notification, setNotification] = React.useState({
    data: [],
    loading: true,
    error: {},
  });

  React.useEffect(() => {
    fnGetAdvisory(setNotification);
  }, []);

  const _keyExtractor = (item) => KEY_INBOX + item.id;

  const _renderItem = (props) => {
    const {index, item} = props;
    return <AdvisoryItem key={index} {...item} />;
  };

  return (
    <FormLayout>
      {notification.loading ? (
        <Loader isVisible={notification.loading} />
      ) : (
        <Container>
          <FlatList
            data={notification.data}
            numColumns={1}
            renderItem={_renderItem}
            keyExtractor={_keyExtractor}
            showsVerticalScrollIndicator={false}
          />
        </Container>
      )}
    </FormLayout>
  );
};

export default NotificationScreen;
