/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(-30)}px;
  margin-left: ${verticalScale(-10)}px;
  margin-right: ${verticalScale(-10)}px;
`;
