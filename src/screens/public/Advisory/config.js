// @flow

export const KEY_INBOX = 'key-inbox-';
export const typeImageMap = {
  announcement: require('@assets/icons/announcement-big.png'),
  alert: require('@assets/icons/announcement-big.png'),
  warning: require('@assets/icons/advisory-warning-big.png'),
  important: require('@assets/icons/advisory-warning-big.png'),
};
export const ANNOUNCEMENT = 'announcement';
export const SANITIZE_BODY_REGEX: any = /<\/?[^>]+(>|$)/g;
export const ANNOUNCEMENTS = 'Announcements';
export const IMPORTANT = 'important';
export const COLOR_WHITE = '#FFFFFF';
export const COLOR_BLACK = '#000000';
export const BOLD = 'bold';
export const HOUR = 'hour';
export const NO_DATE = 'NO DATE';
export const CHEVRON_RIGHT = 'chevron-right';

export const mockData: Object[] = [
  {
    id: 1,
    title: 'Announcement',
    body:
      '<span>Please schedule your transactions accordingly to avoid any inconvenience or late charges.</span> <span>We wish you and your loved ones a blessed week ahead.</span>',
    type: 'important',
    date: Date.now(),
  },
  {
    id: 0,
    title: 'Meralco bills payment.',
    body:
      '<p>In observance of the Holy Week, the Banks schedule of operations will be as follows: Selected branches..  <p>We wish you and your loved ones a blessed week ahead.We wish you and your loved ones a blessed week ahead.',
    type: 'announcement',
    date: Date.now(),
  },
  {
    id: 2,
    title: 'Title',
    body:
      '<span>In observance of the Holy Week, the Banks schedule of operations will be as follows: Selected branches. We wish you and your loved ones a blessed week ahead.</span>',
    type: 'announcement',
    date: Date.now(),
  },
];
