## LoginScreen
LoginScreen component.

# Usage
```js
import LoginScreen from '@screens/public/LoginScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
SampleProp                | boolean               | false                   | This is only a sample
