/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {verticalScale, scale, moderateScale} from '@utils/scale';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Text from '@components/Text';
import {Platform} from 'react-native';
import OtherActionsButton from '@components/OtherActionsButton';

import {testProps} from '@utils/componentCustomProps';

export const Logo: ComponentType<any> = styled.Image.attrs((props) => ({
  source: props.theme.logoSrc,
}))`
  width: ${verticalScale(147.78)}px;
  height: ${verticalScale(32)}px;
  align-self: center;
`;

export const UsernameContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(33)}px;
`;

export const PasswordContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(24)}px;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(32)}px;
`;

export const RecoverAccessContainer: ComponentType<any> = styled.View`
  flex-direction: row-reverse;
  margin-top: ${verticalScale(16)}px;
`;

export const BottomContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-bottom: ${verticalScale(32)}px;
`;

export const ContentContainer: ComponentType<any> = styled.View`
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: ${(props) => {
    return (
      (props.isActionOpen
        ? props.theme?.otherActions?.containerBackground
        : props.theme?.color?.BACKGROUND_100) || 'transparent'
    );
  }};
  border-top-right-radius: 16px;
  border-top-left-radius: 16px;
`;

export const CloseIcon: ComponentType<any> = styled.View`
  height: ${verticalScale(4)}px;
  width: ${scale(54)}px;
  border-radius: ${scale(4)}px;
  background-color: ${(props) =>
    props.theme?.color?.TEXT_300_LOW || 'transparent'};
  margin-top: ${verticalScale(20)}px;
`;

export const StyledIonIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    color: props.theme?.otherActions?.chevronUp || 'transparent',
  }),
)``;

export const OtherActionsContainer: ComponentType<any> = styled.View`
  width: 100%;
  padding-top: ${moderateScale(19)}px;
  padding-left: ${verticalScale(32)}px;
  padding-right: ${verticalScale(32)}px;
  align-items: flex-start;
  justify-content: space-between;
  height: 100%;
  padding-bottom: ${moderateScale(55)}px;
`;

export const BiometricsContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-bottom: ${verticalScale(24)}px;
`;

export const BiometricsIcon: ComponentType<any> = styled.Image.attrs(
  (props) => ({
    source:
      props.deviceSensor === 'Face ID'
        ? require('@assets/icons/face_id.png')
        : require('@assets/icons/finger_print.png'),
  }),
)`
  width: ${scale(22)}px;
  height: ${scale(22)}px;
`;

export const BiometricsLinkContainer: ComponentType<any> = styled.View`
  padding-left: ${scale(10)}px;
`;

export const ModalContainer: ComponentType<any> = styled.View``;

export const SecondaryBtnContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const OtherOptionsIconsContainer: ComponentType<any> = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
`;

export const SignUpContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: flex-end;
`;

export const StyledSignUp: ComponentType<any> = styled(Text)`
  font-size: ${moderateScale(14, 0.75)}px;
  font-weight: 700;
  padding-bottom: ${Platform.OS === 'ios' ? verticalScale(2) : 0}px;
`;

export const BottomSheetTitle: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'h3',
    ...testProps('otherActionsTestId'),
  }),
)`
  color: ${(props) => props.theme.mainTopMenuItems.color};
  font-family: MyriadPro-Semibold;
`;

export const StyledLocation: ComponentType<any> = styled(
  OtherActionsButton,
).attrs((props) => ({
  icon: props.theme?.otherActions?.location,
  title: 'ATM & Branch Locator',
}))``;

export const StyledQr: ComponentType<any> = styled(OtherActionsButton).attrs(
  (props) => ({
    icon: props.theme?.otherActions?.qr,
    title: 'QR Code',
  }),
)``;

export const StyledBank: ComponentType<any> = styled(OtherActionsButton).attrs(
  (props) => ({
    icon: props.theme?.otherActions?.bank,
    title: 'Open a Bank Account',
  }),
)``;
