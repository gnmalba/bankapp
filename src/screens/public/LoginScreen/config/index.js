/**
 * @flow
 */

export const ACTION_TEST_ID = 'handleCloseActionTestId';
export const SHOW_ACTION_TEST_ID = 'handleShowActionTestID';
export const SHEET_CONTENT_TESTID = 'bottomSheetContentTestId';
export const ACTION_TESTID = 'actionTestId';
