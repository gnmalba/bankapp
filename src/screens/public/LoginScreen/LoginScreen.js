/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

import React, {useState, useRef, useEffect, useCallback} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {StatusBar, View, Keyboard} from 'react-native';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import JailBroken from 'jail-monkey';
/**
 * Temporarily remove netinfo implementation due to production crash
 *
 * import {useNetInfo} from '@react-native-community/netinfo';
 */
import BackgroundTimer from 'react-native-background-timer';
import {useFocusEffect} from '@react-navigation/core';

import * as types from '@ducks/actionTypes';

import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';
import type {PropsType} from './types';
import {SHEET_CONTENT_TESTID, ACTION_TESTID} from './config';
import {Logo} from './Styled';
import {
  BottomSheetContent,
  Action,
  StyledUsername,
  StyledPassword,
  StyledLoginButton,
  StyledRecoverAccess,
  StyledSignupVersion,
  StyledBiometricsLogin,
  StyledModal,
  StyledSnackbar,
} from './components';
import type {State, AuthenticationType} from '@ducks/types';

import {moderateScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

const LoginScreen = (props: PropsType): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const {
    loading,
    proceedToOtp,
    proceedToFullPageError,
    error,
    proceedToBioRegistration,
  } = authentication;
  const {initActionOpen = false, theme, navigation} = props;
  const [isActionOpen, setIsActionOpen] = useState(initActionOpen);
  const [keyboardVisible, setKeyboardVisible] = useState(false);
  const bottomSheetRef: any = useRef();
  const fall = new Animated.Value(1);
  const dispatch = useDispatch();

  /**
   * TODO:
   * Crashing in production

    const netInfo = useNetInfo();

    useEffect(() => {
      console.dir(netInfo);
      if (netInfo.type !== 'unknown' && netInfo.isConnected !== null) {
        navigation.navigate('SlowInternetAlert');
      }
    }, [netInfo]);
  */

  const show = () => {
    setKeyboardVisible(true);
  };

  const hide = () => {
    setKeyboardVisible(false);
  };

  useEffect(() => {
    dispatch({
      type: types.BIOMETRICS_GET_SENSOR,
    });

    if (JailBroken.isJailBroken()) {
      navigation.navigate('DeviceSecurityAlert');
    }
    BackgroundTimer.stopBackgroundTimer();
    Keyboard.addListener('keyboardDidShow', show);
    Keyboard.addListener('keyboardDidHide', hide);
    return () => {
      Keyboard.removeListener('keyboardDidShow', show);
      Keyboard.removeListener('keyboardDidHide', hide);
    };
  }, []);

  useEffect(() => {
    proceedToOtp && navigation.navigate('Verification');
    proceedToFullPageError &&
      navigation.navigate('FullPageError', {
        msg: error,
      });
    proceedToBioRegistration && navigation.navigate('BiometricsRegistration');
  }, [proceedToFullPageError, proceedToOtp, proceedToBioRegistration]);

  const handleActionDown = () => {
    bottomSheetRef.current.snapTo(1);
    setIsActionOpen(false);
  };

  const handleActionUp = () => {
    bottomSheetRef.current.snapTo(0);
    setIsActionOpen(true);
  };

  const renderContent = (): React$Element<any> => {
    return (
      <BottomSheetContent
        {...testProps(SHEET_CONTENT_TESTID)}
        isActionOpen={isActionOpen}
        handleActionDown={handleActionDown}
        handleActionUp={handleActionUp}
      />
    );
  };

  const containerStyle = {
    justifyContent: 'space-between',
    height: '100%',
  };

  useFocusEffect(
    useCallback(() => {
      dispatch({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {username: '', password: ''},
      });
    }, [dispatch]),
  );

  return (
    <>
      <StatusBar
        barStyle={theme?.topAppBar.barStyle}
        backgroundColor={
          loading ? theme?.topAppBar.loading : theme?.topAppBar.backgroundColor
        }
      />
      {isActionOpen && (
        <Action
          {...testProps(ACTION_TESTID)}
          onTouchStart={handleActionDown}
          fall={fall}
        />
      )}
      <SingleScreen
        padForTablet={true}
        contents={
          <>
            <View>
              <Logo {...testProps('LoginScreenLogo')} />
              <StyledUsername />
              <StyledPassword />
              <StyledLoginButton />
              <StyledRecoverAccess {...testProps('LoginRecoverAccessTestId')} />
              <StyledModal {...testProps('LoginModalId')} />
              <Loader
                {...testProps('loaderTestId')}
                showLabel="Test label"
                isVisible={loading}
              />
            </View>
          </>
        }
        contentContainerStyle={containerStyle}
        bottom={
          <>
            <StyledBiometricsLogin />
            <StyledSignupVersion />
          </>
        }
        spaceFromBottom={260}
      />
      {keyboardVisible ? (
        <></>
      ) : (
        <BottomSheet
          snapPoints={[moderateScale(220, 0.85), moderateScale(35, 0.85)]}
          initialSnap={1}
          renderContent={renderContent}
          ref={bottomSheetRef}
          callbackNode={fall}
          onOpenStart={handleActionUp}
          onCloseStart={handleActionDown}
        />
      )}
      <StyledSnackbar />
    </>
  );
};

export default LoginScreen;
