/**
 * @flow
 */

import {StyleSheet} from 'react-native';

import {verticalScale, scale} from '@utils/scale';

type StylesType = {
  flex1: Object,
  handleCloseAction: Object,
  closeIcon: Object,
  renderContentContainer: Object,
  renderContentSubContainer: Object,
  container: Object,
};

const Styles: StylesType = StyleSheet.create({
  flex1: {
    flex: 1,
  },
  handleCloseAction: {
    position: 'absolute',
    backgroundColor: 'black',
    height: '100%',
    width: '100%',
    zIndex: 2,
  },
  closeIcon: {
    height: verticalScale(4),
    width: scale(54),
    borderRadius: 4,
    backgroundColor: 'black',
    marginTop: verticalScale(20),
  },
  renderContentContainer: {},
  renderContentSubContainer: {
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: '#102C4B',
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  container: {
    height: verticalScale(48),
  },
});

export default Styles;
