import React from 'react';
import {render} from '@testing-library/react-native';
import StyledRecoverAccess from '../components/StyledRecoverAccess';

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => jest.fn(),
  };
});

describe('StyledLoginButton', () => {
  it('Would work properly', () => {
    const all = render(<StyledRecoverAccess />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
