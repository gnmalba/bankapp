import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import StyledPassword from '../components/StyledPassword';

import MockProvider from '@utils/testFramework';
const mockStore = (err) => ({
  authentication: {
    username: 'test',
    password: err,
    usernameError: err,
    passwordError: err,
  },
});

describe('StyledPassword', () => {
  it.each(['', 'Test'])('Would work properly', (err) => {
    const all = render(
      <MockProvider store={mockStore(err)}>
        <StyledPassword />
      </MockProvider>,
    );
    const inputEl = all.getByTestId('passwordInputId');
    fireEvent(inputEl, 'onFocus');
    fireEvent(inputEl, 'onBlur');
    fireEvent(inputEl, 'onChangeText');
    fireEvent(inputEl, 'handleClear');

    expect(all.toJSON()).toMatchSnapshot();
  });
});
