import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import StyledUsername from '../components/StyledUsername';

import MockProvider from '@utils/testFramework';
const mockStore = (err) => ({
  authentication: {
    username: err,
    password: err,
    usernameError: err,
    passwordError: err,
  },
});

describe('StyledPassword', () => {
  it.each(['', 'test'])('Would work properly', (err) => {
    const all = render(
      <MockProvider store={mockStore(err)}>
        <StyledUsername />
      </MockProvider>,
    );
    const inputEl = all.getByTestId('userNameInputId');
    fireEvent(inputEl, 'onFocus');
    fireEvent(inputEl, 'onBlur');
    fireEvent(inputEl, 'onChangeText');
    fireEvent(inputEl, 'handleClear');

    expect(all.toJSON()).toMatchSnapshot();
  });
});
