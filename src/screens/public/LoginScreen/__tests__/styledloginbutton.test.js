import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import StyledLoginButton from '../components/StyledLoginButton';

import MockProvider from '@utils/testFramework';
const mockStore = (err) => ({
  authentication: {
    username: 'test',
    password: 'test',
    usernameError: err,
    passwordError: err,
  },
});

describe('StyledLoginButton', () => {
  it.each(['', 'Error'])('Would work properly', (err) => {
    const all = render(
      <MockProvider store={mockStore(err)}>
        <StyledLoginButton />
      </MockProvider>,
    );
    const btnEl = all.getByTestId('loginTestId');
    fireEvent(btnEl, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });
});
