import React from 'react';
import BottomSheetContent from '../components/BottomSheetContent';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

import {render} from '@testing-library/react-native';

describe('BottomSheetContent', () => {
  const defaultProps = {
    testID: 'bottomSheetContentTestId',
    isActionOpen: true,
    handleActionDown: () => {},
    handleActionUp: () => {},
  };
  it('should work properly', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <BottomSheetContent {...defaultProps} />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
