import React from 'react';
import {render} from '@testing-library/react-native';
import LoginScreen from '../LoginScreen';
import {DARK_THEME} from '@utils/constants';

jest.mock('@react-native-community/netinfo', () => ({
  useNetInfo: () => ({
    details: {
      strength: 100,
    },
    isConnected: true,
  }),
}));
jest.mock('jail-monkey', () => ({
  isJailBroken: () => false,
}));
jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => jest.fn(),
  };
});
import MockProvider from '@utils/testFramework';
import {NavigationContext} from '@react-navigation/core';
const mockStore = (err) => ({
  authentication: {
    username: 'Test',
    password: 'Test',
    usernameError: 'Err',
    passwordError: 'Err',
    loading: err === true,
  },
  biometrics: {
    deviceSensor: 'Fingerprint',
  },
  recoverAccess: {
    showUsernameSentSnackbar: false,
  },
});

const navContext = {
  isFocused: () => true,
  addListener: jest.fn(() => jest.fn()),
};

describe('LoginScreen', () => {
  it.each([undefined, true, false])('Should work as expected', (bools) => {
    const all = render(
      <NavigationContext.Provider value={navContext}>
        <MockProvider store={mockStore(bools)}>
          <LoginScreen initActionOpen={bools} theme={DARK_THEME} />
        </MockProvider>
      </NavigationContext.Provider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
