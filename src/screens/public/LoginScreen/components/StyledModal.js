// @flow

import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Modal from '@composition/Modal';
import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';
import * as S from '../Styled';
import * as A from '@ducks/authentication/actionTypes';

import type {AuthenticationType, State} from '@ducks/types';

export const StyledModal = (): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const [shouldBeVisible, setShouldBeVisible] = useState(false);
  const {
    showModal,
    error,
    modalPrimaryBtnAction,
    modalSecondaryBtnAction,
    showPrimaryModalBtn,
    showSecondaryModalBtn,
    modalPrimaryBtnText,
    modalSecondaryBtnText,
  } = authentication;
  const dispatch = useDispatch();

  const centerStyle = {textAlign: 'center'};

  useEffect(() => {
    let timeout;
    if (showModal) {
      // NOTE: Delay is needed for iOS modal issues
      timeout = setTimeout(() => {
        setShouldBeVisible(true);
      }, 300);
    } else {
      setShouldBeVisible(false);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [showModal]);

  return (
    <Modal visible={shouldBeVisible}>
      <S.ModalContainer>
        <Text mode="body1" style={centerStyle}>
          {error}
        </Text>
        {showPrimaryModalBtn && <Separator space={40} />}
        {showPrimaryModalBtn && (
          <Button
            type="primary"
            size="large"
            state="default"
            title={modalPrimaryBtnText || 'Okay'}
            onPress={() => {
              dispatch(modalPrimaryBtnAction);
              dispatch({
                type: A.AUTHENTICATION_UPDATE_STATE,
                newState: {
                  showModal: false,
                },
              });
            }}
          />
        )}
        {showSecondaryModalBtn && (
          <S.SecondaryBtnContainer>
            <Separator space={showPrimaryModalBtn ? 20 : 40} />
            <Button
              type="secondary"
              size="large"
              state="default"
              titleThemeColor={
                showPrimaryModalBtn ? 'TEXT_300_HIGH' : 'SECONDARY_DEFAULT'
              }
              title={modalSecondaryBtnText || "No, I'll do it again next time"}
              onPress={() => {
                dispatch(modalSecondaryBtnAction);
              }}
            />
          </S.SecondaryBtnContainer>
        )}
      </S.ModalContainer>
    </Modal>
  );
};

export default StyledModal;
