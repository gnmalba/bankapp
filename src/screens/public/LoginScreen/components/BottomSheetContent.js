// @flow

import React from 'react';
import {TouchableOpacity} from 'react-native';
import type {BottomSheetComponentType} from '../types';
import {testProps} from '@utils/componentCustomProps';

import {scale} from '@utils/scale';
import {
  StyledLocation,
  StyledQr,
  StyledBank,
  ContentContainer,
  OtherActionsContainer,
  BottomSheetTitle,
  OtherOptionsIconsContainer,
  CloseIcon,
  StyledIonIcon,
} from '../Styled';
import * as C from '../config';

const BottomSheetContent = (
  props: BottomSheetComponentType,
): React$Element<any> => {
  const {isActionOpen, handleActionDown, handleActionUp, testID} = props;

  return (
    <ContentContainer isActionOpen={isActionOpen}>
      <TouchableOpacity
        {...testProps(testID || C.SHEET_CONTENT_TESTID)}
        onPress={isActionOpen ? handleActionDown : handleActionUp}>
        {isActionOpen ? (
          <CloseIcon />
        ) : (
          <StyledIonIcon name="chevron-up" size={scale(32)} />
        )}
      </TouchableOpacity>
      <OtherActionsContainer {...testProps('otherActionsContainer')}>
        <BottomSheetTitle {...testProps('otherActionsTitle')}>
          Other Actions
        </BottomSheetTitle>
        <OtherOptionsIconsContainer>
          <StyledLocation
            {...testProps('otherActionsButtonATMBranchLocator')}
          />
          <StyledQr {...testProps('otherActionsButtonQRCode')} />
          <StyledBank {...testProps('otherActionsButtonOpenBankAccount')} />
        </OtherOptionsIconsContainer>
      </OtherActionsContainer>
    </ContentContainer>
  );
};

export default BottomSheetContent;
