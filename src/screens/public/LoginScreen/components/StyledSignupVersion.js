// @flow

import React from 'react';
import {TouchableOpacity} from 'react-native';

import Text from '@components/Text';
import Separator from '@components/Separator';
import {testProps} from '@utils/componentCustomProps';
import {BottomContainer, SignUpContainer, StyledSignUp} from '../Styled';
import {BUILD_VERSION} from 'react-native-dotenv';

export const StyledSignupVersion = (): React$Node => {
  return (
    <BottomContainer>
      <SignUpContainer>
        <Text
          {...testProps('signUpMessageTestId')}
          mode="body2"
          themeColor="TEXT_300_HIGH">
          {"Don't have an account yet? "}
        </Text>
        <TouchableOpacity {...testProps('signUpLinkTestId')} onPress={() => {}}>
          <StyledSignUp mode="custom" themeColor="SECONDARY_DEFAULT">
            Sign Up
          </StyledSignUp>
        </TouchableOpacity>
      </SignUpContainer>

      <Separator space={8} />
      <Text
        {...testProps('versionTestId')}
        mode="overline2"
        themeColor="TEXT_300_LOW">
        {`VERSION ${BUILD_VERSION}`}
      </Text>
    </BottomContainer>
  );
};

export default StyledSignupVersion;
