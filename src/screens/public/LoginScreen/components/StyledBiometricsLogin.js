/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import React, {useEffect} from 'react';
import {AppState, Platform, Pressable} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';

import Text from '@components/Text';
import * as S from '../Styled';
import {testProps} from '@utils/componentCustomProps';
import * as types from '@ducks/biometrics/actionTypes';
import SendIntentAndroid from 'react-native-send-intent';

import type {State} from '@ducks/types';

export const StyledBiometricsLogin = (): React$Node => {
  const {biometrics, authentication} = useSelector((state: State) => state);
  const {
    deviceSensor,
    isBiometricsEnrolled,
    biometricsError,
    sendAndroidIntent,
    linkDisabled,
  } = biometrics;
  const {encryptionPublicKey} = authentication;
  const showModal: boolean = useSelector(
    (state: State) => state.authentication.showModal,
  );
  const dispatch = useDispatch();

  const handleAppStateChange = (nextState = 'active') => {
    if (nextState === 'active') {
      dispatch({
        type: types.BIOMETRICS_GET_SENSOR,
      });
      dispatch({
        type: types.BIOMETRICS_UPDATE_STATE,
        newState: {
          sendAndroidIntent: false,
        },
      });
    }
  };

  const handlePress = () => {
    dispatch({
      type: types.BIOMETRICS_START_FLOW,
      payload: {
        isBiometricsEnrolled,
        biometricsError,
        deviceSensor,
        encryptionPublicKey,
      },
    });
  };

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);
    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, []);

  useEffect(() => {
    if (!showModal && sendAndroidIntent && Platform.OS === 'android') {
      SendIntentAndroid.openSettings('android.settings.SECURITY_SETTINGS');
    }
  }, [showModal, sendAndroidIntent]);

  const getLinkDisabledProps = () => ({
    themeColor: linkDisabled ? 'DISABLED_STATUS' : 'TEXT_300_HIGH',
    state: linkDisabled ? 'disabled' : 'default',
  });

  return deviceSensor ? (
    <S.BiometricsContainer>
      <S.BiometricsIcon />
      <S.BiometricsLinkContainer>
        <Pressable
          {...testProps('biometricsLogin')}
          onPress={handlePress}
          disabled={linkDisabled}>
          <Text mode="body3" {...getLinkDisabledProps()}>
            {`Login using ${deviceSensor}`}
          </Text>
        </Pressable>
      </S.BiometricsLinkContainer>
    </S.BiometricsContainer>
  ) : (
    <></>
  );
};

export default StyledBiometricsLogin;
