// @flow

import React from 'react';
import {TouchableOpacity} from 'react-native';

import Text from '@components/Text';
import * as S from '../Styled';
import {testProps} from '@utils/componentCustomProps';
import {useNavigation} from '@react-navigation/native';

export const StyledRecoverAccess = (): React$Node => {
  const navigation = useNavigation();

  return (
    <S.RecoverAccessContainer>
      <TouchableOpacity
        {...testProps('recoverAccessTestId')}
        onPress={() => {
          navigation.navigate('Recover Access');
        }}>
        <Text mode="body3" themeColor="SECONDARY_DEFAULT">
          Recover Access
        </Text>
      </TouchableOpacity>
    </S.RecoverAccessContainer>
  );
};

export default StyledRecoverAccess;
