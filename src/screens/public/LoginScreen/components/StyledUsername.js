// @flow

import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import TextInput from '@composition/TextInput';

import {
  AUTHENTICATION_SET_USERNAME,
  AUTHENTICATION_UPDATE_STATE,
} from '@ducks/actionTypes';
import type {State, AuthenticationType} from '@ducks/types';
import {UsernameContainer} from '../Styled';
import {testProps} from '@utils/componentCustomProps';

// const ENTER_VALID_USERNAME = 'Please enter a valid username.';

export const StyledUsername = (): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const {username, usernameError} = authentication;
  const [inputState, setInputState] = useState('default');
  const dispatch = useDispatch();

  const setUsername = (text: string) => {
    dispatch({
      type: AUTHENTICATION_SET_USERNAME,
      username: text,
    });
  };

  const setUsernameError = (error: string) => {
    dispatch({
      type: AUTHENTICATION_UPDATE_STATE,
      newState: {usernameError: error},
    });
  };

  const checkForErrors = () => {
    // let error = /[ `!@#$%^&*()+\-=[\]{};':"\\|,.<>/?~]/g.test(username)
    //   ? ENTER_VALID_USERNAME
    //   : '';
    // error = username.length < 8 ? ENTER_VALID_USERNAME : error;
    const error = username.length === 0 ? 'Please enter your username.' : '';
    setUsernameError(error);
    setInputState(error === '' ? 'default' : 'error');
  };

  return (
    <UsernameContainer>
      <TextInput
        label="Username"
        {...testProps('userNameInputId')}
        state={inputState}
        icon="username"
        placeholder="Enter username"
        onChangeText={(text) => {
          setUsername(text);
        }}
        helperText={usernameError}
        value={username}
        handleClear={() => {
          setUsername('');
        }}
        onFocus={() => {
          setUsernameError('');
          setInputState('active');
        }}
        onBlur={() => {
          checkForErrors();
        }}
        textWidth="80%"
      />
    </UsernameContainer>
  );
};

export default StyledUsername;
