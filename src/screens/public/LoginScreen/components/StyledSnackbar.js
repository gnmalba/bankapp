/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Snackbar from '@components/Snackbar';
import * as types from '@ducks/authentication/actionTypes';
import {RECOVER_UPDATE_STATE} from '@ducks/recover-access/actionTypes';

import type {AuthenticationType, State} from '@ducks/types';
import {testProps} from '@utils/componentCustomProps';

type PropTypes = {
  testID?: string,
};

export const StyledSnackbar = (props: PropTypes): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const showUsernameSentSnackbar = useSelector(
    (state) => state.recoverAccess.showUsernameSentSnackbar,
  );
  const {testID} = props;
  const {showSnackbar, snackbarMessage} = authentication;
  const dispatch = useDispatch();

  useEffect(() => {
    const timeHandler = setTimeout(() => {
      dispatch({
        type: types.AUTHENTICATION_UPDATE_STATE,
        newState: {
          showSnackbar: false,
        },
      });
      dispatch({
        type: RECOVER_UPDATE_STATE,
        updatedState: {
          showUsernameSentSnackbar: false,
        },
      });
    }, 5000);
    return () => {
      clearTimeout(timeHandler);
    };
  }, [showSnackbar, showUsernameSentSnackbar]);

  return (
    <Snackbar
      visible={showSnackbar || showUsernameSentSnackbar}
      message={
        showSnackbar
          ? snackbarMessage
          : 'You should receive an email shortly with further instructions.'
      }
      {...testProps(testID || 'snackBarTestId')}
    />
  );
};

export default StyledSnackbar;
