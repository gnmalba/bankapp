// @flow

import React from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Button from '@components/Button';

import type {State, AuthenticationType} from '@ducks/types';
import * as types from '@ducks/authentication/actionTypes';
import * as S from '../Styled';
import {testProps} from '@utils/componentCustomProps';
import {ACCOUNT_LOCKED, INVALID_USER_CREDENTIALS_MSG} from '@utils/constants';
import type {StyledLoginButtonProps} from '../types';

export const StyledLoginButton = (
  props: StyledLoginButtonProps,
): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const dispatch = useDispatch();
  const {
    usernameError,
    passwordError,
    username,
    password,
    encryptionPublicKey,
    error,
  } = authentication;
  const {title, onPress} = props;

  const getButtonState = () => {
    return !usernameError &&
      !passwordError &&
      username !== '' &&
      password !== '' &&
      error !== INVALID_USER_CREDENTIALS_MSG &&
      error !== ACCOUNT_LOCKED
      ? // && password.length >= 8
        'default'
      : 'disabled';
  };

  return (
    <S.ButtonContainer>
      <Button
        {...testProps('loginTestId')}
        title={title || 'Login'}
        type="primary"
        state={getButtonState()}
        size="large"
        onPress={() => {
          if (onPress) {
            onPress();
          }
          dispatch({
            type: types.AUTHENTICATION_START_LOGIN,
            payload: {
              username,
              password,
              encryptionPublicKey,
            },
          });
        }}
      />
    </S.ButtonContainer>
  );
};

export default StyledLoginButton;
