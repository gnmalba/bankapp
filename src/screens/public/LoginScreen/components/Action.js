// @flow

import React from 'react';
import Animated from 'react-native-reanimated';

import type {ActionType} from '@screens/public/LoginScreen/types';
import {ACTION_TEST_ID} from '@screens/public/LoginScreen/config';
import Styles from '@screens/public/LoginScreen/styles';

import {testProps} from '@utils/componentCustomProps';

const Action = (props: ActionType): React$Element<any> => {
  const {onTouchStart, style, testID, fall} = props;

  return (
    <Animated.View
      {...testProps(testID)}
      onTouchStart={onTouchStart}
      style={[
        Styles.handleCloseAction,
        style,
        {opacity: Animated.add(0.3, Animated.multiply(fall, -0.3))},
      ]}
    />
  );
};

Action.defaultProps = {
  testID: ACTION_TEST_ID,
  accessibilityLabel: ACTION_TEST_ID,
};

export default Action;
