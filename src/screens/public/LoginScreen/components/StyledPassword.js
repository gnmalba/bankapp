// @flow

import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import PasswordInput from '@composition/PasswordInput';

import * as types from '@ducks/actionTypes';
import type {State, AuthenticationType} from '@ducks/types';
import * as S from '../Styled';
import {testProps} from '@utils/componentCustomProps';

// const ENTER_VALID_PASSWORD = 'Please enter a valid password.';

export const StyledPassword = (): React$Node => {
  const authentication: AuthenticationType = useSelector(
    (state: State) => state.authentication,
  );
  const {password, passwordError} = authentication;
  const [inputState, setInputState] = useState('default');
  const dispatch = useDispatch();

  const setPassword = (text: string) => {
    dispatch({
      type: types.AUTHENTICATION_SET_PASSWORD,
      password: text,
    });
  };

  const setPasswordError = (error: string) => {
    dispatch({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {passwordError: error},
    });
  };

  const checkForErrors = () => {
    // let error = /\s/g.test(password) ? ENTER_VALID_PASSWORD : '';
    // error = !/[a-z]/g.test(password) ? ENTER_VALID_PASSWORD : error;
    // error = !/[A-Z]/g.test(password) ? ENTER_VALID_PASSWORD : error;
    // error = !/[0-9]/g.test(password) ? ENTER_VALID_PASSWORD : error;
    // error = !/[ `!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/g.test(password)
    //   ? ENTER_VALID_PASSWORD
    //   : error;
    // error = password.length < 8 ? ENTER_VALID_PASSWORD : error;
    const error = password.length === 0 ? 'Please enter your password.' : '';
    setPasswordError(error);
    setInputState(error === '' ? 'default' : 'error');
  };

  return (
    <S.PasswordContainer>
      <PasswordInput
        {...testProps('passwordInputId')}
        onChangeText={(text) => {
          setPassword(text);
        }}
        handleClear={() => {
          setPassword('');
        }}
        state={inputState}
        helperText={passwordError}
        value={password}
        onFocus={() => {
          setPasswordError('');
          setInputState('active');
        }}
        onBlur={() => {
          checkForErrors();
        }}
        textWidth="78%"
      />
    </S.PasswordContainer>
  );
};

export default StyledPassword;
