/**
 * @flow
 */

import type {accessibilityType} from '@utils/commonTypes';

export type PropsType = {
  initActionOpen?: boolean,
  theme?: Object,
  navigation: {
    navigate: any,
  },
};

export type ActionType = {
  ...accessibilityType,
  onTouchStart: () => void,
  style?: Object,
  fall: any,
};

export type BottomSheetType = {
  onOpenStart?: () => void,
  onCloseEnd?: () => void,
  bottomSheetRef?: any,
  fall?: any,
  testID?: string,
  accessibilityLabel?: string,
  renderContent?: any,
  style?: Object,
};

export type BottomSheetComponentType = {
  ...accessibilityType,
  isActionOpen?: boolean,
  handleActionDown?: () => void,
  handleActionUp?: () => void,
};

export type StyledLoginButtonProps = {
  title?: string,
  onPress?: () => void,
};
