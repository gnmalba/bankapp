## OnboardingScreen
OnboardingScreen component.

# Usage
```js
import OnboardingScreen from '@screens/public/OnboardingScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
