import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import OnboardingScreen from '../OnboardingScreen';
import MockProvider from '@utils/testFramework';

describe('OnboardingScreen', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider>
        <OnboardingScreen navigation={{navigate: () => {}, reset: () => {}}} />
      </MockProvider>,
    );
    const el = all.getByTestId('OnboardingContinueLoginButton');
    fireEvent(el, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });
});
