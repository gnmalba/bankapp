/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  navigation: {
    navigate: (route: string, params?: Object) => void,
    reset: (params?: Object) => void,
  },
};
