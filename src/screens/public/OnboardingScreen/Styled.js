/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale, scale} from '@utils/scale';

export const ContentContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const RenderContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const ImageContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(49)}px;
  margin-bottom: ${verticalScale(49)}px;
`;

export const TextContainer: ComponentType<any> = styled.View`
  margin-left: ${scale(5)}px;
  margin-right: ${scale(5)}px;
`;
