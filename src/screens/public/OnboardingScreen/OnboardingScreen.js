/**
 * @format
 * @flow
 */

import React from 'react';
import {Image, StatusBar} from 'react-native';
import Swiper from 'react-native-swiper';
import {useDispatch} from 'react-redux';

import CenteredSingleScreen from '@composition/CenteredSingleScreen';
import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {verticalScale, scale} from '@utils/scale';
import OB_LIST, {whatsNew} from './config';
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import * as S from './Styled';
import * as A from '@ducks/authentication/actionTypes';
import * as B from '@ducks/biometrics/actionTypes';

const OnboardingScreen = (props: PropsType): React$Node => {
  const {theme, navigation} = props;
  const dispatch = useDispatch();
  const renderItem = (item) => {
    return (
      <S.RenderContainer
        {...testProps('OnboardingContainer' + item.id)}
        key={item.id}>
        <S.ImageContainer {...testProps('OnboardingImage' + item.id)}>
          <Image
            source={item.image}
            style={{height: item.height, width: item.width}}
          />
        </S.ImageContainer>
        <S.TextContainer>
          <Text mode="h3" {...testProps('OnboardingTitle' + item.id)}>
            {item.subTitle}
          </Text>
          <Separator
            {...testProps('OnboardingSeperator' + item.id)}
            space={12}
          />
          <Text
            {...testProps('OnboardingDescription' + item.id)}
            mode="body1"
            themeColor="TEXT_100_HIGH">
            {item.description}
          </Text>
        </S.TextContainer>
      </S.RenderContainer>
    );
  };

  const renderContents = () => {
    const dotStyle = {
      height: scale(8),
      width: scale(8),
      marginBottom: 1,
      borderRadius: scale(4),
      marginRight: scale(5),
      marginLeft: scale(5),
    };
    const activeDotStyle = {
      height: scale(8),
      width: scale(8),
      borderRadius: scale(4),
      marginBottom: 1,
      backgroundColor: '#09BAD0',
      marginRight: scale(5),
      marginLeft: scale(5),
    };
    return (
      <S.RenderContainer>
        <StatusBar backgroundColor={theme?.color.BACKGROUND_200} />
        <Separator space={31} />
        <Text {...testProps('OnboardingHeader')} mode="h2">
          {whatsNew}
        </Text>
        {
          // $FlowFixMe[not-a-component]
          <Swiper
            loop={false}
            dotColor="#445870"
            paginationStyle={{
              bottom: verticalScale(41),
            }}
            dotStyle={dotStyle}
            activeDotStyle={activeDotStyle}>
            {OB_LIST.map((item) => renderItem(item))}
          </Swiper>
        }
      </S.RenderContainer>
    );
  };

  const renderAction = () => {
    return (
      <S.RenderContainer>
        <Button
          {...testProps('OnboardingContinueLoginButton')}
          type="secondary"
          size="large"
          state="default"
          title="Continue to login screen"
          titleThemeColor="SECONDARY_DEFAULT"
          onPress={() => {
            dispatch({
              type: A.AUTHENTICATION_UPDATE_STATE,
              newState: {
                onboarded: true,
              },
            });
            dispatch({
              type: B.BIOMETRICS_UPDATE_STATE,
              newState: {
                linkDisabled: true,
              },
            });
            AsyncStorage.setItem('onboarded', 'true');
            AsyncStorage.setItem('biometricsLinkDisabled', 'true');
            navigation.navigate('Login');
            navigation.reset({
              index: 0,
              key: null,
              routes: [{name: 'Login'}],
            });
          }}
        />
      </S.RenderContainer>
    );
  };

  return (
    <CenteredSingleScreen contents={renderContents()} action={renderAction()} />
  );
};

export default OnboardingScreen;
