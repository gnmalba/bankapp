// @flow

export const whatsNew = 'What’s new with Metrobank';
import {moderateScale} from '@utils/scale';

type ListType = {
  id: number,
  title: string,
  image: number,
  subTitle: string,
  description: string,
  width: number,
  height: number,
};

const OB_LIST: ListType[] = [
  {
    id: 1,
    title: whatsNew,
    image: parseInt(require('@assets/images/Group-176.png'), 10),
    subTitle: 'Dashboard Customization',
    description:
      'You can now have more control on what appears first on your dashboard. Add and move your favorite functions.',
    width: moderateScale(151),
    height: moderateScale(170),
  },
  {
    id: 2,
    title: whatsNew,
    image: parseInt(require('@assets/images/Group-187.png'), 10),
    subTitle: 'All your accounts in one place',
    description: 'Have a convenient way of viewing all your Metrobank accounts',
    width: moderateScale(200),
    height: moderateScale(196),
  },
  {
    id: 3,
    title: whatsNew,
    image: parseInt(require('@assets/images/Character-1.png'), 10),
    subTitle: 'A better online banking experience',
    description:
      'Bank at your convenience. Use your smartphone or PC to send money, trade, and pay bills.',
    width: moderateScale(181),
    height: moderateScale(207),
  },
];

export default OB_LIST;
