/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  title?: string,
  body?: string,
  type?: string,
  route?: {
    params?: Object,
  },
  navigation?: {
    navigate: (screen: string) => void,
  },
};
