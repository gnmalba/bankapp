/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale, moderateScale} from '@utils/scale';

export const StyledScrollView: ComponentType<any> = styled.ScrollView``;

export const IconContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const TitleContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(40)}px;
  margin-bottom: ${verticalScale(30)}px;
  align-items: center;
  justify-content: center;
`;

export const Icon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: props.source,
}))`
  width: ${moderateScale(75)}px;
  height: ${moderateScale(75)}px;
`;

export const ButtonContainer: ComponentType<any> = styled.TouchableOpacity`
  margin-top: ${verticalScale(40)}px;
  margin-bottom: ${verticalScale(40)}px;
`;
