## AdvisoryDetail
AdvisoryDetail component.

# Usage
```js
import AdvisoryDetail from '@screens/public/AdvisoryDetail';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
