/**
 * @format
 * @flow
 */

import React from 'react';

import FormLayout from '@composition/FormLayout';
import Text from '@components/Text';
import Button from '@components/Button';

import {moderateScale} from '@utils/scale';
import HTMLView from 'react-native-htmlview';

import {typeImageMap} from '@screens/public/Advisory/config';
import type {PropsType} from './types';

import {
  StyledScrollView,
  TitleContainer,
  IconContainer,
  ButtonContainer,
  Icon,
} from './Styled';

const AdvisoryDetail = (props: PropsType): React$Node => {
  const {title, body, type} = props.route?.params || {};

  const handleNavigateToLogin = () => {
    const {navigation} = props;
    navigation && navigation.navigate('Login');
  };

  return (
    <FormLayout>
      <StyledScrollView>
        <IconContainer>
          <Icon source={typeImageMap[type || 'announcement']} />
        </IconContainer>
        <TitleContainer>
          <Text mode="h2">{title}</Text>
        </TitleContainer>

        <HTMLView
          value={body}
          stylesheet={{
            span: {
              color: '#A2BDDB',
              fontSize: moderateScale(14),
            },
            p: {
              color: '#A2BDDB',
              fontSize: moderateScale(14),
            },
          }}
        />

        <ButtonContainer>
          <Button
            title="Proceed"
            type="primary"
            state="default"
            onPress={handleNavigateToLogin}
          />
        </ButtonContainer>
      </StyledScrollView>
    </FormLayout>
  );
};

export default AdvisoryDetail;
