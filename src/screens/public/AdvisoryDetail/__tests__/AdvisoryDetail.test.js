import React from 'react';
import {render} from '@testing-library/react-native';
import AdvisoryDetail from '../AdvisoryDetail';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('AdvisoryDetail', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <AdvisoryDetail />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
