// @flow

export type PropsType = {
  navigation: any,
  recoverAccess: Object,
  route?: Object,
  authentication?: Object,
};

export type CustomLayoutType = {
  title: string,
  description: string,
  children: React$Element<any>,
};

export type ButtonProps = {
  label?: string,
  onPress?: () => void,
  containerStyle?: Object,
  buttonStyle?: Object,
  textStyle?: Object,
};

export type ActionsType = {
  btnLabel: string,
  btnPress?: () => void,
  textPress?: () => void,
  txtLabel?: string | null,
  btnDisabled?: boolean,
  testID?: string,
};

export type PasscodeType = {
  onTextChange: (text: string) => void,
  passcodeError?: boolean,
  passcode?: string,
  testID?: string,
  onCallPress?: () => {},
  password?: boolean,
  label?: string,
};

export type UsernameEmailType = {
  error?: string,
  borderColor?: string,
  showError?: boolean,
  renderIcon?: React$Element<any>,
  maxInputLength?: number,
  handleBlur?: () => void,
  handleFocus?: () => void,
  placeholderText?: string,
  inputStyle?: Object,
  letterSpacing?: number,
  fontSize?: number,
  textAlign?: any,
  value?: string,
  keyboardType?: any,
  handleChange?: () => void,
  testID?: string,
  accessibilityLabel?: string,
  icon?: any,
  redStatus?: boolean,
  label?: string,
  usernameEmail?: string,
};

export type PasswordType = {
  handleChange: (text: string) => void,
  password?: string,
  onEndEditing?: () => void,
  onBlur?: () => void,
  label?: string,
  showError?: boolean,
  error?: string,
  fontSize?: number,
};

export type RecoveredType = {
  navigation: any,
};

export type MaximumInvalidType = {
  navigation: any,
  recoverAccess: Object,
};

export type SuccessType = {
  testID: string,
  accessibilityLabel?: string,
  title?: string,
  description?: string,
  btnLabel?: string,
  handleNavigate?: () => any,
};
