/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import HTMLView from 'react-native-htmlview';
import {moderateScale} from '@utils/scale';

export const StyledPressable: ComponentType<any> = styled.Pressable``;

export const SendAgainContainer: ComponentType<any> = styled.View`
  flex-direction: row;
`;

export const ExceedMessage: ComponentType<any> = styled(HTMLView).attrs(
  (props) => ({
    stylesheet: {
      p: {
        color: props.theme?.color?.TEXT_300_HIGH || 'transparent',
        fontSize: moderateScale(14),
      },
      span: {color: props.theme?.color?.TEXT_100_HIGH},
      b: {fontWeight: 'bold'},
    },
  }),
)``;
