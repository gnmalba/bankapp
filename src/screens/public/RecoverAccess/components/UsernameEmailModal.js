// @flow

import React from 'react';
import {View} from 'react-native';

import Modal from '@composition/Modal';
import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';

import {ModalActionContainer} from '@screens/private/Dashboard/Styled';
import {verticalScale} from '@utils/scale';

import {testProps} from '@utils/componentCustomProps';

const centerText = {
  textAlign: 'center',
};

type PropsType = {
  txt?: string,
  btnTxt?: string,
  onPress?: () => void,
  visible?: boolean,
};

const UsernameEmailModal = (props: PropsType): React$Node => {
  const {txt, btnTxt, onPress, visible} = props;

  return (
    <Modal visible={visible}>
      <View {...testProps('usernameEmailErrorContainer')}>
        <Text
          mode="h3"
          themeColor="TEXT_300_HIGH"
          style={centerText}
          {...testProps('usernameEmailErrorText')}>
          {txt}
        </Text>
        <Separator space={verticalScale(34)} />
        <ModalActionContainer>
          <Button
            {...testProps('usernameEmailModalOkayBtn')}
            type="secondary"
            title={btnTxt || 'Okay'}
            titleThemeColor="SECONDARY_DEFAULT"
            onPress={onPress}
          />
        </ModalActionContainer>
      </View>
    </Modal>
  );
};

export default UsernameEmailModal;
