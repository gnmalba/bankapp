/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {moderateScale} from '@utils/scale';

export const ImageContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const StyledImage: ComponentType<any> = styled.Image.attrs((props) => ({
  source: props.source,
  resizeMode: 'contain',
}))`
  width: ${moderateScale(62)}px;
  height: ${moderateScale(62)}px;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  width: 100%;
`;
