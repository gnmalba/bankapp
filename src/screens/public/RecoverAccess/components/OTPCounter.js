// @flow

import React from 'react';

import Text from '@components/Text';
import Separator from '@components/Separator';

import {testProps} from '@utils/componentCustomProps';

import {
  SEND_ANOTHER_CODE_TEST_ID,
  OTP_SEND_AGAIN_EXCEEDED_MSG,
  SEND_AGAIN,
  SEND_ANOTHER_CODE,
  SENT_NEW_CODE,
} from '../config';

import {
  StyledPressable,
  SendAgainContainer,
  ExceedMessage,
} from './OTPCounterStyled';

type PropsType = {
  sendOtpCounter: boolean,
  handleSendOtp: () => void,
  sendOtpExceed: boolean,
  error: any,
  remaining: number,
};

const OTPCounter = (props: PropsType): React$Node => {
  const {
    sendOtpCounter,
    handleSendOtp,
    sendOtpExceed,
    error,
    remaining,
  } = props;

  const remainingStr = remaining.toString();
  const timerDisplay =
    remainingStr.length === 2 ? remainingStr : '0' + remainingStr;
  const timerOnGoing = remaining > 0;

  const getDisplayType = (hidden: boolean) => ({
    display: hidden ? 'none' : 'flex',
  });

  return (
    <StyledPressable
      {...testProps(SEND_ANOTHER_CODE_TEST_ID)}
      onPress={handleSendOtp}
      disabled={timerOnGoing || sendOtpExceed}>
      <SendAgainContainer>
        <Separator space={10} />
        <Text
          mode="buttonlink"
          themeColor={
            sendOtpExceed || timerOnGoing ? 'DISABLED_STATUS' : 'TEXT_100_HIGH'
          }>
          {sendOtpCounter ? SEND_AGAIN : SEND_ANOTHER_CODE}
        </Text>
        {remaining <= 60 ? (
          <Text
            mode="body2"
            themeColor="TEXT_300_HIGH"
            style={getDisplayType(!timerOnGoing)}>
            {` (00:${timerDisplay})`}
          </Text>
        ) : (
          <></>
        )}
      </SendAgainContainer>
      <Text
        themeColor="TEXT_300_HIGH"
        mode="body2"
        style={{
          ...{paddingTop: 20},
          ...getDisplayType(!sendOtpCounter || error || sendOtpExceed),
        }}>
        {SENT_NEW_CODE}
      </Text>
      <Separator space={20} />
      <ExceedMessage
        style={getDisplayType(!sendOtpExceed)}
        value={OTP_SEND_AGAIN_EXCEEDED_MSG}
      />
    </StyledPressable>
  );
};

export default OTPCounter;
