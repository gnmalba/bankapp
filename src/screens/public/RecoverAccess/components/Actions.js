/**
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import Button from '@components/Button';
import {testProps} from '@utils/componentCustomProps';

import type {ActionsType} from '../types';
import {TxtLabel} from '../Styled';

const Actions = (props: ActionsType): React$Element<any> => {
  const {btnLabel, btnPress, txtLabel, textPress, btnDisabled, testID} = props;

  return (
    <View {...testProps('actionsContainerTestId')}>
      <Button
        testID={testID}
        title={btnLabel}
        onPress={btnPress}
        type="primary"
        size="large"
        state={btnDisabled ? 'disabled' : 'default'}
      />
      {txtLabel && (
        <TouchableOpacity
          testID={testID ? `text${testID}` : 'actionTextTestId'}
          onPress={textPress}>
          <TxtLabel themeColor="TEXT_100_HIGH" mode="buttontext">
            {txtLabel}
          </TxtLabel>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default Actions;
