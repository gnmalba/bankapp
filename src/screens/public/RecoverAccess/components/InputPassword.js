// @flow

import React from 'react';
import {useSelector} from 'react-redux';

import PasswordInput from '@composition/PasswordInput';

import {testProps} from '@utils/componentCustomProps';

type PropsType = {
  onChangeText: (txt: string) => void,
  handleClear: (txt: string) => void,
  onEndEditing: () => void,
  inputState: string,
  setInputState: (str: string) => void,
};

const InputPassword = (props: PropsType): React$Node => {
  const {
    onChangeText,
    handleClear,
    onEndEditing,
    inputState,
    setInputState,
  } = props;
  const password = useSelector((state) => state.recoverAccess.password);

  return (
    <PasswordInput
      {...testProps('passwordInputId')}
      label="New password"
      onChangeText={(text) => {
        onChangeText(text);
      }}
      handleClear={() => {
        handleClear('');
      }}
      state={inputState}
      value={password}
      onFocus={() => {
        setInputState('active');
      }}
      onBlur={() => {
        setInputState('default');
      }}
      onEndEditing={onEndEditing}
      textWidth="78%"
    />
  );
};

export default InputPassword;
