// @flow

import React, {useEffect} from 'react';
import {View} from 'react-native';

import CenteredSingleScreen from '@composition/CenteredSingleScreen';
import Button from '@components/Button';
import Text from '@components/Text';
import Separator from '@components/Separator';
import {testProps} from '@utils/componentCustomProps';
import {useDispatch} from 'react-redux';

import {SET_PASSWORD_RECOVERED} from '@ducks/recover-access/actionTypes';
import {
  StyledImage,
  ImageContainer,
  ButtonContainer,
} from './MessageScreenStyled';

type SuccessType = {
  image?: any,
  title?: string,
  description?: string,
  btnLabel?: string,
  handleNavigate?: () => void,
  testID?: string,
};

const MessageScreen = (props: SuccessType): React$Element<any> => {
  const {image, title, description, btnLabel, handleNavigate, testID} = props;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: SET_PASSWORD_RECOVERED,
      status: false,
    });
  }, [dispatch]);

  const centerAlign = {
    textAlign: 'center',
  };

  const forTest = testID || 'messageScreen';

  return (
    <CenteredSingleScreen
      contents={
        <>
          <View>
            <ImageContainer>
              <StyledImage source={image} />
            </ImageContainer>
            <Separator space={20} />
            {title ? (
              <Text
                mode="h3"
                style={centerAlign}
                themeColor="TEXT_300_HIGH"
                {...testProps(forTest + 'Title')}>
                {title}
              </Text>
            ) : (
              <></>
            )}
            <Separator space={15} />
            <Text
              mode="body2"
              style={centerAlign}
              themeColor="TEXT_100_HIGH"
              {...testProps(forTest + 'Description')}>
              {description}
            </Text>
          </View>
          <Separator space={40} />
          <ButtonContainer>
            <Button
              {...testProps(forTest + 'Button')}
              title={btnLabel}
              type="primary"
              size="large"
              state="default"
              onPress={handleNavigate}
            />
          </ButtonContainer>
        </>
      }
    />
  );
};

export default MessageScreen;
