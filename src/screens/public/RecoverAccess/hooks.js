// @flow

import {useCallback} from 'react';
import {BackHandler} from 'react-native';
import {useNavigation, useFocusEffect} from '@react-navigation/native';

export const useBackHandler = (route: string, screen?: string = ''): any => {
  const navigation = useNavigation();
  return useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        navigation.navigate(route, screen ? {screen} : {});
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation, route, screen]),
  );
};
