/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Text from '@components/Text';
import {moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View``;

export const ContentsContainer: ComponentType<any> = styled.View``;

export const TxtLabel: ComponentType<any> = styled(Text)`
  text-align: center;
  padding: ${moderateScale(20)}px;
`;
