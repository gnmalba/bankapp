/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useDispatch, useSelector} from 'react-redux';

import Text from '@components/Text';
import Loader from '@composition/Loader';
import Separator from '@components/Separator';
import TextInput from '@composition/TextInput';
import SingleScreen from '@composition/SingleScreen';

import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from '../types';
import {ContentsContainer} from '../Styled';
import {
  SET_USERNAME_EMAIL,
  START_RECOVER_PASSWORD,
  RECOVER_UPDATE_STATE,
} from '@ducks/recover-access/actionTypes';

import {SUBMIT, BACK_TO_LOGIN, RESET_ONCE_MSG} from '../config';
import {
  LOGIN,
  NAVIGATION_MAXIMUMINVALID,
  NAVIGATION_ENTERPASSCODE,
} from '@utils/constants';
import {PASSCODE_ATTEMPTS_EXCEEDED} from '@ducks/recover-access/config';

import Actions from '../components/Actions';
import UsernameEmailModal from '../components/UsernameEmailModal';

const EnterUsernameEmail = (props: PropsType): React$Node => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const recoverAccess = useSelector((state) => state.recoverAccess);
  const {
    usernameEmail,
    usernameEmailError,
    recoverType,
    successfulPasswordReset,
    loading,
    displayError,
    error,
    proceedToPasscode,
  } = recoverAccess;
  const [inputState, setInputState] = useState('default');
  const [isErrorVisible, setIsErrorVisible] = useState(false);
  const [usernameError, setUsernameError] = useState('');

  useEffect(() => {
    proceedToPasscode && navigation.navigate(NAVIGATION_ENTERPASSCODE);
  }, [proceedToPasscode, navigation]);

  useEffect(() => {
    error === PASSCODE_ATTEMPTS_EXCEEDED &&
      navigation.navigate(NAVIGATION_MAXIMUMINVALID);
  }, [error, navigation]);

  const setUsername = (text: string) => {
    dispatch({
      type: SET_USERNAME_EMAIL,
      usernameEmail: text,
    });
  };

  const checkForErrors = () => {
    const localErr =
      usernameEmail.length === 0 ? 'Please enter your username.' : '';
    setUsernameError(localErr);
    setInputState(localErr === '' ? 'default' : 'error');
  };

  const checkResetOnce = () => {
    const label = successfulPasswordReset[usernameEmail];
    if (label) {
      const recoveredDate = new Date(label).toDateString();
      const todaysDate = new Date().toDateString();
      return todaysDate !== recoveredDate;
    }
    return true;
  };

  const renderContents = () => (
    <ContentsContainer>
      <Text mode="h2">Enter username or email</Text>
      <Separator space={8} />
      <Text mode="body3" themeColor="TEXT_100_HIGH">
        Enter your username or email address and we will send you instructions
        on how to create a new password
      </Text>
      <Separator space={40} />
      <TextInput
        label="Username or email"
        {...testProps('userNameInputId')}
        state={inputState}
        icon="username"
        placeholder="Enter username or email"
        onChangeText={(text) => {
          setUsername(text);
        }}
        helperText={usernameError}
        value={usernameEmail}
        handleClear={() => {
          setUsername('');
        }}
        onFocus={() => {
          setUsernameError('');
          setInputState('active');
        }}
        onBlur={() => {
          checkForErrors();
        }}
        textWidth="80%"
      />
    </ContentsContainer>
  );

  const renderActions = () => (
    <Actions
      btnLabel={SUBMIT}
      btnPress={() => {
        dispatch({
          type: RECOVER_UPDATE_STATE,
          updatedState: {
            error: '',
          },
        });
        if (checkResetOnce()) {
          dispatch({
            type: START_RECOVER_PASSWORD,
            payloadData: {
              usernameEmail,
              type: recoverType,
            },
          });
        } else {
          setIsErrorVisible(true);
        }
      }}
      txtLabel={BACK_TO_LOGIN}
      textPress={() => {
        navigation.navigate(LOGIN);
      }}
      btnDisabled={!usernameEmail.length > 0 || usernameEmailError}
      testID="actionsTestId"
    />
  );

  return (
    <>
      <Loader isVisible={loading} />
      <UsernameEmailModal
        visible={isErrorVisible || displayError}
        txt={displayError ? error : RESET_ONCE_MSG}
        onPress={() => {
          setIsErrorVisible(false);
          dispatch({
            type: RECOVER_UPDATE_STATE,
            updatedState: {
              displayError: false,
              error: '',
            },
          });
        }}
      />
      <SingleScreen
        contents={renderContents()}
        bottom={renderActions()}
        spaceFromBottom={250}
      />
    </>
  );
};

export default EnterUsernameEmail;
