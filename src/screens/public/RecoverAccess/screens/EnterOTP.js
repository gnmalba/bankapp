/**
 * @format
 * @flow
 */

import React, {useEffect, useState, useCallback} from 'react';
import {BackHandler} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation, useFocusEffect} from '@react-navigation/native';

import Text from '@components/Text';
import Separator from '@components/Separator';
import Pincode from '@components/Pincode';
import {testProps} from '@utils/componentCustomProps';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';
import Modal from '../components/UsernameEmailModal';

import OTPCounter from '../components/OTPCounter';

import {
  RECOVER_SUBMIT_OTP,
  SET_RECOVER_OTP,
  RECOVER_RESEND_OTP,
  RECOVER_ACCESS_FAILED,
} from '@ducks/recover-access/actionTypes';
import {
  NAVIGATION_ENTERPASSWORD,
  LOGIN,
  NAVIGATION_MAXIMUMINVALID,
} from '@utils/constants';
import {
  MAXIMUM_INVALID_OTP_MSG,
  MAXIMUM_INVALID_CREDENTIALS_MSG,
} from '@ducks/recover-access/config';
import {TYPE_PASSWORD, ENTER_OTP_CODE} from '../config';

type PropsType = {
  testID?: string,
};

const EnterOTP = (props: PropsType): React$Node => {
  const recoverAccess = useSelector((state) => state.recoverAccess);
  const {
    loading,
    error,
    usernameEmail,
    email,
    generatorId,
    mfaToken,
    mpin,
    otp,
    proceedToPassword,
    recoverType,
    proceedToLogin,
    sendOtpCounter,
  } = recoverAccess;
  const {testID} = props;
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [isErrorVisible, setIsErrorVisible] = useState(false);
  const [newDate, setNewDate] = useState(Date.now());
  const [basis, setBasis] = useState(Date.now());
  const [intervalHandle, setIntervalHandle] = useState();

  const remaining = Math.floor(
    (new Date(basis).getTime() - new Date(newDate).getTime()) / 1000,
  );

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        dispatch({
          type: RECOVER_ACCESS_FAILED,
          proceedToOtp: true,
          invalidOtp: true,
          error: '',
          clearMpin: false,
        });
        return false;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [dispatch]),
  );

  useEffect(() => {
    if (proceedToPassword) {
      navigation.navigate(NAVIGATION_ENTERPASSWORD);
    } else if (proceedToLogin) {
      navigation.navigate(LOGIN);
    }
  }, [proceedToPassword, proceedToLogin, navigation]);

  useEffect(() => {
    if (error === MAXIMUM_INVALID_OTP_MSG) {
      setIsErrorVisible(true);
    } else if (error === MAXIMUM_INVALID_CREDENTIALS_MSG) {
      navigation.navigate(NAVIGATION_MAXIMUMINVALID);
    }
  }, [error, navigation]);

  const setOTP = (text) => {
    dispatch({type: SET_RECOVER_OTP, otp: text});
  };

  const handleSendOtp = () => {
    setBasis(new Date(Date.now() + 1000 * 61).valueOf());
    setNewDate(Date.now());
    dispatch({
      type: RECOVER_RESEND_OTP,
      payloadData:
        recoverType === TYPE_PASSWORD
          ? {
              username: usernameEmail,
              mpin,
              mfaToken,
              type: recoverType,
            }
          : {
              email,
              type: recoverType,
            },
    });
    setIntervalHandle(
      setInterval(() => {
        setNewDate(Date.now());
      }, 1000),
    );
  };

  useEffect(() => {
    if (remaining <= 0) {
      clearInterval(intervalHandle);
    }
  }, [remaining, intervalHandle]);

  useFocusEffect(
    useCallback(() => {
      return () => {
        clearInterval(intervalHandle);
      };
    }, [intervalHandle]),
  );

  const renderContents = () => {
    return (
      <>
        <Text mode="h2" {...testProps((testID || 'passcodeTestId') + 'Title')}>
          Verify your mobile number.
        </Text>
        <Separator space={8} />
        <Text
          mode="body3"
          {...testProps((testID || 'passcodeTestId') + 'Description')}>
          {ENTER_OTP_CODE}
        </Text>
        <Separator space={40} />
        <Pincode
          passcode={otp}
          onTextChange={setOTP}
          password={false}
          passcodeError={error || undefined}
        />
        <Separator space={24} />
        <OTPCounter
          sendOtpCounter={sendOtpCounter}
          handleSendOtp={handleSendOtp}
          sendOtpExceed={sendOtpCounter >= 3}
          error={error}
          remaining={remaining}
        />
      </>
    );
  };

  const renderBottom = () => (
    <Button
      {...testProps((testID || 'otpTestId') + 'Button')}
      size="large"
      type="primary"
      state={otp && otp.length === 6 ? 'default' : 'disabled'}
      title="Next"
      onPress={() => {
        dispatch({
          type: RECOVER_SUBMIT_OTP,
          payloadData: {
            usernameEmail,
            email,
            generatorId,
            mfaToken,
            mpin,
            otp,
            type: recoverType,
          },
        });
      }}
    />
  );

  return (
    <>
      <Loader isVisible={loading} />
      <Modal
        visible={isErrorVisible}
        txt={error}
        onPress={() => {
          setIsErrorVisible(false);
          navigation.navigate(LOGIN);
        }}
      />
      <SingleScreen
        contents={renderContents()}
        bottom={renderBottom()}
        spaceFromBottom={200}
      />
    </>
  );
};

export default EnterOTP;
