/**
 * @format
 * @flow
 */

import React, {useState, useEffect, useCallback} from 'react';
import {Keyboard, BackHandler} from 'react-native';
import {useNavigation, useFocusEffect} from '@react-navigation/core';
import {useDispatch, useSelector} from 'react-redux';

import Text from '@components/Text';
import Loader from '@composition/Loader';
import Separator from '@components/Separator';
import PasswordInput from '@composition/PasswordInput';
import SingleScreen from '@composition/SingleScreen';
import ChecklistItem from '@composition/ChecklistItem';
import Modal from '../components/UsernameEmailModal';

import InputPassword from '../components/InputPassword';

import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from '../types';
import {ContentsContainer} from '../Styled';
import {
  SET_RECOVER_PASSWORD,
  RECOVER_CONFIRM_PASSWORD,
  SEND_NEW_PASSWORD,
  SET_PROCEED_TO_LOGIN,
} from '@ducks/recover-access/actionTypes';

import {
  SUBMIT,
  BACK_TO_LOGIN,
  NO_SPACE,
  UPPER_CHAR,
  LOWER_CHAR,
  NUMERIC_CHAR,
  SPECIAL_CHAR,
  PASSWORD_NOTMATCHED_MSG,
} from '../config';
import {NAVIGATION_PASSWORDCHANGED, LOGIN} from '@utils/constants';

import Actions from '../components/Actions';

const renderActions = ({
  handleProceed,
  dispatch,
  navigation,
  btnDisabled,
  notMatched,
  handleTextPress,
}) => (
  <Actions
    btnLabel={SUBMIT}
    btnPress={handleProceed}
    txtLabel={BACK_TO_LOGIN}
    textPress={handleTextPress}
    btnDisabled={btnDisabled || notMatched}
    testID="actionsTestId"
  />
);

const RenderModal = ({error, dispatch, navigation, onPress}) => {
  return (
    <Modal visible={error !== ''} txt={error} btnTxt="Okay" onPress={onPress} />
  );
};

const renderHeader = () => (
  <>
    <Text mode="h2">Create new password</Text>
    <Separator space={8} />
    <Text mode="body3" themeColor="TEXT_100_HIGH">
      Make sure that your new password is unique and must not match any past
      passwords
    </Text>
    <Separator space={40} />
  </>
);

const EnterPassword = (props: PropsType): React$Node => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const recoverAccess = useSelector((state) => state.recoverAccess);
  const {
    usernameEmail,
    generatorId,
    mfaToken,
    mpin,
    otp,
    password,
    confirmPassword,
    passwordRecovered,
    passwordHasEightCharacters,
    passwordHasAnUpperCharacter,
    passwordHasANumericCharacter,
    passwordHasASpecialCharacter,
    passwordHasNoSpace,
    passwordHasALowerCaseCharacter,
    recoverType,
    loading,
    error,
  } = recoverAccess;

  const btnDisabled =
    !passwordHasEightCharacters ||
    !passwordHasAnUpperCharacter ||
    !passwordHasANumericCharacter ||
    !passwordHasASpecialCharacter ||
    !passwordHasNoSpace ||
    !passwordHasALowerCaseCharacter;

  const [showConfirm, setShowConfirm] = useState(false);
  const [notMatched, setNotMatched] = useState(false);
  const [inputState, setInputState] = useState('default');
  const [onfirmState, setConfirmState] = useState('default');

  const proceedToLogin = () => {
    dispatch({
      type: SET_PROCEED_TO_LOGIN,
      action: false,
    });
    navigation.navigate(LOGIN);
  };

  const handleBlur = useCallback(() => {
    setInputState('default');
    if (!btnDisabled) {
      setShowConfirm(true);
    }
  }, [btnDisabled]);

  useEffect(() => {
    Keyboard.addListener('keyboardDidHide', handleBlur);
    return () => {
      Keyboard.removeListener('keyboardDidHide', handleBlur);
    };
  }, [handleBlur]);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        navigation.navigate('Login');
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation]),
  );

  useEffect(() => {
    if (btnDisabled) {
      setShowConfirm(false);
    }
  }, [password, btnDisabled]);

  useEffect(() => {
    if (passwordRecovered) {
      navigation.navigate(NAVIGATION_PASSWORDCHANGED);
    }
  }, [passwordRecovered, navigation]);

  const handleChange = (text) => {
    dispatch({
      type: SET_RECOVER_PASSWORD,
      password: text,
    });
  };

  const handleVerifyChange = (text) => {
    setNotMatched(false);
    dispatch({
      type: RECOVER_CONFIRM_PASSWORD,
      confirmPassword: text,
    });
  };

  const checkPasswordMatched = () => {
    if (confirmPassword === password) {
      return true;
    }
    setNotMatched(true);
    setConfirmState('error');
    return false;
  };

  const setConfirmDefault = () => {
    setConfirmState('default');
  };

  const handleProceed = () => {
    if (checkPasswordMatched()) {
      dispatch({
        type: SEND_NEW_PASSWORD,
        payloadData: {
          usernameEmail,
          generatorId,
          mfaToken,
          mpin,
          otp,
          password,
          type: recoverType,
        },
      });
    }
  };

  const renderContents = () => (
    <ContentsContainer>
      {renderHeader()}
      <InputPassword
        onChangeText={handleChange}
        handleClear={handleChange}
        onEndEditing={handleBlur}
        inputState={inputState}
        setInputState={setInputState}
      />
      {!showConfirm && (
        <>
          <Separator space={10} />
          <ChecklistItem
            approved={passwordHasNoSpace && password.length}
            text={NO_SPACE}
          />
          <ChecklistItem
            approved={passwordHasAnUpperCharacter}
            text={UPPER_CHAR}
          />
          <ChecklistItem
            approved={passwordHasALowerCaseCharacter}
            text={LOWER_CHAR}
          />
          <ChecklistItem
            approved={passwordHasANumericCharacter}
            text={NUMERIC_CHAR}
          />
          <ChecklistItem
            approved={passwordHasASpecialCharacter}
            text={SPECIAL_CHAR}
          />
        </>
      )}
      {showConfirm && (
        <>
          <Separator space={30} />
          <PasswordInput
            {...testProps('confirmPasswordInputId')}
            label="Confirm password"
            onChangeText={(text) => {
              handleVerifyChange(text);
            }}
            handleClear={() => {
              handleVerifyChange('');
            }}
            state={onfirmState}
            value={confirmPassword}
            onFocus={() => {
              setConfirmState('active');
            }}
            onEndEditing={setConfirmDefault}
            onBlur={setConfirmDefault}
            textWidth="78%"
            helperText={notMatched ? PASSWORD_NOTMATCHED_MSG : ''}
          />
        </>
      )}
    </ContentsContainer>
  );

  return (
    <>
      <Loader isVisible={loading} />
      {RenderModal({error, dispatch, navigation, onPress: proceedToLogin})}
      <SingleScreen
        contents={renderContents()}
        bottom={renderActions({
          handleProceed,
          dispatch,
          navigation,
          btnDisabled,
          notMatched,
          handleTextPress: proceedToLogin,
        })}
        spaceFromBottom={250}
      />
    </>
  );
};

export default EnterPassword;
