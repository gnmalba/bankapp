// @flow

import React, {useCallback} from 'react';
import {BackHandler} from 'react-native';
import {useNavigation, useFocusEffect} from '@react-navigation/native';

import MessageScreen from '../components/MessageScreen';

import {LOGIN_UPDATED, START_NEW_CREDENTIALS, LOGIN} from '../config';

const image = require('@assets/icons/success.png');

const PasswordChanged = (): React$Node => {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        navigation.navigate('Login');
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation]),
  );

  return (
    <MessageScreen
      title={LOGIN_UPDATED}
      image={image}
      description={START_NEW_CREDENTIALS}
      btnLabel={LOGIN}
      handleNavigate={() => {
        navigation.navigate(LOGIN);
      }}
    />
  );
};

export default PasswordChanged;
