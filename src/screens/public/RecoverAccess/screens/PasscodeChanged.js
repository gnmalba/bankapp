// @flow

import React, {useCallback} from 'react';
import {BackHandler} from 'react-native';
import {useNavigation, useFocusEffect} from '@react-navigation/native';

import MessageScreen from '../components/MessageScreen';

const image = require('@assets/icons/success.png');

const PasswordChanged = (): React$Node => {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        navigation.navigate('Drawer');
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation]),
  );

  return (
    <MessageScreen
      title="Passcode Change"
      image={image}
      description="This is a confirmation that the passcode for your Metrobank account has been changed."
      btnLabel="Go to Dashboard"
      handleNavigate={() => {
        navigation.navigate('Drawer');
      }}
    />
  );
};

export default PasswordChanged;
