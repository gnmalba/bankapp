/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/core';
import {useDispatch, useSelector} from 'react-redux';

import Text from '@components/Text';
import Spinner from '@composition/Loader/Spinner';
import Separator from '@components/Separator';
import TextInput from '@composition/TextInput';
import SingleScreen from '@composition/SingleScreen';
import CenteredSingleScreen from '@composition/CenteredSingleScreen';

import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from '../types';
import {ContentsContainer} from '../Styled';
import {
  SET_RECOVER_EMAIL,
  START_RECOVER_USERNAME,
} from '@ducks/recover-access/actionTypes';

import {SUBMIT, BACK_TO_LOGIN, SEND_USERNAME_ONCE_MSG} from '../config';
import {LOGIN, NAVIGATION_ENTEROTP} from '@utils/constants';

import Actions from '../components/Actions';
import UsernameEmailModal from '../components/UsernameEmailModal';

const EnterEmail = (props: PropsType): React$Node => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const recoverAccess = useSelector((state) => state.recoverAccess);
  const {
    email,
    recoverType,
    loading,
    proceedToOtp,
    successfulUsernameSent,
  } = recoverAccess;
  const [inputState, setInputState] = useState('default');
  const [isErrorVisible, setIsErrorVisible] = useState(false);
  const [emailError, setEmailError] = useState('');

  useEffect(() => {
    proceedToOtp && navigation.navigate(NAVIGATION_ENTEROTP);
  }, [navigation, proceedToOtp]);

  const handleChangeEmail = (text) => {
    dispatch({
      type: SET_RECOVER_EMAIL,
      email: text,
    });
  };

  const checkForErrors = () => {
    const localErr = email.length === 0 ? 'Please enter your email.' : '';
    setEmailError(localErr);
    setInputState(localErr === '' ? 'default' : 'error');
  };

  const checkResetOnce = () => {
    const label = successfulUsernameSent[email];
    if (label) {
      const recoveredDate = new Date(label).toDateString();
      const todaysDate = new Date().toDateString();
      return todaysDate !== recoveredDate;
    }
    return true;
  };

  const renderContents = () => (
    <ContentsContainer>
      <Text mode="h2">Enter email address</Text>
      <Separator space={8} />
      <Text mode="body3" themeColor="TEXT_100_HIGH">
        Enter your email address and we will send you your username
      </Text>
      <Separator space={40} />
      <TextInput
        label="Email address"
        {...testProps('userNameInputId')}
        state={inputState}
        icon="username"
        placeholder="Enter email address"
        onChangeText={(text) => {
          handleChangeEmail(text);
        }}
        helperText={emailError}
        value={email}
        handleClear={() => {
          handleChangeEmail('');
        }}
        onFocus={() => {
          setEmailError('');
          setInputState('active');
        }}
        onBlur={() => {
          checkForErrors();
        }}
        textWidth="80%"
      />
    </ContentsContainer>
  );

  const renderActions = () => (
    <Actions
      btnLabel={SUBMIT}
      btnPress={() => {
        if (checkResetOnce()) {
          proceedToOtp
            ? navigation.navigate(NAVIGATION_ENTEROTP)
            : dispatch({
                type: START_RECOVER_USERNAME,
                payloadData: {
                  email,
                  type: recoverType,
                },
              });
        } else {
          setIsErrorVisible(true);
        }
      }}
      txtLabel={BACK_TO_LOGIN}
      textPress={() => {
        navigation.navigate(LOGIN);
      }}
      btnDisabled={email.length === 0}
      testID="actionsTestId"
    />
  );

  const renderLoading = () => (
    <>
      <Spinner isVisible={true} />
      <Separator space={15} />
      <Text mode="body3">Verifying email address</Text>
    </>
  );

  return (
    <>
      <UsernameEmailModal
        visible={isErrorVisible}
        txt={SEND_USERNAME_ONCE_MSG}
        onPress={() => {
          setIsErrorVisible(false);
        }}
      />
      {!loading && (
        <SingleScreen
          contents={renderContents()}
          bottom={renderActions()}
          spaceFromBottom={250}
        />
      )}
      {loading && (
        <CenteredSingleScreen contents={renderLoading()} action={<></>} />
      )}
    </>
  );
};

export default EnterEmail;
