/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {moderateScale, scale} from '@utils/scale';

import {ARROW_ICON} from '../config';

export const ButtonContainer: ComponentType<any> = styled.View.attrs(() => ({
  shadowColor: '#000',
  shadowOffset: {
    width: 5,
    height: 5,
  },
  shadowOpacity: 0.32,
  shadowRadius: 3,
  elevation: 5,
}))`
  padding: ${scale(15)}px;
  margin-top: ${scale(10)}px;
  margin-bottom: ${scale(10)}px;
  border-radius: 3px;
  background-color: ${(props) =>
    props.theme?.otherActions?.containerBackground || 'transparent'};
`;

export const StyledButton: ComponentType<any> = styled.TouchableOpacity`
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

export const ChevronRight: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    size: moderateScale(22),
    name: ARROW_ICON,
    color: props.theme?.color?.TEXT_300_HIGH || 'transparent',
  }),
)``;
