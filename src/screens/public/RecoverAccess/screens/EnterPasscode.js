/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/core';

import Text from '@components/Text';
import Separator from '@components/Separator';
import Pincode from '@components/Pincode';
import {testProps} from '@utils/componentCustomProps';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';
import Modal from '../components/UsernameEmailModal';

import {
  SET_PASSCODE,
  RECOVER_PASSWORD,
  RECOVER_UPDATE_STATE,
} from '@ducks/recover-access/actionTypes';
import {HelperText} from './EnterPasscodeStyled';
import {FORGOT_PASSCODE_MSG, CALL_HOTLINE_MSG} from '../config';
import {
  NAVIGATION_ENTEROTP,
  NAVIGATION_MAXIMUMINVALID,
  LOGIN,
} from '@utils/constants';
import {PASSCODE_ATTEMPTS_EXCEEDED} from '@ducks/recover-access/config';

type PropsType = {
  testID?: string,
};

const EnterPasscode = (props: PropsType): React$Node => {
  const recoverAccess = useSelector((state) => state.recoverAccess);
  const {
    passcode,
    passcodeErrorMsg,
    error,
    mpin,
    usernameEmail,
    mfaToken,
    recoverType,
    proceedToOtp,
    loading,
    displayError,
  } = recoverAccess;
  const {testID} = props;
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const invalid = /[._,-\s]/.test(passcode);

  useEffect(() => {
    proceedToOtp && navigation.navigate(NAVIGATION_ENTEROTP);
  }, [proceedToOtp, navigation]);

  useEffect(() => {
    if (error === PASSCODE_ATTEMPTS_EXCEEDED) {
      navigation.navigate(NAVIGATION_MAXIMUMINVALID);
    }
  }, [error, navigation]);

  const setPasscode = (text) => {
    dispatch({type: SET_PASSCODE, passcode: text});
  };

  const renderContents = () => {
    return (
      <>
        <Text mode="h2" {...testProps((testID || 'passcodeTestId') + 'Title')}>
          Please enter your passcode.
        </Text>
        <Separator space={8} />
        <Text
          mode="body3"
          {...testProps((testID || 'passcodeTestId') + 'Description')}>
          To proceed, please enter your 6-digit passcode.
        </Text>
        <Separator space={40} />
        <Pincode
          passcode={passcode}
          onTextChange={setPasscode}
          password={true}
          passcodeError={passcodeErrorMsg || error || undefined}
        />
        <Separator space={24} />
        <HelperText
          value={passcodeErrorMsg ? FORGOT_PASSCODE_MSG : CALL_HOTLINE_MSG}
        />
      </>
    );
  };

  const renderBottom = () => (
    <Button
      {...testProps((testID || 'passcodeTestId') + 'Button')}
      size="large"
      type="primary"
      state={
        passcode && passcode.length === 6 && !passcodeErrorMsg && !invalid
          ? 'default'
          : 'disabled'
      }
      title="Next"
      onPress={() => {
        mpin
          ? navigation.navigate(NAVIGATION_ENTEROTP)
          : dispatch({
              type: RECOVER_PASSWORD,
              payloadData: {
                usernameEmail,
                passcode,
                mfaToken,
                type: recoverType,
              },
            });
      }}
    />
  );

  return (
    <>
      <Loader isVisible={loading} />
      <Modal
        visible={displayError}
        txt={error}
        btnTxt="Go back to login"
        onPress={() => {
          dispatch({
            type: RECOVER_UPDATE_STATE,
            updatedState: {
              displayError: false,
            },
          });
          navigation.navigate(LOGIN);
        }}
      />
      <SingleScreen
        contents={renderContents()}
        bottom={renderBottom()}
        spaceFromBottom={200}
      />
    </>
  );
};

export default EnterPasscode;
