// @flow

import React, {useCallback} from 'react';
import {BackHandler} from 'react-native';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';

import MessageScreen from '../components/MessageScreen';

import {LOGIN} from '../config';
import {SET_RECOVER_ERROR} from '@ducks/recover-access/actionTypes';

const image = require('@assets/icons/warning-big.png');

const MaximumInvalid = (): React$Node => {
  const navigation = useNavigation();
  const error = useSelector((state) => state.recoverAccess.error);
  const dispatch = useDispatch();

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        navigation.navigate('Login');
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation]),
  );

  return (
    <MessageScreen
      image={image}
      description={error}
      btnLabel="Go back"
      handleNavigate={() => {
        dispatch({
          type: SET_RECOVER_ERROR,
          error: '',
        });
        navigation.navigate(LOGIN);
      }}
    />
  );
};

export default MaximumInvalid;
