/**
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {useDispatch} from 'react-redux';

import Text from '@components/Text';
import Separator from '@components/Separator';
import SingleScreen from '@composition/SingleScreen';

import type {PropsType} from '../types';
import {ContentsContainer} from '../Styled';
import {SET_RECOVER_TYPE} from '@ducks/recover-access/actionTypes';

import {
  FORGOT_PASSWORD,
  TYPE_PASSWORD,
  FORGOT_USERNAME,
  TYPE_USERNAME,
} from '../config';

import {
  NAVIGATION_ENTEREMAIL,
  NAVIGATION_ENTERUSERNAMEEMAIL,
} from '@utils/constants';

import {
  ButtonContainer,
  StyledButton,
  ChevronRight,
} from './RecoverAccessStyled';

const RecoverAccess = (props: PropsType): React$Node => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const setRecoverType = (type: string) => {
    dispatch({
      type: SET_RECOVER_TYPE,
      recoverType: type,
    });
  };

  const renderButton = (
    label: string,
    onPress?: () => void,
    containerStyle?: Object,
    buttonStyle?: Object,
    textStyle?: Object,
  ): React$Element<any> => {
    return (
      <ButtonContainer>
        <StyledButton testID={label.replace(/ /g, '')} onPress={onPress}>
          <Text mode="body1" themeColor="TEXT_300_HIGH">
            {label}
          </Text>
          <ChevronRight />
        </StyledButton>
      </ButtonContainer>
    );
  };

  const renderContent = () => (
    <View>
      {renderButton(FORGOT_PASSWORD, () => {
        setRecoverType(TYPE_PASSWORD);
        navigation.navigate(NAVIGATION_ENTERUSERNAMEEMAIL);
      })}
      {renderButton(FORGOT_USERNAME, () => {
        setRecoverType(TYPE_USERNAME);
        navigation.navigate(NAVIGATION_ENTEREMAIL);
      })}
    </View>
  );

  const renderContents = () => (
    <ContentsContainer>
      <Text mode="h2">How can we help?</Text>
      <Separator space={8} />
      <Text mode="body3" themeColor="TEXT_100_HIGH">
        Select from the following options below if you are having trouble
        accessing your account
      </Text>
      <Separator space={40} />
      {renderContent()}
    </ContentsContainer>
  );

  return <SingleScreen contents={renderContents()} />;
};

export default RecoverAccess;
