/**
 * @format
 * @flow
 */

import React, {useEffect, useCallback} from 'react';
import {BackHandler} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation, useFocusEffect} from '@react-navigation/core';

import Text from '@components/Text';
import Separator from '@components/Separator';
import Pincode from '@components/Pincode';
import {testProps} from '@utils/componentCustomProps';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import Loader from '@composition/Loader';

import {
  SET_PASSCODE,
  START_NEW_PASSCODE,
} from '@ducks/recover-access/actionTypes';
import {NAVIGATION_PASSCODECHANGED} from '@utils/constants';

type PropsType = {
  testID?: string,
};

const ChangePasscode = (props: PropsType): React$Node => {
  const {testID} = props;
  const recoverAccess = useSelector((state) => state.recoverAccess);
  const authentication = useSelector((state) => state.authentication);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {
    passcode,
    loading,
    proceedToSuccess,
    passcodeErrorMsg,
    error,
  } = recoverAccess;
  const {encryptionPublicKey, data} = authentication || {};
  const invalid = /[._,-\s]/.test(passcode);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        navigation.navigate('Drawer');
        return true;
      };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () =>
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [navigation]),
  );

  useEffect(() => {
    if (proceedToSuccess) {
      navigation.navigate(NAVIGATION_PASSCODECHANGED);
    }
  }, [proceedToSuccess, navigation]);

  const setPasscode = (text) => {
    dispatch({type: SET_PASSCODE, passcode: text});
  };

  const renderContents = () => {
    return (
      <>
        <Text mode="h2" {...testProps((testID || 'passcodeTestId') + 'Title')}>
          Enter new passcode.
        </Text>
        <Separator space={8} />
        <Text
          mode="body3"
          {...testProps((testID || 'passcodeTestId') + 'Description')}>
          Make sure that no one knows your new passcode
        </Text>
        <Separator space={40} />
        <Pincode
          passcode={passcode}
          onTextChange={setPasscode}
          password={true}
          passcodeError={passcodeErrorMsg || error || undefined}
        />
        <Separator space={24} />
        <Button
          {...testProps((testID || 'passcodeTestId') + 'Button')}
          size="large"
          type="primary"
          state={
            passcode && passcode.length === 6 && !passcodeErrorMsg && !invalid
              ? 'default'
              : 'disabled'
          }
          title="Save"
          onPress={() => {
            dispatch({
              type: START_NEW_PASSCODE,
              payloadData: {
                encryptionPublicKey,
                customerId: data.customerId,
                passcode,
              },
            });
          }}
        />
      </>
    );
  };

  return (
    <>
      <Loader isVisible={loading} />
      <SingleScreen
        contents={renderContents()}
        bottom={<></>}
        spaceFromBottom={200}
      />
    </>
  );
};

export default ChangePasscode;
