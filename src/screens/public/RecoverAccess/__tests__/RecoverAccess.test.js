import React from 'react';
import {render} from '@testing-library/react-native';
import RecoverAccess from '../screens/RecoverAccess';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

describe('RecoverAccess', () => {
  it('Should work as expected', () => {
    const all = render(
      <MockProvider store={{}}>
        <ThemeProvider theme={DARK_THEME}>
          <RecoverAccess />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
