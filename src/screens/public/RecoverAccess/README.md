## RecoverAccess
RecoverAccess component.

# Usage
```js
import RecoverAccess from '@screens/public/RecoverAccess';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
