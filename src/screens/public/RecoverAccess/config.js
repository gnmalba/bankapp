// @flow

export const HOW_CAN_WE_HELP = 'How can we help?';
export const SELECT_OPTIONS =
  'Select from the following options below if you are having trouble accessing your account';
export const ARROW_ICON = 'chevron-forward';
export const FORGOT_PASSWORD = 'I forgot my password';
export const FORGOT_USERNAME = 'I forgot my username';
export const USERNAME_EMAIL_LABEL = 'Username or email';
export const USERNAME_EMAIL_PLACEHOLDER = 'Enter username or email';
export const USERNAME_ICON = 'ios-person-sharp';
export const USERNAME_TESTID = 'inputUsername';
export const USERNAME_NAME = 'username';
export const BACK_TO_LOGIN = 'Go back to login';
export const LOGIN = 'Login';
export const SUBMIT = 'Submit';
export const NEXT = 'Next';
export const ENTER_USERNAME_EMAIL = 'Enter username or email';
export const ENTER_EMAIL_ADDRESS = 'Enter email address';
export const USERNAME_EMAIL = 'Username or email';
export const VERIFY_MOBILE = 'Verify your mobile number.';
export const ENTER_OTP_CODE =
  'Please enter the 6-digit verification code we sent to your registered mobile number.';
export const NEW_PASSWORD = 'New Password';
export const ENTER_PASSWORD = 'Enter password';
export const EIGHT_CHARS = 'Contains at least eight characters';
export const NO_SPACE = 'Must have no space';
export const UPPER_CHAR = 'Must have at least 1 upper case character';
export const LOWER_CHAR = 'Must have at least 1 lower case characteer';
export const NUMERIC_CHAR = 'Must have at least 1 number';
export const SPECIAL_CHAR = 'Must have at least 1 special character';
export const PROCEED = 'Proceed';
export const EMAIL_ADDRESS = 'Email address';
export const ENTER_EMAIL = 'Enter you email';
export const USERNAME = 'Username';
export const ENTER_USERNAME = 'Enter your username';
export const USERNAME_EMAIL_ERR_MSG = 'Requires 8 characters or more.';
export const PASSWORD_ERR_MSG = 'Password cannot contain spaces.';
export const INVALID_EMAIL_FORMAT = 'Invalid email format.';
export const ENTERPASSCODE = 'EnterPasscode';
export const ENTEREMAIL_TEST_ID = 'enterEmailTestId';
export const ENTER_USERNAME_EMAIL_DESC =
  'Enter your username or email address and we will send you instructions on how to create a new password';
export const ENTER_PASSCODE_TITLE = 'Please enter your passcode.';
export const ENTER_PASSCODE_DESC = 'To proceed, enter your 6-digit passcode.';
export const PASSCODE_ERROR = 'Accepts numeric characters only.';
export const CONFIRM_PASSWORD = 'Confirm password';
export const TYPE_USERNAME = 'username';
export const TYPE_PASSWORD = 'password';
export const ENTER_EMAIL_SCREEN = 'EnterEmail';
export const ENTER_EMAIL_DESC =
  'Enter your email address and we will send you your username.';
export const EMAIL_VERIFICATION_REGEX: any = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const CREATE_NEW_PASSWORD = 'Create new password';
export const NEW_PASSWORD_DESC =
  'Make sure that yoour new password is unique and must not match any past passwords';
export const LOGIN_UPDATED = 'Your login credentials have been updated';
export const START_NEW_CREDENTIALS =
  'To start using Metrobank Online, please login using your username and new password. Never share these to anyone.';
export const SEND_ANOTHER_CODE = 'Send another code';
export const OTP_TIMER_LENGTH = 61;
export const SENT_NEW_CODE = "We've sent a new code to your phone number.";
export const SEND_AGAIN = 'Send again';
export const CANCEL = 'Cancel';
export const PASSWORD_ENTRY_LIFESPAN = 60000;
export const OTP_SEND_AGAIN_EXCEEDED_MSG =
  "<p>You have reached the maximum number of tries to resend the code. If you didn't receive a code, please call our hotline <span><b>(02)88-700-700</b></span> for further assistance.</p>";
export const RESET_ONCE_MSG =
  'You have just recently changed your password, you are only allowed to change it once per day.';
export const CALL_HOTLINE_MSG =
  "<p>If you didn't receive a code, please call our hotline <span><b>(02) 88-700-700</b></span> for further assistance.</p>";
export const FORGOT_PASSCODE_MSG =
  '<p>If you forgot passcode, please call our hotline <span><b>(02) 88-700-700</b></span> for further assistance.</p>';
export const LIFETIME_EXPIRED_MSG = 'Lifespan expired, try the process again.';
export const PASSWORD_NOTMATCHED_MSG = 'Password do not match.';
export const SEND_USERNAME_ONCE_MSG =
  'We have just recently sent your username, you are only allowed to request once per day.';
export const ENTER_NEW_PASSCODE = 'Enter new Passcode';
export const RECOVER_PASSCODE_DESCRIPTION =
  'Make sure that no one knows your new passcode';
export const SAVE = 'Save';
export const SEND_ANOTHER_CODE_TEST_ID = 'sendAnotherCodeTestId';
export const VERIFYING_EMAIL = 'Verifying email address';
