import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import LoginOtpVerification from '../LoginOtpVerification';
import {testProps} from '@utils/componentCustomProps';

import MockProvider from '@utils/testFramework';
const mockStore = () => ({
  authentication: {
    passcode: '123456',
    rememberThisDevice: 'false',
  },
});

describe('LoginOtpVerification', () => {
  it.each(['testId', ''])('Should work as expected', (testID) => {
    const all = render(
      <MockProvider store={mockStore()}>
        <LoginOtpVerification {...testProps(testID)} />
      </MockProvider>,
    );
    all.debug();
    if (testID) {
      const inputEl = all.getByTestId('smoothInputTestId');
      fireEvent(inputEl, 'onChangeText', '111111');
      const btnEl = all.getByTestId(`${testID}Button`);
      fireEvent(btnEl, 'onPress');
    }
    expect(all.toJSON()).toMatchSnapshot();
  });
});
