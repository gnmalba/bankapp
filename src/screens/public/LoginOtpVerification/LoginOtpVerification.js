/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import PasscodeHelper from './components/PasscodeHelper';
import Text from '@components/Text';
import Separator from '@components/Separator';
import Pincode from '@components/Pincode';
import {testProps} from '@utils/componentCustomProps';
import Button from '@components/Button';
import SingleScreen from '@composition/SingleScreen';
import {scale} from '@utils/scale';

import * as types from '@ducks/actionTypes';
import type {PropsType} from './types';
import {
  CheckContainer,
  CheckIcon,
  RememberContainer,
  NoticeContainer,
  NoticeIcon,
} from './Styled';

const LoginOtpVerification = (props: PropsType): React$Node => {
  const authentication = useSelector((state) => state.authentication);
  const {
    error,
    passcode,
    passcodeError,
    username,
    password,
    mfaToken,
    encryptionPublicKey,
    proceedToFullPageError,
    rememberThisDevice,
    loginMethod,
    signedToken,
  } = authentication;
  const {testID, theme} = props;
  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        proceedToOtp: false,
      },
    });
  }, []);

  useEffect(() => {
    if (proceedToFullPageError) {
      navigation.navigate('FullPageError', {
        msg: error,
      });
    }
  }, [proceedToFullPageError]);

  const renderContents = () => {
    return (
      <>
        <Text mode="h2" {...testProps((testID || 'passcodeTestId') + 'Title')}>
          You are logging in using a new device
        </Text>
        <Separator space={8} />
        <Text
          mode="body2"
          {...testProps((testID || 'passcodeTestId') + 'Description')}>
          Please enter the verification code we sent to your phone number
        </Text>
        <Separator space={40} />
        <Pincode
          passcode={passcode}
          onTextChange={(text) => {
            dispatch({
              type: types.AUTHENTICATION_UPDATE_STATE,
              newState: {passcode: text, passcodeError: ''},
            });
          }}
          password={false}
          passcodeError={passcodeError}
        />
        <Separator space={24} />
        <PasscodeHelper />
      </>
    );
  };

  const renderBottom = () => {
    return (
      <>
        <TouchableOpacity
          onPress={() => {
            dispatch({
              type: types.AUTHENTICATION_TOGGLE_REMEMBER_THIS_DEVICE,
            });
          }}>
          <RememberContainer>
            <CheckContainer checked={rememberThisDevice} theme={theme}>
              <CheckIcon theme={theme} />
            </CheckContainer>
            <Text mode="body2">I want to remember this device</Text>
          </RememberContainer>
        </TouchableOpacity>
        <Separator space={19} />
        <NoticeContainer theme={theme}>
          <NoticeIcon />
          <Text
            mode="body2"
            style={{
              fontSize: scale(10),
              marginRight: scale(14),
            }}>
            Be sure to remove and forget the devices you don’t recognize or no
            longer use.
          </Text>
        </NoticeContainer>
        <Separator space={24} />
        <Button
          {...testProps((testID || 'passcodeTestId') + 'Button')}
          size="large"
          type="primary"
          state={
            passcode && passcode.length === 6 && !passcodeError
              ? 'default'
              : 'disabled'
          }
          title="Next"
          onPress={() => {
            dispatch({
              type: types.AUTHENTICATION_SUBMIT_OTP,
              payload: {
                username,
                password,
                mfaToken,
                encryptionPublicKey,
                totp: passcode,
                rememberMe: rememberThisDevice.toString(),
                signedToken: loginMethod === 'biometrics' ? signedToken : '',
              },
            });
          }}
        />
      </>
    );
  };

  return (
    <SingleScreen
      contents={renderContents()}
      bottom={renderBottom()}
      spaceFromBottom={300}
    />
  );
};

export default LoginOtpVerification;
