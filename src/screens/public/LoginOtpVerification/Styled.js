// @flow

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const RememberContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const CheckContainer: ComponentType<any> = styled.View`
  background-color: ${(props) =>
    props.checked
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.BACKGROUND_200};
  width: ${scale(18)}px;
  height: ${scale(18)}px;
  border-radius: ${scale(2)}px;
  margin-right: ${scale(11)}px;
  border-color: ${(props) =>
    props.checked
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.TEXT_300_LOW};
  border-width: 1px;
  align-items: center;
  justify-content: center;
`;

export const CheckIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'checkmark-sharp',
    size: scale(18),
    color: props.theme.color.BACKGROUND_200,
  }),
)``;

export const NoticeContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.displayOptionsTooltip.divider};
  height: ${verticalScale(47)}px;
  width: 100%;
  border-radius: 4px;
  flex-direction: row;
  align-items: center;
  padding-left: ${scale(18)}px;
  padding-right: ${scale(30)}px;
`;

export const NoticeIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/notice-white.png'),
}))`
  width: ${scale(16.25)}px;
  height: ${scale(15)}px;
  margin-right: ${scale(18)}px;
`;
