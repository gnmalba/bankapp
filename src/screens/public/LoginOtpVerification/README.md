## LoginOtpVerification
LoginOtpVerification component.

# Usage
```js
import LoginOtpVerification from '@screens/public/LoginOtpVerification';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
