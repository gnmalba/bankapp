/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import Text from '@components/Text';
import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: flex-end;
`;

export const SendContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: flex-end;
  align-items: center;
`;

export const StyledText: ComponentType<any> = styled(Text)`
  padding-bottom: ${verticalScale(2)}px;
`;
