// @flow

import React from 'react';
import {TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import Text from '@components/Text';

import Styles from './styles';
import {Container, SendContainer} from './Styled';
import {testProps} from '@utils/componentCustomProps';
import {
  AUTHENTICATION_START_LOGIN,
  AUTHENTICATION_INCREMENT_NUMBER_OF_OTP_REQUESTS,
} from '@ducks/authentication/actionTypes';
import Separator from '@components/Separator';
import {useCntDown} from '@utils/useCntDown';

const PasscodeHelper = (): any => {
  const dispatch = useDispatch();
  const authentication = useSelector((state) => state.authentication);
  const {
    username,
    password,
    encryptionPublicKey,
    numberOfOtpRequests,
  } = authentication;
  const [timerDisplay, setExpire] = useCntDown();
  const timerText = `(00:${
    timerDisplay.toString().length === 1 ? '0' + timerDisplay : timerDisplay
  })`;
  const cnt = 60;

  return (
    <>
      <Container>
        <TouchableOpacity
          disabled={
            numberOfOtpRequests > 2 || (timerDisplay < cnt && timerDisplay > 0)
          }
          onPress={() => {
            setExpire(cnt);
            dispatch({
              type: AUTHENTICATION_START_LOGIN,
              payload: {
                username,
                password,
                encryptionPublicKey,
              },
            });
            dispatch({
              type: AUTHENTICATION_INCREMENT_NUMBER_OF_OTP_REQUESTS,
            });
          }}>
          <SendContainer>
            <Text
              {...testProps('passcodeHelperTestIdStr2')}
              mode="custom"
              style={Styles.customText}
              themeColor={
                numberOfOtpRequests > 2 || timerDisplay !== 0
                  ? 'TEXT_300_LOW'
                  : 'SECONDARY_DEFAULT'
              }>
              Send another code
            </Text>
            <Text mode="subtitle">{timerDisplay > 0 ? timerText : ''}</Text>
          </SendContainer>
        </TouchableOpacity>
      </Container>
      <Separator space={24} />
      {numberOfOtpRequests > 0 && numberOfOtpRequests < 3 ? (
        <Text mode="body2">We’ve sent a new code to your mobile number.</Text>
      ) : (
        <></>
      )}
      {numberOfOtpRequests >= 3 ? (
        <Text mode="body2">
          You have reached the maximum number of tries to resend the code. If
          you didn’t receive a code, please call our hotline (02) 88-700-700 for
          further assistance.
        </Text>
      ) : (
        <></>
      )}
    </>
  );
};

export default PasscodeHelper;
