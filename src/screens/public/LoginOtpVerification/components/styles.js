/**
 * @flow
 */

import {StyleSheet} from 'react-native';

type StylesType = {
  customText: Object,
};

const Styles: StylesType = StyleSheet.create({
  customText: {
    fontWeight: 'bold',
    fontSize: 18,
    paddingRight: 10,
  },
});

export default Styles;
