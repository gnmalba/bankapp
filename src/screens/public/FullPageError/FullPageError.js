/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';
import CenteredSingleScreen from '@composition/CenteredSingleScreen';
import {useDispatch} from 'react-redux';

import * as types from '@ducks/authentication/actionTypes';
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import styles from './styles';
import * as S from './Styled';

const FullPageError = (props: PropsType): React$Node => {
  const {route, navigation} = props;
  const msg = route?.params?.msg;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {
        proceedToFullPageError: false,
      },
    });
  }, []);

  const renderContents = () => {
    return (
      <>
        <S.StyledIcon {...testProps('errorIcon')} />
        <Separator space={40} />
        <Text
          mode="h2"
          style={styles.centerText}
          {...testProps('errorHeading')}>
          Oops!, sorry something went wrong
        </Text>
        <Separator space={16} />
        <Text mode="body2" {...testProps('errorDetails')}>
          {msg}
        </Text>
      </>
    );
  };

  return (
    <CenteredSingleScreen
      contents={renderContents()}
      action={
        <Button
          type="primary"
          size="large"
          state="default"
          title="Go back to Login"
          onPress={() => {
            navigation && navigation.navigate('Login');
          }}
          {...testProps('backToLoginButton')}
        />
      }
    />
  );
};

export default FullPageError;
