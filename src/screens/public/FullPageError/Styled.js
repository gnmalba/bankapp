/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale} from '@utils/scale';

export const StyledIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/warning-big.png'),
}))`
  width: ${scale(60)}px;
  height: ${scale(55)}px;
`;
