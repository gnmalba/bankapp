import React from 'react';
import {render} from '@testing-library/react-native';
import FullPageError from '../FullPageError';
import MockProvider from '@utils/testFramework';

describe('FullPageError', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider>
        <FullPageError />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
