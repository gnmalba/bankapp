/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  route?: Object,
  navigation?: {
    navigate: any,
  },
};
