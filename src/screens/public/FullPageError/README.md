## FullPageError
FullPageError component.

# Usage
```js
import FullPageError from '@screens/public/FullPageError';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
