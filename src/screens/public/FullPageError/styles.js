/**
 * @flow
 */

import {StyleSheet} from 'react-native';

type StylesType = {
  centerText: Object,
};

const Styles: StylesType = StyleSheet.create({
  centerText: {
    textAlign: 'center',
  },
});

export default Styles;
