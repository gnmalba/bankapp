## Passcode

Full screen passcode

# Usage

```js
import Passcode from '@screens/common/Passcode';

# Props
```

| Prop               | Type     | Required | Description                    |
| ------------------ | -------- | -------- | ------------------------------ |
| testID             | string   | false    | The testID prop                |
| accessibilityLabel | string   | false    | The accessibilityLabel prop    |
| onTextChange       | Function | false    | Change text handler            |
| theme              | Object   | false    | Theme object                   |
| passcodeError      | string   | false    | passcode error                 |
| passcode           | string   | false    | passcode                       |
| onCallPress        | Function | false    |                                |
| password           | booelan  | false    | If true, number will be masked |

# Example

```js
<Pincode
  passcode={authentication.passcode}
  onTextChange={(text) => {
    dispatch({
      type: types.AUTHENTICATION_UPDATE_STATE,
      newState: {passcode: text},
    });
  }}
  password
  passcodeError="Sorry something went wrong"
/>
```
