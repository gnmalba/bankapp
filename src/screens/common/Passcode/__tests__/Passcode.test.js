import React from 'react';
import {render} from '@testing-library/react-native';
import Passcode from '../Passcode';
import {testProps} from '@utils/componentCustomProps';

import MockProvider from '@utils/testFramework';
const mockStore = (err) => ({
  authentication: {
    username: 'Test',
    password: 'Test',
    usernameError: 'Err',
    passwordError: 'Err',
    loading: err === true,
  },
});

describe('Passcode', () => {
  it.each(['123456', ''])('Should work as expected', (testID) => {
    const all = render(
      <MockProvider store={mockStore(true)}>
        <Passcode {...testProps(testID)} passcode={testID} />
      </MockProvider>,
    );
    all.debug();
    expect(all.toJSON()).toMatchSnapshot();
  });
});
