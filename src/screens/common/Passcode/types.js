/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  passcode?: string,
  password?: boolean,
  title?: string,
  description?: any,
  btnTitle?: string,
  enableAutomaticScroll?: boolean,
  helperContent?: React$Node,
  passcodeError?: string,
  spaceFromBottom?: number,
  onTextChange?: (options?: any) => void,
  onSubmit?: (options?: any) => void,
  onCancel?: (options?: any) => void,
};
