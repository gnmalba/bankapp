/**
 * @format
 * @flow
 */

import React from 'react';
import {View} from 'react-native';

import SingleScreen from '@composition/SingleScreen';
import Text from '@components/Text';
import Pincode from '@components/Pincode';
import Button from '@components/Button';
import {CancelButton} from '@composition/EnrollAccount';

import type {PropsType} from './types';
import {Separator} from './Styled';
import {testProps} from '@utils/componentCustomProps';
import {StyledScrollView} from '@screens/private/PayBillsCredit/MainScreen/components/Form/Styled';

const Passcode = (props: PropsType): React$Node => {
  const {
    title,
    description,
    passcode,
    password,
    btnTitle,
    helperContent,
    testID,
    passcodeError,
    spaceFromBottom,
    onTextChange,
    enableAutomaticScroll = true,
    onSubmit,
    onCancel,
  } = props;

  const renderContents = () => {
    return (
      <StyledScrollView enableAutomaticScroll={enableAutomaticScroll}>
        <Text mode="h2" {...testProps((testID || 'passcodeTestId') + 'Title')}>
          {title}
        </Text>
        <Separator space="8" />
        <Text
          mode="body2"
          themeColor="TEXT_100_HIGH"
          {...testProps((testID || 'passcodeTestId') + 'Description')}>
          {description}
        </Text>
        <Separator space="40" />
        <View {...testProps('pincodeInputTestId')}>
          <Pincode
            passcode={passcode}
            onTextChange={onTextChange}
            password={password}
            passcodeError={passcodeError}
          />
        </View>
        <Separator space="24" />
        {helperContent}
      </StyledScrollView>
    );
  };

  const renderBottom = () => (
    <>
      <Button
        {...testProps((testID || 'passcodeTestId') + 'Button')}
        size="large"
        type="primary"
        state={
          passcode && passcode.length === 6 && !passcodeError
            ? 'default'
            : 'disabled'
        }
        title={btnTitle || ''}
        onPress={onSubmit}
      />
      {onCancel && <CancelButton handleCancel={onCancel} />}
    </>
  );

  return (
    <SingleScreen
      spaceFromBottom={spaceFromBottom}
      contents={renderContents()}
      bottom={renderBottom()}
    />
  );
};

export default Passcode;
