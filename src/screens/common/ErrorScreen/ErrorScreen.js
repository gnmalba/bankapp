/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Compositions
import SingleScreen from '@composition/SingleScreen';

// Components
import Button from '@components/Button';

// Utils
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import {
  Container,
  ErrorContainer,
  StyledErrorIcon,
  StyledErrorHeading,
  StyledErrorDetails,
  ButtonContainer,
  StyledSecondaryButton,
} from './Styled';

const ErrorScreen = (props: PropsType): React$Node => {
  const {
    theme,
    isSystemError,
    heading,
    details,
    buttonContents,
    headingAlignCenter = false,
    handlePrimaryButton,
    handleSecondaryButton,
    primaryButtonTitle,
    secondaryButtonTitle,
  } = props;
  const GENERIC_ERROR = {
    HEADING: 'Oops! Something went wrong',
    DETAILS:
      "Sorry! Our system can't process your transaction right now due to an error. You can try again or go back to the Dashboard screen.",
  };

  const renderGenericButtons = () => {
    return (
      <>
        {handlePrimaryButton && (
          <Button
            title={primaryButtonTitle || 'Check Details'}
            type="primary"
            state="default"
            onPress={handlePrimaryButton}
            {...testProps('checkDetailsButton')}
          />
        )}
        {handleSecondaryButton && (
          <StyledSecondaryButton>
            <Button
              title={secondaryButtonTitle || 'Go Back to Dashboard'}
              type="secondary"
              titleThemeColor="TEXT_300_HIGH"
              state={'default'}
              onPress={handleSecondaryButton}
              {...testProps('goToDashboardButton')}
            />
          </StyledSecondaryButton>
        )}
      </>
    );
  };

  return (
    <SingleScreen
      contents={
        <Container>
          <ErrorContainer>
            <StyledErrorIcon theme={theme} {...testProps('errorIcon')} />
            {isSystemError && (
              <>
                <StyledErrorHeading
                  theme={theme}
                  {...testProps('errorHeading')}>
                  {GENERIC_ERROR.HEADING}
                </StyledErrorHeading>
                <StyledErrorDetails
                  theme={theme}
                  {...testProps('errorDetails')}>
                  {GENERIC_ERROR.DETAILS}
                </StyledErrorDetails>
              </>
            )}
            {!isSystemError && (
              <>
                {heading && (
                  <StyledErrorHeading
                    theme={theme}
                    isTextCenter={headingAlignCenter}
                    {...testProps('errorHeading')}>
                    {heading}
                  </StyledErrorHeading>
                )}
                {details && (
                  <StyledErrorDetails
                    theme={theme}
                    {...testProps('errorDetails')}>
                    {details}
                  </StyledErrorDetails>
                )}
              </>
            )}
          </ErrorContainer>
          <ButtonContainer {...testProps('errorButtonContainer')}>
            {isSystemError && renderGenericButtons()}
            {!isSystemError && buttonContents}
          </ButtonContainer>
        </Container>
      }
    />
  );
};

export default ErrorScreen;
