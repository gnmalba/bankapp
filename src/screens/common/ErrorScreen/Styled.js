/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {scale, verticalScale} from '@utils/scale';
import Text from '@components/Text';
import {HEIGHT} from '@utils/constants';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  height: ${HEIGHT / 1.5}px;
  justify-content: center;
`;

export const ErrorContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(40)}px;
`;

export const StyledErrorIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'ios-warning',
    size: scale(64),
    color: props.theme.color.RED_STATUS,
  }),
)`
  margin-bottom: ${verticalScale(32)}px;
`;

export const StyledErrorHeading: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'h3',
    customColor: props.theme.color.TEXT_300_HIGH,
  }),
)`
  margin-bottom: ${verticalScale(16)}px;
  text-align: ${(props) => (props?.isTextCenter ? 'center' : 'auto')};
`;

export const StyledErrorDetails: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body2',
    customColor: props.theme.color.TEXT_100_HIGH,
  }),
)`
  margin-bottom: ${verticalScale(16)}px;
  text-align: center;
  line-height: ${verticalScale(20)}px;
`;

export const StyledSecondaryButton: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;
