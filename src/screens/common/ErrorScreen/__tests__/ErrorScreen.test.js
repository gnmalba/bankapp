import React from 'react';
import {render} from '@testing-library/react-native';
import ErrorScreen from '../ErrorScreen';
import {testProps} from '@utils/componentCustomProps';

describe('ErrorScreen', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<ErrorScreen {...testProps('errorScreenTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
  it('Should render sysmtem error', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ErrorScreen isSystemError={true} {...testProps('errorScreenTestId')} />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
