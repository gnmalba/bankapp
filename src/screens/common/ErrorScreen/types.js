/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  buttonContents?: any,
  details?: any,
  heading?: string,
  isSystemError?: boolean,
  handlePrimaryButton?: any,
  handleSecondaryButton?: any,
  theme?: Object,
  headingAlignCenter?: boolean,
  primaryButtonTitle?: string,
  secondaryButtonTitle?: string,
};
