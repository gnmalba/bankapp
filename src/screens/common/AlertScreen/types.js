/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  icon?: any,
  title?: string,
  description1?: string,
  description2?: string,
  description3?: string,
  descriptionChild?: React$Node,
  navigation: {
    goBack: () => void,
  },
};
