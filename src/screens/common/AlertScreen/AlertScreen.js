/**
 * @format
 * @flow
 */

import React from 'react';

import Text from '@components/Text';
import Button from '@components/Button';
import Separator from '@components/Separator';
import CenteredSingleScreen from '@composition/CenteredSingleScreen';

import type {PropsType} from './types';
import {Container, Unlock, CenterContainer} from './Styled';
import {testProps} from '@utils/componentCustomProps';

const DeviceSecurityAlert = (props: PropsType): React$Node => {
  const {
    icon,
    title,
    description1,
    description2,
    description3,
    descriptionChild,
  } = props;

  const textAlign = {
    textAlign: 'left',
  };

  const renderDesc = (text: string, testID: string = ''): React$Node => {
    return (
      <>
        <Separator space={16} />
        <Text
          mode="body2"
          themeColor="TEXT_100_HIGH"
          style={textAlign}
          {...testProps(testID)}>
          {text}
        </Text>
      </>
    );
  };

  const renderContents = () => {
    return (
      <Container>
        <CenterContainer>
          <Unlock source={icon} {...testProps('alertIcon')} />
          <Separator space={38} />
          <Text mode="h2" {...testProps('alertTitle')}>
            {title}
          </Text>
        </CenterContainer>
        {description1 ? renderDesc(description1, 'alertDescription1') : <></>}
        {description2 ? renderDesc(description2, 'alertDescription2') : <></>}
        {description3 ? renderDesc(description3, 'alertDescription3') : <></>}
        {descriptionChild !== null || descriptionChild !== undefined ? (
          <>
            <Separator space={16} />
            {descriptionChild}
          </>
        ) : (
          <></>
        )}
      </Container>
    );
  };

  const renderAction = () => {
    return (
      <Button
        type="primary"
        state="default"
        title="Proceed"
        size="large"
        onPress={() => {
          props.navigation.goBack();
        }}
        {...testProps('goBackButton')}
      />
    );
  };

  return (
    <CenteredSingleScreen contents={renderContents()} action={renderAction()} />
  );
};

export default DeviceSecurityAlert;
