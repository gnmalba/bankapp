import React from 'react';
import {render} from '@testing-library/react-native';
import AlertScreen from '../AlertScreen';

describe('AlertScreen', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<AlertScreen />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
