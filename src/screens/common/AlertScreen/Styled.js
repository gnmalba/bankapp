/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  /* justify-content: center;
  align-items: center; */
`;

export const CenterContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
`;

export const Unlock: ComponentType<any> = styled.Image`
  width: ${scale(50)}px;
  height: ${scale(50)}px;
`;
