/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Overlay} from 'react-native-elements';
import Text from '@components/Text';

import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled(Overlay).attrs(() => ({
  backdropStyle: {backgroundColor: 'transparent'},
  overlayStyle: {
    backgroundColor: 'transparent',
    padding: 0,
    borderWidth: 0,
    elevation: 0,
    shadowOpacity: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
}))``;

export const StyledLoaderIcon: ComponentType<any> = styled.Image.attrs(
  (props) => ({
    source: require('@assets/icons/loader.png'),
  }),
)`
  width: ${scale(32)}px;
  height: ${scale(32)}px;
  tint-color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
`;

export const StyledText: ComponentType<any> = styled(Text).attrs(() => ({
  mode: 'body1',
}))`
  margin-top: ${verticalScale(20)}px;
`;
