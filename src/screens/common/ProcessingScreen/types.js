/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  label?: string,
  theme?: Object,
  route?: Object,
  hideLabel?: boolean,
};
