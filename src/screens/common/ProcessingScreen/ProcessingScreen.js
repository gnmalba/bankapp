/**
 * @format
 * @flow
 */

// React modules
import React, {useEffect} from 'react';
import {Animated, Easing} from 'react-native';

// Compositions
import SingleScreen from '@composition/SingleScreen';

// Utils

import type {PropsType} from './types';
import {Container, StyledLoaderIcon, StyledText} from './Styled';

const ProcessingScreen = (props: PropsType): React$Node => {
  const {label, theme, route} = props;

  const transform = new Animated.Value(0);

  useEffect(() => {
    Animated.loop(
      Animated.timing(transform, {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  }, [transform]);

  const spin = transform.interpolate({
    inputRange: [0, 1],
    // $FlowFixMe[speculation-ambiguous]
    outputRange: ['360deg', '0deg'],
  });

  return (
    <SingleScreen
      isFullScreen
      contents={
        <Container isVisible={true}>
          <>
            <Animated.View
              style={{
                transform: [{rotate: spin}],
              }}>
              <StyledLoaderIcon theme={theme} />
            </Animated.View>
            {props.hideLabel ? null : (
              <StyledText>
                {label || route?.params?.label || 'Processing transaction'}
              </StyledText>
            )}
          </>
        </Container>
      }
    />
  );
};

export default ProcessingScreen;
