## ProcessingScreen
ProcessingScreen component.

# Usage
```js
import ProcessingScreen from '@screens/common/ProcessingScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
