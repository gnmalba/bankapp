import React from 'react';
import {render} from '@testing-library/react-native';
import ProcessingScreen from '../ProcessingScreen';
import {testProps} from '@utils/componentCustomProps';

describe('ProcessingScreen', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ProcessingScreen {...testProps('processingScreenTestId')} />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
