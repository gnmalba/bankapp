import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {NavigationContainer} from '@react-navigation/native';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

import TermsAndCondition from '../TermsCondition';

jest.mock('@react-navigation/native', () => {
  const nav = jest.requireActual('@react-navigation/native');
  nav.useNavigation = () => jest.fn();
  return nav;
});

describe('Terms and Condition', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const navigation = {navigate: jest.fn()};

    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{
            installments: {
              currentInstallmentType: '',
            },
          }}>
          <ThemeProvider theme={DARK_THEME}>
            <TermsAndCondition
              {...testProps('activateCardStackId')}
              navigation={navigation}
            />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should work as expected for Balance Transfer', () => {
    // Edit the args inside render and provide props.
    const navigation = {navigate: jest.fn()};

    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{
            installments: {
              currentInstallmentType: 'BT',
            },
          }}>
          <ThemeProvider theme={DARK_THEME}>
            <TermsAndCondition
              {...testProps('activateCardStackId')}
              navigation={navigation}
            />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should work as expected for Balance Conversion', () => {
    // Edit the args inside render and provide props.
    const navigation = {navigate: jest.fn()};

    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{
            installments: {
              currentInstallmentType: 'BC',
            },
          }}>
          <ThemeProvider theme={DARK_THEME}>
            <TermsAndCondition
              {...testProps('activateCardStackId')}
              navigation={navigation}
            />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should work as expected for Cash 2 Go', () => {
    // Edit the args inside render and provide props.
    const navigation = {navigate: jest.fn()};

    const all = render(
      <NavigationContainer>
        <MockProvider
          store={{
            installments: {
              currentInstallmentType: 'C2GO',
            },
          }}>
          <ThemeProvider theme={DARK_THEME}>
            <TermsAndCondition
              {...testProps('activateCardStackId')}
              navigation={navigation}
            />
          </ThemeProvider>
        </MockProvider>
      </NavigationContainer>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
