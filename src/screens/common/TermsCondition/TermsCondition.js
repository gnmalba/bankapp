/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';

import SingleScreen from '@composition/SingleScreen';
import Text from '@components/Text';

import type {PropsTermsType} from './types';
import {testProps} from '@utils/componentCustomProps';
import {
  BalanceTransferData,
  BalanceConversionData,
  Cash2GoData,
} from './mockdata';
import {useSelector} from 'react-redux';
import {installmentsSelector} from '@ducks/installments/selectors';
import Separator from '@components/Separator';
const TermsAndCondition = (props: PropsTermsType): React$Node => {
  const {testID} = props;
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const installmentsState = useSelector(installmentsSelector);
  const {currentInstallmentType} = installmentsState;
  const balTrans = BalanceTransferData('');
  const balconv = BalanceConversionData('');
  const c2go = Cash2GoData();
  useEffect(() => {
    if (currentInstallmentType === 'BT') {
      setTitle(balTrans.title);
      setDescription(balTrans.description);
    } else if (currentInstallmentType === 'BC') {
      setTitle(balconv.title);
      setDescription(balconv.description);
    } else if (currentInstallmentType === 'C2GO') {
      setTitle(c2go.title);
      setDescription(c2go.description);
    }
  }, [currentInstallmentType, balTrans, balconv, c2go]);
  const renderContents = () => {
    return (
      <>
        <Text mode="h3" {...testProps((testID || 'termsCondition') + 'Title')}>
          {title}
        </Text>
        <Separator space={30} />
        <Text
          mode="body1"
          {...testProps((testID || 'termsCondition') + 'Title')}
          themeColor="TEXT_100_HIGH">
          {description}
        </Text>
      </>
    );
  };
  return <SingleScreen scrollEnabled contents={renderContents()} />;
};

export default TermsAndCondition;
