/**
 * @flow
 */

export type PropsTermsType = {
  testID?: string,
  accessibilityLabel?: string,
  title?: string,
  description?: string,
};
