## CheckboxComponent
CheckboxComponent component.

# Usage
```js
import CheckboxComponent from '@components/CheckboxComponent';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
