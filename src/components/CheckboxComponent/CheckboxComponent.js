/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Utils

import {moderateScale} from '@utils/scale';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';

type CheckBoxPropTypes = {
  title: string,
  size: number,
  checkedIcon: any,
  uncheckedIcon: any,
  checkedColor: string,
  uncheckedColor: string,
  isChecked: boolean,
  onPress: any,
  value: any,
  index: any,
  testID?: string,
  height?: number,
};

const CheckboxComponent = (props: CheckBoxPropTypes): React$Node => {
  const {testID, height = 20} = props;
  return (
    <>
      <S.StyledCheckbox
        {...testProps(testID || 'styledCheckBoxTestId')}
        activeOpacity={1}
        size={moderateScale(props.size)}
        title={props.title}
        checkedIcon={props.checkedIcon}
        uncheckedIcon={props.uncheckedIcon}
        checkedColor={props.checkedColor}
        uncheckedColor={props.uncheckedColor}
        checked={props.isChecked}
        onPress={() => props.onPress(props.value, props.index)}
        height={height}
        // disabled={props.disabled}
      />
    </>
  );
};

export default CheckboxComponent;
