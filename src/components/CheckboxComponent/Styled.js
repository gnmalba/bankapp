/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {CheckBox} from 'react-native-elements';
import {verticalScale, scale, moderateScale} from '@utils/scale';

export const StyledCheckbox: ComponentType<any> = styled(CheckBox).attrs(
  (props) => ({
    containerStyle: {
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      padding: 0,
      height: verticalScale(props.height),
      width: scale(162),
    },
    textStyle: {
      color: props.theme.color.TEXT_300_HIGH,
      fontSize: moderateScale(14),
      fontStyle: 'normal',
      fontWeight: 'normal',
      paddingLeft: 0,
      marginLeft: scale(6),
    },
  }),
)``;
