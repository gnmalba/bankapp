import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import CheckboxComponent from '../CheckboxComponent';
import {testProps} from '@utils/componentCustomProps';

describe('Card', () => {
  it('Should work as expected', () => {
    const all = render(
      <CheckboxComponent {...testProps('cardTestId')} onPress={() => {}} />,
    );
    const el = all.getByTestId('cardTestId');
    all.debug();
    fireEvent(el, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });
});
