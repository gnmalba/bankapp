import React from 'react';
import {render} from '@testing-library/react-native';
import Icon from '../index';

describe('ErrorModal', () => {
  it('Should work as expected', () => {
    const all = render(<Icon name="logout" size={30} color="white" />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
