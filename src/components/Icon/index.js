/**
 * @flow
 */
import type {Icon} from 'react-native-vector-icons';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import IcoMoonConfig from '@assets/selection.json';

const CustomIcon: Class<Icon<string>> = createIconSetFromIcoMoon(IcoMoonConfig);

export default CustomIcon;
