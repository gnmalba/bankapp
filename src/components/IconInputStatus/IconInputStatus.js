/**
 * @format
 * @flow
 */

import React, {createElement} from 'react';
import {Pressable} from 'react-native';
import Text from '@components/Text';
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import StyledImages, {
  IconInputStatusContainer,
  StyledInput,
  StyledIconContainer,
  statusIcons,
  StyledLeftContainer,
  StyledRightContainer,
  StyledPressableActiveStatus,
} from './Styled';

const IconInputStatus = (props: PropsType): React$Node => {
  const {
    state = '',
    icon,
    iconLabel,
    theme,
    customIcon,
    customStatus,
    showStatus,
    handleClear,
    iconPressHandler,
    testID = '',
    customBackgroundColor,
    dividerHeight = 20,
    ...rest
  } = props;
  const key = state === 'active' && icon === 'date' ? `${icon}${state}` : icon;
  const statusVisible = ['active', 'error', 'correct'].indexOf(state) !== -1;

  return (
    <>
      <IconInputStatusContainer
        state={state}
        isFromNewPassword={rest?.isFromNewPassword}
        customBackgroundColor={customBackgroundColor}>
        <Pressable onPress={iconPressHandler}>
          <StyledLeftContainer>
            {(icon || iconLabel || customIcon) && (
              <StyledIconContainer>
                {icon && key ? (
                  createElement(StyledImages[key], {
                    state,
                    theme,
                    ...testProps(testID + 'Icon'),
                  })
                ) : (
                  <></>
                )}
                {!icon && iconLabel && (
                  <Text {...testProps(testID + 'IconLabel')} mode="body2">
                    {iconLabel}
                  </Text>
                )}
                {!icon && customIcon}
              </StyledIconContainer>
            )}
            <StyledInput dividerHeight={dividerHeight} {...props} />
          </StyledLeftContainer>
        </Pressable>
        {showStatus && (
          <StyledRightContainer>
            <StyledPressableActiveStatus
              {...testProps(testID + 'StatusIconButton_' + state)}
              handleClear={handleClear}
              state={state}>
              {state &&
                customStatus === undefined &&
                statusIcons[state] &&
                statusVisible &&
                createElement(statusIcons[state], null)}
              {customStatus}
            </StyledPressableActiveStatus>
          </StyledRightContainer>
        )}
      </IconInputStatusContainer>
    </>
  );
};

IconInputStatus.defaultProps = {
  showStatus: true,
};

export default IconInputStatus;
