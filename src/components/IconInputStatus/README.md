## IconInputStatus

IconInputStatus component.

# Usage

```js
import IconInputStatus from '@components/IconInputStatus';

# Props
```

| Prop      | Type   | Required | Description                        |
| --------- | ------ | -------- | ---------------------------------- |
| textWidth | number | false    | Set the width of the the text area |
