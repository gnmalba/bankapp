/**
 * @flow
 */

import type {IconInputStatusType} from '@utils/commonTypes';

export type PropsType = {
  ...IconInputStatusType,
  textWidth?: string,
  customBackgroundColor?: string,
  dividerHeight?: number,
  isFromNewPassword?: any,
};
