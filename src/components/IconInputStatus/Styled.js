/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {moderateScale, scale, verticalScale} from '@utils/scale';

export const StyledCalendarImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    return {
      source: require('@assets/icons/calendar.png'),
      resizeMode: 'cover',
      tintColor:
        props.theme?.textInput?.iconInputStatusImagesTint || 'transparent',
    };
  },
)`
  width: ${moderateScale(19.5)}px;
  height: ${moderateScale(18)}px;
`;

export const StyledCalendarImageActive: ComponentType<any> = styled(
  StyledCalendarImage,
).attrs((props) => {
  return {
    source: require('@assets/icons/calendar-active.png'),
    tintColor: props.theme?.textInput?.calendarActiveTint || 'transparent',
  };
})``;

export const StyledUsernameImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    return {
      source: props.theme.textInput.username,
      resizeMode: 'cover',
      tintColor:
        props.theme?.textInput?.iconInputStatusImagesTint || 'transparent',
    };
  },
)`
  width: ${moderateScale(14)}px;
  height: ${moderateScale(14)}px;
`;

export const StyledSearchImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    return {
      source: require('@assets/icons/search-icon.png'),
      resizeMode: 'cover',
      tintColor:
        props.theme?.textInput?.iconInputStatusImagesTint || 'transparent',
    };
  },
)`
  tint-color: ${(props) =>
    props.theme.textInput?.iconColor?.search || 'transparent'};
  width: ${moderateScale(14)}px;
  height: ${moderateScale(14)}px;
  margin-horizontal: ${scale(-5)}px;
`;

export const StyledPasswordImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    return {
      source: props.theme.textInput.password,
      resizeMode: 'cover',
      tintColor:
        props.theme?.textInput?.iconInputStatusImagesTint || 'transparent',
    };
  },
)`
  width: ${moderateScale(18)}px;
  height: ${moderateScale(18)}px;
  margin-horizontal: ${scale(-2)}px;
`;

export const StyledCorrectStatus: ComponentType<any> = styled.Image.attrs(
  (props) => {
    return {
      source: require('@assets/icons/checkmark.png'),
      resizeMode: 'cover',
    };
  },
)`
  width: ${moderateScale(24)}px;
  height: ${moderateScale(24)}px;
`;

export const StyledErrorStatus: ComponentType<any> = styled(
  StyledCorrectStatus,
).attrs((props) => {
  return {
    source: require('@assets/icons/warning.png'),
  };
})``;

export const StyledActiveStatus: ComponentType<any> = styled(
  StyledCorrectStatus,
).attrs((props) => {
  return {
    source: require('@assets/icons/close.png'),
    tintColor: props.theme?.color?.TEXT_300_HIGH || 'transparent',
  };
})`
  tint-color: ${(props) =>
    props.theme.textInput?.iconColor?.clear || 'transparent'};
`;

export const StyledPressableActiveStatus: ComponentType<any> = styled.Pressable.attrs(
  (props) => {
    return {
      onPress: props.handleClear,
      disabled: props.state !== 'active',
    };
  },
)``;

export const IconInputStatusContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  height: ${moderateScale(48)}px;
  align-items: center;
  border-radius: 4px;
  background-color: ${(props) => {
    const {state, theme} = props;
    return (
      props.customBackgroundColor ||
      theme.textInput?.backgroundColor[state || 'default'] ||
      'transparent'
    );
  }};
  border: ${(props) => {
    const {state, theme} = props;
    if (props.isFromNewPassword) {
      return theme.textInput?.border.correct || 'transparent';
    }
    return theme.textInput?.border[state || 'default'] || 'transparent';
  }};
  padding-right: ${moderateScale(10)}px;
  padding-top: ${verticalScale(8)}px;
  padding-bottom: ${verticalScale(8)}px;
`;

export const StyledInput: ComponentType<any> = styled.TextInput.attrs(
  (props) => ({
    placeholderTextColor: props.theme?.textInput?.placeholderTextColor,
    editable: props.state !== 'disabled',
    width: props.textWidth || 'auto',
    height: props.dividerHeight,
    selectionColor: props.theme?.color?.TEXT_100_HIGH,
    maxLength: 50,
  }),
)`
  padding-left: ${moderateScale(15)}px;
  padding-right: ${moderateScale(15)}px;
  color: ${(props) => props.theme.textInput?.textColor || 'transparent'};
  border-left-color: ${(props) =>
    props.theme?.textInput?.dividerColor || 'transparent'};
  border-left-width: ${(props) =>
    props.icon || props.iconLabel || props.customIcon ? '1px' : '0px'};
  padding-top: 0px;
  padding-bottom: 0px;
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${moderateScale(18)}px;
`;

export const StyledIconContainer: ComponentType<any> = styled.View`
  padding-left: ${moderateScale(15)}px;
  padding-right: ${moderateScale(15)}px;
  padding-top: ${moderateScale(5)}px;
  padding-bottom: ${moderateScale(7)}px;
`;

export const StyledMobilePrefixContainer: ComponentType<any> = styled.View`
  margin-left: ${moderateScale(-8)}px;
  padding-right: ${moderateScale(14)}px;
`;

export const StyledLeftContainer: ComponentType<any> = styled.View`
  align-items: center;
  flex-direction: row;
  flex: 4;
`;

export const StyledRightContainer: ComponentType<any> = styled.View`
  flex-direction: row-reverse;
  flex: 1;
`;

export const statusIcons = {
  active: StyledActiveStatus,
  correct: StyledCorrectStatus,
  error: StyledErrorStatus,
  default: undefined,
  disabled: undefined,
  filled: undefined,
};

export default {
  date: StyledCalendarImage,
  dateactive: StyledCalendarImageActive,
  username: StyledUsernameImage,
  password: StyledPasswordImage,
  search: StyledSearchImage,
};
