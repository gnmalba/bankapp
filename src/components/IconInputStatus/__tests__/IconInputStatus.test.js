import React from 'react';
import {Text} from 'react-native';
import {render} from '@testing-library/react-native';
import IconInputStatus from '../IconInputStatus';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

const customIcon = <Text>{'>'}</Text>;

describe('IconInputStatus', () => {
  it.each(['date', 'username', 'password', undefined])(
    'Should work',
    (icon) => {
      const all = render(
        <ThemeProvider theme={DARK_THEME}>
          <IconInputStatus
            icon={icon}
            iconLabel="PHP"
            state="active"
            customIcon={customIcon}
          />
        </ThemeProvider>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
  it.each(['error', 'active', 'correct'])(
    'Should work as expected',
    (state) => {
      const all = render(<IconInputStatus icon="date" state={state} />);
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
  it('Should work properly', () => {
    const all = render(<IconInputStatus state="active" />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
