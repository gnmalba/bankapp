/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Modal from 'react-native-modal';

export const StyledModal: ComponentType<any> = styled(Modal)`
  margin: 0;
  justify-content: flex-end;
`;
