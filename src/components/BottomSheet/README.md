## BottomSheet
BottomSheet component.

# Usage
```js
import BottomSheet from '@components/BottomSheet';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
