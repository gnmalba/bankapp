/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {StyledModal} from './Styled';
import {testProps} from '@utils/componentCustomProps';

const BottomSheet = ({
  visible = false,
  onBackdropPress,
  onBackButtonPress,
  onModalHide,
  hasBackdrop = true,
  avoidKeyboard = true,
  useNativeDriver = true,
  testID = '',
  accessibilityLabel = '',
  children,
  styles,
}: PropsType): React$Node => {
  return (
    <StyledModal
      isVisible={visible}
      hasBackdrop={hasBackdrop}
      backdropColor="black"
      backdropOpacity={0.6}
      onBackdropPress={onBackdropPress}
      onBackButtonPress={onBackButtonPress}
      onModalHide={onModalHide}
      avoidKeyboard={avoidKeyboard}
      useNativeDriver={useNativeDriver}
      styles={styles}
      {...testProps(`${testID || accessibilityLabel}Modal`)}>
      {children}
    </StyledModal>
  );
};

export default BottomSheet;
