/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  children: React$Node,
  visible?: boolean,
  hasBackdrop?: boolean,
  onBackdropPress?: () => void,
  onBackButtonPress?: () => void,
  onModalHide?: () => void,
  useNativeDriver?: boolean,
  avoidKeyboard?: boolean,
  styles?: Object,
};
