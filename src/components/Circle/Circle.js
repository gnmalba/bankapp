/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';

const Circle = (props: PropsType): React$Node => {
  const {size, children} = props;
  return (
    <S.CircleContainer>
      <S.CircleItem size={size}>{children}</S.CircleItem>
    </S.CircleContainer>
  );
};

export default Circle;
