import React from 'react';
import {render} from '@testing-library/react-native';
import Circle from '../Circle';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('Circle', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <Circle size={12} />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
