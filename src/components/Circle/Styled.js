/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale} from '@utils/scale';

export const CircleContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
`;

export const CircleItem: ComponentType<any> = styled.View`
  height: ${(props) => scale(props.size)}px;
  width: ${(props) => scale(props.size)}px;
  border-radius: ${(props) => scale(props.size / 2)}px;
  background-color: ${(props) => props.theme.circleBackground}
  justify-content: center;
  align-items: center;
  margin: 10px;
`;
