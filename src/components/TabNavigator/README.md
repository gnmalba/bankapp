## TabNavigator
TabNavigator component.

# Usage
```js
import TabNavigator from '@components/TabNavigator';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
