/**
 * @flow
 */

import styled from 'styled-components/native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {moderateScale, scale} from '@utils/scale';
import type {ComponentType} from 'react';

const Tab = createMaterialTopTabNavigator();

const StyledTabNavigator: ComponentType<any> = styled(Tab.Navigator).attrs(
  (props) => ({
    sceneContainerStyle: {
      backgroundColor: props.theme.color.BACKGROUND_200,
    },
    tabBarOptions: {
      scrollEnabled: props.scrollEnabled ? props.scrollEnabled : false,
      allowFontScaling: false,
      activeTintColor: props.theme.color.TEXT_300_HIGH,
      inactiveTintColor: props.theme.color.TEXT_100_HIGH,
      indicatorContainerStyle: {
        borderBottomColor: props.theme.tabNavigator.borderBottomColor,
        borderBottomWidth: 2,
      },
      style: {
        backgroundColor: props.theme.color.BACKGROUND_200,
      },
      labelStyle: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: moderateScale(12),
        width: scale(100),
      },
      indicatorStyle: {
        testID: 'TabIndicatorTestId',
        borderBottomColor: props.theme.tab.activeTintColor,
        borderBottomWidth: 2,
        position: 'absolute',
        bottom: -2,
      },
    },
  }),
)``;

const TabScreen: ComponentType<any> = Tab.Screen;
export {StyledTabNavigator, TabScreen};
