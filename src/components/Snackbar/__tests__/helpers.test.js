import {timeSnackbar} from '../helpers';

describe('helpers', () => {
  test('helpers', () => {
    expect(timeSnackbar(1000, () => {})).toBeUndefined();
  });
});
