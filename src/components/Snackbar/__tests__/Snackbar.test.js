import React from 'react';
import {render} from '@testing-library/react-native';
import Snackbar from '../Snackbar';

describe('Snackbar', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<Snackbar />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
