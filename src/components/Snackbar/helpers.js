// @flow

export const timeSnackbar = (
  msBeforeFadeOut: number,
  setter: Function,
): void => {
  setter(true);
  setTimeout(() => {
    setter(false);
  }, msBeforeFadeOut);
};
