/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import Text from '@components/Text';
import {TextContainer, StyledSnackbar, Container, SnackbarIcon} from './Styled';

const Snackbar = (props: PropsType): React$Node => {
  const {visible, message, testID, accessibilityLabel} = props;
  return (
    <StyledSnackbar
      testID={testID}
      accessibilityLabel={accessibilityLabel}
      visible={visible}
      textMessage={() => (
        <Container>
          <SnackbarIcon />
          <TextContainer>
            <Text mode="body2" customColor="#166437">
              {message}
            </Text>
          </TextContainer>
        </Container>
      )}
    />
  );
};

export default Snackbar;
