/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Platform} from 'react-native';
import styled from 'styled-components/native';
import {verticalScale, scale} from '@utils/scale';
import SnackBar from 'react-native-snackbar-component';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  padding: ${verticalScale(12)}px ${verticalScale(0)}px ${verticalScale(10)}px
    ${verticalScale(0)}px;
`;

export const SnackbarIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  resizeMode: 'contain',
  source: require('@assets/icons/snackbar-check.png'),
}))`
  height: ${verticalScale(12)}px;
  width: ${scale(16)}px;
  margin-right: ${Platform.OS === 'ios' ? scale(6) : scale(7)}px;
  margin-left: ${scale(12)}px;
`;

export const StyledSnackbar: ComponentType<any> = styled(SnackBar).attrs(
  (props) => ({
    bottom: props.bottom ? verticalScale(70) : verticalScale(25),
    backgroundColor: '#97D7B2',
    messageColor: '#166437',
    containerStyle: {
      borderWidth: 1,
      borderColor: '#126E39',
      marginHorizontal: scale(16),
      borderRadius: 4,
    },
  }),
)``;

export const TextContainer: ComponentType<any> = styled.View`
  margin-right: ${scale(40)}px;
`;
