/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {Container, Icon, Unread} from './Styled';

const AdvisoryIcon = (props: PropsType): React$Node => {
  const {onPress, unread = true} = props;

  return (
    <Container onPress={onPress}>
      <Icon />
      {unread ? <Unread unread={unread} /> : <></>}
    </Container>
  );
};

export default AdvisoryIcon;
