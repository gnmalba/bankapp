import React from 'react';
import {render} from '@testing-library/react-native';
import AdvisoryIcon from '../AdvisoryIcon';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('AdvisoryIcon', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <AdvisoryIcon />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
