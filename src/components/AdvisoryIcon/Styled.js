/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.TouchableOpacity`
  flex-direction: row;
`;

export const Icon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: props.theme.mainTopMenuItems.notification,
  tintColor: props.theme?.mainTopMenuItems?.color || 'transparent',
}))`
  width: ${moderateScale(18)}px;
  height: ${moderateScale(22)}px;
  margin-right: 10px;
`;

export const Unread: ComponentType<any> = styled.View`
  width: ${moderateScale(14.5)}px;
  height: ${moderateScale(14.5)}px;
  background-color: #d0483f;
  border-radius: 10px;
  transform: translateX(${moderateScale(-17)}px);
  border-color: ${(props) => props.theme.color.BACKGROUND_300};
  border-width: ${moderateScale(3.5)}px;
`;
