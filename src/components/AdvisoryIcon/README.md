## AdvisoryIcon
AdvisoryIcon component.

# Usage
```js
import AdvisoryIcon from '@components/AdvisoryIcon';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
