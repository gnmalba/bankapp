/**
 * @flow
 */

export type PropsType = {
  onPress?: () => void,
  containerStyle?: Object,
  imageStyle?: Object,
  testID?: string,
  accessibilityLabel?: string,
  source: any,
  children?: Node,
};
