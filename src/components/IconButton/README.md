## IconButton

IconButton component.

# Usage

```js
import IconButton from '@components/IconButton';

# Props
```

| Prop               | Type         | Required | Description                      |
| ------------------ | ------------ | -------- | -------------------------------- |
| onPress            | function     | false    | The press handler                |
| containerStyle     | Style Object | false    | Style of the parent container    |
| imageStyle         | Style Object | false    | Style of the image               |
| testID             | string       | false    | test id                          |
| accessibilityLabel | string       | false    | accessibility label              |
| source             | any          | true     | source of the image              |
| children           | Node         | false    | any additional nodes after image |

# references

1. headerLeft for login navigator
2. headerRight for login navigator

# Example

```js
/**
 * @flow
 */

import React from 'react';
import {CENTER, IS_DEVICE_TABLET} from '@utils/constants';
import {moderateScale, verticalScale} from '@utils/scale';
import IconButton from '@components/IconButton';

import type {navigationOptionType} from '../types';
const source = require('@assets/icons/back-icon.png');

export const navigationOption: navigationOptionType = {
  headerTitleAllowFontScaling: false,
  headerBackTitleVisible: false,
  headerLeft: (props) => <IconButton source={source} {...props} />,
  headerStyle: {
    borderBottomWidth: 0,
    shadowOpacity: 0,
  },
  headerTitleAlign: CENTER,
  headerTitleStyle: {
    fontSize: moderateScale(IS_DEVICE_TABLET ? 16 : 20),
    lineHeight: verticalScale(24),
  },
  headerRightContainerStyle: {
    marginRight: moderateScale(IS_DEVICE_TABLET ? -10 : 0),
  },
  cardStyle: {opacity: 1},
};
```
