/**
 * @flow
 */

import {StyleSheet} from 'react-native';
import {scale, verticalScale} from '@utils/scale';

type StylesType = {
  imageStyle: Object,
  containerStyle: Object,
};

const Styles: StylesType = StyleSheet.create({
  containerStyle: {
    marginHorizontal: scale(16),
  },
  imageStyle: {
    height: verticalScale(15),
    width: scale(17),
  },
});

export default Styles;
