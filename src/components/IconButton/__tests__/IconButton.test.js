import React from 'react';
import {render} from '@testing-library/react-native';
import IconButton from '@components/IconButton/IconButton';

describe('IconButton', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<IconButton />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
