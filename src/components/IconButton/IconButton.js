/**
 * @format
 * @flow
 */

import React from 'react';
import {Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {CONTAIN} from '@utils/constants';

import type {PropsType} from './types';
import Styles from './styles';
import {DEFAULT_TEST_ID} from './config';

const IconButton = (props: PropsType): React$Element<any> => {
  const {
    onPress,
    containerStyle,
    testID,
    accessibilityLabel,
    children,
    imageStyle,
    source,
  } = props;

  return (
    <TouchableOpacity
      style={[Styles.containerStyle, containerStyle]}
      onPress={onPress}
      testID={testID}
      accessibilityLabel={accessibilityLabel}>
      <Image
        resizeMode={CONTAIN}
        source={source}
        style={[Styles.imageStyle, imageStyle]}
      />
      {children}
    </TouchableOpacity>
  );
};

IconButton.defaultProps = {
  testID: DEFAULT_TEST_ID,
  accessibilityLabel: DEFAULT_TEST_ID,
};

export default IconButton;
