/**
 * @flow
 */
import React from 'react';
import type {ComponentType} from 'react';

import CalendarPicker from 'react-native-calendar-picker';
import styled from 'styled-components/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {scale, moderateScale} from '@utils/scale';
import moment from 'moment';

export const StyledIcon: ComponentType<any> = styled(MaterialIcons)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  margin-left: ${scale(5)}px;
`;

export const StyledCalendar: ComponentType<any> = styled(CalendarPicker).attrs(
  (props) => {
    // added logic for color of selected date
    const state = props.forwardedRef?.current?.state;
    let isSame = false;
    if (state) {
      isSame = moment(state.selectedStartDate).isSame(state.selectedEndDate);
    }
    return {
      months: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
      ],
      previousComponent: (
        <StyledIcon
          name="keyboard-arrow-left"
          size={moderateScale(22)}
          theme={props.theme}
        />
      ),
      nextComponent: (
        <StyledIcon
          name="keyboard-arrow-right"
          size={moderateScale(22)}
          theme={props.theme}
        />
      ),
      textStyle: {
        color: props.theme.color.TEXT_300_HIGH,
        fontWeight: '600',
        fontFamily: 'MyriadPro-Semibold',
        marginRight: scale(2),
      },
      todayBackgroundColor: 'transparent',
      todayTextStyle: {
        color: state?.selectedStartDate
          ? props.theme.calendar.todayTextColor
          : 'white',
      },
      disabledDatesTextStyle: props.theme.color.TEXT_300_LOW,
      customDayHeaderStyles: () => {
        return {
          textStyle: {color: props.theme.color.TEXT_200_MEDIUIM, opacity: 1},
        };
      },
      dayLabelsWrapper: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        marginLeft: scale(50),
      },
      selectedRangeStartStyle: {
        backgroundColor: props.theme.calendar.defaultDateColor,
      },
      selectedRangeEndStyle: {
        backgroundColor: props.theme.calendar.defaultDateColor,
      },
      selectedRangeStyle: {
        backgroundColor: isSame
          ? props.theme.calendar.defaultDateColor
          : props.theme.calendar.defaultDateColorWithOpacity,
        borderWidth: 0,
      },
      selectedDayColor: props.theme.color.SECONDARY_DEFAULT,
      selectedDayTextColor: props.theme.calendar.selectedDayTextColor,
    };
  },
)``;
