/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  forwardedRef?: string,
  calendarProps?: Object,
  theme?: Object,
};
