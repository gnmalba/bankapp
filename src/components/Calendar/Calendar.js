/**
 * @format
 * @flow
 */

// React modules
import React from 'react';

// Components
import {StyledCalendar} from './Styled';

// Types
import type {PropsType} from './types';

const Calendar = (props: PropsType): React$Node => {
  const {calendarProps} = props;
  return (
    <StyledCalendar
      ref={calendarProps?.forwardedRef}
      startFromMonday={true}
      {...calendarProps}
    />
  );
};

export default Calendar;
