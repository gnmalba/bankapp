import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import {render} from '@testing-library/react-native';
import Calendar from '../Calendar';
import {testProps} from '@utils/componentCustomProps';
import {DARK_THEME} from '@utils/constants';
import moment from 'moment';

describe('Calendar', () => {
  const mockRef = {
    current: {
      state: {
        selectedStartDate: moment(),
        selectedEndDate: moment(),
      },
    },
  };

  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    render(
      <ThemeProvider theme={DARK_THEME}>
        <Calendar {...testProps('calendarTestId')} forwardedRef={mockRef} />
      </ThemeProvider>,
    );
    // expect(all.toJSON()).toMatchSnapshot();
  });
});
