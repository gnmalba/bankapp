/**
 * @format
 * @flow
 */

import React, {useState, createRef} from 'react';

import type {DropdownProps, DropdownItemsProps} from './types';

import {
  Dropdown,
  DropdownTitle,
  DropdownButton,
  DropdownButtonLabel,
  DropdownItemsContainer,
} from './Styled';

import {TOOLTIP_HEIGHT, TOOLTIP_WIDTH} from './constants';

const handlePressTooltip = (ref: Object) => {
  ref.current.toggleTooltip();
};

const renderDropdownTitle = (dropdownTitle?: string) => {
  if (dropdownTitle) {
    return <DropdownTitle>{dropdownTitle}</DropdownTitle>;
  }
  return <></>;
};

const renderDropdownButtonLabel = (buttonLabel?: string) => {
  if (buttonLabel) {
    return <DropdownButtonLabel>{buttonLabel}</DropdownButtonLabel>;
  }
  return <></>;
};

// component parts
const DropdownItems = (props: DropdownItemsProps) => {
  const {setIsActive, dropdownTitle, dropdownItems, onClose = () => {}} = props;

  React.useEffect(() => {
    // set isActive state to false on unmount of dropdown
    return () => {
      setIsActive(false);
      onClose();
    };
  }, [onClose, setIsActive]);

  return (
    <DropdownItemsContainer>
      {renderDropdownTitle(dropdownTitle)}
      {dropdownItems}
    </DropdownItemsContainer>
  );
};

const DropdownPopover = (props: DropdownProps): React$Node => {
  const {customButtonContent, buttonLabel, onClose} = props;
  const sortTooltipRef = createRef();

  const [isActive, setIsActive] = useState(false);

  return (
    <>
      <DropdownButton
        {...props}
        isActive={isActive}
        onPress={() => {
          setIsActive(true);
          handlePressTooltip(sortTooltipRef);
        }}>
        {customButtonContent}
        {renderDropdownButtonLabel(buttonLabel)}
      </DropdownButton>
      <Dropdown
        ref={sortTooltipRef}
        height={TOOLTIP_HEIGHT}
        width={TOOLTIP_WIDTH}
        withPointer={false}
        overlayColor="transparent"
        popover={
          <DropdownItems
            {...props}
            setIsActive={setIsActive}
            onClose={onClose}
          />
        }
      />
    </>
  );
};

export default DropdownPopover;
