/**
 * @flow
 */

export type DropdownProps = {
  buttonLabel?: string,
  dropdownTitle?: string,
  dropdownItems?: React$Node,
  onClose: any,
  customButtonContent?: React$Node,
};

export type DropdownItemsProps = {
  buttonLabel?: string,
  dropdownTitle?: string,
  dropdownItems?: React$Node,
  setIsActive: (boolean | void) => boolean | void,
  onClose: any,
};
