/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Tooltip} from 'react-native-elements';
import styled from 'styled-components/native';

import Text from '@components/Text';

import {TOOLTIP_HEIGHT} from './constants';

export const DropdownItemsContainer: ComponentType<any> = styled.View`
  display: flex;
  align-content: flex-start;
  flex-shrink: 1;
  flex-direction: column;
  margin-top: 14;
  min-width: auto;
  min-height: auto;
`;

export const Dropdown: ComponentType<any> = styled(Tooltip).attrs((props) => ({
  ...props,
  containerStyle: {
    marginTop: 8,
    padding: 16,
    borderRadius: 4,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    shadowColor: '#000',
    shadowOffset: {
      width: 4,
      height: 4,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6,
    minHeight: TOOLTIP_HEIGHT,
    backgroundColor: props.theme.displayOptionsTooltip.tooltip,
  },
}))``;

export const DropdownButton: ComponentType<any> = styled.TouchableOpacity`
  padding: 8px;
  width: auto;
  background-color: ${(props) =>
    props.isActive ? props.theme.displayOptionsTooltip.tooltip : 'transparent'};
`;

export const DropdownButtonLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'link',
    themeColor: 'SECONDARY_DEFAULT',
  }),
)``;

export const DropdownTitle: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'overline2',
    themeColor: 'TEXT_100_HIGH',
  }),
)``;
