## WidgetTooltip
WidgetTooltip component.

# Usage
```js
import WidgetTooltip from '@components/WidgetTooltip';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
