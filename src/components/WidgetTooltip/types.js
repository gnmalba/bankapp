/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  style: Object,
  popover: Object,
  forwardedRef: any,
  width: number,
  setTooltipButtonEnabled: any,
};
