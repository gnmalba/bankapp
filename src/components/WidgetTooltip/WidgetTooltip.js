// React modules
import React, {useState} from 'react';
import {View, Platform, Dimensions} from 'react-native';

// Third party libraries
import {Tooltip} from 'react-native-elements';

// Utils
import {verticalScale, scale} from '@utils/scale';
import type {PropsType} from './types';
import styled from 'styled-components';

const {deviceHeight} = Dimensions.get('window');

const getDeviceHeight = deviceHeight;

const StyledTooltip = styled(Tooltip).attrs((props) => ({
  backgroundColor: props.theme?.displayOptionsTooltip?.tooltip,
}))``;

const ToolTipComponent = (props: PropsType): React$Node => {
  const {
    style,
    popover,
    forwardedRef,
    width,
    setTooltipButtonEnabled,
    onToolTipClose,
  } = props;
  const [sizeOfTooltip, setSizeOfTooltip] = useState({
    layoutHeight: 0,
    layoutWidth: 0,
  });
  const handleOnlayout = (event) => {
    setSizeOfTooltip({
      layoutHeight: event.nativeEvent.layout.height,
      layoutWidth: event.nativeEvent.layout.width,
    });
  };

  const platFormStyle = {
    android: {
      containerStyle: style
        ? style
        : {
            marginTop:
              getDeviceHeight > 700 ? verticalScale(35) : verticalScale(-5),
          },

      width: width ? width : scale(192),
      height: sizeOfTooltip.layoutHeight
        ? sizeOfTooltip.layoutHeight + 40
        : null,
    },
    ios: {
      containerStyle: style ? style : {minHeight: verticalScale(236)},
      width: width ? width : scale(192),
      height: sizeOfTooltip.layoutHeight
        ? sizeOfTooltip.layoutHeight + verticalScale(30)
        : null,
    },
  };

  return (
    <StyledTooltip
      ref={forwardedRef}
      withOverlay
      withPointer={false}
      overlayColor="#00000054"
      popover={<View onLayout={handleOnlayout}>{popover}</View>}
      containerStyle={
        Platform.OS === 'ios'
          ? platFormStyle.ios.containerStyle
          : platFormStyle.android.containerStyle
      }
      width={
        Platform.OS === 'ios'
          ? platFormStyle.ios.width
          : platFormStyle.android.width
      }
      height={
        Platform.OS === 'ios'
          ? platFormStyle.ios.height
          : platFormStyle.android.height
      }
      onClose={() => {
        setTooltipButtonEnabled(false);
        onToolTipClose && onToolTipClose();
      }}
    />
  );
};

export default ToolTipComponent;
