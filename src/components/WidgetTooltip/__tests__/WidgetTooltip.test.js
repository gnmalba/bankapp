import React from 'react';
import {render} from '@testing-library/react-native';
import WidgetTooltip from '../WidgetTooltip';

describe('WidgetTooltip', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<WidgetTooltip />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
