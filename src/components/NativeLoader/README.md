## NativeLoader
NativeLoader component.

# Usage
```js
import NativeLoader from '@components/NativeLoader';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
