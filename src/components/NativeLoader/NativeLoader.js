/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {StyledActivityIndicator, Container} from './Styled';

const NativeLoader = ({size = 30, color}: PropsType): React$Node => (
  <Container>
    <StyledActivityIndicator size={size} color={color} />
  </Container>
);

export default NativeLoader;
