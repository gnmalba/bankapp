/**
 * @flow
 */

import type {ComponentType} from 'react';
import {ActivityIndicator} from 'react-native';
import styled from 'styled-components/native';

import {scale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const StyledActivityIndicator: ComponentType<any> = styled(
  ActivityIndicator,
).attrs((props) => {
  return {
    size: scale(props.size),
    color: props.color ? props.color : props.theme.color.TEXT_300_HIGH,
  };
})``;
