import React from 'react';
import {render} from '@testing-library/react-native';
import NativeLoader from '../NativeLoader';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('NativeLoader', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <NativeLoader />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
