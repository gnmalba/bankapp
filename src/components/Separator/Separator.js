/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';

const Separator = (props: PropsType): React$Node => {
  return <S.Separator space={props.space} />;
};

export default Separator;
