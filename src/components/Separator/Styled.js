import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const Separator: ComponentType<any> = styled.View`
  padding-bottom: ${(props) => verticalScale(props.space)}px;
`;
