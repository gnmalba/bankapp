import React from 'react';
import {render} from '@testing-library/react-native';
import Separator from '../Separator';

describe('Separator', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<Separator />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
