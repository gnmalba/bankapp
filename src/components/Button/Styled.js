/**
 * @flow
 */

import styled from 'styled-components/native';
import Text from '@components/Text';
import type {ComponentType} from 'react';
import {moderateScale, verticalScale, scale} from '@utils/scale';
import * as C from '@components/Button/config';

export const Title: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: props.type === C.LINK ? 'buttonlink' : 'buttontext',
}))`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 700;
  font-size: ${moderateScale(16)}px;
  line-height: ${moderateScale(19.2)}px;
  color: ${(props) => {
    let color = '#ffffff';
    if (props.type === C.PRIMITIVE) {
      color = props.size === C.LARGE ? '#000000' : '#212121';
    }
    if (props.type === C.PRIMARY && props.state === C.DISABLED) {
      color = props.theme?.button?.textColor?.disabled;
    }
    if (props.type === C.SECONDARY || props.type === C.LINK) {
      if (props.state === C.DEFAULT) {
        color = '#00539F';
      }
      if (props.state === C.PRESSED) {
        color = '#004585';
      }
      if (props.state === C.DISABLED) {
        color = '#8DA0B2';
      }
    }
    if (props.state === C.RESET) {
      color = '#808080';
    }
    return (
      props.theme?.color[props.titleThemeColor] ||
      props.titleCustomColor ||
      color
    );
  }};
`;

export const TitleContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => {
    let color = 'transparent';
    if (props.state === C.DISABLED && props.type === C.PRIMARY) {
      color = props.theme?.button?.backgroundColor?.disabled;
    }
    if (props.type === C.PRIMITIVE) {
      color = '#CCCCCC';
    }
    if (props.type === C.PRIMARY) {
      if (props.state === C.DEFAULT) {
        color = '#00539F';
      }
      if (props.state === C.PRESSED) {
        color = '#004585';
      }
    }
    return props.customBackgroundColor || color;
  }};
  border-radius: ${(props) =>
    props.type === C.PRIMARY ? moderateScale(4) + 'px' : '0px'};
  padding-top: ${(props) => {
    if (props.type === C.PRIMARY) {
      return props.size === C.REGULAR
        ? moderateScale(8) + 'px'
        : moderateScale(15) + 'px';
    }
    return '0px';
  }};
  padding-bottom: ${(props) => {
    if (props.type === C.PRIMARY) {
      return props.size === C.REGULAR
        ? moderateScale(8) + 'px'
        : moderateScale(14) + 'px';
    }
    return '0px';
  }};
`;

export const FabImage: ComponentType<any> = styled.Image`
  resize-mode: contain;
  tint-color: ${(props) => props?.customIconColor || 'white'};
  margin-right: ${(props) => {
    return props.title ? moderateScale(8) + 'px' : 0;
  }};
  height: ${(props) => {
    return (
      verticalScale(props.customIconHeight) +
        (props.customIconHeight && 'px') || verticalScale(20) + 'px'
    );
  }};
  width: ${(props) => {
    return (
      scale(props.customIconWidth) + (props.customIconWidth && 'px') ||
      scale(20) + 'px'
    );
  }};
`;

export const FabContainer: ComponentType<any> = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding: ${(props) => {
    return scale(16) + 'px';
  }};
  background-color: ${(props) => {
    let color = 'transparent';
    if (props.state === C.DISABLED && props.type === C.FAB) {
      color = props.theme?.button?.backgroundColor?.disabled;
    }
    if (props.type === C.FAB) {
      if (props.state === C.DEFAULT) {
        color = '#00539F';
      }
      if (props.state === C.PRESSED) {
        color = '#004585';
      }
    }
    return props.customBackgroundColor || color;
  }};
  border-radius: ${(props) => {
    return props.title ? scale(26) + 'px' : scale(50) + 'px';
  }};
  width: ${(props) => {
    return props.title
      ? scale(props.customWidth || 94) + 'px'
      : scale(56) + 'px';
  }};
  box-shadow: 0px 2px 4px #05182e;
`;
