## Button

Button component.

# Usage

```js
import Button from '@components/Button';
```

# Types

```js
export type ButtonType = 'link' | 'primary' | 'secondary' | 'primitive';
export type ButtonSize = 'large' | 'regular';
export type ButtonState = 'default' | 'pressed' | 'disabled' | 'reset';
export type themeColorType =
  | 'PRIMARY_DISABLED'
  | 'PRIMARY_DEFAULT'
  | 'PRIMARY_PRESSED'
  | 'SECONDARY_DISABLED'
  | 'SECONDARY_DEFAULT'
  | 'SECONDARY_PRESSED'
  | 'BACKGROUND_300'
  | 'BACKGROUND_200'
  | 'BACKGROUND_100'
  | 'TEXT_300_LOW'
  | 'TEXT_200_MEDIUIM'
  | 'TEXT_100_HIGH'
  | 'GREEN_STATUS'
  | 'RED_STATUS'
  | 'DISABLED_STATUS'
  | 'ORANGE_STATUS'
  | 'YELLOW_STATUS';
```

# Props

```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | Test ID
accessibilityLabel        | string                | true                    | Accessibility label
title                     | string                | true                    | The title of the button
titleThemeColor           | themeColorType        | false                   | Theme color name
titleCustomColor          | themeColorType        | false                   | A user defined color value.
customBackgroundColor     | string                | false                   | The customized background color
type                      | ButtonType            | false                   | One of the few defined type
size                      | ButtonSize            | false                   | One of the few defined size
state                     | ButtonState           | false                   | One of the few defined state
onPress                   | Function              | false                   | The onpRess handler function
```

# Example

```js
<Button
  title="Button text"
  type="primary"
  state="disabled"
  size="large"
  titleThemeColor="TEXT_300_LOW"
  onPress={() => {
    console.dir('Hello World');
  }}
/>
```
