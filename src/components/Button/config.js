/**
 * @flow
 */

export const DISABLED = 'disabled';
export const DEFAULT = 'default';
export const PRESSED = 'pressed';
export const RESET = 'reset';
export const LINK = 'link';
export const PRIMARY = 'primary';
export const SECONDARY = 'secondary';
export const PRIMITIVE = 'primitive';
export const FAB = 'fab';
export const LARGE = 'large';
export const REGULAR = 'regular';
