/**
 * @flow
 */

import type {themeColorType} from '@utils/commonTypes';

export type ButtonType = 'link' | 'primary' | 'secondary' | 'primitive' | 'fab';

export type ButtonSize = 'large' | 'regular';

export type ButtonState = 'default' | 'pressed' | 'disabled' | 'reset';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  title?: string,
  titleThemeColor?: themeColorType,
  titleCustomColor?: string,
  customBackgroundColor?: string,
  type?: ButtonType,
  size?: ButtonSize,
  state?: ButtonState,
  icon?: string,
  customIconWidth?: number,
  customIconHeight?: number,
  customIconColor?: string,
  source?: string,
  onPress?: () => any,
  labelTestID?: string,
  activeOpacity?: number,
};
