/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity} from 'react-native';
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import {Title, TitleContainer, FabContainer, FabImage} from './Styled';

const Button = (props: PropsType): React$Node => {
  const {
    title,
    onPress,
    testID,
    accessibilityLabel,
    labelTestID,
    activeOpacity,
    ...rest
  } = props;
  const renderButton = () => {
    switch (rest.type) {
      case 'primary':
      case 'primitive':
        return (
          <TitleContainer {...rest}>
            {props.icon && (
              <FabImage
                source={props.icon}
                {...props}
                {...testProps(
                  labelTestID ||
                    (accessibilityLabel || 'btnIconTestId') + 'Icon',
                )}
              />
            )}
            <Title
              {...testProps(
                labelTestID ||
                  (accessibilityLabel || 'btnTitleTestId') + 'Label',
              )}
              {...rest}>
              {title}
            </Title>
          </TitleContainer>
        );
      case 'fab':
        return (
          <FabContainer {...props}>
            <FabImage source={props.icon} {...props} />
            <Title
              {...testProps(
                labelTestID ||
                  (accessibilityLabel || 'btnTitleTestId') + 'Label',
              )}
              {...rest}>
              {title}
            </Title>
          </FabContainer>
        );
      default:
        return (
          <Title
            {...testProps(
              labelTestID || (accessibilityLabel || 'btnTitleTestId') + 'Label',
            )}
            {...rest}>
            {title}
          </Title>
        );
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      onPress={onPress}
      {...testProps(testID || 'buttonTestId')}
      disabled={rest.state === 'disabled'}>
      {renderButton()}
    </TouchableOpacity>
  );
};

export default Button;
