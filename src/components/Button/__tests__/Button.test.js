import React from 'react';
import {render} from '@testing-library/react-native';
import Button from '../Button';
import {DARK_THEME} from '@utils/constants';

describe('Button', () => {
  it.each([
    {type: 'link', size: 'large', state: 'default', title: 'Button'},
    {type: 'primary', size: 'large', state: 'pressed'},
    {type: 'primary', size: 'regular', state: 'pressed'},
    {type: 'secondary', size: 'regular', state: 'disabled'},
    {
      type: 'fab',
      size: 'regular',
      state: 'disabled',
      title: 'Button',
      customIconHeight: 20,
    },
    {type: 'fab', size: 'regular', state: 'default', customIconWidth: 20},
    {type: 'fab', size: 'regular', state: 'reset'},
    {type: 'fab', size: 'regular', state: 'pressed'},
    {type: 'primitive', size: 'regular', state: 'reset'},
    {type: 'primitive', size: 'large', state: 'reset', title: 'Button'},
    {type: 'primary', state: 'disabled'},
    {type: 'secondary', size: 'regular', state: 'pressed'},
    {type: 'primary', state: 'default', title: 'Button'},
    {type: 'primary', state: 'pressed'},
    {},
  ])('Should work as expected', (param) => {
    const all = render(
      <Button
        type={param.type}
        size={param.size}
        state={param.state}
        theme={DARK_THEME}
        title={param.title}
        customIconWidth={param.customIconWidth}
        customIconHeight={param.customIconHeight}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
