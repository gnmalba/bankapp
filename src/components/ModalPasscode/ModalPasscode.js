/**
 * @format
 * @flow
 */
// React modules
import React from 'react';
import {Modal} from 'react-native';

import type {PropsType} from './types';
import * as S from './Styled';

// Components
import Button from '@components/Button';
import Pincode from '@components/Pincode';
import Text from '@components/Text';

const ModalPasscode = (props: PropsType): React$Node => {
  const {
    isVisible,
    mainButtonLabel = 'Proceed',
    passcodeError,
    negativeCallback,
    positiveCallback,
    password,
    theme,
    passcode = '',
    setPasscode,
  } = props;

  const getButtonState = (): 'default' | 'disabled' => {
    if (passcode.length === 6) {
      return 'default';
    }
    return 'disabled';
  };

  return (
    <S.MainContainer>
      <Modal
        animationType="none"
        transparent
        visible={isVisible}
        onRequestClose={negativeCallback}>
        <S.CenterContentContainer>
          <S.ContainerBody theme={theme}>
            <S.HeaderContainer>
              <Text mode="body1">Enter your passcode to proceed</Text>
            </S.HeaderContainer>

            <Pincode
              passcode={passcode}
              onTextChange={(text) => setPasscode(text)}
              passcodeError={passcodeError}
              password={password}
            />

            <S.ButtonsContainer>
              <Button
                type="primary"
                state={getButtonState()}
                title={mainButtonLabel}
                onPress={() => positiveCallback(passcode)}
              />

              <S.NegativeButtonContainer onPress={negativeCallback}>
                <Text mode="subtitle">Cancel</Text>
              </S.NegativeButtonContainer>
            </S.ButtonsContainer>
          </S.ContainerBody>
        </S.CenterContentContainer>
      </Modal>
    </S.MainContainer>
  );
};

export default ModalPasscode;
