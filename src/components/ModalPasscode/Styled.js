/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
`;

export const CenterContentContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1;
  background-color: 'rgba(0, 0, 0, 0.8)';
`;

export const ContainerBody: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(40)}px;
  padding-bottom: ${verticalScale(32)}px;
  padding-left: ${verticalScale(16)}px;
  padding-right: ${verticalScale(16)}px;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  border-radius: 16px;
`;

export const HeaderContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(40)}px;
  align-items: center;
`;

export const ButtonsContainer: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(32)}px;
`;

export const NegativeButtonContainer: ComponentType<any> = styled.TouchableOpacity`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;
