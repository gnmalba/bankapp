## ModalPasscode
ModalPasscode component.

# Usage
```js
import ModalPasscode from '@components/ModalPasscode';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
