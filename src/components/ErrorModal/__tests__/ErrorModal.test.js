import React from 'react';
import {render} from '@testing-library/react-native';
import ErrorModal from '../ErrorModal';
import {testProps} from '@utils/componentCustomProps';

describe('ErrorModal', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<ErrorModal {...testProps('dividerTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
