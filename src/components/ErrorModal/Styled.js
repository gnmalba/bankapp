/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';
import Text from '@components/Text';
import {Image} from 'react-native';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
`;

export const CenterContentContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
  padding-right: 16px;
  padding-left: 16px;
  flex: 1;
  background-color: 'rgba(0, 0, 0, 0.8)';
`;

export const ContainerBody: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(32)};
  padding-bottom: ${verticalScale(32)};
  padding-left: ${verticalScale(24)};
  padding-right: ${verticalScale(24)};
  min-width: 280px
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  border-radius: 16;
`;

export const ImageContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const ContentContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(16)};
  margin-bottom: ${verticalScale(16)};
`;

export const StyledText: ComponentType<any> = styled(Text)`
  text-align: center;
`;

export const StyledImage: ComponentType<any> = styled(Image)``;
