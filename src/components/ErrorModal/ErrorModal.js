/**
 * @format
 * @flow
 */
// React modules
import React from 'react';
import {Modal} from 'react-native';

import type {PropsType} from './types';
import * as S from './Styled';

// Components
import Button from '@components/Button';

const ErrorModal = (props: PropsType): React$Node => {
  const {
    theme,
    isVisible,
    buttonTitle = 'Go back',
    onPressButton,
    errorText = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  } = props;

  const icon = require('@assets/icons/warning.png');
  return (
    <S.MainContainer>
      <Modal
        animationType="none"
        transparent
        visible={isVisible}
        onRequestClose={onPressButton}>
        <S.CenterContentContainer>
          <S.ContainerBody theme={theme}>
            <S.ImageContainer>
              <S.StyledImage source={icon} />
            </S.ImageContainer>

            <S.ContentContainer>
              <S.StyledText mode="body1">{errorText}</S.StyledText>
            </S.ContentContainer>

            <Button
              type="primary"
              state="default"
              title={buttonTitle}
              onPress={onPressButton}
            />
          </S.ContainerBody>
        </S.CenterContentContainer>
      </Modal>
    </S.MainContainer>
  );
};

export default ErrorModal;
