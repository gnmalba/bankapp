/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  buttonTitle: string,
  onPressButton: () => void,
  errorText: string,
  isVisible: boolean,
};
