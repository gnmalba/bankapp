import React from 'react';
import {render} from '@testing-library/react-native';
import KeyboardAwareView from '../KeyboardAwareView';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('KeyboardAwareView', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <KeyboardAwareView />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
