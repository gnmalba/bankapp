/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {} from './Styled';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Platform} from 'react-native';

const KeyboardAwareView = (props: PropsType): React$Node => {
  return (
    <KeyboardAwareScrollView
      extraHeight={Platform.select({
        ios: 170,
        android: 100,
      })}
      keyboardShouldPersistTaps="always"
      enableOnAndroid>
      {props.children}
    </KeyboardAwareScrollView>
  );
};

export default KeyboardAwareView;
