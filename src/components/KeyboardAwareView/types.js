/**
 * @flow
 */

export type PropsType = {
  children: React$Node,
};
