## KeyboardAwareView
KeyboardAwareView component.

# Usage
```js
import KeyboardAwareView from '@components/KeyboardAwareView';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
