import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import PasscodeModal from '../PasscodeModal';
import {testProps} from '@utils/componentCustomProps';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('PasscodeModal', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <PasscodeModal
          {...testProps('dividerTestId')}
          positiveCallback={() => {}}
        />
      </ThemeProvider>,
    );
    const btnEl = all.getByTestId('buttonTestId');
    fireEvent(btnEl, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should behave as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <PasscodeModal
          {...testProps('dividerTestId')}
          positiveCallback={() => {}}
        />
      </ThemeProvider>,
    );

    const pincodeComp = all.getByTestId('smoothInputTestId');
    fireEvent(pincodeComp, 'onChangeText', '111111');

    const button = all.getByTestId('buttonTestId');
    fireEvent(button, 'onPress');
  });
});
