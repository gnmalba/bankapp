## PasscodeModal
PasscodeModal component.

# Usage
```js
import PasscodeModal from '@components/PasscodeModal';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
