/**
 * @format
 * @flow
 */
// React modules
import React, {useState} from 'react';
import {Modal} from 'react-native';

import type {PropsType} from './types';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';

// Components
import Button from '@components/Button';
import Pincode from '@components/Pincode';
import Text from '@components/Text';

const PasscodeModal = (props: PropsType): React$Node => {
  const {
    isVisible,
    mainButtonLabel = 'Proceed',
    passcodeError,
    negativeCallback,
    positiveCallback,
    password,
    theme,
  } = props;

  const [passcode, setPassode] = useState('');

  const getButtonState = (): 'default' | 'disabled' => {
    if (passcode.length === 6) {
      return 'default';
    }
    return 'disabled';
  };

  return (
    <S.MainContainer>
      <Modal
        animationType="none"
        transparent
        visible={isVisible}
        onRequestClose={negativeCallback}>
        <S.CenterContentContainer>
          <S.ContainerBody theme={theme}>
            <S.HeaderContainer>
              <Text mode="body1">Enter your passcode to proceed</Text>
            </S.HeaderContainer>

            <Pincode
              passcode={passcode}
              onTextChange={(text) => setPassode(text)}
              passcodeError={passcodeError}
              password={password}
            />

            <S.ButtonsContainer>
              <Button
                type="primary"
                state={getButtonState()}
                title={mainButtonLabel}
                onPress={() => positiveCallback(passcode)}
              />

              <S.NegativeButtonContainer
                {...testProps('negativeButton')}
                onPress={negativeCallback}>
                <Text mode="subtitle">Cancel</Text>
              </S.NegativeButtonContainer>
            </S.ButtonsContainer>
          </S.ContainerBody>
        </S.CenterContentContainer>
      </Modal>
    </S.MainContainer>
  );
};

export default PasscodeModal;
