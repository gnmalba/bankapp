/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  mainButtonLabel: string,
  passcodeError: string,
  positiveCallback: (passcode: string) => void,
  negativeCallback: () => void,
  isVisible: boolean,
  password?: boolean,
};
