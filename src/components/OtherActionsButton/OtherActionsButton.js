/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';

const OtherActionsButton = (props: PropsType): React$Node => {
  const {icon, title, testID = ''} = props;

  return (
    <S.Container>
      <S.ContentContainer>
        <S.Icon source={icon} {...testProps(testID + 'Icon')} />
        <S.Title {...testProps(testID)}>{title}</S.Title>
      </S.ContentContainer>
    </S.Container>
  );
};

export default OtherActionsButton;
