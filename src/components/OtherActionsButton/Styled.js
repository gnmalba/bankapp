/**
 * @flow
 */

import Text from '@components/Text';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View.attrs(() => ({
  shadowColor: '#000',
  shadowOffset: {
    width: 4,
    height: 4,
  },
  shadowOpacity: 0.32,
  shadowRadius: 4,
  elevation: 4,
}))`
  width: ${scale(98)}px;
  height: ${scale(98)}px;
  background-color: ${(props) =>
    props.theme.otherActions.contentContainerBackground};
  border-radius: 8px;
  padding-top: ${verticalScale(15)}px;
  padding-bottom: ${verticalScale(12)}px;
`;

export const ContentContainer: ComponentType<any> = styled.View`
  height: 100%;
  justify-content: space-around;
  align-items: center;
  background-color: ${(props) =>
    props.theme.otherActions.contentContainerBackground};
`;

export const Icon: ComponentType<any> = styled.Image.attrs((props) => {
  return {
    tintColor: props.theme.mainTopMenuItems.color,
  };
})`
  width: ${scale(32)}px;
  height: ${scale(32)}px;
`;

export const Title: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
}))`
  font-weight: 600;
  font-family: 'MyriadPro-Semibold';
  text-align: center;
  color: ${(props) => props.theme.mainTopMenuItems.color};
`;
