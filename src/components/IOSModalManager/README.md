## IosModalManager
IosModalManager component.

# Usage
```js
import IosModalManager from '@components/IosModalManager';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
