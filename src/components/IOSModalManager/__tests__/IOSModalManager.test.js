import React from 'react';
import {render, cleanup} from '@testing-library/react-native';
import IosModalManager from '../IosModalManager';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import {Text} from 'react-native';
describe('IosModalManager', () => {
  const MockComponent = (props) => <Text {...props}>qweqwe</Text>;
  const MockHOC = IosModalManager(MockComponent);

  afterEach(cleanup);
  it('Should work as expected when visible', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <MockHOC isVisible />
      </ThemeProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should work as expected when not visible', () => {
    render(
      <ThemeProvider theme={DARK_THEME}>
        <MockHOC isVisible={false} />
      </ThemeProvider>,
    );
  });
});
