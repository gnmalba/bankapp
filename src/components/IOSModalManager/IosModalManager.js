/* eslint-disable react-hooks/rules-of-hooks */

import React, {useState, useEffect} from 'react';

const IosModalManager = (Component) => (props) => {
  const {isVisible} = props;
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    let timeout;
    if (isVisible) {
      // NOTE: Delay is needed for iOS modal issues
      timeout = setTimeout(() => {
        setIsModalVisible(true);
      }, 300);
    } else {
      setIsModalVisible(false);
    }

    return () => {
      clearTimeout(timeout);
    };
  }, [isVisible]);
  return <Component {...props} isVisible={isModalVisible} />;
};
export default IosModalManager;
