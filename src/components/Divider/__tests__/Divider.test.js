import React from 'react';
import {render} from '@testing-library/react-native';
import Divider from '../Divider';
import {testProps} from '@utils/componentCustomProps';

describe('Divider', () => {
  it.each([true, false])('Should work as expected', (value) => {
    // Edit the args inside render and provide props.
    const all = render(
      <Divider
        {...testProps('dividerTestId')}
        marginVertical={value}
        marginHorizontal={value}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
