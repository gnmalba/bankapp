/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const StyledDivider: ComponentType<any> = styled.View`
  height: 1.5px;
  background-color: ${(props) => props.theme.color.TEXT_300_LOW}
  margin-top: ${(props) =>
    props.marginVertical ? verticalScale(props.marginVertical) : 0}px;
  margin-bottom: ${(props) =>
    props.marginVertical ? verticalScale(props.marginVertical) : 0}px;
  margin-left: ${(props) =>
    props.marginHorizontal ? verticalScale(props.marginHorizontal) : 0}px;
  margin-right: ${(props) =>
    props.marginHorizontal ? verticalScale(props.marginHorizontal) : 0}px;
`;
