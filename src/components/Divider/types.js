/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  marginHorizontal?: number,
  marginVertical?: number,
  theme?: Object,
};
