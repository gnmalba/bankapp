/**
 * @format
 * @flow
 */

import React from 'react';
import type {PropsType} from './types';
import * as S from './Styled';

const Divider = (props: PropsType): React$Node => {
  const {
    marginHorizontal,
    marginVertical,
    theme,
    testID,
    accessibilityLabel,
  } = props;
  return (
    <S.StyledDivider
      theme={theme}
      marginHorizontal={marginHorizontal}
      marginVertical={marginVertical}
      testID={testID}
      accessibilityLabel={accessibilityLabel}
    />
  );
};

export default Divider;
