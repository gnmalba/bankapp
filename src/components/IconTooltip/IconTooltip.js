/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {Image} from 'react-native';

// Utils
import type {PropsType} from './types';
import {} from './Styled';
import {scale, verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

const IconTooltip = (props: PropsType): React$Node => {
  const {height = 20, width = 20, style, source, color, testID = ''} = props;
  return (
    <Image
      {...testProps(testID)}
      style={[
        {
          height: verticalScale(height),
          width: scale(width),
          tintColor: color,
        },
        style,
      ]}
      source={source}
      resizeMode="contain"
    />
  );
};

export default IconTooltip;
