import React from 'react';
import {render} from '@testing-library/react-native';
import IconTooltip from '../IconTooltip';

describe('IconTooltip', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<IconTooltip />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
