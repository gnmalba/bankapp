/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  height?: number,
  width?: number,
  style?: Object,
  source: any,
  color?: string,
};
