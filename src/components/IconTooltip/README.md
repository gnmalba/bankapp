## IconTooltip
IconTooltip component.

# Usage
```js
import IconTooltip from '@components/IconTooltip';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
