/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  data: any[],
  placeholder: string,
  onSelect?: () => void,
  setter?: (any) => void,
  selected?: Object,
  isOpen: boolean,
  openSetter: Function,
  label: string,
};
