import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import SelectInput from '../SelectInput';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

const generateTestOptions = (n) => {
  return [...Array(n).keys()].map((value, index) => {
    return {
      id: index,
      value: `value#${index + 1}`,
    };
  });
};

describe('SelectInput', () => {
  const mockAppState = {};

  const testOptions = generateTestOptions(10);

  const defaultProps = {
    ...mockAppState,
    data: testOptions,
    placeholder: 'TestPlaceholder',
    onSelect: jest.fn(() => {}),
    setter: jest.fn(() => {}),
    selected: undefined,
    isOpen: false,
    openSetter: jest.fn(() => {}),
    label: 'TestLabel',
  };

  it('Should work as expected', () => {
    const props = {
      ...defaultProps,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <SelectInput {...testProps('selectInputTestId')} {...props} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('ListItem onPress should work as expected', () => {
    const props = {
      ...defaultProps,
      isOpen: true,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <SelectInput {...testProps('selectInputTestId')} {...props} />
        </ThemeProvider>
      </MockProvider>,
    );
    const el = all.getByTestId('DropdownBankListItem0');
    fireEvent(el, 'onPress');

    expect(all.toJSON()).toMatchSnapshot();
  });

  it('SelectBankInput onPress should work as expected', () => {
    const props = {
      ...defaultProps,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <SelectInput {...testProps('selectInputTestId')} {...props} />
        </ThemeProvider>
      </MockProvider>,
    );
    const el = all.getByTestId('SelectBankInput');
    fireEvent(el, 'onPress');

    expect(all.toJSON()).toMatchSnapshot();
  });

  it.each([{isOpen: false}, {isOpen: true}])(
    'should render correctly when %o',
    (additionalProps) => {
      const props = {
        ...defaultProps,
        ...additionalProps,
      };

      const all = render(
        <MockProvider store={mockAppState}>
          <ThemeProvider theme={DARK_THEME}>
            <SelectInput {...testProps('selectInputTestId')} {...props} />
          </ThemeProvider>
        </MockProvider>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );

  it.each([{selected: undefined}, {selected: testOptions[0]}])(
    'should render correctly when %o',
    (additionalProps) => {
      const propss = {
        ...defaultProps,
        ...additionalProps,
      };

      const all = render(
        <MockProvider store={mockAppState}>
          <ThemeProvider theme={DARK_THEME}>
            <SelectInput {...testProps('selectInputTestId')} {...propss} />
          </ThemeProvider>
        </MockProvider>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
