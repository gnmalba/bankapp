/**
 * @format
 * @flow
 */

import React, {useState, useRef, useEffect} from 'react';
import type {PropsType} from './types';
import {
  Container,
  DropdownContainer,
  DropdownIcon,
  DropdownIconHolder,
  DropdownListItem,
  DropdownListItemText,
  InputDivider,
  LabelContainer,
  PlaceholderText,
  SelectionContainer,
  HistoryFlatList,
  FlatListContainer,
} from './Styled';
import Text from '@components/Text';
import {TouchableWithoutFeedback, View, TouchableOpacity} from 'react-native';
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';
const SelectInput = (props: PropsType): React$Node => {
  const {
    data,
    placeholder,
    onSelect,
    setter,
    selected,
    isOpen,
    openSetter,
    label,
  } = props;
  const [selectedValue: Object, setSelectedValue] = useState(selected);
  const handlePress = () => {
    openSetter(!isOpen);
  };

  const [indexSelected, setIndexSelected] = useState(0);
  const flatListRef: Object = useRef();

  useEffect(() => {
    const index = indexSelected;

    // autoscroll feature
    if (isOpen) {
      flatListRef.current.scrollToIndex({
        index,
        animation: false,
      });
    }
  }, [isOpen, indexSelected]);

  const handleSelect = (item, index) => {
    setSelectedValue(item);
    openSetter(false);

    if (onSelect) {
      onSelect();
    }

    if (setter) {
      setter(item);
    }
    setIndexSelected(index);
  };

  const isSelected = (currentKey) => {
    if (!selectedValue) {
      return false;
    }

    // todo
    return currentKey === selectedValue.id;
  };

  const renderDisplay = () => {
    if (selected) {
      return (
        <Text
          mode="body1"
          numberOfLines={1}
          {...testProps('selectInputBankLabelId')}>
          {selected.value}
        </Text>
      ); // todo
    }

    if (selectedValue) {
      return (
        <Text
          mode="body1"
          numberOfLines={1}
          {...testProps('selectInputBankLabelId')}>
          {selectedValue.value}
        </Text>
      ); // todo
    }
    return (
      <PlaceholderText mode="body1" {...testProps('selectInputBankLabelId')}>
        {placeholder}
      </PlaceholderText>
    );
  };

  const _renderItem = ({item, index}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        key={item.index}
        onPress={() => handleSelect(item, index)}
        {...testProps('DropdownBankListItem' + index)}>
        <DropdownListItem key={item.index} isSelected={isSelected(item.id)}>
          <DropdownListItemText
            numberOfLines={1}
            {...testProps('DropdownBankList' + index)}>
            {item.value}
          </DropdownListItemText>
        </DropdownListItem>
      </TouchableOpacity>
    );
  };

  const _getItemLayout = (_, index) => {
    return {
      length: verticalScale(47),
      offset: verticalScale(47) * index,
      index,
      indexSelected,
    };
  };

  const DropdownList = () => {
    return (
      <View>
        <FlatListContainer>
          <HistoryFlatList
            keyExtractor={(item) => item.Code}
            ref={flatListRef}
            data={data}
            renderItem={_renderItem}
            bounces={false}
            getItemLayout={_getItemLayout}
            showsVerticalScrollIndicator={false}
          />
        </FlatListContainer>
      </View>
    );
  };

  return (
    <>
      <Container>
        <LabelContainer>
          <Text mode="body2" {...testProps('SelectBankLabel')}>
            {label}
          </Text>
        </LabelContainer>
        <TouchableWithoutFeedback
          onPress={handlePress}
          {...testProps('SelectBankInput')}>
          <View>
            <DropdownContainer
              isOpen={isOpen}
              {...testProps('SelectBankDropdown')}>
              <SelectionContainer {...testProps('SelectBankField')}>
                {renderDisplay()}
              </SelectionContainer>
              <InputDivider {...testProps('SelectBankDivider')} />
              <DropdownIconHolder>
                <DropdownIcon
                  isOpen={isOpen}
                  {...testProps('SelectBankIcon')}
                />
              </DropdownIconHolder>
            </DropdownContainer>
          </View>
        </TouchableWithoutFeedback>
      </Container>
      {isOpen && <DropdownList />}
    </>
  );
};

export default SelectInput;
