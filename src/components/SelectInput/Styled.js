/**
 * @flow
 */

import {moderateScale, verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Text from '@components/Text';

import {FlatList as RNFlatList, Platform} from 'react-native';

export const Container: ComponentType<any> = styled.View`
  margin-bottom: ${moderateScale(25)}px;
  z-index: 10;
`;

export const LabelContainer: ComponentType<any> = styled.View`
  margin-bottom: ${moderateScale(12)}px;
`;

export const InputDivider: ComponentType<any> = styled.View`
  border-right-width: 1;
  border-right-color: ${(props) => props.theme.color.TEXT_300_LOW};
  height: 150%;
  align-self: center;
  margin-right: ${verticalScale(12)}px;
  margin-left: ${verticalScale(24)}px;
`;

export const DropdownContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.textInput.backgroundColor.default};
  border-width: ${(props) => '1px'};
  border-color: ${(props) =>
    props.isOpen
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.textInput?.border.default};
  border-radius: 4px;
  padding: ${verticalScale(12)}px ${verticalScale(16)}px;
  display: flex;
  flex-direction: row;
`;

export const DropdownIconHolder: ComponentType<any> = styled.View`
  margin-left: auto;
`;

export const DropdownIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: props.isOpen ? 'keyboard-arrow-up' : 'keyboard-arrow-down',
    size: moderateScale(18),
    color: props.theme.color.TEXT_300_LOW,
  }),
)``;

export const SelectionContainer: ComponentType<any> = styled.View`
  flex: 1;
`;

export const HistoryFlatList: ComponentType<any> = styled(RNFlatList).attrs(
  (props) => ({
    ...Platform.select({
      ios: {
        alwaysBounceHorizontal: false,
        alwaysBounceVertical: false,
        bounces: false,
      },
      android: {
        overScrollMode: 'never',
      },
    }),
    contentContainerStyle: {
      backgroundColor: 'transparent',
    },
  }),
)`
  width: 100%;
  align-self: center;
`;

export const FlatListContainer: ComponentType<any> = styled.View`
  flex: 1;
  max-height: ${verticalScale(235)}px
  position: absolute;
  top: ${moderateScale(-24)}px;
  left: 0;
  right: 0;
  z-index: 2;
`;

export const DropdownListItem: ComponentType<any> = styled.View`
  background-color: ${(props) =>
    props.isSelected
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.BACKGROUND_300};
  padding: ${verticalScale(12)}px ${verticalScale(16)}px;
  border-width: 0.8px;
  height: ${verticalScale(47)}px;
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
`;

export const PlaceholderText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    customColor: props.theme.textInput.placeholderTextColor,
  }),
)``;

export const DropdownListItemText: ComponentType<any> = styled(Text)`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: 400;
  font-size: ${moderateScale(16)}px;
  line-height: ${moderateScale(21)}px;
`;
