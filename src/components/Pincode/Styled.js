/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  margin-top: ${(props) => verticalScale(10)}px;
`;

export const ErrorContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const TextContainer: ComponentType<any> = styled.View`
  width: 90%;
`;

export const StyledImage: ComponentType<any> = styled.Image`
  width: ${scale(24)}px;
  height: ${scale(24)}px;
`;
