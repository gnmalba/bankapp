// @flow

import React from 'react';
import {Keyboard} from 'react-native';
import {withTheme} from 'styled-components';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import {testProps} from '@utils/componentCustomProps';

import Text from '@components/Text';
import {ErrorContainer, StyledImage, Container, TextContainer} from './Styled';

import {scale, verticalScale} from '@utils/scale';
import type {PasscodeType} from './types';

const Pincode = (props: PasscodeType): React$Element<any> => {
  const {
    passcode,
    onTextChange,
    theme,
    passcodeError,
    testID,
    password,
  } = props;
  const errorIcon = require('@assets/icons/warning.png');
  const cellStyleFocused = {
    borderColor: theme?.color.SECONDARY_DEFAULT,
    borderWidth: 1,
  };

  return (
    <>
      <SmoothPinCodeInput
        {...testProps(testID || 'pincodeInputTestId')}
        password={password}
        mask="*"
        value={passcode}
        onTextChange={(text) => {
          onTextChange(text);
        }}
        cellSize={scale(37)}
        codeLength={6}
        cellStyleFocused={cellStyleFocused}
        cellStyle={{
          ...{
            flex: 1,
            borderRadius: 4,
            width: scale(37),
            height: verticalScale(48),
          },
          backgroundColor: theme?.color.BACKGROUND_300,
          ...(!passcodeError
            ? {}
            : {borderWidth: 1, borderColor: theme?.color.RED_STATUS}),
          ...{borderWidth: 0.5},
        }}
        textStyle={{
          fontSize: scale(18),
          color: theme?.text.textColor,
        }}
        cellSpacing={scale(15)}
        keyboardType="numeric"
        animationFocused={null}
        onFulfill={() => {
          Keyboard.dismiss();
        }}
        restrictToNumbers
      />
      <Container>
        {passcodeError ? (
          <ErrorContainer>
            <TextContainer>
              <Text
                themeColor="RED_STATUS"
                mode="body2"
                {...testProps('passcodeErrorMsgTestId')}>
                {passcodeError}
              </Text>
            </TextContainer>
            <StyledImage
              source={errorIcon}
              {...testProps('passcodeErrorIconTestId')}
            />
          </ErrorContainer>
        ) : (
          <></>
        )}
      </Container>
    </>
  );
};

Pincode.defaultProps = {
  testID: 'smoothInputTestId',
};

export default (withTheme(Pincode): any);
