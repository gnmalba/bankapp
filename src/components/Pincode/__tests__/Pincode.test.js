import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import Pincode from '../Pincode';

describe('Pincode', () => {
  it.each(['testId', undefined, null])('Should work as expected', (testID) => {
    const all = render(
      <Pincode
        testID={testID}
        passcodeError={testID}
        onTextChange={() => {}}
      />,
    );
    all.debug();
    if (testID) {
      const pinEl = all.getByTestId(testID);
      fireEvent(pinEl, 'onTextChange');
    }
    expect(all.toJSON()).toMatchSnapshot();
  });
});
