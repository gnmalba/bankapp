/**
 * @flow
 */

export type PasscodeType = {
  onTextChange: (text: string) => void,
  theme?: Object,
  passcodeError?: boolean,
  passcode?: string,
  testID?: string,
  onCallPress?: () => {},
  password?: boolean,
  label?: string,
};
