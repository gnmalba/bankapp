// @flow

import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import Icon from '@components/Icon';
import styled from 'styled-components/native';
import {useDispatch} from 'react-redux';
import {moderateScale} from '@utils/scale';

import {testProps} from '@utils/componentCustomProps';
import {AUTHENTICATION_UPDATE_STATE} from '@ducks/authentication/actionTypes';

type PropsType = {
  testID?: string,
};

const StyledIcon = styled(Icon).attrs((props) => ({
  name: 'logout',
  color: props.theme.mainTopMenuItems.color,
  size: moderateScale(28),
}))``;

const LogoutIcon = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const {testID} = props;

  return (
    <TouchableWithoutFeedback
      {...testProps(testID || 'logoutIconTestId')}
      onPress={() => {
        dispatch({
          type: AUTHENTICATION_UPDATE_STATE,
          newState: {showLogoutModal: true},
        });
      }}>
      <StyledIcon />
    </TouchableWithoutFeedback>
  );
};

export default LogoutIcon;
