import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import DashboardHeader from '@components/DashboardHeader';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('DashboardHeader', () => {
  const store = {
    authentication: {
      showLogoutModal: false,
    },
    dashboard: {
      customizeWidgetShown: false,
    },
  };
  it.each(['dashboardHeaderTestId', ''])('Should work properly', (testId) => {
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <DashboardHeader {...testProps(testId)} />
        </ThemeProvider>
      </MockProvider>,
    );
    all.debug();
    if (testId) {
      const headerEl = all.getByTestId(testId);
      fireEvent(headerEl, 'leftComponent');
      fireEvent(headerEl, 'centerComponent');
    }
    expect(all.toJSON()).toMatchSnapshot();
  });
});
