import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import LogoutIcon from '@components/DashboardHeader/components/LogoutIcon';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

describe('DashboardHeader', () => {
  it.each(['logoutIconTestId', ''])('Should work properly', (testId) => {
    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <LogoutIcon {...testProps(testId)} />
        </ThemeProvider>
      </MockProvider>,
    );
    if (testId) {
      const el = all.getByTestId(testId);
      fireEvent(el, 'onPress');
    }
    expect(all.toJSON()).toMatchSnapshot();
  });
});
