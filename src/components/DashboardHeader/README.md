## DashboardHeader

Card component.

# Usage

```js
import DashboardHeader from '@components/DashboardHeader';

# Props
```

| Prop       | Type    | Required | Description           |
| ---------- | ------- | -------- | --------------------- |
| SampleProp | boolean | false    | This is only a sample |
