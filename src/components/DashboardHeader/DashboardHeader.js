/**
 * @flow
 */
import React from 'react';
import {useSelector} from 'react-redux';

import {TouchableWithoutFeedback} from 'react-native';
import {Header as RNEHeader} from 'react-native-elements';
import styled from 'styled-components/native';
import LogoutModal from '@screens/private/Dashboard/components/LogoutModal';
import EnrollBillerModal from '@screens/private/Dashboard/components/EnrollBillerModal';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {testProps} from '@utils/componentCustomProps';
import {moderateScale} from '@utils/scale';

import LogoutButton from './components/LogoutIcon';
type HeaderPropTypes = {
  onDrawerClick: () => void,
  testID?: string,
};

const Logo = styled.Image.attrs((props) => ({
  source: props.theme.logoSrc,
  resizeMode: 'contain',
}))`
  height: ${moderateScale(30)}px;
  width: ${moderateScale(125)}px;
`;

const StyledHeader = styled(RNEHeader).attrs((props) => ({
  barStyle: props.theme.topAppBar.barStyle,
  containerStyle: {
    borderBottomColor: 'transparent',
  },
  backgroundColor: props.theme.topAppBar.backgroundColor,
  opacity: props.shouldHeaderDimmed ? 0.5 : 1,
}))``;

const ThemedIcon = styled(Ionicons).attrs((props) => ({
  color: props.theme.mainTopMenuItems.color,
}))``;

const RightComponentContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const VerticalSpaceDivider = styled.View`
  width: 10px;
`;

const DashboardHeader = (props: HeaderPropTypes): React$Node => {
  const {onDrawerClick, testID} = props;
  const {showLogoutModal} = useSelector((state) => state.authentication);
  const {customizeWidgetShown, isShowEnrollBillerPrompt} = useSelector(
    (state) => state.dashboard,
  );

  return (
    <>
      <LogoutModal visible={showLogoutModal} />
      <EnrollBillerModal visible={isShowEnrollBillerPrompt} />
      <StyledHeader
        {...testProps(testID || 'styleHeaderTestId')}
        shouldHeaderDimmed={customizeWidgetShown}
        leftComponent={() => (
          <TouchableWithoutFeedback
            onPress={onDrawerClick}
            {...testProps('handleOpenDrawerTestID')}>
            <ThemedIcon
              name="menu-outline"
              size={moderateScale(30)}
              {...testProps('dashboardHeaderMenuIconId')}
            />
          </TouchableWithoutFeedback>
        )}
        centerComponent={() => (
          <Logo {...testProps('dashboardHeaderMenuLogoId')} />
        )}
        rightComponent={
          <RightComponentContainer>
            <TouchableWithoutFeedback>
              <ThemedIcon
                name="notifications"
                size={moderateScale(25)}
                {...testProps('dashboardHeaderMenuBellId')}
              />
            </TouchableWithoutFeedback>
            <VerticalSpaceDivider />
            <LogoutButton {...testProps('handleLogoutTestID')} />
          </RightComponentContainer>
        }
      />
    </>
  );
};

export default DashboardHeader;
