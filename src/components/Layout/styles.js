/**
 * @flow
 */

import {StyleSheet} from 'react-native';

type StylesType = {
  bgImage: Object,
  bg: Object,
  padded: Object,
};

const Styles: StylesType = StyleSheet.create({
  bgImage: {
    resizeMode: 'cover',
  },
  bg: {
    flex: 1,
  },
  padded: {
    padding: 10,
  },
});

export default Styles;
