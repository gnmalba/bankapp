## Layout

Layout component.

# Usage

```js
import Layout from '@components/Layout';

# Props
```

| Prop     | Type       | Required | Description |
| -------- | ---------- | -------- | ----------- |
| padded   | boolean    | false    |
| plain    | boolean    | false    |
| children | React Node | false    |
