/**
 * @flow
 */

import {type Node} from 'react';

export type PropsType = {
  padded?: boolean,
  plain?: boolean,
  children?: Node,
  theme?: Object,
  padding?: number,
};
