/**
 * @format
 * @flow
 */

import React from 'react';
import type {PropsType} from './types';
import {StyledLayoutImage, StyledPlainLayout} from './Styled';

const Layout = (props: PropsType): React$Node => {
  const {plain, children, theme, padded, padding = 0} = props;

  if (!plain) {
    return (
      <StyledLayoutImage padded={padded} padding={padding} theme={theme}>
        {children}
      </StyledLayoutImage>
    );
  }
  return <StyledPlainLayout>{children}</StyledPlainLayout>;
};

export default Layout;
