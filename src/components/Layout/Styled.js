/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';

export const StyledLayoutImage: ComponentType<any> = styled.ImageBackground.attrs(
  (props) => ({
    source: props.theme?.layout?.src,
  }),
)`
  flex: 1;
  resize-mode: cover;
  padding: ${(props) => (props.padded ? '16' : props.padding)}px;
`;
export const StyledPlainLayout: ComponentType<any> = styled.View`
  flex: 1;
`;
