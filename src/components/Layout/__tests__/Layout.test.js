import React from 'react';
import {render} from '@testing-library/react-native';
import Layout from '../Layout';
import {DARK_THEME} from '@utils/constants';

describe('Layout', () => {
  it.each([true, false])('Should work as expected %s', (bools) => {
    const all = render(
      <Layout plain={bools} padded={bools} theme={DARK_THEME} />,
    );
    render(<Layout plain={bools} padded={!bools} theme={DARK_THEME} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
