/**
 * @format
 * @flow
 */

import React from 'react';

import type {BasicTextInputType} from '@utils/commonTypes';
import {StyledInput} from './Styled';

const BasicTextInput = (props: BasicTextInputType): React$Node => {
  const {nativeProps, ...rest} = props;
  return (
    <>
      <StyledInput {...rest} {...nativeProps} />
    </>
  );
};

export default BasicTextInput;
