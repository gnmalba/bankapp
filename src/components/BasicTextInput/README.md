## BasicTextInput

BasicTextInput component.

# Usage

```js
import BasicTextInput from '@components/BasicTextInput';

# Props
```

| Prop       | Type    | Required | Description           |
| ---------- | ------- | -------- | --------------------- |
| SampleProp | boolean | false    | This is only a sample |
