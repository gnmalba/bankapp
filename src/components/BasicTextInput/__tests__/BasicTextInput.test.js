import React from 'react';
import {render} from '@testing-library/react-native';
import BasicTextInput from '../BasicTextInput';
import {DARK_THEME} from '@utils/constants';

describe('BasicTextInput', () => {
  it('Should work as expected', () => {
    const all = render(<BasicTextInput theme={DARK_THEME} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
