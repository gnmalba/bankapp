/**
 * @flow
 */

// Uncomment this line to start using styled components.
import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {moderateScale} from '@utils/scale';

export const StyledInput: ComponentType<any> = styled.TextInput.attrs(
  (props) => ({
    placeholderTextColor: props.theme?.textInput?.placeholderTextColor,
    editable: props.state !== 'disabled',
    selectionColor: props.theme?.color?.TEXT_100_HIGH,
  }),
)`
  height: ${moderateScale(48)}px;
  border-radius: ${moderateScale(4)}px;
  font-size: ${moderateScale(18)}px;
  font-family: Myriad Pro;
  line-height: ${moderateScale(20)}px;
  font-style: normal;
  font-weight: normal;
  padding: ${moderateScale(14)}px ${moderateScale(35)}px ${moderateScale(14)}px
    ${moderateScale(16)}px;
  align-items: center;
  background-color: ${(props) => {
    const {state, theme} = props;
    return theme.textInput?.backgroundColor[state || 'default'];
  }};
  color: ${(props) => props.theme.textInput?.textColor};
  border: ${(props) => {
    const {state, theme} = props;
    return theme.textInput?.border[state || 'default'];
  }};
`;
