import {formatDate} from '@utils/date';

const FIELD_NAMES = {
  startDate: 'START_DATE',
  endDate: 'END_DATE',
};

const checkCurrentDateIsLesser = (currentDate, date) => {
  // current date is lesser
  return formatDate(currentDate) < formatDate(date);
};

const checkCurrentDateIsEqual = (currentDate, date) => {
  // dates are equal
  return currentDate && formatDate(currentDate) === formatDate(date);
};

const checkCurrentDateIsGreater = (currentDate, date) => {
  // current date is greater
  return formatDate(currentDate) > formatDate(date);
};

const selectedStartDateIsValid = (date, type) =>
  date && type === FIELD_NAMES.startDate;

const selectedEndDateIsValid = (date, type) =>
  date && type === FIELD_NAMES.endDate;

const endDateActiveAndHasDateRange = (dateRange, activeDateField) => {
  const {selectedStartDate, selectedEndDate} = dateRange;
  return (
    selectedStartDate &&
    selectedEndDate &&
    activeDateField === FIELD_NAMES.endDate
  );
};

const shouldUpdateEndDate = (dateRange, date, activeDateField) => {
  const {selectedStartDate} = dateRange;
  return (
    endDateActiveAndHasDateRange(dateRange, activeDateField) &&
    (checkCurrentDateIsEqual(selectedStartDate, date) ||
      checkCurrentDateIsLesser(selectedStartDate, date))
  );
};

const shouldUpdateStartDate = (dateRange, date, activeDateField) => {
  const {selectedStartDate, selectedEndDate} = dateRange;
  return (
    selectedStartDate &&
    selectedEndDate &&
    checkCurrentDateIsGreater(selectedStartDate, date)
  );
};

const hasDateRangeAndEndDateLesser = (dateRange, date) => {
  const {selectedStartDate, selectedEndDate} = dateRange;

  return (
    selectedStartDate &&
    selectedEndDate &&
    checkCurrentDateIsLesser(selectedEndDate, date)
  );
};

export const handleDateChange = ({
  date,
  type,
  setDateRange,
  setActiveDateField,
  dateRange,
  activeDateField,
  customOnChange = () => {},
}) => {
  const updatedEndDate = {
    ...dateRange,
    selectedEndDate: date,
  };

  const updatedStartDate = {
    ...dateRange,
    selectedStartDate: date,
  };

  if (selectedStartDateIsValid(date, type)) {
    if (shouldUpdateEndDate(dateRange, date, activeDateField)) {
      setDateRange(updatedEndDate);
      customOnChange(updatedEndDate);
      setActiveDateField(FIELD_NAMES.endDate);
      return;
    }
    if (shouldUpdateStartDate(dateRange, date, activeDateField)) {
      setDateRange(updatedStartDate);
      customOnChange(updatedStartDate);
      setActiveDateField(FIELD_NAMES.startDate);
      return;
    }
    if (hasDateRangeAndEndDateLesser(dateRange, date)) {
      setDateRange(updatedEndDate);
      customOnChange(updatedEndDate);
      setActiveDateField(FIELD_NAMES.endDate);
      return;
    }
    setDateRange(updatedStartDate);
    customOnChange(updatedStartDate);
    setActiveDateField(FIELD_NAMES.startDate);
    return;
  }
  if (selectedEndDateIsValid(date, type)) {
    setDateRange(updatedEndDate);
    customOnChange(updatedEndDate);
    setActiveDateField(FIELD_NAMES.endDate);
  }
};
