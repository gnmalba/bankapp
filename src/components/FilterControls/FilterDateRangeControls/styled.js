/**
 * @format
 * @flow
 */

import styled from 'styled-components/native';

import type {ComponentType} from 'react';

// UTILS
import {verticalScale} from '@utils/scale';

// STYLED

export const CalendarContainer: ComponentType<any> = styled.View`
  margin: 1px;
`;

export const DateRangeControlContainer: ComponentType<any> = styled.View`
  justify-content: space-between;
  flex-direction: row;
  margin-bottom: ${verticalScale(16)}px;
`;
