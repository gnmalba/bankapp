/**
 * @format
 * @flow
 */

import React from 'react';
import moment from 'moment';

// COMPONENTS
import Calendar from '@components/Calendar';
import Button from '@components/Button';
import {CalendarContainer, DateRangeControlContainer} from './styled';

import FilterDateButton from '../FilterDateButton';

// UTILS
import {verticalScale} from '@utils/scale';
import type {FilterDateRangeControlsProps} from '../types';
import {handleDateChange} from './utils';

const DEFAULT_DATE_RANGE = {
  selectedStartDate: null,
  selectedEndDate: null,
};

const FIELD_NAMES = {
  startDate: 'START_DATE',
  endDate: 'END_DATE',
};

const handleCancel = ({
  startDate,
  endDate,
  setDateRange,
  setActiveDateField,
  onPressCancel,
}) => {
  setDateRange({
    ...DEFAULT_DATE_RANGE,
    selectedStartDate: startDate,
    selectedEndDate: endDate,
  });
  setActiveDateField(null);
  onPressCancel && onPressCancel();
};

// RENDER HELPER [calendar component]
const renderCalendar = (props) => {
  const {
    activeDateField,
    calendarTipRef,
    showCalendar,
    dateRange,
    setDateRange,
    setActiveDateField,
    customOnChange,
  } = props;

  const hasDateValue = dateRange.selectedStartDate || dateRange.selectedEndDate;

  const additionalCalendarProperties = [
    {
      date: moment(),
      style: {
        backgroundColor: hasDateValue ? 'transparent' : '#09BAD0',
        width: verticalScale(38),
        height: verticalScale(38),
      },
    },
  ];

  if (showCalendar) {
    return (
      <CalendarContainer>
        <Calendar
          calendarProps={{
            ...dateRange,
            minDate: moment().subtract(90, 'days'),
            maxDate: moment(),
            disabledDates: (date) => date.isAfter(moment(), 'day'),
            onDateChange: (date: string, type: string) =>
              handleDateChange({
                date,
                type,
                setDateRange,
                setActiveDateField,
                dateRange,
                activeDateField,
                customOnChange,
              }),
            customDatesStyles: additionalCalendarProperties,
            allowRangeSelection: true,
            forwardedRef: calendarTipRef,
          }}
        />
      </CalendarContainer>
    );
  }

  return <></>;
};

const renderButtons = (props) => {
  const {
    startDate,
    endDate,
    showCalendar,
    dateRange,
    setDateRange,
    setActiveDateField,
    onPressApply,
    onPressCancel,
  } = props;

  const applyIsAllowed =
    (dateRange.selectedStartDate && dateRange.selectedEndDate) ||
    (!dateRange.selectedStartDate && !dateRange.selectedEndDate);

  if (showCalendar) {
    return (
      <>
        <Button
          state={applyIsAllowed ? 'default' : 'disabled'}
          type="primary"
          size="large"
          title="Apply"
          onPress={() => {
            onPressApply(dateRange);
            setActiveDateField('');
          }}
        />
        <Button
          type="primary"
          size="large"
          title="Cancel"
          onPress={() =>
            handleCancel({
              startDate,
              endDate,
              setDateRange,
              setActiveDateField,
              onPressCancel,
            })
          }
        />
      </>
    );
  }

  return <></>;
};

const FilterDateRangeControls = (
  props: FilterDateRangeControlsProps,
): React$Node => {
  const {initialActiveField, onPressDateControl, startDate, endDate} = props;

  const calendarTipRef = React.useRef(null);

  const [dateRange, setDateRange] = React.useState(DEFAULT_DATE_RANGE);
  const [activeDateField, setActiveDateField] = React.useState(
    initialActiveField,
  );

  const showCalendar =
    activeDateField === FIELD_NAMES.startDate ||
    activeDateField === FIELD_NAMES.endDate;

  const isActiveField = (fieldName) => activeDateField === fieldName;

  // set initial date range with provided value props
  React.useEffect(() => {
    setDateRange({
      selectedStartDate: startDate,
      selectedEndDate: endDate,
    });
  }, [startDate, endDate, setDateRange]);

  React.useEffect(() => {
    setActiveDateField(initialActiveField);
  }, [initialActiveField, setActiveDateField]);

  return (
    <>
      <DateRangeControlContainer>
        <FilterDateButton
          label="from"
          isActive={isActiveField(FIELD_NAMES.startDate)}
          dateValue={dateRange.selectedStartDate}
          onPress={() => {
            setActiveDateField(FIELD_NAMES.startDate);
            onPressDateControl(FIELD_NAMES.startDate);
          }}
        />
        <FilterDateButton
          label="to"
          isActive={isActiveField(FIELD_NAMES.endDate)}
          dateValue={dateRange.selectedEndDate}
          onPress={() => {
            setActiveDateField(FIELD_NAMES.endDate);
            onPressDateControl(FIELD_NAMES.endDate);
          }}
        />
      </DateRangeControlContainer>
      {renderCalendar({
        ...props,
        calendarTipRef,
        dateRange,
        showCalendar,
        activeDateField,
        setDateRange,
        setActiveDateField,
      })}
      {renderButtons({
        ...props,
        dateRange,
        showCalendar,
        setActiveDateField,
        setDateRange,
      })}
    </>
  );
};

export default FilterDateRangeControls;
