import type {ComponentType} from 'react';
import {Image} from 'react-native';

import styled from 'styled-components/native';

import Text from '@components/Text';

import {verticalScale} from '@utils/scale';

export const FilterBtn: ComponentType<any> = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  align-content: center;
`;

export const FilterIcon: ComponentType<any> = styled(Image).attrs((props) => ({
  source: props.theme.FilterIcon,
  resizeMode: 'contain',
}))`
  width: ${verticalScale(12)}px;
  height: ${verticalScale(12)}px;
  margin-horizontal: ${verticalScale(10)}px;
`;

export const FilterBtnLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'subtitle',
    themeColor: 'SECONDARY_DEFAULT',
  }),
)`
  margin-right: 8;
`;
