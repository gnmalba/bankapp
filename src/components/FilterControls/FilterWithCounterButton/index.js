/**
 * @format
 * @flow
 */

import React from 'react';

// COMPONENTS
import Avatar from '@components/Avatar';

import {FilterBtn, FilterBtnLabel, FilterIcon} from './styled';

// UTILS
import {isEmpty} from '../utils';
import type {FilterButtonProps} from '../types';

const SIZES = {
  filterCountSize: 18,
  filterCountLabel: {fontSize: 12},
};

const FilterCount = ({filterCount}): React$Node => {
  if (isEmpty(filterCount)) {
    return <></>;
  }
  return (
    <Avatar
      size={SIZES.filterCountSize}
      title={filterCount.toString()}
      titleStyle={SIZES.filterCountLabel}
    />
  );
};

// MAIN
const FilterWithCounterButton = (props: FilterButtonProps): React$Node => {
  const {filterCount, onPress} = props;
  return (
    <FilterBtn onPress={onPress}>
      <FilterIcon />
      <FilterBtnLabel>Filter</FilterBtnLabel>
      <FilterCount filterCount={filterCount} />
    </FilterBtn>
  );
};

export default FilterWithCounterButton;
