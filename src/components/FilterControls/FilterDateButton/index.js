/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity} from 'react-native';

// COMPONENTS
import {DateButtonContainer, DateButtonLabel, DateControlLabel} from './styled';
import {ButtonDivider, CalendarIcon} from '../Styled';

// UTILS

import {formatDate} from '@utils/date';

import type {FilterDateButtonProps} from '../types';

const DATE_FORMAT_FOR_DISPLAY = 'MM/DD/YYYY';

// MAIN

const FilterDateButton = (props: FilterDateButtonProps): React$Node => {
  const {dateValue, label, onPress} = props;

  const dateButtonLabel = dateValue
    ? formatDate(dateValue, DATE_FORMAT_FOR_DISPLAY)
    : DATE_FORMAT_FOR_DISPLAY;

  return (
    <TouchableOpacity onPress={onPress}>
      <DateControlLabel>{label}</DateControlLabel>
      <DateButtonContainer {...props}>
        <CalendarIcon />
        <ButtonDivider />
        <DateButtonLabel {...props}>{dateButtonLabel}</DateButtonLabel>
      </DateButtonContainer>
    </TouchableOpacity>
  );
};

export default FilterDateButton;
