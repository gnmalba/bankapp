/**
 * @format
 * @flow
 */

import styled from 'styled-components/native';

// COMPONENTS
import Text from '@components/Text';

// UTILS
import {verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';

// STYLED
export const DateButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  flex-direction: row;
  min-width: ${verticalScale(156)}px;
  padding: ${verticalScale(9)}px;
  border-radius: 5px;
  border-width: 1px;
  border-color: ${(props) =>
    props.isActive
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.filterTransaction.calendarInputBorder};
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
  ${'' /* background-color: yellow; */}
  ${'' /* width: 100%; */}
   margin: 10px auto;
`;

export const DateButtonLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'subtitle',
    customColor: props.isActive
      ? props.theme.color.TEXT_300_HIGH
      : props.theme.filterTransaction.placeholderText,
  }),
)``;

export const DateControlLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    themeColor: 'TEXT_300_HIGH',
  }),
)`
  text-transform: capitalize;
  margin-bottom: ${verticalScale(6)}px;
`;
