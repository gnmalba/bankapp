/**
 * @format
 * @flow
 */

import React from 'react';

import type {FilterModalProps} from '../types';

import {
  Modal,
  ModalScrollView,
  FilterControlsContainer,
  HeaderContainer,
  HeaderTitle,
  HeaderSection,
  HeaderButton,
  HeaderLeft,
  HeaderRight,
} from './styled';

const HeaderLeftSection = (props) => (
  <HeaderSection align="left">
    <HeaderButton onPress={props.onPressLeftButton}>
      <HeaderLeft />
    </HeaderButton>
  </HeaderSection>
);

const HeaderRightSection = (props) => (
  <HeaderSection align="right">
    <HeaderButton onPress={props.onPressRightButton}>
      <HeaderRight>{props.rightButtonLabel || ''}</HeaderRight>
    </HeaderButton>
  </HeaderSection>
);

const HeaderTitleSection = ({filterControlName}) => (
  <HeaderSection align="center">
    <HeaderTitle>{filterControlName}</HeaderTitle>
  </HeaderSection>
);

const FilterModalHeader = (props) => {
  return (
    <HeaderContainer>
      <HeaderLeftSection {...props} />
      <HeaderTitleSection {...props} />
      <HeaderRightSection {...props} />
    </HeaderContainer>
  );
};

const FilterModal = (props: FilterModalProps): React$Node => {
  const {children, visible} = props;
  return (
    <Modal
      transparent
      animationType="none"
      visible={visible}
      statusBarTranslucent
      maxSwipeDistance={0}>
      <ModalScrollView>
        <FilterControlsContainer>
          <FilterModalHeader {...props} />
          {children}
        </FilterControlsContainer>
      </ModalScrollView>
    </Modal>
  );
};

export default FilterModal;
