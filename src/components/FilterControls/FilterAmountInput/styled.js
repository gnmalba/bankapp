/**
 * @format
 * @flow
 */
import styled from 'styled-components/native';

import Text from '@components/Text';

import type {ComponentType} from 'react';
import {scale, moderateScale, verticalScale} from '@utils/scale';

// STYLED

export const InputLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  themeColor: 'TEXT_300_HIGH',
}))`
  font-weight: normal;
  margin-bottom: ${verticalScale(12)}px;
  font-family: Myriad Pro;
`;

export const CurrencyCode: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
  themeColor: 'TEXT_300_HIGH',
}))`
  padding-horizontal: ${scale(12)}px;
  margin-top: ${verticalScale(6)}px;
  margin-bottom: ${verticalScale(6)}px;
`;

export const InputContainer: ComponentType<any> = styled.View`
  height: ${verticalScale(40)}px;
  flex-direction: row;
  border-radius: 5px;
  border-width: 1px;
  margin-bottom: ${verticalScale(16)}px;
  padding-vertical: ${verticalScale(5)}px;
  border-color: ${(props) =>
    props.isActive && !props.isError
      ? props.theme.color.SECONDARY_DEFAULT
      : props.amtErrorRender
      ? props.theme.color.RED_STATUS
      : props.theme.filterTransaction.calendarInputBorder};
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
`;

export const AmountTextInput: ComponentType<any> = styled.TextInput.attrs(
  (props) => ({
    selectionColor: props.theme?.color?.TEXT_100_HIGH,
  }),
)`
  flex: 1;
  font-size: ${moderateScale(15)}px;
  border-left-width: 1px;
  padding-horizontal: ${scale(16)}px;
  border-color: #445870;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;
