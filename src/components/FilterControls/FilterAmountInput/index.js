/**
 * @format
 * @flow
 */

import React from 'react';

import {
  CurrencyCode,
  InputContainer,
  InputLabel,
  AmountTextInput,
} from './styled';

import type {FilterAmountProps} from '../types';

import {testProps} from '@utils/componentCustomProps';

// RENDER HELPER [amount input label]
const renderInputLabel = (label) => {
  if (label) {
    return <InputLabel>{label}</InputLabel>;
  }
  return <></>;
};

// MAIN COMPONENT [FilterAmountInput]
const FilterAmountInput = (props: FilterAmountProps): React$Node => {
  const {currencyCode, isActive, isError} = props;

  return (
    <>
      {renderInputLabel(props.label)}
      <InputContainer isActive={isActive} isError={isError}>
        <CurrencyCode>{currencyCode}</CurrencyCode>
        <AmountTextInput
          {...props}
          placeholder={'0.00'}
          returnKeyType="done"
          keyboardType="decimal-pad"
          placeholderTextColor={'#445870'}
          selectionColor={'#FFFFFF'}
          {...testProps('filterTransactionAmountFromField')}
        />
      </InputContainer>
    </>
  );
};

export default FilterAmountInput;
