/**
 * @flow
 */

import type {ComponentType} from 'react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import styled from 'styled-components/native';

import Text from '@components/Text';

import {verticalScale} from '@utils/scale';

// COMMON

export const ButtonDivider: ComponentType<any> = styled.View`
  border-left-width: 1px;
  margin-horizontal: ${verticalScale(10)}px;
  border-color: ${(props) => props.theme.textInput.dividerColor};
  height: ${verticalScale(20)}px;
`;

export const FilterSectionTitle: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    ...props,
    mode: 'overline1',
    themeColor: 'TEXT_100_HIGH',
  }),
)`
  margin-bottom: ${verticalScale(20)}px;
`;

export const CalendarIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'calendar-today',
    size: 20,
    color: props.theme?.text.textEmphasis,
  }),
)``;

// FILTER MODAL

export const Modal: ComponentType<any> = styled.Modal`
  justify-content: flex-end;
  margin: 0;
  position: absolute;
`;

export const ModalScrollView: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs((props) => ({
  contentContainerStyle: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'flex-end',
    flex: 1,
  },
}))``;

export const FilterControlsContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_200};
  border-top-right-radius: ${verticalScale(20)}px;
  border-top-left-radius: ${verticalScale(20)}px;
  height: 90%;
  padding-horizontal: ${verticalScale(24)}px;
  padding-top: ${verticalScale(24)}px;
`;

// MODAL HEADER

export const HeaderContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${verticalScale(24)}px;
  align-items: center;
`;

export const HeaderSection: ComponentType<any> = styled.View`
  flex: ${(props) => (props.align === 'center' ? 2 : 1)};
  align-items: ${(props) => {
    if (props.align !== 'center') {
      return props.align === 'right' ? 'flex-end' : 'flex-start';
    }
    return 'center';
  }};
`;

export const HeaderButton: ComponentType<any> = styled.TouchableOpacity``;

export const HeaderTitle: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'h3',
  themeColor: 'TEXT_100_HIGH',
}))``;

export const HeaderRight: ComponentType<any> = styled(Text).attrs((props) => ({
  ...props,
  mode: 'subtitle',
  themeColor: 'SECONDARY_DEFAULT',
}))``;

export const HeaderLeft: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'west',
    size: 20,
    color: props.theme?.color.TEXT_300_HIGH,
  }),
)``;
