import React from 'react';
import {render} from '@testing-library/react-native';
import {
  FilterModal,
  FilterWithCounterButton,
  FilterAmountInput,
  FilterDateButton,
} from '@components/FilterControls';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

const filterModalSampleProps = {
  filterControlName: 'Transaction History',
  visible: true,
};

const filterButtonSampleProps = {
  label: 'Filter',
  filter: 3,
};

const filterAmountSampleProps = {
  isActive: true,
  isError: false,
  currencyCode: 'USD',
  value: 3000,
  label: 'Money',
  onChangeText: (value) => value,
};

const filterDateControlSampleProps = {
  isActive: true,
  label: 'Date',
};

describe('FilterControls', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <FilterModal {...filterModalSampleProps}>
          <FilterWithCounterButton {...filterButtonSampleProps} />
          <FilterAmountInput {...filterAmountSampleProps} />
          <FilterDateButton {...filterDateControlSampleProps} />
        </FilterModal>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
