/**
 * @format
 * @flow
 */

import React from 'react';

// COMPONENTS
import FilterAmountInput from '../FilterAmountInput';
import {FilterAmountContainer, ErrorText} from './styled';

// TYPES
import type {FilterAmountRangeProps} from '../types';

// UTILS
import {checkMinIsGreaterThanMax} from '../utils';

const FIELD_NAMES = {
  min: 'enteredMinAmount',
  max: 'enteredMaxAmount',
};

const errors = {
  minIsGreater: 'Amount FROM must be lower than TO',
};

// RENDER HELPERS
const renderErrorMessage = (hasError) => {
  if (hasError) {
    return <ErrorText>{errors.minIsGreater}</ErrorText>;
  }
  return <></>;
};

const FilterAmountInputRange = (props: FilterAmountRangeProps): React$Node => {
  const {
    setAmountRange,
    onFocusAmountField,
    minAmount,
    maxAmount,
    activeField,
  } = props;

  const hasError = checkMinIsGreaterThanMax(minAmount, maxAmount);

  return (
    <FilterAmountContainer>
      {/* MIN AMOUNT FIELD */}
      <FilterAmountInput
        isError={hasError}
        isActive={activeField === FIELD_NAMES.min}
        label="From"
        value={minAmount}
        onFocus={() => onFocusAmountField(FIELD_NAMES.min)}
        onChangeText={(value) => setAmountRange(value, FIELD_NAMES.min)}
        onBlur={onFocusAmountField}
        currencyCode={props.currencyCode || 'PHP'}
      />
      {/* MAX AMOUNT FIELD */}
      <FilterAmountInput
        isError={hasError}
        isActive={activeField === FIELD_NAMES.max}
        label="To"
        value={maxAmount}
        onFocus={() => onFocusAmountField(FIELD_NAMES.max)}
        onChangeText={(value) => setAmountRange(value, FIELD_NAMES.max)}
        currencyCode={props.currencyCode || 'PHP'}
      />
      {renderErrorMessage(hasError)}
    </FilterAmountContainer>
  );
};

export default FilterAmountInputRange;
