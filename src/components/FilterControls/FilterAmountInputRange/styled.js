/**
 * @format
 * @flow
 */

import styled from 'styled-components/native';

// COMPONENTS
import Text from '@components/Text';

// TYPES
import type {ComponentType} from 'react';

// UTILS
import {verticalScale} from '@utils/scale';

// STYLED
export const FilterAmountContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(24)}px;
`;

export const ErrorText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
  themeColor: 'RED_STATUS',
}))``;
