/**
 * @flow
 */

export type FilterModalProps = {
  testID?: string,
  accessibilityLabel?: string,
  children?: React$Node,
  visible?: boolean,
  rightButtonLabel?: string,
  filterControlName: string,
  onPressLeftButton?: any,
  onPressRightButton?: any,
};

export type ButtonProps = {
  label?: string,
  onPress?: any,
};

export type FilterButtonProps = ButtonProps & {
  filterCount: number,
};

export type FilterAmountProps = {
  testID?: string,
  accessibilityLabel?: string,
  currencyCode: string,
  isActive: boolean,
  isError: boolean,
  label?: string,
  value: ?number,
  onFocus?: any,
  onEndEditing?: any,
  onBlur?: any,
  onChangeText?: (value: ?number) => any,
};

export type FilterAmountRangeProps = {
  testID?: string,
  accessibilityLabel?: string,
  currencyCode?: string,
  minAmount: ?number,
  maxAmount: ?number,
  activeField: string,
  setAmountRange: any, //(value: string, field: string) => Object,
  onFocusAmountField: (field: string) => Object,
};

export type FilterDateButtonProps = {
  isActive: ?boolean,
  dateValue: ?string,
  onPress?: any,
  label?: ?string,
};

export type SelectedDateRange = {
  selectedStartDate: ?string,
  selectedEndDate: ?string,
};

export type FilterDateRangeControlsProps = {
  startDate: ?string,
  endDate: ?string,
  initialActiveField?: ?string,
  customOnChange?: (dateRange: SelectedDateRange) => any,
  onPressDateControl: (fieldName: string) => any,
  onPressApply: (dateRange: SelectedDateRange) => any,
  onPressCancel: () => any,
};
