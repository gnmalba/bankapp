export const isEmpty = (value) =>
  !value || value === undefined || value === 0 || value === '';

export const checkMinIsGreaterThanMax = (min, max) => min && max && min > max;
export const checkAmountRangeIsEmpty = (min, max) =>
  (!min && !max) || (min === 0 && max === 0);
