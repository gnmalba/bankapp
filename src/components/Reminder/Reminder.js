/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {StyledContainer, StyledIcon, StyledText} from './Styled';

const Reminder = (props: PropsType): React$Node => {
  const {theme, colorTheme} = props;
  return (
    <StyledContainer theme={theme} colorTheme={colorTheme}>
      <StyledIcon />
      <StyledText>{props.text}</StyledText>
    </StyledContainer>
  );
};

export default Reminder;
