import React from 'react';
import {render} from '@testing-library/react-native';
import Reminder from '../Reminder';
import {testProps} from '@utils/componentCustomProps';

describe('Reminder', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<Reminder {...testProps('reminderTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
