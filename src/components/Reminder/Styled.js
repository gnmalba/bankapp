/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Text from '@components/Text';

import {verticalScale, moderateScale} from '@utils/scale';

export const StyledContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: row;
  border-radius: 4px;
  align-items: flex-start;
  background-color: ${(props) => {
    let color = 'transparent';
    if (props.colorTheme) {
      color = props.theme.reminder[props.colorTheme].backgroundColor;
    }
    return color;
  }};
  border: ${(props) => {
    let color = 'transparent';
    if (props?.colorTheme) {
      color = props.theme.reminder[props.colorTheme].borderColor;
    }
    return color;
  }};
  padding: 8px 16px;
`;

// still wip
export const StyledIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/warning-icon.png'),
  resizeMode: 'contain',
}))`
  tint-color: #bda726;
  margin-right: ${moderateScale(17)}px;
  margin-top: ${verticalScale(3)}px;
  align-self: center;
  height: ${verticalScale(18)}px;
  width: ${moderateScale(18)}px;
`;

export const StyledText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
}))`
  font-family: Myriad Pro;
  width: 85%;
  color: #7e6f1b;
`;
