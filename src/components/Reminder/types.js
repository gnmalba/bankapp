/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  text: string,
  colorTheme?: string,
  theme?: Object,
};
