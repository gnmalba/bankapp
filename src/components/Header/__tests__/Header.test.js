import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import Header from '@components/Header';
import {testProps} from '@utils/componentCustomProps';

const params = [
  {
    isBackButton: true,
    title: 'Test',
    testID: 'headerTestIdHeader',
  },
  {
    isBackButton: false,
    title: undefined,
    testID: 'headerTestIdHeader',
  },
  {
    isBackButton: undefined,
    title: '',
    testID: undefined,
  },
];

describe('Header', () => {
  it.each(params)('Should work properly', (param) => {
    const all = render(
      <Header
        {...testProps('headerTestId')}
        isBackButton={param.isBackButton}
        title={param.title}
        onBackClick={param.isBackButton}
      />,
    );
    all.debug();
    if (param.testID) {
      const el = all.getByTestId('headerTestIdHeader');
      fireEvent(el, 'leftComponent');
      fireEvent(el, 'centerComponent');
      fireEvent(el, 'rightComponent');
    }
    expect(all.toJSON()).toMatchSnapshot();
  });
});
