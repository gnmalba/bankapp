import {BackIcon, HamburgerIcon, GearIcon, InfoIcon} from '../Styled';
import React from 'react';
import {render} from '@testing-library/react-native';
import {testProps} from '@utils/componentCustomProps';

describe('Styled', () => {
  test('Styled', () => {
    expect(
      render(
        <BackIcon
          {...testProps('backIconTestId')}
          source={require('@assets/icons/back-icon.png')}
        />,
      ).toJSON(),
    ).toMatchSnapshot();

    expect(
      render(<HamburgerIcon {...testProps('hambergerIconTestIs')} />).toJSON(),
    ).toMatchSnapshot();

    expect(
      render(<GearIcon {...testProps('gearIconTestId')} />).toJSON(),
    ).toMatchSnapshot();

    expect(
      render(<InfoIcon {...testProps('InfoIcontestId')} />).toJSON(),
    ).toMatchSnapshot();
  });
});
