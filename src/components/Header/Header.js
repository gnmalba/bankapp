/**
 * @flow
 */
import React from 'react';

import {TouchableOpacity} from 'react-native-gesture-handler';

import {StyledHeader, BackIcon, HamburgerIcon} from './Styled';
import {testProps} from '@utils/componentCustomProps';
import {SemiboldText} from '@utils/commonStyles';

type HeaderPropTypes = {
  onBackClick?: ?() => void,
  title: string,
  leftComponent?: any,
  centerComponent?: React$Node,
  rightComponent?: any,
  isBackButton?: boolean,
  isTransparent?: boolean,
  testID?: string,
};

const Header = (props: HeaderPropTypes): React$Node => {
  const {
    onBackClick,
    leftComponent,
    centerComponent,
    rightComponent,
    title = '',
    isBackButton = true,
    testID = '',
    isTransparent,
  } = props;
  const baseTestID = testID || title;
  const backButton = () =>
    onBackClick &&
    ((
      <TouchableOpacity
        onPress={onBackClick}
        {...testProps(baseTestID + 'BackButton')}>
        <BackIcon />
      </TouchableOpacity>
    ) ||
      leftComponent);

  const hamburgerMenu = () => (
    <TouchableOpacity
      onPress={onBackClick}
      {...testProps(baseTestID + 'MenuButton')}>
      <HamburgerIcon />
    </TouchableOpacity>
  );

  return (
    <StyledHeader
      {...testProps(baseTestID + 'Header')}
      leftComponent={() => (isBackButton ? backButton() : hamburgerMenu())}
      centerComponent={() =>
        title &&
        ((
          <SemiboldText
            numberOfLines={1}
            mode="h3"
            {...testProps(baseTestID + 'HeaderTitle')}>
            {title}
          </SemiboldText>
        ) ||
          centerComponent)
      }
      rightComponent={() => rightComponent}
      isTransparent={isTransparent}
    />
  );
};

export default Header;
