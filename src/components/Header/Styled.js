/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {Header as RNEHeader} from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {moderateScale, verticalScale} from '@utils/scale';

export const StyledHeader: ComponentType<any> = styled(RNEHeader).attrs(
  (props) => ({
    backgroundColor: props.isTransparent
      ? 'transparent'
      : props.theme?.topAppBar?.backgroundColor,
    barStyle: props.theme?.topAppBar?.barStyle,
    containerStyle: {
      borderBottomColor: 'transparent',
    },
    leftContainerStyle: {
      justifyContent: 'center',
    },
    centerContainerStyle: {
      justifyContent: 'center',
      marginTop: verticalScale(3),
    },
    rightContainerStyle: {
      justifyContent: 'center',
    },
  }),
)``;

export const BackIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/back-icon.png'),
}))`
  resize-mode: contain;
  height: ${moderateScale(18)}px;
  width: ${moderateScale(18)}px;
  margin-left: ${moderateScale(8)}px;
  tint-color: ${(props) => props.theme.topAppBar.iconColor};
`;

export const HamburgerIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'menu',
    size: moderateScale(25),
    color: props.theme.customizeDashboard.customizeText,
  }),
)``;

export const GearIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/gear-icon.png'),
}))`
  resize-mode: contain;
  height: ${moderateScale(20)}px;
  width: ${moderateScale(20)}px;
  tint-color: ${(props) => props.theme.topAppBar.iconColor};
`;

export const InfoIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/information-icon.png'),
}))`
  resize-mode: contain;
  height: ${moderateScale(24)}px;
  width: ${moderateScale(24)}px;
  margin-left: ${moderateScale(8)}px;
  tint-color: ${(props) => props.theme.topAppBar.iconColor};
`;
