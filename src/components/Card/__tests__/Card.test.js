/**
 * @flow
 */

import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import {render, fireEvent} from '@testing-library/react-native';
import {Card} from '../Card';
import Text from '@components/Text';
import MockProvider from '@utils/testFramework';
import {DARK_THEME} from '@utils/constants';

const store = {
  config: {
    widgets: [
      {
        ref: 1,
        label: 'Test Label A',
        loading: false,
      },
      {
        ref: 2,
        label: 'Test Label B',
        loading: false,
      },
    ],
  },
};

describe('Card', () => {
  it('Should render <Card /> component as expected', () => {
    const component = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <Card title="TestTitle">
            <Text mode="body1">Test child</Text>
          </Card>
        </ThemeProvider>
      </MockProvider>,
    );

    const el = component.getByTestId('moreOptionsTestTitleBtn');
    component.debug();
    fireEvent(el, 'onPress');
    expect(component.toJSON()).toMatchSnapshot();
  });
});
