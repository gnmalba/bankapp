/**
 * @flow
 */

// Third party libraries
import {Text} from 'react-native-elements';
import Entypo from 'react-native-vector-icons/Entypo';

// Utils
import {verticalScale, scale} from '@utils/scale';
import styled from 'styled-components/native';

const Container: any = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: 8px;
  padding: 16px;
`;

const Header: any = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Divider: any = styled.View`
  background-color: ${(props) => props.theme.card.dividerBgColor};
  height: ${scale(1)}px;
  margin: 16px 0;
`;

const Title: any = styled(Text)`
  color: ${(props) => props.theme.card.textColor};
  font-style: normal;
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${scale(20)}px;
  line-height: 24px;
`;

const Icon: any = styled(Entypo)`
  margin-top: ${verticalScale(2)}px;
  bottom: 1;
`;

const HeaderButtonContainer: any = styled.View`
  border-radius: ${scale(24)}px;
  height: ${scale(24)}px;
  width: ${scale(24)}px;
  align-content: center;
  align-self: center;
  align-items: center;
  justify-content: center;
  background-color: ${(props) =>
    props.bgColor
      ? props.theme?.displayOptionsTooltip?.widgetHeaderIcon
      : 'transparent'};
`;

export {Container, Header, Title, Divider, Icon, HeaderButtonContainer};
