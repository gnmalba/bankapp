// @flow

import {StyleSheet} from 'react-native';
import {scale, moderateScale, verticalScale} from '@utils/scale';

const styles: Object = StyleSheet.create({
  toolTipStyles: {
    borderRadius: moderateScale(4),
    marginTop: verticalScale(-20),
    paddingVertical: verticalScale(16),
    shadowColor: '#000',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 8,
    transform: [{translateY: verticalScale(34)}, {translateX: scale(3.4)}],
  },
});

export default styles;
