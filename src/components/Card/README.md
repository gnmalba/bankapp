## BasicTextInput

Card component.

# Usage

```js
import BasicTextInput from '@components/Card';

# Props
```

| Prop       | Type    | Required | Description           |
| ---------- | ------- | -------- | --------------------- |
| SampleProp | boolean | false    | This is only a sample |
