/**
 * @flow
 */

// React modules
import React, {useState, useRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity, Platform} from 'react-native';

// Third party libraries
import Entypo from 'react-native-vector-icons/Entypo';
import styled from 'styled-components/native';

// Components
import WidgetTooltip from '@components/WidgetTooltip';
import DisplayOptions from '@composition/DisplayOptions';
import Text from '@components/Text';

// Utils
import * as Styled from './Styled';
import type {CardPropTypes} from './types';
import {moderateScale, scale} from '@utils/scale';
import styles from './styles';
import {testProps} from '@utils/componentCustomProps';

import type {Widget} from '@ducks/config/types';

import {getWidgets} from '@ducks/config/selectors';
import {removeWidget} from '@ducks/config/actions';
import {toggleCustomizeWidget} from '@ducks/dashboard/actions';
import {navigate} from '@navigations/service';

const StyledIcon = styled(Entypo).attrs((props) => ({
  color: props.theme.color.TEXT_100_HIGH,
}))``;

export const Card = (props: CardPropTypes): React$Node => {
  const widgets = useSelector(getWidgets);
  const dispatch = useDispatch();
  const {title, actions, children, options, theme, testID = ''} = props;
  const [isTooltipButtonEnabled, setTooltipButtonEnabled] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);

  const toolTipRef = useRef(null);

  const handleToggleTooltip = () => {
    if (toolTipRef.current) {
      toolTipRef.current.toggleTooltip();

      setTooltipButtonEnabled(true);
    }
  };

  const handleSetFiltered = (value, index) => {
    setSelectedOption(index);
  };

  const getWidgetRefByLabel = (label: string): Widget =>
    widgets.find((widget) => widget.label === label);

  const widgetHeaderTooltip = () => {
    return (
      <WidgetTooltip
        forwardedRef={toolTipRef}
        withOverlay
        popover={
          <DisplayOptions
            // index={index}
            widgetFlag={title}
            // componentName={componentName}
            options={options}
            selectedOption={selectedOption}
            setFilteredBy={handleSetFiltered}
            onMovePress={() => {
              handleToggleTooltip();
              dispatch(toggleCustomizeWidget());
              navigate('CustomizeWidgetScreen');
            }}
            onRemovePress={(label) => {
              const widgetRefToBeRemove = getWidgetRefByLabel(label);
              // not necessary checking but just to avoid breaking of the app in case widget does not set up correctly
              if (typeof widgetRefToBeRemove !== 'undefined') {
                handleToggleTooltip();
                dispatch(removeWidget(widgetRefToBeRemove.ref, true));
              }
            }}
          />
        }
        width={Platform.select({android: scale(185), ios: scale(190)})}
        style={styles.toolTipStyles}
        setTooltipButtonEnabled={setTooltipButtonEnabled}
        {...testProps((testID || 'tooltipDisplayOptions') + 'Tooltip')}
      />
    );
  };

  return (
    <Styled.Container theme={theme}>
      <Styled.Header>
        <Text mode="h3" {...testProps(`${title}testId`)}>
          {title}
        </Text>
        <Styled.HeaderButtonContainer bgColor={isTooltipButtonEnabled}>
          {widgetHeaderTooltip()}
          <TouchableOpacity
            onPress={handleToggleTooltip}
            {...testProps(`moreOptions${title}Btn`)}>
            <StyledIcon
              name="dots-three-vertical"
              size={moderateScale(16)}
              theme={theme}
            />
          </TouchableOpacity>
        </Styled.HeaderButtonContainer>
        {actions}
      </Styled.Header>
      <Styled.Divider theme={theme} {...testProps(testID + 'Divider')} />
      {children}
    </Styled.Container>
  );
};

export const Divider = Styled.Divider;

export default Card;
