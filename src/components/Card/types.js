/**
 * @flow
 */

export type CardPropTypes = {
  title: string,
  children: React$Node,
  actions?: React$Node,
  options?: any,
  theme?: Object,
  testID?: string,
};
