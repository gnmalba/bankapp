import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';

import Avatar from '@components/Avatar';
import {testProps} from '@utils/componentCustomProps';

import {DARK_THEME, LIGHT_THEME} from '@utils/constants';

describe('Avatar', () => {
  it.each([
    {
      theme: LIGHT_THEME,
      title: 'Jane Doe',
      uri: 'http://placehold.it/480x270',
      size: 50,
    },
    {
      theme: DARK_THEME,
      title: null,
      uri: 'http://placehold.it/480x270',
      size: null,
    },
    {theme: DARK_THEME, title: 'John Doe', uri: null, size: null},
    {theme: LIGHT_THEME, title: null, uri: null, size: null},
    {theme: DARK_THEME, title: 'Metrobank', uri: null, size: 100},
    {theme: DARK_THEME, title: 'Metrobank', uri: null, size: 'large'},
    {theme: DARK_THEME, title: 'Metrobank', uri: null, size: 'medium'},
  ])('Should work as expected', (params) => {
    const all = render(
      <ThemeProvider theme={params.theme}>
        <Avatar
          {...testProps('avatarTestId')}
          uri={params.uri}
          title={params.title}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
