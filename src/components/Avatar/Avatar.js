/**
 * @format
 * @flow
 */

import React from 'react';
import styled from 'styled-components/native';

import RNEAvatar from 'react-native-elements/dist/avatar/Avatar';
import {scale} from '@utils/scale';

import type {ComponentType} from 'react';
import type {AvatarProps} from './types';

const getInitial = (text) => {
  if (text) {
    return text.charAt(0).toUpperCase();
  }
  return 'M';
};

const AVATAR_SIZES = {
  small: 24,
  medium: 40,
  large: 64,
};

const getNumberSize = (size) => {
  return typeof size === 'number' ? scale(size - size / 4) : null;
};

const getSize = (size: string | number) => {
  if (getNumberSize(size)) {
    return {
      padding: getNumberSize(size),
      borderRadius: 50,
    };
  }
  return {
    padding: scale(
      AVATAR_SIZES[size || 'small'] - AVATAR_SIZES[size || 'small'] / 4,
    ),
    borderRadius: 50,
  };
};

const Container: ComponentType<any> = styled.View``;

const StyledAvatar: ComponentType<any> = styled(RNEAvatar).attrs((props) => ({
  ...props,
  rounded: true,
  title: getInitial(props.title),
  activeOpacity: 0.7,
  overlayContainerStyle: {
    backgroundColor: props.theme?.text?.textEmphasis,
    ...getSize(props.size),
  },
  titleStyle: props.titleStyle,
  source: {uri: props.uri},
}))``;

const Avatar = (props: AvatarProps): React$Node => {
  return (
    <Container>
      <StyledAvatar {...props} />
    </Container>
  );
};

export default Avatar;
