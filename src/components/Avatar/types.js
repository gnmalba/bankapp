/**
 * @flow
 */

export type AvatarProps = {
  testID?: string,
  size?: string | number,
  title?: string,
  uri?: string,
};
