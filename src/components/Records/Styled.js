/**
 * @flow
 */

import type {ComponentType} from 'react';
import {FlatList} from 'react-native';

import styled from 'styled-components/native';

import {verticalScale} from '@utils/scale';

// RECORDS LIST

export const Container: ComponentType<any> = styled.View``;

export const List: ComponentType<any> = styled(FlatList).attrs((props) => ({
  contentContainerStyle: {
    flexGrow: 1,
    backgroundColor: 'transparent',
  },
}))`
  width: 100%;
`;

// RECORD ITEM

export const Item: ComponentType<any> = styled.View`
  flex-direction: row;
  padding-vertical: ${verticalScale(16)}px;
  background-color: ${(props) =>
    props.index % 2 === 0
      ? props.theme.color.BACKGROUND_200
      : props.theme.transaction.background};
`;
