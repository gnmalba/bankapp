/**
 * @format
 * @flow
 */

import React from 'react';

import type {RecordsProps} from './types';
import {List} from './Styled';

// NOTE: update this implementation if custom props, extra handling will be added
const Records = (props: RecordsProps): React$Node => {
  return <List {...props} />;
};

export default Records;
