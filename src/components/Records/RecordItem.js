/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity} from 'react-native';

import type {RecordItemProps} from './types';
import {Item} from './Styled';

const RecordItem = (props: RecordItemProps): React$Node => {
  const {index, children, onPress} = props;

  return (
    <TouchableOpacity onPress={onPress}>
      <Item index={index}>{children}</Item>
    </TouchableOpacity>
  );
};

export default RecordItem;
