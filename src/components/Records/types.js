/**
 * @flow
 */

export type RecordsProps = {
  data: Array<any>,
};

export type RecordItemProps = {
  testID?: string,
  accessibilityLabel?: string,
  index: number,
  children?: React$Node,
  onPress?: any,
};
