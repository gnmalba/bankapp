import React from 'react';
import {render} from '@testing-library/react-native';
import {Records, RecordItem} from '../index';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('Records', () => {
  it('Should work as expected', () => {
    const data = [1, 2, 3];
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <Records
          data={data}
          renderItem={(item, index) => <RecordItem index={index} />}
          keyExtractor={(item, index) => `${item}-${index}`}
        />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
