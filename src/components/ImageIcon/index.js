/**
 * @flow
 */

import {testProps} from '@utils/componentCustomProps';
import React from 'react';

import styled from 'styled-components/native';

export type ImageIconProps = {
  source: React$Node,
  height?: number,
  width?: number,
  testID?: string,
};

const Image = styled.Image`
  height: ${(props) => (props.height ? props.height : '18px')};
  width: ${(props) => (props.width ? props.width : '18px')};
`;

const ImageIcon = ({
  source,
  height,
  width,
  testID,
}: ImageIconProps): React$Element<any> => (
  <Image
    source={source}
    height={height}
    width={width}
    {...testProps(testID || 'imageIconTestId')}
  />
);

export default ImageIcon;
