/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

// Utils
import {useTheme} from 'styled-components';
import IconTooltip from '@components/IconTooltip';
import type {PropsType, CheckboxTemplatePropsType} from './types';
import * as S from './Styled';
import {DARK_THEME} from '@utils/constants';

const RADIO_ICON = require('@assets/icons/active-circle-icon.png');
const UNCHECKED_RADIO_ICON = require('@assets/icons/inactive-circle-icon.png');

export const CheckboxTemplate = ({
  theme,
  disabled = false,
  checked = false,
  testID = '',
  accessibilityLabel,
}: CheckboxTemplatePropsType): React$Node => {
  const color = checked
    ? theme.color.SECONDARY_DEFAULT
    : theme.color.HIGH_EMPHASIS;

  return (
    <Ionicons
      name={checked ? 'ios-checkbox' : 'ios-square-outline'}
      size={20}
      color={disabled ? theme.color.TEXT_300_LOW : color}
      testID={testID}
      accessibilityLabel={accessibilityLabel}
    />
  );
};

export const RadioCheckboxTemplate = ({
  theme,
  disabled = false,
  checked = false,
  testID,
  accessibilityLabel,
}: CheckboxTemplatePropsType): React$Node => {
  const color = checked
    ? theme.color.SECONDARY_DEFAULT
    : theme.color.HIGH_EMPHASIS;

  return (
    <IconTooltip
      source={checked ? RADIO_ICON : UNCHECKED_RADIO_ICON}
      height={20}
      width={20}
      color={
        disabled
          ? theme.color.TEXT_300_LOW
          : checked
          ? color
          : theme.color.TEXT_300_LOW
      }
      testID={testID}
      accessibilityLabel={accessibilityLabel}
    />
  );
};

const Checkbox = (props: PropsType): React$Node => {
  const {
    testID = '',
    accessibilityLabel,
    title,
    checked,
    onPress,
    isCheckbox = true,
    disabled = false,
    fontSize = 18,
    customStyle = {},
  } = props;
  const theme = useTheme() || DARK_THEME;

  const renderIcon = () => {
    if (!isCheckbox) {
      return (
        <RadioCheckboxTemplate
          theme={theme}
          disabled={disabled}
          checked={checked}
          testID={testID}
          accessibilityLabel={accessibilityLabel}
        />
      );
    }

    return (
      <CheckboxTemplate
        theme={theme}
        disabled={disabled}
        checked={checked}
        testID={testID}
        accessibilityLabel={accessibilityLabel}
      />
    );
  };

  return (
    <>
      <S.StyledCheckbox
        title={title}
        titleProps={{
          testID: `${testID}Label`,
          accessibilityLabel: `${accessibilityLabel}Label`,
        }}
        checked={checked}
        disabled={disabled}
        onPress={onPress}
        checkedIcon={renderIcon()}
        uncheckedIcon={renderIcon()}
        activeOpacity={1}
        customTheme={theme}
        fontSize={fontSize}
        {...customStyle}
      />
    </>
  );
};

export default Checkbox;
