/**
 * @flow
 */

export type PropsType = {
  checked: boolean,
  onPress: () => void,
  title: string,
  isCheckbox?: boolean,
  disabled?: boolean,
  testID?: string,
  accessibilityLabel: string,
  fontSize?: number,
  customStyle?: Object,
};

export type CheckboxTemplatePropsType = {
  theme: Object,
  disabled?: boolean,
  checked: boolean,
  testID?: string,
  accessibilityLabel?: string,
};
