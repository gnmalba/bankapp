/**
 * @flow
 */

import type {ComponentType} from 'react';
import {CheckBox} from 'react-native-elements';
import styled from 'styled-components/native';
import {moderateScale, verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View``;

export const StyledCheckbox: ComponentType<any> = styled(CheckBox).attrs(
  (props) => ({
    containerStyle: {
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      color: props.customTheme.color.TEXT_300_HIGH,
      marginLeft: 0,
      paddingLeft: 0,
      margin: 0,
      padding: 0,
      marginBottom: props.marginBottom || verticalScale(18),
      ...props.customStyle,
    },
    textStyle: {
      fontFamily: 'Myriad Pro',
      fontWeight: 'normal',
      fontSize: moderateScale(props.fontSize),
      color: props.customTheme.color.TEXT_300_HIGH,
      opacity: props.disabled ? 0.5 : 1,
    },
  }),
)``;
