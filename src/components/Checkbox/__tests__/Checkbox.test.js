import React from 'react';
import {render} from '@testing-library/react-native';
import Checkbox, {CheckboxTemplate, RadioCheckboxTemplate} from '../Checkbox';
import {testProps} from '@utils/componentCustomProps';
import {DARK_THEME} from '@utils/constants';

const testCase = 'Should work as expected';
describe('Checkbox', () => {
  it.each([true, false, null, undefined])(testCase, (bools) => {
    // Edit the args inside render and provide props.
    const all = render(
      <Checkbox
        {...testProps('checkBocTestId')}
        isCheckbox={bools}
        disabled={bools}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});

describe('CheckboxTemplate', () => {
  it.each([true, false, null, undefined])(testCase, (bools) => {
    // Edit the args inside render and provide props.
    const all = render(
      <CheckboxTemplate
        {...testProps('checkBocTestId')}
        checked={bools}
        disabled={bools}
        theme={DARK_THEME}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});

describe('RadioCheckboxTemplate', () => {
  it.each([true, false, null, undefined])(testCase, (bools) => {
    // Edit the args inside render and provide props.
    const all = render(
      <RadioCheckboxTemplate
        {...testProps('checkBocTestId')}
        checked={bools}
        disabled={bools}
        theme={DARK_THEME}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
