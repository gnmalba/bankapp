## TimePicker

TimePicker component.

# Usage

```js
import TimePicker from '@components/TimePicker';

# Props
```

| Prop               | Type   | Required | Description                 |
| ------------------ | ------ | -------- | --------------------------- |
| testID             | string | true     | The testID prop             |
| accessibilityLabel | string | true     | The accessibilityLabel prop |
| timePickerProps    | Object | true     | The props of time picker    |
