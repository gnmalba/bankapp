import React from 'react';
import {render} from '@testing-library/react-native';
import TimePicker from '../TimePicker';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('TimePicker', () => {
  const defaultProps = {
    timePickerProps: {
      date: new Date('Thu Apr 29 2021 16:41:03 GMT+0800 (PST)'),
    },
  };
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <TimePicker {...defaultProps} />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
