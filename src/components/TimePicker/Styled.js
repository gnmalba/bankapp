/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import DatePicker from 'react-native-date-picker';

export const StyledTimePicker: ComponentType<any> = styled(DatePicker).attrs(
  (props) => ({
    mode: 'time',
    fadeToColor: props.theme.color.BACKGROUND_100,
    textColor: props.theme.color.TEXT_300_HIGH,
    androidVariant: 'iosClone',
    style: {
      flex: 1,
    },
    dividerHeight: 1,
    date: props.date,
    onDateChange: props.onDateChange,
  }),
)``;
