/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {StyledTimePicker} from './Styled';

const TimePicker = (props: PropsType): React$Node => {
  const {timePickerProps} = props;
  return <StyledTimePicker {...timePickerProps} />;
};

export default TimePicker;
