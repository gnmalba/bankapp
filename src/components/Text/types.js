/**
 * @flow
 */

export type TextModeType =
  | 'h1'
  | 'h2'
  | 'h3'
  | 'normal'
  | 'body1'
  | 'body2'
  | 'body3'
  | 'subtitle'
  | 'buttontext'
  | 'buttonlink'
  | 'link'
  | 'link1'
  | 'caption'
  | 'overline1'
  | 'overline2'
  | 'custom'
  | 'listitem'
  | 'subBody'
  | 'normalSemibold'
  | 'inputerror';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  mode?: TextModeType,

  nativeProps?: Object,
  children?: React$Node,
  customColor?: string,
  themeColor?: string,
  theme?: Object,
  customStyle?: Object,
};
