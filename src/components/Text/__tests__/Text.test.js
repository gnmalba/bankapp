import React from 'react';
import {render} from '@testing-library/react-native';
import Text from '../Text';
import {testProps} from '@utils/componentCustomProps';

describe('Text', () => {
  it.each(['body2', 'overline1', 'custom', undefined])(
    'Should work as expected',
    (mode) => {
      const all = render(
        <Text
          mode={mode}
          themeColor={mode === undefined ? mode : 'TEXT_300_LOW'}
          {...testProps('testId')}
        />,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
