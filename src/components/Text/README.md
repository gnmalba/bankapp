## Text

Text component.

# Usage and Types

```js
import Text from '@components/Text';
type mode =
| 'h1'
| 'h2'
| 'h3'
| 'body1'
| 'body2'
| 'subtitle'
| 'buttontext'
| 'buttonlink'
| 'link'
| 'caption'
| 'overline1'
| 'overline2',

export type themeColorType =
| 'PRIMARY_DISABLED'
| 'PRIMARY_DEFAULT'
| 'PRIMARY_PRESSED'
| 'SECONDARY_DISABLED'
| 'SECONDARY_DEFAULT'
| 'SECONDARY_PRESSED'
| 'BACKGROUND_300'
| 'BACKGROUND_200'
| 'BACKGROUND_100'
| 'TEXT_300_LOW'
| 'TEXT_200_MEDIUIM'
| 'TEXT_100_HIGH'
| 'GREEN_STATUS'
| 'RED_STATUS'
| 'DISABLED_STATUS'
| 'ORANGE_STATUS'
| 'YELLOW_STATUS';

# Props
```

| Prop               | Type           | Required | Description             |
| ------------------ | -------------- | -------- | ----------------------- |
| testID             | string         | true     | One of the preset modes |
| accessibilityLabel | string         | true     | One of the preset modes |
| mode               | string         | true     | One of the preset modes |
| themeColor         | themeColorType | false    | Theme color name        |
| customColor        | string         | false    | User defined color      |

# Example

```js
<Text
  {...testProps('versionTestId')}
  mode="overline2"
  themeColor="TEXT_300_LOW">
  VERSION
</Text>
```
