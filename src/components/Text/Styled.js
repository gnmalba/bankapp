/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {moderateScale} from '@utils/scale';

const getColor = (props) => {
  if ((props.customColor || props.themeColor) && props.theme?.color) {
    return props.customColor || props.theme?.color[props.themeColor];
  }
  return props.theme?.text?.textColor || 'transparent';
};

const custom: ComponentType<any> = styled.Text`
  color: ${(props) => {
    return getColor(props);
  }};
`;

const h1: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${moderateScale(34)}px;
  line-height: ${moderateScale(41)}px;
  color: ${(props) => getColor(props)};
`;

const h2: ComponentType<any> = styled(h1)`
  font-family: Myriad Pro;
  font-size: ${moderateScale(24)}px;
  line-height: ${moderateScale(29)}px;
`;

const h3: ComponentType<any> = styled(h1)`
  font-family: Myriad Pro;
  font-weight: 600;
  font-size: ${moderateScale(20)}px;
  line-height: ${moderateScale(24)}px;
`;

const normal: ComponentType<any> = styled(h1)`
  font-family: Myriad Pro;
  font-weight: 400;
  font-size: ${moderateScale(20)}px;
  line-height: ${moderateScale(24)}px;
`;

const normalSemibold: ComponentType<any> = styled(h1)`
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${moderateScale(20)}px;
  line-height: ${moderateScale(24)}px;
`;

const body1: ComponentType<any> = styled(h1)`
  font-size: ${moderateScale(18)}px;
  line-height: ${moderateScale(22)}px;
`;

const body2: ComponentType<any> = styled(h1)`
  font-size: ${moderateScale(15)}px;
  line-height: ${moderateScale(17)}px;
`;

const body3: ComponentType<any> = styled(body2)`
  font-family: Myriad Pro;
`;

const body4: ComponentType<any> = styled(h1)`
  font-size: ${moderateScale(14)}px;
  line-height: ${moderateScale(22)}px;
`;

const subtitle: ComponentType<any> = styled(h1)`
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${moderateScale(16)}px;
  line-height: ${moderateScale(19)}px;
`;

const buttontext: ComponentType<any> = styled(subtitle)`
  font-weight: 700;
  letter-spacing: ${moderateScale(1.0125)}px;
`;

const buttonlink: ComponentType<any> = styled(h1)`
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${moderateScale(14)}px;
  line-height: ${moderateScale(17)}px;
  letter-spacing: ${moderateScale(1.0125)}px;
`;

const link: ComponentType<any> = styled(h1)`
  font-family: Myriad Pro;
  font-weight: bold;
  font-size: ${moderateScale(14)}px;
  line-height: ${moderateScale(17)}px;
  letter-spacing: ${moderateScale(1.0125)}px;
`;

const link1: ComponentType<any> = styled(h1)`
  font-family: Myriad Pro;
  font-weight: normal;
  font-size: ${moderateScale(14)}px;
  line-height: ${moderateScale(17)}px;
  letter-spacing: ${moderateScale(1.0125)}px;
`;

const caption: ComponentType<any> = styled(h1)`
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${moderateScale(12)}px;
  line-height: ${moderateScale(15)}px;
`;

const overline1: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${moderateScale(12)}px;
  line-height: ${moderateScale(14)}px;
  letter-spacing: ${moderateScale(1.846154)}px;
  text-transform: uppercase;
  color: ${(props) => getColor(props)};
`;

const overline2: ComponentType<any> = styled(overline1)`
  font-family: MyriadPro-Semibold;
  font-size: ${moderateScale(10)}px;
  line-height: ${moderateScale(12)}px;
`;

const listitem: ComponentType<any> = styled(h1)`
  font-weight: 400;
  font-size: ${moderateScale(16)}px;
  line-height: ${moderateScale(19.2)}px;
  letter-spacing: ${moderateScale(1.0125)}px;
`;

const inputerror: ComponentType<any> = styled(body2)`
  font-size: ${moderateScale(14)}px;
`;

const subBody: ComponentType<any> = styled(h1)`
  font-family: Myriad Pro;
  font-size: ${moderateScale(12)}px;
  line-height: ${moderateScale(15.1)}px;
`;

export default {
  h1,
  h2,
  h3,
  normal,
  normalSemibold,
  body1,
  body2,
  body3,
  body4,
  subtitle,
  buttontext,
  buttonlink,
  link,
  caption,
  overline1,
  overline2,
  custom,
  listitem,
  inputerror,
  link1,
  subBody,
};
