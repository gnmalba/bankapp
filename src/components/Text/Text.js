/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import Texts from './Styled';

const Text = (props: PropsType): React$Node => {
  const {mode, nativeProps, children, ...rest} = props;
  return (
    <>
      {React.createElement(
        Texts[mode || 'body2'],
        {...nativeProps, ...rest},
        children,
      )}
    </>
  );
};

export default Text;
