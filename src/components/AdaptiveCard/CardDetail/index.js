/**
 * @format
 * @flow
 */

import React from 'react';
import styled from 'styled-components/native';

import Text from '@components/Text';
import type {CardDetailProps} from '../types';
import type {ComponentType} from 'react';

const Container: ComponentType<any> = styled.View`
  align-items: flex-end;
`;

const renderLabel = (props) => {
  const {label, labelColor} = props;
  if (label) {
    return (
      <Text mode="overline2" themeColor={labelColor}>
        {label}
      </Text>
    );
  }
  return <></>;
};

const renderValue = (props) => {
  const {value, valueColor} = props;
  if (value) {
    return (
      <Text mode="body1" themeColor={valueColor}>
        {value}
      </Text>
    );
  }
  return <></>;
};

const CardDetail = (props: CardDetailProps): React$Node => {
  return (
    <Container>
      {renderLabel(props)}
      {renderValue(props)}
    </Container>
  );
};

export default CardDetail;
