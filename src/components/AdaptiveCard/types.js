/**
 * @flow
 */

export type CardProps = {
  testID?: string,
  accessibilityLabel?: string,
  children: React$Node,
  onPress?: () => any,
};

export type CardTitleProps = {
  testID?: string,
  accessibilityLabel?: string,
  avatarSource?: string,
  title?: string,
  titleColor?: string,
  subtitle?: string,
  subtitleColor?: string,
  showAvatar?: boolean,
  showButton?: boolean,
};

export type CardDetailProps = {
  testID?: string,
  accessibilityLabel?: string,
  label?: string,
  labelColor?: string,
  value?: string,
  valueColor?: string,
};
