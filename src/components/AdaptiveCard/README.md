## AdaptiveCard

AdaptiveCard component.

## Basic Usage

Import the components like so:

```javascript
import {Card, CardTitle, CardDetail} from '@components/AdaptiveCard';
```

Then insert the card in your code:

```js
<Card>
  <CardTitle
    showAvatar
    showButton
    theme={theme}
    title="John Doe"
    subtitle="1111********4444"
    subtitleColor="TEXT_100_HIGH"
    avatarSource={'http://placehold.it/480x270'}
  />
  <CardDetail
    theme={theme}
    label={label}
    labelColor="TEXT_100_HIGH"
    value={value}
  />
</Card>
```

## Card Component Options

| Prop                 | Type     | Effect                      | Default Value |
| -------------------- | -------- | --------------------------- | ------------- |
| `testID`             | `string` | The testID prop             | `undefined`   |
| `accessibilityLabel` | `string` | The accessibilityLabel prop | `undefined`   |

## CardTitle Component Options

| Prop                 | Type      | Effect                                                        | Default Value |
| -------------------- | --------- | ------------------------------------------------------------- | ------------- |
| `testID`             | `string`  | The testID prop                                               | `undefined`   |
| `accessibilityLabel` | `string`  | The accessibilityLabel prop                                   | `undefined`   |
| `title`              | `string`  | The title text                                                | `undefined`   |
| `subtitle`           | `string`  | The subtitle text                                             | `undefined`   |
| `showAvatar`         | `boolean` | Whether the avatar should be shown                            | `false`       |
| `avatarSource`       | `object`  | The avatar image to be shown, passed to Image's `source` prop | `undefined`   |
| `titleColor`         | `string`  | gets color from theme.color                                   | `undefined`   |
| `subtitleColor`      | `object`  | gets color from theme.color                                   | `undefined`   |
| `showButton`         | `boolean` | Whether the custom button should be shown                     | `false`       |

## CardDetail Component Options

| Prop         | Type     | Effect                      | Default Value   |
| ------------ | -------- | --------------------------- | --------------- |
| `label`      | `string` | The label text              | `undefined`     |
| `value`      | `string` | The value text              | `undefined`     |
| `labelColor` | `string` | gets color from theme.color | `TEXT_100_HIGH` |
| `valueColor` | `object` | gets color from theme.color | `TEXT_100_HIGH` |
