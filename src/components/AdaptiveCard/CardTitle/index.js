/**
 * @format
 * @flow
 */

import React from 'react';
import styled from 'styled-components/native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Avatar from '@components/Avatar';
import Text from '@components/Text';
import type {CardTitleProps} from '../types';
import type {ComponentType} from 'react';

const Container: ComponentType<any> = styled.View`
  flex-direction: row;
  margin-bottom: 16;
`;

const DetailsContainer: ComponentType<any> = styled.View`
  ${(props) => props.showAvatar && 'margin-left: 12px;'}
  flex: 1;
`;

const CardTitleButton: ComponentType<any> = styled.TouchableOpacity`
  position: absolute;
  align-self: flex-end;
  right: -10;
  justify-content: center;
`;

const CardTitleIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    ...props,
    name: 'keyboard-arrow-right',
    size: 36,
    color: props.theme?.text.textEmphasis,
  }),
)``;

const renderCardTitleButton = (props) => {
  const {showButton} = props;
  if (showButton) {
    return (
      <CardTitleButton>
        <CardTitleIcon {...props} />
      </CardTitleButton>
    );
  }
};
const renderAvatar = (props) => {
  const {showAvatar = false} = props;
  if (showAvatar) {
    return <Avatar {...props} />;
  }
  return <></>;
};

const renderTitle = (props) => {
  const {title, titleColor} = props;
  if (title) {
    return (
      <Text mode="body1" themeColor={titleColor}>
        {title}
      </Text>
    );
  }
  return <></>;
};

const renderSubtitle = (props) => {
  const {subtitle, subtitleColor} = props;
  if (subtitle) {
    return (
      <Text mode="overline2" themeColor={subtitleColor}>
        {subtitle}
      </Text>
    );
  }
  return <></>;
};

const CardTitle = (props: CardTitleProps): React$Node => {
  return (
    <Container>
      {renderAvatar(props)}
      <DetailsContainer {...props}>
        {renderTitle(props)}
        {renderSubtitle(props)}
        {renderCardTitleButton(props)}
      </DetailsContainer>
    </Container>
  );
};

export default CardTitle;
