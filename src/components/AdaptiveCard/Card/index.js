/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity} from 'react-native';

import type {CardProps} from '../types';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

const Container: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: 8px;
  padding: 16px;
  margin-bottom: 16px;
  box-shadow: 4px 4px 4px rgba(0, 0, 0, 0.4);
  elevation: 4px;
`;

const Card = (props: CardProps): React$Node => {
  const {onPress} = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <Container>{props.children}</Container>
    </TouchableOpacity>
  );
};

export default Card;
