import React from 'react';
import {render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';

import {Card, CardDetail, CardTitle} from '@components/AdaptiveCard';
import {DARK_THEME, LIGHT_THEME} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

const SAMPLE_TITLE = 'this is a title';
const SAMPLE_SUB = 'subtitle';
const SAMPLE_LABEL = 'this is a label';
const SAMPLE_VALUE = 'this is a value';
describe('AdaptiveCard', () => {
  it.each([
    {
      title: SAMPLE_TITLE,
      subtitle: SAMPLE_SUB,
      theme: LIGHT_THEME,
      showAvatar: true,
      showButton: true,
    },
    {
      title: SAMPLE_TITLE,
      subtitle: SAMPLE_SUB,
      theme: DARK_THEME,
      showAvatar: true,
      showButton: false,
    },
    {
      title: SAMPLE_TITLE,
      subtitle: SAMPLE_SUB,
      theme: DARK_THEME,
      showAvatar: false,
      showButton: true,
    },
    {
      title: SAMPLE_TITLE,
      subtitle: SAMPLE_SUB,
      theme: LIGHT_THEME,
      showAvatar: false,
      showButton: false,
    },
    {
      title: undefined,
      subtitle: undefined,
      theme: LIGHT_THEME,
      showAvatar: false,
      showButton: false,
    },
    {
      value: SAMPLE_VALUE,
      label: SAMPLE_LABEL,
      theme: LIGHT_THEME,
      showAvatar: false,
      showButton: false,
    },
    {
      value: undefined,
      label: undefined,
      theme: LIGHT_THEME,
      showAvatar: false,
      showButton: false,
    },
    {
      title: SAMPLE_TITLE,
      subtitle: SAMPLE_SUB,
      value: SAMPLE_VALUE,
      label: SAMPLE_LABEL,
      theme: LIGHT_THEME,
      showAvatar: true,
      showButton: true,
    },
  ])('Should work as expected', (params) => {
    const all = render(
      <ThemeProvider theme={params.theme}>
        <Card {...testProps('cardTestId')}>
          <CardTitle
            showAvatar={params.showAvatar}
            title={params.title}
            subtitle={params.subtitle}
            showButton
          />
          <CardTitle
            showAvatar={params.showAvatar}
            title={params.title}
            subtitle={params.subtitle}
            subtitleColor="TEXT_100_HIGH"
            showButton
          />
          <CardDetail label={params.label} value={params.value} />
          <CardDetail
            label={params.label}
            labelColor="TEXT_100_HIGH"
            value={params.value}
          />
        </Card>
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
