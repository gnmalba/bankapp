/**
 * @flow
 */

const TRANSFER_SERVICE = Object.freeze({
  INSTAPAY: 'INSTAPAY',
  PESONET: 'PESONET',
});

export default TRANSFER_SERVICE;
