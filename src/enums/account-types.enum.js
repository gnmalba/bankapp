// @flow

export const ACCOUNT_TYPES = Object.freeze({
  DEPOSIT: 0,
  MB_PREPAID: 1,
  CC: 2,
  YAZZ: 3,
});

export default ACCOUNT_TYPES;
