## Modal
Modal component.

# Usage
```js
import Modal from '@composition/Modal';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
SampleProp                | boolean               | false                   | This is only a sample
