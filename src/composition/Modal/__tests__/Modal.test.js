import React from 'react';
import {render} from '@testing-library/react-native';
import Modal from '../Modal';

jest.mock('react-native', () => jest.requireActual('react-native'));

describe('Modal', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<Modal />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
