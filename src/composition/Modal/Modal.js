/**
 * @format
 * @flow
 */

import React from 'react';
import {testProps} from '@utils/componentCustomProps';

import type {PropsType} from './types';
import * as S from './Styled';

const Modal = (props: PropsType): React$Node => {
  const {
    visible,
    children,
    isFullWidth,
    onBackdropPress,
    testID,
    handleRequestClose,
    customStyle = {},
  } = props;
  return (
    <S.ModalContainer
      isVisible={visible}
      isFullWidth={isFullWidth}
      onBackdropPress={onBackdropPress}
      onRequestClose={handleRequestClose}
      customStyle={customStyle}
      {...props}
      {...testProps(testID || 'modalContainerTestId')}>
      <S.ModalContentContainer>{children}</S.ModalContentContainer>
    </S.ModalContainer>
  );
};

export default Modal;
