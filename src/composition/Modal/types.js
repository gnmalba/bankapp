// @flow

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  children?: React$Node,
  visible?: boolean,
  isFullWidth?: boolean,
  onBackdropPress?: any,
  handleRequestClose?: () => void,
  customStyle?: Object,
};
