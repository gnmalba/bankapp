/**
 * @flow
 */

import styled from 'styled-components/native';
import {Overlay} from 'react-native-elements';
import type {ComponentType} from 'react';
import {moderateScale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';

const PHONE_MODAL_WIDTH = '80%';
const TABLET_MODAL_WIDTH = '60%';
const modalWidth = IS_DEVICE_TABLET ? TABLET_MODAL_WIDTH : PHONE_MODAL_WIDTH;
export const ModalContainer: ComponentType<any> = styled(Overlay).attrs(
  (props) => ({
    overlayStyle: {
      width: props.isFullWidth ? '100%' : modalWidth,
      padding: 0,
      borderRadius: moderateScale(8),
      backgroundColor: props.theme?.modal?.backgroundColor || 'transparent',
      alignItems: 'center',
      elevation: 10,
      ...props.customStyle,
    },
  }),
)``;

export const ModalContentContainer: ComponentType<any> = styled.View`
  padding-top: ${moderateScale(32)}px;
  padding-bottom: ${moderateScale(30)}px;
  padding-left: ${moderateScale(32)}px;
  padding-right: ${moderateScale(32)}px;
  width: 100%;
`;
