## TransactionHistory
TransactionHistory component.

# Usage
```js
import TransactionHistory from '@composition/TransactionHistory';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
