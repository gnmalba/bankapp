import React from 'react';
import {render} from '@testing-library/react-native';
import TransactionHistory from '../TransactionHistory';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import {creditSoaSuppleLoadingStates} from '../LoadingStates';
import Spinner from '@composition/Loader/Spinner';
import Text from '@components/Text';
import * as S from '../Styled';
import {cardCount, actionSearch} from '../actions';
const data = {
  cardStatus: 'test',
};

describe('TransactionHistory', () => {
  const NO_RESULTS_FOUND = 'No results found.';
  const SPINNER_LOADER = (
    <S.LoaderContainer>
      <Spinner isVisible={true} />
    </S.LoaderContainer>
  );
  const SOA_NO_RESULTS_MESSAGE = (
    <S.LoaderContainer>
      <Text mode="h3">{NO_RESULTS_FOUND}</Text>
      <S.LoaderText mode="h3">
        Try different keywords or remove search filters.
      </S.LoaderText>
    </S.LoaderContainer>
  );
  const NO_RESULTS_MESSAGE = (
    <S.LoaderContainer>
      <Text mode="normalSemibold">{NO_RESULTS_FOUND}</Text>
      <S.LoaderText mode="h3">
        Try different keywords or remove search filters.
      </S.LoaderText>
    </S.LoaderContainer>
  );
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    render(
      <MockProvider store={{}}>
        <ThemeProvider theme={DARK_THEME}>
          <TransactionHistory data={data} theme={DARK_THEME} />
        </ThemeProvider>
      </MockProvider>,
    );
  });
  it('Testing Loading States creditSoaSuppleLoadingStates true', () => {
    // Edit the args inside render and provide props.

    let expected = SPINNER_LOADER;
    let datas = creditSoaSuppleLoadingStates(
      [],
      [],
      {loading: true},
      true,
      1,
      true,
      'yazz',
    );
    expect(datas).toEqual(expected);
  });
  it('Testing Loading States creditSoaSuppleLoadingStates false', () => {
    // Edit the args inside render and provide props.

    let expected = SOA_NO_RESULTS_MESSAGE;
    let datas = creditSoaSuppleLoadingStates(
      [],
      [],
      {loading: false},
      true,
      1,
      true,
      'yazz',
    );
    expect(datas).toEqual(expected);
  });
  it('Testing Loading States creditSoaSuppleLoadingStates false creditsoa false', () => {
    // Edit the args inside render and provide props.

    let expected = NO_RESULTS_MESSAGE;
    let datas = creditSoaSuppleLoadingStates(
      [],
      [],
      {loading: false},
      true,
      1,
      false,
      'yazz',
    );
    expect(datas).toEqual(expected);
  });

  it('Shoul work on Card Count', () => {
    // Edit the args inside render and provide props.
    let expected = 10;
    let datas = cardCount('credit', [], 1);
    expect(datas).toEqual(expected);
  });

  it('Shoul work on actionSearch', () => {
    // Edit the args inside render and provide props.

    let datas = actionSearch(
      '',
      '',
      true,
      false,
      () => {},
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      {},
    );
    expect(datas).toEqual(undefined);
  });
});
