/**
 * @flow
 */
import type {Account} from '@ducks/deposit/types';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  index?: number,
  cardType: string,
  data: {...Account, cardStatus: 'ACTV'} | Array<any>,
  transaction: TransactionType | Object,
  titleTransaction: string,
  overviewData?: Object,
  creditSupplementary?: boolean,
  accountId?: string | null,
  primaryId?: string,
  id?: string,
  creditSOA?: boolean,
  billingDate?: string,
  generatedDate?: string,
  soaAccountId?: string,
  unfilteredData?: Object,
  currencyCode?: string,
  theme?: any,
  disableFilterButton?: boolean,
  currentTab?: string,
  refreshTime?: number,
};

type TransactionType = {
  data: TransactionDataType,
  error: boolean,
  loading: boolean,
};

type TransactionDataType = {
  customerId: string,
  sequenceNo: string,
  totalRecords: number,
  totalRecordsReturned: number,
  transactionHistory: Array<any>,
};

export type actionSearchTypes = {
  accountId?: string | null,
  billingDate?: string,
  creditSOA?: boolean,
  creditSupplementary?: boolean,
  generatedDate?: string,
  id?: string,
  primaryId?: string,
  soaAccountId?: string,
  cardType: string,
};
