/* eslint-disable sonarjs/cognitive-complexity */

/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

import type {PropsType} from './types';
import * as S from './Styled';

import SearchBox from '@composition/SearchBox';
import RenderItem from '@composition/TransactionHistoryList';
import FilterTransaction from '@composition/FilterTransaction';

import {useDispatch} from 'react-redux';
import {actionSearch, cardCount} from './actions';
import {testProps} from '@utils/componentCustomProps';
import {
  creditSoaSuppleLoadingStates,
  despositPrepaidLoadingStates,
} from './LoadingStates';
import {Keyboard} from 'react-native';

const TransactionHistory = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const {
    cardType,
    transaction,
    titleTransaction,
    data,
    overviewData,
    creditSupplementary,
    accountId,
    primaryId,
    id,
    billingDate,
    generatedDate,
    soaAccountId,
    creditSOA,
    unfilteredData,
    currencyCode,
    theme,
    disableFilterButton,
    currentTab,
    refreshTime,
  } = props;

  const [modalVisible, setModalVisible] = React.useState(false);
  const [showmore, setShowmore] = React.useState(
    cardCount(cardType, transaction?.data?.transactionHistory || [], 1),
  );
  const [currentPage, setCurrentPage] = React.useState(1);
  const [value, setValue] = React.useState('');
  const [numberOfFilters, setNumberOfFilters] = React.useState(0);
  const [runningBalance, setRunningBalance] = React.useState(true);
  const [initialFilters, setInitialFilters] = React.useState({});

  const overviewCurrency = overviewData ? overviewData[0].currency : 'PHP';

  const isSearching = value.length > 0 ? true : false;
  const handleCloseModal = () => setModalVisible(false);

  const noDataLoader = () =>
    cardType === 'credit' || cardType === 'yazz'
      ? creditSoaSuppleLoadingStates(
          data,
          unfilteredData,
          transaction,
          isSearching,
          numberOfFilters,
          creditSOA,
          cardType,
        )
      : despositPrepaidLoadingStates(
          data,
          unfilteredData,
          transaction,
          isSearching,
          numberOfFilters,
        );

  useEffect(() => {
    setShowmore(
      cardCount(
        cardType,
        transaction?.data?.transactionHistory || [],
        currentPage,
      ),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [transaction, currentPage]);

  useEffect(() => {
    setInitialFilters({});
    setNumberOfFilters(0);
    setValue('');
  }, [refreshTime]);

  useEffect(() => {
    actionSearch(
      accountId,
      billingDate,
      creditSOA,
      creditSupplementary,
      dispatch,
      generatedDate,
      id,
      primaryId,
      soaAccountId,
      value,
      cardType,
      currentTab,
      initialFilters,
    );
  }, [
    accountId,
    billingDate,
    creditSOA,
    creditSupplementary,
    dispatch,
    generatedDate,
    id,
    primaryId,
    soaAccountId,
    value,
    cardType,
    currentTab,
    initialFilters,
  ]);
  useEffect(() => {
    if (transaction?.loading === false && cardType === 'credit') {
      Keyboard.dismiss();
    }
  }, [transaction, cardType]);

  return (
    <S.TransactionContainer>
      <S.TrsansactionView>
        <S.TransactionText {...testProps('transactHistLabelTestId')}>
          {titleTransaction}
        </S.TransactionText>
        <S.TransactionDivider {...testProps('transactHistDivider')} />
      </S.TrsansactionView>
      <S.PaddedLayout>
        <S.SearchFilterContainer>
          <S.SearchContainer
            {...testProps('transactionHistorySearchBoxContainer')}>
            <SearchBox
              currentTab={currentTab}
              creditSOA={creditSOA}
              placeholder={
                cardType === 'credit' ? 'Search description' : 'Search'
              }
              onChange={(text) => {
                setValue(text);
              }}
              value={value}
              setValue={setValue}
              maxLength={200}
            />
          </S.SearchContainer>
          <S.HistoryTouch
            {...testProps('transactionHistoryFilterTestId')}
            onPress={() =>
              cardType === 'yazz' && disableFilterButton
                ? null
                : setModalVisible(true)
            }>
            <S.FilterContainer>
              <S.FilterIcon {...testProps('transactionHistoryFilterIcon')} />
              <S.FilterText
                {...testProps('transactionHistoryFilterLabelTestId')}>
                Filter
              </S.FilterText>
              {numberOfFilters > 0 && (
                <S.NumberOfFilterContainer>
                  <S.NumberOfFilterText
                    {...testProps('transactionHistoryActivatedTestId')}>
                    {numberOfFilters}
                  </S.NumberOfFilterText>
                </S.NumberOfFilterContainer>
              )}
              <FilterTransaction
                modalVisible={modalVisible}
                handleCloseModal={handleCloseModal}
                cardType={cardType}
                overviewData={overviewData}
                creditSupplementary={creditSupplementary}
                accountId={accountId}
                primaryId={primaryId}
                id={id}
                billingDate={billingDate}
                generatedDate={generatedDate}
                soaAccountId={soaAccountId}
                creditSOA={true}
                setNumberOfFilters={setNumberOfFilters}
                numberOfFilters={numberOfFilters}
                currencyCode={currencyCode}
                setValue={setValue}
                setRunningBalance={setRunningBalance}
                runningBalance={runningBalance}
                setModalVisible={setModalVisible}
                theme={theme}
                currentTab={currentTab}
                value={value}
                setInitialFilters={setInitialFilters}
                refreshTime={refreshTime}
                setCurrentPage={setCurrentPage}
              />
            </S.FilterContainer>
          </S.HistoryTouch>
        </S.SearchFilterContainer>
      </S.PaddedLayout>

      <S.HistoryContainer>
        <S.HistoryFlatList
          data={(transaction?.data?.transactionHistory || []).slice(
            0,
            showmore,
          )}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          renderItem={(listProps) => (
            <RenderItem
              item={listProps.item}
              index={listProps.index}
              cardType={cardType}
              runningBalance={runningBalance}
              overviewCurrency={overviewCurrency}
              creditSupplementary={creditSupplementary}
            />
          )}
          keyExtractor={(item, index) => `${item.transactionDate}__${index}`}
          ListEmptyComponent={() => noDataLoader()}
        />
        {transaction?.data?.transactionHistory &&
          transaction?.data?.transactionHistory.length > 10 &&
          transaction?.data?.transactionHistory.length > showmore && (
            <S.ShowMoreContainer>
              <S.ShowMoreTouch
                onPress={() => {
                  cardType === 'credit'
                    ? setCurrentPage(currentPage + 1)
                    : setShowmore(showmore + 10);
                }}
                {...testProps('transactionHistoryShowMoreButton')}>
                <S.ShowMoreText
                  {...testProps('transactionHistoryShowMoreButtonText')}>
                  Show more
                </S.ShowMoreText>
                <S.ShowMoreIcon
                  {...testProps('transactionHistoryShowMoreButtonIcon')}
                />
              </S.ShowMoreTouch>
            </S.ShowMoreContainer>
          )}
      </S.HistoryContainer>
    </S.TransactionContainer>
  );
};

export default TransactionHistory;
