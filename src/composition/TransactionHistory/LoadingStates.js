/**
 * @format
 * @flow
 */

import React from 'react';
import * as S from './Styled';
import Text from '@components/Text';
import Spinner from '@composition/Loader/Spinner';
import {CARD_STATUS} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

const NO_RESULTS_FOUND = 'No results found.';

const NO_RECENT_MESSAGE = (
  <S.LoaderContainer>
    <S.LoaderText noTransactions={true} {...testProps('noRecentTransactions')}>
      There are no recent{'\n'}transactions
    </S.LoaderText>
  </S.LoaderContainer>
);

const SPINNER_LOADER = (
  <S.LoaderContainer>
    <Spinner isVisible={true} />
  </S.LoaderContainer>
);

const CANNOT_DISPLAYED_MESSAGE = (
  <S.LoaderContainer>
    <Text mode="h3">Cannot be displayed</Text>
    <S.LoaderText mode="h3">
      We cannot show the transaction history{'\n'}
      because there is an issue on your account.
    </S.LoaderText>
  </S.LoaderContainer>
);

const NO_RESULTS_MESSAGE = (
  <S.LoaderContainer>
    <Text mode="normalSemibold">{NO_RESULTS_FOUND}</Text>
    <S.LoaderText mode="h3">
      Try different keywords or remove search filters.
    </S.LoaderText>
  </S.LoaderContainer>
);

const SOA_NO_RESULTS_MESSAGE = (
  <S.LoaderContainer>
    <Text mode="h3">{NO_RESULTS_FOUND}</Text>
    <S.LoaderText mode="h3">
      Try different keywords or remove search filters.
    </S.LoaderText>
  </S.LoaderContainer>
);

export const creditSoaSuppleLoadingStates = (
  data: Object,
  unfilteredData: Object,
  transaction: Object,
  isSearching: boolean,
  numberOfFilters: number,
  creditSOA?: boolean,
  cardType?: string,
): any => {
  if (transaction.loading) {
    return SPINNER_LOADER;
  } else {
    if ((isSearching || numberOfFilters) && creditSOA) {
      return SOA_NO_RESULTS_MESSAGE;
    } else if (isSearching || numberOfFilters) {
      return NO_RESULTS_MESSAGE;
    } else {
      return NO_RECENT_MESSAGE;
    }
  }
};

export const despositPrepaidLoadingStates = (
  data: Object,
  unfilteredData: Object,
  transaction: Object,
  isSearching: boolean,
  numberOfFilters: number,
): any => {
  if (data?.active || data?.cardStatus === CARD_STATUS.ACTIVE) {
    if (isSearching || numberOfFilters || unfilteredData?.totalRecords > 0) {
      return NO_RESULTS_MESSAGE;
    } else {
      return NO_RECENT_MESSAGE;
    }
  } else {
    return CANNOT_DISPLAYED_MESSAGE;
  }
};
