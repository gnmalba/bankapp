/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale, scale, moderateScale} from '@utils/scale';
import {
  Image,
  FlatList as RNFlatList,
  TouchableOpacity,
  Platform,
} from 'react-native';
import {Divider as RNEDivider} from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {IS_DEVICE_TABLET} from '@utils/constants';

export const TransactionContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(30)}px;
  padding-bottom: ${verticalScale(20)}px;
`;

export const PaddedLayout: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
`;

export const TransactionDivider: ComponentType<any> = styled.View`
  border: ${(props) => props.theme.transaction.dividerColor};
  border-width: 1px;
  margin-top: ${verticalScale(8)}px;
`;

export const TrsansactionView: ComponentType<any> = styled.View`
  margin-horizontal: ${scale(24)}px;
`;

export const TransactionText: ComponentType<any> = styled.Text`
  font-family: MyriadPro-Semibold;
  font-weight: 600;
  font-size: ${scale(20)}px;
  line-height: ${scale(24)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const SearchContainer: ComponentType<any> = styled.View`
  width: ${scale(260)}px;
  margin-top: ${verticalScale(16)}px;
`;

export const SearchFilterContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  padding-bottom: ${verticalScale(10)}px;
`;

export const FilterContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  margin-top: ${verticalScale(33)}px;
`;

export const FilterIcon: ComponentType<any> = styled(Image).attrs((props) => ({
  source: props.theme.FilterIcon,
  resizeMode: 'contain',
}))`
  width: ${scale(12)}px;
  height: ${verticalScale(12)}px;
  margin-horizontal: ${scale(10)}px;
`;

export const FilterText: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${scale(14)}px;
  line-height: ${scale(17)}px;
  letter-spacing: ${scale(0.8)}px;
  color: ${(props) => props.theme.tab.activeTintColor};
`;

export const HistoryContainer: ComponentType<any> = styled.View``;
export const HistoryFlatList: ComponentType<any> = styled(RNFlatList).attrs(
  (props) => ({
    ...Platform.select({
      ios: {
        alwaysBounceHorizontal: false,
        alwaysBounceVertical: false,
        bounces: false,
      },
      android: {
        overScrollMode: 'never',
      },
    }),
    contentContainerStyle: {
      marginTop: verticalScale(15),
      backgroundColor: 'transparent',
    },
  }),
)`
  width: 100%;
  align-self: center;
`;

export const HistoryDivider: ComponentType<any> = styled(RNEDivider)`
  background-color: transparent;
  height: ${scale(16)}px;
`;

export const HistoryTouch: ComponentType<any> = styled(TouchableOpacity).attrs(
  (props) => ({
    onPress: props.onPress,
    activeOpacity: 1,
  }),
)``;

export const ShowMoreContainer: ComponentType<any> = styled.View``;
export const ShowMoreTouch: ComponentType<any> = styled(TouchableOpacity).attrs(
  (props) => ({
    onPress: props.onPress,
    activeOpacity: 1,
  }),
)`
  margin-top: ${verticalScale(15)}px;
  align-items: center;
`;

export const ShowMoreIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'keyboard-arrow-down',
    size: 35,
  }),
)`
  color: #a2bddb;
`;

export const ShowMoreText: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(14)}px;
  line-height: ${scale(17)}px;
  text-align: center;
  letter-spacing: ${1}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const LoaderContainer: ComponentType<any> = styled.View`
  margin-vertical: ${IS_DEVICE_TABLET
    ? verticalScale(72)
    : verticalScale(110)}px;
  align-items: center;
`;

export const LoaderText: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${(props) =>
    props.noTransactions ? moderateScale(18) : moderateScale(14)}px;
  line-height: ${scale(17)}px;
  display: flex;
  align-items: center;
  text-align: center;
  margin-top: ${verticalScale(5)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const NumberOfFilterContainer: ComponentType<any> = styled.View`
  justify-content: center;
  width: ${scale(32)}px;
  height: ${scale(32)}px;
  margin-left: ${scale(4)}px;
  background-color: ${(props) =>
    props.theme.filterTransaction.numberOfFiltersCircle};
  border-radius: 100px;
  top: ${verticalScale(-8)}px;
`;

export const NumberOfFilterText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(14)}px;
  font-weight: bold;
  text-align: center;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;
