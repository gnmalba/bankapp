/**
 * @format
 * @flow
 */

import moment from 'moment';
import {isEmpty} from 'lodash';

/* eslint-disable sonarjs/cognitive-complexity */
export const actionSearch = (
  accountId?: string | null,
  billingDate?: string,
  creditSOA?: boolean,
  creditSupplementary?: boolean,
  dispatch: any,
  generatedDate?: string,
  id?: string,
  primaryId?: string,
  soaAccountId?: string,
  value?: string,
  cardType?: string,
  currentTab?: string,
  initialFilters?: Object,
): any => {
  if (cardType === 'deposit' || cardType === 'prepaid') {
    dispatch({
      type: 'FILTER_DEPOSIT_PREPAID_REQUEST',
      payload: {
        description: value,
      },
    });
  } else if (cardType === 'yazz') {
    dispatch({
      type: 'GET_YAZZ_TRANSACTION_HISTORY_REQUESTED',
      payload: {cardSerialNo: accountId, description: value},
    });
  } else {
    if (creditSupplementary) {
      const hasData = isEmpty(initialFilters);
      const defaultPayload = {
        cardSerialNo: primaryId,
        cardOwners: id,
        description: value,
      };
      const payload = hasData
        ? defaultPayload
        : {
            ...initialFilters,
            ...defaultPayload,
          };
      dispatch({
        type: 'REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST',
        payload,
      });
    } else if (creditSOA) {
      /* eslint-disable sonarjs/no-collapsible-if  */
      if (currentTab !== undefined) {
        if (currentTab === billingDate) {
          if (billingDate === 'Incoming') {
            const hasData = isEmpty(initialFilters);
            const defaultPayload = {
              cardSerialNo: soaAccountId,
              description: value,
            };
            const payload = hasData
              ? defaultPayload
              : {
                  ...initialFilters,
                  ...defaultPayload,
                };
            dispatch({
              type: 'REQUEST_SOA_TRANSACTION_API',
              payload,
              tab: 'incoming',
            });
          } else {
            const hasData = isEmpty(initialFilters);
            const defaultPayload = {
              date: moment(generatedDate).format('YYYY-MM-DD'),
              cardSerialNo: soaAccountId,
              description: value,
            };
            const payload = hasData
              ? defaultPayload
              : {
                  ...initialFilters,
                  ...defaultPayload,
                };
            dispatch({
              type: 'REQUEST_SOA_TRANSACTION_API',
              payload,
              tab: 'past',
            });
          }
        }
      }
    } else {
      const hasData = isEmpty(initialFilters);
      const payload = hasData
        ? {
            cardSerialNo: accountId,
            description: value,
          }
        : {
            ...initialFilters,
            description: value,
          };

      return dispatch({
        type: 'REQUEST_CREDIT_TRANSACTION_CURRENT_LIST',
        payload: payload,
      });
    }
  }
};

export const cardCount = (
  cardType: string,
  data: Object[],
  currentPage: number,
): any => {
  if (cardType === 'credit') {
    const nextEnd = currentPage * 10;
    const initialData = data.slice(0, nextEnd);
    const filteredData = initialData.filter(
      (item, index) => item.title === undefined,
    );
    let transToAdd = initialData.length - filteredData.length;
    while (
      data.length > nextEnd + transToAdd &&
      data
        .slice(0, nextEnd + transToAdd)
        .filter((item) => item.title === undefined).length < nextEnd
    ) {
      transToAdd++;
    }
    return nextEnd + transToAdd;
  } else {
    return 10;
  }
};
