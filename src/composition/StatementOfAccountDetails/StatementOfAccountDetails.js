/* eslint-disable sonarjs/cognitive-complexity */
/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import Text from '@components/Text';
import Button from '@components/Button';
import Layout from '@components/Layout';
import moment from 'moment';
import TransactionHistory from '@composition/TransactionHistory';

import {useSelector, useDispatch} from 'react-redux';

import {testProps} from '@utils/componentCustomProps';
import {creditStateSelector} from '@ducks/credit/selectors';
import {getCardToDispatch} from '@utils/installments';
import {updateBalanceConversionFrom} from '@ducks/installments/actions';

const StatementOfAccountDetails = (props: PropsType): React$Node => {
  const {item, accountId, route, navigation, showButtons, currentTab} = props;

  const overviewData = route?.params?.overviewData;
  const currency = overviewData ? overviewData[0]?.currency : '';

  const dispatch = useDispatch();
  const soaTransaction = useSelector((state) => state.soa.soaTransaction);
  const creditState = useSelector(creditStateSelector);

  const {
    totalAmountDue,
    minimumAmountDue,
    lastPayment,
    lastPaymentDate,
    dueDate,
    billingDate,
    generatedDate,
  } = item;

  const canConvert = () => {
    if (Array.isArray(overviewData) && overviewData.length > 0) {
      const obj = overviewData[0];
      const balanceConversionShortcut = obj.shortcuts.find(
        (s) => s.code === 'BALANCE CONVERSION',
      );

      if (balanceConversionShortcut) {
        return balanceConversionShortcut.enabled;
      }
    }

    return true;
  };

  const handleConvertToInstallments = () => {
    const currentCard = getCardToDispatch(
      creditState.creditList.data,
      creditState.creditDetails.data,
    );
    dispatch(updateBalanceConversionFrom(currentCard, false, false));
    navigation.navigate('Installments', {
      screen: 'BalanceConversionScreen',
      params: {fromSOA: true},
    });
  };

  /* eslint-disable sonarjs/no-collapsible-if */
  React.useEffect(() => {
    if (currentTab !== undefined) {
      if (currentTab === billingDate) {
        if (billingDate === 'Incoming') {
          dispatch({
            type: 'REQUEST_SOA_TRANSACTION_API',
            payload: {
              cardSerialNo: accountId,
            },
            tab: 'incoming',
          });
        } else {
          dispatch({
            type: 'REQUEST_SOA_TRANSACTION_API',
            payload: {
              date: moment(generatedDate).format('YYYY-MM-DD'),
              cardSerialNo: accountId,
            },
            tab: 'past',
          });
        }
      }
    }
  }, [billingDate, generatedDate, accountId, currentTab, dispatch]);

  const optionalButton = () => {
    return (
      <S.ButtonContainer>
        <Button
          state="default"
          title={'Pay credit card'}
          type="primary"
          size="large"
          onPress={() => {
            dispatch({
              type: 'SET_PAY_BILLS_TYPE',
              payload: true,
            });
          }}
          {...testProps('PayCreditCard')}
        />
        <S.Divider />
        {canConvert() && (
          <S.StyledConvertButton
            title="Convert to installments"
            onPress={handleConvertToInstallments}
          />
        )}
      </S.ButtonContainer>
    );
  };

  const optionalDetails = () => {
    return (
      <S.TextContainer>
        <S.TextContainerItem>
          <Text
            mode="overline1"
            themeColor="TEXT_100_HIGH"
            {...testProps('statementOfAccountMinimumAmountDueLabel')}>
            Minimum amount due
          </Text>
          <Text
            mode="body1"
            themeColor="TEXT_300_HIGH"
            {...testProps('statementOfAccountMinimumAmountDueValue')}>
            {currency} {minimumAmountDue}
          </Text>
          <S.Divider />
          <Text
            mode="overline1"
            themeColor="TEXT_100_HIGH"
            {...testProps('statementOfAccountLastPaymentLabel')}>
            last payment
          </Text>
          <Text
            mode="body1"
            themeColor="TEXT_300_HIGH"
            {...testProps('statementOfAccountLastPaymentValue')}>
            {lastPayment}
          </Text>
        </S.TextContainerItem>
        <S.DividerVertical />
        <S.TextContainerItem>
          <Text
            mode="overline1"
            themeColor="TEXT_100_HIGH"
            {...testProps('statementOfAccountDueDateLabel')}>
            due date
          </Text>
          <Text
            mode="body1"
            themeColor="TEXT_300_HIGH"
            {...testProps('statementOfAccountDueDateValue')}>
            {dueDate}
          </Text>
          <S.Divider />

          <Text
            mode="overline1"
            themeColor="TEXT_100_HIGH"
            {...testProps('statementOfAccountLastPaymentDateLabel')}>
            last payment date
          </Text>
          <Text
            mode="body1"
            themeColor="TEXT_300_HIGH"
            {...testProps('statementOfAccountLastPaymentDateValue')}>
            {lastPaymentDate}
          </Text>
        </S.TextContainerItem>
      </S.TextContainer>
    );
  };

  return (
    <Layout>
      <S.ScrollContainer>
        <S.MainContainer>
          <S.Container>
            <Text
              mode="overline1"
              themeColor="TEXT_100_HIGH"
              {...testProps('statementOfAccountTotalAmountDueLabel')}>
              Total amount due
            </Text>
            <Text
              mode="h2"
              {...testProps('statementOfAccountTotalAmountDueValue')}>
              {currency} {totalAmountDue}
            </Text>
          </S.Container>

          <S.DetailsContainer>
            {billingDate !== 'Incoming' && optionalDetails()}
            {showButtons && optionalButton()}
          </S.DetailsContainer>
        </S.MainContainer>

        <TransactionHistory
          cardType="credit"
          data={[]}
          transaction={soaTransaction}
          titleTransaction="Transaction History"
          overviewData={overviewData}
          billingDate={billingDate}
          generatedDate={generatedDate}
          currentTab={currentTab}
          soaAccountId={accountId}
          creditSOA={true}
        />
      </S.ScrollContainer>
    </Layout>
  );
};

export default StatementOfAccountDetails;
