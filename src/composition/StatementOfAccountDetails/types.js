/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  accountId: string,
  navigation: Object,
  item: {
    totalAmountDue: string,
    minimumAmountDue: string,
    lastPayment: string,
    lastPaymentDate: string,
    dueDate: string,
    billingDate: string,
    generatedDate: string,
  },
  route?: any,
  lastIndex?: any,
  showButtons?: boolean,
  currentTab?: string,
};
