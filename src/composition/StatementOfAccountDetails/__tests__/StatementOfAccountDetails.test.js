import React from 'react';
import {render} from '@testing-library/react-native';
import StatementOfAccountDetails from '../StatementOfAccountDetails';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import moment from 'moment';

jest.mock('@react-navigation/native', () => ({
  getFocusedRouteNameFromRoute: () => ({}),
}));

const mockStore = () => ({
  soa: {
    soaTransaction: {},
  },
});

const DATE_FORMAT = 'MM/DD/YYYY';

const item = {
  totalAmountDue: 0,
  minimumAmountDue: 0,
  lastPayment: 0,
  lastPaymentDate: moment('20210101').format(DATE_FORMAT),
  dueDate: moment('20210101').format(DATE_FORMAT),
  billingDate: moment('20210101').format(DATE_FORMAT),
  generatedDate: moment('20210101').format(DATE_FORMAT),
};

describe('StatementOfAccountDetails', () => {
  it('Should work as expected', () => {
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <StatementOfAccountDetails item={item} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
