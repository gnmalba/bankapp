## StatementOfAccountDetails
StatementOfAccountDetails component.

# Usage
```js
import StatementOfAccountDetails from '@composition/StatementOfAccountDetails';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
