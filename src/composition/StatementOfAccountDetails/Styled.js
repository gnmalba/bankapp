/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils//scale';
import {ScrollView} from 'react-native';
import Button from '@components/Button';

export const MainContainer: ComponentType<any> = styled.View`
  margin-horizontal: ${scale(24)}px;
`;

export const ScrollContainer: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    showsVerticalScrollIndicator: false,
    scrollEnabled: true,
    keyboardShouldPersistTaps: 'handled',
  }),
)``;

export const Container: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: ${verticalScale(40)}px;
`;

export const DetailsContainer: ComponentType<any> = styled.View``;

export const Divider: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(10)}px;
`;

export const TextContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: ${verticalScale(40)}px;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(30)}px;
`;

export const TextContainerItem: ComponentType<any> = styled.View`
  flex-direction: column;
`;

export const DividerVertical: ComponentType<any> = styled.View`
  width: ${scale(15)}px;
`;

export const StyledConvertButton: ComponentType<any> = styled(Button).attrs(
  (props) => ({
    type: 'primary',
    size: 'large',
    icon: require('@assets/icons/cycle-icon.png'),
    customIconColor: 'black',
    customBackgroundColor: 'white',
    titleCustomColor: 'black',
  }),
)``;
