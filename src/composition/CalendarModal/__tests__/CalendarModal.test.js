import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CalendarModal from '../CalendarModal';
import {DARK_THEME} from '@utils/constants';

describe('CalendarModal', () => {
  it.each(['calendarModalTestId', ''])('Should work as expected', (testID) => {
    // Edit the args inside render and provide props.
    const all = render(
      <CalendarModal
        calendarProps={{
          theme: DARK_THEME,
        }}
        handleOnApply={() => {}}
        handleOnCancel={() => {}}
      />,
    );
    if (testID) {
      const el1 = all.getByTestId('calendarModalApplyButton');
      fireEvent(el1, 'onPress');
      const el2 = all.getByTestId('calendarModalCancelButton');
      fireEvent(el2, 'onPress');
    }
    // expect(all.toJSON()).toMatchSnapshot();
  });
});
