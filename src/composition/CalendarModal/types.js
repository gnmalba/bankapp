/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  showVisible: boolean,
  disabledApply?: boolean,
  handleOnApply: Function,
  handleOnCancel: Function,
  calendarProps?: ?Object,
  theme?: Object,
};
