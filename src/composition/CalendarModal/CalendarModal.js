/* eslint-disable react-hooks/exhaustive-deps */
/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useEffect} from 'react';

// Third party libraries
import moment from 'moment';

// Components
import Button from '@components/Button';
import Calendar from '@components/Calendar';
import {ModalActionContainer, CancelButtonContainer} from './Styled';
import {testProps} from '@utils/componentCustomProps';

// Compositions
import Modal from '@composition/Modal';

// Types
import type {PropsType} from './types';

// Utils
import {DATE_FORMAT} from '@utils/constants';

const CalendarModal = (props: PropsType): React$Node => {
  const {
    showVisible,
    disabledApply,
    handleOnCancel,
    handleOnApply,
    calendarProps,
    testID,
  } = props;
  const [selectedDate, setSelectedDate] = useState(null);
  const [isVisible, setIsVisible] = useState(false);
  const [buttonState, setButtonState] = useState('default');

  useEffect(() => {
    setIsVisible(showVisible);
  }, [showVisible]);

  useEffect(() => {
    disabledApply ? setButtonState('disabled') : setButtonState('default');
  }, [disabledApply]);

  useEffect(() => {
    if (
      calendarProps?.selectedStartDate &&
      selectedDate ===
        moment(calendarProps?.selectedStartDate).format(DATE_FORMAT)
    ) {
      setButtonState('disabled');
    } else {
      setButtonState('default');
    }

    if (
      (calendarProps?.selectedStartDate == null && selectedDate == null) ||
      selectedDate == null
    ) {
      setButtonState('disabled');
    }
  }, [selectedDate]);

  const handleDateChange = (date) => {
    const dateParsed = moment(date).format(DATE_FORMAT);
    setSelectedDate(dateParsed);
  };

  const handleOnPressApply = () => {
    const dateParsed = moment(calendarProps?.selectedStartDate).format(
      DATE_FORMAT,
    );
    const date = selectedDate || dateParsed;
    handleOnApply(date);
  };

  return (
    <Modal visible={isVisible} isFullWidth>
      <Calendar
        calendarProps={{
          ...calendarProps,
          selectedStartDate: calendarProps?.selectedStartDate,
          onDateChange: handleDateChange,
        }}
      />
      <ModalActionContainer>
        <Button
          {...testProps((testID || 'calendarModal') + 'ApplyButton')}
          type="primary"
          state={buttonState}
          title="Apply"
          onPress={() => handleOnPressApply()}
        />
        <CancelButtonContainer>
          <Button
            {...testProps((testID || 'calendarModal') + 'CancelButton')}
            type="secondary"
            title="Cancel"
            titleThemeColor="TEXT_300_HIGH"
            onPress={() => handleOnCancel()}
          />
        </CancelButtonContainer>
      </ModalActionContainer>
    </Modal>
  );
};

export default CalendarModal;
