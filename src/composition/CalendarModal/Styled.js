/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View``;
export const ModalActionContainer: any = styled.View`
  margin-top: ${verticalScale(29)}px;
`;
export const CancelButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;
