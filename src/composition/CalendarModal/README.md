## CalendarModal
CalendarModal component.

# Usage
```js
import CalendarModal from '@composition/CalendarModal';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
