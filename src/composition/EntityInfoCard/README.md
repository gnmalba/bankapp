## EntityInfoCard

EntityInfoCard component.

## Basic Usage

Import the components like so:

```javascript
import EntityInfoCard from '@composition/EntityInfoCard';
```

Then insert the card in your code:

```js
<EntityInfoCard
  theme={theme}
  showAvatar
  title="test"
  subtitle="test"
  label="test"
  value="test"
  status="Pending"
/>
```

## EntityInfoCard Component Options

| Prop                 | Type       | Effect                                                        | Default Value   |
| -------------------- | ---------- | ------------------------------------------------------------- | --------------- |
| `testID`             | `string`   | The testID prop                                               | `undefined`     |
| `accessibilityLabel` | `string`   | The accessibilityLabel prop                                   | `undefined`     |
| `title`              | `string`   | The title text                                                | `undefined`     |
| `subtitle`           | `string`   | The subtitle text                                             | `undefined`     |
| `label`              | `string`   | The label text                                                | `undefined`     |
| `value`              | `string`   | The value text                                                | `undefined`     |
| `showAvatar`         | `boolean`  | Whether the avatar should be shown                            | `false`         |
| `avatarSource`       | `string`   | The avatar image to be shown, passed to Image's `source` prop | `undefined`     |
| `titleColor`         | `string`   | gets color from theme.color                                   | `undefined`     |
| `subtitleColor`      | `string`   | gets color from theme.color                                   | `TEXT_100_HIGH` |
| `labelColor`         | `string`   | gets color from theme.color                                   | `TEXT_100_HIGH` |
| `valueColor`         | `string`   | gets color from theme.color                                   | `TEXT_100_HIGH` |
| `showButton`         | `boolean`  | Whether the custom button should be shown                     | `false`         |
| `status`             | `string`   | if undefined, will render a transparent text as placeholder   | `undefined`     |
| `onPressCard`        | `function` | run a function upon pressing the card                         | `undefined`     |
