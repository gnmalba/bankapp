/**
 * @format
 * @flow
 */

import React from 'react';
import styled from 'styled-components/native';

import {Card, CardTitle, CardDetail} from '@components/AdaptiveCard';
import Text from '@components/Text';

import type {PropsType} from './types';
import type {ComponentType} from 'react';

const STATUS_COLORS = {
  completed: 'GREEN_STATUS',
  pending: 'YELLOW_STATUS',
  'in progress': 'YELLOW_STATUS',
  cancelled: 'ORANGE_STATUS',
  failed: 'RED_STATUS',
};

const DetailsContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const getColor = (status) => {
  if (status) {
    return STATUS_COLORS[status.toLowerCase()];
  }
  return 'TEXT_200_MEDIUIM';
};

const renderValueHelper = (status) => {
  if (status) {
    return (
      <Text mode="overline2" themeColor={getColor(status)}>
        {status}
      </Text>
    );
  }
  return (
    <Text mode="body1" customColor="transparent">
      {'empty'}
    </Text>
  );
};

const EntityInfoCard = (props: PropsType): React$Node => {
  const {showAvatar, status, onPressCard} = props;
  return (
    <Card onPress={onPressCard}>
      <CardTitle
        {...props}
        showAvatar={showAvatar}
        subtitleColor="TEXT_100_HIGH"
        showButton
      />
      <DetailsContainer {...props}>
        {renderValueHelper(status)}
        <CardDetail {...props} labelColor="TEXT_100_HIGH" />
      </DetailsContainer>
    </Card>
  );
};

export default EntityInfoCard;
