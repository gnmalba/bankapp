/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  title: string,
  subtitle: string,
  label: string,
  value: string,
  status?: 'pending' | 'completed' | 'in progress' | 'canceled' | 'failed',
  showAvatar?: boolean,
  onPressCard?: (any) => any,
};

export type EntityInfoPlaceholderProps = {
  testID?: string,
  accessibilityLabel?: string,
  showAvatar?: boolean,
};
