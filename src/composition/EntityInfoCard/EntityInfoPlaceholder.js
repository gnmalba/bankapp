/**
 * @format
 * @flow
 */

import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

import {Card} from '@components/AdaptiveCard';
import type {EntityInfoPlaceholderProps} from './types';

const renderPlaceholder = (settings, hide) =>
  (!hide && <SkeletonPlaceholder.Item {...settings} />) || <></>;

const EntityInfoPlaceholder = (
  props: EntityInfoPlaceholderProps,
): React$Node => {
  const {showAvatar} = props;
  return (
    <Card>
      <SkeletonPlaceholder
        backgroundColor={'#486788'}
        highlightColor={'#f5f5f5'}>
        <SkeletonPlaceholder.Item flexDirection="row">
          {renderPlaceholder(
            {width: 30, height: 30, marginRight: 12, borderRadius: 15},
            !showAvatar,
          )}
          <>
            {renderPlaceholder({width: 72, height: 12, margin: 2})}
            {renderPlaceholder({width: 120, height: 8, margin: 2})}
          </>
        </SkeletonPlaceholder.Item>
        {renderPlaceholder({
          width: 120,
          height: 8,
          margin: 2,
          alignSelf: 'flex-end',
        })}
        {renderPlaceholder({
          width: 72,
          height: 12,
          margin: 2,
          alignSelf: 'flex-end',
        })}
      </SkeletonPlaceholder>
    </Card>
  );
};

export default EntityInfoPlaceholder;
