import React from 'react';
import {render} from '@testing-library/react-native';
import EntityInfoCard from '../EntityInfoCard';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('EntityInfoCard', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <EntityInfoCard />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
