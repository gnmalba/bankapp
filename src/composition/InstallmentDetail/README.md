## InstallmentDetail
InstallmentDetail component.

# Usage
```js
import InstallmentDetail from '@composition/InstallmentDetail';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
