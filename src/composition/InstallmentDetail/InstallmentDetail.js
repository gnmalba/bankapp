/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {
  InstallmentLabel,
  InstallmentDetailContainer,
  InstallmentValue,
  InstallmentValueLabel,
} from './Styled';

const InstallmentDetail = (props: PropsType): React$Node => {
  const {
    label,
    value,
    subValue,
    small,
    testID = 'installmentDetail',
    alignItems,
    accessibilityLabel = 'installmentDetail',
  } = props;
  return (
    <InstallmentDetailContainer alignItems={alignItems}>
      <InstallmentLabel
        mode="overline2"
        small={small}
        testID={testID + 'Label'}
        accessibilityLabel={accessibilityLabel + 'Label'}>
        {label}
      </InstallmentLabel>
      <InstallmentValue>
        <InstallmentValueLabel
          testID={testID + 'Value'}
          accessibilityLabel={accessibilityLabel + 'Value'}>
          {value}
        </InstallmentValueLabel>
        {subValue && (
          <InstallmentValueLabel
            testID={testID + 'SuvValue'}
            accessibilityLabel={accessibilityLabel + 'SubValue'}>
            {subValue}
          </InstallmentValueLabel>
        )}
      </InstallmentValue>
    </InstallmentDetailContainer>
  );
};

export default InstallmentDetail;
