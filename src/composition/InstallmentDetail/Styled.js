/**
 * @flow
 */

import Text from '@components/Text';
import {moderateScale, verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View``;

export const InstallmentLabel: ComponentType<any> = styled(Text)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  letter-spacing: 0.85px;
  flex: 2
  margin-right: ${verticalScale(20)}px;
  font-size: ${(props) => moderateScale(props.small ? 10 : 12)}px
`;

export const InstallmentValueLabel: ComponentType<any> = styled(Text)`
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  letter-spacing: 0.85px;
  flex: 2;
  margin-right: ${verticalScale(10)}px;
  font-size: ${(props) => moderateScale(props.small ? 10 : 12)}px;
`;

export const InstallmentValue: ComponentType<any> = styled.View`
  margin-left: auto;
  flex: 4;
  align-items: flex-start;
`;

export const InstallmentDetailContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(8)}px;
  margin-bottom: ${verticalScale(8)}px;
  display: flex;
  flex-direction: row;
  align-items: ${(props) => (props?.alignItems ? props?.alignItems : 'center')};
`;
