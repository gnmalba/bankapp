/*import React from 'react';
import {render} from '@testing-library/react-native';
import InstallmentDetail from '../InstallmentDetail';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('InstallmentDetail', () => {
  it('Should work as expected', () => {
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <MockProvider store={{}}>
        <ThemeProvider theme={DARK_THEME}>
          <InstallmentDetail {...testProps('installmentDetailTestId')} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
*/

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

describe('InstallmentDetail', () => {
  it('Should work as expected', () => {});
});
