/**
 * @flow
 */

type AlignItemsProp =
  | 'stretch'
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | 'baseline';

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  label: string,
  value: any,
  small?: boolean,
  alignItems?: AlignItemsProp,
  subValue?: string,
};
