/**
 * @format
 * @flow
 */

import React, {useRef, useState} from 'react';

import type {PropsType} from './types';
import {
  Container,
  FilterCountContainer,
  FilterCount,
  PressableHolder,
  StyledText,
} from './Styled';
import IconTooltip from '@components/IconTooltip';
import {useTheme} from 'styled-components';
import WidgetTooltip from '@components/WidgetTooltip';
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';
import cardStyles from '@components/Card/styles';
import SortContent from './components/SortContent';
import {sortInstallmentsRequest} from '@ducks/installments/actions';
import Modal from '@composition/Modal';
import FilterModalContent from './components/FilterModalContent';
import {DARK_THEME} from '@utils/constants';

const FilterAndSortInstallments = (props: PropsType): React$Node => {
  const theme = useTheme();

  const {installmentType, creditList, numberOfFilters} = props;

  // eslint-disable-next-line no-unused-vars
  const [isTooltipButtonEnabled, setTooltipButtonEnabled] = useState(false);
  const sortRef = useRef();

  const [filterModalVisible, setFilterModalVisible] = useState(false);
  const [sortByModalVisible, setSortByModalVisible] = useState(false);

  const handleToggleSortTooltip = () => {
    if (sortRef.current) {
      sortRef.current.toggleTooltip();

      setTooltipButtonEnabled(true);
      setSortByModalVisible(true);
    }
  };

  const handleSortPress = () => {
    handleToggleSortTooltip();
  };

  const SortModalContent = (
    <SortContent
      dispatchAction={sortInstallmentsRequest}
      installmentType={installmentType}
      creditList={creditList}
    />
  );

  const SortTooltip = () => {
    return (
      <WidgetTooltip
        forwardedRef={sortRef}
        withOverlay
        width={verticalScale(265)}
        style={cardStyles.toolTipStyles}
        setTooltipButtonEnabled={setTooltipButtonEnabled}
        popover={SortModalContent}
        onToolTipClose={() => setSortByModalVisible(false)}
      />
    );
  };

  const modalCustomStyle = {marginBottom: verticalScale(-64)}; // Fix for the strange offset at the bottom of the modal
  const FilterModal = () => (
    <Modal
      customStyle={modalCustomStyle}
      isFullWidth
      visible={filterModalVisible}
      handleRequestClose={() => setFilterModalVisible(false)}>
      <FilterModalContent
        installmentType={installmentType}
        closeModal={() => setFilterModalVisible(false)}
      />
    </Modal>
  );

  return (
    <Container>
      <PressableHolder
        onPress={() => setFilterModalVisible(true)}
        {...testProps('cardInstallmentsFilterButton')}>
        <IconTooltip
          height={15}
          width={15}
          source={require('@assets/icons/filter.png')}
          color={
            theme
              ? theme.color.SECONDARY_DEFAULT
              : DARK_THEME.color.SECONDARY_DEFAULT
          }
          {...testProps('cardInstallmentsFilterButtonIcon')}
        />
        <StyledText
          mode="link"
          themeColor="SECONDARY_DEFAULT"
          {...testProps('cardInstallmentsFilterButtonLabel')}>
          Filter
        </StyledText>
        {numberOfFilters > 0 && (
          <FilterCountContainer>
            <FilterCount>{numberOfFilters}</FilterCount>
          </FilterCountContainer>
        )}
      </PressableHolder>
      <PressableHolder
        isPressed={sortByModalVisible}
        onPress={handleSortPress}
        {...testProps('cardInstallmentsSortByButton')}>
        <IconTooltip
          height={15}
          width={15}
          source={require('@assets/icons/Sort.png')}
          color={
            theme
              ? theme.color.SECONDARY_DEFAULT
              : DARK_THEME.color.SECONDARY_DEFAULT
          }
          {...testProps('cardInstallmentsSortByButtonIcon')}
        />
        <StyledText
          mode="link"
          themeColor="SECONDARY_DEFAULT"
          {...testProps('cardInstallmentsSortByLabel')}>
          Sort by
        </StyledText>
        {SortTooltip()}
      </PressableHolder>
      {FilterModal()}
    </Container>
  );
};

export default FilterAndSortInstallments;
