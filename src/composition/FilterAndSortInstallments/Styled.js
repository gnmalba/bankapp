/**
 * @flow
 */

import {scale, moderateScale, verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import {TouchableOpacity} from 'react-native';
import styled from 'styled-components/native';
import Text from '@components/Text';

export const StyledText: ComponentType<any> = styled(Text)`
  margin-left: 8px;
`;

export const Container: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  margin-left: auto;
  align-items:center; 
  margin-right: ${verticalScale(24)}px;
  margin-top: ${verticalScale(8)}px
  margin-bottom: ${verticalScale(8)}px
`;

export const PressableHolder: ComponentType<any> = styled(TouchableOpacity)`
  display: flex;
  flex-direction: row;
  margin-left: ${verticalScale(16)}px;
  align-items: center;
  background-color: ${(props) =>
    props.isPressed === true
      ? props.theme.color.PRIMARY_PRESSED
      : 'transparent'};
  padding: 10px;
  border-radius: 8px;
`;

export const ModalContainer: ComponentType<any> = styled.View`
  margin-bottom: ${() => verticalScale(-12)}px;
  padding-horizontal: 12px;
`;

export const FilterModalContainer: ComponentType<any> = styled.View``;

export const FilterModalHeader: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderItemContainer: ComponentType<any> = styled.View`
  flex: 2;
`;

export const HeaderTitleContainer: ComponentType<any> = styled.View`
  text-align: center;
  align-items: center;
  flex: 4;
`;

export const ClearAllContainer: ComponentType<any> = styled.View`
  align-self: flex-end;
`;

export const FilterCountContainer: ComponentType<any> = styled.View`
  background-color: ${(props) =>
    props.theme.filterTransaction.numberOfFiltersCircle};
  border-radius: ${scale(20)}px;
  margin-left: ${scale(4)}px;
  width: ${scale(28)}px;
  height: ${scale(28)}px;
  align-items: center;
  justify-content: center;
`;

export const FilterCount: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'caption',
}))`
  font-size: ${moderateScale(14)}px;
  padding-top: ${moderateScale(2)}px;
`;

export const AmountInputContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(-24)}px;
`;

export const StyledTouchableOpacity: ComponentType<any> = styled(
  TouchableOpacity,
)`
  width: 24px;
`;
