## FilterAndSortInstallments
FilterAndSortInstallments component.

# Usage
```js
import FilterAndSortInstallments from '@composition/FilterAndSortInstallments';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
