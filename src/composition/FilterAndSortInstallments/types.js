/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  installmentType?: 'BT' | 'BC' | 'C2GO' | '',
  creditList?: any[],
  numberOfFilters: number,
};
