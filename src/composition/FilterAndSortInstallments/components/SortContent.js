// @flow

import Checkbox from '@components/Checkbox';
import Separator from '@components/Separator';
import Text from '@components/Text';
import {installmentsSelector} from '@ducks/installments/selectors';
import {
  FILTER_CONSTANTS,
  getCardSerNoCreditState,
  SORT_OPTIONS,
} from '@utils/installments';
import React, {useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {ModalContainer} from '../Styled';

import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';

const SortContent = (props: any): React$Node => {
  const {dispatchAction, installmentType, creditList} = props;
  const installmentsState = useSelector(installmentsSelector);
  const {sortedBy} = installmentsState;

  const getInitialValue = () => {
    switch (installmentType) {
      case 'BT':
        return sortedBy.bt;
      case 'BC':
        return sortedBy.bc;
      case 'C2GO':
        return sortedBy.c2g;

      default:
        return 0;
    }
  };

  const dispatch = useDispatch();

  const [selectedValue, setSelectedValue] = useState(getInitialValue());

  const handlePress = (value, numValue) => {
    setSelectedValue(numValue);

    setTimeout(() => {
      dispatch(
        dispatchAction({
          installmentType,
          sortedBy: value,
          cardSerNo: getCardSerNoCreditState(creditList),
          minAmount: FILTER_CONSTANTS.MINIMUM_AMOUNT,
          maxAmount: FILTER_CONSTANTS.MAXIMUM_AMOUNT,
        }),
      );
    }, 0);
  };
  const checkboxCustomStyle = {marginBottom: verticalScale(8)};
  return (
    <ModalContainer>
      <Text mode="overline2" themeColor="TEXT_100_HIGH">
        sort by
      </Text>
      <Separator space={12} />

      {/* TODO: Just refactor and map this for consistency */}
      <Checkbox
        fontSize={14}
        title="Status"
        isCheckbox={false}
        onPress={() => handlePress(SORT_OPTIONS.status, 0)}
        checked={selectedValue === 0}
        {...testProps(`cardInstallmentsSortOption${0}LabelId`)}
        customStyle={checkboxCustomStyle}
      />
      <Checkbox
        fontSize={14}
        title="Date Applied: Latest to Oldest"
        isCheckbox={false}
        onPress={() => handlePress(SORT_OPTIONS.dateLatest, 1)}
        checked={selectedValue === 1}
        {...testProps(`cardInstallmentsSortOption${1}LabelId`)}
        customStyle={checkboxCustomStyle}
      />
      <Checkbox
        fontSize={14}
        title="Date Applied: Oldest to Latest"
        isCheckbox={false}
        onPress={() => handlePress(SORT_OPTIONS.dateOldest, 2)}
        checked={selectedValue === 2}
        {...testProps(`cardInstallmentsSortOption${2}LabelId`)}
        customStyle={checkboxCustomStyle}
      />
      <Checkbox
        fontSize={14}
        title="Amount: High to Low"
        isCheckbox={false}
        onPress={() => handlePress(SORT_OPTIONS.amountHigh, 3)}
        checked={selectedValue === 3}
        {...testProps(`cardInstallmentsSortOption${3}LabelId`)}
        customStyle={checkboxCustomStyle}
      />
      <Checkbox
        fontSize={14}
        title="Amount: Low to High"
        isCheckbox={false}
        onPress={() => handlePress(SORT_OPTIONS.amountLow, 4)}
        checked={selectedValue === 4}
        {...testProps(`cardInstallmentsSortOption${4}LabelId`)}
        customStyle={checkboxCustomStyle}
      />
    </ModalContainer>
  );
};

export default SortContent;
