// @flow
import Button from '@components/Button';
import Checkbox from '@components/Checkbox';
import IconTooltip from '@components/IconTooltip';
import Separator from '@components/Separator';
import Text from '@components/Text';
import AmountInput from '@composition/AmountInput';
import {filterInstallmentsRequest} from '@ducks/installments/actions';
import {installmentsSelector} from '@ducks/installments/selectors';
import {removeCommas} from '@utils/numberInputs';
import {testProps} from '@utils/componentCustomProps';
import React, {useEffect, useState} from 'react';
import {TouchableOpacity, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  ClearAllContainer,
  FilterModalContainer,
  FilterModalHeader,
  HeaderItemContainer,
  HeaderTitleContainer,
  AmountInputContainer,
  StyledTouchableOpacity,
} from '../Styled';
import KeyboardDismissibleViewSv from '@composition/KeyboardDismissibleViewSv';
import {useTheme} from 'styled-components/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {FILTER_CONSTANTS} from '@utils/installments';

type FilterModalContentProps = {
  installmentType?: 'BT' | 'C2GO' | 'BC' | '',
  closeModal: () => void,
};

const mapStatus = (value) => {
  switch (value) {
    case 0:
      return 'SHOW ALL';
    case 1:
      return 'IN PROGRESS';
    case 2:
      return 'APPROVED';
    case 3:
      return 'DECLINED';
    default:
      return 'SHOW ALL';
  }
};

const mapStatusValue = (str) => {
  switch (str) {
    case 'SHOW ALL':
      return 0;
    case 'IN PROGRESS':
      return 1;
    case 'APPROVED':
      return 2;
    case 'DECLINED':
      return 3;
    default:
      return 0;
  }
};

// eslint-disable-next-line sonarjs/cognitive-complexity
const FilterModalContent = (props: FilterModalContentProps): React$Node => {
  const {installmentType, closeModal} = props;
  const dispatch = useDispatch();
  const installmentsState = useSelector(installmentsSelector);
  const {filters} = installmentsState;

  const [isAmountFocused, setAmountFocused] = useState(false);
  const [status, setStatus] = useState(0);
  const [amount, setAmount] = useState({
    amountFrom: '',
    amountTo: '',
  });

  const theme = useTheme();

  const getFilterState = () => {
    let filterState;
    if (filters) {
      if (installmentType === 'BT') {
        filterState = filters.bt;
      } else if (installmentType === 'BC') {
        filterState = filters.bc;
      } else if (installmentType === 'C2GO') {
        filterState = filters.c2g;
      } else {
        filterState = {};
      }
    }

    return filterState;
  };

  useEffect(() => {
    const filterState = getFilterState();
    if (filterState) {
      setStatus(mapStatusValue(filterState.status));

      let aFrom = filterState.minAmount;
      let aTo = filterState.maxAmount;

      if (filterState.minAmount === FILTER_CONSTANTS.MINIMUM_AMOUNT) {
        aFrom = '';
      }

      if (filterState.maxAmount === FILTER_CONSTANTS.MAXIMUM_AMOUNT) {
        aTo = '';
      }

      setAmount({amountFrom: aFrom, amountTo: aTo});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (amount.amountFrom && amount.amountTo) {
      const aFrom = Number(removeCommas(amount.amountFrom + ''));
      const aTo = Number(removeCommas(amount.amountTo + ''));

      if (aFrom === aTo) {
        setAmountError('Amount TO must not be the same with amount FROM.');
      } else if (aFrom > aTo) {
        setAmountError('Amount TO must be greater than amount FROM.');
      } else {
        setAmountError('');
      }
    }
  }, [amount]);

  const handleApplyPress = () => {
    const statusStr = mapStatus(status);
    const {amountFrom, amountTo} = amount;

    let aFrom;
    let aTo;

    if (amountFrom === '' || amountTo === '') {
      aFrom = FILTER_CONSTANTS.MINIMUM_AMOUNT;
      aTo = FILTER_CONSTANTS.MAXIMUM_AMOUNT;
    } else {
      aFrom = Number(removeCommas(amountFrom + ''));
      aTo = Number(removeCommas(amountTo + ''));
    }

    const params = {
      status: statusStr,
      installmentType,
      minAmount: aFrom + '',
      maxAmount: aTo + '',
    };

    closeModal();
    dispatch(filterInstallmentsRequest(params));
  };

  const [amountError, setAmountError] = useState('');

  const setAmountFrom = (val) => {
    return setAmount({...amount, amountFrom: val});
  };
  const setAmountTo = (val) => {
    return setAmount({...amount, amountTo: val});
  };

  const handleClearAll = () => {
    setStatus(0);
    setAmount({amountTo: '', amountFrom: ''});
  };

  const getButtonDisabled = () => {
    if (isAmountFocused) {
      return true;
    }
    if (status === -1) {
      return true;
    }
    if (
      (!amount.amountFrom && amount.amountTo) ||
      (amount.amountFrom && !amount.amountTo)
    ) {
      return true;
    }

    if (
      amount.amountFrom &&
      amount.amountTo &&
      Number(removeCommas(amount.amountFrom + '')) >=
        Number(removeCommas(amount.amountTo + ''))
    ) {
      return true;
    }

    return false;
  };

  const handleFocus = () => {
    setAmountFocused(true);
    setAmountError('');
  };

  return (
    <KeyboardAwareScrollView
      extraHeight={Platform.select({
        ios: 170,
        android: 100,
      })}
      keyboardShouldPersistTaps="always"
      enableOnAndroid>
      <KeyboardDismissibleViewSv scrollEnabled={true}>
        <FilterModalContainer>
          <FilterModalHeader>
            <HeaderItemContainer>
              <StyledTouchableOpacity onPress={closeModal}>
                <IconTooltip
                  source={require('@assets/icons/back-icon.png')}
                  width={18}
                  height={18}
                  color={theme.mainTopMenuItems.color}
                />
              </StyledTouchableOpacity>
            </HeaderItemContainer>
            <HeaderTitleContainer>
              <Text mode="h3" {...testProps('filterInstallmentsTitle')}>
                Filter Installments
              </Text>
            </HeaderTitleContainer>
            <HeaderItemContainer>
              <TouchableOpacity
                onPress={handleClearAll}
                {...testProps('filterInstallmentsClearAllButton')}>
                <ClearAllContainer>
                  <Text
                    mode="buttonlink"
                    {...testProps('filterInstallmentsClearAllButtonLabel')}
                    themeColor="SECONDARY_DEFAULT">
                    Clear all
                  </Text>
                </ClearAllContainer>
              </TouchableOpacity>
            </HeaderItemContainer>
          </FilterModalHeader>
          <Separator space={12} />
          <Text
            mode="overline2"
            {...testProps('filterInstallmentsByStatus')}
            themeColor="TEXT_100_HIGH">
            by status
          </Text>
          <Separator space={12} />

          {/* TODO: Should refactor this into a mapping function instead */}
          <Checkbox
            title="Show all"
            isCheckbox={false}
            onPress={() => setStatus(0)}
            checked={status === 0}
            {...testProps('filterInstallmentsShowAll')}
          />
          <Checkbox
            title="In Progress"
            isCheckbox={false}
            onPress={() => setStatus(1)}
            checked={status === 1}
            {...testProps('filterInstallmentsInProgress')}
          />
          <Checkbox
            title="Approved"
            isCheckbox={false}
            onPress={() => setStatus(2)}
            checked={status === 2}
            {...testProps('filterInstallmentsApproved')}
          />
          <Checkbox
            title="Declined"
            isCheckbox={false}
            onPress={() => setStatus(3)}
            checked={status === 3}
            {...testProps('filterInstallmentsDeclined')}
          />
          <Text
            mode="overline2"
            {...testProps('filterInstallmentsByAmount')}
            themeColor="TEXT_100_HIGH">
            by amount
          </Text>
          <Separator space={12} />
          <AmountInputContainer>
            <AmountInput
              label="From"
              placeholder="0.00"
              currency="PHP"
              value={`${amount.amountFrom}`}
              onBlur={(value) => {
                setAmountFocused(false);
                setAmountFrom(value);
              }}
              onFocus={handleFocus}
              errorMessage={amountError}
              {...testProps('filterInstallmentsAmount')}
            />
          </AmountInputContainer>
          <AmountInputContainer>
            <AmountInput
              label="To"
              placeholder="0.00"
              currency="PHP"
              value={`${amount.amountTo}`}
              onBlur={(value) => {
                setAmountFocused(false);
                setAmountTo(value);
              }}
              onFocus={handleFocus}
              errorMessage={amountError}
            />
          </AmountInputContainer>
          <Button
            title="Apply"
            type="primary"
            state={getButtonDisabled() ? 'disabled' : 'default'}
            onPress={handleApplyPress}
            {...testProps('filterInstallmentsApplyButton')}
          />
          <Button
            title="Cancel"
            titleThemeColor="CANCEL_BUTTON"
            type="primary"
            onPress={() => closeModal()}
            {...testProps('filterInstallmentsCloseButton')}
          />
        </FilterModalContainer>
      </KeyboardDismissibleViewSv>
    </KeyboardAwareScrollView>
  );
};

export default FilterModalContent;
