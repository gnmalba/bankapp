/**
 * @format
 * @flow
 */
// React modules
import React from 'react';
import {TouchableOpacity} from 'react-native';
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropsType} from './types';

// Composition
import Modal from '@composition/Modal';

// Components
import Text from '@components/Text';

// Styles
import * as S from './Styled';

const ModalWithCTA = (props: PropsType): React$Node => {
  // CTA mean CALL TO ACTION
  const {
    testID,
    isVisible,
    isFullWidth,
    titleProps,
    subtitleProps,
    positiveButtonProps,
    negativeButtonProps,
  } = props;
  return (
    <Modal visible={isVisible} isFullWidth={isFullWidth}>
      {titleProps?.text && (
        <Text {...testProps(testID ? testID : '' + 'Title')} mode="h3">
          {titleProps.text}
        </Text>
      )}

      {subtitleProps?.text && (
        <S.DeleteModal.Subtitle
          {...testProps(testID ? testID : '' + 'Description')}
          mode="body2">
          {subtitleProps.text}
        </S.DeleteModal.Subtitle>
      )}

      <S.DeleteModal.ButtonContainer>
        {positiveButtonProps?.buttonText && (
          <TouchableOpacity
            {...testProps(testID ? testID : '' + 'Positive')}
            onPress={positiveButtonProps.callback}>
            <S.DeleteModal.PositiveButtonText
              accessibilityLabel="positiveButtonLabel"
              mode="subtitle">
              {positiveButtonProps.buttonText}
            </S.DeleteModal.PositiveButtonText>
          </TouchableOpacity>
        )}

        {negativeButtonProps?.buttonText && (
          <TouchableOpacity
            {...testProps(testID ? testID : '' + 'Negative')}
            onPress={negativeButtonProps.callback}>
            <S.DeleteModal.NegativeButtonText
              accessibilityLabel="negativeButtonLabel"
              mode="subtitle">
              {negativeButtonProps.buttonText}
            </S.DeleteModal.NegativeButtonText>
          </TouchableOpacity>
        )}
      </S.DeleteModal.ButtonContainer>
    </Modal>
  );
};

export default ModalWithCTA;
