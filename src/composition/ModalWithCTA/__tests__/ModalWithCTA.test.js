import React from 'react';
import {render} from '@testing-library/react-native';
import ModalWithCTA from '../ModalWithCTA';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('FilterTransaction', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ModalWithCTA />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
