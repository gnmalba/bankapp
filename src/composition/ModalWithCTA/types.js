/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  isVisible: boolean,
  isFullWidth?: boolean,
  titleProps?: {
    text: string,
    titleStyle?: Object,
  },
  subtitleProps?: {
    text: string,
    subtitleStyle?: Object,
  },
  positiveButtonProps?: {
    buttonText: string,
    buttonTextStyle?: Object,
    callback: () => void,
  },
  negativeButtonProps?: {
    buttonText: string,
    buttonTextStyle?: Object,
    callback: () => void,
  },
};
