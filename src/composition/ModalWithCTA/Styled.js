/**
 * @flow
 */

import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';
import Text from '@components/Text';

export const DeleteModal: Object = {
  Subtitle: styled(Text)`
    color: ${(props) => props.theme.color.TEXT_100_HIGH};
    margin-top: ${verticalScale(16)}px;
    margin-bottom: ${verticalScale(16)}px;
  `,

  ButtonContainer: styled.View`
    align-items: center;
  `,

  PositiveButtonText: styled(Text)`
    color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
    margin-top: ${verticalScale(8)}px;
    margin-bottom: ${verticalScale(16)}px;
  `,

  NegativeButtonText: styled(Text)`
    color: #bababa;
  `,
};
