## ModalWithCta
ModalWithCta component.

# Usage
```js
import ModalWithCta from '@composition/ModalWithCta';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
