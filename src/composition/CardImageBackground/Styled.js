/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';
import {ImageBackground, TouchableOpacity} from 'react-native';
import Button from '@components/Button';

const mastercard = require('@assets/images/master-card.png');
const creditcard = require('@assets/images/credit-card.png');

export const CardContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${scale(24)}px;
`;

export const CardOverlayForActivation: ComponentType<any> = styled.View`
  width: ${scale(196)}px;
  height: ${scale(132)}px;
  background-color: rgba(28, 30, 35, 0.5);
  position: absolute;
  z-index: 1;
  justify-content: center;
  align-items: center;
  border-radius: ${scale(5)}px;
`;

export const StyledButton: ComponentType<any> = styled(Button).attrs({
  type: 'primitive',
  activeOpacity: 0.9,
})`
  padding-vertical: ${scale(5)}px;
  padding-horizontal: ${scale(10)}px;
  border-radius: ${scale(4)}px;
`;

export const CardStyled: ComponentType<any> = styled(ImageBackground).attrs(
  (props) => ({
    source: props.cardType === 'credit' ? creditcard : mastercard,
  }),
)`
  width: ${scale(196)}px;
  height: ${scale(132)}px;
  resize-mode: contain;
  padding-horizontal: ${scale(16)}px;
  padding-top: ${scale(20)}px;
  padding-bottom: ${scale(24)}px;
  justify-content: center;
`;

export const CardDetails: ComponentType<any> = styled.View``;

export const CardAlias: ComponentType<any> = styled.Text.attrs((props) => ({
  numberOfLines: 1,
}))`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(16)}px;
  color: #ffffff;
  line-height: ${scale(19)}px;
`;

export const CardNumber: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${scale(10)}px;
  line-height: ${scale(12)}px;
  letter-spacing: ${scale(0.846154)}px;
  text-transform: uppercase;
  color: #ffffff;
`;

export const CardIsPrimary: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${scale(10)}px;
  line-height: ${scale(12)}px;
  letter-spacing: ${scale(0.846154)}px;
  text-transform: uppercase;
  color: #ffffff;
  margin-top: ${verticalScale(60)}px;
`;

export const CardTouch: ComponentType<any> = styled(TouchableOpacity).attrs(
  (props) => ({
    ...props,
    onPress: props.onPress,
    activeOpacity: 1,
  }),
)``;
