/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import {accountsFormat, maskingFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';

/**
 * TODO: Please update this component and enable eslint soon
 */
// eslint-disable-next-line sonarjs/cognitive-complexity
const CardImageBackground = (props: PropsType): React$Node => {
  const {cardType, theme, data, onPress} = props;

  const getAlias = () => {
    switch (cardType) {
      case 'credit':
        return data?.item?.embossingName;
      case 'yazz':
        return data?.embossingName;
      case 'deposit':
      case 'prepaid':
        return data?.depositAccount?.alias;
      default:
        return null;
    }
  };

  const getAccountNo = () => {
    switch (cardType) {
      case 'credit':
        return data?.item?.cardno;
      case 'yazz':
        return data?.cardno;
      case 'deposit':
      case 'prepaid':
        return data?.depositAccount?.accountNo;
      default:
        return null;
    }
  };

  const getIsPrimary = () => {
    switch (cardType) {
      case 'credit':
        return data?.item?.primaryCardFlag === '1' ? true : false;
      case 'yazz':
        return data?.primaryCardFlag;
      case 'deposit':
      case 'prepaid':
        return data?.depositAccount?.initialAccount;
      default:
        return null;
    }
  };

  const isCredit = cardType === 'credit' ? true : false;
  const alias = getAlias();
  const accountNo = getAccountNo();
  const isPrimary = getIsPrimary();
  const activationFlag = isCredit ? data?.item?.activationFlag : 0;
  const uniqueIndex = data?.index ? data.index + 1 : '';

  return (
    <S.CardTouch
      {...testProps('overviewCardImageTestId' + uniqueIndex)}
      onPress={onPress}>
      <S.CardContainer>
        <S.CardStyled cardType={cardType}>
          {activationFlag === 1 && (
            <S.CardOverlayForActivation>
              <S.StyledButton
                title="Activate"
                onPress={() => {
                  const {embossingName, cardno: cardNo, cardSerNo} = data.item;

                  props.navigation?.navigate('ActivateCardMainStack', {
                    screen: 'ActivateCardConfirmation',
                    params: {
                      embossingName,
                      cardNo,
                      cardSerNo,
                    },
                  });
                }}
              />
            </S.CardOverlayForActivation>
          )}
          <S.CardDetails>
            <S.CardAlias
              {...testProps('overviewAliasTestId' + uniqueIndex)}
              theme={theme}>
              {alias}
            </S.CardAlias>
            <S.CardNumber
              {...testProps('overviewAccountNoTestId' + uniqueIndex)}
              theme={theme}>
              {cardType === 'credit' || cardType === 'yazz'
                ? maskingFormat(accountNo, true)
                : accountsFormat(accountNo)}
            </S.CardNumber>
            <S.CardIsPrimary
              {...testProps('overviewInitialAccountTestId' + uniqueIndex)}
              theme={theme}>
              {isPrimary && 'PRIMARY'}
            </S.CardIsPrimary>
          </S.CardDetails>
        </S.CardStyled>
      </S.CardContainer>
    </S.CardTouch>
  );
};

export default CardImageBackground;
