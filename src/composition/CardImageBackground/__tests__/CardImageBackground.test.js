import React from 'react';
import {render} from '@testing-library/react-native';
import CardImageBackground from '../CardImageBackground';
import {testProps} from '@utils/componentCustomProps';

describe('ActionShortcuts', () => {
  const data = (flag = '1') => ({
    item: {
      embossingName: 'test',
      cardno: '123',
      primaryCardFlag: '1',
    },
    embossingName: 'test',
    cardno: '123',
    primaryCardFlag: '1',
    depositAccount: {
      alias: 'test',
      accountNo: '123',
      initialAccount: '12',
    },
  });

  it.each(['credit', 'deposit', 'prepaid', 'yazz', ''])(
    'Should work as expected',
    (type) => {
      // Edit the args inside render and provide props.
      const all = render(
        <CardImageBackground
          {...testProps(type === '' ? undefined : type)}
          cardType={type}
          data={data(type === '' ? '0' : '1')}
          onPress={() => {}}
        />,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
