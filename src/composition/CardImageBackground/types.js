/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation?: {
    navigate: (route: string, params?: Object) => void,
  },
  cardType?: string,
  theme?: Object,
  data: Object,
  onPress?: () => void,
};
