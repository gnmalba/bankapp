## CardImageBackground
CardImageBackground component.

# Usage
```js
import CardImageBackground from '@composition/CardImageBackground';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
