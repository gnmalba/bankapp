// React modules
import React from 'react';

// Components
import Button from '@components/Button';
import {CancelButtonContainer} from './Styled';

// Utils
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropsType} from './types';

const CancelButton = (props: PropsType): React$Node => {
  const {handleCancel} = props;

  return (
    <CancelButtonContainer>
      <Button
        {...testProps('EnrollmentCancelButton')}
        title="Cancel"
        type="secondary"
        titleThemeColor="TEXT_300_HIGH"
        state={'default'}
        onPress={handleCancel}
      />
    </CancelButtonContainer>
  );
};

export default CancelButton;
