import React from 'react';
import {render} from '@testing-library/react-native';
import Alias from '../Alias';
import {ThemeProvider} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

describe('AccountNumber', () => {
  it('Should work properly', () => {
    render(
      <ThemeProvider theme={DARK_THEME}>
        <Alias />
      </ThemeProvider>,
    );
  });
});
