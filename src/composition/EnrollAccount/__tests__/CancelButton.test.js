import React from 'react';
import {render} from '@testing-library/react-native';
import CancelButton from '../CancelButton';
import {ThemeProvider} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

describe('AccountNumber', () => {
  it('Should work properly', () => {
    render(
      <ThemeProvider theme={DARK_THEME}>
        <CancelButton />
      </ThemeProvider>,
    );
  });
});
