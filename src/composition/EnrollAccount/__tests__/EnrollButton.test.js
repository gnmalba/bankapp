import React from 'react';
import {render} from '@testing-library/react-native';
import EnrollButton from '../EnrollButton';
import {ThemeProvider} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

describe('AccountNumber', () => {
  it.each([true, false])('Should work properly', (bools) => {
    render(
      <ThemeProvider theme={DARK_THEME}>
        <EnrollButton isValid={bools} />
      </ThemeProvider>,
    );
  });
});
