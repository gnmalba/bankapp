import React from 'react';
import {render} from '@testing-library/react-native';
import AccountNumber from '../AccountNumber';
import {ThemeProvider} from 'styled-components';
import {DARK_THEME} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

describe('AccountNumber', () => {
  it.each(['1234444', undefined])('Should work properly', (acct) => {
    render(
      <ThemeProvider theme={DARK_THEME}>
        <AccountNumber accountNo={acct} {...testProps('accountNumberTestId')} />
      </ThemeProvider>,
    );
  });
});
