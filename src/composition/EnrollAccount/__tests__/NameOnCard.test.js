import React from 'react';
import {render} from '@testing-library/react-native';
import NameOnCard from '../NameOnCard';
import {ThemeProvider} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

describe('AccountNumber', () => {
  it.each([true, false])('Should work properly', (bools) => {
    render(
      <ThemeProvider theme={DARK_THEME}>
        <NameOnCard />
      </ThemeProvider>,
    );
  });
});
