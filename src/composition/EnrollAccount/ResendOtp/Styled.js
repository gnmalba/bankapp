/**
 * @flow
 */

import type {ComponentType} from 'react';
import Text from '@components/Text';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const ResendTextContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const StyledResendText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'buttontext',
    customColor:
      !props.timerOngoing && props.resendCount < 3
        ? props.theme.color.SECONDARY_DEFAULT
        : props.theme.color.PRIMARY_DISABLED,
  }),
)`
  font-family: 'MyriadPro-Semibold';
  font-weight: 600;
`;

export const StyledTimerText: ComponentType<any> = styled(Text)`
  margin-left: 12px;
`;

export const StyledAdditionalText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    customColor: props.theme.color.TEXT_300_HIGH,
  }),
)`
  margin-top: ${verticalScale(20)}px;
`;

export const HighlightedText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    customColor: props.theme.defaultColor,
  }),
)`
  font-weight: bold;
`;
