/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  handleResendCode: any,
  theme?: Object,
};
