/* eslint-disable sonarjs/cognitive-complexity */
/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {TouchableWithoutFeedback} from 'react-native';

import type {PropsType} from './types';
import {
  StyledResendText,
  ResendTextContainer,
  StyledTimerText,
  StyledAdditionalText,
  HighlightedText,
} from './Styled';
import {testProps} from '@utils/componentCustomProps';

const ResendOtp = (props: PropsType): React$Node => {
  const {handleResendCode, theme} = props;

  const [isResendCodeDisabled, setResendCodeDisabled] = useState(false);
  const [resendCount, setResendCount] = useState(0);
  const [showAdditionalMessage, setShowAdditionalMessage] = useState(true);
  const [timeLeft, setTimeLeft] = useState(0);

  useEffect(() => {
    if (!timeLeft) {
      resendCount < 3 && setResendCodeDisabled(false);
      return;
    }

    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);

    return () => clearInterval(intervalId);
  }, [resendCount, timeLeft]);

  useEffect(() => {
    if (resendCount === 3) {
      setResendCodeDisabled(true);
    }
  }, [resendCount]);

  useEffect(() => {
    if (resendCount === 3 && timeLeft === 0) {
      setShowAdditionalMessage(false);
    }
  }, [resendCount, timeLeft]);

  useEffect(() => {
    if (timeLeft < 0 && resendCount !== 0) {
      setResendCodeDisabled(false);
    }
  }, [timeLeft, resendCount]);

  const handleOnResend = () => {
    setTimeLeft(60);
    setResendCount(resendCount + 1);
    setResendCodeDisabled(true);
    handleResendCode();
  };

  return (
    <>
      <ResendTextContainer>
        <TouchableWithoutFeedback
          {...testProps('resendOTPSendAnotherCodeButton')}
          onPress={isResendCodeDisabled ? null : () => handleOnResend()}
          disabled={isResendCodeDisabled || resendCount === 3}>
          <StyledResendText
            theme={theme}
            isResendCodeDisabled={isResendCodeDisabled || resendCount === 3}
            resendCount={resendCount}
            timerOngoing={timeLeft > 0}
            {...testProps('resendOTPSendAnotherCode')}>
            {!isResendCodeDisabled && resendCount === 0
              ? 'Send another code'
              : 'Send again'}
          </StyledResendText>
        </TouchableWithoutFeedback>
        {isResendCodeDisabled && timeLeft > 0 && (
          <StyledTimerText {...testProps('resendOTPTimer')}>
            (00: {timeLeft.toString().length === 2 ? timeLeft : `0${timeLeft}`})
          </StyledTimerText>
        )}
      </ResendTextContainer>
      {isResendCodeDisabled && resendCount <= 3 && timeLeft !== 0 && (
        <StyledAdditionalText {...testProps('resendOTPMessage')}>
          We&apos;ve sent a new code to your mobile number.
        </StyledAdditionalText>
      )}
      {!showAdditionalMessage && (
        <StyledAdditionalText {...testProps('resendOTPAdditionalMessage')}>
          You have reached the maximum number of tries to resend the code. If
          you didn&apos;t receive a code, please call our hotline{' '}
          <HighlightedText>(02) 88-700-700</HighlightedText> for further
          assistance.
        </StyledAdditionalText>
      )}
    </>
  );
};

export default ResendOtp;
