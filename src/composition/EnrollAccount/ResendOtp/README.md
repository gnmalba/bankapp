## ResendOtp
ResendOtp component.

# Usage
```js
import ResendOtp from '@composition/ResendOtp';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
