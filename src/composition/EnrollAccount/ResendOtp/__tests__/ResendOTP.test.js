import React from 'react';
import {render} from '@testing-library/react-native';
import ResendOtp from '../ResendOtp';
import {testProps} from '@utils/componentCustomProps';

describe('ResendOtp', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<ResendOtp {...testProps('resendOtpTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
