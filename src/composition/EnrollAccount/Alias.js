// React modules
import React from 'react';

// Compositions
import FormikInput from '@composition/FormikInput';

// Components
import {FieldContainer} from './Styled';

// Types
import type {PropsType} from './types';

const Alias = (props: PropsType): React$Node => {
  const {
    alias,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
  } = props;

  return (
    <FieldContainer>
      <FormikInput
        testID="EnrollmentAliasInput"
        name="alias"
        label="Account Alias"
        placeholderText="Enter Alias"
        description={'Assign an alias for your linked account.'}
        value={alias}
        error={error}
        isTouched={isTouched}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={handleSetFieldTouched}
        textWidth="98%"
      />
    </FieldContainer>
  );
};

export default Alias;
