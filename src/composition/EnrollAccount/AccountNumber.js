/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable react-hooks/exhaustive-deps */
// React modules
import React, {useState, useEffect} from 'react';
import {Platform} from 'react-native';
// Compositions
import FormikInput from '@composition/FormikInput';

// Components
import {
  AccountNumberFieldContainer,
  AccountNumberLabel,
  AccountNumberMaskText,
} from './Styled';

// Utils
import {accountNumberInputMask} from '@utils/accountNumber';
import {removeSpace} from '@utils/commons';
import {moderateScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropsType} from './types';

const AccountNumberInput = (props: PropsType): React$Node => {
  const {
    accountNo,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
    isValid,
    theme,
  } = props;

  const depositMask = 'XXXXXXXXXXXXX';
  const creditMask = 'XXXXXXXXXXXXXXXX';
  const defaultPadding = moderateScale(17);

  const [
    accountNumberMaskPlaceholder,
    setAccountNumberMaskPlaceholder,
  ] = useState(depositMask);
  const [placeholderPadding, setPlaceholderPadding] = useState(defaultPadding);
  const [showInputPlaceholder, setShowInputPlaceholder] = useState(true);
  const isNumber = new RegExp('^[0-9]+$');
  useEffect(() => {
    if (accountNo?.length) {
      if (accountNo.match(isNumber)) {
        const unformattedAccountNo = removeSpace(accountNo);
        if (unformattedAccountNo.length > 12) {
          const newMask = handleUpdateInputMask(
            creditMask,
            unformattedAccountNo.length,
          );
          setAccountNumberMaskPlaceholder(newMask);
        } else {
          const newMask = handleUpdateInputMask(
            depositMask,
            unformattedAccountNo.length,
          );
          setAccountNumberMaskPlaceholder(newMask);
        }

        let additionalPadding = 0;

        const fieldArray = unformattedAccountNo.split('');
        fieldArray.forEach((field) => {
          additionalPadding += getAdditionalPadding(Platform.OS);
        });

        setPlaceholderPadding(additionalPadding + defaultPadding);
      }
    } else {
      setAccountNumberMaskPlaceholder(depositMask);
      setPlaceholderPadding(defaultPadding);
    }
  }, [defaultPadding, accountNo]);

  const getAdditionalPadding = (os) => {
    let additionalPadding = 0;
    if (os === 'ios') {
      additionalPadding += moderateScale(6);
    } else {
      additionalPadding += moderateScale(6.4);
    }
    return additionalPadding;
  };

  const handleUpdateInputMask = (mask, length) => {
    const splitMask = mask.split('');
    for (let x = 0; x < length; x++) {
      splitMask[x] = ' ';
    }
    return splitMask.join('');
  };

  const resetAccountNo = () => {
    setAccountNumberMaskPlaceholder(depositMask);
    setPlaceholderPadding(defaultPadding);
    handleResetField('accountNo');
  };

  return (
    <>
      <AccountNumberLabel {...testProps('EnrollmentAccountNumberInputLabel')}>
        Account Number
      </AccountNumberLabel>
      <AccountNumberFieldContainer
        theme={theme}
        adjustMarginBottom={
          error || !showInputPlaceholder || isValid || !accountNo
        }>
        <FormikInput
          testID="EnrollmentAccountNumberInput"
          name="accountNo"
          placeholderText={
            showInputPlaceholder ? 'Enter account or card number' : ''
          }
          description="This can either be your 13-digit account number or 16-digit card number."
          value={accountNumberInputMask(accountNo)}
          error={error}
          isTouched={isTouched}
          isValid={isValid}
          handleInputChange={handleInputChange}
          handleResetField={resetAccountNo}
          handleSetFieldTouched={handleSetFieldTouched}
          handleOnFocus={() => setShowInputPlaceholder(false)}
          handleOnBlur={() => setShowInputPlaceholder(true)}
          keyboardType="number-pad"
          maxLength={19}
          customBackgroundColor={'transparent'}
          textWidth={'98%'}
        />
        {!showInputPlaceholder && (
          <AccountNumberMaskText placeholderPadding={placeholderPadding}>
            {accountNumberInputMask(accountNumberMaskPlaceholder, true)}
          </AccountNumberMaskText>
        )}
      </AccountNumberFieldContainer>
    </>
  );
};

export default AccountNumberInput;
