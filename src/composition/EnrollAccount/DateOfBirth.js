// React modules
import React, {useState} from 'react';

// Third party libraries
import {TouchableOpacity} from 'react-native';
import moment from 'moment';

// Compositions
import CalendarModal from '@composition/CalendarModal';

// Components
import Text from '@components/Text';
import {
  FieldContainer,
  LabelContainer,
  DateContainer,
  DatePlaceholder,
  ErrorContainer,
  ErrorText,
  ErrorIcon,
} from './Styled';

// Utils
import {ERRORS} from '@screens/private/EnrollAccount/constants';
import {testProps} from '@utils/componentCustomProps';
import {DATE_FORMAT} from '@utils/constants';

// Types
import type {PropsType} from './types';

const DateOfBirth = (props: PropsType): React$Node => {
  const {
    dateOfBirth,
    handleInputChange,
    handleSetFieldTouched,
    hasAdditionalError,
    error,
    isTouched,
  } = props;
  const today = moment();

  const [showModal, setShowModal] = useState(false);
  const [dobError, setDobError] = useState(null);

  const validateDateOfBirth = (date) => {
    const validDate = moment().diff(date, 'years') >= 18;

    if (!validDate) {
      setDobError(ERRORS.DATE_OF_BIRTH.IS_BELOW_18);
      hasAdditionalError(true);
    } else {
      setDobError(null);
      hasAdditionalError(false);
    }
  };
  return (
    <>
      <FieldContainer>
        <LabelContainer>
          <Text {...testProps('EnrollmentDateOfBirthLabel')}>
            Date of Birth
          </Text>
        </LabelContainer>
        <TouchableOpacity activeOpacity={1} onPress={() => setShowModal(true)}>
          <DateContainer hasError={dobError || (error && isTouched)}>
            {!!dateOfBirth === false && (
              <DatePlaceholder
                isPlaceholder
                {...testProps('EnrollmentDateOfBirthValue')}>
                {DATE_FORMAT}
              </DatePlaceholder>
            )}
            {!!dateOfBirth === true && (
              <DatePlaceholder {...testProps('EnrollmentDateOfBirthValue')}>
                {dateOfBirth}
              </DatePlaceholder>
            )}
            {dobError && <ErrorIcon />}
            {error && isTouched && !dobError && <ErrorIcon />}
          </DateContainer>
          {dobError || (error && isTouched) ? (
            <ErrorContainer>
              <ErrorText {...testProps('EnrollmentDateOfBirthErrorText')}>
                {dobError || error}
              </ErrorText>
            </ErrorContainer>
          ) : (
            <></>
          )}
        </TouchableOpacity>
      </FieldContainer>
      {showModal && (
        <CalendarModal
          showVisible={true}
          handleOnCancel={() => {
            setShowModal(false);
            handleSetFieldTouched('dateOfBirth');
          }}
          handleOnApply={(calendarDate) => {
            handleInputChange('dateOfBirth')(calendarDate);
            handleSetFieldTouched('dateOfBirth');
            validateDateOfBirth(calendarDate);
            setShowModal(false);
          }}
          calendarProps={{
            selectedStartDate: dateOfBirth,
            allowRangeSelection: false,
            initialDate: dateOfBirth || moment(),
            maxDate: moment(today, 'YYYY/MM/DD'),
            disabledDates: (date) => date.isAfter(today, 'day'),
          }}
        />
      )}
    </>
  );
};

export default DateOfBirth;
