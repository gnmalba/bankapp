/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Platform} from 'react-native';
import styled from 'styled-components/native';
import Text from '@components/Text';

import {
  HEIGHT,
  GUIDELINE_BASE_HEIGHT,
  IS_DEVICE_TABLET,
} from '@utils/constants';
import {verticalScale, moderateScale, scale} from '@utils/scale';

export const FieldContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(18)}px;
`;

export const CancelButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
  margin-bottom: ${verticalScale(10)}px;
`;

export const LabelContainer: ComponentType<any> = styled.View`
  padding: 0px 0px ${moderateScale(12)}px 0px;
  flex-direction: row;
  align-items: center;
`;

export const DateContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  height: ${moderateScale(48)}px;
  align-items: center;
  border-radius: 4px;
  background-color: ${(props) => {
    const {state, theme} = props;
    return (
      theme.textInput?.backgroundColor[state || 'default'] || 'transparent'
    );
  }};
  border: ${(props) => {
    return props.hasError ? props.theme.textInput?.border.error : 'transparent';
  }};
  padding-right: ${moderateScale(18)}px;
  padding-top: ${verticalScale(8)}px;
  padding-bottom: ${verticalScale(8)}px;
`;

export const DatePlaceholder: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    customColor: props.isPlaceholder
      ? props.theme.textInput.placeholderTextColor
      : props.theme.color.TEXT_300_HIGH,
  }),
)`
  padding-left: ${moderateScale(18)}px;
  padding-top: ${() =>
    Platform.OS === 'ios' && HEIGHT > GUIDELINE_BASE_HEIGHT
      ? verticalScale(4) + 'px'
      : verticalScale(8) + 'px'};
  padding-bottom: ${verticalScale(8)}px;
  width: 93%;
`;

export const ErrorContainer: ComponentType<any> = styled.View`
  padding-top: ${verticalScale(8)}px;
`;

export const ErrorText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
  customColor: props.theme?.textInput?.helperTextColor.error,
}))``;

export const ErrorIcon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/warning.png'),
}))`
  resize-mode: contain;
  height: ${verticalScale(24)}px;
  width: ${scale(22)}px;
  tint-color: ${(props) => props.theme?.textInput?.helperTextColor.error};
`;

export const AccountNumberFieldContainer: ComponentType<any> = styled.View`
  flex-direction: column;
  margin-bottom: ${(props) => {
    return props.adjustMarginBottom ? verticalScale(32) + 'px' : '-1px';
  }};
  background-color: ${(props) => {
    return props.theme.color.BACKGROUND_300;
  }};
  height: ${IS_DEVICE_TABLET ? verticalScale(45) : verticalScale(40)}px;
  border-radius: 4px;
`;

export const AccountNumberLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body2',
  }),
)`
  padding: 0px 0px ${moderateScale(12)}px 0px;
  flex-direction: row;
  align-items: center;
`;

export const AccountNumberMaskText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    customColor: props.theme.textInput.placeholderTextColor,
  }),
)`
  z-index: -1;
  position: absolute;
  margin-top: ${(props) => {
    const phoneMargin = Platform.OS === 'ios' ? '4.2%' : '4.5%';
    return IS_DEVICE_TABLET ? '2.9%' : phoneMargin;
  }};
  margin-left: ${(props) => {
    return props.placeholderPadding + 'px';
  }};
  font-size: ${moderateScale(18)}px;
`;
