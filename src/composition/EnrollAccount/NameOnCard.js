// React modules
import React from 'react';

// Compositions
import FormikInput from '@composition/FormikInput';

// Components
import {FieldContainer} from './Styled';

// Types
import type {PropsType} from './types';

const NameOnCard = (props: PropsType): React$Node => {
  const {
    nameOnCard,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
  } = props;

  return (
    <FieldContainer>
      <FormikInput
        testID="EnrollmentNameOnCardInput"
        name="nameOnCard"
        label="Name on Card"
        placeholderText="Enter name on card"
        value={nameOnCard}
        error={error}
        isTouched={isTouched}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={handleSetFieldTouched}
        maxLength={22}
        textWidth={'98%'}
      />
    </FieldContainer>
  );
};

export default NameOnCard;
