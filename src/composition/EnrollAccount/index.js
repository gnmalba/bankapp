export {default as AccountNumber} from './AccountNumber';
export {default as Alias} from './Alias';
export {default as NameOnCard} from './NameOnCard';
export {default as DateOfBirth} from './DateOfBirth';
export {default as EnrollButton} from './EnrollButton';
export {default as CancelButton} from './CancelButton';
export {default as ResendOtp} from './ResendOtp';
