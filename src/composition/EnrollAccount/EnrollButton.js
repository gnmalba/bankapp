// React modules
import React, {useState, useEffect} from 'react';

// Components
import Button from '@components/Button';

// Utils
import {testProps} from '@utils/componentCustomProps';

// Types
import type {PropsType} from './types';

const SubmitButton = (props: PropsType): React$Node => {
  const {handleSubmit, isValid} = props;
  const [buttonState, setButtonState] = useState('default');

  useEffect(() => {
    if (isValid) {
      setButtonState('default');
    } else {
      setButtonState('disabled');
    }
  }, [isValid]);

  return (
    <Button
      {...testProps('EnrollmentSubmitButton')}
      title="Enroll"
      type="primary"
      state={buttonState}
      onPress={handleSubmit}
    />
  );
};

export default SubmitButton;
