/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  action?: React$Node,
  contents?: React$Node,
};
