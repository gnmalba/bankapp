## CenteredSingleScreen
CenteredSingleScreen component.

# Usage
```js
import CenteredSingleScreen from '@composition/CenteredSingleScreen';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
