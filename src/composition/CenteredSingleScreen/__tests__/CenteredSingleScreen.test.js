import React from 'react';
import {render} from '@testing-library/react-native';
import CenteredSingleScreen from '../CenteredSingleScreen';

describe('CenteredSingleScreen', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<CenteredSingleScreen />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
