/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';

export const Main: ComponentType<any> = styled.View`
  flex-direction: column-reverse;
  height: 100%;
  justify-content: space-between;
`;

export const Container: ComponentType<any> = styled.View`
  height: 100%;
  align-items: center;
  justify-content: center;
  padding: ${verticalScale(70)}px ${scale(32)}px 0px ${scale(32)}px;
`;

export const ActionContainer: ComponentType<any> = styled.View`
  padding: 0px ${scale(32)}px ${scale(32)}px ${scale(32)}px;
`;
