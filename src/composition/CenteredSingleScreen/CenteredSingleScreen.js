/**
 * @format
 * @flow
 */

import React from 'react';
import Layout from '@components/Layout';

import type {PropsType} from './types';
import * as S from './Styled';

const FullPageError = (props: PropsType): React$Node => {
  const {action, contents} = props;
  return (
    <Layout>
      <S.Main>
        <S.ActionContainer>{action}</S.ActionContainer>
        <S.Container>{contents}</S.Container>
      </S.Main>
    </Layout>
  );
};

export default FullPageError;
