/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation: {
    navigate: any,
  },
};

export type OptionItem = {
  name: string,
  code: string,
};

export type SelectInputProps = {
  ...PropsType,
  label: string,
  placeholder: string,
  selectedOption: string,
  setSelectedOption: (string, Object) => any,
  options: Array<OptionItem>,
  isOpen: boolean,
  setIsOpen: boolean,
};

export type SelectListProps = {
  isOpen: boolean,
  options: Array<OptionItem>,
  selectOption: (number) => Object,
};

// Country Code
export type CountryCodeItem = {
  phoneCode: string,
};

export type CountryCodeListProps = {
  isOpen: boolean,
  options: Array<CountryCodeItem>,
  selectOption: (number) => Object,
};

export type ContactTypeProps = {
  value: string,
  setContactType: (string) => any,
};

export type AccountTypeProps = {
  label: string,
  value: string,
  setAccountType: (string) => any,
};
export type ConfirmItemProps = {
  label: string,
  value: string,
};
