/**
 * @flow
 */

// React modules
import React from 'react';

// Components
import {ConfirmItemContainer, Label, Value} from './Styled';

// Types
import type {ConfirmItemProps} from './types';

const ConfirmItem = (props: ConfirmItemProps): React$Node => {
  const {label, value} = props;

  if (!value) {
    return null;
  }
  return (
    <ConfirmItemContainer>
      <Label themeColor="TEXT_100_HIGH">{label}</Label>
      <Value>{value}</Value>
    </ConfirmItemContainer>
  );
};

export default ConfirmItem;
