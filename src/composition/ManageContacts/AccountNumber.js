// React modules
import React, {useState, useEffect} from 'react';

// Compositions
import FormikInput from '@composition/FormikInput';

// Utils
import {accountNumberInputMask} from '@utils/accountNumber';

// Types
import type {PropsType} from './types';

// Components
import {FieldContainer} from './Styled';

const AccountNumber = (props: PropsType): React$Node => {
  const {
    accountNo,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
  } = props;

  const [maskedAccountNumber, setMaskedAccountNumber] = useState(null);

  useEffect(() => {
    if (accountNo) {
      setMaskedAccountNumber(accountNumberInputMask(accountNo));
    } else {
      setMaskedAccountNumber(null);
    }
  }, [accountNo]);

  return (
    <FieldContainer>
      <FormikInput
        testID="EnrollmentAccountNumberInput"
        name="accountNo"
        label="Account Number"
        placeholderText="Enter account or card number"
        description="This can either be your 13-digit account number or 16-digit card number."
        value={maskedAccountNumber}
        error={error}
        isTouched={isTouched}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={handleSetFieldTouched}
        keyboardType="number-pad"
        maxLength={19}
      />
    </FieldContainer>
  );
};

export default AccountNumber;
