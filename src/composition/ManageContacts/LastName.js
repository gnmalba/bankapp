// React modules
import React from 'react';

// Compositions
import FormikInput from '@composition/FormikInput';

// Components
import {FieldContainer} from './Styled';

// Types
import type {PropsType} from './types';

const LastName = (props: PropsType): React$Node => {
  const {
    value,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
  } = props;

  return (
    <FieldContainer>
      <FormikInput
        testID="EnrollmentNameOnCardInput"
        name="lastName"
        label="Last Name"
        placeholderText="Your contact's last name"
        value={value}
        error={error}
        isTouched={isTouched}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={handleSetFieldTouched}
      />
    </FieldContainer>
  );
};

export default LastName;
