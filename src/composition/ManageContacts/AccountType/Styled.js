/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import {moderateScale} from '@utils/scale';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export const UncheckedIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'radio-button-unchecked',
    size: moderateScale(24),
  }),
)`
  color: ${(props) => props.theme?.textInput?.placeholderTextColor};
  align-self: center;
`;

export const CheckedIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: 'radio-button-checked',
    size: moderateScale(24),
  }),
)`
  color: ${(props) => props.theme.color.SECONDARY_DEFAULT};
  align-self: center;=
`;
