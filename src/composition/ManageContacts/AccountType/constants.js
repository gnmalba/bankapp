export const ORGANIZATION_TYPE = {label: 'Organization', value: 'Organization'};
export const INDIVIDUAL_TYPE = {label: 'Individual', value: 'Individual'};

export const ACCOUNT_TYPE = [INDIVIDUAL_TYPE, ORGANIZATION_TYPE];
