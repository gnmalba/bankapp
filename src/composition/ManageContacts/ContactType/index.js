/**
 * @flow
 */

// React modules
import React from 'react';

// Components
import Text from '@components/Text';
import {FieldContainer} from '../Styled';
import * as Styled from './Styled';
import CheckboxComponent from '@components/CheckboxComponent';

// Types
import type {ContactTypeProps} from '../types';

//  Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';
import {CONTACT_TYPES} from './constants';

const ContactType = (props: ContactTypeProps): React$Node => {
  const {value, setContactType} = props;
  const theme = useTheme() || DARK_THEME;

  const handleCheckbox = (val) => {
    setContactType(val);
  };
  return (
    <FieldContainer>
      <Text mode="body2" themeColor="TEXT_100_HIGH">
        Contact Type
      </Text>
      {CONTACT_TYPES.map((contact_type, index) => {
        return (
          <CheckboxComponent
            index={index}
            title={contact_type.label}
            size={34}
            height={26}
            checkedIcon={<Styled.CheckedIcon theme={theme} />}
            uncheckedIcon={<Styled.UncheckedIcon theme={theme} />}
            checkedColor={theme.color.SECONDARY_DEFAULT}
            uncheckedColor={theme.color.SECONDARY_DEFAULT}
            isChecked={contact_type.value === value}
            onPress={() => handleCheckbox(contact_type.value)}
            value={contact_type.value}
          />
        );
      })}
    </FieldContainer>
  );
};

export default ContactType;
