/**
 * @flow
 */

// React modules
import React from 'react';
import {Pressable} from 'react-native';

// Components
import Text from '@components/Text';
import * as Styled from './Styled';

// Types
import type {CountryCodeListProps} from '../types';

//  Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

const CountryCodeList = (props: CountryCodeListProps): React$Node => {
  const {isOpen, options, selectOption} = props;
  const theme = useTheme() || DARK_THEME;

  if (!isOpen) {
    return null;
  }

  return (
    <Styled.CountryCodeListContainer theme={theme}>
      {options.map((option, index) => {
        return (
          <Pressable key={index} onPress={() => selectOption(index)}>
            <Styled.CountryCodeListItem theme={theme}>
              <Text mode="body1">{option.phoneCode}</Text>
            </Styled.CountryCodeListItem>
          </Pressable>
        );
      })}
    </Styled.CountryCodeListContainer>
  );
};

export default CountryCodeList;
