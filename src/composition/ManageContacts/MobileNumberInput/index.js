// React modules
import React, {useState} from 'react';

// Components
import Text from '@components/Text';
import {FieldContainer} from '../Styled';
import * as Styled from './Styled';

// Types
import type {SelectInputProps} from '../types';

//  Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

// Compositions
import CountryCodeList from './CountryCodeList';
import {View} from 'react-native';

const MobileNumberInput = (props: SelectInputProps): React$Node => {
  const {
    label,
    placeholder,
    options,
    setFieldValue,
    countryCode,
    value,
    error,
    // isTouched,
    // handleResetField,
    // handleSetFieldTouched,
  } = props;
  const theme = useTheme() || DARK_THEME;
  const [isOpen, setIsOpen] = useState(false);
  const [isFocused, setIsFocused] = useState(false);

  const handleFocus = () => {
    setIsFocused(true);
  };

  const handleBlur = () => {
    setIsFocused(false);
  };

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const selectOption = (index) => {
    setFieldValue('countryCode', options[index]);
    toggleDropdown();
  };

  const onChangeText = (text) => {
    setFieldValue('mobileNumber', text);
  };

  return (
    <FieldContainer>
      {label && <Text mode="body2">{label}</Text>}
      <Styled.MobileInputContainer
        theme={theme}
        isOpen={isOpen}
        isFocused={isFocused}
        error={error}>
        <Styled.FlagContainer onPress={toggleDropdown}>
          <Text>{countryCode.country}</Text>
          <Text mode="body2">{countryCode.phoneCode}</Text>
        </Styled.FlagContainer>
        <Styled.Divider theme={theme} />
        <View>
          <Styled.NumberTextInput
            theme={theme}
            onFocus={handleFocus}
            onBlur={handleBlur}
            placeholder={placeholder}
            onChangeText={onChangeText}
            value={value}
          />
        </View>
      </Styled.MobileInputContainer>

      {error && (
        <Styled.StyledText mode="body2" state={'error'}>
          {error}
        </Styled.StyledText>
      )}
      <CountryCodeList
        isOpen={isOpen}
        options={options}
        selectOption={selectOption}
      />
    </FieldContainer>
  );
};

export default MobileNumberInput;
