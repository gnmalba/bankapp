/**
 * @flow
 */

import type {ComponentType} from 'react';
import {Pressable, TextInput} from 'react-native';
import styled from 'styled-components/native';

import {verticalScale, scale, moderateScale} from '@utils/scale';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import Text from '@components/Text';

export const MobileInputContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
  flex-direction: row;
  height: ${moderateScale(48)}px;
  border-radius: 5px;
  border: ${(props) => {
    if (props.error) {
      return props.theme.textInput?.border.error;
    } else {
      return `1px solid ${
        props.isFocused
          ? props.theme.color.SECONDARY_DEFAULT
          : props.theme.color.BACKGROUND_300
      }`;
    }
  }};
  margin-top: 12px;
  border-bottom-left-radius: ${(props) => (props.isOpen ? '0px' : '5px')};
  border-bottom-right-radius: ${(props) => (props.isOpen ? '0px' : '5px')};
`;

export const SelectInputText: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(12)}px;
  justify-content: center;
  flex: 1;
`;

export const Divider: ComponentType<any> = styled.View`
  justify-content: center;
  border-right-width: 1px;
  margin-top: ${verticalScale(10)};
  margin-bottom: ${verticalScale(10)};
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
`;

export const ArrowIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: `keyboard-arrow-${props.isOpen ? 'up' : 'down'}`,
    size: scale(22),
  }),
)`
  color: #a2bddb;
  align-self: center;
  margin-horizontal: ${scale(10)}px;
`;

export const CountryCodeListContainer: ComponentType<any> = styled.View`
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
  max-height: ${verticalScale(240)};
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
`;

export const CountryCodeListItem: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(16)}px;
  height: ${verticalScale(40)}px;
  justify-content: center;
  border-width: 1px;
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
`;

export const FlagContainer: ComponentType<any> = styled(Pressable).attrs(
  (props) => ({
    ...props,
    onPress: props.onPress,
  }),
)`
  flex-direction: row;
  align-items: center;
  margin-horizontal: ${scale(14)}px;
`;

export const NumberTextInput: ComponentType<any> = styled(TextInput).attrs(
  (props) => ({
    ...props,
    placeholderTextColor: props.theme?.textInput?.placeholderTextColor,
  }),
)`
  border-radius: ${moderateScale(4)}px;
  font-size: ${moderateScale(16)}px;
  font-family: Myriad Pro;
  line-height: ${moderateScale(20)}px;
  font-style: normal;
  font-weight: normal;
  padding: ${moderateScale(14)}px ${moderateScale(35)}px ${moderateScale(14)}px
    ${moderateScale(16)}px;
  align-items: center;
  color: ${(props) => props.theme.textInput?.textColor};
  flex: 1;
`;

export const StyledText: ComponentType<any> = styled(Text).attrs((props) => {
  return {
    customColor:
      props.theme?.textInput?.helperTextColor[props.state || 'default'],
  };
})`
  margin-top: ${verticalScale(8)}px;
`;
