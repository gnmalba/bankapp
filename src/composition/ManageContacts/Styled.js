/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import {verticalScale, scale} from '@utils/scale';

// Components
import Text from '@components/Text';

export const FieldContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(25)}px;
`;

export const CancelButtonContainer: ComponentType<any> = styled.View`
  align-items: center;
  margin-top: ${verticalScale(16)}px;
`;

export const ConfirmItemContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(16)}px;
  flex-direction: row;
  align-items: center;
`;

export const Label: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'overline1',
}))`
  flex: 2;
`;

export const Value: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body1',
}))`
  font-family: Myriad Pro;
  flex: 3;
`;

export const SuccessLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'overline1',
}))`
  flex: 1;
  margin-right: ${scale(8)}px;
`;

export const SuccessValue: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body1',
}))`
  font-family: Myriad Pro;
  flex: 2;
`;
