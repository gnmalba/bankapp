/**
 * @flow
 */

// React modules
import React from 'react';
import {Pressable} from 'react-native';

// Components
import Text from '@components/Text';
import * as Styled from './Styled';

// Types
import type {SelectListProps} from '../types';

//  Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

const SelectList = (props: SelectListProps): React$Node => {
  const {isOpen, options, selectOption} = props;
  const theme = useTheme() || DARK_THEME;

  if (!isOpen) {
    return null;
  }
  return (
    <Styled.SelectListContainer theme={theme} nestedScrollEnabled={true}>
      {options.map((option, index) => {
        return (
          <Pressable key={index} onPress={() => selectOption(index)}>
            <Styled.SelectListItem theme={theme}>
              <Text mode="body1">{option.name}</Text>
            </Styled.SelectListItem>
          </Pressable>
        );
      })}
    </Styled.SelectListContainer>
  );
};

export default SelectList;
