/**
 * @flow
 */

import type {ComponentType} from 'react';
import {ScrollView} from 'react-native';
import styled from 'styled-components/native';

import {verticalScale, scale} from '@utils/scale';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export const SelectInputTextContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
  flex-direction: row;
  height: ${verticalScale(48)}px;
  border-radius: 5px;
  border-color: ${(props) =>
    props.isOpen
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.BACKGROUND_300};
  border-width: 1px;
  margin-top: 12px;
  border-bottom-left-radius: ${(props) => (props.isOpen ? '0px' : '5px')};
  border-bottom-right-radius: ${(props) => (props.isOpen ? '0px' : '5px')};
`;

export const SelectInputText: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(12)};
  justify-content: center;
  flex: 1;
`;

export const Divider: ComponentType<any> = styled.View`
  justify-content: center;
  border-right-width: 1px;
  margin-top: ${verticalScale(10)}px;
  margin-bottom: ${verticalScale(10)}px;
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
`;

export const ArrowIcon: ComponentType<any> = styled(MaterialIcons).attrs(
  (props) => ({
    name: `keyboard-arrow-${props.isOpen ? 'up' : 'down'}`,
    size: scale(22),
  }),
)`
  color: #a2bddb;
  align-self: center;
  margin-horizontal: ${scale(10)}px;
`;

export const SelectListContainer: ComponentType<any> = styled(
  ScrollView,
).attrs((props) => ({...props}))`
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
  max-height: ${verticalScale(240)}px;
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
`;

export const SelectListItem: ComponentType<any> = styled.View`
  padding-horizontal: ${scale(16)}px;
  height: ${verticalScale(40)}px;
  justify-content: center;
  border-width: 1px;
  border-color: ${(props) => props.theme.color.TEXT_300_LOW};
`;
