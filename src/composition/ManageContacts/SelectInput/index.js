/**
 * @flow
 */

// React modules
import React, {useState} from 'react';
import {Pressable} from 'react-native';

// Components
import Text from '@components/Text';
import {FieldContainer} from '../Styled';
import * as Styled from './Styled';
import SelectList from './SelectList';

// Types
import type {SelectInputProps} from '../types';

//  Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

const SelectInput = (props: SelectInputProps): React$Node => {
  const {
    label,
    placeholder,
    selectedOption,
    setSelectedOption,
    options,
  } = props;
  const theme = useTheme() || DARK_THEME;
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const selectOption = (index) => {
    setSelectedOption('bank', options[index]);
    toggleDropdown();
  };

  return (
    <FieldContainer>
      {label && <Text mode="body2">{label}</Text>}
      <Pressable onPress={toggleDropdown}>
        <Styled.SelectInputTextContainer theme={theme} isOpen={isOpen}>
          <Styled.SelectInputText>
            {selectedOption ? (
              <Text mode="body1">{selectedOption}</Text>
            ) : (
              <Text mode="body1" customColor={'#486485'}>
                {placeholder}
              </Text>
            )}
          </Styled.SelectInputText>
          <Styled.Divider theme={theme} />
          <Styled.ArrowIcon isOpen={isOpen} />
        </Styled.SelectInputTextContainer>
      </Pressable>
      <SelectList
        isOpen={isOpen}
        options={options}
        selectOption={selectOption}
      />
    </FieldContainer>
  );
};

export default SelectInput;
