// React modules
import React from 'react';

// Components
import {FieldContainer} from '../Styled';
import {Label, FieldInputContainer, FieldInputValue} from './Styled';

// Types
import type {PropsType} from './types';

// Utils
import {useTheme} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

const DisabledField = (props: PropsType): React$Node => {
  const {label, value} = props;
  const theme = useTheme() || DARK_THEME;

  return (
    <FieldContainer>
      <Label>{label}</Label>
      <FieldInputContainer theme={theme}>
        <FieldInputValue>{value}</FieldInputValue>
      </FieldInputContainer>
    </FieldContainer>
  );
};

export default DisabledField;
