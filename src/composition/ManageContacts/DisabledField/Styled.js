/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import {verticalScale, scale} from '@utils/scale';

// Components
import Text from '@components/Text';

export const Label: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
}))`
  margin-bottom: ${verticalScale(12)}px;
  opacity: 0.5;
`;

export const FieldInputContainer: ComponentType<any> = styled.View`
  height: ${verticalScale(40)}px;
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
  padding-horizontal: ${scale(16)}px;
  justify-content: center;
  border-radius: 4px;
`;

export const FieldInputValue: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
  }),
)`
  opacity: 0.5;
`;
