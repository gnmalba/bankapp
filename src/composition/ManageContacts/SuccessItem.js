/**
 * @flow
 */

// React modules
import React from 'react';

// Components
import {ConfirmItemContainer, SuccessLabel, SuccessValue} from './Styled';

// Types
import type {ConfirmItemProps} from './types';

const SuccessItem = (props: ConfirmItemProps): React$Node => {
  const {label, value} = props;

  if (!value) {
    return null;
  }
  return (
    <ConfirmItemContainer>
      <SuccessLabel themeColor="TEXT_100_HIGH">{label}</SuccessLabel>
      <SuccessValue>{value}</SuccessValue>
    </ConfirmItemContainer>
  );
};

export default SuccessItem;
