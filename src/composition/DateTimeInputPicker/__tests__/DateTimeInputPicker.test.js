import React from 'react';
import {render} from '@testing-library/react-native';
import DateTimeInputPicker from '../DateTimeInputPicker';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('DateTimeInputPicker', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <DateTimeInputPicker />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
