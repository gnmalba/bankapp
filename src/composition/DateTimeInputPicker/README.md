## DateTimeInputPicker
DateTimeInputPicker component.

# Usage
```js
import DateTimeInputPicker from '@composition/DateTimeInputPicker';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
