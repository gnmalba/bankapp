/**
 * @flow
 */

type Modes = 'date' | 'dateTime';

type CalendarModalProp = {
  showVisibleCalendarModal: boolean,
  handleCalendarOnApply: any,
  handleCalendarOnCancel: any,
  calendarProps: Object,
};
type TimePickerModalProp = {
  showVisibleTimePickerModal: boolean,
  timePickerLabel: string,
  onBackdropPress: any,
  timePickerProps: Object,
};

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  mode: Modes,
  calendarModalProp: CalendarModalProp,
  timePickerModalProp?: TimePickerModalProp,
};
