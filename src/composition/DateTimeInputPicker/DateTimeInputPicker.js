/**
 * @format
 * @flow
 */
// React Modules
import React from 'react';

// Types
import type {PropsType} from './types';

// Components
import {Container} from './Styled';
import TextInput from '@composition/TextInput';
import {testProps} from '@utils/componentCustomProps';
import CalendarModal from '@composition/CalendarModal';
import TimePickerModal from '@composition/TimePickerModal';

const DateTimeInputPicker = (props: PropsType): React$Node => {
  const {
    calendarModalProp,
    timePickerModalProp,
    mode,
    ...textInputProps
  } = props;

  const renderDateModes = () => {
    if (mode === 'dateTime') {
      return (
        <>
          {calendarModalProp?.showVisibleCalendarModal && (
            <CalendarModal
              showVisible={calendarModalProp?.showVisibleCalendarModal}
              handleOnApply={calendarModalProp?.handleCalendarOnApply}
              handleOnCancel={calendarModalProp?.handleCalendarOnCancel}
              calendarProps={calendarModalProp?.calendarProps}
            />
          )}
          {timePickerModalProp?.showVisibleTimePickerModal && (
            <TimePickerModal
              mode="body2"
              timePickerLabel={timePickerModalProp?.timePickerLabel}
              onBackdropPress={timePickerModalProp?.onBackdropPress}
              timePickerProps={timePickerModalProp?.timePickerProps}
            />
          )}
        </>
      );
    } else if (mode === 'date') {
      return (
        calendarModalProp?.showVisibleCalendarModal && (
          <CalendarModal
            showVisible={calendarModalProp?.showVisibleCalendarModal}
            handleOnApply={calendarModalProp?.handleCalendarOnApply}
            handleOnCancel={calendarModalProp?.handleCalendarOnCancel}
            calendarProps={calendarModalProp?.calendarProps}
          />
        )
      );
    }
  };

  return (
    <Container>
      <TextInput {...testProps('dateTimeInputPicker')} {...textInputProps} />
      {renderDateModes()}
    </Container>
  );
};

export default DateTimeInputPicker;
