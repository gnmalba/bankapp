import React from 'react';
import {render} from '@testing-library/react-native';
import KeyboardDismissibleViewSv from '../KeyboardDismissibleViewSv';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('KeyboardDismissibleViewSv', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <KeyboardDismissibleViewSv />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
