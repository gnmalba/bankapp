/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  children: React$Node,
  scrollEnabled: boolean,
};
