## KeyboardDismissibleViewSv
KeyboardDismissibleViewSv component.

# Usage
```js
import KeyboardDismissibleViewSv from '@composition/KeyboardDismissibleViewSv';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
