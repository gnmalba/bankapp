/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {} from './Styled';
import {ScrollView} from 'react-native';

const KeyboardDismissibleViewSv = (props: PropsType): React$Node => {
  return (
    <ScrollView
      scrollEnabled={props.scrollEnabled}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}>
      {props.children}
    </ScrollView>
  );
};

export default KeyboardDismissibleViewSv;
