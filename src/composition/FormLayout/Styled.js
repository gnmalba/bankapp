/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {HEIGHT, WIDTH} from '@utils/constants';
import {moderateScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  margin-top: ${(props) =>
    (props.customMarginTop && props.customMarginTop + 'px') || 16 + 'px'};
  padding: ${moderateScale(36)}px;
`;
export const StyledImageBackground: ComponentType<any> = styled.ImageBackground.attrs(
  (props) => {
    return {
      source: props.theme?.layout?.src,
    };
  },
)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: -9999;
  width: ${WIDTH}px;
  height: ${(props) => (props.isFullScreen ? '-1px' : HEIGHT + 'px')} 
  padding-bottom: 40px;
`;
