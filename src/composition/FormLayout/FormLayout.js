/**
 * @format
 * @flow
 */

import React from 'react';
import {StyledImageBackground, Container} from './Styled';

import type {PropsType} from './types';

const FormLayout = (props: PropsType): React$Node => {
  const {isFullScreen, customMarginTop} = props;
  return (
    <StyledImageBackground isFullScreen={isFullScreen}>
      <Container customMarginTop={customMarginTop}>{props.children}</Container>
    </StyledImageBackground>
  );
};

export default FormLayout;
