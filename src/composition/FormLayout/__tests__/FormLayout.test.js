import React from 'react';
import {render} from '@testing-library/react-native';
import FormLayout from '../FormLayout';
import {testProps} from '@utils/componentCustomProps';

describe('FormLayout', () => {
  it('Should work as expected', () => {
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(<FormLayout {...testProps('formLayoutTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
