/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  children: any,
  isFullScreen?: boolean,
  customMarginTop?: number,
};
