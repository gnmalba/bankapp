/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  isVisible: boolean,
  theme?: Object,
};
