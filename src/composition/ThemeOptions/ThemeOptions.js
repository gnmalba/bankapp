/**
 * @format
 * @flow
 */

// React modules
import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Modal, TouchableOpacity, Platform} from 'react-native';

// Components
import Checkbox from '@components/Checkbox';
import IosModalManager from '@components/IOSModalManager';

// Utils
import type {PropsType} from './types';
import * as S from './Styled';
import {verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';
import {getTheme} from '@ducks/config/selectors';
import {setTheme} from '@ducks/config/actions';

const ThemeOptions = (props: PropsType): React$Node => {
  const currentTheme = useSelector(getTheme);
  const dispatch = useDispatch();
  const {isVisible, theme} = props;
  const [shouldBeVisible, setShouldBeVisible] = useState(true);

  const showGuidedTour = async () => {
    dispatch({
      type: 'SHOW_GUIDED_TOUR',
    });
    setShouldBeVisible(false);
  };

  const setLightTheme = () => dispatch(setTheme('light'));

  const setDarkTheme = () => dispatch(setTheme('dark'));
  return (
    <>
      <Modal
        animationType="none"
        transparent
        visible={isVisible && shouldBeVisible}>
        <S.MainContainer>
          <S.Container2
            onTouchStart={() => {
              showGuidedTour();
            }}
          />
          <S.Container3 {...testProps('ThemeOptionContainer')} theme={theme}>
            <S.DashboardSetupText {...testProps('ThemeOptionTitle')}>
              Dashboard Setup
            </S.DashboardSetupText>
            <S.MessageText {...testProps('ThemeOptionDescription')}>
              You can choose a theme for the dashboard by selecting either Light
              or Dark Mode
            </S.MessageText>

            <S.SelectionContainer
              {...testProps('ThemeOptionChecboxContainer')}
              topMargin={Platform.select({
                android: verticalScale(50),
                ios: verticalScale(26),
              })}>
              <Checkbox
                {...testProps('ThemeOptionChecboxLight')}
                title="Light"
                checked={currentTheme === 'light'}
                onPress={setLightTheme}
                isCheckbox={false}
              />
              <Checkbox
                {...testProps('ThemeOptionChecboxDark')}
                title="Dark"
                checked={currentTheme === 'dark'}
                onPress={setDarkTheme}
                isCheckbox={false}
              />
            </S.SelectionContainer>

            <S.ButtonContainer>
              <TouchableOpacity
                {...testProps('ThemeOptionContinueLoginButton')}
                onPress={showGuidedTour}>
                <S.ButtonText
                  {...testProps('ThemeOptionContinueLoginButtonLabel')}
                  theme={theme}>
                  Continue
                </S.ButtonText>
              </TouchableOpacity>
            </S.ButtonContainer>
          </S.Container3>
        </S.MainContainer>
      </Modal>
    </>
  );
};

export default (IosModalManager(ThemeOptions): any);
