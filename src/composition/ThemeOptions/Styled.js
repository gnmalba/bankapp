/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale, scale, moderateScale} from '@utils/scale';
import {DARK_THEME} from '@utils/constants';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.4);
`;

export const Container2: ComponentType<any> = styled.View`
  background-color: transparent;
  flex: 1;
`;

export const Container3: ComponentType<any> = styled.View`
  height: ${verticalScale(245)}px;
  align-content: flex-end;
  justify-content: flex-end;
  padding: ${scale(32)}px;
  padding-top: ${verticalScale(24)}px;
  background-color: ${(props) =>
    props.theme.color.BACKGROUND_100 || DARK_THEME.color.BACKGROUND_100};
  color: ${(props) =>
    props.theme.color.TEXT_300_HIGH || DARK_THEME.color.TEXT_300_HIGH};
`;

export const SelectionContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  align-content: center
  justify-content: center;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const MessageText: ComponentType<any> = styled.Text`
  margin-top: ${verticalScale(18)}px;
  font-size: ${moderateScale(14)}px;
  justify-content: center
  align-items: center
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  margin-bottom: ${verticalScale(28)}px;
`;

export const ButtonText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(16)}px;
  font-weight: 700;
  color: ${(props) =>
    props.theme.color.SECONDARY_DEFAULT || DARK_THEME.color.SECONDARY_DEFAULT};
`;

export const DashboardSetupText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(20)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-weight: bold;
`;
