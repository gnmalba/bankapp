import React from 'react';
import {render} from '@testing-library/react-native';
import ThemeOptions from '../ThemeOptions';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

const store = {
  config: {
    theme: 'dark',
  },
};

describe('ThemeOptions', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ThemeOptions />
        </ThemeProvider>
      </MockProvider>,
    );
  });
});
