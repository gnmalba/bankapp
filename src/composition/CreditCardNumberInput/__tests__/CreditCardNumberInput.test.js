import React from 'react';
import {render} from '@testing-library/react-native';
import CreditCardNumberInput from '../CreditCardNumberInput';
import {testProps} from '@utils/componentCustomProps';

describe('CreditCardNumberInput', () => {
  it('Should work as expected', () => {
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <CreditCardNumberInput {...testProps('EnrollmentAccountNumberInput')} />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
