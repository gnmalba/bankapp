## CreditCardNumberInput
CreditCardNumberInput component.

# Usage
```js
import CreditCardNumberInput from '@composition/CreditCardNumberInput';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
