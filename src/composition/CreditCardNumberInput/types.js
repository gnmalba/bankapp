/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  accountNo?: string | null,
  label?: string | null,
  placeholderText?: string | null,
  maxLength?: number | null,
  handleInputChange?: () => (string) => void,
  handleResetField?: () => void,
  handleSetFieldTouched?: () => void,
  handleChangeCreditNumber?: () => void,
  error?: boolean,
  value?: string,
  isTouched?: boolean,
  isValid?: boolean,
  handleOnBlur?: Function,
};
