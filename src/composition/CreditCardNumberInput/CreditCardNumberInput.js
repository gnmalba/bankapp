/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import FormikInput from '@composition/FormikInput';

const CreditCardNumberInput = (props: PropsType): React$Node => {
  const {
    accountNo,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
    isValid,
    label,
    placeholderText,
    maxLength,
    handleOnBlur,
  } = props;

  return (
    <FormikInput
      testID="EnrollmentAccountNumberInput"
      name="accountNo"
      label={label}
      returnKeyType="done"
      placeholderText={placeholderText}
      // description="This can either be your 13-digit account number or 16-digit card number."
      value={accountNo}
      error={error}
      isTouched={isTouched}
      isValid={isValid}
      handleInputChange={handleInputChange}
      handleResetField={handleResetField}
      handleSetFieldTouched={handleSetFieldTouched}
      keyboardType="number-pad"
      maxLength={maxLength}
      textWidth={'98%'}
      handleOnBlur={handleOnBlur}
    />
  );
};

export default CreditCardNumberInput;
