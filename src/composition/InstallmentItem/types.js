/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  amount?: string,
  duration?: string,
  status?: string,
  date?: string,
  monthlyAmortization?: string,
  onPress: () => void,
};
