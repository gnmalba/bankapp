## InstallmentItem
InstallmentItem component.

# Usage
```js
import InstallmentItem from '@composition/InstallmentItem';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
