/**
 * @flow
 */

import Text from '@components/Text';
import {verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Feather from 'react-native-vector-icons/Feather';

export const getStatusColor = (status: string, theme: any): string => {
  switch (status) {
    case 'IN PROGRESS':
      return theme.color.YELLOW_STATUS;
    case 'PENDING':
      return theme.color.YELLOW_STATUS;
    case 'APPROVED':
      return theme.color.GREEN_STATUS;
    case 'DECLINED':
      return theme.color.RED_STATUS;
    case 'DECLINE':
      return theme.color.RED_STATUS;
    default:
      return theme.color.TEXT_300_HIGH;
  }
};

export const Container: ComponentType<any> = styled.View`
  height: ${verticalScale(115)}px;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  margin-top: ${verticalScale(8)}px;
  margin-bottom: ${verticalScale(8)}px;
  margin-left: ${verticalScale(24)}px;
  margin-right: ${verticalScale(24)}px;
  border-radius: 8px;
  display: flex;
  flex-direction: row;
  padding: ${verticalScale(16)}px;
`;

export const LeftSide: ComponentType<any> = styled.View`
  display: flex;
`;

export const RightSide: ComponentType<any> = styled.View`
  margin-left: auto;
  display: flex;
`;

export const StatusText: ComponentType<any> = styled(Text)`
  margin-top: auto;
  color: ${(props) => getStatusColor(props.status, props.theme)};
`;

export const DateWrapper: ComponentType<any> = styled.View`
  margin-top: auto;
  align-items: flex-end;
`;

export const RightIcon: ComponentType<any> = styled(Feather).attrs((props) => ({
  name: 'chevron-right',
  size: 30,
  color: props.theme.color.TEXT_100_HIGH,
}))``;
