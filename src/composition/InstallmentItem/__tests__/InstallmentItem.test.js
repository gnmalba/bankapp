import {render} from '@testing-library/react-native';
import {DARK_THEME} from '@utils/constants';
import React from 'react';
import {ThemeProvider} from 'styled-components/native';
import InstallmentItem from '../InstallmentItem';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

describe('InstallmentItem', () => {
  it.each(['IN PROGRESS', 'PENDING', 'APPROVED', 'DECLINED', 'DECLINE', ''])(
    'Should work as expected',
    (status) => {
      const props = {
        amount: '1000',
        duration: '2',
        status,
        monthlyAmortization: '422',
        onPress: jest.fn(),
        date: 'February 21, 2020',
      };

      const component = render(
        <ThemeProvider theme={DARK_THEME}>
          <InstallmentItem {...props} />
        </ThemeProvider>,
      );

      expect(component.toJSON()).toMatchSnapshot();
    },
  );
});
