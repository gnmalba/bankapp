/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {
  Container,
  DateWrapper,
  LeftSide,
  RightSide,
  StatusText,
  RightIcon,
} from './Styled';
import Text from '@components/Text';
import Separator from '@components/Separator';
import {IconHolder} from '@composition/TouchableDisplay/Styled';
import {testProps} from '@utils/componentCustomProps';
import {Pressable} from 'react-native';
import {numberFormatClean} from '@utils/numberInputs';

const InstallmentItem = (props: PropsType): React$Node => {
  const {testID = ''} = props;
  return (
    <Pressable onPress={props.onPress}>
      <Container>
        <LeftSide>
          <Text mode="body1" {...testProps(`${testID}Amount`)}>
            PHP {numberFormatClean(props.amount)}
          </Text>
          <Separator space={4} />
          <Text
            mode="overline2"
            themeColor="TEXT_100_HIGH"
            {...testProps(`${testID}MonthlyAmortization`)}>
            PHP {numberFormatClean(props.monthlyAmortization)} for{' '}
            {props.duration} months
          </Text>
          <StatusText
            mode="overline2"
            status={props.status}
            {...testProps(`${testID}Status`)}>
            {props.status}
          </StatusText>
        </LeftSide>
        <RightSide>
          <IconHolder>
            <RightIcon {...testProps(`${testID}Chevron`)} />
          </IconHolder>
          <DateWrapper>
            <Text
              mode="overline2"
              themeColor="TEXT_100_HIGH"
              {...testProps(`${testID}DateApplied`)}>
              DATE APPLIED
            </Text>
            <Separator space={4} />
            <Text mode="body2" {...testProps(`${testID}Date`)}>
              {props.date}
            </Text>
          </DateWrapper>
        </RightSide>
      </Container>
    </Pressable>
  );
};

export default InstallmentItem;
