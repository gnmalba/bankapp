/**
 * @format
 * @flow
 */

import React from 'react';

import BasicTextInput from '@components/BasicTextInput';
import Text from '@components/Text';

import type {LabelledTextInputType} from '@utils/commonTypes';
import {LabelContainer} from './Styled';

import {testProps} from '@utils/componentCustomProps';

const LabelledTextInput = (props: LabelledTextInputType): React$Node => {
  const {
    label,
    labelSuffix,
    inputProps,
    labelProps,
    testID = '',
    ...rest
  } = props;
  return (
    <>
      <LabelContainer>
        <Text {...testProps(`${testID}Label`)} {...labelProps} mode="body2">
          {label}
        </Text>
        {labelSuffix ? labelSuffix : <></>}
      </LabelContainer>
      <BasicTextInput
        {...testProps(`${testID}Field`)}
        {...inputProps}
        {...rest}
      />
    </>
  );
};

export default LabelledTextInput;
