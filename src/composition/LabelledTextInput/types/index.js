/**
 * @flow
 */

import type {LabelledTextInputType} from '@utils/commonTypes';

export type PropsType = {
  ...LabelledTextInputType,
};
