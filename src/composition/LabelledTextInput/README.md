## LabelledTextInput
LabelledTextInput component.

# Usage
```js
import LabelledTextInput from '@composition/LabelledTextInput';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
SampleProp                | boolean               | false                   | This is only a sample
