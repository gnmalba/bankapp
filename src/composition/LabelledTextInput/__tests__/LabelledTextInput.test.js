import React from 'react';
import {Text} from 'react-native';
import {render} from '@testing-library/react-native';
import LabelledTextInput from '../LabelledTextInput';
import {DARK_THEME} from '@utils/constants';

describe('LabelledTextInput', () => {
  it.each([undefined, <Text>Something</Text>])(
    'Should work as expected',
    (suffix) => {
      const all = render(
        <LabelledTextInput
          labelProps={{theme: DARK_THEME}}
          inputProps={{theme: DARK_THEME}}
          labelSuffix={suffix}
        />,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
