/**
 * @flow
 */

// Uncomment this line to start using styled components.
import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {moderateScale} from '@utils/scale';

export const LabelContainer: ComponentType<any> = styled.View`
  padding: 0px 0px ${moderateScale(12)}px 0px;
  flex-direction: row;
  align-items: center;
`;
