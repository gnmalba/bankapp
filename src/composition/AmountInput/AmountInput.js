/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';

import type {PropsType} from './types';
import {
  Container,
  CurrencyTextWrapper,
  IconHolder,
  InputBoxWrapper,
  InputDivider,
  LabelContainer,
  StyledBasicTextInput,
} from './Styled';
import Text from '@components/Text';
import {useDispatch} from 'react-redux';
import {numberFormatClean, removeCommas} from '@utils/numberInputs';
import Separator from '@components/Separator';
import {testProps} from '@utils/componentCustomProps';
import {ErrorIcon} from '@composition/EnrollAccount/Styled';

const MAX_LENGTH = {
  DECIMAL: 19,
  NONDECIMAL: 16,
  ONBLUR: 30,
};

const AmountInput = (props: PropsType): React$Node => {
  const {
    placeholder,
    label,
    currency,
    onBlur,
    value,
    dispatchAction,
    otherProps,
    errorMessage,
    onFocus,
  } = props;

  const dispatch = useDispatch();
  const [amount, setAmount] = useState(value || '');
  const [isFocused, setFocused] = useState(false);
  const [maxLength, setMaxLength] = useState(MAX_LENGTH.NONDECIMAL);

  useEffect(() => {
    if (value) {
      setAmount(numberFormatClean(value));
    } else {
      setAmount('');
    }
  }, [value]);

  const handleLength = (text: string) => {
    setMaxLength(MAX_LENGTH.DECIMAL);

    return text;
  };

  const handleFocus = () => {
    setFocused(true);
    if (onFocus) {
      onFocus();
    }
    if (amount.includes('.00')) {
      setAmount(amount.replace('.00', ''));
    }
  };

  const handleBlur = () => {
    setFocused(false);
    if (onBlur) {
      onBlur(amount);
    }

    if (amount) {
      setAmount(numberFormatClean(amount));
    }

    if (dispatchAction) {
      if (otherProps?.installmentType) {
        dispatch(
          dispatchAction(
            otherProps.installmentType,
            Number(removeCommas(amount)),
          ),
        );
      } else {
        dispatch(dispatchAction(amount));
      }
    }

    setMaxLength(MAX_LENGTH.ONBLUR);
  };

  const handleChange = (text: string) => {
    let newText: string = text;

    if (isNaN(newText)) {
      newText = newText.replace(/[^0-9.]/g, '');
      if (newText.split('.').length > 2) {
        newText = newText.replace(/\.+$/, '');
      }
    }

    if (newText.startsWith('.')) {
      newText = '0' + newText;
    }

    if (
      newText.startsWith('0') &&
      newText.length > 1 &&
      !newText.includes('.')
    ) {
      newText = newText[1];
    }

    if (newText.includes('.')) {
      const indexOfDot = newText.indexOf('.');
      const decimals = newText.split('.');
      if (decimals.length) {
        const rightSide = decimals[1];
        if (rightSide.length > 2) {
          newText = newText.substring(0, indexOfDot + 3);
        }
      }
    }

    newText = handleLength(newText);

    setAmount(newText);
  };

  return (
    <Container>
      <LabelContainer>
        <Text {...testProps(`Lbl ${label}`)}>{label}</Text>
      </LabelContainer>
      <InputBoxWrapper
        hasError={errorMessage}
        isFocused={isFocused}
        {...testProps('AmountFieldWrapper')}>
        <CurrencyTextWrapper>
          <Text mode="body1" {...testProps(`${label} Currency`)}>
            {currency}
          </Text>
        </CurrencyTextWrapper>
        <InputDivider />
        <StyledBasicTextInput
          returnKeyType="done"
          onBlur={handleBlur}
          placeholder={placeholder}
          keyboardType="decimal-pad"
          value={`${amount}`}
          onChangeText={handleChange}
          onFocus={handleFocus}
          {...testProps(`${label} Field`)}
          maxLength={maxLength}
        />
        {Boolean(errorMessage) && (
          <IconHolder>
            <ErrorIcon />
          </IconHolder>
        )}
      </InputBoxWrapper>
      <Separator space={5} />
      <Text
        mode="inputerror"
        themeColor="RED_STATUS"
        {...testProps('AmountInlineError')}>
        {errorMessage}
      </Text>
    </Container>
  );
};

export default AmountInput;
