/**
 * @flow
 */

import BasicTextInput from '@components/BasicTextInput';
import {moderateScale, verticalScale, scale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(25)}px;
`;

export const LabelContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(12)}px;
`;

export const InputBoxWrapper: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  padding-right: 5px;
  background-color: ${(props) => {
    const {state, theme} = props;
    return theme.textInput?.backgroundColor[state || 'default'];
  }};
  border-color: ${(props) => {
    if (props.hasError) {
      return props.theme.color.RED_STATUS;
    } else if (props.isFocused) {
      return props.theme.color.SECONDARY_DEFAULT;
    } else {
      return props.theme.textInput?.border.default;
    }
  }};
  border-width: 1px;
  border-radius: 5px;
  align-items: center;
  border-radius: ${scale(5)}px;
`;

export const CurrencyTextWrapper: ComponentType<any> = styled.View`
  justify-content: center;
  padding: ${verticalScale(16)}px;
`;

export const InputDivider: ComponentType<any> = styled.View`
  border-right-width: 1px;
  border-right-color: ${(props) => props.theme.color.TEXT_300_LOW};
  height: 60%;
`;

export const StyledBasicTextInput: ComponentType<any> = styled(
  BasicTextInput,
).attrs((props) => ({
  nativeProps: {
    style: {
      fontSize: moderateScale(19),
      flex: 1,
      borderWidth: 0,
    },
  },
}))``;

export const IconHolder: ComponentType<any> = styled.View`
  margin: 0 ${verticalScale(12)}px;
`;
