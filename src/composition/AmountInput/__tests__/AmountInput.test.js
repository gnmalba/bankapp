/*import React from 'react';
import {render} from '@testing-library/react-native';
import AmountInput from '../AmountInput';
import {testProps} from '@utils/componentCustomProps';

describe('AmountInput', () => {
  it('Should work as expected', () => {
    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(<AmountInput {...testProps('amountInputTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
*/

import {fireEvent, render} from '@testing-library/react-native';
import AmountInput from '@composition/AmountInput';
import React from 'react';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('@utils/componentCustomProps', () => ({
  generateId: (reference) => {
    return {testID: reference, accessibilityLabel: reference};
  },
  testProps: (reference) => ({
    testID: reference,
    accessibilityLabel: reference,
  }),
}));

describe('AmountInput', () => {
  let SAMPLE_INPUTS = {
    normal: '123456789',
    decimal: '1234.123',
    nanInput: 'abc',
    nanInputMultiplePeriod: '..',
    startsWithPeriod: '.2345',
    startsWithZero: '05',
  };
  let SAMPLE_OUTPUTS = {
    decimal: '1,234.12',
    normal: '123,456,789.00',
    nanInput: '',
    nanInputMultiplePeriod: '',
    startsWithPeriod: '0.23',
    startsWithZero: '5.00',
  };

  it('Should accept empty value prop', () => {
    const label = 'amount';

    const component = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <AmountInput
            placeholder="Enter amount"
            label={label}
            currency="PHP"
            value=""
          />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(component.toJSON()).toBeTruthy();
  });

  it('Should render correctly when there is an error', () => {
    const label = 'amount';

    const component = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <AmountInput
            placeholder="Enter amount"
            label={label}
            currency="PHP"
            value=""
            errorMessage={'Error message'}
          />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(component.toJSON()).toBeTruthy();
  });

  it('Should follow validations', () => {
    const label = 'amount';
    const testId = label + ' Field';

    const {getByTestId} = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <AmountInput
            placeholder="Enter amount"
            label={label}
            currency="PHP"
            value="123"
            onFocus={jest.fn()}
            onBlur={jest.fn()}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    const input = getByTestId(testId);
    fireEvent(input, 'onFocus');
    fireEvent.changeText(input, SAMPLE_INPUTS.normal);
    fireEvent(input, 'onBlur');
    expect(input.props.value).toBe(SAMPLE_OUTPUTS.normal);

    fireEvent.changeText(input, SAMPLE_INPUTS.decimal);
    fireEvent(input, 'onBlur');
    expect(input.props.value).toBe(SAMPLE_OUTPUTS.decimal);

    fireEvent.changeText(input, SAMPLE_INPUTS.nanInput);
    fireEvent(input, 'onBlur');
    expect(input.props.value).toBe(SAMPLE_OUTPUTS.nanInput);

    fireEvent.changeText(input, SAMPLE_INPUTS.nanInputMultiplePeriod);
    fireEvent(input, 'onBlur');
    expect(input.props.value).toBe(SAMPLE_OUTPUTS.nanInputMultiplePeriod);

    fireEvent.changeText(input, SAMPLE_INPUTS.startsWithZero);
    fireEvent(input, 'onBlur');
    expect(input.props.value).toBe(SAMPLE_OUTPUTS.startsWithZero);

    fireEvent.changeText(input, SAMPLE_INPUTS.startsWithPeriod);
    fireEvent(input, 'onBlur');
    expect(input.props.value).toBe(SAMPLE_OUTPUTS.startsWithPeriod);
  });

  it.each([null, 'BT'])(
    'Should dispatch action when dispatch props are provided',
    (installmentType) => {
      const label = 'amount';
      const testId = label + ' Field';

      const dispatchAction = () => ({type: jest.mock()});

      const {getByTestId} = render(
        <MockProvider>
          <ThemeProvider theme={DARK_THEME}>
            <AmountInput
              placeholder="Enter amount"
              label={label}
              currency="PHP"
              value="123"
              onFocus={jest.fn()}
              onBlur={jest.fn()}
              dispatchAction={dispatchAction}
              otherProps={{installmentType}}
            />
          </ThemeProvider>
        </MockProvider>,
      );

      const input = getByTestId(testId);

      fireEvent(input, 'onFocus');
      fireEvent.changeText(input, SAMPLE_INPUTS.normal);
      fireEvent(input, 'onBlur');
      // expect(dispatchAction).toHaveBeenCalled();
    },
  );

  it('Should work when default values has zeros', () => {
    const label = 'amount';
    const testId = label + ' Field';

    const {getByTestId} = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <AmountInput
            placeholder="Enter amount"
            label={label}
            currency="PHP"
            value="123.00"
            onFocus={jest.fn()}
            onBlur={jest.fn()}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    const input = getByTestId(testId);
    fireEvent(input, 'onFocus');
    expect(input.props.value).toBe('123');
  });
});
