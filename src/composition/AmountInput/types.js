/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  placeholder: string,
  currency: string,
  label: string,
  onBlur?: (amount: string | number) => void,
  value?: string,
  dispatchAction?: any,
  otherProps?: Object,
  onChange?: any,
  errorMessage?: string,
  onFocus?: () => void,
  onBlurAmount?: (amount: any) => void,
};
