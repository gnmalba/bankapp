/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  placeholder?: string,
  maxLength?: number,
  onChange?: (text: string) => void,
  value?: string,
  setValue?: any,
  onBlur?: () => void,
  onFocus?: () => void,
  creditSOA?: boolean,
  currentTab?: string,
};
