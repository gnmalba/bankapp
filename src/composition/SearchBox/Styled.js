/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';

// Third party libraries
import {scale, verticalScale} from '@utils/scale';
import styled from 'styled-components/native';

export const SearchContainer: ComponentType<any> = styled.View`
  flex-direction: row;
`;

export const IconContainer: ComponentType<any> = styled.View`
  position: absolute;
  left: ${scale(-25)}px;
  top: ${verticalScale(20)}px;
`;
