/**
 * @format
 * @flow
 */

// React modules
import React, {useEffect} from 'react';

// Components
import TextInput from '@composition/TextInput';

// Utils
import type {PropsType} from './types';
import {testProps} from '@utils/componentCustomProps';
import {SearchContainer} from './Styled';

const SearchBox = (props: PropsType): React$Node => {
  const {
    placeholder,
    onChange,
    value,
    setValue,
    onBlur,
    onFocus,
    maxLength,
    creditSOA,
    currentTab,
  } = props;

  const [inputState, setInputState] = React.useState('default');
  const [previousTab, setPreviousTab] = React.useState('');
  const handleClearInputs = () => {
    if (setValue) {
      setValue('');
    }
  };

  useEffect(() => {
    if (creditSOA) {
      setPreviousTab(previousTab);
      if (currentTab !== previousTab && setValue) {
        setPreviousTab(currentTab);
        setValue('');
      }
    }
  }, [creditSOA, setValue, currentTab, previousTab]);

  return (
    <>
      <SearchContainer>
        <TextInput
          {...testProps('searchBox')}
          maxLength={maxLength || 50}
          icon={'search'}
          placeholder={placeholder}
          onChangeText={onChange}
          value={value}
          state={inputState}
          handleClear={handleClearInputs}
          onFocus={() => {
            setInputState('active');
            onFocus && onFocus();
          }}
          textWidth="85%"
          onBlur={() => {
            setInputState('default');
            onBlur && onBlur();
          }}
          dividerHeight={46}
        />
      </SearchContainer>
    </>
  );
};

export default SearchBox;
