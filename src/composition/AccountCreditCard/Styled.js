/**
 * @flow
 */

// Utils
import {moderateScale, verticalScale} from '@utils/scale';
import {Text} from 'react-native';
import styled from 'styled-components/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

const getDisabledTextColor = (disabled: boolean): string => {
  return disabled ? 'TEXT_300_LOW' : 'TEXT_100_HIGH';
};

const Container: any = styled.View`
  display: flex;
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: 8px;
`;

const Header: any = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const AccountDetails: any = styled.View`
  display: flex;
  max-width: 85%;
  flex-direction: column;
`;

const AccountNumber: any = styled(Text)`
  color: ${(props) => props.theme?.color.TEXT_100_HIGH};
  font-style: normal;
  font-family: MyriadPro-Bold;
  font-weight: 700;
  font-size: ${moderateScale(10)}px;
  letter-spacing: ${moderateScale(0.846154)}px;
  margin-top: ${verticalScale(5)}px;
`;

const PrimaryContainer: any = styled.View`
  margin-top: ${verticalScale(5)}px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: flex-start;
`;

const PrimaryText: any = styled(Text)`
  color: ${(props) => props.theme?.color.GREEN_STATUS};
  font-style: normal;
  font-weight: 600;
  font-size: ${moderateScale(12)}px;
  font-family: MyriadPro-Semibold;
`;

const BottomContainer: any = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: ${verticalScale(32)}px;
`;

const BalanceContainer: any = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  flex: 1;
  align-items: center;
`;

const BalanceDetails: any = styled.View`
  display: flex;
  margin-bottom: ${verticalScale(5)}px;
`;

const BalancePair: any = styled.View`
  display: flex;
  flex-direction: column;
  margin-bottom: ${verticalScale(16)}px;
`;

const BalanceText: any = styled.View`
  text-align: right;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-end;
`;

const Label: any = styled(Text)`
  color: ${(props) => props.theme?.color.TEXT_100_HIGH};
  font-style: normal;
  font-family: MyriadPro-Bold;
  font-weight: 700;
  font-size: ${moderateScale(10)}px;
  letter-spacing: ${moderateScale(0.846154)}px;
  margin-bottom: ${10}px;
`;

const AccountNumberDisabled: typeof AccountNumber = styled(AccountNumber)`
  color: ${(props) => props.theme?.color[getDisabledTextColor(props.disabled)]};
`;

const LabelDisabled: typeof Label = styled(Label)`
  color: ${(props) => props.theme?.color[getDisabledTextColor(props.disabled)]};
`;

const WarningIcon: any = styled(Ionicons).attrs((props) => ({
  name: 'warning',
  size: 20,
  color: props.theme.color.RED_STATUS,
}))``;

export {
  Container,
  Header,
  AccountDetails,
  AccountNumber,
  BalanceContainer,
  BottomContainer,
  PrimaryText,
  PrimaryContainer,
  BalanceText,
  BalanceDetails,
  Label,
  BalancePair,
  AccountNumberDisabled,
  LabelDisabled,
  WarningIcon,
};
