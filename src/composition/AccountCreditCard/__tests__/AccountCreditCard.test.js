import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import AccountCreditCard from '../AccountCreditCard';
import {testProps} from '@utils/componentCustomProps';

describe('AccountCreditCard', () => {
  it.each(['accountCreditCardTestId', ''])(
    'Should work as expected',
    (testId) => {
      const all = render(
        <AccountCreditCard
          {...testProps(testId)}
          onClick={testId ? () => {} : undefined}
          isPrimary={testId === '' ? true : undefined}
        />,
      );
      if (testId) {
        const el = all.getByTestId(testId);
        fireEvent(el, 'onPress');
      }
      expect(all.toJSON()).toMatchSnapshot();
    },
  );

  it.each([() => {}, undefined])('Should work as expected', (func) => {
    const all = render(
      <AccountCreditCard
        {...testProps('accountCreditCardTestId')}
        onClick={func}
      />,
    );
    const el = all.getByTestId('accountCreditCardTestId');
    fireEvent(el, 'onPress');
  });
});
