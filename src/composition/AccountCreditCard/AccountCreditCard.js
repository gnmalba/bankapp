/**
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import Entypo from 'react-native-vector-icons/Entypo';
import {TouchableOpacity} from 'react-native-gesture-handler';

import CircularProgressBar from '@composition/CircularProgressBar';
import Text from '@components/Text';
import {moderateScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

import {
  Container,
  Header,
  AccountDetails,
  AccountNumber,
  BottomContainer,
  PrimaryContainer,
  PrimaryText,
  BalanceContainer,
  BalanceDetails,
  BalancePair,
  Label,
  AccountNumberDisabled,
  LabelDisabled,
  WarningIcon,
} from './Styled';

import type {AccountCreditCardType} from './types';

const LABELS = {
  OUTSTANDING_BALANCE: 'OUTSTANDING BALANCE',
  AVAILABLE_CREDIT: 'AVAILABLE CREDIT',
  CREDIT_LIMIT: 'CREDIT LIMIT',
};

const AccountCreditCard = (props: AccountCreditCardType): React$Node => {
  const {
    name,
    accountNumber,
    isPrimary = false,
    onClick,
    outstandingBalance,
    availableCredit,
    creditLimit,
    theme,
    testID = '',
    uniqueIndex,
    calculatedBalance,
  } = props;

  const handleOnPress = () => {
    if (onClick) {
      onClick();
    }
  };

  const renderItemValue = (
    label: string,
    value: string,
    itemTestID: string,
  ): React$Node => {
    const itemTestIdPrefix = uniqueIndex
      ? uniqueIndex + itemTestID
      : itemTestID;

    return (
      <BalancePair>
        <Label theme={theme} {...testProps(`${itemTestIdPrefix}TestId`)}>
          {label}
        </Label>
        <Text
          mode="body1"
          numberOfLines={1}
          {...testProps(`${itemTestIdPrefix}ValueTestId`)}>
          {value}
        </Text>
      </BalancePair>
    );
  };

  const testIdPrefix = uniqueIndex ? uniqueIndex + testID : testID;
  const touchableTestID = uniqueIndex
    ? `${uniqueIndex}widgetItemNavigateButton`
    : 'accountCreditCardTestId';

  return (
    <TouchableOpacity
      {...testProps(touchableTestID)}
      onPress={handleOnPress}
      disabled={!onClick}
      activeOpacity={onClick ? 0.2 : 1}>
      <Container theme={theme}>
        <Header>
          <AccountDetails>
            <Text
              mode="body1"
              numberOfLines={1}
              {...testProps(
                `${testIdPrefix}AccountAlias` || 'accountCreditCardAliasTestId',
              )}>
              {name}
            </Text>
            <AccountNumber
              theme={theme}
              {...testProps(
                `${testIdPrefix}AccountNumber` || 'accountNumberTestId',
              )}>
              {accountNumber}
            </AccountNumber>
            <PrimaryContainer>
              {isPrimary && (
                <PrimaryText
                  theme={theme}
                  {...testProps(
                    `${testIdPrefix}Primary` || 'primaryAccTestId',
                  )}>
                  Primary Account
                </PrimaryText>
              )}
            </PrimaryContainer>
          </AccountDetails>
          <Entypo
            name="chevron-right"
            size={moderateScale(25)}
            color={'#A2BDDB'}
            {...testProps(`${testIdPrefix}NavigateToAccount`)}
          />
        </Header>
        <BottomContainer>
          <BalanceContainer>
            <BalanceDetails>
              {renderItemValue(
                LABELS.OUTSTANDING_BALANCE,
                outstandingBalance,
                'OutstandingBal',
              )}
              {renderItemValue(
                LABELS.AVAILABLE_CREDIT,
                availableCredit,
                'AvailableCredit',
              )}
              {renderItemValue(LABELS.CREDIT_LIMIT, creditLimit, 'CreditLimit')}
            </BalanceDetails>
            <CircularProgressBar
              size="large"
              theme={theme}
              value={calculatedBalance}
              testID={testIdPrefix + 'CreditUsage'}
            />
          </BalanceContainer>
        </BottomContainer>
      </Container>
    </TouchableOpacity>
  );
};

/**
 * Error in card
 */

const AccountCreditCardErrorDetails = (
  testIdPrefix: string,
  alias: string,
  accountNumber: string,
  flag: ?string,
) => (
  <AccountDetails>
    <Text
      mode="body1"
      numberOfLines={1}
      themeColor={flag !== null ? 'TEXT_300_HIGH' : 'TEXT_300_LOW'}
      {...testProps(`${testIdPrefix}AccountAlias`)}>
      {alias}
    </Text>
    {flag !== null ? (
      <AccountNumberDisabled {...testProps(`${testIdPrefix}AccountNumber`)}>
        {accountNumber}
      </AccountNumberDisabled>
    ) : (
      <Text
        mode="body1"
        numberOfLines={1}
        themeColor="TEXT_300_LOW"
        {...testProps(`${testIdPrefix}AccountNumber`)}>
        <WarningIcon /> Cannot be displayed
      </Text>
    )}
  </AccountDetails>
);

const AccountCreditCardErrorBalancePair = (
  testIdPrefix: string,
  label: string,
  disabled?: boolean = true,
) => (
  <BalancePair>
    <LabelDisabled disabled={disabled} {...testProps(`${testIdPrefix}TestId`)}>
      {label}
    </LabelDisabled>
    <Text
      mode="body1"
      numberOfLines={1}
      themeColor={disabled ? 'TEXT_300_LOW' : 'TEXT_300_HIGH'}
      {...testProps(`${testIdPrefix}ValueTestId`)}>
      <WarningIcon /> Cannot be displayed
    </Text>
  </BalancePair>
);

export const AccountCreditCardError = ({
  testIdPrefix,
  uniqueIndex,
  accountNumber,
  alias,
  flag,
  onClick,
}: {
  testIdPrefix: string,
  uniqueIndex: number,
  accountNumber: string,
  alias: string,
  flag: ?string,
  onClick: () => void,
}): React$Node => {
  const isClickable: boolean = flag === null ? false : true;
  const handleClick = () => {
    if (isClickable) {
      onClick();
    }
  };

  return (
    <TouchableOpacity
      {...testProps(
        isClickable
          ? `${uniqueIndex}widgetItemNavigateButton`
          : `${uniqueIndex}accountCreditCardTestId`,
      )}
      activeOpacity={isClickable ? 0.2 : 1}
      onPress={handleClick}>
      <Container>
        <Header>
          {AccountCreditCardErrorDetails(
            testIdPrefix,
            alias,
            accountNumber,
            flag,
          )}
        </Header>
        <BottomContainer>
          <BalanceContainer>
            <BalanceDetails>
              {AccountCreditCardErrorBalancePair(
                `${uniqueIndex}OutstandingBal`,
                LABELS.OUTSTANDING_BALANCE,
                !isClickable,
              )}
              {AccountCreditCardErrorBalancePair(
                `${uniqueIndex}AvailableCredit`,
                LABELS.AVAILABLE_CREDIT,
                !isClickable,
              )}
              {AccountCreditCardErrorBalancePair(
                `${uniqueIndex}CreditLimit`,
                LABELS.CREDIT_LIMIT,
                !isClickable,
              )}
            </BalanceDetails>
            <CircularProgressBar size="large" value={0} />
          </BalanceContainer>
        </BottomContainer>
      </Container>
    </TouchableOpacity>
  );
};

/**
 * End error in card
 */

export default AccountCreditCard;
