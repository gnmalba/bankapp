/**
 * @flow
 */

export type AccountCreditCardType = {
  name: string,
  accountNumber: string | number,
  onClick?: () => void,
  isPrimary?: boolean,
  outstandingBalance: string,
  availableCredit: string,
  creditLimit: string,
  theme?: Object,
  testID?: string,
  uniqueIndex?: number,
  calculatedBalance: number,
};
