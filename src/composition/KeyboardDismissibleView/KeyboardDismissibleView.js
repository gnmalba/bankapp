/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {Container} from './Styled';
import {Keyboard, TouchableWithoutFeedback} from 'react-native';

const KeyboardDismissibleView = (props: PropsType): React$Node => {
  const handlePress = () => {
    if (props.onPress) {
      props.onPress();
    }

    Keyboard.dismiss();
  };

  return (
    <TouchableWithoutFeedback onPress={handlePress}>
      <Container>{props.children}</Container>
    </TouchableWithoutFeedback>
  );
};

export default KeyboardDismissibleView;
