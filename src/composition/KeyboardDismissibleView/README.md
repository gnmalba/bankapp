## KeyboardDismissibleView
KeyboardDismissibleView component.

# Usage
```js
import KeyboardDismissibleView from '@composition/KeyboardDismissibleView';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
