import React from 'react';
import {render} from '@testing-library/react-native';
import KeyboardDismissibleView from '../KeyboardDismissibleView';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('KeyboardDismissibleView', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <KeyboardDismissibleView />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
