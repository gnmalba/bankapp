/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity, View, Platform} from 'react-native';

// Third party libraries
import {connect} from 'react-redux';

// Components
import Text from '@components/Text';

// Utils
import type {PropsType} from './types';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';

const screenName = 'GuidedTourCustomizeDashboard';

const TourCustomizeDashboard = (props: PropsType): React$Node => {
  const children = '';
  const {isVisible, onClose} = props;

  return (
    <>
      <S.StyledTooltip
        {...testProps(screenName + 'ContainerId')}
        onClose={onClose}
        placement={'bottom'}
        isVisible={isVisible}
        animated={true}
        tooltipLeftMargin={Platform.select({android: 40, ios: 50})}
        tooltipTopMargin={Platform.select({
          android: -20,
          ios: verticalScale(30),
        })}
        arrowLeftMargin={Platform.select({android: 110, ios: 110})}
        arrowMarginTop={Platform.select({
          android: -20,
          ios: verticalScale(30),
        })}
        position={null}
        content={
          <S.Container>
            <View>
              <S.Label {...testProps(screenName + 'TitleId')}>
                Customize your dashboard
              </S.Label>
              <S.Message
                {...testProps(screenName + 'DescriptionId')}
                mode="body2">
                You can now have more control on what appears first on your
                dashboard
              </S.Message>
            </View>
            <S.ButtonContainer>
              <TouchableOpacity
                {...testProps(screenName + 'ContinueButtonId')}
                onPress={onClose}>
                <S.ButtonText
                  {...testProps(screenName + 'ContinueButtonLabelId')}
                  mode="body2">
                  Continue
                </S.ButtonText>
              </TouchableOpacity>
            </S.ButtonContainer>
          </S.Container>
        }>
        <Text mode="body1">{children}</Text>
      </S.StyledTooltip>
    </>
  );
};

const mapStateToProps = ({dashboard}) => ({dashboard});
export default (connect(mapStateToProps, null)(TourCustomizeDashboard): any);
