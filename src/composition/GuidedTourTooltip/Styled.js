/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';

// Third party libraries
import Tooltip from 'react-native-walkthrough-tooltip';

// Components
import Text from '@components/Text';

// Utils
import styled from 'styled-components/native';
import {scale, moderateScale, verticalScale} from '@utils/scale';

export const Container: ComponentType<any> = styled.View`
  flex: 1;
  padding: ${scale(12)}px;
`;

export const Label: ComponentType<any> = styled.Text`
  font-weight: bold;
  font-size: ${moderateScale(16)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const Message: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(12)}px;
  margin-top: ${verticalScale(8)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const ButtonText: ComponentType<any> = styled(Text).attrs((props) => ({
  themeColor: 'SECONDARY_DEFAULT',
  mode: 'h3',
}))`
  font-size: ${moderateScale(14)}px;
  font-weight: normal;
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: row-reverse;
  margin-top: ${verticalScale(24)}px;
`;

export const StyledTooltip: ComponentType<any> = styled(Tooltip).attrs(
  (props) => ({
    contentStyle: {
      backgroundColor: props.theme.guidedTourTooltip.background,
      height: null,
      marginLeft: scale(props.tooltipLeftMargin),
      marginTop: verticalScale(props.tooltipTopMargin),
      position: props.position,
      width: scale(240),
    },
    arrowSize: {width: scale(14), height: scale(14)},
    arrowStyle: {
      marginLeft: verticalScale(props.arrowLeftMargin),
      marginTop: verticalScale(props.arrowMarginTop),
    },
    tooltipStyle: {
      elevation: 0,
      shadowOpacity: 0,
    },
  }),
)``;
