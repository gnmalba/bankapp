/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity, View, Platform} from 'react-native';

// Third party libraries
import {connect} from 'react-redux';

// Components
import Text from '@components/Text';

// Utils
import type {PropsType} from './types';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';

const screenName = 'GuidedTourQuickLinks';

const QuickLinks = (props: PropsType): React$Node => {
  const children = '';
  const {isVisible, onClose} = props;

  return (
    <>
      <S.StyledTooltip
        {...testProps(screenName + 'ContainerId')}
        onClose={onClose}
        placement={'top'}
        isVisible={isVisible}
        animated={true}
        tooltipLeftMargin={-12}
        tooltipTopMargin={Platform.select({
          android: -40,
          ios: 0,
        })}
        arrowLeftMargin={Platform.select({android: 3, ios: 3})}
        arrowMarginTop={Platform.select({
          android: -40,
          ios: 0,
        })}
        position={null}
        content={
          <S.Container>
            <View>
              <S.Label {...testProps(screenName + 'TitleId')}>
                Quick Links
              </S.Label>
              <S.Message
                {...testProps(screenName + 'DescriptionId')}
                mode="body2">
                This is where you can find the important transaction shortcuts.
              </S.Message>
            </View>
            <S.ButtonContainer>
              <TouchableOpacity
                {...testProps(screenName + 'ContinueButtonId')}
                onPress={onClose}>
                <S.ButtonText
                  {...testProps(screenName + 'ContinueButtonLabelId')}
                  mode="body2">
                  Continue
                </S.ButtonText>
              </TouchableOpacity>
            </S.ButtonContainer>
          </S.Container>
        }>
        <Text mode="body1">{children}</Text>
      </S.StyledTooltip>
    </>
  );
};

const mapStateToProps = ({dashboard}) => ({dashboard});
export default (connect(mapStateToProps, null)(QuickLinks): any);
