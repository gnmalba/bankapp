/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  isVisible: boolean,
  onClose: Function,
};
