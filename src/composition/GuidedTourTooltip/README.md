## GuidedTourTooltip
GuidedTourTooltip component.

# Usage
```js
import GuidedTourTooltip from '@composition/GuidedTourTooltip';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
