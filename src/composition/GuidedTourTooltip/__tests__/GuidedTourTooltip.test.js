// React modules
import React from 'react';

// Third party libraries
import {render} from '@testing-library/react-native';

// Components
import QuickLinks from '../QuickLinks';
import TourCustomizeDashboard from '../TourCustomizeDashboard';
import TourCustomizeWidgets from '../TourCustomizeWidgets';

// Utils
import {INITIAL_STATE as guidedTourState} from '@ducks/dashboard/reducer';
import MockProvider from '@utils/testFramework';

describe('GuidedTourTooltip', () => {
  const store = {guidedTourState};
  it('Quick links render as expected', async () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <QuickLinks />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Customize dashboard render as expected', async () => {
    const all = render(
      <MockProvider store={store}>
        <TourCustomizeDashboard />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Customize widgets render as expected', async () => {
    const all = render(
      <MockProvider store={store}>
        <TourCustomizeWidgets />
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
