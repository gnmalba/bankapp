/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity, View, Platform} from 'react-native';

// Components
import Text from '@components/Text';

// Utils
import type {PropsType} from './types';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';
import {verticalScale, moderateScale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';

const screenName = 'GuidedTourCustomizeWidget';

const TourCustomizeWidgets = (props: PropsType): React$Node => {
  const children = '';
  const {isVisible, onClose} = props;

  return (
    <>
      <S.StyledTooltip
        {...testProps(screenName + 'ContainerId')}
        onClose={onClose}
        placement={'bottom'}
        isVisible={isVisible}
        animated={true}
        tooltipLeftMargin={Platform.select({
          android: moderateScale(45),
          ios: IS_DEVICE_TABLET ? moderateScale(30) : moderateScale(60),
        })}
        tooltipTopMargin={Platform.select({
          android: verticalScale(43),
          ios: verticalScale(63),
        })}
        arrowLeftMargin={Platform.select({
          android: moderateScale(140),
          ios: moderateScale(116),
        })}
        arrowMarginTop={Platform.select({
          android: verticalScale(30),
          ios: verticalScale(55),
        })}
        position={'absolute'}
        content={
          <S.Container>
            <View>
              <S.Label {...testProps(screenName + 'TitleId')}>
                Customize Widgets
              </S.Label>
              <S.Message
                {...testProps(screenName + 'DescriptionId')}
                mode="body2">
                You can move, delete, and select options! Try it out!
              </S.Message>
            </View>
            <S.ButtonContainer>
              <TouchableOpacity
                {...testProps(screenName + 'ContinueButtonId')}
                onPress={onClose}>
                <S.ButtonText
                  {...testProps(screenName + 'ContinueButtonLabelId')}
                  mode="body2">
                  Continue
                </S.ButtonText>
              </TouchableOpacity>
            </S.ButtonContainer>
          </S.Container>
        }>
        <Text mode="body1">{children}</Text>
      </S.StyledTooltip>
    </>
  );
};

export default TourCustomizeWidgets;
