import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import InformationModal from '../InformationModal';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME, INSTALLLMENT_INFO_FALLBACK} from '@utils/constants';
import {useSelector} from 'react-redux';

jest.mock('react-native-vector-icons', () => {
  return {
    RNVectorIconsManager: jest.mock(),
    createIconSetFromIcoMoon: jest.fn(),
  };
});

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

describe('InformationModal', () => {
  const stores = [
    {
      installments: {
        information: {
          data: INSTALLLMENT_INFO_FALLBACK.C2GO,
          loading: false,
          visible: true,
          isInitial: true,
        },
      },
    },
    {
      installments: {
        information: {
          data: null,
          loading: true,
          visible: true,
          isInitial: false,
        },
      },
    },
    {
      installments: {
        information: {
          data: null,
          loading: false,
          visible: false,
          isInitial: false,
        },
      },
    },
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  it.each(stores)('Should work as expected', (store) => {
    const {loading, isInitial, data, visible} = store.installments.information;
    useSelector.mockImplementation((callback) => {
      return callback(store);
    });

    // For issues with theme being undefined, please pass a theme object to
    // the Styled component that needs it.
    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <InformationModal {...testProps('informationModalTestId')} />
        </ThemeProvider>
      </MockProvider>,
    );

    if (!loading && visible) {
      if (data && data.length && isInitial) {
        const doNotShowCheckbox = all.getByTestId('infoModalDoNotShowCheckbox');
        fireEvent.press(doNotShowCheckbox);
      }

      const getMoreInfoButton = all.getByTestId('infoModalGetMoreInfoBtn');
      fireEvent.press(getMoreInfoButton);

      const closeIconButton = all.getByTestId('infoModalCloseIconTouchable');
      fireEvent.press(closeIconButton);
    }

    expect(all.toJSON()).toMatchSnapshot();
  });
});
