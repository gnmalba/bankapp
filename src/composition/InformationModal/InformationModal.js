/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Linking, TouchableOpacity} from 'react-native';

import type {PropsType} from './types';
import {
  Header,
  StyledIcon,
  Content,
  Title,
  TextBody,
  CheckboxContainer,
} from './Styled';
import Button from '@components/Button';
import Checkbox from '@components/Checkbox';
import NativeLoader from '@components/NativeLoader';
import BottomSheet from '@components/BottomSheet';

import type {InstallmentStateType} from '@ducks/installments/types';
import {
  getInformationModalRequested,
  setInformationModalVisible,
} from '@ducks/installments/actions';
import {saveInformationModal} from '@utils/installments';
import {testProps} from '@utils/componentCustomProps';

const InformationModal = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const {
    information,
    currentInstallmentType,
  }: InstallmentStateType = useSelector((state) => state.installments);
  const {data, loading, visible, isInitial} = information;

  const [doNotShowAgain, setdoNotShowAgain] = useState(false);

  useEffect(() => {
    if (visible && !information.data) {
      dispatch(getInformationModalRequested(currentInstallmentType));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visible, currentInstallmentType, dispatch]);

  const handleSetVisible = () => {
    dispatch(setInformationModalVisible(!visible));

    if (visible) {
      handleDismiss();
    }
  };

  const handleSetDoNotShowAgain = () => {
    setdoNotShowAgain(!doNotShowAgain);
  };

  const handleDismiss = () => {
    if (visible) {
      dispatch(setInformationModalVisible(false));

      if (doNotShowAgain) {
        saveInformationModal(currentInstallmentType);
        setdoNotShowAgain(false);
      }
    }
  };

  const handleButtonPress = () => {
    if (data) {
      Linking.openURL(data.Link);
    }
  };

  const renderContent = (): React$Node => {
    return (
      <>
        <TouchableOpacity
          onPress={handleSetVisible}
          {...testProps('infoModalCloseIconTouchable')}>
          <StyledIcon name="close-a" {...testProps('infoModalCloseIcon')} />
        </TouchableOpacity>

        {loading && <NativeLoader />}
        {!loading && data && (
          <Header>
            <Title {...testProps('infoModalHeader')}>{data.Header}</Title>
            <TextBody {...testProps('infoModalDetails')}>
              {data.Details}
            </TextBody>
            <CheckboxContainer>
              {isInitial && (
                <Checkbox
                  {...testProps('infoModalDoNotShowCheckbox')}
                  labelTestID="infoModalDoNotShowLabel"
                  title="Do not show this again"
                  checked={doNotShowAgain}
                  fontSize={14}
                  onPress={handleSetDoNotShowAgain}
                />
              )}
            </CheckboxContainer>

            <Button
              title="Get more info"
              state="default"
              type="primary"
              onPress={handleButtonPress}
              {...testProps('infoModalGetMoreInfoBtn')}
              activeOpacity={0.6}
            />
          </Header>
        )}
      </>
    );
  };

  return (
    <BottomSheet
      {...testProps('infoModalBottomSheet')}
      visible={visible}
      onBackdropPress={handleDismiss}
      onBackButtonPress={handleDismiss}
      onModalHide={handleDismiss}
      accessibilityLabel={'info'}>
      <Content>{renderContent()}</Content>
    </BottomSheet>
  );
};

export default InformationModal;
