/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Fontisto from 'react-native-vector-icons/Fontisto';

import Text from '@components/Text';
import {scale, verticalScale} from '@utils/scale';

export const Content: ComponentType<any> = styled.View`
  shadow-color: #000;
  width: 100%;
  min-height: ${verticalScale(300)}px;
  padding: ${scale(24)}px;
  border-radius: ${scale(4)}px;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
`;

export const Header: ComponentType<any> = styled.View`
  display: flex;
  justify-content: space-between;
`;

export const StyledIcon: ComponentType<any> = styled(Fontisto).attrs(
  (props) => ({
    color: props.theme.color.TEXT_300_HIGH,
    size: scale(12),
  }),
)`
  align-self: flex-end;
  margin-bottom: ${verticalScale(2)}px;
`;

export const Title: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
}))`
  margin-bottom: ${verticalScale(24)}px;
`;

export const SubTitle: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'subtitle',
}))`
  margin-bottom: ${verticalScale(17)}px;
`;

export const TextBody: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
  customColor: props.theme.color.TEXT_100_HIGH,
}))`
  margin-bottom: ${verticalScale(24)}px;
`;

export const CheckboxContainer: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(24)}px;
  min-height: ${verticalScale(40)}px;
`;
