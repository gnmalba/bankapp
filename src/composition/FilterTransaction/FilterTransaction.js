/* eslint-disable sonarjs/cognitive-complexity */
/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useEffect, useRef} from 'react';
import {TouchableOpacity, View, ScrollView} from 'react-native';

// Third party libraries
import {useDispatch} from 'react-redux';
import moment from 'moment';

// Components
import Button from '@components/Button';
import FilterTransHeader from './components/Header';
import CalendarContent from './components/CalendarContent';
import AmountInput from './components/AmountInput';
import CheckboxContent from './components/CheckboxContent';

// Utils
import type {PropsType} from './types';
import {
  StyledModal,
  StyledScrollView,
  ContentContainer,
  ContentSubContainer,
  CancelText,
  StyledDivider,
  CardOwnerTypeContainer,
  CreditScrollView,
} from './Styled';
import {scale, verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';
import CreditContent from './components/CreditContent';
import {
  addDataToPayload,
  addDataToSOACurrentPayload,
  addDataToSOAIncomingPayload,
  addDataToSupplePayload,
  filterCardOwnerFunction,
  filteredCardList,
  filterTypeFunction,
  numberOfFiltersActivated,
} from './utils';

const FilterTransaction = (props: PropsType): React$Node => {
  const dispatch = useDispatch();
  const {
    modalVisible,
    handleCloseModal,
    cardType,
    overviewData,
    creditSupplementary,
    accountId,
    primaryId,
    billingDate,
    generatedDate,
    soaAccountId,
    setNumberOfFilters,
    numberOfFilters,
    currencyCode,
    setValue,
    setRunningBalance,
    runningBalance,
    setModalVisible,
    theme,
    currentTab,
    id,
    value,
    setInitialFilters,
    refreshTime,
    setCurrentPage,
  } = props;

  // calendar
  const initialCalendarValue = {
    dateFrom: null,
    dateTo: null,
    isTodayActive: false,
    fieldSelected: '',
  };
  const [calendarData, setCalendarData] = useState(initialCalendarValue);
  const [showCalendar, setShowCalendar] = useState(false);

  // amount filter
  const initialAmountValue = {
    amountFrom: null,
    amountTo: null,
    amountActive: '',
    allAmountsActive: false,
  };
  const [amountFilter, setAmountFilter] = useState(initialAmountValue);
  const [amtErrorRender, setAmtErrorRender] = useState(false);

  // For credit
  const initialTypeValue = [
    {title: 'Credit', isChecked: false, value: 'CR'},
    {title: 'Debit', isChecked: false, value: 'DB'},
  ];
  const [cardlistData, setCardlistData] = useState(overviewData);
  const [transactionType, setTransactionType] = useState(initialTypeValue);

  const [buttonDisabled, setButtonDisabled] = useState('disabled');
  const [clearFilters, setClearFilters] = useState(false);

  const scrollViewRef = useRef(null);
  const calendarTipRef = useRef(null);

  // For cancel filtering
  const defaultPrevious = {
    prevDateFrom: null,
    prevDateTo: null,
    prevAmtFrom: null,
    prevAmtTo: null,
    prevTrxnType: initialTypeValue,
    prevCardOwner: overviewData,
  };
  const [previousValues, setPreviousValues] = useState(defaultPrevious);

  // For cancel calendar filter
  const [calendarPrevValues, setCalendarPrevValues] = useState({
    calendarPrevFrom: null,
    calendarPrevTo: null,
  });

  // Clearing inputs if filters are activated
  const [applyBtnFilterActivated, setApplyBtnFilterActivated] = useState(false);

  useEffect(() => {
    setTransactionType(initialTypeValue);
    setCalendarData(initialCalendarValue);
    setAmountFilter(initialAmountValue);
    setPreviousValues(defaultPrevious);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshTime]);

  useEffect(() => {
    if (calendarData.dateFrom && calendarData.dateTo) {
      setAmountFilter({...amountFilter, allAmountsActive: false});
    }
    /* eslint-disable react-hooks/exhaustive-deps */
  }, [calendarData]);

  useEffect(() => {
    const {amountFrom, amountTo} = amountFilter;

    if (numberOfFilters > 0 && !amountFrom && !amountTo) {
      setApplyBtnFilterActivated(true);
    } else {
      setApplyBtnFilterActivated(false);
    }
  }, [amountFilter, numberOfFilters]);

  useEffect(() => {
    const {amountFrom, amountTo, allAmountsActive} = amountFilter;
    const {dateFrom, dateTo} = calendarData;
    const creditCheckBoxes =
      filterCardOwnerFunction(cardlistData).length > 0 ||
      filterTypeFunction(transactionType).length > 0;
    const filterInputs =
      (amountFrom && amountTo && !amtErrorRender) || applyBtnFilterActivated;
    const dateInputs =
      dateFrom && dateTo && !amtErrorRender && !allAmountsActive;

    if (
      creditCheckBoxes ||
      filterInputs ||
      dateInputs ||
      applyBtnFilterActivated
    ) {
      setButtonDisabled('default');
    } else {
      setButtonDisabled('disabled');
    }
  }, [
    cardType,
    calendarData,
    amountFilter,
    numberOfFilters,
    transactionType,
    cardlistData,
    amtErrorRender,
    applyBtnFilterActivated,
  ]);

  const handleFilterData = () => {
    const {dateFrom, dateTo} = calendarData;
    const {amountFrom, amountTo} = amountFilter;

    setCurrentPage && setCurrentPage(1);
    if (clearFilters) {
      setInitialFilters({});
      setClearFilters(false);
    }

    const payload = {
      cardSerialNo: accountId,
      cardOwners: filterCardOwnerFunction(cardlistData),
      trxnTypes: filterTypeFunction(transactionType),
    };

    const supplePayload = {
      cardSerialNo: primaryId,
      cardOwners: id,
      trxnTypes: filterTypeFunction(transactionType),
      description: value,
    };

    const soaCurrentPayload = {
      date: moment(generatedDate).format('YYYY-MM-DD'),
      cardSerialNo: soaAccountId,
      cardOwners: filterCardOwnerFunction(cardlistData),
      trxnTypes: filterTypeFunction(transactionType),
    };

    const soaIncomingPayload = {
      date: moment(generatedDate).format('YYYY-MM-DD'),
      cardSerialNo: soaAccountId,
      cardOwners: filterCardOwnerFunction(cardlistData),
      trxnTypes: filterTypeFunction(transactionType),
    };

    const depositPrepaidPayload = {
      dateFrom,
      dateTo,
      amountFrom,
      amountTo,
    };

    addDataToPayload(payload, dateFrom, dateTo, transactionType);
    addDataToSupplePayload(supplePayload, dateFrom, dateTo, transactionType);
    addDataToSOAIncomingPayload(
      soaIncomingPayload,
      dateFrom,
      dateTo,
      transactionType,
    );
    addDataToSOACurrentPayload(
      soaCurrentPayload,
      dateFrom,
      dateTo,
      transactionType,
    );

    if (cardType === 'deposit' || cardType === 'prepaid') {
      // FOR DEPOSIT AND PREPAID
      dispatch({
        type: 'FILTER_DEPOSIT_PREPAID_REQUEST',
        payload: depositPrepaidPayload,
      });
    } else {
      // FOR YAZZ FILTERS
      dispatch({
        type: 'REQUEST_FILTER_AMOUNT_DATE_YAZZ',
        payload: depositPrepaidPayload,
      });
    }

    if (cardType === 'credit') {
      if (creditSupplementary) {
        // FOR SUPPLEMENTARY FILTERS
        setInitialFilters(supplePayload);
        dispatch({
          type: 'REQUEST_SUPPLEMENTARY_TRANSACTION_CURRENT_LIST',
          payload: supplePayload,
        });
      } else {
        // FOR CREDIT FILTERS
        setInitialFilters(payload);
        dispatch({
          type: 'REQUEST_CREDIT_TRANSACTION_CURRENT_LIST',
          payload,
        });
      }
    }

    // FOR CREDIT CARD SOA

    /* eslint-disable sonarjs/no-collapsible-if */
    if (currentTab !== undefined) {
      if (currentTab === billingDate) {
        if (billingDate === 'Incoming') {
          dispatch({
            type: 'REQUEST_SOA_TRANSACTION_API',
            payload: soaIncomingPayload,
            tab: 'incoming',
          });
        } else {
          dispatch({
            type: 'REQUEST_SOA_TRANSACTION_API',
            payload: soaCurrentPayload,
            tab: 'past',
          });
        }
      }
    }

    numberOfFiltersActivated(
      dateFrom,
      dateTo,
      amountFrom,
      amountTo,
      setNumberOfFilters,
      transactionType,
      cardlistData,
    );

    setValue('');
    setApplyBtnFilterActivated(false);

    handleCloseModal();

    setPreviousValues({
      prevDateFrom: dateFrom,
      prevDateTo: dateTo,
      prevAmtFrom: amountFrom,
      prevAmtTo: amountTo,
      prevCardOwner: cardlistData,
      prevTrxnType: transactionType,
    });
  };

  const handleCloseFilterModal = () => {
    const {
      prevDateFrom,
      prevDateTo,
      prevAmtFrom,
      prevAmtTo,
      prevCardOwner,
      prevTrxnType,
    } = previousValues;
    setClearFilters(false);
    handleCloseModal();
    if (numberOfFilters === 0) {
      setAmountFilter({
        amountFrom: null,
        amountTo: null,
        allAmountsActive: false,
      });
      setCalendarData({dateFrom: null, dateTo: null});
      setTransactionType(initialTypeValue);
      setCardlistData(filteredCardList(overviewData));
    } else {
      setCalendarData({
        dateFrom: prevDateFrom,
        dateTo: prevDateTo,
      });
      setAmountFilter({
        amountFrom: prevAmtFrom,
        amountTo: prevAmtTo,
        amountActive: '',
        allAmountsActive: false,
      });
      setCardlistData(prevCardOwner);
      setTransactionType(prevTrxnType);
    }
  };

  const depositAndPrepaidBody = () => {
    return (
      <View style={{marginHorizontal: scale(15)}}>
        <CalendarContent
          showCalendar={showCalendar}
          setShowCalendar={setShowCalendar}
          calendarData={calendarData}
          setCalendarData={setCalendarData}
          calendarTipRef={calendarTipRef}
          setCalendarPrevValues={setCalendarPrevValues}
          calendarPrevValues={calendarPrevValues}
          cardType={cardType}
        />
        {!showCalendar && (
          <>
            <StyledDivider />
            <ScrollView alwaysBounceVertical={false}>
              <AmountInput
                amountFilter={amountFilter}
                setAmountFilter={setAmountFilter}
                currencyCode={currencyCode}
                setAmtErrorRender={setAmtErrorRender}
                amtErrorRender={amtErrorRender}
              />
              {cardType === 'deposit' && (
                <CheckboxContent
                  runningBalance={runningBalance}
                  setRunningBalance={setRunningBalance}
                />
              )}
              {submitItem()}
            </ScrollView>
          </>
        )}
      </View>
    );
  };

  const creditBody = () => {
    return (
      <CardOwnerTypeContainer extendHeight={cardlistData?.length > 3}>
        {!showCalendar && (
          <CreditContent
            transactionType={transactionType}
            cardlistData={cardlistData}
            setCardlistData={setCardlistData}
            setTransactionType={setTransactionType}
            creditSupplementary={creditSupplementary}
          />
        )}
        <View style={{marginTop: verticalScale(10)}} />
        <CalendarContent
          showCalendar={showCalendar}
          setShowCalendar={setShowCalendar}
          calendarData={calendarData}
          setCalendarData={setCalendarData}
          scrollViewRef={scrollViewRef}
          calendarTipRef={calendarTipRef}
          setCalendarPrevValues={setCalendarPrevValues}
          calendarPrevValues={calendarPrevValues}
          cardType={cardType}
        />
        {!showCalendar && <>{submitItem()}</>}
      </CardOwnerTypeContainer>
    );
  };

  const submitItem = () => {
    return (
      <View
        style={{
          marginTop: verticalScale(30),
        }}>
        <Button
          type="primary"
          size="large"
          title="Apply"
          onPress={handleFilterData}
          {...testProps('filterTransactionApplyButton')}
          state={buttonDisabled}
        />
        <TouchableOpacity
          // eslint-disable-next-line react-native/no-inline-styles
          style={{marginTop: verticalScale(10), alignSelf: 'center'}}
          onPress={handleCloseFilterModal}
          {...testProps('filterTransactionCancelButton')}>
          <CancelText
            {...testProps('filterTransactionCancelButtonLabel')}
            theme={theme}>
            Cancel
          </CancelText>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <>
      <StyledModal
        onRequestClose={handleCloseModal}
        transparent
        animationType="none"
        visible={modalVisible}
        statusBarTranslucent
        maxSwipeDistance={0}>
        <StyledScrollView bounces={false}>
          <ContentContainer contentHeight={verticalScale(610)}>
            <ContentSubContainer>
              <FilterTransHeader
                showCalendar={showCalendar}
                cardType={cardType}
                initialTypeValue={initialTypeValue}
                setTransactionType={setTransactionType}
                setCardlistData={setCardlistData}
                overviewData={overviewData}
                setCalendarData={setCalendarData}
                setAmountFilter={setAmountFilter}
                setNumberOfFilters={setNumberOfFilters}
                calendarData={calendarData}
                amountFilter={amountFilter}
                cardlistData={cardlistData}
                transactionType={transactionType}
                setModalVisible={setModalVisible}
                setShowCalendar={setShowCalendar}
                theme={theme}
                setAmtErrorRender={setAmtErrorRender}
                calendarTipRef={calendarTipRef}
                handleCloseFilterModal={handleCloseFilterModal}
                setCalendarPrevValues={setCalendarPrevValues}
                calendarPrevValues={calendarPrevValues}
                headerTitle={
                  showCalendar ? 'Filter by date' : 'Filter Transactions'
                }
                clearTitle={showCalendar ? 'Clear' : 'Clear All'}
                setApplyBtnFilterActivated={setApplyBtnFilterActivated}
                filtersActivated={numberOfFilters > 0}
                setClearFilters={setClearFilters}
              />
              {cardType !== 'credit' ? (
                depositAndPrepaidBody()
              ) : (
                <CreditScrollView
                  cardListLength={cardlistData?.length}
                  ref={scrollViewRef}
                  showsVerticalScrollIndicator={false}
                  bounces={false}
                  scrollEnabled={
                    !showCalendar && cardlistData?.length > 3 ? true : false
                  }>
                  {creditBody()}
                </CreditScrollView>
              )}
            </ContentSubContainer>
          </ContentContainer>
        </StyledScrollView>
      </StyledModal>
    </>
  );
};

export default FilterTransaction;
