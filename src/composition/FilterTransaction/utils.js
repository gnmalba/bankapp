import moment from 'moment';

const DATE_FORMAT = 'yyyy-MM-DD';

export const filterTypeFunction = (type) => {
  const tempFilterType = type?.filter(
    (filterTypes) => filterTypes.isChecked === true,
  );

  const filterTypesArray = [];

  tempFilterType?.forEach((item) => {
    filterTypesArray.push(item.value);
  });

  return filterTypesArray;
};

export const filterCardOwnerFunction = (cardOwners) => {
  const tempFilterCardOwners = cardOwners?.filter(
    (filterCardOwners) => filterCardOwners.isChecked === true,
  );

  const filterCardOwnersArray = [];

  tempFilterCardOwners?.forEach((item) => {
    filterCardOwnersArray.push(item.cardSerNo);
  });

  return filterCardOwnersArray;
};

// FOR CREDIT AND SUPPLEMENTARY FILTERS
export const addDataToPayload = (
  payload,
  dateFrom,
  dateTo,
  transactionType,
) => {
  if (dateFrom && dateTo) {
    payload.startDate = moment(dateFrom).format(DATE_FORMAT);
    payload.endDate = moment(dateTo).format(DATE_FORMAT);
  } else if (filterTypeFunction(transactionType).length > 0) {
    payload.trxnTypes = filterTypeFunction(transactionType);
  }
};

export const addDataToSupplePayload = (
  supplePayload,
  dateFrom,
  dateTo,
  transactionType,
) => {
  if (dateFrom && dateTo) {
    supplePayload.startDate = moment(dateFrom).format(DATE_FORMAT);
    supplePayload.endDate = moment(dateTo).format(DATE_FORMAT);
  } else if (filterTypeFunction(transactionType).length > 0) {
    supplePayload.trxnTypes = filterTypeFunction(transactionType);
  }
};

// FOR SOA INCOMING FILTERS
export const addDataToSOAIncomingPayload = (
  soaIncomingPayload,
  dateFrom,
  dateTo,
  transactionType,
) => {
  if (dateFrom && dateTo) {
    soaIncomingPayload.startDate = moment(dateFrom).format(DATE_FORMAT);
    soaIncomingPayload.endDate = moment(dateTo).format(DATE_FORMAT);
  } else if (filterTypeFunction(transactionType).length > 0) {
    soaIncomingPayload.trxnTypes = filterTypeFunction(transactionType);
  }
};

// FOR SOA CURRENT FILTERS
export const addDataToSOACurrentPayload = (
  soaCurrentPayload,
  dateFrom,
  dateTo,
  transactionType,
) => {
  if (dateFrom && dateTo) {
    soaCurrentPayload.startDate = moment(dateFrom).format(DATE_FORMAT);
    soaCurrentPayload.endDate = moment(dateTo).format(DATE_FORMAT);
  } else if (filterTypeFunction(transactionType).length > 0) {
    soaCurrentPayload.trxnTypes = filterTypeFunction(transactionType);
  }
};

export const filteredCardList = (data) => {
  return data?.map((item) => ({
    ...item,
    isChecked: false,
  }));
};

// Set number of filters

export const numberOfFiltersActivated = (
  dateFrom,
  dateTo,
  amountFrom,
  amountTo,
  setNumberOfFilters,
  transactionType,
  cardlistData,
) => {
  let totalFilterCount = 0;

  if (dateFrom || dateTo) {
    totalFilterCount += 1;
  }

  if (amountFrom || amountTo) {
    totalFilterCount += 1;
  }

  if (filterTypeFunction(transactionType).length > 0) {
    totalFilterCount += 1;
  }

  if (filterCardOwnerFunction(cardlistData).length > 0) {
    totalFilterCount += 1;
  }

  setNumberOfFilters(totalFilterCount);
};

export const amountStringToNumber = (number) => {
  if (number) {
    return number.replace(/,/g, '');
  }
};

/* AMOUNT INPUT VALIDATIONS */

export const numberOfPeriods = (value) => {
  return value.split('.').length - 1;
};

export const reduceAmount = (value) => {
  const strArr = value.split('.');
  return strArr[0] + '.' + strArr[1].substr(0, 2);
};

export const numberOfDecimals = (value) => {
  if (!value || !value.includes('.')) {
    return 0;
  }

  const hasDecimal = value.split('.').length > 1;
  if (hasDecimal) {
    return value.split('.')[1].length;
  }

  return 0;
};

export const amountInputValidation = (
  value,
  field,
  amountFilter,
  setAmountFilter,
) => {
  if (value.includes(',')) {
    setAmountFilter({...amountFilter, [field]: value.split(',').join('')});
  } else if (!value.match(/^(\d+\.?\d{0,9}|\.\d{1,2})$/)) {
    if (numberOfPeriods(value) > 1) {
      setAmountFilter({...amountFilter, [field]: reduceAmount(value)});
    } else {
      setAmountFilter();
      setAmountFilter({
        ...amountFilter,
        [field]: value ? value.replace(/[^0-9.]/g, '') : null,
      });
    }
  } else {
    if (numberOfDecimals(value) > 2) {
      const newVal =
        value.split('.')[0] + '.' + value.split('.')[1].substr(0, 2);
      setAmountFilter({...amountFilter, [field]: newVal});
    } else {
      setAmountFilter({...amountFilter, [field]: value});
    }
  }
  if (value.startsWith('0') && value.length > 1 && !value.includes('.')) {
    value = value[1];
    setAmountFilter({...amountFilter, [field]: value});
  }
};
