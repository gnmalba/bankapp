/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {Platform, Dimensions, ScrollView} from 'react-native';
import styled from 'styled-components/native';

// Third party libraries
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Divider} from 'react-native-elements';

// Components
import Text from '@components/Text';

// Utils
import {scale, verticalScale, moderateScale} from '@utils/scale';

const {height} = Dimensions.get('window');
const getDeviceHeight = height;

export const Container: ComponentType<any> = styled.View``;

export const StyledScrollView: ComponentType<any> = styled(
  KeyboardAwareScrollView,
).attrs((props) => ({
  contentContainerStyle: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'flex-end',
    flex: 1,
    flexDirection: Platform.select({ios: 'column'}),
    height: Platform.select({android: getDeviceHeight}),
  },
}))``;

export const StyledModal: ComponentType<any> = styled.Modal`
  justify-content: flex-end;
  margin: 0;
  position: absolute;
`;

export const ContentContainer: ComponentType<any> = styled.View.attrs(
  (props) => ({
    height: props.contentHeight,
  }),
)`
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
  border-top-right-radius: ${scale(20)}px;
  border-top-left-radius: ${scale(20)}px;
`;

export const ContentSubContainer: ComponentType<any> = styled.View`
  height: ${verticalScale(600)}px;
  padding-horizontal: ${scale(15)}px;
  padding-top: ${verticalScale(25)}px;
  border-top-left-radius: ${scale(8)}px;
  border-top-right-radius: ${scale(8)}px;
  flex: 1;
  background-color: ${(props) => props.theme.color.BACKGROUND_200};
`;

export const StyledDivider: ComponentType<any> = styled(Divider).attrs(
  (props) => ({
    marginTop: verticalScale(15),
    marginBottom: verticalScale(20),
    height: verticalScale(1),
    color: props.theme.filterTransaction.divider,
  }),
)``;

export const CardOwnerTypeContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_200};
  height: ${(props) => verticalScale(props.extendHeight ? 700 : 610)}px
  padding-top: ${verticalScale(15)}px;
  padding-bottom: ${verticalScale(15)}px;
  padding-right: ${scale(15)}px;
  padding-left: ${scale(15)}px;
`;

// Action buttons

export const CancelText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
}))`
  color: ${(props) => props.theme.filterTransaction.cancelButton};
  font-weight: bold;
  font-size: ${moderateScale(17)}px;
`;

export const CreditScrollView: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    contentContainerStyle: {
      paddingBottom: verticalScale(50),
    },
  }),
)``;
