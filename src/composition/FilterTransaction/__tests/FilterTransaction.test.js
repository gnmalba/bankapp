import React from 'react';
import {render} from '@testing-library/react-native';
import FilterTransaction from '../FilterTransaction';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('FilterTransaction', () => {
  const mockStore = () => ({});
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <FilterTransaction theme={DARK_THEME} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
