/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useEffect} from 'react';
import {TouchableOpacity} from 'react-native';

// Components
import IconTooltip from '@components/IconTooltip';

// Utils
import {
  HeaderContainer,
  TitleContainer,
  HeaderTitle,
  HeaderClearText,
} from './Styled';
import {verticalScale, scale} from '@utils/scale';
import {
  filterCardOwnerFunction,
  filteredCardList,
  filterTypeFunction,
} from '../utils';
import type {FilterHeaderPropTypes} from '../types';
import styled from 'styled-components';
import {IS_DEVICE_TABLET} from '@utils/constants';

const StyledIcon = styled(IconTooltip).attrs((props) => ({
  source: require('@assets/icons/back-icon.png'),
  width: scale(15),
  height: IS_DEVICE_TABLET ? verticalScale(8) : verticalScale(11),
  color: props?.theme?.color?.TEXT_300_HIGH,
}))``;

const FilterTransHeader = (props: FilterHeaderPropTypes): React$Node => {
  const {
    showCalendar,
    cardType,
    initialTypeValue,
    setTransactionType,
    setCardlistData,
    overviewData,
    setCalendarData,
    setAmountFilter,
    calendarData,
    amountFilter,
    cardlistData,
    transactionType,
    setShowCalendar,
    theme,
    setAmtErrorRender,
    calendarTipRef,
    handleCloseFilterModal,
    setCalendarPrevValues,
    calendarPrevValues,
    headerTitle,
    clearTitle,
    setApplyBtnFilterActivated,
    filtersActivated,
    setClearFilters,
  } = props;

  const [clearButtonDisabled, setClearButtonDisabled] = useState(true);

  useEffect(() => {
    const {dateFrom, dateTo} = calendarData;
    const {amountFrom, amountTo} = amountFilter;
    const creditCheckBoxes =
      filterCardOwnerFunction(cardlistData).length > 0 ||
      filterTypeFunction(transactionType).length > 0;
    const filterInputs = amountFrom || amountTo;
    const dateInputs = dateFrom || dateTo;

    if (creditCheckBoxes || filterInputs || dateInputs) {
      setClearButtonDisabled(false);
    } else {
      setClearButtonDisabled(true);
    }
  }, [calendarData, amountFilter, cardlistData, transactionType, cardType]);

  const handleClearFilter = () => {
    setClearFilters(true);
    calendarTipRef?.current?.resetSelections();
    if (showCalendar) {
      // Clear calendar fields
      setCalendarData({dateFrom: null, dateTo: null});
    } else {
      // Clear all fields
      setTransactionType(initialTypeValue);
      setCardlistData(filteredCardList(overviewData));
      setCalendarData({dateFrom: null, dateTo: null});
      setAmountFilter({
        amountFrom: null,
        amountTo: null,
        allAmountsActive: false,
      });
      setAmtErrorRender(false);
      setCalendarPrevValues({calendarDateFrom: null, calendarDateTo: null});
      if (filtersActivated) {
        setApplyBtnFilterActivated(true);
      }
    }
  };

  const handleCloseModal = () => {
    const {calendarPrevFrom, calendarPrevTo} = calendarPrevValues;
    if (showCalendar) {
      setShowCalendar(false);
      setCalendarData({
        dateFrom: calendarPrevFrom ? calendarPrevFrom : null,
        dateTo: calendarPrevTo ? calendarPrevTo : null,
      });
    } else {
      handleCloseFilterModal();
    }
  };

  return (
    <>
      <HeaderContainer>
        <TouchableOpacity
          onPress={() => handleCloseModal()}
          activeOpacity={1}
          style={{marginTop: verticalScale(3)}}>
          <StyledIcon theme={theme} />
        </TouchableOpacity>

        <TitleContainer>
          <HeaderTitle>{headerTitle}</HeaderTitle>
        </TitleContainer>

        <TouchableOpacity
          style={{marginTop: verticalScale(5)}}
          disabled={clearButtonDisabled}
          onPress={handleClearFilter}>
          <HeaderClearText clearButtonDisabled={clearButtonDisabled}>
            {clearTitle}
          </HeaderClearText>
        </TouchableOpacity>
      </HeaderContainer>
    </>
  );
};

export default FilterTransHeader;
