/* eslint-disable no-unused-vars */
/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';

// Components

// Utils
import {
  CreditStyledCheckbox,
  CardOwnerText,
  CreditAccInformationContainer,
  EmbossingNameText,
  AccNumberText,
  TypeText,
  StyledDivider,
  StyledCheckedIcon,
  StyledUncheckedIcon,
} from './Styled';
import {verticalScale} from '@utils/scale';
import {maskingFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import type {CreditBodyPropTypes} from '../types';

const CreditContent = (props: CreditBodyPropTypes): React$Node => {
  const {
    cardlistData,
    setCardlistData,
    transactionType,
    setTransactionType,
    creditSupplementary,
  } = props;

  const [cardOwnerEnabled, setCardOwnerEnabled] = React.useState(false);
  const [typeEnabled, setTypeEnabled] = React.useState(false);

  const filterCardOwner = (title, isChecked, value) => {
    const array = cardlistData.map((tran) => {
      let tempRow = tran;
      if (tran.cardSerNo === value) {
        tempRow = {...tran, isChecked: !isChecked};
        if (tempRow.isChecked) {
          setCardOwnerEnabled(true);
        } else {
          setCardOwnerEnabled(false);
        }
      }
      return tempRow;
    });
    setCardlistData(array);
  };

  const filterTransactionType = (title, isChecked) => {
    const array = transactionType.map((typ) => {
      let tempRow = typ;
      if (typ.title === title) {
        tempRow = {...typ, isChecked: !isChecked};
        if (tempRow.isChecked) {
          setTypeEnabled(true);
        } else {
          setTypeEnabled(false);
        }
      }
      return tempRow;
    });

    setTransactionType(array);
  };

  const checkboxItem = (title, isChecked, value, index, type) => {
    const uniqueIndex = index + 1;
    return (
      <View style={{marginTop: verticalScale(22)}}>
        <CreditStyledCheckbox
          {...testProps(uniqueIndex + type + 'Checkbox')}
          title={title}
          checked={isChecked}
          value={value}
          onPress={() => {
            type === 'TYPE'
              ? filterTransactionType(title, isChecked)
              : filterCardOwner(title, isChecked, value);
          }}
          checkedIcon={
            <StyledCheckedIcon
              disabledColor={cardlistData?.length > 1 || type === 'TYPE'}
              {...testProps(uniqueIndex + type + 'CheckboxIcon')}
            />
          }
          uncheckedIcon={
            <StyledUncheckedIcon
              {...testProps(uniqueIndex + type + 'CheckboxIconUnchecked')}
            />
          }
          iconType={'ionicon'}
          Component={TouchableWithoutFeedback}
        />
      </View>
    );
  };

  return (
    <>
      {!creditSupplementary && (
        <>
          <CardOwnerText {...testProps('filterTransactionsCardOwnerLabel')}>
            CARD OWNER
          </CardOwnerText>
          {cardlistData?.map((item, index) => {
            return (
              // eslint-disable-next-line react-native/no-inline-styles
              <View style={{flexDirection: 'row'}} key={index}>
                <View>
                  {checkboxItem(
                    null,
                    cardlistData?.length > 1 ? item.isChecked : true,
                    item.cardSerNo,
                    index,
                    'cardOwner',
                  )}
                </View>
                <CreditAccInformationContainer>
                  <EmbossingNameText>
                    {item?.embossingName
                      ? item.embossingName.toUpperCase()
                      : ''}
                  </EmbossingNameText>
                  <AccNumberText>
                    {maskingFormat(item.cardno, true)}
                  </AccNumberText>
                </CreditAccInformationContainer>
              </View>
            );
          })}
          <StyledDivider {...testProps('filterTransactionCardOwnerDivider')} />
        </>
      )}

      <TypeText>TYPE</TypeText>

      {transactionType?.map((item, index) => {
        return (
          <View key={index}>
            {checkboxItem(
              item.title,
              item.isChecked,
              item.value,
              index,
              'TYPE',
            )}
          </View>
        );
      })}

      <StyledDivider {...testProps('filterTransactionTypeDivider')} />
    </>
  );
};

export default CreditContent;
