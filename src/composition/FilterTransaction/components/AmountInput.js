/**
 * @format
 * @flow
 */

// React modules
import React, {useRef} from 'react';
import {View} from 'react-native';

// Utils
import {
  AmountText,
  HeaderTextContainer,
  FromAndToText,
  TextInputContainer,
  PHPTextStyle,
  AmountTextInput,
  ErrorText,
} from './Styled';
import {testProps} from '@utils/componentCustomProps';
import {numberFormatNoLimit, removeCommas} from '@utils/numberInputs';
import {amountInputValidation} from '../utils';
import {numberFormat} from '@utils/accountNumber';
export type PropsType = {
  amountFilter: Object,
  setAmountFilter: any,
  currencyCode: string,
  setAmtErrorRender: any,
  amtErrorRender: boolean,
};

const AmountInput = (props: PropsType): React$Node => {
  const {
    amountFilter,
    setAmountFilter,
    currencyCode,
    setAmtErrorRender,
    amtErrorRender,
  } = props;
  const {amountFrom, amountTo, amountActive} = amountFilter;
  const toAmountRef = useRef(null);

  const handleBlur = () => {
    if (toAmountRef.current) {
      toAmountRef.current.focus();
    }

    setAmountFilter({...amountFilter, amountActive: ''});
  };

  const handleChangeAmountInputs = (value, field: string) => {
    amountInputValidation(value, field, amountFilter, setAmountFilter);
  };

  const handleEndEditAmountFrom = () => {
    const parsedAmountFrom = parseFloat(amountFrom);
    if (!isNaN(parsedAmountFrom)) {
      setAmountFilter({
        ...amountFilter,
        amountFrom: numberFormatNoLimit(numberFormat(amountFrom)),
        allAmountsActive: true,
      });
    } else {
      setAmountFilter({
        ...amountFilter,
        amountFrom: null,
        allAmountsActive: true,
      });
    }
  };

  const handleEndEditAmountTo = () => {
    const parsedAmountTo = parseFloat(amountTo);
    if (!isNaN(parsedAmountTo)) {
      setAmountFilter({
        ...amountFilter,
        amountTo: numberFormatNoLimit(numberFormat(amountTo)),
        allAmountsActive: true,
      });
    } else {
      setAmountFilter({
        ...amountFilter,
        amountTo: null,
        allAmountsActive: true,
      });
    }
  };

  const handleAmountFromFocus = () => {
    setAmountFilter({
      ...amountFilter,
      amountActive: 'amountFrom',
    });
  };

  const handleAmountToFocus = () => {
    setAmountFilter({
      ...amountFilter,
      amountActive: 'amountTo',
    });
  };

  const renderError = () => {
    if (
      Number(removeCommas(numberFormatNoLimit(amountTo))) <
      Number(removeCommas(numberFormatNoLimit(amountFrom)))
    ) {
      setAmtErrorRender(true);
      return <ErrorText>Amount FROM must be lower than TO</ErrorText>;
    } else {
      setAmtErrorRender(false);
    }
  };

  return (
    <>
      <View>
        <AmountText {...testProps('filterTransactionAmountLbl')}>
          AMOUNT
        </AmountText>

        <HeaderTextContainer>
          <FromAndToText {...testProps('filterTransactionFromLbl')}>
            From
          </FromAndToText>
        </HeaderTextContainer>

        <TextInputContainer amountActive={amountActive === 'amountFrom'}>
          <PHPTextStyle
            {...testProps('filterTransactionAmountFromCurrencyLabelId')}>
            {currencyCode}
          </PHPTextStyle>

          <AmountTextInput
            placeholder={amountActive === 'amountFrom' ? '' : '0.00'}
            returnKeyType="done"
            keyboardType="decimal-pad"
            placeholderTextColor={'#445870'}
            selectionColor={'#FFFFFF'}
            value={amountFrom}
            onFocus={handleAmountFromFocus}
            onEndEditing={handleEndEditAmountFrom}
            onChangeText={(value) => {
              handleChangeAmountInputs(value, 'amountFrom');
            }}
            onBlur={handleBlur}
            {...testProps('filterTransactionAmountFromField')}
          />
        </TextInputContainer>

        <HeaderTextContainer>
          <FromAndToText {...testProps('filterTransactionAmountToLabelId')}>
            To
          </FromAndToText>
        </HeaderTextContainer>

        <TextInputContainer
          amountActive={amountActive === 'amountTo'}
          amtErrorRender={amtErrorRender}>
          <PHPTextStyle {...testProps('filterTransactionAmountToField')}>
            {currencyCode}
          </PHPTextStyle>

          <AmountTextInput
            ref={toAmountRef}
            placeholder={amountActive === 'amountTo' ? '' : '0.00'}
            returnKeyType="done"
            keyboardType="decimal-pad"
            placeholderTextColor={'#445870'}
            selectionColor={'#FFFFFF'}
            value={amountTo}
            onFocus={handleAmountToFocus}
            onEndEditing={handleEndEditAmountTo}
            onChangeText={(value) => {
              handleChangeAmountInputs(value, 'amountTo');
            }}
            onBlur={() => setAmountFilter({...amountFilter, amountActive: ''})}
            {...testProps('filterTransactionAmountToFieldId')}
          />
        </TextInputContainer>

        {renderError()}
      </View>
    </>
  );
};

export default AmountInput;
