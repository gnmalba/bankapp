/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {Platform} from 'react-native';

// Third party libraries
import {Divider, CheckBox} from 'react-native-elements';

// Components
import IconTooltip from '@components/IconTooltip';
import Text from '@components/Text';

// Utils
import styled from 'styled-components/native';
import {scale, verticalScale, moderateScale} from '@utils/scale';

/* HEADER STYLE */
export const HeaderContainer: ComponentType<any> = styled.View`
  height: ${verticalScale(40)}px;
  border-top-right-radius: ${scale(100)}px;
  border-top-left-radius: ${scale(100)}px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding-left: ${scale(10)}px;
  padding-right: ${scale(0)}px;
  background-color: ${(props) => props.theme.color.BACKGROUND_200};
`;

export const TitleContainer: ComponentType<any> = styled.View`
  flex: 1;
  align-content: center;
  justify-content: center;
  flex-direction: row;
  margin-left: ${scale(20)}px;
`;

export const HeaderTitle: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_300_HIGH',
}))`
  font-size: ${moderateScale(20)}px;
  font-weight: 600;
  font-family: MyriadPro-Semibold;
`;

export const HeaderClearText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'h3',
  }),
)`
  font-size: ${moderateScale(14)}px;
  align-self: flex-end;
  font-weight: bold;
  color: ${(props) =>
    props.clearButtonDisabled
      ? props.theme.color.DISABLED_STATUS
      : props.theme.color.SECONDARY_DEFAULT};
  margin-top: ${verticalScale(-3)}px;
`;

/* CALENDAR CONTENT STYLE */
export const ByDateText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_100_HIGH',
}))`
  font-weight: bold;
  letter-spacing: 0.85px;
  padding-bottom: ${verticalScale(5)}px;
  font-size: ${moderateScale(12)}px;
`;

export const DateContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  margin-top: ${verticalScale(10)}px;
  margin-bottom: ${verticalScale(5)}px;
`;

export const DateFromText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_300_HIGH',
}))`
  font-weight: normal;
  margin-right: ${scale(140)}px;
  font-size: ${moderateScale(13)}px;
  margin-bottom: ${verticalScale(-10)}px;
`;

export const DateToText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_300_HIGH',
}))`
  font-weight: normal;
  justify-content: flex-end;
  font-size: ${moderateScale(13)}px;
  margin-bottom: ${verticalScale(-10)}px;
`;

export const FromDateCalendarButton: ComponentType<any> = styled.View`
  align-items: center;
  height: ${verticalScale(33)}px;
  width: ${scale(148)}px;
  flex-direction: row;
  border-radius: 5px;
  margin-right: ${scale(17)}px;
  margin-bottom: ${verticalScale(5)}px;
  border-width: 1px;
  border-color: ${(props) =>
    props.fieldSelected === 'calendarFromField'
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.filterTransaction.calendarInputBorder};
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
`;

export const FromDateInputContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const BorderLeft: ComponentType<any> = styled.View`
  border-left-width: 1px;
  border-color: #445870;
  height: ${verticalScale(20)}px;
`;

export const FromTextInput: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'h2',
  }),
)`
  font-size: ${moderateScale(14)}px;
  padding: ${scale(3)}px;
  color: ${(props) =>
    props.fromTxtColor
      ? props.theme.color.TEXT_300_HIGH
      : props.theme.filterTransaction.placeholderText};
  margin-left: ${scale(5)}px;
`;

export const ToDateCalendarButton: ComponentType<any> = styled.View`
  align-items: center;
  height: ${verticalScale(33)}px;
  width: ${scale(148)}px;
  flex-direction: row;
  border-radius: 5px;
  border-width: 1px;
  border-color: ${(props) =>
    props.fieldSelected === 'calendarToField'
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.filterTransaction.calendarInputBorder};
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
`;

export const ToDateInputContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ToTextInput: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h2',
}))`
  font-size: ${moderateScale(14)}px;
  padding: ${scale(3)}px;
  color: ${(props) =>
    props.toTxtColor
      ? props.theme.color.TEXT_300_HIGH
      : props.theme.filterTransaction.placeholderText};
  margin-left: ${scale(5)}px;
`;

/* AMOUNT INPUT CONTENT STYLE */
export const AmountText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_100_HIGH',
}))`
  font-size: ${moderateScale(12)}px;
  font-weight: bold;
  letter-spacing: 0.85px;
`;

export const HeaderTextContainer: ComponentType<any> = styled.View`
  flex: 0;
  flex-direction: row;
  margin-top: ${verticalScale(5)}px;
`;

export const FromAndToText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    themeColor: 'TEXT_300_HIGH',
  }),
)`
  font-weight: normal;
  padding-top: ${verticalScale(10)}px;
  padding-bottom: ${verticalScale(10)}px;
  margin-right: ${scale(150)}px;
  font-family: Myriad Pro;
`;

export const TextInputContainer: ComponentType<any> = styled.View`
  width: ${scale(310)}px;
  height: ${verticalScale(40)}px;
  flex-direction: row;
  border-radius: 5px;
  border-width: 1px;
  margin-top: ${verticalScale(5)}px;
  padding-top: ${verticalScale(5)}px;
  padding-bottom: ${verticalScale(5)}px;
  border-color: ${(props) =>
    props.amountActive && !props.amtErrorRender
      ? props.theme.color.SECONDARY_DEFAULT
      : props.amtErrorRender
      ? props.theme.color.RED_STATUS
      : props.theme.filterTransaction.calendarInputBorder};
  background-color: ${(props) => props.theme.color.BACKGROUND_300};
`;

export const PHPTextStyle: ComponentType<any> = styled.Text`
  margin-left: ${scale(10)}px;
  margin-right: ${scale(Platform.select({android: 6, ios: 3}))}px;
  margin-top: ${verticalScale(Platform.select({android: 4, ios: 7}))}px;
  margin-bottom: ${verticalScale(Platform.select({android: 4, ios: 7}))}px;
  align-self: center;
  width: ${scale(30)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-size: ${moderateScale(13)};
`;

export const AmountTextInput: ComponentType<any> = styled.TextInput.attrs(
  (props) => ({
    selectionColor: props.theme?.color?.TEXT_100_HIGH,
  }),
)`
  font-size: ${scale(15)}px;
  border-left-width: 1px;
  padding-top: ${verticalScale(6)}px;
  padding-bottom: ${verticalScale(6)}px;
  padding-left: ${scale(10)}px;
  padding-right: ${scale(10)}px;
  width: ${scale(310)}px;
  border-color: #445870;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  max-width: ${scale(260)}px;
`;

/* CHECKBOX ITEM STYLE */
export const StyledCheckbox: ComponentType<any> = styled(CheckBox).attrs(
  (props) => ({
    containerStyle: {
      backgroundColor: 'transparent',
      borderColor: props.theme.color.BACKGROUND_100,
      margin: 0,
      padding: 0,
    },
    textStyle: {
      color: props.theme.color.TEXT_300_HIGH,
      fontWeight: 'normal',
      fontSize: moderateScale(19),
    },
  }),
)``;

/* CREDIT CONTENT */
export const CardOwnerText: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'h3',
    themeColor: 'TEXT_100_HIGH',
  }),
)`
  font-size: ${moderateScale(13)}px;
`;

export const CreditAccInformationContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(20)}px;
  flex-direction: column;
  margin-left: ${scale(11)}px;
`;

export const EmbossingNameText: ComponentType<any> = styled.Text`
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-size: ${moderateScale(19)}px;
  font-family: Myriad Pro;
  margin-top: ${verticalScale(-2)};
`;

export const AccNumberText: ComponentType<any> = styled.Text`
  margin-top: ${verticalScale(5)}px;
  font-weight: 700;
  font-size: ${moderateScale(11)}px;
  font-family: Myriad Pro;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const StyledDivider: ComponentType<any> = styled(Divider).attrs(
  (props) => ({
    marginTop: verticalScale(15),
    marginBottom: verticalScale(15),
    height: 1,
  }),
)`
  background-color: ${(props) => props.theme.filterTransaction.divider};
`;

export const TypeText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_100_HIGH',
}))`
  font-size: ${moderateScale(13)}px;
  font-weight: bold;
`;

export const CreditStyledCheckbox: ComponentType<any> = styled(CheckBox).attrs(
  (props) => ({
    containerStyle: {
      backgroundColor: 'transparent',
      borderColor: props.theme.color.BACKGROUND_200,
      margin: 0,
      padding: 0,
    },
    textStyle: {
      color: props.theme.color.TEXT_300_HIGH,
      fontSize: 18,
      fontWeight: 'normal',
    },
  }),
)``;

export const StyledCheckedIcon: ComponentType<any> = styled(IconTooltip).attrs(
  (props) => ({
    width: 14,
    height: 14,
    source: require('@assets/icons/square-checked.png'),
    color: props.disabledColor
      ? props.theme.color.SECONDARY_DEFAULT
      : props.theme.color.TEXT_100_HIGH,
  }),
)``;

export const StyledUncheckedIcon: ComponentType<any> = styled(
  IconTooltip,
).attrs((props) => ({
  width: 14,
  height: 14,
  source: require('@assets/icons/square-unchecked.png'),
  color: props.theme.color.TEXT_300_LOW,
}))``;

export const ErrorText: ComponentType<any> = styled(Text).attrs((props) => ({
  themeColor: 'RED_STATUS',
}))`
  margin-top: ${verticalScale(10)}px;
  font-family: Myriad Pro;
`;

export const StyledCalendarIcon: ComponentType<any> = styled(IconTooltip).attrs(
  (props) => ({
    source: require('@assets/icons/calendar.png'),
    color:
      props.name === props.selected
        ? props.theme.calendar.iconActiveBgColor
        : props.theme.calendar.iconInActiveBgColor,
    width: 20,
    height: 20,
  }),
)`
  margin: ${scale(5)}px ${scale(10)}px;
`;
