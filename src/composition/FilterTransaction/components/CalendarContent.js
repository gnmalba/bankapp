/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity, View} from 'react-native';

// Components
import FilterCalendar from '@composition/FilterCalendar';

// Third party libraries
import moment from 'moment';

// Utils
import {
  ByDateText,
  DateContainer,
  DateFromText,
  DateToText,
  FromDateCalendarButton,
  FromDateInputContainer,
  BorderLeft,
  FromTextInput,
  ToDateCalendarButton,
  ToDateInputContainer,
  ToTextInput,
  StyledCalendarIcon,
} from './Styled';

import {testProps} from '@utils/componentCustomProps';

export type PropsType = {
  showCalendar: boolean,
  setShowCalendar: any,
  calendarData: Object,
  setCalendarData: any,
  scrollViewRef?: any,
  calendarTipRef?: any,
  calendarPrevValues: Object,
  setCalendarPrevValues: (any) => any,
  cardType?: string,
};

const DATE_FORMAT = 'MM/DD/YYYY';

const CalendarContent = (props: PropsType): React$Node => {
  const {
    showCalendar,
    setShowCalendar,
    calendarData,
    setCalendarData,
    scrollViewRef,
    calendarTipRef,
    calendarPrevValues,
    setCalendarPrevValues,
    cardType,
  } = props;
  const [calendarButtonActv, setCalendarButtonActv] = React.useState(false);

  const handleClickCalendarFrom = () => {
    const {dateFrom} = calendarData;
    setShowCalendar(true);
    setCalendarData({
      ...calendarData,
      dateFrom: showCalendar ? dateFrom : moment().format(DATE_FORMAT),
      dateTo: null,
      fieldSelected: 'calendarFromField',
    });
    scrollToTop();
    setCalendarButtonActv(false);
  };

  const scrollToTop = () => {
    if (scrollViewRef) {
      scrollViewRef.current.scrollTo({
        y: 0,
        animated: false,
      });
    }
  };

  const handleClickCalendarTo = () => {
    const {dateFrom, dateTo} = calendarData;
    setShowCalendar(true);
    setCalendarData({
      ...calendarData,
      dateTo: dateTo,
      fieldSelected: 'calendarFromField',
    });
    scrollToTop();
    if (dateFrom && dateTo) {
      setCalendarButtonActv(true);
    }
  };

  const dateToCurrentDate = moment(calendarData.dateTo)
    .subtract(1, 'days')
    .format('MM/DD/YYYY');

  const calendarToValue =
    cardType === 'credit' ? calendarData.dateTo : dateToCurrentDate;

  return (
    <>
      {!showCalendar && (
        <ByDateText {...testProps('filterTransactionByDateLabelId')}>
          DATE
        </ByDateText>
      )}
      <DateContainer>
        <DateFromText {...testProps('filterTransactionFromLabelId')}>
          From
        </DateFromText>
        <DateToText {...testProps('filterTransactionToLabelId')}>To</DateToText>
      </DateContainer>
      {/* eslint-disable-next-line react-native/no-inline-styles */}
      <View style={{flexDirection: 'row', marginTop: 10}}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={handleClickCalendarFrom}
          {...testProps('handlePressFromCalenderTestID')}>
          <FromDateCalendarButton fieldSelected={calendarData.fieldSelected}>
            <StyledCalendarIcon
              name="calendarFromField"
              selected={calendarData.fieldSelected}
            />
            <FromDateInputContainer>
              <BorderLeft />
              <FromTextInput
                fromTxtColor={calendarData.dateFrom}
                {...testProps('filterTransactionDateFromId')}>
                {calendarData.dateFrom ? calendarData.dateFrom : DATE_FORMAT}
              </FromTextInput>
            </FromDateInputContainer>
          </FromDateCalendarButton>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={1}
          onPress={handleClickCalendarTo}
          {...testProps('handlePressToCalenderTestID')}>
          <ToDateCalendarButton fieldSelected={calendarData.fieldSelected}>
            <StyledCalendarIcon
              name="calendarToField"
              selected={calendarData.fieldSelected}
            />
            <ToDateInputContainer>
              <BorderLeft />
              <ToTextInput
                toTxtColor={calendarData.dateTo}
                {...testProps('filterTransactionDateToId')}>
                {calendarData.dateTo ? calendarToValue : DATE_FORMAT}
              </ToTextInput>
            </ToDateInputContainer>
          </ToDateCalendarButton>
        </TouchableOpacity>
      </View>

      {showCalendar && (
        <FilterCalendar
          calendarData={calendarData}
          calendarTipRef={calendarTipRef}
          setShowCalendar={setShowCalendar}
          setCalendarData={setCalendarData}
          calendarButtonActv={calendarButtonActv}
          setCalendarButtonActv={setCalendarButtonActv}
          calendarPrevValues={calendarPrevValues}
          setCalendarPrevValues={setCalendarPrevValues}
          cardType={cardType}
        />
      )}
    </>
  );
};

export default CalendarContent;
