/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';

// Components
import IconTooltip from '@components/IconTooltip';

// Utils
import {StyledCheckbox} from './Styled';
import {verticalScale, scale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';
import styled from 'styled-components';

export type PropsType = {
  runningBalance: boolean,
  setRunningBalance: any,
};

const StyledCheckedIcon = styled(IconTooltip).attrs((props) => ({
  width: scale(18),
  height: verticalScale(18),
  source: require('@assets/icons/square-checked.png'),
  color: props.theme.color.SECONDARY_DEFAULT,
}))``;

const StyledUncheckedIcon = styled(IconTooltip).attrs((props) => ({
  width: scale(18),
  height: verticalScale(18),
  source: require('@assets/icons/square-unchecked.png'),
  color: props.theme.color.TEXT_300_LOW,
}))``;

const CheckboxContent = (props: PropsType): React$Node => {
  const {runningBalance, setRunningBalance} = props;

  const handleClickCheckbox = () => {
    setRunningBalance(!runningBalance);
  };

  return (
    <>
      <View
        style={{
          marginTop: verticalScale(30),
        }}>
        <StyledCheckbox
          {...testProps('showRunningBalanceCheckbox')}
          title="Show running balance"
          checked={runningBalance}
          onPress={handleClickCheckbox}
          checkedIcon={<StyledCheckedIcon {...testProps('darkCheckboxIcon')} />}
          uncheckedIcon={
            <StyledUncheckedIcon
              {...testProps('filterTransactionBalUncheckId')}
            />
          }
          iconType={'ionicon'}
          Component={TouchableWithoutFeedback}
        />
      </View>
    </>
  );
};

export default CheckboxContent;
