## EnrollButton
EnrollButton component.

# Usage
```js
import EnrollButton from '@screens/common/EnrollButton';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
