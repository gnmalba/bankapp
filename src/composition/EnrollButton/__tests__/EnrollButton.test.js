import React from 'react';
import {render} from '@testing-library/react-native';
import EnrollButton from '../EnrollButton';

describe('EnrollButton', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<EnrollButton />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
