/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const ButtonContainer: ComponentType<any> = styled.View`
  position: absolute;
  bottom: ${verticalScale(30)}px;
  right: 15px;
`;
