/**
 * @flow
 */

export type PropsType = {
  testId: string,
  accessibilityLabel?: string,
  onPress: () => void,
};
