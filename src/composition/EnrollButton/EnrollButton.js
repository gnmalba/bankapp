/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import Button from '@components/Button';
import {testProps} from '@utils/componentCustomProps';

const EnrollButton = (props: PropsType): React$Node => {
  const {onPress, testId} = props;
  return (
    <S.ButtonContainer>
      <Button
        type="fab"
        icon={require('@assets/icons/fab-add-icon.png')}
        customIconHeight={11}
        customIconWidth={12}
        state="default"
        title="Enroll"
        onPress={onPress}
        {...testProps(testId)}
      />
    </S.ButtonContainer>
  );
};

export default EnrollButton;
