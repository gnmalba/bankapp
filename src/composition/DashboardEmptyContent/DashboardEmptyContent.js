/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {
  StyledEmptyTextContainer,
  StyledEmptyTextTouchableOpacity,
  StyledPlusIcon,
  StyledText,
} from './Styled';
import {testProps} from '@utils/componentCustomProps';

const DashboardEmptyContent = ({onPressText}: PropsType): React$Node => {
  return (
    <StyledEmptyTextContainer>
      <StyledEmptyTextTouchableOpacity
        {...testProps('addShortcut')}
        onPress={onPressText}>
        <StyledPlusIcon />
        <StyledText>Add a shortcut</StyledText>
      </StyledEmptyTextTouchableOpacity>
    </StyledEmptyTextContainer>
  );
};

export default DashboardEmptyContent;
