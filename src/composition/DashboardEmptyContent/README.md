## DashboardEmptyContent
DashboardEmptyContent component.

# Usage
```js
import DashboardEmptyContent from '@composition/DashboardEmptyContent';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
