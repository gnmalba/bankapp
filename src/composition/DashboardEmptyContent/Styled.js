/**
 * @flow
 */

import type {ComponentType} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import styled from 'styled-components/native';
import {moderateScale, scale, verticalScale} from '@utils/scale';
import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View``;

export const StyledEmptyTextContainer: ComponentType<any> = styled.View`
  padding: ${moderateScale(50)}px;
  justify-content: center;
  align-content: center;
  align-items: center;
`;

export const StyledEmptyTextTouchableOpacity: ComponentType<any> = styled(
  TouchableOpacity,
)`
  flex: 1;
  flex-direction: row;
`;

export const StyledPlusIcon: ComponentType<any> = styled(Image).attrs(
  (props) => {
    return {
      source: require('@assets/icons/plus-icon.png'),
    };
  },
)`
  width: ${scale(18)}px;
  height: ${verticalScale(18)}px;
`;

export const StyledText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'subtitle',
  themeColor: 'SECONDARY_DEFAULT',
}))`
  align-self: center;
  margin-left: ${scale(8)}px;
`;
