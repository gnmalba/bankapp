import React from 'react';
import {render} from '@testing-library/react-native';
import DashboardEmptyContent from '../DashboardEmptyContent';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('DashboardEmptyContent', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <DashboardEmptyContent />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
