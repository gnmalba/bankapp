/**
 * @format
 * @flow
 */
import {moderateScale, scale, verticalScale} from '@utils/scale';
import styled from 'styled-components/native';
import type {ComponentType} from 'react';

const CircleContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
`;

const CircleItem: ComponentType<any> = styled.View`
  height: ${(props) => scale(props.size)}px;
  width: ${(props) => scale(props.size)}px;
  border-radius: ${(props) => scale(props.size / 2)}px;
  background-color: ${(props) => props.theme.circleBackground}
  justify-content: center;
  align-items: center;
  margin-horizontal: ${scale(2)};
`;

const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  margin-horizontal: ${scale(32)};
`;

const IconContainer: ComponentType<any> = styled.View`
  padding: 18px;
`;

const ImageContainer: ComponentType<any> = styled.Image`
  height: ${moderateScale(24)};
  width: ${moderateScale(24)};
  margin-horizontal: ${scale(4)}px;
`;

const TouchableContainer: ComponentType<any> = styled.TouchableOpacity`
  align-items: center;
  background-color: ${(props) => props.customTheme.color.BACKGROUND_100};
  flex-direction: row;
  margin-vertical: ${verticalScale(8)};
  border-radius: 4;
`;

export {
  CircleContainer,
  CircleItem,
  ImageContainer,
  IconContainer,
  MainContainer,
  TouchableContainer,
};
