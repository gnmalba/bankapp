export const fnIconMapper = (name) => {
  switch (name) {
    case 'Airlines/Travel Agencies':
      return 'airplane';
    case 'Credit Card':
      return 'creditCard';
    case 'Government':
      return 'bank';
    case 'Others':
      return 'others';
    case 'Utilities':
      return 'utilities';

    default:
      return null;
  }
};

export const ICON_MAPPER = {
  airplane: require('@assets/icons/airplane.png'),
  creditCard: require('@assets/icons/credit-card.png'),
  bank: require('@assets/icons/bank.png'),
  utilities: require('@assets/icons/utilities.png'),
  others: require('@assets/icons/others.png'),
};

export const filterByCategory = ({category, data}) => {
  return data.filter((d) => d.name === category);
};
