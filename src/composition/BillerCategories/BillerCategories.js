import React, {useState} from 'react';
import {FlatList} from 'react-native';

// import Circle from '@components/Circle';
import Text from '@components/Text';

import {DARK_THEME} from '@utils/constants';
import {useTheme} from 'styled-components';

import {
  CircleContainer,
  CircleItem,
  IconContainer,
  ImageContainer,
  MainContainer,
  TouchableContainer,
} from './Styled';

// import DATA from './mockData';
import {fnIconMapper, ICON_MAPPER} from './utils';

const BillerCategories = ({categories, onPress}): React$Node => {
  const theme = useTheme() || DARK_THEME;
  const [list] = useState(categories);

  const onPressIcon = (item): void => {
    onPress(item.id);
  };

  const renderIcon = (name) => {
    const iconName = fnIconMapper(name);
    if (iconName) {
      return (
        <ImageContainer resizeMode="contain" source={ICON_MAPPER[iconName]} />
      );
    }
    return (
      <CircleContainer>
        <CircleItem size={26}>
          <Text mode="body1">{name.charAt(0)}</Text>
        </CircleItem>
      </CircleContainer>
    );
  };

  const renderItem = ({item}) => {
    return (
      <TouchableContainer onPress={() => onPressIcon(item)} customTheme={theme}>
        <IconContainer>{renderIcon(item.name)}</IconContainer>
        <Text mode="body1">{item.name}</Text>
      </TouchableContainer>
    );
  };

  return (
    <MainContainer>
      <Text mode="h3">Biller Categories</Text>

      <FlatList
        data={list}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
    </MainContainer>
  );
};

export default BillerCategories;
