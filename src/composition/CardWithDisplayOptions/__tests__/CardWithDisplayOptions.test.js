import * as React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import CardwithDisplayOptions from '../index';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('CardwithDisplayOptions', () => {
  const mockAppState = {
    config: {
      theme: 'dark',
      widgets: [
        {
          ref: 1,
          label: 'Deposits',
          loading: false,
        },
        {
          ref: 2,
          label: 'TestLabelB',
          loading: false,
        },
      ],
      removedWidgets: [],
      checked: [],
      limitShown: false,
    },
  };

  const defaultProps = {
    ...mockAppState,
    children: null,
    title: 'Deposits',
    accountPesoAndForeign: {
      peso: [],
      foreign: [],
    },
    onCurrencySelected: jest.fn(() => {}),
  };

  it('Should work as expected', () => {
    const props = {
      ...defaultProps,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <CardwithDisplayOptions
            {...testProps('cardWithDisplayOptions')}
            {...props}
          />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Deposit option should work as expected', () => {
    const props = {
      ...defaultProps,
      title: 'Deposits',
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <CardwithDisplayOptions
            {...testProps('cardWithDisplayOptions')}
            {...props}
          />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('DisplayExtraActions should work as expected', async () => {
    const props = {
      ...defaultProps,
    };
    const setState = jest.fn();
    const useStateMock = (initState) => [true, setState];

    jest.spyOn(React, 'useState').mockImplementationOnce(useStateMock);

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <CardwithDisplayOptions
            {...testProps('cardWithDisplayOptions')}
            {...props}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    const el1 = all.getByTestId('cardWithDisplayOptionsToolTip');
    fireEvent(el1, 'onClose');

    const el2 = all.getByTestId('cardWithDisplayOptionsTitle');
    fireEvent(el2, 'onPress');

    expect(all.toJSON()).toMatchSnapshot();
  });

  it('RadioBoxOption onPress should work as expected', () => {
    const props = {
      ...defaultProps,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <CardwithDisplayOptions
            {...testProps('cardWithDisplayOptions')}
            {...props}
          />
        </ThemeProvider>
      </MockProvider>,
    );

    const el1 = all.getByTestId('cardWithDisplayOptionsToggler');
    fireEvent(el1, 'onPress');
  });
});
