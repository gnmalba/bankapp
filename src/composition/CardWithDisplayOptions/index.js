/**
 * @flow
 */

import React, {useRef, useState} from 'react';

import {useSelector, useDispatch} from 'react-redux';

import {TouchableOpacity} from 'react-native';

import styled from 'styled-components/native';
import Entypo from 'react-native-vector-icons/Entypo';
import {Tooltip} from 'react-native-elements';

import {
  Container as StyledCardContainer,
  Header as StyledCardHeader,
  Divider as StyledCardDivider,
} from '@components/Card/Styled';

import Text from '@components/Text';

import {moderateScale, scale, verticalScale} from '@utils/scale';

import RadioBox from './components/RadioBox';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {getWidgets} from '@ducks/config/selectors';
import {removeWidget} from '@ducks/config/actions';
import {toggleCustomizeWidget} from '@ducks/dashboard/actions';

import type {
  AccountShape,
  DisplayOptionsCurrency,
} from '@ducks/accounts-displayoptions/types';
import type {Widget} from '@ducks/config/types';

import {navigate} from '@navigations/service';

import {IS_DEVICE_TABLET} from '@utils/constants';

import {
  StyledRadioButtonContainer,
  StyledRadioButtonLabel,
} from './components/RadioBox/Styled';
import {testProps} from '@utils/componentCustomProps';

const StyledDotsVerticalIcon = styled(Entypo).attrs((props) => ({
  color: props.theme.color.TEXT_100_HIGH,
  size: moderateScale(16),
  name: 'dots-three-vertical',
}))``;

const StyledTooltip = styled(Tooltip).attrs((props) => ({
  backgroundColor: props.theme.displayOptionsTooltip.tooltip,
  containerStyle: {
    transform: [{translateY: verticalScale(6)}, {translateX: scale(-3)}],
  },
}))``;

const StyledDivider = styled(StyledCardDivider)`
  background-color: ${(props) => props.theme.displayOptionsTooltip.divider};
  margin: 0;
`;

const StyledTooltipContainer = styled.View`
  background-color: ${(props) => props.theme.displayOptionsTooltip.tooltip};
  border-radius: ${moderateScale(6)}px;
  padding: ${scale(16)}px;
  width: ${moderateScale(240)}px;
  height: ${verticalScale(IS_DEVICE_TABLET ? 250 : 210)}px;
`;

const StyledDisplayOptionsToggler = styled.TouchableOpacity`
  background-color: ${(props) =>
    props.active ? props.theme.displayOptionsTooltip.tooltip : 'transparent'};
  border-radius: 50px;
  padding: 3px;
`;

const StyledDisplayOptionsTitle = styled(Text).attrs((props) => ({
  mode: 'overline2',
  customColor: props.theme.color.HIGH_EMPHASIS,
}))`
  margin-bottom: ${verticalScale(10)}px;
`;

type CardWithDisplayOptionsPropTypes = {
  children: React$Node,
  title: string,
  accountPesoAndForeign: AccountShape,
  onCurrencySelected: (ref: DisplayOptionsCurrency) => void,
};

const StyledMoveIcon = styled(Ionicons).attrs((props) => ({
  name: 'move-outline',
  color: props.theme.color.TEXT_100_HIGH,
  size: scale(20),
}))`
  transform: rotate(0deg);
`;

const StyledRemoveIcon = styled(Ionicons).attrs((props) => ({
  name: 'trash',
  color: props.disabled
    ? props.theme.color.PRIMARY_DISABLED
    : props.theme.color.TEXT_100_HIGH,
  size: scale(20),
}))``;

const StyledDisplayOptionsExtraActionsContainer = styled.View`
  margin-top: ${verticalScale(10)}px;
`;

type DisplayOptionsExtraActionsPropTypes = {
  onMovePress: () => void,
  onRemovePress: () => void,
  selectedTitle: string,
};

const DisplayOptionsExtraActions = ({
  onMovePress,
  onRemovePress,
  selectedTitle,
}: DisplayOptionsExtraActionsPropTypes) => {
  const isRemoveDisabled = selectedTitle === 'Deposits';

  return (
    <StyledDisplayOptionsExtraActionsContainer>
      <TouchableOpacity onPress={onMovePress}>
        <StyledRadioButtonContainer>
          <StyledMoveIcon />
          <StyledRadioButtonLabel>Move</StyledRadioButtonLabel>
        </StyledRadioButtonContainer>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={isRemoveDisabled ? 1 : 0.5}
        onPress={() => {
          if (!isRemoveDisabled) {
            onRemovePress();
          }
        }}>
        <StyledRadioButtonContainer>
          <StyledRemoveIcon disabled={isRemoveDisabled} />
          <StyledRadioButtonLabel disabled={isRemoveDisabled}>
            Remove Widget
          </StyledRadioButtonLabel>
        </StyledRadioButtonContainer>
      </TouchableOpacity>
    </StyledDisplayOptionsExtraActionsContainer>
  );
};

const defaultOptions: Array<DisplayOptionsCurrency> = [
  'peso',
  'foreign',
  'all',
];

const CardWithDisplayOptions = ({
  children,
  title,
  accountPesoAndForeign,
  onCurrencySelected,
}: CardWithDisplayOptionsPropTypes): React$Node => {
  const dispatch = useDispatch();

  const widgets = useSelector(getWidgets);

  const toolTipRef = useRef(null);
  const [isTooltipShown, setIsTooltipShown] = useState(false);
  const [contentContainerDimention, setContentContainerDimention] = useState({
    height: 0,
    width: 0,
  });

  const getWidgetRefByLabel = (): Widget =>
    widgets.find((widget) => widget.label === title);

  const toggleTooltip = () => {
    if (toolTipRef.current) {
      if (!toolTipRef.current.isVisible) {
        setIsTooltipShown(true);
      }
      toolTipRef.current.toggleTooltip();
    }
  };

  const onPopoverClose = () => {
    setIsTooltipShown(false);
  };

  const popOverContentContainerOnLayout = (e) => {
    setContentContainerDimention({
      height: e.nativeEvent.layout.height,
      width: e.nativeEvent.layout.width,
    });
  };

  const handleOnTitlePress = () => {
    const selectedWidgetObject = getWidgetRefByLabel();

    if (typeof selectedWidgetObject !== 'undefined') {
      /**
       * TODO: This should redirect to specific tabs, for now navigate in Accounts only
       * const ref = selectedWidgetObject.ref;
       */

      navigate('Accounts');
    }
  };

  return (
    <StyledCardContainer>
      <StyledCardHeader>
        <TouchableOpacity
          onPress={handleOnTitlePress}
          {...testProps('cardWithDisplayOptionsTitle')}>
          <Text mode="h3">{title}</Text>
        </TouchableOpacity>
        <StyledDisplayOptionsToggler
          onPress={toggleTooltip}
          active={isTooltipShown}
          {...testProps('cardWithDisplayOptionsToggler')}>
          <StyledTooltip
            ref={toolTipRef}
            withPointer={false}
            overlayColor="#00000054"
            {...testProps('cardWithDisplayOptionsToolTip')}
            popover={
              <StyledTooltipContainer
                onLayout={popOverContentContainerOnLayout}
                {...testProps('cardWithDisplayOptionsToolTipContainer')}>
                <StyledDisplayOptionsTitle mode="overline2">
                  DISPLAY OPTIONS
                </StyledDisplayOptionsTitle>
                <RadioBox
                  options={[
                    {
                      ref: 'peso',
                      text: 'Peso Accounts',
                      disabled: accountPesoAndForeign.peso.length === 0,
                    },
                    {
                      ref: 'foreign',
                      text: 'Foreign Accounts',
                      disabled: accountPesoAndForeign.foreign.length === 0,
                    },
                    {
                      ref: 'all',
                      text: 'All Accounts',
                    },
                  ]}
                  selected={accountPesoAndForeign.selected}
                  onRadioSelected={(ref) => {
                    if (defaultOptions.includes(ref)) {
                      const index = defaultOptions.indexOf(ref);
                      onCurrencySelected(defaultOptions[index]);
                    }
                  }}
                />
                <StyledDivider />
                <DisplayOptionsExtraActions
                  selectedTitle={title}
                  onMovePress={() => {
                    toggleTooltip();
                    dispatch(toggleCustomizeWidget());
                    navigate('CustomizeWidgetScreen');
                  }}
                  onRemovePress={() => {
                    const widgetRefToBeRemove = getWidgetRefByLabel();

                    if (typeof widgetRefToBeRemove !== 'undefined') {
                      toggleTooltip();
                      dispatch(removeWidget(widgetRefToBeRemove.ref, true));
                    }
                  }}
                />
              </StyledTooltipContainer>
            }
            width={contentContainerDimention.width}
            height={contentContainerDimention.height}
            onClose={onPopoverClose}
          />
          <StyledDotsVerticalIcon />
        </StyledDisplayOptionsToggler>
      </StyledCardHeader>
      <StyledCardDivider />
      {children}
    </StyledCardContainer>
  );
};

export default CardWithDisplayOptions;
