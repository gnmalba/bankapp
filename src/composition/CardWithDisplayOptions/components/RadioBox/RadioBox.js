/**
 * @flow
 */
import React, {useState} from 'react';

import {TouchableOpacity} from 'react-native';

import {testProps} from '@utils/componentCustomProps';

import {
  StyledRadioBoxContainer,
  StyledRadioButtonContainer,
  StyledActiveRadioButton,
  StyledInactiveRadioButton,
  StyledRadioButtonLabel,
} from './Styled';

type REF = number | string;

type RadioOption = {|
  ref: REF,
  text: string,
  disabled?: boolean,
|};

type RadioBoxPropTypes = {|
  options: Array<RadioOption>,
  onRadioSelected: (ref: REF) => void,
  selected?: REF,
|};

const RadioBox = ({
  options,
  selected,
  onRadioSelected,
}: RadioBoxPropTypes): React$Node => {
  const [localSelectedOption, setLocalSelectedOption] = useState(selected);

  if (options.length === 0) {
    return null;
  }

  const handleOnSetLocalSelected = (ref: REF) => {
    setLocalSelectedOption(ref);
    onRadioSelected(ref);
  };

  return (
    <StyledRadioBoxContainer>
      {options.map(({ref, text, disabled}, index) => {
        return (
          <TouchableOpacity
            key={`${ref}-${index}`}
            {...testProps(`radioBoxOption${ref}-${index}`)}
            activeOpacity={disabled ? 1 : 0.5}
            onPress={() => {
              if (!disabled) {
                handleOnSetLocalSelected(ref);
              }
            }}>
            <StyledRadioButtonContainer>
              {localSelectedOption === ref ? (
                <StyledActiveRadioButton />
              ) : (
                <StyledInactiveRadioButton disabled={disabled} />
              )}
              <StyledRadioButtonLabel disabled={disabled}>
                {text}
              </StyledRadioButtonLabel>
            </StyledRadioButtonContainer>
          </TouchableOpacity>
        );
      })}
    </StyledRadioBoxContainer>
  );
};

RadioBox.defaultProps = {
  onRadioSelected: () => {},
};

export default RadioBox;
