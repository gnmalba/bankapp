/**
 * @flow
 */
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import Text from '@components/Text';

import Ionicons from 'react-native-vector-icons/Ionicons';

import {scale, verticalScale} from '@utils/scale';

export const StyledRadioBoxContainer: ComponentType<any> = styled.View``;

export const StyledActiveRadioButton: ComponentType<any> = styled(
  Ionicons,
).attrs((props) => ({
  name: 'radio-button-on-outline',
  color: props.theme.color.SECONDARY_DEFAULT,
  size: scale(20),
}))``;

export const StyledInactiveRadioButton: ComponentType<any> = styled(
  Ionicons,
).attrs((props) => ({
  name: 'radio-button-off-outline',
  color: props.disabled
    ? props.theme.color.PRIMARY_DISABLED
    : props.theme.color.TEXT_100_HIGH,
  size: scale(20),
}))``;

export const StyledRadioButtonContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: ${verticalScale(10)}px;
`;

export const StyledRadioButtonLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    ...(props.disabled && {
      customColor: props.theme.color.PRIMARY_DISABLED,
    }),
  }),
)`
  margin-left: ${scale(7)}px;
  font-size: ${scale(16)}px;
`;
