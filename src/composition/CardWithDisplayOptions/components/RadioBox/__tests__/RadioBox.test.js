import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import RadioBox from '../RadioBox';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

const generateTestOptions = (n) => {
  return [...Array(n).keys()].map((value, index) => {
    return {
      ref: index,
      disabled: index % 2 === 0, // for testing only, alternate true and false
      text: `TestOption${index}`,
    };
  });
};

describe('RadioBox', () => {
  const mockAppState = {};

  const defaultProps = {
    ...mockAppState,
    options: generateTestOptions(10),
    onRadioSelected: jest.fn(() => {}),
    selected: null,
  };

  it('Should work as expected', () => {
    const props = {
      ...defaultProps,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <RadioBox {...testProps('radioBoxTestId')} {...props} />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('RadioBoxOption onPress should work as expected', () => {
    const props = {
      ...defaultProps,
    };

    const all = render(
      <MockProvider store={mockAppState}>
        <ThemeProvider theme={DARK_THEME}>
          <RadioBox {...testProps('radioBoxTestId')} {...props} />
        </ThemeProvider>
      </MockProvider>,
    );

    const el1 = all.getByTestId('radioBoxOption0-0');
    fireEvent(el1, 'onPress');

    const el2 = all.getByTestId('radioBoxOption1-1');
    fireEvent(el2, 'onPress');
  });

  it.each([{options: []}, {options: generateTestOptions(10)}])(
    'should render correctly when %o',
    (additionalProps) => {
      const props = {
        ...defaultProps,
        ...additionalProps,
      };

      const all = render(
        <MockProvider store={mockAppState}>
          <ThemeProvider theme={DARK_THEME}>
            <RadioBox {...testProps('radioBoxTestId')} {...props} />
          </ThemeProvider>
        </MockProvider>,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
