// @flow

import {Image} from 'react-native';
import {scale, verticalScale} from '@utils/scale';
import styled from 'styled-components/native';
import {CircleItem} from '@components/Circle/Styled';
import {IS_DEVICE_TABLET} from '@utils/constants';

export const CircleContainer: any = styled.View`
  justify-content: center;
  flex-direction: row;
  margin-top: ${verticalScale(16)}px;
  height: ${verticalScale(100)}px;
  width: ${scale(82)}px;
`;

export const ItemContainer: any = styled.View`
  justify-content: center;
  align-self: center;
  flex-direction: column;
  align-items: center;
`;

export const CircleText: any = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(14)}px;
  line-height: ${scale(17)}px;
  text-align: center;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  margin-top: ${verticalScale(5)}px;
  min-height: ${verticalScale(50)}px;
`;

export const CustomCircle: any = styled(CircleItem).attrs((props) => ({
  size: IS_DEVICE_TABLET ? props.size * 0.8 : props.size,
}))`
  background-color: ${(props) =>
    props.displayIn === 'dashboard'
      ? props.theme.circle.dashboardBgColor
      : props.theme.circle.overviewBgColor};
`;

export const CustomImage: any = styled(Image)`
  resize-mode: contain;
  height: ${scale(24)}px;
  width: ${scale(24)}px;
`;
