/**
 * @format
 * @flow
 */

import React from 'react';
import {TouchableOpacity} from 'react-native';

import Text from '@components/Text';
import {
  CircleText,
  ItemContainer,
  CircleContainer,
  CustomCircle,
  CustomImage,
} from './Styled';
import {testProps} from '@utils/componentCustomProps';

type PropsType = {
  name: string,
  icon?: any,
  onClick?: () => void,
  disabled?: boolean,
  theme?: Object,
  displayIn?: 'dashboard' | 'accounts',
  testID?: string,
};

const ShortcutIconButton = ({
  name,
  icon,
  onClick,
  theme,
  disabled = false,
  displayIn = 'dashboard',
  testID = '',
}: PropsType): React$Node => {
  const handleOnPress = () => {
    if (onClick) {
      onClick();
    }
  };

  const renderContent = () => {
    return icon ? (
      <CustomImage source={icon} {...testProps(testID + 'Image')} />
    ) : (
      <Text mode="body1" {...testProps(testID + 'Image')}>
        N/A
      </Text>
    );
  };

  return (
    <TouchableOpacity
      onPress={handleOnPress}
      disabled={disabled}
      activeOpacity={onClick ? 0.2 : 1}
      {...testProps(`${testID}Touchable`)}>
      <CircleContainer>
        <ItemContainer>
          <CustomCircle size={56} displayIn={displayIn}>
            {renderContent()}
          </CustomCircle>
          <CircleText theme={theme} {...testProps(testID + 'Label')}>
            {name}
          </CircleText>
        </ItemContainer>
      </CircleContainer>
    </TouchableOpacity>
  );
};

export default ShortcutIconButton;
