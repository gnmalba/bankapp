import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import ShortcutIconButton from '../ShortcutIconButton';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME, SHORTCUT_ICONS} from '@utils/constants';

describe('ShortcutIconButton', () => {
  const possibleProps = [
    {
      name: 'Balance Conversion',
      icon: SHORTCUT_ICONS.BALANCE_CONVERSION,
      onClick: () => jest.fn(),
      disabled: false,
      testID: '',
    },
    {
      name: 'Balance Transfer',
      icon: null,
      onClick: undefined,
      disabled: true,
      testID: 'test',
    },
  ];
  it.each(possibleProps)('Should work as expected', (props) => {
    // Edit the args inside render and provide props.

    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ShortcutIconButton props={props} />
      </ThemeProvider>,
    );

    if (props.onClick) {
      all.debug();
      const touchable = all.getByTestId(`${props.testID}Touchable`);
      fireEvent.press(touchable);
    }

    expect(all.toJSON()).toMatchSnapshot();
  });
});
