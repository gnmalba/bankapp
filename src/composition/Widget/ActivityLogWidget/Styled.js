/**
 * @flow
 */
import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {ListItem} from 'react-native-elements';

import Text from '@components/Text';

const StyledListItem: typeof ListItem = styled(ListItem).attrs({
  containerStyle: {
    backgroundColor: 'transparent',
    paddingHorizontal: 0,
    paddingVertical: 10,
  },
})``;

const Description: ComponentType<any> = styled(Text).attrs({
  mode: 'body2',
})``;

const StyledDate: ComponentType<any> = styled(Text).attrs({
  mode: 'overline2',
})`
  color: ${(props) => props.theme.activityLog.date};
`;

export {StyledListItem, Description, StyledDate};
