/**
 * @flow
 */

import React from 'react';

import Card from '@components/Card';

import {StyledListItem, Description, StyledDate} from './Styled';

const mockDate = '01/07/2020';

const MOCKS = [
  {
    description: 'Renamed Savings Account 1 to My Savings',
    date: mockDate,
  },
  {
    description: 'Change Password',
    date: mockDate,
  },
  {
    description: 'Change Passcode',
    date: mockDate,
  },
  {
    description: 'Unlinked device iPhone 7 (iOS 13.0)',
    date: mockDate,
  },
  {
    description: 'Generated a static QR code',
    date: mockDate,
  },
];

type ActivityLogRowPropTypes = {
  description: string,
  date: string,
};
const ActivityLogRow = ({
  description,
  date,
}: ActivityLogRowPropTypes): React$Node => (
  <StyledListItem>
    <StyledListItem.Content>
      <StyledListItem.Title>
        <Description>{description}</Description>
      </StyledListItem.Title>
      <StyledListItem.Subtitle>
        <StyledDate>{date}</StyledDate>
      </StyledListItem.Subtitle>
    </StyledListItem.Content>
  </StyledListItem>
);

type ActivityLogPropTypes = {
  label: string,
};

const ActivityLogWidget = (props: ActivityLogPropTypes): React$Node => {
  if (MOCKS.length === 0) {
    return null;
  }

  return (
    <Card title={props.label}>
      {MOCKS.map((row, index) => (
        <ActivityLogRow
          key={index}
          description={row.description}
          date={row.date}
        />
      ))}
    </Card>
  );
};

export default ActivityLogWidget;
