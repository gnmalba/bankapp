/**
 * @flow
 */

import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import * as NavigationService from '@navigations/service';
import ShortcutIconButton from '@composition/ShortcutIconButton';
import {SHORTCUT_ICONS_CODE, SHORTCUT_ICONS_NAME} from '@utils/constants';

import {ShortcutContainer, Container} from './Styled';
import type {ShortcutType, ShortcutsState} from '@ducks/shortcuts/types';
import {getCheckedShortcuts} from '@ducks/shortcuts/selectors';
import DashboardCard from '@composition/DashboardCard';
import {createDisplayOptionItem} from '@composition/DashboardCard/utils';
import {
  addCheckedShortcut,
  removeCheckedShortcut,
} from '@ducks/shortcuts/actions';
import DashboardEmptyContent from '@composition/DashboardEmptyContent';
import {testProps} from '@utils/componentCustomProps';

const SHORTCUTS_ENUM = {
  REPORT_LOST_CARD: 'Report as lost',
  ACTIVATE_CARDS: 'Activate cards',
  PAY_BILLS: 'Pay Bills',
  SEND_MONEY: 'Send Money',
};

const handleOnPress = (item: Object, navigation): void => {
  const {stack, screen, name} = item;

  if (stack && screen) {
    NavigationService.navigate(stack, {screen});
  } else {
    switch (name) {
      case SHORTCUTS_ENUM.REPORT_LOST_CARD:
        NavigationService.navigate('Report Lost Card');
        break;
      case SHORTCUTS_ENUM.ACTIVATE_CARDS:
        NavigationService.navigate('ActivateCardMainStack');
        break;
      case SHORTCUTS_ENUM.PAY_BILLS:
        NavigationService.navigate('PayBillsCredit', {screen: 'MainScreen'});
        break;
      case SHORTCUTS_ENUM.SEND_MONEY:
        NavigationService.navigate('SendMoneyStack', {
          screen: 'MainScreen',
        });
        break;
      default:
        break;
    }
  }
};

const renderShorcuts = (shortcuts: ShortcutType[]) =>
  shortcuts
    .filter((shortcut) => shortcut.forShortcut === true)
    .map((shortcut, i) => {
      const uniqueIndex = i + 1;
      return (
        <ShortcutContainer key={`${shortcut.code}-${i}`}>
          <ShortcutIconButton
            icon={SHORTCUT_ICONS_CODE[shortcut.code]}
            name={SHORTCUT_ICONS_NAME[shortcut.code]}
            onClick={() => handleOnPress(shortcut)}
            testID={`Shortcut${uniqueIndex}`}
          />
        </ShortcutContainer>
      );
    });

type ShortcutWidgetPropTypes = {
  label: string,
};

const ShortcutWidget = (props: ShortcutWidgetPropTypes): React$Node => {
  const checkedShortcuts: ShortcutType[] = useSelector(getCheckedShortcuts);
  const {
    checkedShortcuts: checkedShortcutIds,
    data,
  }: ShortcutsState = useSelector((state) => state.shortcuts);
  const [tooltipRef, setTooltipRef] = useState(null);
  const [displayOptions, setDisplayOptions] = useState([]);
  const dispatch = useDispatch();

  const toggleTooltip = () => {
    if (tooltipRef?.current) {
      tooltipRef.current.toggleTooltip();
    }
  };

  useEffect(() => {
    setDisplayOptions(
      data.map((shortcut) => {
        const checked = checkedShortcutIds.includes(shortcut.id);
        return createDisplayOptionItem({
          title: SHORTCUT_ICONS_NAME[shortcut.code],
          checked,
          onOptionPress: () => {
            dispatch(
              checked
                ? removeCheckedShortcut(shortcut.id)
                : addCheckedShortcut(shortcut.id),
            );
          },
        });
      }),
    );
  }, [data, checkedShortcuts, dispatch, checkedShortcutIds]);

  return (
    <DashboardCard
      title={props.label}
      options={displayOptions}
      forwardedTooltipRef={setTooltipRef}>
      <Container {...testProps('mainShortcutContainer')}>
        {checkedShortcuts.length > 0 ? (
          renderShorcuts(checkedShortcuts)
        ) : (
          <DashboardEmptyContent onPressText={toggleTooltip} />
        )}
      </Container>
    </DashboardCard>
  );
};

export default ShortcutWidget;
