import {
  DEFAULT_INSTALLMENT_CHECKED,
  DEFAULT_INSTALLMENT_SHORTCUTS,
} from '@ducks/shortcuts/utils';
import {CREDIT_CARDS} from './accounts.mock';
import {ALL_WIDGETS, CHECKED_WIDGETS} from '@ducks/config/reducer';

export const WIDGET_DEFAULT_MOCK_STORE = {
  credit: {
    creditList: {
      loading: false,
      data: CREDIT_CARDS,
      error: false,
      errors: [],
    },
  },
  shortcuts: {
    data: [],
    checkedShortcuts: [],
    installmentsShortcuts: DEFAULT_INSTALLMENT_SHORTCUTS,
    checkedInstallmentsShortcuts: DEFAULT_INSTALLMENT_CHECKED,
  },
  config: {
    widgets: ALL_WIDGETS,
    removedWidgets: [],
    checked: CHECKED_WIDGETS,
    customizedWidgets: CHECKED_WIDGETS,
  },
};
