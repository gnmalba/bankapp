/**
 * @flow
 */

// React modules
import React from 'react';

import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import styled from 'styled-components/native';

import type {WidgetTypes} from './types';

// Components
import {Container as CardContainer} from '@components/Card/Styled';
import DepositWidget from './DepositWidget';
import CreditWidget from './CreditWidget';
import PrepaidWidget from './PrepaidWidget';
import ShortCutWidget from './ShortcutWidget';
import CardInstallmentWidget from './CardInstallmentWidget/CardInstallmentWidget';
import RecentTransactionWidget from './RecentTransactionWidget';
import ActivityLogWidget from './ActivityLogWidget';

import Card from '@components/Card';
import Text from '@components/Text';

const StyledSkeletonPlaceholder = styled(SkeletonPlaceholder).attrs(
  (props) => ({
    highlightColor: props.theme.skeletonPlaceholder.highlightColor,
    backgroundColor: props.theme.skeletonPlaceholder.backgroundColor,
    borderRadius: '2px',
  }),
)``;

export const WidgetLoader = (): React$Node => {
  return (
    <CardContainer>
      <StyledSkeletonPlaceholder>
        <SkeletonPlaceholder.Item width={'30%'} height={20} borderRadius={4} />
        <SkeletonPlaceholder.Item
          width={'100%'}
          height={84}
          borderRadius={4}
          marginTop={20}
        />
        <SkeletonPlaceholder.Item
          width={'100%'}
          height={84}
          borderRadius={4}
          marginTop={20}
        />
      </StyledSkeletonPlaceholder>
    </CardContainer>
  );
};

function Widget({widgetUniqueRef, navigation, label}: WidgetTypes): React$Node {
  switch (widgetUniqueRef) {
    case 1:
      return <DepositWidget navigation={navigation} label={label} />;
    case 2:
      return <CreditWidget navigation={navigation} label={label} />;
    case 3:
      return <PrepaidWidget navigation={navigation} label={label} />;
    case 4:
      return <ShortCutWidget navigation={navigation} label={label} />;
    case 5:
      return <CardInstallmentWidget navigation={navigation} label={label} />;
    case 6:
      return (
        <Card title="Inbox">
          <Text mode="body1">Inbox Widget</Text>
        </Card>
      );
    case 7:
      return <ActivityLogWidget label={label} />;
    case 8:
      return <RecentTransactionWidget navigation={navigation} label={label} />;
    default:
      return null;
  }
}

export default Widget;
