/**
 * @flow
 */

import React from 'react';
import {View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {Divider} from '@components/Card/Card';
import AccountCard from '@composition/AccountCard';
import CardWithDisplayOptions from '@composition/CardWithDisplayOptions';

import {getDepositsForWidgets} from '@ducks/deposit/selectors';
import {getDepositPrimaryAccount} from '@ducks/deposit/utils';
import {getDepositPesoAndForeignAccounts} from '@ducks/accounts-displayoptions/selectors';
import {setSelectedOptionByAccount} from '@ducks/accounts-displayoptions/actions';

import {testProps} from '@utils/componentCustomProps';
import {maskingFormat} from '@utils/accountNumber';

type DepositWidgetPropTypes = {
  label: string,
  navigation: {
    navigate: (route: string, params?: Object) => void,
  },
};

const DepositWidget = (props: DepositWidgetPropTypes): React$Node => {
  const dispatch = useDispatch();

  const depositAccounts = useSelector(getDepositsForWidgets);
  const depositPesoAndForeignAccounts = useSelector(
    getDepositPesoAndForeignAccounts,
  );

  if (depositAccounts.length === 0) {
    return null;
  }

  const depositPrimaryAccount = getDepositPrimaryAccount(depositAccounts);

  let depositWidgets = depositAccounts;

  if (depositPrimaryAccount) {
    depositWidgets = [
      depositPrimaryAccount,
      ...depositAccounts.filter(
        (account) => account.id !== depositPrimaryAccount.id,
      ),
    ];
  }

  depositWidgets = depositWidgets.sort((a, b) => (a.id > b.id ? 1 : -1));

  const filteredDepositWidgets = depositWidgets.filter((account) => {
    const selected = depositPesoAndForeignAccounts.selected;
    if (selected === 'peso') {
      return depositPesoAndForeignAccounts.peso.includes(account.id);
    } else if (selected === 'foreign') {
      return depositPesoAndForeignAccounts.foreign.includes(account.id);
    } else {
      return true;
    }
  });

  const renderAccounts = () =>
    filteredDepositWidgets.map((account, index) => {
      const uniqueIndex = index + 1;
      return (
        <View key={`${account.id}-${index}`}>
          <AccountCard
            name={account.alias}
            accountNumber={maskingFormat(account.accountNo)}
            balanceValue={account.availableBalanceValueWithCurrency}
            balanceLabel="AVAILABLE"
            isPrimary={account.initialAccount}
            testID={'PesoSavings'}
            uniqueIndex={uniqueIndex}
            onClick={() => {
              props.navigation.navigate('DepositScreenOverview', {
                alias: account.alias,
                id: account.id,
              });
            }}
            isInactive={!account.active}
          />
          {index !== filteredDepositWidgets.length - 1 && (
            <Divider {...testProps(uniqueIndex + 'PesoSavingsDivider')} />
          )}
        </View>
      );
    });

  return (
    <CardWithDisplayOptions
      title={props.label}
      accountPesoAndForeign={depositPesoAndForeignAccounts}
      onCurrencySelected={(ref) =>
        dispatch(setSelectedOptionByAccount(ref, 'deposit'))
      }>
      {renderAccounts()}
    </CardWithDisplayOptions>
  );
};

export default DepositWidget;
