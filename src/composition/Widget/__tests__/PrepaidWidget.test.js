import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import PrepaidWidget from '../PrepaidWidget';
import {DARK_THEME} from '@utils/constants';
import {ThemeProvider} from 'styled-components/native';
import MockProvider from '@utils/testFramework';
import {PREPAID_ACCOUNTS, YAZZ_ACCOUNTS} from '../__mocks__/accounts.mock';
import {useSelector} from 'react-redux';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

describe('PrepaidWidget', () => {
  const DEFAULT_STATE = {
    deposit: {
      prepaids: [],
    },
    yazz: {
      data: [],
    },
    accountDisplayOptions: {
      prepaid: {
        peso: [],
        foreign: [],
        selected: 'all',
      },
    },
  };

  const FILLED_STATE = {
    deposit: {
      prepaids: PREPAID_ACCOUNTS,
    },
    yazz: {
      data: YAZZ_ACCOUNTS,
    },
    accountDisplayOptions: {
      prepaid: {
        peso: [],
        foreign: [],
        selected: 'all',
      },
    },
  };

  const FILLED_STATE_WITH_OPTIONS = {
    deposit: {
      prepaids: PREPAID_ACCOUNTS,
    },
    yazz: {
      data: YAZZ_ACCOUNTS,
    },
    accountDisplayOptions: {
      prepaid: {
        peso: [PREPAID_ACCOUNTS[0].id, YAZZ_ACCOUNTS[0].cardNo],
        foreign: [PREPAID_ACCOUNTS[1].id, YAZZ_ACCOUNTS[0].cardNo],
        selected: 'all',
      },
    },
  };

  const stores = [
    DEFAULT_STATE,
    FILLED_STATE,
    {
      ...FILLED_STATE_WITH_OPTIONS,
      accountDisplayOptions: {
        ...FILLED_STATE_WITH_OPTIONS.accountDisplayOptions,
      },
      prepaid: {
        ...FILLED_STATE_WITH_OPTIONS.accountDisplayOptions.prepaid,
        selected: 'peso',
      },
    },
    {
      ...FILLED_STATE_WITH_OPTIONS,
      accountDisplayOptions: {
        ...FILLED_STATE_WITH_OPTIONS.accountDisplayOptions,
      },
      prepaid: {
        ...FILLED_STATE_WITH_OPTIONS.accountDisplayOptions.prepaid,
        selected: 'foreign',
      },
    },
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  let iteration = 0;
  it.each(stores)('Should work as expected', (store) => {
    useSelector.mockImplementation((callback) => {
      return callback(store);
    });

    const props = {
      navigation: {
        navigate: jest.fn(),
      },
    };

    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <PrepaidWidget {...props} />
        </ThemeProvider>
      </MockProvider>,
    );

    if (iteration > 3) {
      const cardTouchable = all.getByTestId('1widgetItemNavigateButton');
      fireEvent.press(cardTouchable);
    }

    expect(all.toJSON()).toMatchSnapshot();

    iteration++;
  });
});
