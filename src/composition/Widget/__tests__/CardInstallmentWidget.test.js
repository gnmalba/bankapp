import React from 'react';
import {useSelector} from 'react-redux';
import {fireEvent, render} from '@testing-library/react-native';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

import {WIDGET_DEFAULT_MOCK_STORE} from '../__mocks__/widget.mock';
import CardInstallmentWidget from '../CardInstallmentWidget';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

describe('CardInstallmentWidget', () => {
  const stores = [
    WIDGET_DEFAULT_MOCK_STORE,
    {
      ...WIDGET_DEFAULT_MOCK_STORE,
      shortcuts: {
        ...WIDGET_DEFAULT_MOCK_STORE.shortcuts,
        installmentsShortcuts: WIDGET_DEFAULT_MOCK_STORE.shortcuts.installmentsShortcuts.map(
          (shortcut) => {
            return {
              ...shortcut,
              enabled: true,
            };
          },
        ),
      },
    },
    {
      ...WIDGET_DEFAULT_MOCK_STORE,
      shortcuts: {
        ...WIDGET_DEFAULT_MOCK_STORE.shortcuts,
        checkedInstallmentsShortcuts: [],
        installmentsShortcuts: [],
      },
    },
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  it.each(stores)('Should work as expected', async (store) => {
    const {checkedInstallmentsShortcuts} = store.shortcuts;
    useSelector.mockImplementation((callback) => {
      return callback(store);
    });

    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <CardInstallmentWidget />
        </ThemeProvider>
      </MockProvider>,
    );

    if (checkedInstallmentsShortcuts.length) {
      checkedInstallmentsShortcuts.forEach((_, index) => {
        const uniqueIndex = index + 1;
        const shortcutItemButton = all.getByTestId(
          `InstallmentShortcut${uniqueIndex}Touchable`,
        );

        fireEvent.press(shortcutItemButton);
      });
    }

    const okayBtn = all.getByTestId('PropmtModalNotAllowedOkay');
    fireEvent.press(okayBtn);

    expect(all.toJSON()).toMatchSnapshot();
  });
});
