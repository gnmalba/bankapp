import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CreditWidget from '../CreditWidget';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {useSelector} from 'react-redux';
import {WIDGET_DEFAULT_MOCK_STORE} from '../__mocks__/widget.mock';
import {CREDIT_CARDS} from '../__mocks__/accounts.mock';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

describe('CreditWidget', () => {
  const stores = [
    // default state
    {
      credit: {
        creditList: {
          data: [],
        },
      },
      accountDisplayOptions: {
        credit: {
          peso: [],
          foreign: [],
          selected: 'all',
        },
      },
    },
    // with expected data
    {
      credit: WIDGET_DEFAULT_MOCK_STORE.credit,
      accountDisplayOptions: {
        credit: {
          peso: [],
          foreign: [],
          selected: 'all',
        },
      },
    },
    // with null and not allowed flag
    {
      credit: {
        ...WIDGET_DEFAULT_MOCK_STORE.credit,
        creditList: {
          ...WIDGET_DEFAULT_MOCK_STORE.credit.creditList,
          data: [
            {...CREDIT_CARDS[0], flag: null},
            {...CREDIT_CARDS[0], flag: 'NOT_ALLOWED'},
          ],
        },
      },
      accountDisplayOptions: {
        credit: {
          peso: [],
          foreign: [],
          selected: 'all',
        },
      },
    },
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  it.each(stores)('Should work as expected', (store) => {
    useSelector.mockImplementation((callback) => {
      return callback(store);
    });

    const props = {
      navigation: {
        navigate: jest.fn(),
      },
    };

    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <CreditWidget {...props} />
        </ThemeProvider>
      </MockProvider>,
    );

    const creditList = store.credit.creditList.data;
    const hasNonFlag = creditList.some(
      (card) => card.flag === null || card.flag === 'NOT_ALLOWED',
    );

    if (creditList.length > 0) {
      if (hasNonFlag) {
        const creditCard = all.getByTestId('1accountCreditCardTestId');
        fireEvent.press(creditCard);
      }

      creditList.forEach((card, index) => {
        const uniqueIndex = index + 1;
        const id =
          card.flag !== null
            ? `${uniqueIndex}widgetItemNavigateButton`
            : `${uniqueIndex}accountCreditCardTestId`;

        const creditCard = all.getByTestId(id);
        fireEvent.press(creditCard);
      });
    }

    expect(all.toJSON()).toMatchSnapshot();
  });
});
