import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import ShortcutWidget from '../ShortcutWidget';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME, INSTALLMENT_SHORTCUT_CODES} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {WIDGET_DEFAULT_MOCK_STORE} from '../__mocks__/widget.mock';
import {CREDIT_CARDS} from '../__mocks__/accounts.mock';

const filterShortcuts = (shortcuts) => {
  return shortcuts.filter(
    (shortcut) =>
      !INSTALLMENT_SHORTCUT_CODES.includes(shortcut.code) &&
      shortcut.forShortcut === true,
  );
};
const stores = [
  {...WIDGET_DEFAULT_MOCK_STORE},
  {
    shortcuts: {
      data: filterShortcuts(CREDIT_CARDS[0].shortcuts),
      checkedShortcuts: filterShortcuts(CREDIT_CARDS[0].shortcuts).map(
        (shortcut) => shortcut.id,
      ),
      installmentsShortcuts: [],
      checkedInstallmentsShortcuts: [],
    },
  },

  // For testing out, if case with stack and screen
  {
    shortcuts: {
      data: filterShortcuts(CREDIT_CARDS[0].shortcuts).map((shortcut) => {
        return {
          ...shortcut,
          stack: 'SomeStackName',
          screen: 'SomeScreenName',
        };
      }),
      checkedShortcuts: filterShortcuts(CREDIT_CARDS[0].shortcuts).map(
        (shortcut) => shortcut.id,
      ),
      installmentsShortcuts: [],
      checkedInstallmentsShortcuts: [],
    },
  },
];

describe('ShortcutWidget', () => {
  it.each(stores)('Should work as expected', (store) => {
    const {shortcuts} = store;
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ShortcutWidget />
        </ThemeProvider>
      </MockProvider>,
    );

    fireEvent.press(all.getByTestId('tooltipTogglerTouchable'));

    if (shortcuts.checkedShortcuts.length) {
      shortcuts.checkedShortcuts.forEach((shortcutId, index) => {
        fireEvent.press(all.getByTestId(`Shortcut${index + 1}Touchable`));
      });

      // expect all of the checked ones to be rendered
      expect(all.getByTestId('mainShortcutContainer').children.length).toBe(
        shortcuts.checkedShortcuts.length,
      );
    }

    expect(all.toJSON()).toMatchSnapshot();
  });
});
