import React from 'react';

import {ThemeProvider} from 'styled-components/native';
import {render} from '@testing-library/react-native';

import ActivityLogWidget from '../ActivityLogWidget';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

const store = {
  config: {
    widgets: [
      {
        ref: 1,
        label: 'Test Label A',
        loading: false,
      },
      {
        ref: 2,
        label: 'Test Label B',
        loading: false,
      },
    ],
  },
};
describe('Activity Log Widget Test', () => {
  it('Activity Log Widget should render as expected', () => {
    const component = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <ActivityLogWidget />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(component.toJSON()).toMatchSnapshot();
  });
});
