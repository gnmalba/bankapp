import React from 'react';
import {render} from '@testing-library/react-native';
import RecentTransactionWidget from '../RecentTransactionWidget';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

const store = {
  config: {
    widgets: [
      {
        ref: 1,
        label: 'Test Label A',
        loading: false,
      },
      {
        ref: 2,
        label: 'Test Label B',
        loading: false,
      },
    ],
  },
};

describe('RecentTransactionWidget', () => {
  it('Should work as expected', () => {
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <RecentTransactionWidget />
        </ThemeProvider>
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
