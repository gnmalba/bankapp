import React from 'react';
import {render} from '@testing-library/react-native';
import DepositWidget from '../DepositWidget';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {DEPOSIT_ACCOUNTS, PREPAID_ACCOUNTS} from '../__mocks__/accounts.mock';
import {useSelector} from 'react-redux';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

describe('DepositWidget', () => {
  const stores = [
    {
      deposit: {
        deposits: {
          savingsAndDeposits: [],
          timeDeposits: [],
        },
        prepaids: [],
      },
      accountDisplayOptions: {
        deposit: {
          peso: [],
          foreign: [],
          selected: 'all',
        },
      },
    },
    {
      deposit: {
        deposits: DEPOSIT_ACCOUNTS,
        prepaids: PREPAID_ACCOUNTS,
      },
      accountDisplayOptions: {
        deposit: {
          peso: [DEPOSIT_ACCOUNTS.savingsAndDeposits[0].id],
          foreign: [DEPOSIT_ACCOUNTS.savingsAndDeposits[1].id],
          selected: 'all',
        },
      },
    },
    {
      deposit: {
        deposits: DEPOSIT_ACCOUNTS,
        prepaids: PREPAID_ACCOUNTS,
      },
      accountDisplayOptions: {
        deposit: {
          peso: [DEPOSIT_ACCOUNTS.savingsAndDeposits[0].id],
          foreign: [DEPOSIT_ACCOUNTS.savingsAndDeposits[1].id],
          selected: 'peso',
        },
      },
    },
    {
      deposit: {
        deposits: DEPOSIT_ACCOUNTS,
        prepaids: PREPAID_ACCOUNTS,
      },
      accountDisplayOptions: {
        deposit: {
          peso: [DEPOSIT_ACCOUNTS.savingsAndDeposits[0].id],
          foreign: [DEPOSIT_ACCOUNTS.savingsAndDeposits[1].id],
          selected: 'foreign',
        },
      },
    },
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  it.each(stores)('Should work as expected', (store) => {
    useSelector.mockImplementation((callback) => {
      return callback(store);
    });

    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <DepositWidget />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(all.toJSON()).toMatchSnapshot();
  });
});
