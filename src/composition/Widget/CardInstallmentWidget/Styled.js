// @flow

import {verticalScale} from '@utils/scale';
import styled from 'styled-components/native';

export const Container: any = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  align-self: center;
  align-content: space-around;
`;

export const ShortcutContainer: any = styled.View`
  flex-basis: 33.33333333333333%;
  justify-content: center;
  align-items: center;
`;

export const MessageModalContent: any = styled.View`
  height: ${verticalScale(100)}px;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: space-around;
  align-items: center;
  align-content: space-around;
`;
