/**
 * @flow
 */

import React, {useState} from 'react';
import {Pressable} from 'react-native';
import {useNavigation} from '@react-navigation/core';

import ShortcutIconButton from '@composition/ShortcutIconButton';
import Modal from '@composition/Modal';
import Text from '@components/Text';

import {SHORTCUT_ICONS_CODE, SHORTCUT_ICONS_NAME} from '@utils/constants';
import {testProps} from '@utils/componentCustomProps';

import {ShortcutContainer, Container, MessageModalContent} from './Styled';

import {getHighestAvailableBalanceCard, toCardList} from '@utils/installments';
import {useDispatch, useSelector} from 'react-redux';
import {creditSelector} from '@ducks/credit/selectors';
import {
  updateBalanceConversionFrom,
  updateBalanceTransferTo,
  updateCash2GoFrom,
} from '@ducks/installments/actions';

import type {ShortcutType, ShortcutsState} from '@ducks/shortcuts/types';
import {getCheckedInstallmentShortcuts} from '@ducks/shortcuts/selectors';
import {createDisplayOptionItem} from '@composition/DashboardCard/utils';
import {
  addCheckedInstallmentShortcut,
  removeCheckedInstallmentShortcut,
} from '@ducks/shortcuts/actions';
import DashboardCard from '@composition/DashboardCard';
import DashboardEmptyContent from '@composition/DashboardEmptyContent';

type CardInstallmentWidgetPropType = {
  label: string,
};

const CardInstallmentWidget = (
  props: CardInstallmentWidgetPropType,
): React$Node => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [visible, setvisible] = useState(false);

  const creditState = useSelector(creditSelector);
  const highestBalanceCard = getHighestAvailableBalanceCard(
    toCardList(creditState?.data, 'BT') || [],
    'availableCredit',
  );

  const highestBalanceCardC2GO = getHighestAvailableBalanceCard(
    toCardList(creditState?.data, 'C2GO') || [],
    'availableCredit',
  );

  const highestBalanceCardBC = getHighestAvailableBalanceCard(
    toCardList(creditState?.data, 'BC') || [],
    'outstandingBalance',
  );

  const onClickShortcut = (code: string, enabled: boolean) => {
    if (!enabled) {
      setvisible(true);
      return;
    }

    switch (code) {
      case 'BALANCE CONVERSION':
        dispatch(
          updateBalanceConversionFrom(highestBalanceCardBC, false, false),
        );
        navigation.navigate('Installments', {
          screen: 'BalanceConversionScreen',
        });
        break;
      case 'BALANCE TRANSFER':
        dispatch(updateBalanceTransferTo(highestBalanceCard, false));
        navigation.navigate('Installments', {
          screen: 'BalanceTransferScreen',
        });
        break;
      case 'CASH2GO INSTL':
        dispatch(updateCash2GoFrom(highestBalanceCardC2GO, false, false));

        navigation.navigate('Installments', {
          screen: 'Cash2GoScreen',
        });
        break;

      default:
        break;
    }
  };

  const checkedShortcuts: ShortcutType[] = useSelector(
    getCheckedInstallmentShortcuts,
  );
  const {
    checkedInstallmentsShortcuts: checkedShortcutIds,
    installmentsShortcuts,
  }: ShortcutsState = useSelector((state) => state.shortcuts);
  const [tooltipRef, setTooltipRef] = useState(null);
  const displayOptions = installmentsShortcuts.map((shortcut) => {
    const checked = checkedShortcutIds.includes(shortcut.id);
    return createDisplayOptionItem({
      title: SHORTCUT_ICONS_NAME[shortcut.code],
      checked,
      onOptionPress: () => {
        dispatch(
          checked
            ? removeCheckedInstallmentShortcut(shortcut.id)
            : addCheckedInstallmentShortcut(shortcut.id),
        );
      },
    });
  });

  const toggleTooltip = () => {
    if (tooltipRef?.current) {
      tooltipRef.current.toggleTooltip();
    }
  };

  return (
    <DashboardCard
      title={props.label}
      options={displayOptions}
      forwardedTooltipRef={setTooltipRef}
      triggerTooltip={toggleTooltip}>
      <Container>
        {checkedShortcuts.length > 0 ? (
          checkedShortcuts.map((shortcut, i) => {
            const uniqueIndex = i + 1;
            return (
              <ShortcutContainer key={`ci-${shortcut.code}-${i}`}>
                <ShortcutIconButton
                  icon={SHORTCUT_ICONS_CODE[shortcut.code]}
                  name={SHORTCUT_ICONS_NAME[shortcut.code]}
                  onClick={() =>
                    onClickShortcut(shortcut.code, shortcut.enabled)
                  }
                  testID={`InstallmentShortcut${uniqueIndex}`}
                />
              </ShortcutContainer>
            );
          })
        ) : (
          <DashboardEmptyContent onPressText={toggleTooltip} />
        )}
      </Container>

      <Modal visible={visible}>
        <MessageModalContent>
          <Text mode="body1">
            You're not allowed to perform this action yet.
          </Text>
          <Pressable
            onPress={() => {
              setvisible(false);
            }}>
            <Text
              customColor="#09BAD0"
              mode="buttontext"
              {...testProps('PropmtModalNotAllowedOkay')}>
              Okay
            </Text>
          </Pressable>
        </MessageModalContent>
      </Modal>
    </DashboardCard>
  );
};

export default CardInstallmentWidget;
