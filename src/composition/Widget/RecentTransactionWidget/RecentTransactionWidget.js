/**
 * @flow
 */

import React from 'react';

import Card from '@components/Card';

import {transactionOptions} from '@composition/DisplayOptions/utils/constants';

type RecentTransactionWidgetPropTypes = {
  label: string,
};

const RecentTransactionWidget = (
  props: RecentTransactionWidgetPropTypes,
): React$Node => {
  return (
    <Card title={props.label} options={transactionOptions}>
      <></>
    </Card>
  );
};

export default RecentTransactionWidget;
