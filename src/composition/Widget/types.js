// @flow

import type {REF} from '@ducks/config/types';
export type WidgetTypes = {|
  widgetUniqueRef: REF,
  label: string,
  navigation: {
    navigate: (route: string, params?: Object) => void,
  },
|};
