/**
 * @flow
 */

import React from 'react';

import {View} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';

import Card from '@components/Card';
import {Divider} from '@screens/private/Dashboard/Styled';
import AccountCreditCard, {
  AccountCreditCardError,
} from '@composition/AccountCreditCard';
import {accountOptions} from '@composition/DisplayOptions/utils/constants';

import CardWithDisplayOptions from '@composition/CardWithDisplayOptions';

// temprarily add format here
import {maskingFormat} from '@utils/accountNumber';
import {numberToCurrencyFormat} from '@ducks/deposit/utils';
import {testProps} from '@utils/componentCustomProps';
import {getCreditPercentage} from '@utils/credit';

import {getCreditPesoAndForeignAccounts} from '@ducks/accounts-displayoptions/selectors';
import {setSelectedOptionByAccount} from '@ducks/accounts-displayoptions/actions';
import {CREDIT_FULL_PAGE_ERROR} from '@utils/constants';

type CreditWigetPropTypes = {
  label: string,
  navigation: {
    navigate: (route: string, params?: Object) => void,
  },
};

const displayAmount = (currency: string, amount: number): string => {
  return `${currency || ''} ${numberToCurrencyFormat(amount)}`;
};

const handleFilteredCreditClick = (navigation: any, flag: string) => {
  navigation.navigate('Error Screen', {
    isSystemError: false,
    heading: CREDIT_FULL_PAGE_ERROR.header,
    details: CREDIT_FULL_PAGE_ERROR.body,
    headingAlignCenter: true,
  });
};

const CreditWidget = (props: CreditWigetPropTypes): React$Node => {
  const dispatch = useDispatch();

  const credits = useSelector((state) => state.credit.creditList.data);
  const creditPesoAndForeignAccounts = useSelector(
    getCreditPesoAndForeignAccounts,
  );

  if (credits.length === 0) {
    return null;
  }

  const filteredCreditWidgets = credits.filter((account) => {
    const selected = creditPesoAndForeignAccounts.selected;
    if (account.flag === null || account.flag === 'NOT_ALLOWED') {
      return true;
    }

    if (selected === 'peso') {
      return creditPesoAndForeignAccounts.peso.includes(account.cardNo);
    } else if (selected === 'foreign') {
      return creditPesoAndForeignAccounts.foreign.includes(account.cardNo);
    } else {
      return true;
    }
  });

  return filteredCreditWidgets.map((account, index) => {
    const uniqueIndex = index + 1;

    if (account.flag === null || account.flag === 'NOT_ALLOWED') {
      return (
        <View key={`${account.alias}-${index}`}>
          <Card title="Credit Card" options={accountOptions}>
            <AccountCreditCardError
              alias={account.alias}
              testIdPrefix="CreditSavings"
              accountNumber={maskingFormat(account.cardNo, true)}
              flag={account.flag}
              onClick={() =>
                handleFilteredCreditClick(props.navigation, account.flag)
              }
              uniqueIndex={uniqueIndex}
            />
          </Card>
          {index !== filteredCreditWidgets.length - 1 && <Divider />}
        </View>
      );
    }

    return (
      <View key={`${account.cardSerNo}-${index}`}>
        <CardWithDisplayOptions
          title={props.label}
          accountPesoAndForeign={creditPesoAndForeignAccounts}
          onCurrencySelected={(ref) =>
            dispatch(setSelectedOptionByAccount(ref, 'credit'))
          }>
          <AccountCreditCard
            name={account.alias}
            accountNumber={maskingFormat(account.cardNo, true)}
            outstandingBalance={displayAmount(
              account.currency,
              account.outstandingBalance,
            )}
            availableCredit={displayAmount(
              account.currency,
              account.availableCredit,
            )}
            creditLimit={displayAmount(account.currency, account.creditLimit)}
            isPrimary={![0, null].includes(account.primary)}
            calculatedBalance={getCreditPercentage(
              account.creditLimit,
              account.availableCredit,
              account.flag,
            )}
            onClick={() => {
              props.navigation.navigate('CreditScreenOverview', {
                id: account.cardSerNo,
                alias: account.alias,
              });
            }}
            testID={'CreditSavings'}
            uniqueIndex={uniqueIndex}
          />
        </CardWithDisplayOptions>
        {index !== filteredCreditWidgets.length - 1 && (
          <Divider {...testProps(uniqueIndex + 'CreditSavingsDivider')} />
        )}
      </View>
    );
  });
};

export default CreditWidget;
