/**
 * @flow
 */

import React from 'react';
import {View} from 'react-native';

import {useDispatch, useSelector} from 'react-redux';

import {Divider} from '@components/Card/Card';
import AccountCard from '@composition/AccountCard';

import type {YazzCards} from '@ducks/yazz/types';

import {getPrepaidFromDeposit} from '@ducks/deposit/selectors';
import type {Prepaids} from '@ducks/deposit/types';
import {yazzSelector} from '@ducks/yazz/selectors';
import {maskingFormat} from '@utils/accountNumber';
import {numberToCurrencyFormat} from '@ducks/deposit/utils';
import {testProps} from '@utils/componentCustomProps';

import CardWithDisplayOptions from '@composition/CardWithDisplayOptions';

import {getPrepaidPesoAndForeignAccounts} from '@ducks/accounts-displayoptions/selectors';
import {setSelectedOptionByAccount} from '@ducks/accounts-displayoptions/actions';

import {CREDIT_FULL_PAGE_ERROR} from '@utils/constants';

type PrepaidWidgetPropTypes = {
  label: string,
  navigation: {
    navigate: (route: string, params?: Object) => void,
  },
};

let uniqueIndex = 0;
const renderPrepaidAccount = (
  account,
  index: number,
  prepaidLength: number,
  yazzLength: number,
  navigation,
) => {
  const combinedLength = index !== prepaidLength - 1 || yazzLength > 0;
  uniqueIndex = index + 1;
  return (
    <View key={`${account.id}-${index}`}>
      <AccountCard
        name={account.alias}
        accountNumber={maskingFormat(account.accountNo)}
        balanceValue={account.availableBalanceValueWithCurrency}
        balanceLabel="AVAILABLE"
        onClick={() => {
          navigation.navigate('PrepaidScreenOverview', {
            id: account.id,
            alias: account.alias,
          });
        }}
        testID={'Prepaid'}
        uniqueIndex={uniqueIndex}
        isPrimary={account.initialAccount}
        isInactive={!account.active}
      />
      {combinedLength && (
        <Divider {...testProps(uniqueIndex + 'PrepaidWidgetDivider')} />
      )}
    </View>
  );
};

const renderYazzAccount = (
  account,
  index: number,
  yazzLength: number,
  navigation,
) => {
  uniqueIndex = index + 1;
  const isInactive = account.flag === null || account.flag === 'NOT_ALLOWED';
  const isPrimary = account.primary === null ? 0 : account.primary;
  return (
    <View key={`${account.cardNo}-${index}`}>
      <AccountCard
        name={account.alias}
        accountNumber={maskingFormat(account.cardNo, true)}
        balanceLabel="AVAILABLE"
        balanceValue={`${account.currency} ${numberToCurrencyFormat(
          account.outstandingBalance,
        )}`}
        isPrimary={isPrimary !== 0}
        onClick={() => {
          if (account.flag && account.flag === 'NOT_ALLOWED') {
            navigation.navigate('Error Screen', {
              isSystemError: false,
              heading: CREDIT_FULL_PAGE_ERROR.header,
              details: CREDIT_FULL_PAGE_ERROR.body,
              headingAlignCenter: true,
            });
          } else {
            navigation.navigate('YazzScreenOverview', {
              id: account.cardSerNo,
              alias: account.alias,
            });
          }
        }}
        isInactive={isInactive}
      />
      {index !== yazzLength - 1 && (
        <Divider {...testProps(uniqueIndex + 'PrepaidWidgetDivider')} />
      )}
    </View>
  );
};

const PrepaidWidget = (props: PrepaidWidgetPropTypes): React$Node => {
  const dispatch = useDispatch();
  const prepaidPesoAndForeignAccounts = useSelector(
    getPrepaidPesoAndForeignAccounts,
  );
  const prepaids: Prepaids = useSelector(getPrepaidFromDeposit);
  const yazz: YazzCards = useSelector(yazzSelector);

  if (prepaids.length === 0 && yazz.length === 0) {
    return null;
  }

  const filteredPrepaidWidgets = prepaids.filter((account) => {
    const selected = prepaidPesoAndForeignAccounts.selected;
    if (selected === 'peso') {
      return prepaidPesoAndForeignAccounts.peso.includes(account.id);
    } else if (selected === 'foreign') {
      return prepaidPesoAndForeignAccounts.foreign.includes(account.id);
    } else {
      return true;
    }
  });

  const filteredYazzWidgets = yazz.filter((account) => {
    const selected = prepaidPesoAndForeignAccounts.selected;
    if (selected === 'peso') {
      return prepaidPesoAndForeignAccounts.peso.includes(account.cardNo);
    } else if (selected === 'foreign') {
      return prepaidPesoAndForeignAccounts.foreign.includes(account.cardNo);
    } else {
      return true;
    }
  });

  return (
    <CardWithDisplayOptions
      title={props.label}
      accountPesoAndForeign={prepaidPesoAndForeignAccounts}
      onCurrencySelected={(ref) =>
        dispatch(setSelectedOptionByAccount(ref, 'prepaid'))
      }>
      {filteredPrepaidWidgets.map((account, index) =>
        renderPrepaidAccount(
          account,
          index,
          filteredPrepaidWidgets.length,
          filteredYazzWidgets.length,
          props.navigation,
        ),
      )}
      {filteredYazzWidgets.map((account, index) =>
        renderYazzAccount(
          account,
          index,
          filteredYazzWidgets.length,
          props.navigation,
        ),
      )}
    </CardWithDisplayOptions>
  );
};

export default PrepaidWidget;
