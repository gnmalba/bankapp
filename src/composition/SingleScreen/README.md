## SingleScreen

Used for screens that fit without the need for scrolling.
Applies to screens that have static bottom contents like CTAs

# Usage

```js
import SingleScreen from '@composition/SingleScreen';

# Props
```

| Prop               | Type   | Required | Description                 |
| ------------------ | ------ | -------- | --------------------------- |
| testID             | string | false    | The testID prop             |
| accessibilityLabel | string | false    | The accessibilityLabel prop |
| contents           | Node   | false    | The testID prop             |
| bottom             | Node   | false    | The accessibilityLabel prop |

# Example

```js
<SingleScreen
  contents={<Text mode="h1">Header</Text>}
  bottom={<Text mode="h2">Bottom</Text>}
/>
```
