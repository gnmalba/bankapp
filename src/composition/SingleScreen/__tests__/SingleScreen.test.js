import React from 'react';
import {render} from '@testing-library/react-native';
import SingleScreen from '../SingleScreen';

describe('SingleScreen', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(<SingleScreen />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
