/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {Dimensions} from 'react-native';
import {verticalScale, scale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';

export const Container: ComponentType<any> = styled.View`
  padding-left: ${(props) =>
    scale(IS_DEVICE_TABLET && props.padForTablet ? 32 : 0)}px;
  padding-right: ${(props) =>
    scale(IS_DEVICE_TABLET && props.padForTablet ? 32 : 0)}px;
  justify-content: space-between;
  flex: 1;
  /* background-color: yellow; */
  margin-top: -15px;
`;

export const TopContainer: ComponentType<any> = styled.ScrollView.attrs(
  (props) => ({
    scrollEnabled: props.scrollEnabled,
    keyboardShouldPersistTaps: 'handled',
    showsVerticalScrollIndicator: false,
  }),
)``;

export const BottomContainer: ComponentType<any> = styled.View.attrs(
  (props) => ({
    top:
      Dimensions.get('window').height -
      verticalScale(props.spaceFromBottom || 200),
  }),
)`
  position: absolute;
  left: ${(props) => scale(IS_DEVICE_TABLET && props.padForTablet ? 32 : 0)}px;
  width: 100%;
`;
