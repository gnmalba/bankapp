/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import FormLayout from '@composition/FormLayout';
import * as S from './Styled';

const SingleScreen = (props: PropsType): React$Node => {
  const {
    contents,
    bottom,
    spaceFromBottom,
    isFullScreen,
    padForTablet,
    scrollEnabled = false,
    ...rest
  } = props;

  return (
    <FormLayout isFullScreen={isFullScreen}>
      <S.Container padForTablet={padForTablet}>
        <S.TopContainer {...rest} scrollEnabled={scrollEnabled}>
          {contents}
        </S.TopContainer>
        {bottom !== undefined && (
          <S.BottomContainer
            spaceFromBottom={spaceFromBottom}
            padForTablet={padForTablet}>
            {bottom}
          </S.BottomContainer>
        )}
      </S.Container>
    </FormLayout>
  );
};

export default SingleScreen;
