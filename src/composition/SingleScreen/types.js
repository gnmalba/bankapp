/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  contents?: React$Node,
  bottom?: React$Node,
  spaceFromBottom?: number,
  isFullScreen?: boolean,
  padForTablet?: boolean,
  scrollEnabled?: boolean,
};
