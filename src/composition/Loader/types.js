/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  isVisible: boolean,
  showLabel?: string,
  theme?: Object,
};
