import React from 'react';
import {render} from '@testing-library/react-native';
import Loader from '../Loader';
import {testProps} from '@utils/componentCustomProps';

describe('Loader', () => {
  it('Should work as expected', () => {
    const all = render(<Loader {...testProps('loaderTestId')} />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
