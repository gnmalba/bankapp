// @flow

import {StyleSheet} from 'react-native';
import {scale} from '@utils/scale';

const styles: Object = StyleSheet.create({
  icon: {
    width: scale(32),
    height: scale(32),
  },
});

export default styles;
