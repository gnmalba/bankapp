/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {Overlay} from 'react-native-elements';

export const Container: ComponentType<any> = styled(Overlay).attrs(() => ({
  overlayStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    elevation: 0,
    shadowOpacity: 0,
  },
}))`
  background-color: transparent;
  padding: 0;
  border-width: 0;
  align-items: center;
  justify-content: center;
`;

export const StyledImage: ComponentType<any> = styled.Image.attrs((props) => ({
  source: require('@assets/icons/loader.png'),
}))``;

export const SpinnerContainer: ComponentType<any> = styled.View`
  background-color: transparent;
  padding: 0;
  border-width: 0;
  align-items: center;
  justify-content: center;
`;
