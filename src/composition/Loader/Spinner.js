/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';
import {Animated, Easing, View} from 'react-native';
import {withTheme} from 'styled-components';

import * as S from './Styled';
import styles from './styles';
import type {PropsType} from './types';

const Loader = ({isVisible, theme}: PropsType) => {
  const transform = new Animated.Value(0);

  useEffect(() => {
    if (isVisible) {
      Animated.loop(
        Animated.timing(transform, {
          toValue: 1,
          duration: 3000,
          easing: Easing.linear,
          useNativeDriver: true,
        }),
      ).start();
    }
  }, [transform, isVisible]);

  const spin = transform.interpolate({
    inputRange: [0, 1],
    // $FlowFixMe[speculation-ambiguous]
    outputRange: ['360deg', '0deg'],
  });

  return (
    <S.SpinnerContainer isVisible={isVisible}>
      <View>
        <Animated.View
          style={{
            transform: [{rotate: spin}],
          }}>
          <S.StyledImage
            style={{...styles.icon, tintColor: theme?.color.SECONDARY_DEFAULT}}
          />
        </Animated.View>
      </View>
    </S.SpinnerContainer>
  );
};

export default (withTheme(Loader): any);
