import React from 'react';
import {render} from '@testing-library/react-native';
import DashboardCard, {DisplayOptionsExtraActions} from '../DashboardCard';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {useSelector} from 'react-redux';
import {WIDGET_DEFAULT_MOCK_STORE} from '@composition/Widget/__mocks__/widget.mock';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

describe('DashboardCard', () => {
  const mockStore = () => ({
    config: {
      theme: 'dark',
      widgets: [],
      removedWidgets: [],
      checked: [],
      limitShown: false,
    },
  });

  const mockProps = [
    {
      options: [
        {
          title: 'Sample title',
          onPress: () => {},
          checked: false,
        },
      ],
    },
    {options: undefined},
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  it('Should DisplayOptionsExtraActions', () => {
    const props = {
      onMovePress: jest.fn(),
      onRemovePress: jest.fn(),
      selectedTitle: 'title',
    };

    const {toJSON, getByTestId} = render(
      <ThemeProvider theme={DARK_THEME}>
        <DisplayOptionsExtraActions props={props} />
      </ThemeProvider>,
    );

    getByTestId('DisplayOptionsMoveTouchable');
    expect(props.onMovePress).not.toHaveBeenCalled();
    getByTestId('DisplayOptionsRemoveTouchable');
    expect(props.onRemovePress).not.toHaveBeenCalled();

    expect(toJSON()).toMatchSnapshot();
  });

  it.each(mockProps)('Should work as expected', async (props) => {
    useSelector.mockImplementation((callback) => {
      return callback(WIDGET_DEFAULT_MOCK_STORE);
    });

    const {toJSON} = render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <DashboardCard options={props.options} />
        </ThemeProvider>
      </MockProvider>,
    );

    expect(toJSON()).toMatchSnapshot();
  });

  it('should create createDisplayOptionItem properly', () => {});
});
