/**
 * @format
 * @flow
 */

import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity} from 'react-native';

import type {Widget} from '@ducks/config/types';
import type {PropsType, DisplayOptionsExtraActionsPropTypes} from './types';
import {
  Container as StyledCardContainer,
  Divider,
  Header as StyledCardHeader,
} from '@components/Card/Styled';
import Text from '@components/Text';
import Checkbox from '@components/Checkbox';

import {
  StyledDisplayOptionsExtraActionsContainer,
  StyledDisplayOptionsTitle,
  StyledDisplayOptionsToggler,
  StyledDivider,
  StyledDotsVerticalIcon,
  StyledTooltip,
  StyledTooltipContainer,
  StyledRadioButtonContainer,
  StyledRadioButtonLabel,
  StyledMoveIcon,
  StyledRemoveIcon,
} from './Styled';

import {removeWidget} from '@ducks/config/actions';
import {getWidgets} from '@ducks/config/selectors';
import {navigate} from '@navigations/service';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {testProps} from '@utils/componentCustomProps';

export const DisplayOptionsExtraActions = ({
  onMovePress,
  onRemovePress,
  selectedTitle,
}: DisplayOptionsExtraActionsPropTypes): React$Node => {
  return (
    <StyledDisplayOptionsExtraActionsContainer>
      <TouchableOpacity
        onPress={onMovePress}
        {...testProps('DisplayOptionsMoveTouchable')}>
        <StyledRadioButtonContainer>
          <StyledMoveIcon />
          <StyledRadioButtonLabel>Move</StyledRadioButtonLabel>
        </StyledRadioButtonContainer>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          onRemovePress();
        }}
        {...testProps('DisplayOptionsRemoveTouchable')}>
        <StyledRadioButtonContainer>
          <StyledRemoveIcon />
          <StyledRadioButtonLabel>Remove Widget</StyledRadioButtonLabel>
        </StyledRadioButtonContainer>
      </TouchableOpacity>
    </StyledDisplayOptionsExtraActionsContainer>
  );
};

const DashboardCard = ({
  children,
  title,
  options = [],
  forwardedTooltipRef,
}: PropsType): React$Node => {
  const dispatch = useDispatch();
  const widgets = useSelector(getWidgets);
  const toolTipRef = useRef<any>(null);
  const [isTooltipShown, setIsTooltipShown] = useState(false);
  const [contentContainerDimention, setContentContainerDimention] = useState({
    height: 0,
    width: 0,
  });

  const popOverContentContainerOnLayout = (e) => {
    setContentContainerDimention({
      height: e.nativeEvent.layout.height,
      width: e.nativeEvent.layout.width,
    });
  };

  const toggleTooltip = () => {
    if (toolTipRef.current) {
      toolTipRef.current.toggleTooltip();
    }
  };

  const getWidgetRefByLabel = (): Widget =>
    widgets.find((widget) => widget.label === title);

  const onPopoverClose = () => {
    setIsTooltipShown(false);
  };

  useEffect(() => {
    if (forwardedTooltipRef) {
      forwardedTooltipRef(toolTipRef);
    }
  });

  useEffect(() => {
    if (toolTipRef.current) {
      setIsTooltipShown(toolTipRef.current.isVisible);
    }
  }, [toolTipRef]);

  return (
    <StyledCardContainer>
      <StyledCardHeader>
        <Text mode="h3">{title}</Text>
        <StyledDisplayOptionsToggler
          onPress={toggleTooltip}
          active={isTooltipShown}
          {...testProps('tooltipTogglerTouchable')}>
          <StyledTooltip
            ref={toolTipRef}
            withPointer={false}
            overlayColor="#00000054"
            popover={
              <StyledTooltipContainer
                onLayout={popOverContentContainerOnLayout}>
                <StyledDisplayOptionsTitle mode="overline2">
                  DISPLAY OPTIONS
                </StyledDisplayOptionsTitle>

                <KeyboardAwareScrollView>
                  {options.map((option, i) => {
                    return (
                      <Checkbox
                        key={`shortcutCheckbox-${i}`}
                        title={option.title}
                        onPress={() => {
                          option.onOptionPress();
                        }}
                        checked={option.checked}
                        {...testProps(`shortcutCheckbox-${i}`)}
                      />
                    );
                  })}
                </KeyboardAwareScrollView>

                <StyledDivider />
                <DisplayOptionsExtraActions
                  {...testProps('DisplayOptionsExtraActions')}
                  selectedTitle={title}
                  onMovePress={() => {
                    toggleTooltip();
                    navigate('CustomizeWidgetScreen');
                  }}
                  onRemovePress={() => {
                    const widgetRefToBeRemove = getWidgetRefByLabel();

                    if (typeof widgetRefToBeRemove !== 'undefined') {
                      toggleTooltip();
                      dispatch(removeWidget(widgetRefToBeRemove.ref, true));
                    }
                  }}
                />
              </StyledTooltipContainer>
            }
            width={contentContainerDimention.width}
            height={contentContainerDimention.height}
            onClose={onPopoverClose}
          />
          <StyledDotsVerticalIcon />
        </StyledDisplayOptionsToggler>
      </StyledCardHeader>
      <Divider />
      {children}
    </StyledCardContainer>
  );
};

export default DashboardCard;
