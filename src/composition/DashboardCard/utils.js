// @flow

import type {DisplayOptionItem} from './types';

export const createDisplayOptionItem = ({
  title,
  onOptionPress,
  checked,
}: DisplayOptionItem): DisplayOptionItem => {
  return {
    title,
    checked,
    onOptionPress,
  };
};
