## DashboardCard
DashboardCard component.

# Usage
```js
import DashboardCard from '@composition/DashboardCard';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
