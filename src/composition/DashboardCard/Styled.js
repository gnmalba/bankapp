/**
 * @flow
 */

import styled from 'styled-components/native';
import Entypo from 'react-native-vector-icons/Entypo';
import {Tooltip} from 'react-native-elements';

import {Divider as StyledCardDivider} from '@components/Card/Styled';

import Text from '@components/Text';

import {moderateScale, scale, verticalScale} from '@utils/scale';

import Ionicons from 'react-native-vector-icons/Ionicons';

import type {ComponentType} from 'react';

export const StyledDotsVerticalIcon: ComponentType<any> = styled(Entypo).attrs(
  (props) => ({
    color: props.theme.color.TEXT_100_HIGH,
    size: moderateScale(16),
    name: 'dots-three-vertical',
  }),
)``;

export const StyledTooltip: ComponentType<any> = styled(Tooltip).attrs(
  (props) => ({
    backgroundColor: props.theme.displayOptionsTooltip.tooltip,
    containerStyle: {
      transform: [{translateY: verticalScale(6)}, {translateX: scale(-3)}],
    },
  }),
)``;

export const StyledDivider: ComponentType<any> = styled(StyledCardDivider)`
  background-color: ${(props) => props.theme.displayOptionsTooltip.divider};
  margin: 0;
`;

export const StyledTooltipContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.displayOptionsTooltip.tooltip};
  border-radius: ${moderateScale(6)}px;
  padding: ${scale(16)}px;
  width: ${scale(240)}px;
  height: ${scale(250)}px;
`;

export const StyledDisplayOptionsToggler: ComponentType<any> = styled.TouchableOpacity`
  background-color: ${(props) =>
    props.active ? props.theme.displayOptionsTooltip.tooltip : 'transparent'};
  border-radius: 50px;
  padding: 3px;
`;

export const StyledDisplayOptionsTitle: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'overline2',
    customColor: props.theme.color.HIGH_EMPHASIS,
  }),
)`
  margin-bottom: ${verticalScale(10)}px;
`;

export const StyledMoveIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'move-outline',
    color: props.theme.color.TEXT_100_HIGH,
    size: scale(20),
  }),
)`
  transform: rotate(0deg);
`;

export const StyledRemoveIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'trash',
    color: props.disabled
      ? props.theme.color.PRIMARY_DISABLED
      : props.theme.color.TEXT_100_HIGH,
    size: scale(20),
  }),
)``;

export const StyledDisplayOptionsExtraActionsContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(10)}px;
`;

export const StyledInactiveRadioButton: ComponentType<any> = styled(
  Ionicons,
).attrs((props) => ({
  name: 'radio-button-off-outline',
  color: props.disabled
    ? props.theme.color.PRIMARY_DISABLED
    : props.theme.color.TEXT_100_HIGH,
  size: scale(20),
}))``;

export const StyledRadioButtonContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: ${verticalScale(10)}px;
`;

export const StyledRadioButtonLabel: ComponentType<any> = styled(Text).attrs(
  (props) => ({
    mode: 'body1',
    ...(props.disabled && {
      customColor: props.theme.color.PRIMARY_DISABLED,
    }),
  }),
)`
  margin-left: ${scale(7)}px;
  font-size: ${scale(16)}px;
`;
