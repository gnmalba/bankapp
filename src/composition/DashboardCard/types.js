/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  options: DisplayOptionItem[],
  children: React$Node,
  title: string,
  forwardedTooltipRef?: (ref: any) => void,
};

export type DisplayOptionsExtraActionsPropTypes = {
  onMovePress: () => void,
  onRemovePress: () => void,
  selectedTitle: string,
};

export type DisplayOptionItem = {
  title: string,
  onOptionPress: () => void,
  checked: boolean,
};
