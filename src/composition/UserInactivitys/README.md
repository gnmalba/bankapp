## UserInactivitys
UserInactivitys component.

# Usage
```js
import UserInactivitys from '@composition/UserInactivitys';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
