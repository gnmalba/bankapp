/**
 * @format
 * @flow
 */

import React, {useEffect, useState} from 'react';
import UserInactivity from 'react-native-user-inactivity';
import BackgroundTimer from 'react-native-background-timer';
import {useDispatch} from 'react-redux';

import Modal from '@composition/Modal';
import Button from '@components/Button';
import Text from '@components/Text';
import Separator from '@components/Separator';

import {
  AUTHENTICATION_START_LOGOUT,
  AUTHENTICATION_REFRESH_TOKEN,
  AUTHENTICATION_INACTIVITY_LOGOUT,
} from '@ducks/authentication/actionTypes';

import type {PropsType} from './types';
import {
  ModalContainer,
  SecondaryBtnContainer,
} from '@screens/public/LoginScreen/Styled';

const UserInactivitys = (props: PropsType): React$Node => {
  const {children} = props;
  const [basis, setBasis] = useState(
    new Date(Date.now() + 1000 * 180).valueOf(),
  );
  const [refreshHandler, setRefreshHandler] = useState();
  const [newDate, setNewDate] = useState(Date.now());
  const dispatch = useDispatch();
  const centerStyle = {textAlign: 'center'};
  const remaining = Math.floor(
    (new Date(basis).getTime() - new Date(newDate).getTime()) / 1000,
  );

  const refresh = (noExpiry = true) => {
    dispatch({
      type: AUTHENTICATION_REFRESH_TOKEN,
      payload: {
        noExpiryChecking: noExpiry,
      },
    });
  };

  const startLogout = () => {
    dispatch({
      type: AUTHENTICATION_START_LOGOUT,
    });
  };

  const stopAll = () => {
    BackgroundTimer.stopBackgroundTimer();
    setBasis(new Date(Date.now() + 1000 * 180).valueOf());
  };

  useEffect(() => {
    if (remaining <= 0) {
      stopAll();
      clearInterval(refreshHandler);
      dispatch({
        type: AUTHENTICATION_START_LOGOUT,
        payload: {
          onSuccess: AUTHENTICATION_INACTIVITY_LOGOUT,
        },
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [remaining, dispatch]);

  useEffect(() => {
    const intervalHandler = setInterval(() => {
      dispatch({
        type: AUTHENTICATION_REFRESH_TOKEN,
      });
    }, 60000);
    setRefreshHandler(intervalHandler);
    return () => {
      clearInterval(intervalHandler);
    };
  }, [dispatch]);

  return (
    <UserInactivity
      timeForInactivity={60000}
      onAction={(isActive) => {
        if (!isActive) {
          refresh();
          BackgroundTimer.stopBackgroundTimer();
          setBasis(new Date(Date.now() + 1000 * 180).valueOf());
          BackgroundTimer.runBackgroundTimer(() => {
            setNewDate(Date.now());
          }, 1000);
        } else {
          if (remaining > 59) {
            stopAll();
          }
        }
      }}>
      {children}
      <Modal visible={remaining <= 60 && remaining >= 0}>
        <ModalContainer>
          <Text mode="body1" style={centerStyle}>
            Your session will end in
          </Text>
          <Separator space={24} />
          <Text mode="body1" style={centerStyle}>{`${remaining} seconds`}</Text>
          <Separator space={40} />
          <Button
            type="primary"
            state="default"
            size="large"
            title="Continue Session"
            onPress={() => {
              stopAll();
            }}
          />
          <Separator space={15} />
          <SecondaryBtnContainer>
            <Button
              type="secondary"
              state="default"
              size="large"
              title="Logout"
              titleThemeColor="TEXT_300_HIGH"
              onPress={() => {
                stopAll();
                startLogout();
              }}
            />
          </SecondaryBtnContainer>
        </ModalContainer>
      </Modal>
    </UserInactivity>
  );
};

export default UserInactivitys;
