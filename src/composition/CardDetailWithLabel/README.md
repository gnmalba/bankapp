## CardDetailWithLabel
CardDetailWithLabel component.

# Usage
```js
import CardDetailWithLabel from '@composition/CardDetailWithLabel';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
