/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';

export const Container: ComponentType<any> = styled.View`
  display: flex;
  flex: ${(props) => props.flex};
  margin-left: ${(props) => (props.leftOffset ? -props.leftOffset : 0)}px;
`;

export const MainWrapper: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 0px;
`;
