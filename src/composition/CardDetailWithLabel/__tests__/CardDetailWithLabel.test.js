import React from 'react';
import {render} from '@testing-library/react-native';
import CardDetailWithLabel from '../CardDetailWithLabel';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('CardDetailWithLabel', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <CardDetailWithLabel />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
