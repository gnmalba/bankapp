/**
 * @flow
 */

import type {TextModeType} from '@components/Text/types';
export type PropsType = {
  testID?: string,
  name: string,
  cardNumber: string,
  nameMode: TextModeType,
  numberMode: string,
  small?: boolean,
  label: string,
};
