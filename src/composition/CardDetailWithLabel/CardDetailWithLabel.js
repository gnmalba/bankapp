/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {Container, MainWrapper} from './Styled';
import Text from '@components/Text';
import {SemiboldText} from '@utils/commonStyles';
import {testProps} from '@utils/componentCustomProps';
import {InstallmentLabel} from '@composition/InstallmentDetail/Styled';

const CardDetailWithLabel = (props: PropsType): React$Node => {
  const {
    name,
    cardNumber,
    nameMode = 'body1',
    numberMode,
    testID,
    small = false,
    label,
  } = props;

  const getTestId = () => (testID ? testID : '');

  return (
    <MainWrapper>
      <Container flex={3}>
        <InstallmentLabel
          small={small}
          mode="overline2"
          themeColor="TEXT_100_HIGH">
          {label}
        </InstallmentLabel>
      </Container>
      <Container flex={5}>
        <Text mode={nameMode}>{name}</Text>
        <SemiboldText
          {...testProps(getTestId() + 'Number')}
          mode={numberMode}
          themeColor="TEXT_100_HIGH">
          {cardNumber}
        </SemiboldText>
      </Container>
    </MainWrapper>
  );
};

export default CardDetailWithLabel;
