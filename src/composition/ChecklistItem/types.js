/**
 * @flow
 */

export type ChecklistItemType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  approved?: boolean,
  text?: string,
};
