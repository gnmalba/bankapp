## ChecklistItem
ChecklistItem component.

# Usage
```js
import ChecklistItem from '@composition/ChecklistItem';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
