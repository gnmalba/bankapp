// @flow

import React from 'react';

import type {ChecklistItemType} from './types';
import {StyledIcon, StyledText, Container} from './Styled';

const ChecklistItem = (props: ChecklistItemType): React$Element<any> => {
  const {approved, text} = props;

  return (
    <Container>
      <StyledIcon approved={approved} />
      <StyledText approved={approved}>{text}</StyledText>
    </Container>
  );
};

export default ChecklistItem;
