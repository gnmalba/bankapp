import React from 'react';
import {render} from '@testing-library/react-native';
import ChecklistItem from '../ChecklistItem';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('ChecklistItem', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <ChecklistItem />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
