/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {moderateScale} from '@utils/scale';
import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const StyledIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'checkmark-sharp',
    size: moderateScale(18),
    color: props.approved
      ? props.theme?.color?.GREEN_STATUS || 'transparent'
      : props.theme?.color?.DISABLED_STATUS || 'transparent',
  }),
)``;

export const StyledText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body2',
  customColor: props.approved
    ? props.theme?.color?.TEXT_300_HIGH || 'transparent'
    : props.theme?.color?.DISABLED_STATUS || 'transparent',
}))`
  padding-left: ${moderateScale(5)}px;
`;
