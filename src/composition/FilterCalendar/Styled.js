/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {TouchableOpacity, Platform} from 'react-native';

// Components
import Text from '@components/Text';

// Utils
import styled from 'styled-components/native';
import {moderateScale, verticalScale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';

export const CancelText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
  themeColor: 'TEXT_300_HIGH',
}))`
  font-size: ${moderateScale(16)}px;
  font-weight: bold;
`;

export const StyledTouchableOpacity: ComponentType<any> = styled(
  TouchableOpacity,
)`
  margin-top: ${verticalScale(10)}px;
  align-self: center;
`;

export const CalendarContainer: ComponentType<any> = styled.View`
  margin-top: ${IS_DEVICE_TABLET
    ? verticalScale(16) + 'px'
    : verticalScale(27) + 'px'};
  margin-bottom: ${IS_DEVICE_TABLET
    ? verticalScale(27) + 'px'
    : verticalScale(37) + 'px'};
  margin-horizontal: ${IS_DEVICE_TABLET
    ? moderateScale(35) + 'px'
    : moderateScale(15) + 'px'};
`;

export const ButtonContainer: ComponentType<any> = styled.View`
  margin-top: ${Platform.select({
    android: verticalScale(-28) + 'px',
    ios: verticalScale(-20) + 'px',
  })};
`;
