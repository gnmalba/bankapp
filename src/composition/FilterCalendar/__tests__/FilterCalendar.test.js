import React from 'react';
import {render} from '@testing-library/react-native';
import FilterCalendar from '../FilterCalendar';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import moment from 'moment';

describe('EnrollSnackbar', () => {
  const mockStore = () => ({
    config: {
      theme: {},
    },
  });
  const mockRef = {
    current: {
      state: {
        selectedStartDate: moment(),
        selectedEndDate: moment(),
      },
    },
  };
  const calendarData = {
    dateFrom: new Date('1/1/21'),
    dateTo: new Date('2/2/21'),
    isTodayActive: true,
  };
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    render(
      <MockProvider store={mockStore()}>
        <ThemeProvider theme={DARK_THEME}>
          <FilterCalendar
            calendarData={calendarData}
            calendarTipRef={mockRef}
          />
        </ThemeProvider>
      </MockProvider>,
    );
    // expect(all.toJSON()).toMatchSnapshot();
  });
});
