/* eslint-disable sonarjs/cognitive-complexity */
/**
 * @format
 * @flow
 */

// React modules
import React, {useState, useEffect} from 'react';
import {View} from 'react-native';

// Third party libraries
import moment from 'moment';
import {useSelector} from 'react-redux';

// Components
import Calendar from '@components/Calendar';
import Button from '@components/Button';

// Utils
import {
  CancelText,
  StyledTouchableOpacity,
  CalendarContainer,
  ButtonContainer,
} from './Styled';
import {scale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';

type PropsType = {
  calendarData: Object,
  calendarTipRef: any,
  setShowCalendar: any,
  setCalendarData: any,
  calendarButtonActv: boolean,
  setCalendarButtonActv: (options: boolean) => any,
  setCalendarPrevValues: (any) => any,
  calendarPrevValues: Object,
  cardType?: string,
};

const FilterCalendar = (props: PropsType): React$Node => {
  const {theme} = useSelector((state) => state.config);
  const {
    calendarData,
    calendarTipRef,
    setShowCalendar,
    setCalendarData,
    calendarButtonActv,
    setCalendarButtonActv,
    setCalendarPrevValues,
    calendarPrevValues,
    cardType,
  } = props;

  const {dateFrom, dateTo, isTodayActive} = calendarData;
  const today = moment();
  const customDatesStyles = [
    {
      date: today.clone(),
      style: {
        backgroundColor: isTodayActive
          ? 'transparent'
          : theme === 'dark'
          ? '#09BAD0'
          : '#00539F',
        width: scale(38),
        height: scale(38),
      },
    },
  ];

  const [buttonState, setButtonState] = useState('disabled');

  useEffect(() => {
    if ((dateFrom && dateTo) || calendarButtonActv) {
      setButtonState('default');
    } else {
      setButtonState('disabled');
    }
  }, [dateFrom, dateTo, calendarButtonActv]);

  const handleDateChange = (date, type) => {
    const dateSelected = moment(date).format('MM/DD/YYYY');
    const dayToPlusOne = moment(date).add(1, 'days').format('MM/DD/YYYY');
    if (type === 'START_DATE') {
      setCalendarData({
        dateFrom: dateSelected,
        dateTo: null,
        fieldSelected: 'calendarFromField',
        isTodayActive: true,
      });
      setCalendarButtonActv(false);
    } else {
      if (date) {
        setCalendarData({
          ...calendarData,
          dateTo: cardType === 'credit' ? dateSelected : dayToPlusOne,
          fieldSelected: 'calendarToField',
          isTodayActive: true,
        });
      }
    }
  };

  const handleApplyChanges = () => {
    setShowCalendar(false);
    setCalendarData({
      ...calendarData,
      fieldSelected: '',
      isTodayActive: false,
    });
    setCalendarButtonActv(false);
    setCalendarPrevValues({
      calendarPrevFrom: dateFrom,
      calendarPrevTo: dateTo,
    });
  };

  const handleCancelFilter = () => {
    const {calendarPrevFrom, calendarPrevTo} = calendarPrevValues;
    setCalendarData({
      dateFrom: calendarPrevFrom ? calendarPrevFrom : null,
      dateTo: calendarPrevTo ? calendarPrevTo : null,
      isTodayActive: false,
    });
    setShowCalendar(false);
  };

  return (
    <>
      <View>
        <CalendarContainer>
          <Calendar
            calendarProps={{
              height: IS_DEVICE_TABLET ? scale(300) : scale(350),
              width: IS_DEVICE_TABLET ? scale(320) : scale(350),
              allowRangeSelection: true,
              forwardedRef: calendarTipRef,
              customDatesStyles: customDatesStyles,
              onDateChange: handleDateChange,
              minDate: moment().subtract(90, 'days'),
              maxDate: moment(today, 'YYYY/MM/DD'),
              disabledDates: (date) => date.isAfter(today, 'day'),
            }}
          />
        </CalendarContainer>

        <ButtonContainer>
          <Button
            type="primary"
            state={buttonState}
            size="large"
            title="Apply"
            onPress={handleApplyChanges}
          />
          <StyledTouchableOpacity onPress={handleCancelFilter}>
            <CancelText>Cancel</CancelText>
          </StyledTouchableOpacity>
        </ButtonContainer>
      </View>
    </>
  );
};

export default FilterCalendar;
