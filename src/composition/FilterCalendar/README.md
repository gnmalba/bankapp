## FilterCalendar
FilterCalendar component.

# Usage
```js
import FilterCalendar from '@composition/FilterCalendar';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
