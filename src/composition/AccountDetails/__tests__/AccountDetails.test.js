import React from 'react';
import {render} from '@testing-library/react-native';
import AccountDetails from '../AccountDetails';
import {testProps} from '@utils/componentCustomProps';
import {CARD_STATUS} from '@utils/constants';

describe('AccountDetails', () => {
  const data = (active) => ({
    depositAccount: {
      accountNo: '123456',
      alias: 'Test',
      accountType: 'test',
      currencyCode: 'PHP',
    },
    productName: 'test',
    currency: 'PHP',
    currentBalance: {
      value: '1.00',
    },
    outstandingBalance: '1.00',
    availableBalance: {
      value: '100',
    },
    active,
    cardStatus: active ? CARD_STATUS.ACTIVE : '',
  });
  it.each(['deposit', 'prepaid', 'yazz', ''])(
    'Should work as expected',
    (type) => {
      // Edit the args inside render and provide props.
      const all = render(
        <AccountDetails
          {...testProps('accountDetailsTestId')}
          cardType={type}
          data={data(type !== '')}
        />,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
