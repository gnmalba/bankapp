/**
 * @format
 * @flow
 */

import React from 'react';
import * as S from './Styled';
import type {PropsType} from './types';
import {numberFormat, maskingFormat} from '@utils/accountNumber';
import {testProps} from '@utils/componentCustomProps';
import IconTooltip from '@components/IconTooltip';
import {CARD_STATUS} from '@utils/constants';

const getAlias = (cardType, data) => {
  if (cardType === 'deposit' || cardType === 'prepaid') {
    return data?.depositAccount?.alias;
  } else if (cardType === 'yazz') {
    return data?.alias;
  } else {
    return '-';
  }
};

const getAccountNo = (cardType, data) => {
  switch (cardType) {
    case 'deposit':
    case 'prepaid':
      return maskingFormat(data?.depositAccount?.accountNo);
    case 'yazz':
      return maskingFormat(data.cardno, true);
    default:
      return '-';
  }
};

const getAccountType = (cardType, data) => {
  switch (cardType) {
    case 'deposit':
    case 'prepaid':
      return data?.depositAccount?.accountType;
    case 'yazz':
      return 'Prepaid';
    default:
      return '-';
  }
};

const getCurrencyCode = (cardType, data) => {
  switch (cardType) {
    case 'deposit':
    case 'prepaid':
      return data?.depositAccount?.currencyCode;
    case 'yazz':
      return data?.currency;
    default:
      return '-';
  }
};

const getCurrentBalance = (cardType, data) => {
  switch (cardType) {
    case 'deposit':
    case 'prepaid':
      return data?.currentBalance?.value;
    case 'yazz':
      return data?.outstandingBalance;
    default:
      return null;
  }
};

const getAvailableBalance = (cardType, data) => {
  switch (cardType) {
    case 'deposit':
    case 'prepaid':
      return data?.availableBalance?.value;
    case 'yazz':
      return data?.availableBalance;
    default:
      return null;
  }
};

const AccountDetails = (props: PropsType): React$Node => {
  const {cardType, data, theme} = props;

  const cardCheck =
    cardType === 'prepaid' || cardType === 'yazz' ? true : false;
  const isPrepaid = cardCheck ? 'Card/Account number' : 'Account number';
  const alias = getAlias(cardType, data);
  const accountNo = getAccountNo(cardType, data);
  const accountType = getAccountType(cardType, data);
  const currencyCode = getCurrencyCode(cardType, data);
  const currentBalance = getCurrentBalance(cardType, data);
  const availableBalance = getAvailableBalance(cardType, data);

  const currentCurrencyBalance = `${currencyCode} ${numberFormat(
    availableBalance,
  )}`;
  const availableCurrencyBalance = `${currencyCode} ${numberFormat(
    currentBalance,
  )}`;

  const reuseDetails = (placeholder, value, detailsTestID) => (
    <>
      <S.AccountHolder flex={1}>
        <S.AccountLabel
          mode="overline1"
          size={12}
          top={7}
          theme={theme}
          {...testProps(detailsTestID + 'Label')}>
          {placeholder}
        </S.AccountLabel>
      </S.AccountHolder>
      <S.AccountHolder flex={1}>
        <S.AccountValue
          size={18}
          theme={theme}
          {...testProps(detailsTestID + 'Value')}>
          {value}
        </S.AccountValue>
      </S.AccountHolder>
    </>
  );
  return (
    <>
      <S.AccountContainer>
        <S.AccountLabel
          {...testProps('overviewAvailableBalanceLabel')}
          mode="overline1"
          size={10}
          top={16}
          theme={theme}>
          available balance
        </S.AccountLabel>

        {data?.active ||
        data?.cardStatus === CARD_STATUS.ACTIVE ||
        cardType === 'yazz' ? (
          <S.AccountValue
            {...testProps('overviewAvailableBalanceValue')}
            size={24}
            theme={theme}>
            {availableCurrencyBalance}
          </S.AccountValue>
        ) : (
          <S.CannotBeDisplayText
            theme={theme}
            {...testProps('overviewAvailableBalanceValue')}>
            Cannot be displayed
          </S.CannotBeDisplayText>
        )}

        <S.AccountLabel
          {...testProps('overviewCurrentBalanceLabel')}
          mode="overline1"
          size={10}
          top={20}
          theme={theme}>
          current balance
        </S.AccountLabel>

        {data?.active ||
        data?.cardStatus === CARD_STATUS.ACTIVE ||
        cardType === 'yazz' ? (
          <S.AccountValue
            {...testProps('overviewCurrentBalanceValue')}
            size={18}
            theme={theme}>
            {currentCurrencyBalance}
          </S.AccountValue>
        ) : (
          <S.CannotBeDisplayText
            theme={theme}
            {...testProps('overviewCurrentBalanceValue')}>
            Cannot be displayed
          </S.CannotBeDisplayText>
        )}
      </S.AccountContainer>

      {!data?.active ||
        (data?.cardStatus === CARD_STATUS.ACTIVE && (
          <S.WarningContainer {...testProps('overviewAccountIssueTestId')}>
            <IconTooltip source={require('@assets/icons/warning.png')} />
            <S.WarningText {...testProps('overviewAccountIssueMessageId')}>
              There is an issue regarding your account. Please contact us
              through our hotline or go to the nearest Metrobank branch.
            </S.WarningText>
          </S.WarningContainer>
        ))}

      <S.AccountDetails top={28}>
        {reuseDetails('Account Alias', alias, 'overviewSummaryAccountAlias')}
      </S.AccountDetails>
      <S.AccountDetails top={15}>
        {reuseDetails(
          'Currency',
          currencyCode,
          'overviewSummaryAccountCurrency',
        )}
      </S.AccountDetails>
      <S.AccountDetails top={10}>
        {reuseDetails(
          'Account type',
          accountType,
          'overviewSummaryAccountType',
        )}
      </S.AccountDetails>
      <S.AccountDetails top={10}>
        {reuseDetails(isPrepaid, accountNo, 'overviewSummaryAccountType')}
      </S.AccountDetails>
    </>
  );
};

export default AccountDetails;
