/**
 * @flow
 */
import type {Account} from '@ducks/deposit/types';
import type {YazzAccountDetails} from '@ducks/yazz/types';
export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  cardType?: 'deposit' | 'prepaid' | 'yazz',
  data: Account & YazzAccountDetails,
  theme?: Object,
};
