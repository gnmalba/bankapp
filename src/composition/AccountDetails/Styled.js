/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import Text from '@components/Text';
import {scale, verticalScale} from '@utils/scale';

export const AccountContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const AccountDetails: ComponentType<any> = styled.View`
  align-items: flex-start;
  margin-top: ${(props) => verticalScale(props.top)}px;
  flex-direction: row;
`;

export const AccountHolder: ComponentType<any> = styled.View`
  flex: ${(props) => props.flex};
`;

export const AccountLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: props.mode,
  ...props,
}))`
  font-size: ${(props) => scale(props.size)}px;
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  line-height: ${scale(14)}px;
  letter-spacing: ${scale(0.846154)}px;
  text-transform: uppercase;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  margin-top: ${(props) => verticalScale(props.top)}px;
`;

export const AccountValue: ComponentType<any> = styled.Text.attrs((props) => ({
  numberOfLines: 1,
}))`
  font-size: ${(props) => scale(props.size)}px;
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  line-height: ${scale(29)}px;
  letter-spacing: ${scale(0.846154)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const WarningContainer: ComponentType<any> = styled.View`
  flex: 1;
  padding: 8px 16px;
  background: #f2ebc3;
  border: 1px solid #cfad34;
  flex-direction: row;
  border-radius: ${scale(4)}px;
  margin-top: ${verticalScale(28)}px;
`;

export const WarningText: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(12)}px;
  line-height: ${scale(14)}px;
  margin-left: ${scale(20)}px;
  color: #7e6f1b;
  flex: 1;
`;

export const CannotBeDisplayText: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(12)}px;
  line-height: ${scale(14)}px;
  margin-top: ${verticalScale(4)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;
