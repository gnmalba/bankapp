/**
 * @flow
 */

export type AccountCardType = {
  name: string,
  accountNumber: string | number,
  onClick?: () => void,
  isPrimary?: boolean,
  balanceValue?: string,
  balanceLabel?: 'OUTSTANDING' | 'AVAILABLE',
  theme?: Object,
  testID?: string,
  uniqueIndex?: number,
  isInactive?: boolean,
  disabled?: boolean,
  cardType?: string,
};
