/**
 * @flow
 */

// Utils
import {moderateScale} from '@utils/scale';
import {Text} from 'react-native';
import styled from 'styled-components/native';

const Container: any = styled.View`
  display: flex;
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: 8px;
`;

const Header: any = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const AccountDetails: any = styled.View`
  display: flex;
  max-width: 85%;
  flex-direction: column;
`;

const AccountNumber: any = styled(Text)`
  color: ${(props) => props.theme?.card.availableBalance};
  font-style: normal;
  font-family: MyriadPro-Bold;
  font-weight: 700;
  font-size: ${moderateScale(10)}px;
  letter-spacing: ${moderateScale(0.846154)}px;
  margin-top: ${10}px;
`;

const PrimaryContainer: any = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-end;
`;

const PrimaryText: any = styled(Text)`
  color: ${(props) => props.theme?.color.GREEN_STATUS};
  font-style: normal;
  font-weight: 600;
  font-size: ${moderateScale(12)}px;
  font-family: MyriadPro-Semibold;
`;

const BottomContainer: any = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const BalanceContainer: any = styled.View`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

const BalanceText: any = styled.View`
  margin-top: ${5}px;
  text-align: right;
  flex-direction: row;
  align-items: flex-end;
  justify-content: flex-end;
`;

const TextInactive: any = styled.Text`
  opacity: 0.3;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-size: ${moderateScale(14)}px;
`;

export {
  Container,
  Header,
  AccountDetails,
  AccountNumber,
  BalanceContainer,
  BottomContainer,
  PrimaryText,
  PrimaryContainer,
  BalanceText,
  TextInactive,
};
