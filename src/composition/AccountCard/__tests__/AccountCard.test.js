import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import AccountCard from '../AccountCard';
import {testProps} from '@utils/componentCustomProps';

describe('AccountCard', () => {
  it.each([() => {}, undefined])('Should work as expected', (func) => {
    const all = render(
      <AccountCard {...testProps('accountCardTestId')} onClick={func} />,
    );
    const el = all.getByTestId('accountCardTestId');
    fireEvent(el, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
  });

  it.each(['accountCardTestId', undefined])(
    'Should work as expected',
    (test) => {
      const all = render(
        <AccountCard
          {...testProps('accountCardTestId')}
          testID={test}
          balanceLabel={test}
          isPrimary={test === undefined}
        />,
      );

      const el = all.getByTestId('accountCardTestId');
      fireEvent(el, 'onPress');
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
