/* eslint-disable  sonarjs/cognitive-complexity */
/**
 * @flow
 */

// React modules
import React from 'react';

// Third party libraries
import Entypo from 'react-native-vector-icons/Entypo';

// Utils
import * as Styled from './Styled';
import type {AccountCardType} from './types';
import {moderateScale} from '@utils/scale';
import Text from '@components/Text';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {testProps} from '@utils/componentCustomProps';
import styled from 'styled-components';
import IconTooltip from '@components/IconTooltip';

const StyledIcon = styled(Entypo).attrs((props) => ({
  name: 'chevron-right',
  size: moderateScale(25),
  color: props?.theme?.card.availableBalance,
}))``;

const AccountCard = (props: AccountCardType): React$Node => {
  const {
    name,
    accountNumber,
    balanceLabel,
    balanceValue,
    isPrimary = false,
    onClick,
    theme,
    testID = '',
    uniqueIndex,
    isInactive = false,
    disabled = false,
    cardType = '',
  } = props;

  const handleOnPress = () => {
    if (onClick) {
      onClick();
    }
  };
  const testIdPrefix = uniqueIndex ? uniqueIndex + testID : testID;
  const touchableTestID = uniqueIndex
    ? `${testIdPrefix}widgetItemNavigateButton`
    : 'accountCardTestId';
  const renderAvailableBalance = () => {
    return (
      <>
        {isInactive ? (
          <>
            <IconTooltip
              height={14}
              source={require('@assets/icons/warning.png')}
            />
            <Styled.TextInactive>Cannot be displayed</Styled.TextInactive>
          </>
        ) : (
          <Text
            mode="body1"
            Lines={1}
            {...testProps(
              `${testIdPrefix}AvailBalValue` || 'balanceValTestId',
            )}>
            {balanceValue}
          </Text>
        )}
      </>
    );
  };

  const renderAccountNumber = () => (
    <>
      {cardType === 'credit' && isInactive && disabled ? (
        <Styled.TextInactive>Cannot be displayed</Styled.TextInactive>
      ) : (
        <Styled.AccountNumber
          theme={theme}
          {...testProps(
            `${testIdPrefix}AccountNumber` || 'accountNumberTestId',
          )}>
          {accountNumber}
        </Styled.AccountNumber>
      )}
    </>
  );

  return (
    <TouchableOpacity
      {...testProps(touchableTestID)}
      onPress={handleOnPress}
      disabled={!onClick || disabled}
      activeOpacity={onClick ? 0.2 : 1}>
      <Styled.Container theme={theme}>
        <Styled.Header>
          <Styled.AccountDetails>
            <Text
              mode="body1"
              numberOfLines={1}
              {...testProps(
                `${testIdPrefix}AccountAlias` || 'accountAliasTestId',
              )}>
              {name}
            </Text>
            <>{renderAccountNumber()}</>
          </Styled.AccountDetails>
          <StyledIcon {...testProps(`${testIdPrefix}NavigateToAccount`)} />
        </Styled.Header>
        <Styled.BottomContainer>
          <Styled.PrimaryContainer>
            {isPrimary && (
              <Styled.PrimaryText
                theme={theme}
                {...testProps(`${testIdPrefix}Primary` || 'primaryAccTestId')}>
                Primary Account
              </Styled.PrimaryText>
            )}
          </Styled.PrimaryContainer>
          <Styled.BalanceContainer>
            <Styled.AccountNumber
              theme={theme}
              {...testProps(
                `${testIdPrefix}AvailBalLabel` || 'availableBalanceLblTestId',
              )}>
              {balanceLabel && `${balanceLabel} BALANCE`}
            </Styled.AccountNumber>
            <Styled.BalanceText>{renderAvailableBalance()}</Styled.BalanceText>
          </Styled.BalanceContainer>
        </Styled.BottomContainer>
      </Styled.Container>
    </TouchableOpacity>
  );
};

export default AccountCard;
