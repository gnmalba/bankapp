import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import PasswordInput from '../PasswordInput';
import {testProps} from '@utils/componentCustomProps';
import {DARK_THEME, LIGHT_THEME} from '@utils/constants';

describe('PasswordInput', () => {
  it.each([DARK_THEME, LIGHT_THEME])('Should work as expected', (theme) => {
    const all = render(
      <PasswordInput
        {...testProps('passwordTestId')}
        state="active"
        theme={theme}
        value="Password"
      />,
    );
    const pressEl = all.getByTestId('passwordTestIdClick');
    fireEvent(pressEl, 'onPress');
    expect(all.toJSON()).toMatchSnapshot();
    render(<PasswordInput state="error" />);
  });
});
