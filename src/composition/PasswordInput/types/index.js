/**
 * @flow
 */

import type {TextInputProps} from '@utils/commonTypes';

export type PropsType = {
  ...TextInputProps,
};
