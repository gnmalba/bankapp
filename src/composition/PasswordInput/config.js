/**
 * @flow
 */

export const PASSWORD = 'Password';
export const PASSWORD_ICON = 'password';
export const ENTER_PASSWORD = 'Enter password';
export const ACTIVE = 'active';
export const ERROR = 'error';
export const CLICK = 'Click';
