/**
 * @format
 * @flow
 */

import React, {useState} from 'react';
import {Pressable} from 'react-native';
import TextInput from '@composition/TextInput';
import {testProps} from '@utils/componentCustomProps';

import * as C from './config';
import type {TextInputProps} from '@utils/commonTypes';
import {StyledVisibilityImage} from './Styled';

const PasswordInput = (props: TextInputProps): React$Node => {
  const {
    value,
    onChangeText,
    state,
    helperText,
    theme,
    placeholder,
    ...rest
  } = props;
  const [visible, setVisible] = useState(false);

  const status = (
    <Pressable
      {...testProps(`${props.testID || 'passwordTestId'}${C.CLICK}`)}
      onPress={() => setVisible(!visible)}>
      <StyledVisibilityImage
        state={state}
        visible={visible}
        theme={theme}
        value={value}
      />
    </Pressable>
  );

  return (
    <TextInput
      {...rest}
      icon={C.PASSWORD_ICON}
      label={rest.label || C.PASSWORD}
      state={state}
      placeholder={placeholder || C.ENTER_PASSWORD}
      value={value}
      helperText={helperText}
      onChangeText={onChangeText}
      secureTextEntry={!visible}
      customStatus={state !== C.ERROR ? status : undefined}
    />
  );
};

export default PasswordInput;
