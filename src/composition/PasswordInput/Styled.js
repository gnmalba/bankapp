/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {moderateScale} from '@utils/scale';

export const StyledVisibilityImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    const liteVisible = require('@assets/icons/visibility-light-active.png');
    const darkVisible = require('@assets/icons/visibility-light.png');
    const darkActive = require('@assets/icons/visibility-dark-active.png');
    const mode = props.theme?.passwordInput?.mode;
    let source =
      props.state === 'active' && mode === 'dark' && props.value.length > 0
        ? darkActive
        : props.theme?.passwordInput?.src;

    if (props.visible) {
      source = mode === 'dark' ? darkVisible : liteVisible;
    }

    return {
      source: source,
      resizeMode: 'cover',
    };
  },
)`
  width: ${moderateScale(24)}px;
  height: ${moderateScale(22.27)}px;
`;
