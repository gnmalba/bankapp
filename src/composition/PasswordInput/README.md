## PasswordInput
PasswordInput component.

# Usage
```js
import PasswordInput from '@composition/PasswordInput';

# Props
```
Prop                  | Type                 | Required               | Description
----------------------|----------------------|------------------------|--------------------------
testID                | string               | true                   | Test ID for testing and accessibility
accessibilityLabel    | string               | true                   | accessibility label
value                 | string               | false                  | The text value
onChangeText          | Function             | false                  | The change handler
nativeProps           | Object               | false                  | Native props added to TextInput
state                 | BasicTextInputState  | false                  | The currect state of the component
helperText            | string               | false                  | if supplied, the bottom helper text

# Example 
```js
  ...
  import {testProps} from '@utils/componentCustomProps';  
  import PasswordInput from '@composition/PasswordInput';
  ...

  const [password, setPassword] = useState('');

  <PasswordInput
    {...testProps('userNameInputId')}
    state="active"
    onChangeText={(text) => {
      setPassword(text);
    }}
    value={password}
    helperText="This is a helper text"
  />
```

# Covers the following scenarios:
![Password](/readme_images/password.jpg)