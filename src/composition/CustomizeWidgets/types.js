/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  onCustomizeDashboardOpen: () => void,
  theme?: Object,
};
