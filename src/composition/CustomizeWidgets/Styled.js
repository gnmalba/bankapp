/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';

// Utils
import styled from 'styled-components/native';
import {scale, verticalScale, moderateScale} from '@utils/scale';
import {IS_DEVICE_TABLET} from '@utils/constants';
import ImageIcon from '@components/ImageIcon';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: flex-end;
  width: 100%;
  background-color: ${(props) => props.theme.layout.backgroundColor};
  padding: ${moderateScale(15)}px;
`;

export const StyledView1: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const StyledView2: ComponentType<any> = styled.View`
  justify-content: center;
  align-items: center;
`;

export const SettingsIcon: ComponentType<any> = styled(Ionicons).attrs(
  (props) => ({
    name: 'settings-sharp',
    color: props.theme.customizeDashboard.customizeText,
    size: IS_DEVICE_TABLET ? 25 : 20,
  }),
)``;

export const StyledText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'body1',
}))`
  color: ${(props) => props.theme.customizeDashboard.customizeText};
  margin-left: ${scale(6)}px;
`;

export const StyledImage: ComponentType<any> = styled(ImageIcon).attrs(
  (props) => {
    return {
      height: '16px',
      width: '16px',
      source: require('@assets/icons/settings.png'),
    };
  },
)`
  height: ${verticalScale(16)}px;
  width: ${scale(16)}px;
  resize-mode: contain;
`;
