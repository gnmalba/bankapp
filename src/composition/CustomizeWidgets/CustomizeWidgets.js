/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity, View} from 'react-native';

// Utils
import * as S from './Styled';
import type {PropsType} from './types';
import {testProps} from '@utils/componentCustomProps';

const CustomizeWidgets = ({
  testID,
  onCustomizeDashboardOpen,
}: PropsType): React$Node => {
  return (
    <View>
      <S.Container>
        <TouchableOpacity
          {...testProps((testID || 'customizeWidgetTestId') + 'CustomizeBtn')}
          onPress={onCustomizeDashboardOpen}>
          <S.StyledView1>
            <S.StyledView2>
              <S.SettingsIcon
                {...testProps('customizeWidgetsCustomizeButtonIconId')}
              />
            </S.StyledView2>
            <S.StyledText {...testProps('customizeWidgetsCustomizeButtonId')}>
              Customize
            </S.StyledText>
          </S.StyledView1>
        </TouchableOpacity>
      </S.Container>
    </View>
  );
};

export default CustomizeWidgets;
