## CustomizeWidgets
CustomizeWidgets component.

# Usage
```js
import CustomizeWidgets from '@composition/CustomizeWidgets';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
