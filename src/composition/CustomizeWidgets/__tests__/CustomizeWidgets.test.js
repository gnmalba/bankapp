import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CustomizeWidgets from '../CustomizeWidgets';
import {StyledImage} from '../Styled';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

const mockStore = () => ({
  dashboard: {
    isQuickLinks: false,
    isCustomizeDashboard: true,
    isCustomizeWidgets: false,
  },
  config: {
    theme: 'dark',
    widgets: [
      {
        ref: 1,
        label: 'Test Label A',
        loading: false,
      },
      {
        ref: 2,
        label: 'Test Label B',
        loading: false,
      },
    ],
    removedWidgets: [1, 2],
    checked: [],
    limitShown: false,
    customizedWidgets: [1, 2],
  },
});

jest.useFakeTimers();

describe('CustomizeWidgets', () => {
  it.each(['customizeWidgetTestId', ''])(
    'Should work as expected',
    (testID) => {
      // Edit the args inside render and provide props.
      const all = render(
        <MockProvider store={mockStore()}>
          <ThemeProvider theme={DARK_THEME}>
            <CustomizeWidgets testID={testID} />
          </ThemeProvider>
        </MockProvider>,
      );
      all.debug();
      const el = all.getByTestId('customizeWidgetTestIdCustomizeBtn');
      fireEvent(el, 'onClick');
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
  it('Should render Styled Image', () => {
    // Edit the args inside render and provide props.
    const all = render(<StyledImage />);
    expect(all.toJSON()).toMatchSnapshot();
  });
});
