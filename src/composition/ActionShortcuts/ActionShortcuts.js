/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import {
  SHORTCUT_ICONS_CODE,
  SHORTCUT_ICONS_NAME,
  CARD_STATUS,
} from '@utils/constants';
import {ShortcutContainer} from '@composition/Widget/ShortcutWidget/Styled';
import ShortcutIconButton from '@composition/ShortcutIconButton';
import {testProps} from '@utils/componentCustomProps';
import * as NavigationService from '@navigations/service';

const ACTION_SHORTCUTS_ENUM = {
  REPORT_LOST_CARD: 'Report lost card',
  ACTIVATE_CARDS: 'Activate Cards',
  PAY_BILLS: 'Pay Bills',
  SEND_MONEY: 'Send Money',
};

const getShortcuts = (data: any, cardType: string) => {
  switch (cardType) {
    case 'credit':
      return data?.data?.shortcuts;
    case 'deposit':
    case 'prepaid':
      return data?.depositAccount?.shortcuts;
    case 'yazz':
      return data?.shortcuts;
    default:
      return [];
  }
};

const ActionShortcuts = (props: PropsType): React$Node => {
  const {theme, data, cardType, accountData, onShortCutPress} = props;
  const isCredit = cardType === 'credit' ? true : false;
  const cardIsActive =
    accountData?.active || accountData?.cardStatus === CARD_STATUS.ACTIVE;
  const isDisabled = !isCredit && !cardIsActive;

  const shortcuts = getShortcuts(data, cardType);

  const handleOnPress = (item: Object): void => {
    const {name} = item;
    if (name) {
      switch (name) {
        case ACTION_SHORTCUTS_ENUM.REPORT_LOST_CARD:
          NavigationService.navigate('Report Lost Card');
          break;
        case ACTION_SHORTCUTS_ENUM.ACTIVATE_CARDS:
          NavigationService.navigate('ActivateCardMainStack');
          break;
        case ACTION_SHORTCUTS_ENUM.PAY_BILLS:
          NavigationService.navigate('PayBillsCredit');
          break;
        case ACTION_SHORTCUTS_ENUM.SEND_MONEY:
          NavigationService.navigate('SendMoneyStack', {
            screen: 'MainScreen',
            params: {
              accountData,
            },
          });
          break;
        default:
          break;
      }
    }
  };

  if (isDisabled) {
    return null;
  }

  return (
    <S.ActionContainer>
      <S.ActionText {...testProps('actionsLabelTestId')} theme={theme}>
        Actions
      </S.ActionText>
      <S.ActionDivider {...testProps('actionsDividerId')} />
      <S.ActionScrollView>
        <S.CircleContainer
          active={
            isCredit
              ? true
              : accountData?.active ||
                accountData?.cardStatus === CARD_STATUS.ACTIVE
          }>
          {shortcuts &&
            shortcuts
              .filter((x) => x.enabled)
              .map((item, index) => (
                <S.ItemContainer key={index}>
                  <ShortcutContainer
                    {...testProps(
                      `${item.code}_actionShortcutIconButtonWrapper`,
                    )}>
                    <ShortcutIconButton
                      {...testProps(`${item.code}_actionShortcutIconButton`)}
                      theme={theme}
                      icon={SHORTCUT_ICONS_CODE[item.code]}
                      name={SHORTCUT_ICONS_NAME[item.code]}
                      onClick={() =>
                        onShortCutPress(item) ?? handleOnPress(item)
                      }
                      displayIn="accounts"
                    />
                  </ShortcutContainer>
                </S.ItemContainer>
              ))}
        </S.CircleContainer>
      </S.ActionScrollView>
    </S.ActionContainer>
  );
};

export default ActionShortcuts;
