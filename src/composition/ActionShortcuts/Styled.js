/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {moderateScale, verticalScale} from '@utils/scale';
import {ScrollView, Image} from 'react-native';
import Text from '@components/Text';

export const ActionContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(40)}px;
`;

export const ActionDivider: ComponentType<any> = styled.View`
  border: 1px solid #445870;
  margin-top: ${verticalScale(8)}px;
`;

export const ActionText: ComponentType<any> = styled(Text).attrs((props) => ({
  mode: 'h3',
}))`
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  font-family: 'MyriadPro-Semibold';
`;

export const CircleContainer: ComponentType<any> = styled.View`
  justify-content: flex-start;
  flex-direction: row;
  margin-top: ${verticalScale(16)}px;
  opacity: ${(props) => (props.active ? 1 : 0.5)};
`;

export const ItemContainer: ComponentType<any> = styled.View`
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-right: ${moderateScale(10)}px;
`;

export const ActionScrollView: ComponentType<any> = styled(ScrollView).attrs(
  (props) => ({
    horizontal: true,
    showsHorizontalScrollIndicator: false,
  }),
)``;

export const IconImage: ComponentType<any> = styled(Image).attrs((props) => ({
  source: props.source,
  resizeMode: 'contain',
}))`
  height: ${(props) => verticalScale(props.height)}px;
  width: ${(props) => moderateScale(props.width)}px;
`;
