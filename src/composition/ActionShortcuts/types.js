/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  data: Object,
  accountData?: Object,
  cardType: string,
  onShortCutPress: (item: Object) => void,
};
