import React from 'react';
import {render} from '@testing-library/react-native';
import ActionShortcuts from '../ActionShortcuts';
import {
  CREDIT_SHORTCUTS_DEFAULT,
  DEPOSIT_PREPAID_SHORTCUTS_DEFAULT,
  DARK_THEME,
} from '@utils/constants';
import {ThemeProvider} from 'styled-components/native';
import {IconImage} from '../Styled';
import {testProps} from '@utils/componentCustomProps';

describe('ActionShortcuts', () => {
  it.each(['credit', 'deposit', 'prepaid', 'yazz', ''])(
    'Should work as expected',
    (type) => {
      // Edit the args inside render and provide props.
      const all = render(
        <ThemeProvider theme={DARK_THEME}>
          <ActionShortcuts
            {...testProps(type === '' ? undefined : type)}
            cardType={type}
            data={CREDIT_SHORTCUTS_DEFAULT}
          />
          <ActionShortcuts
            {...testProps(type === '' ? undefined : type)}
            cardType={type}
            data={DEPOSIT_PREPAID_SHORTCUTS_DEFAULT}
            accountData={{active: true}}
          />
          <ActionShortcuts
            {...testProps(type === '' ? undefined : type)}
            cardType={type}
            data={DEPOSIT_PREPAID_SHORTCUTS_DEFAULT}
            accountData={{active: false}}
          />
          <ActionShortcuts
            {...testProps(type === '' ? undefined : type)}
            cardType={type}
            data={DEPOSIT_PREPAID_SHORTCUTS_DEFAULT}
            accountData={{cardStatus: 'ACTV'}}
          />
        </ThemeProvider>,
      );
      render(<IconImage />);
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
