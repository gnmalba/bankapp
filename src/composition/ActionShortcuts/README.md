## ActionShortcuts
ActionShortcuts component.

# Usage
```js
import ActionShortcuts from '@composition/ActionShortcuts';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
