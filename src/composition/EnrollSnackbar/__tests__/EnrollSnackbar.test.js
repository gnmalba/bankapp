import React from 'react';
import {act, render} from '@testing-library/react-native';
import EnrollSnackbar from '../EnrollSnackbar';
import MockProvider from '@utils/testFramework';

jest.useFakeTimers();
describe('EnrollSnackbar', () => {
  const mockStore = () => ({
    enrollment: {
      showSnackbar: true,
    },
  });
  it('Should work as expected', async () => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={mockStore}>
        <EnrollSnackbar />
      </MockProvider>,
    );

    act(() => {
      expect(all.toJSON()).toMatchSnapshot();
      jest.runAllTimers();
    });
  });
});
