## EnrollSnackbar
EnrollSnackbar component.

# Usage
```js
import EnrollSnackbar from '@composition/EnrollSnackbar';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
