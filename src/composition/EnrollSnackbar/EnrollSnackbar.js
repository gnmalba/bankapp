/* eslint-disable react-hooks/exhaustive-deps */
// @flow

import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import Snackbar from '@components/Snackbar';
import {showEnrollmentSnackbar} from '@ducks/enrollment/actions';
import {testProps} from '@utils/componentCustomProps';
import type {PropsType} from './types';

export const EnrollSnackbar = (props: PropsType): React$Node => {
  const enrollment = useSelector((state) => state.enrollment);
  const {showSnackbar, snackbarMessage} = enrollment;
  const {testId} = props;
  const dispatch = useDispatch();

  useEffect(() => {
    const timeHandler = setTimeout(() => {
      dispatch(
        showEnrollmentSnackbar({
          showSnackbar: false,
          snackbarMessage: null,
        }),
      );
    }, 5000);
    return () => {
      clearTimeout(timeHandler);
    };
  }, [showSnackbar]);

  return (
    <Snackbar
      visible={showSnackbar}
      message={snackbarMessage}
      {...testProps(testId)}
    />
  );
};

export default EnrollSnackbar;
