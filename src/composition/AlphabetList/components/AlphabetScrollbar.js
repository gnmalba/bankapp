/**
 * @format
 * @flow
 */
import React from 'react';
import Text from '@components/Text';
import {AlphabetScrollBarContainer, AlphabetContainer} from '../Styled';
import {alphabet} from '../utils';
import {TouchableOpacity} from 'react-native';

const AlphabetScrollBar = ({
  onClick,
}: {
  onClick(index: number): void,
}): React$Node => (
  <AlphabetScrollBarContainer>
    <AlphabetContainer>
      {alphabet.map((char, index) => {
        return (
          <TouchableOpacity onPress={() => onClick(index)} key={index}>
            <Text mode="overline1" customColor={'#A2BDDB'}>
              {char}
            </Text>
          </TouchableOpacity>
        );
      })}
    </AlphabetContainer>
  </AlphabetScrollBarContainer>
);

export default AlphabetScrollBar;
