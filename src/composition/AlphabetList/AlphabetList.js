/**
 * @format
 * @flow
 */
import React, {useRef} from 'react';
import Text from '@components/Text';
import {
  ColumnContainer,
  RowContainer,
  StyledIcon,
  Divider,
  HeaderContainer,
  StyledList,
  HeaderTextWrapper,
  FullHeightContainer,
} from './Styled';

import type {SectionData, Section} from './types';

import sectionListGetItemLayout from 'react-native-section-list-get-item-layout';
import AlphabetScrollBar from './components/AlphabetScrollbar';
import {TouchableOpacity} from 'react-native';

const ListItem = ({
  name,
  isLast,
}: {
  name: string,
  isLast: boolean,
}): React$Node => (
  <ColumnContainer>
    <RowContainer last={isLast}>
      <Text mode="listitem">{name}</Text>
      <StyledIcon />
    </RowContainer>
    <Divider last={isLast} />
  </ColumnContainer>
);

const renderSectionHeader = ({
  section,
}: {
  section: {title: string},
}): React$Node => {
  return (
    <HeaderContainer>
      <HeaderTextWrapper>
        <Text mode="listitem" customColor={'#FFFFFF'}>
          {section.title}
        </Text>
      </HeaderTextWrapper>
    </HeaderContainer>
  );
};

const renderItem = ({
  item,
  onPress,
}: {
  item: Section,
  onPress: any,
}): React$Node => (
  <TouchableOpacity onPress={onPress}>
    <ListItem name={item.name} isLast={item.isLastItem} />
  </TouchableOpacity>
);

const handleScrollBarPress = (index: number, ref: any): void =>
  ref.current?.scrollToLocation({
    animated: true,
    itemIndex: 0,
    sectionIndex: index,
    viewOffset: 0,
  });

const getKey = (item: Object, index: number): string => item.id + index;
const handleItemPress = (id: string, func: any) => () => func(id);

const getItemLayout = sectionListGetItemLayout({
  // The height of the row with rowData at the given sectionIndex and rowIndex
  getItemHeight: (rowData, sectionIndex, rowIndex) => 75,
});

const AlphabetList = ({
  data,
  onPress,
}: {
  data: Array<SectionData>,
  onPress(id: string): any,
}): React$Node => {
  const listRef = useRef(null);

  /*This is neccesary, immediately return Section list
   to a return statements produces a bug that disables its scrolling for IoS
   */
  const alphabetlist: React$Node = (
    <StyledList
      ref={listRef}
      initialNumToRender={50}
      maxToRenderPerBatch={50}
      windowSize={41}
      extraData={data}
      sections={data}
      removeClippedSubviews={true}
      stickySectionHeadersEnabled={false}
      bounces={false}
      keyExtractor={getKey}
      updateCellsBatchingPeriod={100}
      renderItem={({item}) =>
        renderItem({item: item, onPress: handleItemPress(item.id, onPress)})
      }
      getItemLayout={getItemLayout}
      renderSectionHeader={renderSectionHeader}
    />
  );

  return (
    <FullHeightContainer>
      <AlphabetScrollBar
        onClick={(index: number) => handleScrollBarPress(index, listRef)}
      />
      {alphabetlist}
    </FullHeightContainer>
  );
};

export default AlphabetList;
