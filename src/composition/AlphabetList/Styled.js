/**
 * @format
 * @flow
 */
import {scale, moderateScale} from '@utils/scale';
import Feather from 'react-native-vector-icons/Feather';
import styled from 'styled-components/native';
import type {ComponentType} from 'react';

const StyledIcon: ComponentType<any> = styled(Feather).attrs((props) => ({
  name: 'chevron-right',
  color: props.theme.carousel.indicatorColor,
  size: 30,
}))``;

const StyledList: ComponentType<any> = styled.SectionList`
  flex: 1;
  height: 100%;
  margin-bottom: 20px;
`;

const RowContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: ${moderateScale(20)}px;
  margin-bottom: ${(props) => (props.last ? 0 : moderateScale(20))}px;
  margin-left: 32px;
  margin-right: 23px;
`;
const HeaderTextWrapper: ComponentType<any> = styled.View`
  margin-left: 24px;
  margin-top: 3px;
  margin-bottom: 2px;
`;

const HeaderContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: ${moderateScale(22)}px;
  background-color: ${(props) => props.theme.sectionHeader};
`;

const ColumnContainer: ComponentType<any> = styled.View`
  flex-direction: column;
`;

const AlphabetScrollBarContainer: ComponentType<any> = styled.View`
  position: absolute;
  right: 0;
  width: ${moderateScale(14)}px;
  height: 100%;
  background-color: transparent;
  padding: 10px 1px;
  z-index: 99;
  margin-right: 10px;
`;

const Divider: ComponentType<any> = styled.View`
  background-color: ${(props) =>
    props.last ? 'transparent' : props.theme.card.dividerBgColor};
  height: ${scale(1)}px;
  margin: 0 32px;
`;

const AlphabetContainer: ComponentType<any> = styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: space-evenly;
`;

const FullHeightContainer: ComponentType<any> = styled.View`
  flex: 1;
  height: 100%;
`;

export {
  Divider,
  StyledIcon,
  RowContainer,
  HeaderContainer,
  ColumnContainer,
  StyledList,
  HeaderTextWrapper,
  AlphabetScrollBarContainer,
  AlphabetContainer,
  FullHeightContainer,
};
