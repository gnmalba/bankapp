/**
 * @format
 * @flow
 */

export type SectionData = {
  title: string,
  data: Array<Section>,
};

export type Section = {
  id: string,
  name: string,
  isLastItem: boolean,
};
