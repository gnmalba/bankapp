import React from 'react';
import {render} from '@testing-library/react-native';
import SectionList from '../SectionList';
import {renderComponentWithTheme} from '@utils/testFramework';

const sectionData = [
  {
    title: 'Test Title Data',
    data: [{id: '1', name: 'test', isLastItem: false}],
  },
];

describe('SectionList', () => {
  it('Should work as expected', () => {
    const handlePressMock = jest.fn();
    // Edit the args inside render and provide props.
    const all = render(
      renderComponentWithTheme(
        <SectionList data={sectionData} onPress={() => handlePressMock} />,
      ),
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
