/**
 * @format
 * @flow
 */
import React from 'react';
import Text from '@components/Text';
import {
  ColumnContainer,
  RowContainer,
  StyledIcon,
  Divider,
  HeaderContainer,
  StyledList,
} from './Styled';

import type {SectionData, Section} from './types';
import {StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native';

const ListItem = ({
  name,
  isLast,
}: {
  name: string,
  isLast: boolean,
}): React$Node => (
  <ColumnContainer>
    <RowContainer last={isLast}>
      <Text mode="listitem">{name}</Text>
      <StyledIcon />
    </RowContainer>
    <Divider last={isLast} />
  </ColumnContainer>
);

const renderSectionHeader = ({
  section,
}: {
  section: {title: string},
}): React$Node => {
  return (
    <HeaderContainer>
      <Text mode="overline1" themeColor={'TEXT_100_HIGH'}>
        {section.title}
      </Text>
    </HeaderContainer>
  );
};

const renderItem = ({
  item,
  onPress,
}: {
  item: Section,
  onPress: any,
}): React$Node => (
  <TouchableOpacity onPress={onPress}>
    <ListItem name={item.name} isLast={item.isLastItem} />
  </TouchableOpacity>
);

const handleItemPress = (id: string, func: any) => () => func(id);

function SectionList({
  data,
  onPress,
}: {
  data: Array<SectionData>,
  onPress(id: string): any,
}): React$Node {
  /*This is neccesary, immediately return Section list
   to a return statements produces a bug that disables its scrolling for IoS
   */
  const sectionList: React$Node = (
    /* eslint-disable sonarjs/prefer-immediate-return */

    <StyledList
      extraData={data}
      initialNumToRender={50}
      maxToRenderPerBatch={50}
      windowSize={41}
      sections={data}
      removeClippedSubviews={true}
      stickySectionHeadersEnabled={false}
      bounces={false}
      keyExtractor={(item, index) => item.id + index}
      updateCellsBatchingPeriod={100}
      renderItem={({item}) =>
        renderItem({item: item, onPress: handleItemPress(item.id, onPress)})
      }
      renderSectionHeader={renderSectionHeader}
      contentContainerStyle={styles.listContainer}
    />
  );

  return sectionList;
}

const styles: Object = StyleSheet.create({
  listContainer: {
    flexGrow: 1,
  },
});

export default SectionList;
