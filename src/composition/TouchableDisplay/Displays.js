/**
 * @format
 * @flow
 */

import React from 'react';
import Text from '@components/Text';
import type {} from './types';
import {Display1Container} from './Styled';
import Separator from '@components/Separator';
import {SemiboldText} from '@utils/commonStyles';
import {testProps} from '@utils/componentCustomProps';

export const Display1 = (
  title: string,
  subtitle: string,
  testIds: {title: string, subtitle: string},
): any => {
  return (
    <Display1Container>
      <Text mode="body1" numberOfLines={1} {...testProps(testIds?.title)}>
        {title}
      </Text>
      <Separator space={8} />
      <SemiboldText
        mode="caption"
        themeColor="TEXT_100_HIGH"
        {...testProps(testIds?.subtitle)}>
        {subtitle}
      </SemiboldText>
    </Display1Container>
  );
};

export const Display2 = (
  title: string,
  subtitle1: string,
  subtitle2: string,
): any => {
  return (
    <Display1Container>
      <Text mode="body1" numberOfLines={1}>
        {title}
      </Text>
      <SemiboldText mode="caption" themeColor="TEXT_100_HIGH">
        {subtitle1}
      </SemiboldText>
      <SemiboldText mode="caption" themeColor="TEXT_100_HIGH">
        {subtitle2}
      </SemiboldText>
    </Display1Container>
  );
};
