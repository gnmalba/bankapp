## TouchableDisplay
TouchableDisplay component.

# Usage
```js
import TouchableDisplay from '@composition/TouchableDisplay';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
