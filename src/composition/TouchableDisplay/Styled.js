/**
 * @flow
 */

import {moderateScale, scale, verticalScale} from '@utils/scale';
import type {ComponentType} from 'react';
import styled from 'styled-components/native';

import Feather from 'react-native-vector-icons/Feather';

export const Wrapper: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(25)}px;
`;

export const Container: ComponentType<any> = styled.View`
  background-color: ${(props) =>
    props.disabled
      ? props.theme.textInput.backgroundColor.disabled
      : props.theme.color.BACKGROUND_300};
  border-radius: 4px;
  padding: ${verticalScale(17)}px;
  display: flex;
  flex-direction: row;
  height: ${verticalScale(78)}px;
`;

export const LabelHolder: ComponentType<any> = styled.View`
  margin-bottom: ${verticalScale(12)}px;
`;

export const ContainerPlaceholder: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-weight: 400;
  font-size: ${moderateScale(20)}px;
  line-height: ${moderateScale(24)}px;
  color: ${(props) => props.theme.textInput.placeholderTextColor};
`;

export const RightIcon: ComponentType<any> = styled(Feather).attrs((props) => ({
  name: 'chevron-right',
  size: scale(30),
  color: props.theme.color.TEXT_300_LOW,
}))``;

export const IconHolder: ComponentType<any> = styled.View`
  margin-left: auto;
  align-self: center;
`;

export const Display1Container: ComponentType<any> = styled.View`
  display: flex;
  max-width: 85%;
`;
