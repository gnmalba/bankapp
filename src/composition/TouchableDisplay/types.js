/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  label: string,
  placeholder: string,
  onPress: () => void,
  content?: any,
  disabled?: boolean,
  overrideTestId?: {
    chevron: string,
  },
  customStyles?: {
    rootView?: Object,
  },
};
