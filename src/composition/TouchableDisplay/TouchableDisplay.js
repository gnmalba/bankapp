/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import {
  Container,
  ContainerPlaceholder,
  IconHolder,
  LabelHolder,
  RightIcon,
  Wrapper,
} from './Styled';
import {TouchableOpacity} from 'react-native';
import Text from '@components/Text';
import {generateId, testProps} from '@utils/componentCustomProps';

const TouchableDisplay = (props: PropsType): React$Node => {
  const {
    label,
    placeholder,
    onPress,
    content,
    disabled,
    overrideTestId,
    customStyles = {},
  } = props;
  const chevronTestProps = overrideTestId
    ? overrideTestId.chevron
    : testProps(`chevron ${label}`);
  return (
    <Wrapper {...generateId(`TO ${label}`)} style={customStyles?.rootView}>
      <LabelHolder>
        <Text {...testProps(`Lbl ${label}`)}>{label}</Text>
      </LabelHolder>
      <TouchableOpacity onPress={onPress} disabled={disabled}>
        <Container disabled={disabled}>
          {content ? (
            content
          ) : (
            <ContainerPlaceholder {...testProps(`Placeholder ${label}`)}>
              {placeholder}
            </ContainerPlaceholder>
          )}

          <IconHolder>
            <RightIcon {...chevronTestProps} />
          </IconHolder>
        </Container>
      </TouchableOpacity>
    </Wrapper>
  );
};

export default TouchableDisplay;
