/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale} from '@utils/scale';

export const FieldContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(18)}px;
`;
