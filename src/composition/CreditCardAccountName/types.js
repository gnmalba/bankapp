/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  accountName?: string | null,
  label?: string | null,
  placeholderText?: string | null,
  handleInputChange?: () => (string) => void,
  handleResetField?: () => void,
  handleSetFieldTouched?: () => void,
  error?: boolean,
  value?: string,
  isTouched?: boolean,
  isValid?: boolean,
};
