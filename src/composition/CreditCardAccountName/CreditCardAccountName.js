// React modules
import React from 'react';

// Compositions
import FormikInput from '@composition/FormikInput';

// Components
import {FieldContainer} from './Styled';

// Types
import type {PropsType} from './types';

const CreditCardAccountName = (props: PropsType): React$Node => {
  const {
    accountNameInput,
    label,
    placeholderText,
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    error,
    isTouched,
  } = props;

  return (
    <FieldContainer>
      <FormikInput
        testID="EnrollmentAliasInput"
        name="accountNameInput"
        label={label}
        placeholderText={placeholderText}
        value={accountNameInput}
        error={error}
        isTouched={isTouched}
        handleInputChange={handleInputChange}
        handleResetField={handleResetField}
        handleSetFieldTouched={handleSetFieldTouched}
        maxLength={80}
        textWidth="98%"
      />
    </FieldContainer>
  );
};

export default CreditCardAccountName;
