/**
 * @format
 * @flow
 */

import React, {useState, useEffect} from 'react';
import {Pressable, View} from 'react-native';

// Components
import Text from '@components/Text';
import TextInput from '@composition/TextInput';
import Separator from '@components/Separator';

// Utils
import type {TextInputProps} from '@utils/commonTypes';
import {testProps} from '@utils/componentCustomProps';
import {verticalScale} from '@utils/scale';
import * as S from './Styled';
const PasswordInputValidation = (props: TextInputProps): React$Node => {
  const {
    value,
    onChangeText,
    state,
    helperText,
    theme,
    showTextValidation,
    onBlur,
    setError,
    ...rest
  } = props;
  const [visible, setVisible] = useState(false);
  // for integration passwordpolicy api
  // const manageSettings = useSelector((states) => states.manageSettings);
  const [passwordTextValidation, setPasswordTextValidation] = useState([
    {
      name: 'Must have no space',
      isRegexSpacing: true,
      regex: /\s/,
      isValid: false,
    },
    {
      name: 'Contains at least eight characters',
      regex: /.{8,}/,
      isValid: false,
    },
    {
      name: 'Contains at least one lowercase character',
      regex: /(?=.*?[a-z])/,
      isValid: false,
    },
    {
      name: 'Contains at least one uppercase character',
      regex: /(?=.*?[A-Z])/,
      isValid: false,
    },
    {
      name: 'Contains at least one digit character',
      regex: /(?=.*?[0-9])/,
      isValid: false,
    },
    {
      name: 'Contains at least one special character',
      regex: /(?=.*?[#?!@$%^&*-])/,
      isValid: false,
    },
  ]);
  const isNewPasswordValid = passwordTextValidation.every(
    (item) => item.isValid === true,
  );

  useEffect(() => {
    showTextValidation && showTextValidation(isNewPasswordValid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value]);

  const _renderTextValidation = (items) => {
    const {item, index} = items;
    return (
      <View key={index}>
        <S.TextValidationContainer>
          <S.CheckIconImage
            isValid={item.isValid}
            {...testProps('accountSettingsCheckImageTestId')}
          />
          <Text
            mode="body2"
            themeColor={item.isValid ? 'TEXT_100_HIGH' : 'TEXT_300_LOW'}
            {...testProps('accountSettingsTextBody2testId')}>
            {item?.name}
          </Text>
        </S.TextValidationContainer>
      </View>
    );
  };

  const status = (
    <Pressable
      {...testProps(`${props.testID || 'passwordTestIdClick'}`)}
      onPress={() => setVisible(!visible)}>
      <S.StyledVisibilityImage
        state={state}
        visible={visible}
        theme={theme}
        value={value}
        isFromNewPassword={rest?.isFromNewPassword}
      />
    </Pressable>
  );

  const regexInputValidation = (text) => {
    passwordTextValidation.map((values, index) => {
      const {regex, isRegexSpacing = false} = values;
      // $FlowFixMe[incompatible-call]
      passwordTextValidation[index].isValid = regex.test(text)
        ? !isRegexSpacing
        : isRegexSpacing;
    });
    setPasswordTextValidation(passwordTextValidation);
  };

  const validatePassword = (password) => {
    regexInputValidation(password);
  };

  const handleBlur = () => {
    if (onBlur) {
      onBlur();
    }
  };

  return (
    <>
      <TextInput
        {...rest}
        icon={'password'}
        label={rest.label || 'Password'}
        state={state}
        placeholder={rest.placeholder || 'Enter password'}
        value={value}
        helperText={helperText}
        onChangeText={onChangeText}
        onChange={(event) => {
          const {text} = event.nativeEvent;
          showTextValidation && validatePassword(text);
        }}
        onBlur={handleBlur}
        secureTextEntry={!visible}
        customStatus={state !== 'error' ? status : undefined}
      />

      {showTextValidation && (
        <View>
          <Separator space={verticalScale(10)} />
          {passwordTextValidation.map((item, index) => {
            return _renderTextValidation({item, index});
          })}
        </View>
      )}
    </>
  );
};

export default PasswordInputValidation;
