## PasswordInputValidation
PasswordInputValidation component.

# Usage
```js
import PasswordInputValidation from '@composition/PasswordInputValidation';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
