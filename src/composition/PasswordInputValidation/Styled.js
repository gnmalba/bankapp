/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {verticalScale, moderateScale, scale} from '@utils/scale';
import {Image} from 'react-native';

export const TextValidationContainer: ComponentType<any> = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-vertical: ${verticalScale(4)}px;
`;

export const CheckIconImage: ComponentType<any> = styled(Image).attrs(
  (props) => ({
    tintColor: props.isValid ? '#84D268' : '#445870',
    source: require('@assets/icons/success-icon.png'),
  }),
)`
  width: ${moderateScale(16)}px;
  height: ${moderateScale(16)}px;
  resize-mode: contain;
  align-self: center;
  margin-right: ${scale(5)}px;
`;

export const StyledVisibilityImage: ComponentType<any> = styled.Image.attrs(
  (props) => {
    const liteVisible = require('@assets/icons/visibility-light-active.png');
    const darkVisible = require('@assets/icons/visibility-light.png');
    const darkActive = require('@assets/icons/visibility-dark-active.png');
    const successIcon = require('@assets/icons/success-icon.png');
    const mode = props.theme?.passwordInput?.mode;
    let source =
      props.state === 'active' && mode === 'dark' && props.value.length > 0
        ? darkActive
        : props.theme?.passwordInput?.src;

    if (props.visible) {
      source = mode === 'dark' ? darkVisible : liteVisible;
    }

    if (props.isFromNewPassword) {
      source = successIcon;
    }

    return {
      source: source,
      resizeMode: 'cover',
    };
  },
)`
  width: ${(props) => moderateScale(props.isFromNewPassword ? 15 : 24)}px;
  height: ${(props) => moderateScale(props.isFromNewPassword ? 10 : 22.7)}px;
`;
