/**
 * @flow
 */

import React from 'react';

import SearchBar from '@composition/SearchBar';

import {FilterWithCounterButton} from '@components/FilterControls';

import {SearchControls} from './styled';

import type {FilterButtonProps} from '@components/FilterControls/types';
import type {SearchBarProps} from '@composition/SearchBar/types';

const SEARCH_BAR_FLEX = 1;

const SearchWithFilterModalToggle = (
  props: SearchBarProps & FilterButtonProps,
): React$Node => {
  const {value, setSearchTerm, filterCount, onPress} = props;
  return (
    <SearchControls>
      <SearchBar
        value={value}
        customFlexValue={SEARCH_BAR_FLEX}
        setSearchTerm={setSearchTerm}
      />
      <FilterWithCounterButton filterCount={filterCount} onPress={onPress} />
    </SearchControls>
  );
};

export default SearchWithFilterModalToggle;
