/**
 * @flow
 */

import type {ComponentType} from 'react';

import styled from 'styled-components/native';

import {verticalScale} from '@utils/scale';

// SEARCH CONTROL SECTION

export const SearchControls: ComponentType<any> = styled.View`
  flex-direction: row;
  align-items: center;
  padding-vertical: ${verticalScale(16)}px;
`;
