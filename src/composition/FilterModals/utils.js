import {
  checkMinIsGreaterThanMax,
  checkAmountRangeIsEmpty,
} from '@components/FilterControls/utils';

export const formatFiltersForFields = (data) => ({
  selectedStartDate: data.dateFrom,
  selectedEndDate: data.dateTo,
  enteredMinAmount: data.amountFrom,
  enteredMaxAmount: data.amountTo,
});

export const formatFiltersForSaving = (data) => ({
  dateFrom: data.selectedStartDate,
  dateTo: data.selectedEndDate,
  amountFrom: data.enteredMinAmount,
  amountTo: data.enteredMaxAmount,
});

export const handleApplyDateRange = ({
  temporaryFilters,
  onPressApply,
  dateRange,
}) => {
  const {selectedStartDate, selectedEndDate} = dateRange;
  return onPressApply({
    ...temporaryFilters,
    selectedStartDate,
    selectedEndDate,
  });
};

export const getDateRangeFilterCount = (from, to) => (from || to ? 1 : 0);

export const getAmountRangeFilterCount = (min, max) => {
  return checkMinIsGreaterThanMax(min, max) || checkAmountRangeIsEmpty(min, max)
    ? 0
    : 1;
};

export const getFiltersCount = (filters) => {
  const {dateFrom, dateTo, amountFrom, amountTo} = filters;
  const dateRange = getDateRangeFilterCount(dateFrom, dateTo);
  const amountRange = getAmountRangeFilterCount(amountFrom, amountTo);

  return dateRange + amountRange;
};

export const validateFilterFields = (fields) => {
  const {
    selectedStartDate,
    selectedEndDate,
    enteredMinAmount,
    enteredMaxAmount,
  } = fields;

  const hasDateRangeComplete = selectedStartDate && selectedEndDate;
  const hasValidAmountRange = !checkMinIsGreaterThanMax(
    enteredMinAmount,
    enteredMaxAmount,
  );

  return hasDateRangeComplete || hasValidAmountRange;
};
