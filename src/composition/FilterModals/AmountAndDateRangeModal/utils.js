/**
 * @flow
 */
import type {
  AmountAndDateRangeFilters,
  AmountAndDateRangeFields,
  CombinedUtilProps,
} from './types';

import {
  CLEAR_ALL_MODE,
  CLEAR_DATE_MODE,
  DEFAULT_DATE_RANGE,
  DEFAULT_FILTER_FIELD_VALUES,
  START_DATE_FIELD,
} from './constants';

export const formatFiltersForFields = (
  data: AmountAndDateRangeFilters,
): AmountAndDateRangeFields => ({
  selectedStartDate: data.dateFrom,
  selectedEndDate: data.dateTo,
  enteredMinAmount: data.amountFrom,
  enteredMaxAmount: data.amountTo,
});

export const formatFiltersForSaving = (
  data: AmountAndDateRangeFields,
): AmountAndDateRangeFilters => ({
  dateFrom: data.selectedStartDate,
  dateTo: data.selectedEndDate,
  amountFrom: data.enteredMinAmount,
  amountTo: data.enteredMaxAmount,
});

export const handleApplyDateRange = ({
  dateRange,
  temporaryFilters,
  setActiveField,
  setTemporaryFilters,
}: $Shape<CombinedUtilProps>): void => {
  const {selectedStartDate, selectedEndDate} = dateRange;
  setActiveField('');
  setTemporaryFilters({
    ...temporaryFilters,
    selectedStartDate,
    selectedEndDate,
  });
};

export const handleCustomOnChange = ({
  dateRange,
  temporaryFilters,
  setTemporaryFilters,
}: $Shape<CombinedUtilProps>): void => {
  const {selectedStartDate, selectedEndDate} = dateRange;
  setTemporaryFilters({
    ...temporaryFilters,
    selectedStartDate,
    selectedEndDate,
  });
};

export const handleApplyAllFilters = ({
  temporaryFilters,
  applyFilters,
  onCloseModal,
  setActiveField,
}: $Shape<CombinedUtilProps>) => {
  const formattedFilters = formatFiltersForSaving(temporaryFilters);
  setActiveField('');
  applyFilters(formattedFilters);
  onCloseModal();
};

export const handleClear = ({
  filters,
  setTemporaryFilters,
  temporaryFilters,
  setActiveField,
  showCalendar,
  mode,
}: $Shape<CombinedUtilProps>): void => {
  if (mode === CLEAR_DATE_MODE) {
    setTemporaryFilters({
      ...temporaryFilters,
      ...DEFAULT_DATE_RANGE,
    });
    setActiveField(START_DATE_FIELD);
    return;
  }
  if (mode === CLEAR_ALL_MODE) {
    return setTemporaryFilters(DEFAULT_FILTER_FIELD_VALUES);
  }
  setActiveField('');
  setTemporaryFilters(formatFiltersForFields(filters));
};

export const handleCancel = ({
  onCloseModal,
  filters,
  showCalendar,
  temporaryFilters,
  setTemporaryFilters,
  setActiveField,
}: $Shape<CombinedUtilProps>) => {
  setActiveField('');
  setTemporaryFilters({
    ...formatFiltersForFields(filters),
  });
  !showCalendar && onCloseModal();
};
