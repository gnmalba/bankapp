export const START_DATE_FIELD = 'START_DATE';
export const END_DATE_FIELD = 'END_DATE';

export const CLEAR_DATE_LABEL = 'Clear';
export const CLEAR_ALL_LABEL = 'Clear All';
export const CLEAR_DATE_MODE = 'CLEAR_DATE';
export const CLEAR_ALL_MODE = 'CLEAR_ALL';

export const DEFAULT_DATE_RANGE = {
  selectedStartDate: null,
  selectedEndDate: null,
};

export const DEFAULT_AMOUNT_RANGE = {
  enteredMinAmount: null,
  enteredMaxAmount: null,
};

export const DEFAULT_FILTER_FIELD_VALUES = {
  ...DEFAULT_AMOUNT_RANGE,
  ...DEFAULT_DATE_RANGE,
};

export const DEFAULT_ACTIVE_CONTROLS = {
  activeField: '',
  showFilterModal: false,
  showCalendar: false,
};
