import React from 'react';

import Button from '@components/Button';

import {validateFilterFields} from '../utils';
import {handleApplyAllFilters, handleCancel} from './utils';

const SectionForApplyAndCancel = ({
  showCalendar,
  temporaryFilters,
  applyFilters,
  filters,
  setTemporaryFilters,
  setActiveField,
  onCloseModal,
}) => {
  const isOkayToApply = validateFilterFields(temporaryFilters);

  if (showCalendar) {
    return <></>;
  }
  return (
    <>
      <Button
        state={isOkayToApply ? 'default' : 'disabled'}
        type="primary"
        size="large"
        title="Apply"
        onPress={() =>
          handleApplyAllFilters({
            temporaryFilters,
            applyFilters,
            onCloseModal,
            setActiveField,
          })
        }
      />
      <Button
        type="primary"
        size="large"
        title="Cancel"
        onPress={() =>
          handleCancel({
            filters,
            setTemporaryFilters,
            setActiveField,
            temporaryFilters,
            onCloseModal,
          })
        }
      />
    </>
  );
};

export default SectionForApplyAndCancel;
