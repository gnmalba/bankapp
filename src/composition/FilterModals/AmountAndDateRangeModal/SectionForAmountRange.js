import React from 'react';

import {
  FilterAmountInputRange,
  FilterSectionTitle,
} from '@components/FilterControls';
const SectionAmountRange = ({
  activeField,
  setTemporaryFilters,
  visible,
  setActiveField,
  temporaryFilters,
}) => {
  const {enteredMinAmount, enteredMaxAmount} = temporaryFilters;

  if (visible) {
    return <></>;
  }

  return (
    <>
      <FilterSectionTitle>Amount</FilterSectionTitle>
      <FilterAmountInputRange
        activeField={activeField}
        minAmount={enteredMinAmount}
        maxAmount={enteredMaxAmount}
        onFocusAmountField={(fieldName) => setActiveField(fieldName)}
        setAmountRange={(value, fieldName) =>
          setTemporaryFilters({
            ...temporaryFilters,
            [fieldName]: value,
          })
        }
      />
    </>
  );
};

export default SectionAmountRange;
