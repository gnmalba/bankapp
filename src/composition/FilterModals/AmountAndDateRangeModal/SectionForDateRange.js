import React from 'react';

import {
  FilterDateRangeControls,
  FilterSectionTitle,
} from '@components/FilterControls';

import {handleApplyDateRange, handleCustomOnChange, handleClear} from './utils';

const SectionDateRange = ({
  temporaryFilters,
  activeField,
  setActiveField,
  setTemporaryFilters,
  showCalendar,
  filters,
}) => {
  const {selectedStartDate, selectedEndDate} = temporaryFilters;

  return (
    <>
      <FilterSectionTitle>By Date</FilterSectionTitle>
      <FilterDateRangeControls
        initialActiveField={activeField}
        startDate={selectedStartDate}
        endDate={selectedEndDate}
        customOnChange={(dateRange) =>
          handleCustomOnChange({
            temporaryFilters,
            setTemporaryFilters,
            dateRange,
          })
        }
        onPressApply={(dateRange) =>
          handleApplyDateRange({
            setActiveField,
            temporaryFilters,
            setTemporaryFilters,
            dateRange,
          })
        }
        onPressCancel={() =>
          handleClear({
            setTemporaryFilters,
            filters,
            temporaryFilters,
            setActiveField,
            showCalendar,
            mode: 'cancel',
          })
        }
        onPressDateControl={(controlName) => setActiveField(controlName)}
      />
    </>
  );
};

export default SectionDateRange;
