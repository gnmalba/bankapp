/**
 * @flow
 */

export type DateRangeProps = {
  selectedStartDate: ?string,
  selectedEndDate: ?string,
};

export type AmountAndDateRangeFields = {
  selectedStartDate: ?string,
  selectedEndDate: ?string,
  enteredMinAmount: ?number,
  enteredMaxAmount: ?number,
};

export type AmountAndDateRangeFilters = {
  dateFrom: ?string,
  dateTo: ?string,
  amountFrom: ?number,
  amountTo: ?number,
};

export type CombinedUtilProps = {
  dateRange: DateRangeProps,
  filters: AmountAndDateRangeFilters,
  temporaryFilters: AmountAndDateRangeFields,
  mode: string,
  showCalendar: boolean,
  applyFilters: any,
  onCloseModal: any,
  setActiveField: any,
  setTemporaryFilters: any,
};

export type AmountAndDateRangeMainProps = {
  visible: boolean,
  filters: AmountAndDateRangeFilters,
  applyFilters: any,
  onCloseModal: any,
};
