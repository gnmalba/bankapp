/**
 * @format
 * @flow
 */

import React from 'react';

import {FilterModal} from '@components/FilterControls';

import SectionForAmountRange from './SectionForAmountRange';
import SectionForDateRange from './SectionForDateRange';
import SectionForApplyAndCancel from './SectionForApplyAndCancel';

import {handleCancel, handleClear} from './utils';

import {
  START_DATE_FIELD,
  END_DATE_FIELD,
  CLEAR_ALL_LABEL,
  CLEAR_ALL_MODE,
  CLEAR_DATE_LABEL,
  CLEAR_DATE_MODE,
  DEFAULT_FILTER_FIELD_VALUES,
} from './constants';

import type {AmountAndDateRangeMainProps} from './types';

const AmountAndDateRangeModal = (
  props: AmountAndDateRangeMainProps,
): React$Node => {
  const {filters, applyFilters, visible, onCloseModal} = props;

  const [activeField, setActiveField] = React.useState('');

  const [temporaryFilters, setTemporaryFilters] = React.useState({
    ...DEFAULT_FILTER_FIELD_VALUES,
  });

  const showCalendar =
    activeField === START_DATE_FIELD || activeField === END_DATE_FIELD;

  return (
    <FilterModal
      visible={visible}
      filterControlName={
        showCalendar ? 'Filter by date' : 'Filter Transactions'
      }
      rightButtonLabel={showCalendar ? CLEAR_DATE_LABEL : CLEAR_ALL_LABEL}
      onPressRightButton={() =>
        handleClear({
          setTemporaryFilters,
          filters,
          temporaryFilters,
          setActiveField,
          showCalendar,
          mode: showCalendar ? CLEAR_DATE_MODE : CLEAR_ALL_MODE,
        })
      }
      onPressLeftButton={() =>
        handleCancel({
          filters,
          showCalendar,
          setTemporaryFilters,
          setActiveField,
          temporaryFilters,
          onCloseModal,
        })
      }>
      <SectionForDateRange
        showCalendar={showCalendar}
        filters={filters}
        activeField={activeField}
        temporaryFilters={temporaryFilters}
        setActiveField={setActiveField}
        setTemporaryFilters={setTemporaryFilters}
      />
      <SectionForAmountRange
        visible={showCalendar}
        activeField={activeField}
        temporaryFilters={temporaryFilters}
        setActiveField={setActiveField}
        setTemporaryFilters={setTemporaryFilters}
      />
      <SectionForApplyAndCancel
        showCalendar={showCalendar}
        filters={filters}
        setActiveField={setActiveField}
        setTemporaryFilters={setTemporaryFilters}
        temporaryFilters={temporaryFilters}
        applyFilters={applyFilters}
        onCloseModal={onCloseModal}
      />
    </FilterModal>
  );
};

export default AmountAndDateRangeModal;
