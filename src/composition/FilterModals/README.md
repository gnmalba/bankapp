## FilterModals

FilterModals component.

# Usage

```js
import FilterModals from '@composition/FilterModals';
```

## AmountAndDateRangeModal Props

| Prop               | Type     | Required | Description                                            |
| ------------------ | -------- | -------- | ------------------------------------------------------ |
| testID             | string   | false    | The testID prop                                        |
| accessibilityLabel | string   | false    | The accessibilityLabel prop                            |
| visible            | boolean  | true     | visibility of modal                                    |
| filters            | string   | true     | Amount range and date range filter data                |
| applyFilters       | function | true     | Use a React hook for applying filters                  |
| onCloseModal       | string   | true     | Provide a function that would handle toggling of Modal |

## SearchWithFilterModalToggle Props

| Prop               | Type     | Required | Description                                                |
| ------------------ | -------- | -------- | ---------------------------------------------------------- |
| testID             | string   | true     | The testID prop                                            |
| accessibilityLabel | string   | true     | The accessibilityLabel prop                                |
| value              | string   | true     | Value of search bar                                        |
| filterCount        | string   | true     | Count of active filters                                    |
| setSearchTerm      | function | true     | Use a React hook for setting search value                  |
| onPress            | string   | true     | Provide a function that will handle on press filter toggle |
