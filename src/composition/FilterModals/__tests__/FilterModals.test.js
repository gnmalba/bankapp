import React from 'react';
import {render} from '@testing-library/react-native';
import {
  AmountAndDateRangeModal,
  SearchWithFilterModalToggle,
} from '@composition/FilterModals';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('FilterModals', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <SearchWithFilterModalToggle />
        <AmountAndDateRangeModal />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
