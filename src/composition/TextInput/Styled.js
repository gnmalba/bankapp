/**
 * @flow
 */

import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View`
  align-items: flex-start;
`;

export const StyledText: ComponentType<any> = styled(Text).attrs((props) => {
  return {
    customColor:
      props.theme?.textInput?.helperTextColor[props.state || 'default'],
  };
})``;
