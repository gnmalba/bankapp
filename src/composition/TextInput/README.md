## TextInput
TextInput component.

# Usage
```js
import TextInput from '@composition/TextInput';

# Props
```
Prop                  | Type                 | Required               | Description
----------------------|----------------------|------------------------|--------------------------
testID                | string               | true                   | Test ID for testing and accessibility
accessibilityLabel    | string               | true                   | accessibility label
value                 | string               | false                  | The text value
onChangeText          | Function             | false                  | The change handler
nativeProps           | Object               | false                  | Native props added to TextInput
theme                 | Object               | false                  | Theme object
state                 | BasicTextInputState  | false                  | The currect state of the component
icon                  | iconEnumType         | false                  | The left icon image
customIcon            | Node                 | false                  | Customized icon component
customStatus          | Node                 | false                  | Customized status component
iconLabel             | Node                 | false                  | Additional label props added to the label
showStatus            | boolean              | false                  | Controls the visibility of the status image
handleClear           | Function             | false                  | For active state, handler to cleat the value
secureTextEntry       | boolean              | false                  | if true, value will be masked.
label                 | string               | false                  | If supplied, the top label
helperText            | string               | false                  | if supplied, the bottom helper text