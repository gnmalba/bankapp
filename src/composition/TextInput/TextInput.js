/**
 * @format
 * @flow
 */

import React from 'react';
import Text from '@components/Text';
import IconInputStatus from '@components/IconInputStatus';

import type {TextInputProps} from '@utils/commonTypes';
import {Container, StyledText} from './Styled';
import {LabelContainer} from '@composition/LabelledTextInput/Styled';
import {HelperContainer} from '@composition/TextInputWithHelper/styled';
import {testProps} from '@utils/componentCustomProps';

const TextInput = (props: TextInputProps): React$Node => {
  const {label, helperText, testID, dividerHeight, ...rest} = props;
  return (
    <Container>
      {label && (
        <LabelContainer>
          <Text
            {...testProps(testID ? testID + 'Label' : 'labelTestId')}
            mode="body2">
            {label}
          </Text>
        </LabelContainer>
      )}
      <IconInputStatus
        {...rest}
        testID={testID}
        dividerHeight={dividerHeight}
      />
      {helperText ? (
        <HelperContainer {...testProps(label ? label : '' + 'ErrorTestId')}>
          <StyledText
            mode="body2"
            {...testProps(
              testID ? testID + 'HelperText' : 'inputHelperTextTestId',
            )}
            state={rest.state}>
            {helperText}
          </StyledText>
        </HelperContainer>
      ) : (
        <></>
      )}
    </Container>
  );
};

export default TextInput;
