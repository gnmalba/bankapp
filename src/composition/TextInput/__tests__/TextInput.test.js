import React from 'react';
import {render} from '@testing-library/react-native';
import TextInput from '../TextInput';
import {DARK_THEME} from '@utils/constants';

describe('TextInput', () => {
  it.each(['default', undefined])('Should work as expected', (txt) => {
    const all = render(
      <TextInput label={txt} helperText={txt} state={txt} theme={DARK_THEME} />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
  it.each(['default', undefined])('Should work as expected', (state) => {
    const all = render(
      <TextInput
        label={state}
        helperText={state}
        state={state}
        theme={undefined}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
