/**
 * @format
 * @flow
 */

import type {TransactionType} from './types';
export function checkIfPositiveByCardType(
  item: TransactionType,
  cardType: string,
): boolean {
  switch (cardType) {
    case 'credit':
    case 'yazz':
      return item.isPositve;
    case 'deposit':
      return item?.amount?.value > 0;
    default:
      return false;
  }
}
