/* eslint-disable sonarjs/cognitive-complexity */
/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import moment from 'moment';
import {numberFormat, maskingFormat} from '@utils/accountNumber';
import {ListItem} from 'react-native-elements';
import {testProps} from '@utils/componentCustomProps';

import {checkIfPositiveByCardType} from './utils';

const TransactionHistoryList = (props: PropsType): React$Node => {
  const {
    index = 0,
    item,
    cardType,
    runningBalance,
    overviewCurrency,
    creditSupplementary,
  } = props;

  const isCredit = cardType === 'credit';
  const isDeposit = cardType === 'deposit';

  const currencyCode = isCredit ? overviewCurrency : item?.amount?.currencyCode;
  const amount = item?.amount?.value;
  const description = item?.description?.line1;
  const currencyCodeRunningBalance = item?.runningBalance?.currencyCode;
  const RunningBalance = item?.runningBalance?.value;
  const transactionDate = item?.transactionDate;
  const transactionAmount = item?.transactionAmount;
  const transactionCurrency =
    item?.transactionCurrency === null
      ? currencyCode
      : item?.transactionCurrency;
  const isPositive = checkIfPositiveByCardType(item, cardType);
  const signAmount = isPositive ? '+' : '-';
  const totalAmount = `${signAmount} ${currencyCode} ${numberFormat(
    Math.abs(amount),
  )}`;
  const isPosted = item?.posted ? totalAmount : null;
  const isRunningBalance = `${currencyCodeRunningBalance} ${numberFormat(
    RunningBalance,
  )}`;

  const uniqueIndex = index + 1;

  const renderTitle = () => (
    <S.TitleTransaction>
      <S.LabelTransaction1
        {...testProps(uniqueIndex + 'TransactionHistoryCardEmbossingName')}>
        {creditSupplementary ? 'LATEST TRANSACTIONS' : item?.cardEmbossingName}
      </S.LabelTransaction1>
      {!creditSupplementary && (
        <S.LabelTransaction2
          {...testProps(uniqueIndex + 'TransactionHistoryCardNumber')}>
          {maskingFormat(item?.cardNo, true)}
        </S.LabelTransaction2>
      )}
    </S.TitleTransaction>
  );

  const renderBody = () => (
    <S.RNListitem
      key={index}
      index={index}
      listColor={isCredit ? index % 2 === 0 : index % 2 !== 0}>
      <ListItem.Content>
        <S.TrasactionListContainer>
          <S.ListContainer flex={1} flexDirection="flex-start">
            <S.ListAlias
              {...testProps(uniqueIndex + 'TransactionHistoryCardAlias')}>
              {description}
            </S.ListAlias>

            <S.ListDate
              {...testProps(uniqueIndex + 'TransactionHistoryTransactionDate')}>
              {moment(transactionDate).format('MM/DD/YYYY')}
            </S.ListDate>

            {isCredit && (
              <S.ListRunningBalance
                {...testProps(
                  uniqueIndex + 'TransactionHistoryRunningBalance',
                )}>{`${transactionCurrency} ${numberFormat(
                transactionAmount,
              )}`}</S.ListRunningBalance>
            )}
          </S.ListContainer>

          <S.ListContainer flexDirection="flex-end">
            <S.ListTotal
              amount={20}
              isPositive={isPositive}
              {...testProps(
                uniqueIndex +
                  (isCredit
                    ? 'TransactionHistoryIsPosted'
                    : 'TransactionHistoryDetail'),
              )}>
              {isCredit ? isPosted : totalAmount}
            </S.ListTotal>

            {isDeposit && (
              <S.ListRunningBalance
                {...testProps(
                  uniqueIndex + 'TransactionHistoryRunningBalance',
                )}>
                {runningBalance && isRunningBalance}
              </S.ListRunningBalance>
            )}

            {isCredit && !item?.posted && (
              <S.ListUnposted
                {...testProps(uniqueIndex + 'TransactionHistoryUnposted')}>
                UNPOSTED
              </S.ListUnposted>
            )}
          </S.ListContainer>
        </S.TrasactionListContainer>
      </ListItem.Content>
    </S.RNListitem>
  );

  return item?.title ? renderTitle() : renderBody();
};

export default TransactionHistoryList;
