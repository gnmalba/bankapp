/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';
import {ListItem} from 'react-native-elements';

export const RNListitem: ComponentType<any> = styled(ListItem).attrs(
  (props) => ({
    containerStyle: {
      backgroundColor: props.listColor
        ? props.theme.transaction.background
        : 'transparent',
      padding: verticalScale(20),
    },
  }),
)``;

export const TrasactionListContainer: ComponentType<any> = styled.View`
  flex-direction: row;
`;

export const TrasactionListCard: ComponentType<any> = styled.View`
  padding: ${scale(20)}px;
`;

export const ListAlias: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(18)}px;
  line-height: ${scale(22)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const ListDate: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(14)}px;
  line-height: ${scale(17)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const ListRunningBalance: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(14)}px;
  line-height: ${scale(17)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const ListTotal: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: normal;
  font-size: ${scale(18)}px;
  line-height: ${scale(22)}px;
  color: ${(props) => (props.isPositive ? '#84D268' : '#F65A4C')};
`;

export const ListContainer: ComponentType<any> = styled.View`
  flex-direction: column;
  flex: ${(props) => props.flex};
  align-items: ${(props) => props.flexDirection};
`;

export const ListUnposted: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${scale(10)}px;
  line-height: ${scale(12)}px;
  color: ${(props) => '#B7C823'};
  letter-spacing: ${scale(0.846154)}px;
  text-transform: uppercase;
`;

export const TitleTransaction: ComponentType<any> = styled.View`
  background: ${(props) => props.theme.trasactionList.background};
  flex-direction: row;
  padding-vertical: ${verticalScale(7)}px;
`;

export const LabelTransaction1: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${scale(14)}px;
  line-height: ${scale(14)}px;
  text-transform: uppercase;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  margin-horizontal: ${scale(24)}px;
  flex-direction: column;
  align-self: center;
  flex: 1;
`;

export const LabelTransaction2: ComponentType<any> = styled.Text`
  font-family: Myriad Pro;
  font-style: normal;
  font-weight: bold;
  font-size: ${scale(14)}px;
  line-height: ${scale(14)}px;
  text-transform: uppercase;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  margin-horizontal: ${scale(24)}px;
  flex-direction: column;
  align-self: center;
`;
