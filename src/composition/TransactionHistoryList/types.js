/**
 * @flow
 */
export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  index?: number,
  cardType: string,
  item: Object,
  runningBalance?: boolean,
  overviewCurrency: string,
  creditSupplementary?: boolean,
};

export type TransactionType = {
  amount: {
    value: number,
    currencyCode: string,
  },
  description: {
    line1: string,
  },
  runningBalance: {
    currencyCode: string,
    value: number,
  },
  transactionDate: string,
  transactionAmount: number,
  isPositve: boolean,
};
