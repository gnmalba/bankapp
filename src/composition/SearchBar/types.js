/**
 * @flow
 */

export type SearchBarProps = {
  testID?: string,
  accessibilityLabel?: string,
  value?: string,
  placeholder?: string,
  setSearchTerm: any | ((string) => string),
  customFlexValue?: number,
};
