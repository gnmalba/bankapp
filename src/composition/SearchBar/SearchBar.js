/**
 * @flow
 */

// React Modules
import React, {useState, useCallback, useEffect} from 'react';

// Third party libraries
import {debounce} from 'lodash';

// Styled
import {
  ClearButton,
  ClearIcon,
  SearchBarContainer,
  SearchIconContainer,
  SearchIcon,
  SearchInput,
} from './Styled';

// Utils

import {
  handleSearchInputOnFocusState,
  handleSearchInputOnBlurState,
  isEmpty,
} from './utils';

import type {SearchBarProps} from './types';

const SearchBar = (props: SearchBarProps): React$Node => {
  const {value, placeholder, setSearchTerm, customFlexValue} = props;

  const [isFocused, setIsFocused] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const handler = useCallback(
    debounce((text) => setSearchTerm && setSearchTerm(text), 1000),
    [],
  );

  const handleChange = (text) => setInputValue(text);

  const handleClearSearchbar = () => {
    setSearchTerm('');
    setInputValue('');
  };

  useEffect(() => {
    handler(inputValue);
  }, [handler, inputValue]);

  return (
    <SearchBarContainer isFocused={isFocused} customFlexValue={customFlexValue}>
      <SearchIconContainer>
        <SearchIcon />
      </SearchIconContainer>
      <SearchInput
        value={inputValue}
        onFocus={() => handleSearchInputOnFocusState(isFocused, setIsFocused)}
        onBlur={() => handleSearchInputOnBlurState(isFocused, setIsFocused)}
        onChangeText={handleChange}
        placeholder={placeholder}
      />
      {!isEmpty(value) && (
        <ClearButton onPress={handleClearSearchbar}>
          <ClearIcon />
        </ClearButton>
      )}
    </SearchBarContainer>
  );
};

export default SearchBar;
