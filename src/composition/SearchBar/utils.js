export const isEmpty = (value) => {
  return !value || value === '' || value.length === 0;
};

export const getColors = (props) => {
  const defaultSearchBarColor = {
    background: props.theme.color.BACKGROUND_300,
    border: props.theme.color.BACKGROUND_300,
    borderRight: props.theme.color.TEXT_300_LOW,
  };

  if (props.isFocused) {
    return {
      ...defaultSearchBarColor,
      border: props.theme.color.SECONDARY_DEFAULT,
    };
  }

  return defaultSearchBarColor;
};

export const handleSearchInputOnFocusState = (
  isFocused,
  onFocus,
  otherOnFocusMethods,
) => {
  onFocus(true);
  otherOnFocusMethods &&
    otherOnFocusMethods.length !== 0 &&
    otherOnFocusMethods.forEach((method) => method(isFocused));
};

export const handleSearchInputOnBlurState = (
  isFocused,
  onBlur,
  otherOnBlurMethods,
) => {
  onBlur(false);
  otherOnBlurMethods &&
    otherOnBlurMethods.length !== 0 &&
    otherOnBlurMethods.forEach((method) => method(isFocused));
};
