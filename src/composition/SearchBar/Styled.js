/**
 * @flow
 */

import type {ComponentType} from 'react';

import Fontisto from 'react-native-vector-icons/Fontisto';
import styled from 'styled-components/native';

import {moderateScale, verticalScale, scale} from '@utils/scale';

// Utils
import {getColors} from './utils';

/* Containers */

export const Container: ComponentType<any> = styled.View``;

export const SearchBarContainer: ComponentType<any> = styled.View`
  ${(props) =>
    props.customFlexValue ? `flex: ${props.customFlexValue};` : ''};
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border-width: 1px;
  border-radius: 4px;
  border-color: ${(props) => getColors(props).border};
  background-color: ${(props) => getColors(props).background};
`;

/* Clear Button */

export const ClearButton: ComponentType<any> = styled.TouchableOpacity.attrs(
  (props) => ({
    ...props,
  }),
)`
  padding-horizontal: 10px;
`;

export const ClearIcon: ComponentType<any> = styled(Fontisto).attrs(
  (props) => ({
    name: 'close-a',
    size: scale(9),
    color: props.theme?.text.textEmphasis,
  }),
)``;

/* Search Input */

export const SearchInput: ComponentType<any> = styled.TextInput.attrs(
  (props) => ({
    ...props,
    placeholderTextColor: props.theme?.textInput.placeholderTextColor,
  }),
)`
  flex: 1;
  height: ${verticalScale(48)}px;
  font-size: ${moderateScale(16)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

/* Search Icon */

export const SearchIconContainer: ComponentType<any> = styled.View`
  border-right-width: 1px;
  border-right-color: ${(props) => getColors(props).borderRight};
  padding-horizontal: 8px;
  margin-right: 16px;
  height: ${verticalScale(48)}px;
  align-items: center;
  justify-content: center;
`;

export const SearchIcon: ComponentType<any> = styled(Fontisto).attrs(
  (props) => ({
    name: 'search',
    size: scale(14),
    color: props.theme?.color.TEXT_300_HIGH,
  }),
)``;
