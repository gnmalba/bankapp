import React from 'react';
import {render} from '@testing-library/react-native';
import InstallmentLeavePromptModal from '../InstallmentLeavePromptModal';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {INITIAL_STATE} from '@ducks/installments/reducer';

describe('InstallmentLeavePromptModal', () => {
  const store = {
    installments: INITIAL_STATE,
  };
  it('Should work as expected', () => {
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <InstallmentLeavePromptModal />
        </ThemeProvider>
        ,
      </MockProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
