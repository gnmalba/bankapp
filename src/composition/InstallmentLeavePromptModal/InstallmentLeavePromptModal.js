/**
 * @format
 * @flow
 */

import React from 'react';
import {useDispatch, useSelector} from 'react-redux';

// Composition
import ModalWithCTA from '@composition/ModalWithCTA';

import type {PropsType} from './types';
import {} from './Styled';
import {
  defaulNegativeButtonProps,
  defaultPositiveButtonProps,
  defaultSubTitleProps,
  defaultTitleProps,
} from './config';
import {clearInstallments, setLeavePrompt} from '@ducks/installments/actions';
import {useNavigation} from '@react-navigation/core';

const InstallmentLeavePromptModal = (props: PropsType): React$Node => {
  const {
    currentInstallmentType,
    leavePromptVisible,
    selectedCredit,
  } = useSelector((state) => state.installments);
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleGoBack = () => {
    if (selectedCredit) {
      navigation.navigate('CreditScreenOverview', {
        id: selectedCredit.cardSerNo,
        alias: selectedCredit.alias,
      });
    } else {
      navigation.goBack();
    }
    dispatch(clearInstallments());
    dispatch(setLeavePrompt(false));
  };

  const handleCancel = () => {
    dispatch(setLeavePrompt(false));
  };

  return (
    <ModalWithCTA
      testID="installmentleavePromptModal"
      isVisible={leavePromptVisible}
      titleProps={defaultTitleProps(currentInstallmentType)}
      subtitleProps={defaultSubTitleProps(currentInstallmentType)}
      positiveButtonProps={defaultPositiveButtonProps(handleGoBack)}
      negativeButtonProps={defaulNegativeButtonProps(handleCancel)}
    />
  );
};

export default InstallmentLeavePromptModal;
