// @flow

export const defaultTitleProps = (
  installmentType: string,
): {text: string, titleStyle: Object} => {
  return {
    text: 'Are you sure you want to leave?',
    titleStyle: {},
  };
};

export const defaultSubTitleProps = (
  installmentType: string,
): {text: string, subtitleStyle: Object} => {
  return {
    text: `If you leave this screen, the information you entered will be erased and you will have to enter them again should you wish to continue with your transaction.
    `,
    subtitleStyle: {},
  };
};

export const defaultPositiveButtonProps = (
  cb: Function,
): {buttonText: string, callback: Function, buttonTextStyle: Object} => {
  return {
    buttonText: 'Yes I want to leave',
    callback: cb,
    buttonTextStyle: {},
  };
};

export const defaulNegativeButtonProps = (
  cb: Function,
): {buttonText: string, callback: Function, buttonTextStyle: Object} => {
  return {
    buttonText: 'Cancel',
    callback: cb,
    buttonTextStyle: {},
  };
};
