## InstallmentLeavePromptModal
InstallmentLeavePromptModal component.

# Usage
```js
import InstallmentLeavePromptModal from '@composition/InstallmentLeavePromptModal';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
