/**
 * @flow
 */
type ContainerStyle = {
  height?: number,
  padding?: string,
  alignItems?: string,
  justifyContent?: string,
};

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  timePickerLabel?: string,
  timePickerProps: Object,
  onBackdropPress?: any,
  containerStyle?: ContainerStyle,
  mode:
    | 'h1'
    | 'h2'
    | 'h3'
    | 'body1'
    | 'body2'
    | 'subtitle'
    | 'buttontext'
    | 'buttonlink'
    | 'link'
    | 'caption'
    | 'overline1'
    | 'overline2'
    | 'custom'
    | 'listitem',
};
