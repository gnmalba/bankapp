/**
 * @format
 * @flow
 */
// React Modules
import React from 'react';

// Types
import type {PropsType} from './types';

// Components
import Modal from '@composition/Modal';
import TimePicker from '@components/TimePicker';
import Text from '@components/Text';

// Styled Componentes
import {ModalContainer, TextContainer} from './Styled';

const TimePickerModal = (props: PropsType): React$Node => {
  const {
    timePickerLabel,
    mode,
    onBackdropPress,
    timePickerProps,
    containerStyle,
  } = props;

  return (
    <Modal isVisible={true} onBackdropPress={onBackdropPress}>
      <ModalContainer containerStyle={containerStyle}>
        <TextContainer>
          <Text mode={mode}>{timePickerLabel}</Text>
        </TextContainer>
        <TimePicker timePickerProps={timePickerProps} />
      </ModalContainer>
    </Modal>
  );
};

export default TimePickerModal;
