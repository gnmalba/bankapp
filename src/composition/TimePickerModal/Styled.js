/**
 * @flow
 */

import type {ComponentType} from 'react';
import {View, Platform} from 'react-native';
import styled from 'styled-components/native';

import {scale} from '@utils/scale';

export const ModalContainer: ComponentType<any> = styled(View).attrs(
  (props) => ({
    height: props?.containerStyle?.height,
    alignItems: props?.containerStyle?.alignItems,
    justifyContent: props?.containerStyle?.justifyContent,
  }),
)`
  align-items: center;
  justify-content: center;
`;

export const TextContainer: ComponentType<any> = styled.View`
  padding-bottom: ${Platform.select({android: scale(60), ios: scale(45)})};
`;
