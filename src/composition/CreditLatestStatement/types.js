/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  navigation?: any,
  theme?: Object,
  accountId?: string,
  route?: Object,
  statementDetails?: Object,
  overviewData?: Object,
};
