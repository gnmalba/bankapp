## CreditStatementInstallment
CreditStatementInstallment component.

# Usage
```js
import CreditStatementInstallment from '@composition/CreditStatementInstallment';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
