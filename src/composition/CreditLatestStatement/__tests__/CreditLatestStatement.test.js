import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CreditStatementInstallment from '../CreditLatestStatement';
import {testProps} from '@utils/componentCustomProps';
import MockProvider from '@utils/testFramework';

describe('CreditStatementInstallment', () => {
  const navigation = {
    navigate: () => {},
  };
  it.each(['CreditLatestStatement', ''])(
    'Should work as expected',
    (testID) => {
      // Edit the args inside render and provide props.
      const all = render(
        <MockProvider>
          <CreditStatementInstallment
            {...testProps(testID)}
            navigation={testID === '' ? undefined : navigation}
          />
        </MockProvider>,
      );
      if (testID) {
        const el = all.getByTestId('CreditLatestStatementViewStatement');
        fireEvent(el, 'onPress');
      }
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
