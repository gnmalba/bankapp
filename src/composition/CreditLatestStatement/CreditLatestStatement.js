/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import Text from '@components/Text';
import {useDispatch} from 'react-redux';

import {testProps} from '@utils/componentCustomProps';

const CreditStatementInstallment = (props: PropsType): React$Node => {
  const {navigation, theme, accountId, statementDetails, overviewData} = props;
  const dispatch = useDispatch();

  return (
    <S.MainContainer>
      <Text mode="h3" {...testProps('CreditLatestStatementLastStatementTitle')}>
        Latest Statement
      </Text>

      <S.Divider
        top={6}
        {...testProps('CreditLatestStatementLastStatementTitleDivider')}
      />

      <S.CardContainer theme={theme}>
        <S.DetailsContainer flexDirection="column">
          <Text
            mode="overline1"
            themeColor="TEXT_100_HIGH"
            {...testProps('CreditLatestStatementTotalAmountDueLabel')}>
            Total amount due
          </Text>
          <Text
            mode="h2"
            {...testProps('CreditLatestStatementTotalAmountDueValue')}>
            {statementDetails?.data?.totalAmountDue || 'PHP 0.00'}
          </Text>
        </S.DetailsContainer>

        <S.Divider top={16} {...testProps('CreditLatestStatementDivider1')} />

        <S.DetailsContainer flexDirection="row">
          <S.DetailsContainer flexDirection="column" flex={2}>
            <Text
              mode="overline1"
              themeColor="TEXT_100_HIGH"
              {...testProps('CreditLatestStatementMinimumAmountDueLabel')}>
              Minimum amount due
            </Text>
            <Text
              mode="body2"
              {...testProps('CreditLatestStatementMinimumAmountDueValue')}>
              {statementDetails?.data?.minimumAmountDue || 'PHP 0.00'}
            </Text>
          </S.DetailsContainer>

          <S.DetailsContainer flexDirection="column" flex={1}>
            <Text
              mode="overline1"
              themeColor="TEXT_100_HIGH"
              {...testProps('CreditLatestStatementDueDateLabel')}>
              due date
            </Text>
            <Text
              mode="body2"
              {...testProps('CreditLatestStatementDueDateValue')}>
              {statementDetails?.data?.dueDate || '-'}
            </Text>
          </S.DetailsContainer>
        </S.DetailsContainer>

        <S.Divider top={16} {...testProps('CreditLatestStatementDivider2')} />

        <S.StatementTouch
          {...testProps('CreditLatestStatementViewStatement')}
          onPress={() => {
            dispatch({
              type: 'REQUEST_SOA_INCOMING_LAST_API',
              payload: accountId,
            });
            navigation &&
              navigation.navigate('Accounts', {
                screen: 'StatementOfAccountScreen',
                params: {id: accountId, overviewData: overviewData},
              });
          }}>
          <Text
            mode="link"
            themeColor="SECONDARY_DEFAULT"
            {...testProps('CreditLatestStatementViewStatementLabel')}>
            View Statement
          </Text>
        </S.StatementTouch>
      </S.CardContainer>
    </S.MainContainer>
  );
};

export default CreditStatementInstallment;
