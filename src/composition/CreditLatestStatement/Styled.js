/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {moderateScale, scale, verticalScale} from '@utils/scale';
import {TouchableOpacity} from 'react-native';

export const ActionText: ComponentType<any> = styled.Text`
  font-family: 'MyriadPro-Semibold';
  font-style: normal;
  font-weight: 600;
  font-size: ${(props) => moderateScale(props.size)}px;
  line-height: ${(props) => scale(props.line)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: column;
  margin-top: ${verticalScale(32)}}px;
`;

export const CardContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: ${scale(8)}px;
  padding: ${scale(20)}px;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  flex: ${(props) => props.flex};
  flex-direction: ${(props) => props.flexDirection}; ;
`;

export const StatementTouch: ComponentType<any> = styled(
  TouchableOpacity,
).attrs((props) => ({
  ...props,
  onPress: props.onPress,
  activeOpacity: 1,
}))`
  align-items: center;
`;

export const Divider: ComponentType<any> = styled.View`
  margin-top: ${(props) => verticalScale(props.top)}px;
`;
