/**
 * @flow
 */

import type {ComponentType} from 'react';

import styled from 'styled-components/native';
// Compositions
import TextInput from '@composition/TextInput';

export const Container: ComponentType<any> = styled.View``;

export const StyledTextInput: ComponentType<any> = styled(TextInput)`
  width: 95%;
`;
