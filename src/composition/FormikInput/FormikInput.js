/* eslint-disable sonarjs/cognitive-complexity */

// React modules
import React, {useState, useEffect} from 'react';

// Utils
import {testProps} from '@utils/componentCustomProps';

// Styles
import {StyledTextInput} from './Styled';

// Types
import type {PropsType} from './types';

const FormikInput = (props: PropsType): React$Node => {
  const {
    name,
    label,
    placeholderText,
    description = '',
    handleInputChange,
    handleResetField,
    handleSetFieldTouched,
    handleOnFocus,
    handleOnBlur,
    error,
    isTouched,
    isValid,
    testID,
  } = props;
  const [inputState, setInputState] = useState('default');
  const [showPlaceholder, setShowPlaceholder] = useState(true);
  const [helperText, setHelperText] = useState('');

  useEffect(() => {
    if (error && isTouched) {
      setHelperText(error);
    } else {
      setHelperText(description || '');
    }
  }, [error, description, isTouched, isValid]);

  useEffect(() => {
    if (error && isTouched) {
      setInputState('error');
    }

    if (isValid && !error) {
      setInputState('correct');
    }
  }, [error, isTouched, isValid]);

  return (
    <StyledTextInput
      {...testProps(testID)}
      label={label}
      state={inputState}
      placeholder={showPlaceholder ? placeholderText : ''}
      onChangeText={(text) => {
        handleInputChange(name)(text);
      }}
      helperText={helperText}
      value={{name}}
      handleClear={() => {
        handleResetField(name);
      }}
      onFocus={() => {
        setInputState('active');
        setHelperText(description || '');
        setShowPlaceholder(false);
        handleOnFocus && handleOnFocus();
      }}
      onBlur={() => {
        setShowPlaceholder(true);
        setHelperText(error || description || '');
        if (error && isTouched) {
          setInputState('error');
          setHelperText(error);
        } else {
          handleSetFieldTouched(name);
          if (isValid) {
            setInputState('correct');
          } else {
            setInputState('default');
          }
        }
        handleOnBlur && handleOnBlur();
      }}
      {...props}
    />
  );
};

export default FormikInput;
