import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import FormikInput from '../FormikInput';
import {testProps} from '@utils/componentCustomProps';

describe('FormikInput', () => {
  const mockFunction = () => jest.fn();
  const mockDataName = 'Juan De La Cruz';

  it('Should render as expected', () => {
    const all = render(
      <FormikInput
        name="name"
        label="Name"
        placeholderText="Input Name"
        handleInputChange={mockFunction}
        handleResetField={mockFunction}
        handleSetFieldTouched={mockFunction}
        handleOnFocus={mockFunction}
        handleOnBlur={mockFunction}
        {...testProps('formikInputTestId')}
      />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });

  it('Should be able to handle input change', () => {
    const {getByTestId} = render(
      <FormikInput
        name="name"
        label="Name"
        placeholderText="Input Name"
        handleInputChange={mockFunction}
        handleResetField={mockFunction}
        handleSetFieldTouched={mockFunction}
        handleOnFocus={mockFunction}
        handleOnBlur={mockFunction}
        isTouched={mockFunction}
        {...testProps('formikInputTestId')}
      />,
    );
    const nameFormikInput = getByTestId('formikInputTestId');
    fireEvent(nameFormikInput, 'focus');
    fireEvent(nameFormikInput, 'onChangeText', mockDataName);
    fireEvent(nameFormikInput, 'blur');
  });

  it('Should be able to show correct icon if valid', () => {
    const {getByTestId} = render(
      <FormikInput
        name="name"
        label="Name"
        placeholderText="Input Name"
        handleInputChange={mockFunction}
        handleResetField={mockFunction}
        handleSetFieldTouched={mockFunction}
        handleOnFocus={mockFunction}
        handleOnBlur={mockFunction}
        isTouched={mockFunction}
        isValid={true}
        {...testProps('formikInputTestId')}
      />,
    );
    const nameFormikInput = getByTestId('formikInputTestId');
    fireEvent(nameFormikInput, 'focus');
    fireEvent(nameFormikInput, 'onChangeText', mockDataName);
    fireEvent(nameFormikInput, 'blur');
  });

  it('Should be able to clear the input', () => {
    const {getByTestId} = render(
      <FormikInput
        name="name"
        label="Name"
        placeholderText="Input Name"
        handleInputChange={mockFunction}
        handleResetField={mockFunction}
        handleSetFieldTouched={mockFunction}
        handleOnFocus={mockFunction}
        handleOnBlur={mockFunction}
        isTouched={mockFunction}
        {...testProps('formikInputTestId')}
      />,
    );
    const nameFormikInput = getByTestId('formikInputTestId');
    fireEvent(nameFormikInput, 'focus');
    fireEvent(nameFormikInput, 'onChangeText', mockDataName);
    const clearIcon = getByTestId('formikInputTestIdStatusIconButton_active');
    fireEvent.press(clearIcon);
  });

  it('Should be able to show errors if any', () => {
    const {getByTestId} = render(
      <FormikInput
        name="name"
        label="Name"
        placeholderText="Input Name"
        handleInputChange={mockFunction}
        handleResetField={mockFunction}
        handleSetFieldTouched={mockFunction}
        handleOnFocus={mockFunction}
        handleOnBlur={mockFunction}
        isTouched={mockFunction}
        isValid={false}
        error={'This field is required!'}
        {...testProps('formikInputTestId')}
      />,
    );
    const nameFormikInput = getByTestId('formikInputTestId');
    fireEvent(nameFormikInput, 'focus');
    fireEvent(nameFormikInput, 'blur');
  });
});
