/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';

// Third party libraries
import {Divider} from 'react-native-elements';

// Utils
import styled from 'styled-components/native';
import {moderateScale, scale, verticalScale} from '@utils/scale';

export const MainContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.displayOptionsTooltip.tooltip};
  margin-left: ${scale(6)}px;
`;

export const DisplayOptionsText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(10)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  line-height: ${scale(12)}px;
  font-weight: bold;
  letter-spacing: ${scale(0.846154)}px;
  text-align: left;
  text-transform: uppercase;
`;

export const CheckboxContainer: ComponentType<any> = styled.View`
  flex-direction: column;
  margin-top: ${verticalScale(5)}px;
  margin-bottom: ${verticalScale(10)}px;
  max-height: ${verticalScale(100)}px;
  margin-left: ${scale(-9)}px;
`;

export const StyledDivider: ComponentType<any> = styled(Divider)`
  background-color: ${(props) => props.theme.displayOptionsTooltip.divider};
  margin-top: ${verticalScale(5)}px;
  margin-bottom: ${verticalScale(16)}px;
  height: ${verticalScale(1)}px;
`;

export const ActionText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(14)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
  padding-left: ${scale(11.5)}px;
`;

export const StyledTouchableOpacity: ComponentType<any> = styled.View.attrs(
  (props) => ({
    marginTop: props.removeWidget ? verticalScale(20) : 0,
    marginBottom: props.removeWidget ? verticalScale(16) : 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: scale(3),
  }),
)``;
