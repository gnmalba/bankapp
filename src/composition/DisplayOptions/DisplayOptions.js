/**
 * @format
 * @flow
 */

// React modules
import React from 'react';
import {TouchableOpacity, View} from 'react-native';

// Third party libraries

// Components
import CheckboxComponent from '@components/CheckboxComponent';
import IconTooltip from '@components/IconTooltip';

// Utils
import type {PropsType} from './types';
import * as S from './Styled';
import styled from 'styled-components/native';
import {testProps} from '@utils/componentCustomProps';

const StyledComponent = styled(CheckboxComponent).attrs((props) => ({
  checkedColor: props.theme.color.SECONDARY_DEFAULT,
  uncheckedColor: props.theme.color.TEXT_100_HIGH,
}))``;

const StyledIcon = styled(IconTooltip).attrs((props) => ({
  color: props.theme.color.TEXT_300_HIGH,
}))``;

const DisplayOptions = (props: PropsType): React$Node => {
  const {
    widgetFlag,
    options,
    selectedOption,
    theme,
    testID,
    onMovePress,
    onRemovePress,
  } = props;

  const [selectedIndex, setSelectedIndex] = React.useState(selectedOption);

  return (
    <>
      <S.MainContainer>
        <View>
          <S.DisplayOptionsText>Display Options</S.DisplayOptionsText>
        </View>

        <S.CheckboxContainer>
          {widgetFlag !== 'Shortcuts' &&
            options?.map((item, idx) => {
              return (
                <TouchableOpacity
                  key={idx}
                  disabled={item.disabled}
                  activeOpacity={1}>
                  <StyledComponent
                    {...testProps(
                      (testID || 'displayOptionsComponent') + `Btn${idx}`,
                    )}
                    disabled={item.disabled}
                    onPress={(value, place) => {
                      setSelectedIndex(place);
                    }}
                    value={item.value}
                    index={item.index}
                    size={item.size}
                    title={item.title}
                    checkedIcon={item.checkedIcon}
                    uncheckedIcon={item.uncheckedIcon}
                    isChecked={idx === selectedIndex}
                    theme={theme}
                  />
                </TouchableOpacity>
              );
            })}
        </S.CheckboxContainer>

        <S.StyledDivider />

        <View>
          <TouchableOpacity
            onPress={onMovePress}
            {...testProps((testID || 'displayOptionsIcon') + 'Move')}>
            <S.StyledTouchableOpacity>
              <StyledIcon
                height={15}
                width={15}
                source={require('@assets/icons/move-icon.png')}
                theme={theme}
              />
              <S.ActionText>Move</S.ActionText>
            </S.StyledTouchableOpacity>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => onRemovePress(widgetFlag)}
            {...testProps((testID || 'displayOptionsIcon') + 'Remove')}>
            <S.StyledTouchableOpacity removeWidget={true}>
              <StyledIcon
                height={15}
                width={15}
                source={require('@assets/icons/remove-icon.png')}
                color={'#FFFFFF'}
                theme={theme}
              />
              <S.ActionText>Remove Widget</S.ActionText>
            </S.StyledTouchableOpacity>
          </TouchableOpacity>
        </View>
      </S.MainContainer>
    </>
  );
};

export default DisplayOptions;
