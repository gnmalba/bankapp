/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  widgetFlag: string,
  options: any,
  selectedOption: any,
  theme?: Object,
  onMovePress: () => void,
  onRemovePress: (string) => void,
};
