import IconTooltip from '@components/IconTooltip';
import React from 'react';
import styled from 'styled-components';

const checkedIcon = require('@assets/icons/active-circle-icon.png');
const uncheckedIcon = require('@assets/icons/inactive-circle-icon.png');
const squareChecked = require('@assets/icons/square-checked.png');
const squareUnchecked = require('@assets/icons/square-unchecked.png');

const CheckedIcon = styled(IconTooltip).attrs((props) => ({
  color: props.theme.color.SECONDARY_DEFAULT,
}))``;

const UncheckedIcon = styled(IconTooltip).attrs((props) => ({
  color: props.theme.color.TEXT_100_HIGH,
}))``;

export const CIRCLE_CHECKED = (
  <CheckedIcon source={checkedIcon} height={23} width={23} />
);
export const CIRCLE_UNCHECKED = (
  <UncheckedIcon
    source={uncheckedIcon}
    height={23}
    width={23}
    color={'#A2BDDB'}
  />
);

export const CHECKBOX = (
  <CheckedIcon
    source={squareChecked}
    height={16}
    width={16}
    color={'#09BAD0'}
  />
);

export const SQUARE_OUTLINE = (
  <UncheckedIcon
    source={squareUnchecked}
    height={16}
    width={16}
    color={'#A2BDDB'}
  />
);

export const daysOptions = [
  {
    title: 'Last 5 transactions',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    index: 0,
    value: 5,
  },
  {
    title: 'Last 10 transactions',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    index: 1,
    value: 10,
  },
  {
    title: 'Last 30 transactions',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    index: 2,
    value: 30,
  },
];

export const transactionOptions = [
  {
    title: 'Last 5 transactions',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    index: 0,
    value: 5,
    isChecked: false,
  },
  {
    title: 'Last 10 transactions',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    index: 1,
    value: 10,
    isChecked: false,
  },
  {
    title: 'Last 30 transactions',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    index: 2,
    value: 30,
    isChecked: false,
  },
];

export const accountOptions = [
  {
    title: 'Peso Accounts',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    value: 'PHP',
    index: 0,
    isChecked: false,
    disabled: false,
  },
  {
    title: 'Foreign Accounts',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    value: 'FOREIGN',
    index: 1,
    isChecked: false,
    disabled: false,
  },
  {
    title: 'All Accounts',
    size: 18,
    checkedIcon: CIRCLE_CHECKED,
    uncheckedIcon: CIRCLE_UNCHECKED,
    value: null,
    index: 2,
    isChecked: false,
    disabled: false,
  },
];

export const actionOptions = [
  {
    title: 'Pay Bills',
    name: 'Pay Bills',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 0,
    isChecked: true,
  },
  {
    title: 'Send Money',
    name: 'Send Money',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 1,
    isChecked: true,
    code: 'SEND_MONEY',
    forShortcut: true,
  },
  {
    title: 'Buy Load',
    name: 'Buy Load',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 2,
    isChecked: true,
    code: 'LOAD_PREPAID',
    forShortcut: true,
  },
  {
    title: 'Order Checkbook',
    name: 'Order Checkbook',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 3,
    isChecked: true,
    code: 'ORDER_CHECKBOOK',
    forShortcut: true,
  },
  {
    title: 'Open Time Deposit',
    name: 'Open Time Deposit',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 4,
    isChecked: true,
    code: 'ONLINE_TD',
    forShortcut: true,
  },
  {
    title: 'Send via QR Code',
    name: 'Send via QR Code',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 5,
    isChecked: true,
    code: 'QR_CODE',
    forShortcut: true,
  },
];

export const installmentOptions = [
  {
    title: 'Convert to Cash',
    name: 'Convert to Cash',
    screen: 'Cash2Go',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 0,
    isChecked: false,
    code: 'CASH2GO INSTL',
  },
  {
    title: 'Convert Balance',
    name: 'Convert Balance',
    screen: 'BalanceConversion',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 1,
    isChecked: false,
    code: 'BALANCE CONVERSION',
  },
  {
    title: 'Transfer Balance',
    name: 'Transfer Balance',
    screen: 'BalanceTransfer',
    size: 18,
    checkedIcon: CHECKBOX,
    uncheckedIcon: SQUARE_OUTLINE,
    iconType: 'ionicon',
    index: 2,
    isChecked: false,
    code: 'BALANCE TRANSFER',
  },
];
