import React from 'react';
import {render} from '@testing-library/react-native';
import {CIRCLE_CHECKED, CIRCLE_UNCHECKED} from '../utils/constants';
import {ThemeProvider} from 'styled-components';
import {DARK_THEME} from '@utils/constants';

describe('Constantc', () => {
  it.each([CIRCLE_CHECKED, CIRCLE_UNCHECKED])('Should work', (comp) => {
    render(<ThemeProvider theme={DARK_THEME}>{comp}</ThemeProvider>);
  });
});
