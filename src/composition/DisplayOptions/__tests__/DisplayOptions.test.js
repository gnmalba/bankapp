/**
 * @flow
 */

import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import DisplayOptions from '../DisplayOptions';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';

const options = [
  {
    disabled: true,
    value: 'Test',
    index: 0,
    size: 2,
    title: 'test',
    checkedIcon: true,
    uncheckedIcon: false,
  },
];

const defaultProps = {
  widgetFlag: 'test widget flag',
  testID: 'displayOptionsTestId',
  options,
  selectedOption: {},
  onMovePress: () => {},
  onRemovePress: () => {},
};

describe('DisplayOptions', () => {
  it.each(['displayOptionsTestId'])('Should work as expected', (testID) => {
    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <DisplayOptions {...defaultProps} />
        </ThemeProvider>
      </MockProvider>,
    );
    all.debug();
    if (testID) {
      const el1 = all.getByTestId(`${testID}Move`);
      fireEvent(el1, 'onPress');
      const el2 = all.getByTestId(`${testID}Remove`);
      fireEvent(el2, 'onPress');
      const el3 = all.getByTestId(`${testID}Btn0`);
      fireEvent(el3, 'onPress');
    }
    expect(all.toJSON()).toMatchSnapshot();
  });
});
