/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import CircularProgressBar from '@composition/CircularProgressBar';
import Text from '@components/Text';
import {numberFormat} from '@utils/accountNumber';
import {Pressable, View} from 'react-native';
import EditIcon from '@assets/icons/pencil.png';
import {testProps} from '@utils/componentCustomProps';
import {getCreditPercentage} from '@utils/credit';

const CreditCardDetails = (props: PropsType): React$Node => {
  const {theme, data} = props;
  const currency = data?.currency || '';
  const percentage = getCreditPercentage(
    data?.creditLimit,
    data?.availableCredit !== undefined
      ? data?.availableCredit
      : data?.availableBalance,
    data?.flag,
  );
  const creditLimit = `${currency} ${numberFormat(data?.creditLimit)}`;
  const availableBalance = `${currency} ${numberFormat(
    data?.availableBalance,
  )}`;

  return (
    <S.Container>
      <S.CardContainer theme={theme}>
        <S.RowContainer>
          <S.DetailsContainer>
            <S.StyledLabel
              {...testProps('creditCardDetailsAvailableCreditLabel')}>
              Available Credit
            </S.StyledLabel>
            <Text
              mode="body1"
              {...testProps('creditCardDetailsAvailableCreditValue')}>
              {availableBalance}
            </Text>
            <S.DividerContainer {...testProps('creditCardDetailsDivider')} />
            <S.StyledLabel {...testProps('creditCardDetailsCreditLimitLabel')}>
              Credit limit
            </S.StyledLabel>
            <Text
              mode="body1"
              {...testProps('creditCardDetailsCreditLimitValue')}>
              {creditLimit}
            </Text>
          </S.DetailsContainer>

          <S.flexContainer />

          <S.DetailsContainer>
            <CircularProgressBar
              theme={theme}
              value={percentage}
              {...testProps('creditCardDetailsUsage')}
            />
          </S.DetailsContainer>
        </S.RowContainer>
        <View>
          <Pressable>
            <S.StatusContainer>
              <S.IconImage height={12} width={12} source={EditIcon} />
              <Text mode="link" themeColor="SECONDARY_DEFAULT">
                &nbsp; Increase credit limit
              </Text>
            </S.StatusContainer>
          </Pressable>
        </View>
      </S.CardContainer>
    </S.Container>
  );
};

export default CreditCardDetails;
