import React from 'react';
import {render} from '@testing-library/react-native';
import CreditCardDetails from '../CreditCardDetails';
import {testProps} from '@utils/componentCustomProps';

describe('CreditCardDetails', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <CreditCardDetails {...testProps('creditCardDetailsTestId')} />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
