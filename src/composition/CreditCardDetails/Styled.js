/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';
import {Image} from 'react-native';
import Text from '@components/Text';

export const Container: ComponentType<any> = styled.View``;

export const RowContainer: ComponentType<any> = styled.View`
  flex-direction: row;
`;

export const CardContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: ${scale(8)}px;
  padding: ${scale(16)}px;
  flex-direction: column;
  margin-top: ${verticalScale(32)}px;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  flex-direction: column;
`;

export const flexContainer: ComponentType<any> = styled.View`
  flex: 1;
`;

export const DividerContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(15)}px;
`;

export const IconImage: ComponentType<any> = styled(Image).attrs((props) => ({
  source: props.source,
  resizeMode: 'contain',
}))`
  height: ${(props) => verticalScale(props.height)}px;
  width: ${(props) => scale(props.width)}px;
`;

export const StatusContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-self: center;
  align-items: center;
  margin-top: ${verticalScale(24)}px;
  flex-direction: row;
`;

export const StyledLabel: ComponentType<any> = styled(Text).attrs((props) => ({
  themeColor: 'TEXT_100_HIGH',
  mode: 'overline1',
}))`
  margin-bottom: ${verticalScale(4)}px;
`;
