## CreditCardDetails
CreditCardDetails component.

# Usage
```js
import CreditCardDetails from '@composition/CreditCardDetails';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
