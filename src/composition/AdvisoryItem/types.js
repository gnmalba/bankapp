/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
  title?: string,
  body?: string,
  type?: string,
  date?: string,
  navigation?: {
    navigate: (screen: string, params?: Object) => void,
  },
};
