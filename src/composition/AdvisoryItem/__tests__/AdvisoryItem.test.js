import React from 'react';
import {render} from '@testing-library/react-native';
import AdvisoryItem from '../AdvisoryItem';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('AdvisoryItem', () => {
  it('Should work as expected', () => {
    const all = render(
      <ThemeProvider theme={DARK_THEME}>
        <AdvisoryItem />
      </ThemeProvider>,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
