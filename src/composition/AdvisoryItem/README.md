## AdvisoryItem
AdvisoryItem component.

# Usage
```js
import AdvisoryItem from '@composition/AdvisoryItem';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
