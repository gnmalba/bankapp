/**
 * @format
 * @flow
 */

import React from 'react';
import moment from 'moment';
import {useNavigation} from '@react-navigation/native';

import Text from '@components/Text';
import {scale} from '@utils/scale';

import type {PropsType} from './types';
import {
  Main,
  Container,
  IconContainer,
  Icon,
  NotificationContent,
  Flex1,
  DateFeather,
  DateContainer,
  StyledFeather,
  Title,
} from './Styled';

import {
  typeImageMap,
  SANITIZE_BODY_REGEX,
  IMPORTANT,
  HOUR,
  NO_DATE,
} from '@screens/public/Advisory/config';

const AdvisoryItem = (props: PropsType): React$Node => {
  const {title, body, type, date} = props;
  const description = body?.replace(SANITIZE_BODY_REGEX, '');
  const navigation = useNavigation();

  const handleNavigate = () => {
    navigation.navigate('AdvisoryDetail', {
      title,
      body,
      type,
    });
  };

  return (
    <Main onPress={handleNavigate}>
      <Container>
        <IconContainer>
          <Icon source={typeImageMap[type || 'announcement']} />
        </IconContainer>
        <Flex1>
          <NotificationContent>
            <DateFeather>
              <DateContainer>
                <Text mode="overline2" themeColor="TEXT_100_HIGH">
                  {date
                    ? moment(date).startOf(HOUR).fromNow().toUpperCase()
                    : NO_DATE}
                </Text>
              </DateContainer>
              <StyledFeather />
            </DateFeather>
            <Title>
              <Text
                mode="body2"
                themeColor={
                  type === IMPORTANT ? 'TEXT_300_HIGH' : 'TEXT_100_HIGH'
                }>
                {title}
              </Text>
            </Title>
          </NotificationContent>
          <Text
            mode="body2"
            themeColor={
              type === IMPORTANT ? 'TEXT_300_HIGH' : 'TEXT_200_MEDIUIM'
            }>
            {description?.substr(0, scale(70))}...
          </Text>
        </Flex1>
      </Container>
    </Main>
  );
};

export default AdvisoryItem;
