/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {verticalScale, scale, moderateScale} from '@utils/scale';
import Feather from 'react-native-vector-icons/Feather';

export const Main: ComponentType<any> = styled.TouchableOpacity``;

export const Container: ComponentType<any> = styled.View`
  flex-direction: row;
  min-height: ${moderateScale(100)}px;
  border-bottom-width: 0.5px;
  padding-top: ${verticalScale(10)}px;
  border-color: ${(props) => props.theme?.color?.TEXT_300_LOW || 'transparent'};
`;

export const IconContainer: ComponentType<any> = styled.View`
  margin-right: 20px;
  padding-left: ${scale(6)}px;
  padding-top: ${scale(6)}px;
  width: ${scale(32)}px;
  height: ${scale(32)}px;
  border-radius: ${scale(32)}px;
  background-color: #153961;
`;

export const Icon: ComponentType<any> = styled.Image.attrs((props) => ({
  source: props.source,
}))`
  width: ${scale(20)}px;
  height: ${scale(20)}px;
`;

export const Flex1: ComponentType<any> = styled.View`
  flex: 1;
`;

export const NotificationContent: ComponentType<any> = styled.View`
  width: 100%;
  flex-direction: row-reverse;
  justify-content: space-between;
  align-items: center;
`;

export const Title: ComponentType<any> = styled.View`
  padding-right: 10px;
  max-width: 70%;
`;

export const DateFeather: ComponentType<any> = styled.View`
  flex: 1 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const DateContainer: ComponentType<any> = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
`;

export const StyledFeather: ComponentType<any> = styled(Feather).attrs(
  (props) => ({
    name: 'chevron-right',
    size: moderateScale(27),
    color: props.theme.color.TEXT_300_LOW,
  }),
)`
  padding-top: 3px;
  margin-right: -5px;
`;
