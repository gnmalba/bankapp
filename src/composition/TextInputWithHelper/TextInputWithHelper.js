/**
 * @format
 * @flow
 */

import React from 'react';
import Text from '@components/Text';
import LabelledTextInput from '@composition/LabelledTextInput';

import type {PropsType} from './types';
import {HelperContainer} from './styled';

import {testProps} from '@utils/componentCustomProps';

const TextInputWithHelper = (props: PropsType): React$Node => {
  const {helperText, ...rest} = props;
  const {theme, state} = rest;
  const helperTextColor = theme?.textInput?.helperTextColor[state || 'default'];
  return (
    <>
      <LabelledTextInput {...rest} />
      {helperText && (
        <HelperContainer>
          <Text
            mode="body2"
            {...testProps('inputHelperTextTestId')}
            color={helperTextColor}>
            {helperText}
          </Text>
        </HelperContainer>
      )}
    </>
  );
};

export default TextInputWithHelper;
