import React from 'react';
import {render} from '@testing-library/react-native';
import TextInputWithHelper from '../TextInputWithHelper';
import {DARK_THEME} from '@utils/constants';

describe('TextInputWithHelper', () => {
  it.each(['helper text', undefined])(
    'Should work as expected',
    (helperText) => {
      const all = render(
        <TextInputWithHelper helperText={helperText} theme={DARK_THEME} />,
      );
      expect(all.toJSON()).toMatchSnapshot();
    },
  );
});
