/**
 * @flow
 */

// Uncomment this line to start using styled components.
import styled from 'styled-components/native';
import type {ComponentType} from 'react';
import {moderateScale} from '@utils/scale';

export const HelperContainer: ComponentType<any> = styled.View`
  padding: ${moderateScale(8)}px 0px 0px 0px;
`;
