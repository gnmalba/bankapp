/**
 * @format
 * @flow
 */

import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {TouchableOpacity, View} from 'react-native';

import Checkbox from '@components/Checkbox';
import {BackIcon} from '@components/Header/Styled';

import type {PropsType} from './types';
import * as S from './Styled';
import CustomizeCheckBoxItem from './CustomizeCheckboxItem';
import {
  addWidget,
  removeWidget,
  moveWidget,
  updateWidgetsPlacement,
} from '@ducks/config/actions';

import {testProps} from '@utils/componentCustomProps';

import {
  getActiveWidgetsForCustomization,
  getInActiveWidgetsForCustomization,
  getCheckedWidgetsForCustomization,
} from '@ducks/config/selectors';

import {toggleCustomizeWidget} from '@ducks/dashboard/actions';

const CustomizeDashboard = ({testID, navigation}: PropsType): React$Node => {
  const dispatch = useDispatch();

  const checkedWidgetsForCustomization = useSelector(
    getCheckedWidgetsForCustomization,
  );
  const activeWidgets = useSelector(getActiveWidgetsForCustomization);
  const inactiveWidgets = useSelector(getInActiveWidgetsForCustomization);

  const handleRemoveWidget = (ref: number) => dispatch(removeWidget(ref));

  const handleAddWidget = (ref: number) => dispatch(addWidget(ref));

  const handleMoveWidget = (ref: number, direction: 'up' | 'down') =>
    dispatch(moveWidget(ref, direction));

  return (
    <>
      <S.MainContainer>
        <S.Container2 {...testProps('CustomizeWidgetsModalId')}>
          <S.HeaderContainer>
            <TouchableOpacity
              onPress={() => {
                dispatch({type: 'RESET_CUSOMIZATION_WIDGETS'});
                dispatch(toggleCustomizeWidget());
                navigation.goBack();
              }}
              {...testProps((testID || 'customizeDashboard') + 'BackIcon')}>
              <BackIcon />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                dispatch(
                  updateWidgetsPlacement(checkedWidgetsForCustomization),
                );
                dispatch(toggleCustomizeWidget());
                navigation.goBack();
              }}
              {...testProps((testID || 'customizeDashboard') + 'DoneButton')}>
              <S.StyledDoneButton>Done</S.StyledDoneButton>
            </TouchableOpacity>
          </S.HeaderContainer>

          <View>
            <View>
              <S.TitleText {...testProps('CustomizeDashboardTitle')}>
                Customize your dashboard
              </S.TitleText>
              <S.SubTitleText {...testProps('CustomizeDashboardSubtitle')}>
                Add, remove or move widgets
              </S.SubTitleText>
            </View>

            <S.ListOfCheckBox>
              {activeWidgets.map((widget, index) => (
                <CustomizeCheckBoxItem
                  {...testProps(
                    (testID || 'customizeDashboard') + `Item${index}`,
                  )}
                  key={`active-${widget.ref}-${widget.label}`}
                  label={widget.label}
                  disabled={widget.ref === 1 || widget.loading}
                  lastItem={index === activeWidgets.length - 1}
                  onPressCheckbox={() => handleRemoveWidget(widget.ref)}
                  onUpPress={() => handleMoveWidget(widget.ref, 'up')}
                  onDownPress={() => handleMoveWidget(widget.ref, 'down')}
                />
              ))}
            </S.ListOfCheckBox>

            <S.MoreSectionContainer>
              {inactiveWidgets.length > 0 && (
                <>
                  <S.MoreWidgetsText
                    {...testProps('customizeWidgetsMoreWidgetsId')}>
                    MORE WIDGETS
                  </S.MoreWidgetsText>
                  {inactiveWidgets.map((widget, index) => (
                    <Checkbox
                      key={`inactive-${widget.ref}-${widget.label}`}
                      disabled={widget.loading}
                      title={widget.label}
                      onPress={() => handleAddWidget(widget.ref)}
                      checked={false}
                      isCheckbox={true}
                      {...testProps(
                        (testID || 'customizeWidgets') + `Item${index}`,
                      )}
                    />
                  ))}
                </>
              )}
            </S.MoreSectionContainer>
          </View>
        </S.Container2>
      </S.MainContainer>
    </>
  );
};

export default CustomizeDashboard;
