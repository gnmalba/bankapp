/**
 * @flow
 */

// React modules
import type {ComponentType} from 'react';
import {Image} from 'react-native';

// Third party libraries
import styled from 'styled-components/native';
import Text from '@components/Text';

// Utils
import {verticalScale, scale, moderateScale} from '@utils/scale';

export const MainContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
  flex: 1;
`;

export const Container2: ComponentType<any> = styled.View`
  /* margin-top: ${verticalScale(45)}px; */
  shadow-color: #000;
  width: 100%;
  height: 95%;
  padding: ${scale(24)}px;
  border-radius: 4px;
  background-color: ${(props) => props.theme.color.BACKGROUND_100};
`;

export const HeaderContainer: ComponentType<any> = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${verticalScale(12)}px;
  align-items: center;
`;

export const ListOfCheckBox: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(16)}px;
  align-items: flex-start;
  flex-direction: column;
`;

export const MoreSectionContainer: ComponentType<any> = styled.View`
  margin-top: ${verticalScale(12)}px;
`;

export const TitleText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(20)}px;
  margin-bottom: ${verticalScale(8)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

export const MoreWidgetsText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(12)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
  font-weight: bold;
  margin-bottom: ${verticalScale(17)}px;
`;

export const SubTitleText: ComponentType<any> = styled.Text`
  font-size: ${moderateScale(12)}px;
  color: ${(props) => props.theme.color.TEXT_100_HIGH};
`;

export const BackIcon: any = styled(Image).attrs((props): any => ({
  source: require('@assets/icons/back-icon.png'),
}))`
  height: ${verticalScale(15)}px;
  width: ${scale(17)}px;
`;

export const StyledDoneButton: ComponentType<any> = styled(Text).attrs({
  themeColor: 'TEXT_300_HIGH',
  mode: 'subtitle',
})`
  font-family: Myriad Pro;
`;
