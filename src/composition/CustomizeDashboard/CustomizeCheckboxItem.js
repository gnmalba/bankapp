// @flow

import React from 'react';
import {Pressable, View} from 'react-native';

import type {ComponentType} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styled from 'styled-components';
import {useTheme} from 'styled-components/native';

import Checkbox from '@components/Checkbox';
import {scale, verticalScale} from '@utils/scale';
import {testProps} from '@utils/componentCustomProps';

export type PropTypes = {
  label: Object,
  onUpPress: () => void,
  onDownPress: () => void,
  onPressCheckbox: () => void,
  lastItem?: boolean,
  disabled?: boolean,
  testID?: string,
};

export const CheckboxContainer: ComponentType<any> = styled(View)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const ActionContainer: ComponentType<any> = styled(View)`
  display: flex;
  flex-direction: row;
  align-items: center;
  align-content: center;
  justify-content: center;
  padding-bottom: ${scale(8)}px;
`;

const Divider: ComponentType<any> = styled(View)`
  background-color: ${(props) => props.theme.card.dividerBgColor};
  height: ${scale(1)}px;
  margin-bottom: ${verticalScale(17)}px
  width: 100%;
`;

const CustomizeCheckBoxItem = (props: PropTypes): React$Node => {
  const theme = useTheme();
  const {
    label,
    onUpPress,
    onDownPress,
    onPressCheckbox,
    lastItem,
    disabled,
    testID,
  } = props;

  const handlePressCheckbox = () => {
    if (onPressCheckbox) {
      onPressCheckbox();
    }
  };

  const handleUpPress = () => {
    if (onUpPress) {
      onUpPress();
    }
  };

  const handleDownPress = () => {
    if (onDownPress) {
      onDownPress();
    }
  };

  return (
    <>
      <CheckboxContainer>
        <Pressable>
          <Checkbox
            {...testProps((testID || 'customizeDashboard') + 'Checkbox')}
            key={`active-${label}`}
            title={label}
            onPress={handlePressCheckbox}
            checked={true} // always true
            isCheckbox={true}
            disabled={disabled}
          />
        </Pressable>

        <ActionContainer>
          <View style={{marginRight: scale(15)}}>
            <Pressable
              onPress={handleUpPress}
              {...testProps((testID || 'customizeCheckboxIcon') + 'UpButton')}>
              <Ionicons
                name="caret-up"
                size={26}
                color={theme.color.HIGH_EMPHASIS}
              />
            </Pressable>
          </View>
          <Pressable
            onPress={handleDownPress}
            {...testProps((testID || 'customizeCheckboxIcon') + 'DownButton')}>
            <Ionicons
              name="caret-down"
              size={26}
              color={theme.color.HIGH_EMPHASIS}
            />
          </Pressable>
        </ActionContainer>
      </CheckboxContainer>

      {!lastItem && (
        <Divider
          theme={theme}
          {...testProps((testID || 'customizeCheckboxIcon') + 'Divider')}
        />
      )}
    </>
  );
};

export default CustomizeCheckBoxItem;
