## CustomizeDashboard
CustomizeDashboard component.

# Usage
```js
import CustomizeDashboard from '@composition/CustomizeDashboard';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
