import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CustomizeCheckBoxItem from '../CustomizeCheckboxItem';
import {testProps} from '@utils/componentCustomProps';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

describe('CustomizeDashboard', () => {
  it.each(['customizeDashboardItem', ''])(
    'Should work as expected',
    (testID) => {
      // Edit the args inside render and provide props.
      const all = render(
        <ThemeProvider theme={DARK_THEME}>
          <CustomizeCheckBoxItem
            {...testProps(testID)}
            onDownPress={testID !== '' ? () => {} : undefined}
            onUpPress={testID !== '' ? () => {} : undefined}
          />
        </ThemeProvider>,
      );
      all.debug();
      const upBtn = all.getByTestId(
        `${testID || 'customizeCheckboxIcon'}UpButton`,
      );
      fireEvent(upBtn, 'onPress');
      const dwnBtn = all.getByTestId(
        `${testID || 'customizeCheckboxIcon'}DownButton`,
      );
      fireEvent(dwnBtn, 'onPress');
    },
  );
});
