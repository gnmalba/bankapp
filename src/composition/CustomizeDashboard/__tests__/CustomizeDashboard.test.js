/**
 * @flow
 */

import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CustomizeDashboard from '../CustomizeDashboard';
import MockProvider from '@utils/testFramework';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';

const store = {
  config: {
    theme: 'dark',
    widgets: [
      {
        ref: 1,
        label: 'Test Label A',
        loading: false,
      },
      {
        ref: 2,
        label: 'Test Label B',
        loading: false,
      },
    ],
    removedWidgets: [1, 2],
    checked: [],
    limitShown: false,
    customizedWidgets: [1, 2],
  },
};

const navigation = {
  goBack: () => {},
};

describe('CustomizeDashboard', () => {
  it.each(['customizeDashboard', ''])('Should work as expected', (testID) => {
    // Edit the args inside render and provide props.
    const all = render(
      <MockProvider store={store}>
        <ThemeProvider theme={DARK_THEME}>
          <CustomizeDashboard navigation={navigation} />
        </ThemeProvider>
      </MockProvider>,
    );
    if (testID) {
      const el1 = all.getByTestId(`${testID}BackIcon`);
      fireEvent(el1, 'onPress');
      const el2 = all.getByTestId(`${testID}DoneButton`);
      fireEvent(el2, 'onPress');
      const upBtn = all.getByTestId('customizeDashboardItem0UpButton');
      fireEvent(upBtn, 'onPress');
      const dwnBtn = all.getByTestId('customizeDashboardItem0DownButton');
      fireEvent(dwnBtn, 'onPress');
    }
  });
});
