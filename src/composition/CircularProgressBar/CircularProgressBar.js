/**
 * @format
 * @flow
 */

import React from 'react';

import type {PropsType} from './types';
import * as S from './Styled';
import {testProps} from '@utils/componentCustomProps';

const CircularProgressBar = (props: PropsType): React$Node => {
  const {theme, value = 0, size, testID = ''} = props;
  return (
    <S.CircularGraph size={size} fill={value}>
      {() => {
        return (
          <S.CircularContainer>
            <S.CircularFill theme={theme} {...testProps(testID + 'Value')}>
              {value}%
            </S.CircularFill>
            <S.CircularUsed theme={theme} {...testProps(testID + 'Used')}>
              USED
            </S.CircularUsed>
          </S.CircularContainer>
        );
      }}
    </S.CircularGraph>
  );
};

export default CircularProgressBar;
