## CircularProgressBar
CircularProgressBar component.

# Usage
```js
import CircularProgressBar from '@composition/CircularProgressBar';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
