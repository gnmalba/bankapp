/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {moderateScale} from '@utils/scale';
import Text from '@components/Text';

const DEFAULT_WIDTH = moderateScale(6);

export const CircularContainer: ComponentType<any> = styled.View`
  align-items: center;
`;

export const CircularGraph: ComponentType<any> = styled(
  AnimatedCircularProgress,
).attrs((props) => ({
  fill: props.fill,
  size: Math.round(moderateScale(props.size === 'large' ? 135 : 120)),
  width: props.width ? props.width : DEFAULT_WIDTH,
  backgroundWidth: props.width ? props.width - 1 : DEFAULT_WIDTH - 1,
  backgroundColor: props.theme.circularGraphBackground,
  arcSweepAngle: 280,
  rotation: 220,
  lineCap: 'round',
  tintColor: props.theme.defaultColor,
}))``;

export const CircularFill: ComponentType<any> = styled(Text).attrs((props) => {
  return {
    themeColor: 'TEXT_300_HIGH',
    mode: 'h2',
  };
})``;

export const CircularUsed: ComponentType<any> = styled(Text).attrs((props) => {
  return {
    themeColor: 'TEXT_100_HIGH',
    mode: 'overline1',
  };
})``;
