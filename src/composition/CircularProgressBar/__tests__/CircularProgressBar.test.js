import React from 'react';
import {render} from '@testing-library/react-native';
import CircularProgressBar from '../CircularProgressBar';
import {testProps} from '@utils/componentCustomProps';

describe('CircularProgressBar', () => {
  it('Should work as expected', () => {
    // Edit the args inside render and provide props.
    const all = render(
      <CircularProgressBar {...testProps('circularProgressBarTestId')} />,
    );
    expect(all.toJSON()).toMatchSnapshot();
  });
});
