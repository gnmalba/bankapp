/**
 * @flow
 */

import type {ComponentType} from 'react';
import styled from 'styled-components/native';
import {scale, verticalScale} from '@utils/scale';
import {
  TouchableOpacity,
  FlatList as RNFlatList,
  Image,
  Platform,
} from 'react-native';
import {Divider as RNEDivider} from 'react-native-elements';

export const MainContainer: ComponentType<any> = styled.View`
  flex: 1;
  margin-top: ${verticalScale(30)}px;
`;

export const CardContainer: ComponentType<any> = styled.View`
  background-color: ${(props) => props.theme.card.backgroundColor};
  border-radius: ${scale(8)}px;
  padding-vertical: ${verticalScale(20)}px;
  flex-direction: row;
`;

export const DetailsContainer: ComponentType<any> = styled.View`
  flex: ${(props) => props.flex};
  flex-direction: ${(props) => props.flexDirection};
  margin-right: ${scale(15)}px;
`;

export const InstallmentTouch: ComponentType<any> = styled(
  TouchableOpacity,
).attrs((props) => ({
  ...props,
  onPress: props.onPress,
  activeOpacity: 1,
}))`
  align-items: center;
`;

export const InstallmentFlatList: ComponentType<any> = styled(RNFlatList).attrs(
  (props) => ({
    ...Platform.select({
      ios: {
        alwaysBounceHorizontal: false,
        alwaysBounceVertical: false,
        bounces: false,
      },
      android: {
        overScrollMode: 'never',
      },
    }),
    contentContainerStyle: {
      marginTop: verticalScale(15),
      backgroundColor: 'transparent',
    },
    horizontal: true,
  }),
)`
  width: 100%;
`;

export const InstallmentDivider: ComponentType<any> = styled(RNEDivider)`
  background-color: transparent;
  margin-horizontal: ${scale(11)}px;
`;

export const InstallmentBorder: ComponentType<any> = styled.View`
  border: 1px solid #445870;
  margin-top: ${verticalScale(8)}px;
`;

export const InstallmentText: ComponentType<any> = styled.Text`
  font-family: 'MyriadPro-Semibold';
  font-style: normal;
  font-weight: 600;
  font-size: ${scale(20)}px;
  line-height: ${scale(24)}px;
  color: ${(props) => props.theme.color.TEXT_300_HIGH};
`;

const tinColorIconImage = (props) =>
  props.disabled
    ? props.theme.color.DISABLED_STATUS
    : props?.isRightIcon
    ? props.theme.card.availableBalance
    : undefined;

export const IconImage: ComponentType<any> = styled(Image).attrs((props) => ({
  source: props.source,
  resizeMode: 'contain',
}))`
  height: ${(props) => verticalScale(props.height)}px;
  width: ${(props) => scale(props.width)}px;
`;

export const DisabledIconImage: ComponentType<any> = styled(Image).attrs(
  (props) => ({
    source: props.source,
    resizeMode: 'contain',
    tintColor: tinColorIconImage(props), // For Android tintColor
    style: {
      tintColor: tinColorIconImage(props), // For iOS tintColor
    },
  }),
)`
  height: ${(props) => verticalScale(props.height)}px;
  width: ${(props) => scale(props.width)}px;
`;

export const ImageContainer: ComponentType<any> = styled.View`
  justify-content: center;
  margin-horizontal: ${scale(10)}px;
  flex: 2;
`;

export const StatusContainer: ComponentType<any> = styled.View`
  justify-content: center;
  align-self: center;
  align-items: center;
  margin-top: ${verticalScale(24)}px;
  flex-direction: row;
`;
