/**
 * @format
 * @flow
 */

import React, {useEffect} from 'react';

import type {PropsType, StyledIconImageType} from './types';
import * as S from './Styled';
import Text from '@components/Text';
import Circle from '@components/Circle';
import {
  CARD_INSTALLMENT_MAPPER,
  INSTALLMENT_SHORTCUT_CODES,
} from '@utils/constants';
import RightIcon from '@assets/icons/arrow-right.png';
import ViewStatusIcon from '@assets/icons/view-status-card.png';
import {Pressable, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {creditStateSelector} from '@ducks/credit/selectors';
import {getCardToDispatch} from '@utils/installments';
import {
  updateBalanceConversionFrom,
  updateBalanceTransferTo,
  updateCash2GoFrom,
  setSelectedCreditforInstallments,
} from '@ducks/installments/actions';
import {testProps} from '@utils/componentCustomProps';
import {getEnabledShortcutsByCode} from '@utils/accounts';
import {useNavigation} from '@react-navigation/core';

const StyledIconImage = ({
  height,
  width,
  source,
  disabled,
  isRightIcon,
  testId = '',
}: StyledIconImageType): React$Node => {
  return disabled ? (
    <S.DisabledIconImage
      height={height}
      width={width}
      source={source}
      isRightIcon={isRightIcon}
      disabled={disabled}
      {...testProps(testId)}
    />
  ) : (
    <S.IconImage
      height={height}
      width={width}
      source={source}
      {...testProps(testId)}
    />
  );
};

const CreditInstallment = (props: PropsType): React$Node => {
  const {theme} = props;

  const creditState = useSelector(creditStateSelector);
  const selectedCredit = creditState.selectedCredit;

  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setSelectedCreditforInstallments(selectedCredit));
  }, [dispatch, selectedCredit]);

  const enabledShortcuts = getEnabledShortcutsByCode(
    selectedCredit?.shortcuts || [],
    INSTALLMENT_SHORTCUT_CODES,
  ).map((shortcut) => shortcut.code);

  const handleInstallmentPress = (item) => {
    const currentCard = getCardToDispatch(
      creditState.creditList.data,
      creditState.creditDetails.data,
    );

    if (item.id === 1) {
      dispatch(updateCash2GoFrom(currentCard, false, false));
      navigation.navigate('Installments', {
        screen: 'Cash2GoScreen',
        params: {fromCC: true},
      });
    } else if (item.id === 2) {
      dispatch(updateBalanceConversionFrom(currentCard, false, false));
      navigation.navigate('Installments', {
        screen: 'BalanceConversionScreen',
        params: {fromCC: true},
      });
    } else if (item.id === 3) {
      dispatch(updateBalanceTransferTo(currentCard, false));
      navigation.navigate('Installments', {
        screen: 'BalanceTransferScreen',
        params: {fromCC: true},
      });
    }
  };

  const renderItem = ({item, index}) => {
    const uniqueIndex = index + 1;
    const disabled = !enabledShortcuts.includes(item.code);

    const titleTextColor = !disabled ? 'TEXT_300_HIGH' : 'DISABLED_STATUS';
    const descriptionTextColor = !disabled
      ? 'TEXT_100_HIGH'
      : 'DISABLED_STATUS';

    return (
      <TouchableOpacity
        onPress={() => handleInstallmentPress(item)}
        disabled={disabled}
        {...testProps('CardInstallmentFacilityItem' + uniqueIndex)}>
        <S.CardContainer theme={theme}>
          <S.DetailsContainer flexDirection="column">
            <Circle size={46}>
              <StyledIconImage
                height={29}
                width={27}
                source={item.icon}
                disabled={disabled}
                testId={`CardInstallmentFacilityIcon${uniqueIndex}`}
              />
            </Circle>
          </S.DetailsContainer>
          <S.DetailsContainer flex={2} flexDirection="column">
            <Text
              mode="subtitle"
              themeColor={titleTextColor}
              {...testProps('CardInstallmentFacilityTitle' + uniqueIndex)}>
              {item.title}
            </Text>
            <Text
              mode="caption"
              themeColor={descriptionTextColor}
              {...testProps(
                'CardInstallmentFacilityDescription' + uniqueIndex,
              )}>
              {item.description}
            </Text>
          </S.DetailsContainer>
          <S.DetailsContainer flexDirection="column">
            <S.ImageContainer>
              <StyledIconImage
                isRightIcon
                height={13}
                width={7}
                source={RightIcon}
                disabled={disabled}
                testId={`CardInstallmentFacilityRightChevron${uniqueIndex}`}
              />
            </S.ImageContainer>
          </S.DetailsContainer>
        </S.CardContainer>
      </TouchableOpacity>
    );
  };

  return (
    <S.MainContainer>
      <S.InstallmentText
        theme={theme}
        {...testProps('CardInstallmentFacility')}>
        Card Installment
      </S.InstallmentText>
      <S.InstallmentBorder
        {...testProps('CardInstallmentFacilityLabelDivider')}
      />
      <S.InstallmentFlatList
        data={CARD_INSTALLMENT_MAPPER}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        renderItem={(item) => renderItem(item)}
        keyExtractor={(item) => item.id.toString()}
        ItemSeparatorComponent={({leadingItem}) => <S.InstallmentDivider />}
      />
      <Pressable
        onPress={() =>
          navigation.navigate('Installments', {
            screen: 'ViewInstallmentsScreen',
            params: {fromCC: true},
          })
        }
        {...testProps('CardInstallmentFacilityDivider')}>
        <S.StatusContainer>
          <S.IconImage
            height={15}
            width={15}
            source={ViewStatusIcon}
            {...testProps('CardInstallmentFacilityViewStatusIcon')}
          />
          <Text
            mode="link"
            themeColor="SECONDARY_DEFAULT"
            {...testProps('CardInstallmentFacilityViewStatus')}>
            &nbsp; View Status
          </Text>
        </S.StatusContainer>
      </Pressable>
    </S.MainContainer>
  );
};

export default CreditInstallment;
