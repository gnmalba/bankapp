/**
 * @flow
 */

export type PropsType = {
  testID?: string,
  accessibilityLabel?: string,
  theme?: Object,
};

export type StyledIconImageType = {
  height: number,
  width: number,
  source: any,
  testId?: string,
  disabled?: boolean,
  isRightIcon?: boolean,
};
