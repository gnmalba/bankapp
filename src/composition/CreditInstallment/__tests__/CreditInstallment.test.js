import React from 'react';
import {fireEvent, render} from '@testing-library/react-native';
import CreditInstallment from '../CreditInstallment';
import {ThemeProvider} from 'styled-components/native';
import {DARK_THEME} from '@utils/constants';
import MockProvider from '@utils/testFramework';
import {CREDIT_CARDS} from '@composition/Widget/__mocks__/accounts.mock';
import {useSelector} from 'react-redux';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useSelector: jest.fn(),
}));

jest.mock('@react-navigation/native', () => {
  return {
    useNavigation: () => ({navigate: jest.fn()}),
  };
});

describe('CreditInstallment', () => {
  const stores = [
    {
      credit: {
        creditList: {
          loading: false,
          data: CREDIT_CARDS,
          error: false,
          errors: [],
        },
        selectedCredit: null,
      },
    },
    {
      credit: {
        creditList: {
          loading: false,
          data: CREDIT_CARDS,
          error: false,
          errors: [],
        },
        selectedCredit: CREDIT_CARDS[0],
        creditDetails: {
          data: {},
        },
      },
    },
    {
      credit: {
        creditList: {
          loading: false,
          data: CREDIT_CARDS.map((card) => {
            return {
              ...card,
              shortcuts: card.shortcuts.map((shortcut) => {
                return {
                  ...shortcut,
                  enabled: false,
                };
              }),
            };
          }),
          error: false,
          errors: [],
        },
        selectedCredit: CREDIT_CARDS[0],
        creditDetails: {
          data: {},
        },
      },
    },
  ];

  afterEach(() => {
    useSelector.mockClear();
  });

  it.each(stores)('Should work as expected', (store) => {
    useSelector.mockImplementation((callback) => {
      return callback(store);
    });

    const props = {
      navigation: {
        navigate: jest.fn(),
      },
    };

    const all = render(
      <MockProvider>
        <ThemeProvider theme={DARK_THEME}>
          <CreditInstallment props={props} />
        </ThemeProvider>
      </MockProvider>,
    );

    [1, 2, 3].forEach((index) => {
      const cardFacilityTouchable = all.getByTestId(
        `CardInstallmentFacilityItem${index}`,
      );

      fireEvent.press(cardFacilityTouchable);
    });

    const viewStatusBtn = all.getByTestId('CardInstallmentFacilityDivider');
    fireEvent.press(viewStatusBtn);

    expect(all.toJSON()).toMatchSnapshot();
  });
});
