## CreditInstallment
CreditInstallment component.

# Usage
```js
import CreditInstallment from '@composition/CreditInstallment';

# Props
```
Prop                      | Type                  | Required                | Description
--------------------------|-----------------------|-------------------------|--------------------------
testID                    | string                | true                    | The testID prop
accessibilityLabel        | string                | true                    | The accessibilityLabel prop
