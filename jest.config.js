module.exports = {
  preset: 'react-native',
  transformIgnorePatterns: [
    'node_modules/(?!react-native|react-navigation)/|@react-navigation/.*|jsencrypt|react-navigation-.*|@react-navigation',
  ],
  setupFiles: ['<rootDir>/jest/setup.js'],
  setupFilesAfterEnv: [],
  collectCoverageFrom: ['<rootDir>/src/**/*.js'],
  coverageDirectory: '<rootDir>/coverage',
  moduleFileExtensions: ['js', 'jsx'],
  moduleDirectories: ['.', 'src', 'node_modules'],
  testPathIgnorePatterns: [
    './node_modules',
    './coverage',
    './android',
    './ios',
  ],
  testResultsProcessor: 'jest-sonar-reporter',
  globals: {
    Platform: {
      OS: 'ios',
    },
    IS_STATIC_DEVICE: true,
  },
  moduleNameMapper: {
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
    '\\.(css|less)$': 'identity-obj-proxy',
  },
};
